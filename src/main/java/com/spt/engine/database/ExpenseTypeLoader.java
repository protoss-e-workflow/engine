package com.spt.engine.database;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.entity.app.Company;
import com.spt.engine.entity.app.ExpenseType;
import com.spt.engine.entity.app.ExpenseTypeByCompany;
import com.spt.engine.entity.app.ExpenseTypeFile;
import com.spt.engine.entity.app.ExpenseTypeReference;
import com.spt.engine.entity.app.ExpenseTypeScreen;
import com.spt.engine.entity.app.Reimburse;
import com.spt.engine.repository.app.CompanyRepository;
import com.spt.engine.repository.app.ExpenseTypeByCompanyRepository;
import com.spt.engine.repository.app.ExpenseTypeFileRepository;
import com.spt.engine.repository.app.ExpenseTypeReferenceRepository;
import com.spt.engine.repository.app.ExpenseTypeRepository;
import com.spt.engine.repository.app.ExpenseTypeScreenRepository;
import com.spt.engine.repository.app.ReimburseRepository;
import com.spt.engine.repository.general.MenuRepository;
import com.spt.engine.repository.general.UserRepository;


//@Component
public class ExpenseTypeLoader implements CommandLineRunner {


	private final UserRepository repository;
	private final MenuRepository menuRepository;
	private final ExpenseTypeRepository expenseTypeRepository;
	private final ExpenseTypeReferenceRepository expenseTypeReferenceRepository;
	private final ExpenseTypeFileRepository expenseTypeFileRepository;
	private final ReimburseRepository reimburseRepository;
	private final ExpenseTypeByCompanyRepository expenseTypeByCompanyRepository;
	private final CompanyRepository companyRepository;
	private final ExpenseTypeScreenRepository expenseTypeScreenRepository;


	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    @Value("${OrgSysServer}")
	public  String OrgSysServer ;
    
	@Autowired
	public ExpenseTypeLoader(UserRepository repository,
                             MenuRepository menuRepository,
							 ExpenseTypeRepository expenseTypeRepository,
							 ExpenseTypeReferenceRepository expenseTypeReferenceRepository,
							 ExpenseTypeFileRepository expenseTypeFileRepository,
							 ReimburseRepository reimburseRepository,
							 ExpenseTypeByCompanyRepository expenseTypeByCompanyRepository,
							 CompanyRepository companyRepository,
							 ExpenseTypeScreenRepository expenseTypeScreenRepository
	)
		                      {

		this.repository = repository;
		this.menuRepository = menuRepository;
		this.expenseTypeRepository = expenseTypeRepository;
		this.expenseTypeReferenceRepository = expenseTypeReferenceRepository;
		this.expenseTypeFileRepository = expenseTypeFileRepository;
		this.reimburseRepository = reimburseRepository;
		this.expenseTypeByCompanyRepository = expenseTypeByCompanyRepository;
		this.companyRepository = companyRepository;
		this.expenseTypeScreenRepository = expenseTypeScreenRepository;

	}

	@Override
	public void run(String... strings) throws Exception {
		Company mitrpholCompany = companyRepository.findOne(1L);
		
		/* Expense Type */
		ExpenseType expenseE00001 = new ExpenseType("810300","636100","E00001","ค่าน้ำมัน(ตามสิทธ์ ที่นอกเหนือจาก fleet card)",false,"FC","EXPF_011","MPG:AdminAuth:0.99-AdminAuth.A002")	;expenseTypeRepository.save(expenseE00001);
		ExpenseType expenseE00002 = new ExpenseType("810300","610100","E00002","ค่าน้ำมันดีเซล ตามสิทธิ์ (ลิตร)",false,"FC","EXPF_001","MPG:AdminAuth:0.99-AdminAuth.A002")	;expenseTypeRepository.save(expenseE00002);
		ExpenseType expenseE00003 = new ExpenseType("810300","610101","E00003","ค่าน้ำมันเบนซิน ตามสิทธิ์ (ลิตร)",false,"FC","EXPF_002","MPG:AdminAuth:0.99-AdminAuth.A002")	;expenseTypeRepository.save(expenseE00003);
		ExpenseType expenseE00004 = new ExpenseType("812820","636401","E00004","ค่าสมาชิกหนังสือและวารสาร",false,"CS",null,"MPG:AdminAuth:0.99-AdminAuth.A003")	;expenseTypeRepository.save(expenseE00004);
		ExpenseType expenseE00005 = new ExpenseType("812820","636402","E00005","ค่าตำราวิชาการ",false,"CS",null,"MPG:AdminAuth:0.99-AdminAuth.A003")	;expenseTypeRepository.save(expenseE00005);
		ExpenseType expenseE00006 = new ExpenseType("811600","636301","E00006","เงินบริจาค",false,"CS",null,"MPG:AdminAuth:0.99-AdminAuth.A003")	;expenseTypeRepository.save(expenseE00006);
		ExpenseType expenseE00007 = new ExpenseType("811609","636302","E00007","การบริจาค ที่ถือเป็นรายจ่ายไม่ได้",false,"CS",null,"MPG:AdminAuth:0.99-AdminAuth.A003")	;expenseTypeRepository.save(expenseE00007);
		ExpenseType expenseE00008 = new ExpenseType("810200","624999","E00008","เงินช่วยเหลือพนักงาน",false,"FC","EXPF_016","MPG:AdminAuth:0.99-AdminAuth.A003")	;expenseTypeRepository.save(expenseE00008);
		ExpenseType expenseE00009 = new ExpenseType("811530","630103","E00009","ค่าซ่อมรถยนต์",false,"FC","EXPF_012","MPG:AdminAuth:0.99-AdminAuth.A004")	;expenseTypeRepository.save(expenseE00009);
		ExpenseType expenseE00010 = new ExpenseType("810710","636502","E00010","ภาษีโรงเรือน",false,"CS",null,"MPG:AdminAuth:0.99-AdminAuth.A005")	;expenseTypeRepository.save(expenseE00010);
		ExpenseType expenseE00011 = new ExpenseType(null,"636503","E00011","ภาษีบำรุงท้องที่",false,"CS",null,"MPG:AdminAuth:0.99-AdminAuth.A005")	;expenseTypeRepository.save(expenseE00011);
		ExpenseType expenseE00012 = new ExpenseType("810720","636501","E00012","ภาษีป้าย",false,"CS",null,"MPG:AdminAuth:0.99-AdminAuth.A005")	;expenseTypeRepository.save(expenseE00012);
		ExpenseType expenseE00013 = new ExpenseType("810790","636504","E00013","ค่าภาษีอื่นๆ นอกจากข้างต้น",false,"CS",null,"MPG:AdminAuth:0.99-AdminAuth.A005")	;expenseTypeRepository.save(expenseE00013);
		ExpenseType expenseE00014 = new ExpenseType("811210","636700","E00014","ค่าประกันภัยรถยนต์",false,"FC","EXPF_013","MPG:AdminAuth:0.99-AdminAuth.A005")	;expenseTypeRepository.save(expenseE00014);
		ExpenseType expenseE00015 = new ExpenseType("810740","636603","E00015","ค่าต่อทะเบียนรถยนต์",false,"FC","EXPF_014","MPG:AdminAuth:0.99-AdminAuth.A005")	;expenseTypeRepository.save(expenseE00015);
		ExpenseType expenseE00016 = new ExpenseType("811150","636200","E00016","ค่าโทรศัพท์",false,"FC","EXPF_015","MPG:AdminAuth:0.99-AdminAuth.A006")	;expenseTypeRepository.save(expenseE00016);
		ExpenseType expenseE00017 = new ExpenseType("810790","636605","E00017","ค่าคัดหนังสือรับรอง",false,"CS",null,"MPG:AdminAuth:0.99-AdminAuth.A006")	;expenseTypeRepository.save(expenseE00017);
		ExpenseType expenseE00018 = new ExpenseType("810730","636505","E00018","ค่าใบอนุญาต",false,"CS",null,"MPG:AdminAuth:0.99-AdminAuth.A006")	;expenseTypeRepository.save(expenseE00018);
		ExpenseType expenseE00019 = new ExpenseType("811950","636200","E00019","ค่าส่งเอกสาร",false,"CS",null,"MPG:AdminAuth:0.99-AdminAuth.A006")	;expenseTypeRepository.save(expenseE00019);
		ExpenseType expenseE00020 = new ExpenseType("811100","636200","E00020","ค่าส่งไปรษณีย์",false,"CS",null,"MPG:AdminAuth:0.99-AdminAuth.A006")	;expenseTypeRepository.save(expenseE00020);
		ExpenseType expenseE00021 = new ExpenseType("811911","636910","E00021","ค่าน้ำประปา ค่าไฟฟ้า ",false,"CS",null,"MPG:AdminAuth:0.99-AdminAuth.A006")	;expenseTypeRepository.save(expenseE00021);
		ExpenseType expenseE00022 = new ExpenseType("811911","611100","E00022","ค่าไฟฟ้าเขต",false,"CS",null,"MPG:AdminAuth:0.99-AdminAuth.A006")	;expenseTypeRepository.save(expenseE00022);
		ExpenseType expenseE00023 = new ExpenseType("810700","636200","E00023","ค่าอากรแสตมป์",false,"CS",null,"MPG:AdminAuth:0.99-AdminAuth.A006")	;expenseTypeRepository.save(expenseE00023);
		ExpenseType expenseE00024 = new ExpenseType("810400","636300","E00024","ค่ารับรองบุคคลภายนอก(ค่าอาหาร)",true,"CS",null,"MPG:Auth:0.99-Auth.F001")	;expenseTypeRepository.save(expenseE00024);
		ExpenseType expenseE00025 = new ExpenseType("810400","636300","E00025","ค่ารับรองบุคคลภายนอก(ค่าของขวัญ)",false,"CS",null,"MPG:Auth:0.99-Auth.F001")	;expenseTypeRepository.save(expenseE00025);
		ExpenseType expenseE00026 = new ExpenseType("810409","636309","E00026","ค่ารับรองบุคคลภายนอก อื่นๆ",false,"CS",null,"MPG:Auth:0.99-Auth.F001")	;expenseTypeRepository.save(expenseE00026);
		ExpenseType expenseE00027 = new ExpenseType("860200","636809","E00027","ค่าใช้จ่ายที่ไม่มีใบเสร็จ",false,"CS",null,"MPG:Auth:0.99-Auth.F003")	;expenseTypeRepository.save(expenseE00027);
		ExpenseType expenseE00028 = new ExpenseType("810840","636600","E00028","ค่าธรรมเนียมธนาคารเพื่อการดำเนินงาน",false,"CS",null,"MPG:Auth:0.99-Auth.F004")	;expenseTypeRepository.save(expenseE00028);
		ExpenseType expenseE00029 = new ExpenseType("812800","636305","E00029","ค่าจัดกิจกรรมภายใน, Innovation",false,"CS",null,"MPG:Auth:0.99-Auth.F004")	;expenseTypeRepository.save(expenseE00029);
		ExpenseType expenseE00030 = new ExpenseType(null,"636809","E00030","ค่าเบี้ยเลี้ยงตำรวจ",false,"CS",null,"MPG:Auth:0.99-Auth.F004")	;expenseTypeRepository.save(expenseE00030);
		ExpenseType expenseE00031 = new ExpenseType("811310","632101","E00031","วัสดุสิ้นเปลือง",false,"CS",null,"MPG:Auth:0.99-Auth.F004")	;expenseTypeRepository.save(expenseE00031);
		ExpenseType expenseE00032 = new ExpenseType("810690","636602","E00032","ค่าธรรมเนียมฟ้อง ดำเนินคดี",false,"CS",null,"MPG:Auth:0.99-Auth.F004")	;expenseTypeRepository.save(expenseE00032);
		ExpenseType expenseE00033 = new ExpenseType("810730","636605","E00033","จ่ายค่าธรรมเนียม ให้กับหน่วยงานราชการ",false,"CS",null,"MPG:Auth:0.99-Auth.F004")	;expenseTypeRepository.save(expenseE00033);
		ExpenseType expenseE00034 = new ExpenseType("810300","636100","E00034","ค่า Taxi ปฏิบัติงานระหว่างวัน",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H00601")	;expenseTypeRepository.save(expenseE00034);
		ExpenseType expenseE00035 = new ExpenseType("810300","636100","E00035","ค่า Taxiในประเทศ ก่อน 7.00  หรือ หลังเวลา 20.00",false,"FC","EXPF_010","MPG:HRAuth:0.99-HRAuth.H00601")	;expenseTypeRepository.save(expenseE00035);
		ExpenseType expenseE00036 = new ExpenseType("810300","636100","E00036","ค่า Taxiในประเทศ",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H00601")	;expenseTypeRepository.save(expenseE00036);
		ExpenseType expenseE00037 = new ExpenseType("810300","636100","E00037","ค่าที่พักในประเทศ",false,"FC","EXPF_009","MPG:HRAuth:0.99-HRAuth.H006")	;expenseTypeRepository.save(expenseE00037);
		ExpenseType expenseE00038 = new ExpenseType("810300","636100","E00038","ค่าน้ำมัน(รถเช่า)",false,"FC","EXPF_005","MPG:HRAuth:0.99-HRAuth.H006")	;expenseTypeRepository.save(expenseE00038);
		ExpenseType expenseE00039 = new ExpenseType("810300","624100","E00039","ค่าเบี้ยเลี้ยงเดินทางในประเทศ",false,"FC","EXPF_006","MPG:HRAuth:0.99-HRAuth.H006")	;expenseTypeRepository.save(expenseE00039);
		ExpenseType expenseE00040 = new ExpenseType("810300","636100","E00040","ค่าทางด่วน",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H006")	;expenseTypeRepository.save(expenseE00040);
		ExpenseType expenseE00041 = new ExpenseType("810300","636100","E00041","ค่าที่จอดรถในประเทศ",false,"FC","EXPF_008","MPG:HRAuth:0.99-HRAuth.H006")	;expenseTypeRepository.save(expenseE00041);
		ExpenseType expenseE00042 = new ExpenseType("810300","636100","E00042","ค่าทางด่วน, ค่าบัตรผ่านทาง Easy Pass (ปฏิบัติงานระหว่างวัน)",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H006")	;expenseTypeRepository.save(expenseE00042);
		ExpenseType expenseE00043 = new ExpenseType("810300","636100","E00043","ค่าที่จอดรถในประเทศ (ปฏิบัติงานระหว่างวัน)",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H006")	;expenseTypeRepository.save(expenseE00043);
		ExpenseType expenseE00044 = new ExpenseType("810310","636101","E00044","ค่าตั๋วเครื่องบินต่างประเทศ",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H008")	;expenseTypeRepository.save(expenseE00044);
		ExpenseType expenseE00045 = new ExpenseType("810310","636101","E00045","ค่าเบี้ยเลี้ยงเดินทางต่างประเทศ",false,"FC","EXPF_004","MPG:HRAuth:0.99-HRAuth.H008")	;expenseTypeRepository.save(expenseE00045);
		ExpenseType expenseE00046 = new ExpenseType("810310","636101","E00046","ค่าที่พักต่างประเทศ",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H008")	;expenseTypeRepository.save(expenseE00046);
		ExpenseType expenseE00047 = new ExpenseType("810310","636101","E00047","ค่ารถเดินทางไปกลับสนามบิน-ที่พัก",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H008")	;expenseTypeRepository.save(expenseE00047);
		ExpenseType expenseE00048 = new ExpenseType("810310","636101","E00048","ค่าที่จอดรถต่างประเทศ (สนามบินในประเทศไทย)",false,"FC","EXPF_003","MPG:HRAuth:0.99-HRAuth.H008")	;expenseTypeRepository.save(expenseE00048);
		ExpenseType expenseE00049 = new ExpenseType("810310","636101","E00049","ค่า Taxi ต่างประเทศ",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H008")	;expenseTypeRepository.save(expenseE00049);
		ExpenseType expenseE00050 = new ExpenseType("810310","636101","E00050","ค่าวัคซีนเดินทางต่างประเทศ",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H008")	;expenseTypeRepository.save(expenseE00050);
		ExpenseType expenseE00051 = new ExpenseType("810310","636101","E00051","ค่าใช้จ่ายสื่อสารตปท. (อินเตอร์เนตต่างประเทศ,ค่าซิมการ์ดและค่าโทรศัพท์)",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H008")	;expenseTypeRepository.save(expenseE00051);
		ExpenseType expenseE00052 = new ExpenseType("810310","636101","E00052","ค่าอาหารต่างประเทศ",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H008")	;expenseTypeRepository.save(expenseE00052);
		ExpenseType expenseE00053 = new ExpenseType("810310","636101","E00053","ทำ VISA",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H008")	;expenseTypeRepository.save(expenseE00053);
		ExpenseType expenseE00054 = new ExpenseType("810500","636102","E00054","ค่าอบรมภายนอก",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H010")	;expenseTypeRepository.save(expenseE00054);
		ExpenseType expenseE00055 = new ExpenseType("810520","636104","E00055","ค่าใช้จ่ายในการฝึกอบรมต่างประเทศ",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H011")	;expenseTypeRepository.save(expenseE00055);
		ExpenseType expenseE00056 = new ExpenseType("810300","636100","E00056","เงินช่วยเหลือน้ำมันกรณีนำรถยนต์ส่วนตัวใช้ปฏิบัติงาน",false,"FC","EXPF_007","MPG:HRAuth:0.99-HRAuth.H012")	;expenseTypeRepository.save(expenseE00056);
		ExpenseType expenseE00057 = new ExpenseType("812600","636304","E00057","ค่าใช้จ่ายจัดประชุม",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H013")	;expenseTypeRepository.save(expenseE00057);
		ExpenseType expenseE00058 = new ExpenseType("812600","624101","E00058","ค่าอาหารกรรมการและพนักงาน",false,"CS",null,"MPG:HRAuth:0.99-HRAuth.H013")	;expenseTypeRepository.save(expenseE00058);
		ExpenseType expenseE00059 = new ExpenseType(null,"630102","E00059","ค่าซ่อมแซมเครื่องจักรและอุปกรณ์",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.M001")	;expenseTypeRepository.save(expenseE00059);
		ExpenseType expenseE00060 = new ExpenseType("812020","636303","E00060","ค่าจัดกิจกรรมภายนอก, CSR",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.P001")	;expenseTypeRepository.save(expenseE00060);
		ExpenseType expenseE00061 = new ExpenseType("812090","636303","E00061","ค่าตัวอย่างน้ำตาล",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.P001")	;expenseTypeRepository.save(expenseE00061);
		ExpenseType expenseE00062 = new ExpenseType("812000",null,"E00062","ค่าจัดกิจกรรมส่งเสริมการขาย",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.P001")	;expenseTypeRepository.save(expenseE00062);
		ExpenseType expenseE00063 = new ExpenseType("812110","636303","E00063","ค่าของรางวัลกิจกรรมการตลาด, ค่าประชาสัมพันธ์",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.P001")	;expenseTypeRepository.save(expenseE00063);
		ExpenseType expenseE00064 = new ExpenseType("811400","636400","E00064","ค่ากระดาษและสำเนาเอกสาร",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00064);
		ExpenseType expenseE00065 = new ExpenseType("812890","636999","E00065","ค่าใช้จ่ายเบ็ดเตล็ด มีใบเสร็จ",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00065);
		ExpenseType expenseE00066 = new ExpenseType("811510","630101","E00066","ค่าซ่อมแซมอาคาร",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00066);
		ExpenseType expenseE00067 = new ExpenseType("811540","630104","E00067","ค่าซ่อมแซมอุปกรณ์",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00067);
		ExpenseType expenseE00068 = new ExpenseType(null,"635100","E00068","คชจ.รักษาสภาพแวดล้อม ค.ปลอดภัยในการทำงาน มอก.18001",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00068);
		ExpenseType expenseE00069 = new ExpenseType(null,"635200","E00069","ค่าวิเคราะห์สิ่งแวดล้อม",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00069);
		ExpenseType expenseE00070 = new ExpenseType(null,"635201","E00070","ค่าใช้จ่ายในการรักษาสิ่งแวดล้อม ISO 14000",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00070);
		ExpenseType expenseE00071 = new ExpenseType(null,"635202","E00071","ค่าใช้จ่ายโครงการISO9002",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00071);
		ExpenseType expenseE00072 = new ExpenseType(null,"635204","E00072","ค่าใช้จ่ายโครงการ  ISO 50001",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00072);
		ExpenseType expenseE00073 = new ExpenseType(null,"635900","E00073","ค่าใบรับรองคุณภาพ",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00073);
		ExpenseType expenseE00074 = new ExpenseType(null,"636900","E00074","ค่าวิเคราะห์คุณภาพสินค้า",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00074);
		ExpenseType expenseE00075 = new ExpenseType(null,"636901","E00075","ค่าจัดกิจกรรมโครงการเพื่อพัฒนาระบบงาน",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00075);
		ExpenseType expenseE00076 = new ExpenseType(null,"636903","E00076","ค่าบำบัดสิ่งตกค้าง",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00076);
		ExpenseType expenseE00077 = new ExpenseType(null,"636905","E00077","ค่าส่งเครื่องมือและอุปกรณ์ไปสอบเทียบ",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00077);
		ExpenseType expenseE00078 = new ExpenseType("811410","636400","E00078","ค่าตรายาง",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00078);
		ExpenseType expenseE00079 = new ExpenseType("811300","632100","E00079","เครื่องตกแต่งสำนักงาน",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00079);
		ExpenseType expenseE00080 = new ExpenseType("810290","624999","E00080","สวัสดิการพนักงาน",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00080);
		ExpenseType expenseE00081 = new ExpenseType("850110","650110","E00081","จ่ายเงินกรณีซื้อทรัพย์สิน",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.PC01")	;expenseTypeRepository.save(expenseE00081);
		ExpenseType expenseE00082 = new ExpenseType(null,"510271","E00082","ค่าใช้จ่ายจัดหาอ้อย-สนับสนุนแผนและนโยบาย",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.RM01")	;expenseTypeRepository.save(expenseE00082);
		ExpenseType expenseE00083 = new ExpenseType(null,"510272","E00083","ค่าใช้จ่ายจัดหาอ้อย-ส่วนต่างจัดหาอ้อยและบริหาร",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.RM01")	;expenseTypeRepository.save(expenseE00083);
		ExpenseType expenseE00084 = new ExpenseType(null,"510273","E00084","ค่าใช้จ่ายจัดหาอ้อย-ส่งเสริมและปกป้องอ้อย",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.RM01")	;expenseTypeRepository.save(expenseE00084);
		ExpenseType expenseE00085 = new ExpenseType(null,"510274","E00085","ค่าใช้จ่ายจัดหาอ้อย-ชุมชนและชาวไร่",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.RM01")	;expenseTypeRepository.save(expenseE00085);
		ExpenseType expenseE00086 = new ExpenseType(null,"510275","E00086","ค่าใช้จ่ายจัดหาอ้อย-สนับสนุนคุณภาพอ้อย",false,"CS",null,"MPG:OtherAuth:0.99-OtherAuth.RM01")	;expenseTypeRepository.save(expenseE00086);

		/* Setup Screen */


		/* E00001 : ค่าน้ำมัน(ตามสิทธ์ ที่นอกเหนือจาก feed card) */
		ExpenseTypeScreen expenseE00001_Screen1 = new ExpenseTypeScreen("จำนวนลิตร", "Number of liters", "Number", "liter", true,expenseE00001);        expenseTypeScreenRepository.save(expenseE00001_Screen1);
		ExpenseTypeScreen expenseE00001_Screen2 = new ExpenseTypeScreen("เดือน", "Month", "Date", "month", true,expenseE00001);        expenseTypeScreenRepository.save(expenseE00001_Screen2);


		/* E00002 : ค่าน้ำมันดีเซล ตามสิทธิ์ (ลิตร) */
		ExpenseTypeScreen expenseE00002_Screen1 = new ExpenseTypeScreen("สิทธิ์คงเหลือ ณ ปัจจุบัน", "Balance Liter Per Month", "Number", "variable3", true,expenseE00002);        expenseTypeScreenRepository.save(expenseE00002_Screen1);


		/* E00003 : ค่าน้ำมันเบนซิน ตามสิทธิ์ (ลิตร) */
		ExpenseTypeScreen expenseE00003_Screen1 = new ExpenseTypeScreen("สิทธิ์คงเหลือ ณ ปัจจุบัน", "Balance Liter Per Month", "Number", "variable3", true,expenseE00003);        expenseTypeScreenRepository.save(expenseE00003_Screen1);


		/* E00004 : ค่าสมาชิกหนังสือและวารสาร */
		ExpenseTypeScreen expenseE00004_Screen1 = new ExpenseTypeScreen("ชื่อหนังสือ", "Book name", "String", "subjectName", true,expenseE00004);        expenseTypeScreenRepository.save(expenseE00004_Screen1);


		/* E00005 : ค่าตำราวิชาการ */
		ExpenseTypeScreen expenseE00005_Screen1 = new ExpenseTypeScreen("ชื่อตำราวิชาการ", "Name of academic textbook", "String", "subjectName", true,expenseE00005);        expenseTypeScreenRepository.save(expenseE00005_Screen1);


		/* E00006 : เงินบริจาค */
		ExpenseTypeScreen expenseE00006_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับบริจาค", "Name of the recipient", "String", "receiver", true,expenseE00006);        expenseTypeScreenRepository.save(expenseE00006_Screen1);


		/* E00007 : การบริจาค ที่ถือเป็นรายจ่ายไม่ได้ */
//		ExpenseTypeScreen expenseE00007_Screen1 = new ExpenseTypeScreen("ชื่อพนักงาน", "Employee Name", "String", "employee", true,expenseE00007);        expenseTypeScreenRepository.save(expenseE00007_Screen1);


		/* E00008 : เงินช่วยเหลือพนักงาน */
		ExpenseTypeScreen expenseE00008_Screen1 = new ExpenseTypeScreen("ชื่อพนักงาน", "Employee Name", "String", "employee", true,expenseE00008);        expenseTypeScreenRepository.save(expenseE00008_Screen1);
		ExpenseTypeScreen expenseE00008_Screen2 = new ExpenseTypeScreen("ประเภทเงินช่วยเหลือ", "Type of subsidy", "String", "variable5", false,expenseE00008);        expenseTypeScreenRepository.save(expenseE00008_Screen2);


		/* E00009 : ค่าซ่อมรถยนต์ */
		ExpenseTypeScreen expenseE00009_Screen1 = new ExpenseTypeScreen("ทะเบียนรถ", "License plate", "String", "carLicense", true,expenseE00009);        expenseTypeScreenRepository.save(expenseE00009_Screen1);


		/* E00010 : ภาษีโรงเรือน */
//		ExpenseTypeScreen expenseE00010_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00010);        expenseTypeScreenRepository.save(expenseE00010_Screen1);


		/* E00011 : ภาษีบำรุงท้องที่ */
//		ExpenseTypeScreen expenseE00011_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00011);        expenseTypeScreenRepository.save(expenseE00011_Screen1);


		/* E00012 : ภาษีป้าย */
//		ExpenseTypeScreen expenseE00012_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00012);        expenseTypeScreenRepository.save(expenseE00012_Screen1);


		/* E00013 : ค่าภาษีอื่นๆ นอกจากข้างต้น */
//		ExpenseTypeScreen expenseE00013_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00013);        expenseTypeScreenRepository.save(expenseE00013_Screen1);



		/* E00014 : ค่าประกันภัยรถยนต์ */
		ExpenseTypeScreen expenseE00014_Screen1 = new ExpenseTypeScreen("ทะเบียนรถ", "License plate", "String", "carLicense", true,expenseE00014);        expenseTypeScreenRepository.save(expenseE00014_Screen1);


		/* E00015 : ค่าต่อทะเบียนรถยนต์ */
		ExpenseTypeScreen expenseE00015_Screen1 = new ExpenseTypeScreen("ทะเบียนรถ", "License plate", "String", "carLicense", true,expenseE00015);        expenseTypeScreenRepository.save(expenseE00015_Screen1);


		/* E00016 : ค่าโทรศัพท์ */
		ExpenseTypeScreen expenseE00016_Screen1 = new ExpenseTypeScreen("หมายเลขโทรศัพท์", "Phone number", "String", "phoneNumber", true,expenseE00016);        expenseTypeScreenRepository.save(expenseE00016_Screen1);
		ExpenseTypeScreen expenseE00016_Screen2 = new ExpenseTypeScreen("เดือน", "Month", "Date", "month", true,expenseE00016);        expenseTypeScreenRepository.save(expenseE00016_Screen2);


		/* E00017 : ค่าคัดหนังสือรับรอง */
//		ExpenseTypeScreen expenseE00017_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00017);        expenseTypeScreenRepository.save(expenseE00017_Screen1);


		/* E00018 : ค่าใบอนุญาต */
//		ExpenseTypeScreen expenseE00018_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00018);        expenseTypeScreenRepository.save(expenseE00018_Screen1);


		/* E00019 : ค่าส่งเอกสาร */
//		ExpenseTypeScreen expenseE00019_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00019);        expenseTypeScreenRepository.save(expenseE00019_Screen1);


		/* E00020 : ค่าส่งไปรษณีย์ */
//		ExpenseTypeScreen expenseE00020_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00020);        expenseTypeScreenRepository.save(expenseE00020_Screen1);


		/* E00021 : ค่าน้ำประปา ค่าไฟฟ้า */
//		ExpenseTypeScreen expenseE00021_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00021);        expenseTypeScreenRepository.save(expenseE00021_Screen1);


		/* E00022 : ค่าไฟฟ้าเขต */
//		ExpenseTypeScreen expenseE00022_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00022);        expenseTypeScreenRepository.save(expenseE00022_Screen1);


		/* E00023 : ค่าอากรแสตมป์ */
//		ExpenseTypeScreen expenseE00023_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00023);        expenseTypeScreenRepository.save(expenseE00023_Screen1);



		/* E00024 : ค่ารับรองบุคคลภายนอก(ค่าอาหาร) */
		ExpenseTypeScreen expenseE00024_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00024);        expenseTypeScreenRepository.save(expenseE00024_Screen1);
		ExpenseTypeScreen expenseE00024_Screen2 = new ExpenseTypeScreen("บริษัทผู้รับของ", "Receiver Company", "String", "receiverCompany", true,expenseE00024);        expenseTypeScreenRepository.save(expenseE00024_Screen2);


		/* E00025 : ค่ารับรองบุคคลภายนอก(ค่าของขวัญ) */
		ExpenseTypeScreen expenseE00025_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00025);        expenseTypeScreenRepository.save(expenseE00025_Screen1);
		ExpenseTypeScreen expenseE00025_Screen2 = new ExpenseTypeScreen("บริษัทผู้รับของ", "Receiver Company", "String", "receiverCompany", true,expenseE00025);        expenseTypeScreenRepository.save(expenseE00025_Screen2);


		/* E00026 : ค่ารับรองบุคคลภายนอก อื่นๆ */
//		ExpenseTypeScreen expenseE00026_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00026);        expenseTypeScreenRepository.save(expenseE00026_Screen1);


		/* E00027 : ค่าใช้จ่ายที่ไม่มีใบเสร็จ */
//		ExpenseTypeScreen expenseE00027_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00027);        expenseTypeScreenRepository.save(expenseE00027_Screen1);


		/* E00028 : ค่าธรรมเนียมธนาคารเพื่อการดำเนินงาน */
//		ExpenseTypeScreen expenseE00028_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00028);        expenseTypeScreenRepository.save(expenseE00028_Screen1);



		/* E00029 : ค่าจัดกิจกรรมภายใน */
		ExpenseTypeScreen expenseE00029_Screen1 = new ExpenseTypeScreen("ชื่อกิจกรรม", "Event name", "String", "variable7", true,expenseE00029);        expenseTypeScreenRepository.save(expenseE00029_Screen1);


		/* E00030 : ค่าเบี้ยเลี้ยงตำรวจ */
//		ExpenseTypeScreen expenseE00030_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00030);        expenseTypeScreenRepository.save(expenseE00030_Screen1);


		/* E00031 : วัสดุสิ้นเปลือง */
//		ExpenseTypeScreen expenseE00031_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00031);        expenseTypeScreenRepository.save(expenseE00031_Screen1);


		/* E00032 : ค่าธรรมเนียม ฟ้องดำเนินคดี */
//		ExpenseTypeScreen expenseE00032_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00032);        expenseTypeScreenRepository.save(expenseE00032_Screen1);


		/* E00033 : จ่ายค่าธรรมเนียม ให้กับหน่วยงานราชการี */
//		ExpenseTypeScreen expenseE00033_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00033);        expenseTypeScreenRepository.save(expenseE00033_Screen1);



		/* E00034 : ค่า Taxi ปฏิบัติงานระหว่างวัน */
		ExpenseTypeScreen expenseE00034_Screen1 = new ExpenseTypeScreen("เดินทางจาก", "From", "String", "place1", true,expenseE00034);        expenseTypeScreenRepository.save(expenseE00034_Screen1);
		ExpenseTypeScreen expenseE00034_Screen2 = new ExpenseTypeScreen("เดินทางถึง", "To", "String", "place2", true,expenseE00034);           expenseTypeScreenRepository.save(expenseE00034_Screen2);
		ExpenseTypeScreen expenseE00034_Screen3 = new ExpenseTypeScreen("วันที่เดินทาง", "Travel Date", "Date", "date1", false,expenseE00034);    expenseTypeScreenRepository.save(expenseE00034_Screen3);
		ExpenseTypeScreen expenseE00034_Screen4 = new ExpenseTypeScreen("เวลาตั้งแต่", "Time From", "String", "time1", false,expenseE00034);     expenseTypeScreenRepository.save(expenseE00034_Screen4);
		ExpenseTypeScreen expenseE00034_Screen5 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", false,expenseE00034);         expenseTypeScreenRepository.save(expenseE00034_Screen5);


		/* E00035 : ค่า Taxiในประเทศ ก่อน 7.00  หรือ หลังเวลา 20.00 */
		ExpenseTypeScreen expenseE00035_Screen1 = new ExpenseTypeScreen("เดินทางจาก", "From", "String", "place1", true,expenseE00035);        expenseTypeScreenRepository.save(expenseE00035_Screen1);
		ExpenseTypeScreen expenseE00035_Screen2 = new ExpenseTypeScreen("เดินทางถึง", "To", "String", "place2", true,expenseE00035);           expenseTypeScreenRepository.save(expenseE00035_Screen2);
		ExpenseTypeScreen expenseE00035_Screen3 = new ExpenseTypeScreen("เดือน", "Month", "Date", "month", true,expenseE00035);        expenseTypeScreenRepository.save(expenseE00035_Screen3);


		/* E00036 : ค่า Taxiในประเทศ */
		ExpenseTypeScreen expenseE00036_Screen1 = new ExpenseTypeScreen("เดินทางจาก", "From", "String", "place1", true,expenseE00036);        expenseTypeScreenRepository.save(expenseE00036_Screen1);
		ExpenseTypeScreen expenseE00036_Screen2 = new ExpenseTypeScreen("เดินทางถึง", "To", "String", "place2", true,expenseE00036);           expenseTypeScreenRepository.save(expenseE00036_Screen2);
		ExpenseTypeScreen expenseE00036_Screen3 = new ExpenseTypeScreen("วันที่เดินทาง", "Travel Date", "Date", "date1", false,expenseE00036);    expenseTypeScreenRepository.save(expenseE00036_Screen3);
		ExpenseTypeScreen expenseE00036_Screen4 = new ExpenseTypeScreen("เวลาตั้งแต่", "Time From", "String", "time1", false,expenseE00036);     expenseTypeScreenRepository.save(expenseE00036_Screen4);
		ExpenseTypeScreen expenseE00036_Screen5 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", false,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);


		/* E00037 : ค่าที่พักในประเทศ */
		ExpenseTypeScreen expenseE00037_Screen1 = new ExpenseTypeScreen("ชื่อที่พัก", "Accommodation Name", "String", "variable6", true,expenseE00037);        expenseTypeScreenRepository.save(expenseE00037_Screen1);
		ExpenseTypeScreen expenseE00037_Screen2 = new ExpenseTypeScreen("เข้าพักวันที่", "Check in", "Date", "date1", true,expenseE00037);        expenseTypeScreenRepository.save(expenseE00037_Screen2);
		ExpenseTypeScreen expenseE00037_Screen3 = new ExpenseTypeScreen("ออกวันที่", "Check out", "Date", "date2", true,expenseE00037);        expenseTypeScreenRepository.save(expenseE00037_Screen3);


		/* E00038 : ค่าน้ำมัน(รถเช่า) */
		ExpenseTypeScreen expenseE00038_Screen1 = new ExpenseTypeScreen("เดินทางจาก", "From", "String", "place1", true,expenseE00038);        expenseTypeScreenRepository.save(expenseE00038_Screen1);
		ExpenseTypeScreen expenseE00038_Screen2 = new ExpenseTypeScreen("เดินทางถึง", "To", "String", "place2", true,expenseE00038);           expenseTypeScreenRepository.save(expenseE00038_Screen2);


		/* E00039 : ค่าเบี้ยเลี้ยงเดินทางในประเทศ */
		ExpenseTypeScreen expenseE00039_Screen1 = new ExpenseTypeScreen("สถานที่", "Location", "String", "variable8", true,expenseE00038);           expenseTypeScreenRepository.save(expenseE00039_Screen1);
		ExpenseTypeScreen expenseE00039_Screen2 = new ExpenseTypeScreen("ตั้งแต่วันที่", "Date From", "Date", "date1", true,expenseE00039);         expenseTypeScreenRepository.save(expenseE00039_Screen2);
		ExpenseTypeScreen expenseE00039_Screen3 = new ExpenseTypeScreen("ตั้งแต่เวลา", "Time From", "String", "time1", true,expenseE00039);         expenseTypeScreenRepository.save(expenseE00039_Screen3);
		ExpenseTypeScreen expenseE00039_Screen4 = new ExpenseTypeScreen("ถึงวันที่", "Date To", "Date", "date2", true,expenseE00039);         expenseTypeScreenRepository.save(expenseE00039_Screen4);
		ExpenseTypeScreen expenseE00039_Screen5 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00039);         expenseTypeScreenRepository.save(expenseE00039_Screen5);
		ExpenseTypeScreen expenseE00039_Screen6 = new ExpenseTypeScreen("อัตราเปอร์เซ็นต์", "Percentage", "String", "variable1", true,expenseE00039);         expenseTypeScreenRepository.save(expenseE00039_Screen6);


		/* E00040 : ค่าทางด่วน */
//		ExpenseTypeScreen expenseE00040_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00040);        expenseTypeScreenRepository.save(expenseE00040_Screen1);


		/* E00041 : ค่าที่จอดรถในประเทศ */
//		ExpenseTypeScreen expenseE00041_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00041);        expenseTypeScreenRepository.save(expenseE00041_Screen1);


		/* E00042 : ค่าทางด่วน, ค่าบัตรผ่านทาง Easy Pass (ปฏิบัติงานระหว่างวัน) */
//		ExpenseTypeScreen expenseE00042_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00042);        expenseTypeScreenRepository.save(expenseE00042_Screen1);


		/* E00043 : ค่าที่จอดรถในประเทศ (ปฏิบัติงานระหว่างวัน) */
//		ExpenseTypeScreen expenseE00043_Screen1 = new ExpenseTypeScreen("ชื่อผู้รับของ", "Receiver Name", "String", "receiver", true,expenseE00043);        expenseTypeScreenRepository.save(expenseE00043_Screen1);



        /* E00044 : ค่าตั๋วเครื่องบินต่างประเทศ */
//		ExpenseTypeScreen expenseE00044_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00044);         expenseTypeScreenRepository.save(expenseE00044_Screen1);

        /* E00045 : ค่าเบี้ยเลี้ยงเดินทางต่างประเทศ */
		ExpenseTypeScreen expenseE00045_Screen1 = new ExpenseTypeScreen("ประเทศ", "Country", "String", "country", true,expenseE00045);         expenseTypeScreenRepository.save(expenseE00045_Screen1);
		ExpenseTypeScreen expenseE00045_Screen2 = new ExpenseTypeScreen("ตั้งแต่วันที่", "Date From", "String", "time1", true,expenseE00045);         expenseTypeScreenRepository.save(expenseE00045_Screen2);
		ExpenseTypeScreen expenseE00045_Screen3 = new ExpenseTypeScreen("ถึงเวลา", "Date To", "String", "time2", true,expenseE00045);         expenseTypeScreenRepository.save(expenseE00045_Screen3);
		ExpenseTypeScreen expenseE00045_Screen4 = new ExpenseTypeScreen("อัตราเปอเซนต์", "Percent Rate", "Number", "variable1", true,expenseE00045);         expenseTypeScreenRepository.save(expenseE00045_Screen4);
		ExpenseTypeScreen expenseE00045_Screen5 = new ExpenseTypeScreen("อัตราแลกเปลี่ยน", "Exchange Rate", "Number", "variable2", true,expenseE00045);         expenseTypeScreenRepository.save(expenseE00045_Screen5);


		/* E00046 : ค่าที่พักต่างประเทศ */
		ExpenseTypeScreen expenseE00046_Screen1 = new ExpenseTypeScreen("ชื่อที่พัก", "Name Place", "String", "hotelCode", true,expenseE00046);         expenseTypeScreenRepository.save(expenseE00046_Screen1);
		ExpenseTypeScreen expenseE00046_Screen2 = new ExpenseTypeScreen("เข้าพักวันที่", "Date From", "Date", "date1", false,expenseE00046);         expenseTypeScreenRepository.save(expenseE00046_Screen2);
		ExpenseTypeScreen expenseE00046_Screen3 = new ExpenseTypeScreen("ออกวันที่", "Date To", "Date", "date2", false,expenseE00046);         expenseTypeScreenRepository.save(expenseE00046_Screen3);

       /* E00047 : ค่ารถเดินทางไปกลับสนามบิน-ที่พัก */
//		ExpenseTypeScreen expenseE00047_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

		/* E00048 : ค่าที่จอดรถต่างประเทศ */
		ExpenseTypeScreen expenseE00048_Screen1 = new ExpenseTypeScreen("สถานที่", "Place", "String", "place1", false,expenseE00048);         expenseTypeScreenRepository.save(expenseE00048_Screen1);


	 /* E00049 : ่่ค่า Taxi ต่างประเทศ */
		ExpenseTypeScreen expenseE00049_Screen1 = new ExpenseTypeScreen("เดินทางจาก", "From", "String", "place1", true,expenseE00049);         expenseTypeScreenRepository.save(expenseE00049_Screen1);
		ExpenseTypeScreen expenseE00049_Screen2 = new ExpenseTypeScreen("เดินทางถึง", "To", "String", "place2", true,expenseE00049);         expenseTypeScreenRepository.save(expenseE00049_Screen2);

	/* E00050 : ค่าวัคซีนเดินทางต่างประเทศ */
//		ExpenseTypeScreen expenseE00050_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00051 : ค่าใช้จ่ายสื่อสารตปท */
//		ExpenseTypeScreen expenseE00051_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00052 : ค่าอาหารต่างประเทศ */
//		ExpenseTypeScreen expenseE00052_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00053 : ทำ VISA */
//		ExpenseTypeScreen expenseE00053_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00054 : ค่าอบรมภายนอก */
		ExpenseTypeScreen expenseE00054_Screen1 = new ExpenseTypeScreen("วันที่ฝึกอบรม", "Date", "Date", "date1", true,expenseE00054);         expenseTypeScreenRepository.save(expenseE00054_Screen1);
		ExpenseTypeScreen expenseE00054_Screen2 = new ExpenseTypeScreen("หัวข้อฝึกอบรม", "Subject Name", "String", "subjectName", true,expenseE00054);         expenseTypeScreenRepository.save(expenseE00054_Screen2);
		ExpenseTypeScreen expenseE00054_Screen3 = new ExpenseTypeScreen("สถานที่ฝึกอบรม", "Place", "String", "place1", false,expenseE00054);         expenseTypeScreenRepository.save(expenseE00054_Screen3);

	/* E00055 : ค่าใช้จ่ายในการฝึกอบรมต่างประเทศ */
		ExpenseTypeScreen expenseE00055_Screen1 = new ExpenseTypeScreen("วันที่ฝึกอบรม", "Date", "String", "date", true,expenseE00055);         expenseTypeScreenRepository.save(expenseE00055_Screen1);
		ExpenseTypeScreen expenseE00055_Screen2 = new ExpenseTypeScreen("หัวข้อฝึกอบรม", "Subject Name", "String", "subjectName", true,expenseE00055);         expenseTypeScreenRepository.save(expenseE00055_Screen2);
		ExpenseTypeScreen expenseE00055_Screen3 = new ExpenseTypeScreen("ประเทศ", "Country", "String", "country", true,expenseE00055);         expenseTypeScreenRepository.save(expenseE00055_Screen3);

	/* E00056 : เงินช่วยเหลือน้ำมันกรณีนำรถยนต์ส่วนตัวใช้ปฏิบัติงาน */
		ExpenseTypeScreen expenseE00056_Screen1 = new ExpenseTypeScreen("วันที่เดินทาง", "Travel Date", "Date", "date1", true,expenseE00056);         expenseTypeScreenRepository.save(expenseE00056_Screen1);
		ExpenseTypeScreen expenseE00056_Screen2 = new ExpenseTypeScreen("เดินทางจาก", "Travel From", "String", "place1", true,expenseE00056);         expenseTypeScreenRepository.save(expenseE00056_Screen2);
		ExpenseTypeScreen expenseE00056_Screen3 = new ExpenseTypeScreen("เดินทางถึง", "Time To", "String", "place2", true,expenseE00056);         expenseTypeScreenRepository.save(expenseE00056_Screen3);
		ExpenseTypeScreen expenseE00056_Screen4 = new ExpenseTypeScreen("ระยะทาง", "Distance", "String", "variable4", false,expenseE00056);         expenseTypeScreenRepository.save(expenseE00056_Screen4);

	/* E00057 : ค่าใช้จ่ายจัดประชุม */
		ExpenseTypeScreen expenseE00057_Screen1 = new ExpenseTypeScreen("หัวข้อการประชุม", "Subject Name", "String", "subjectName", true,expenseE00057);         expenseTypeScreenRepository.save(expenseE00057_Screen1);

	/* E0005 : ค่าอาหารกรรมการและพนักงาน */
//		ExpenseTypeScreen expenseE00058_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00059 : ค่าซ่อมแซมเครื่องจักรและอุปกรณ์ */
//		ExpenseTypeScreen expenseE00059_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00060 : ค่าจัดกิจกรรมภายนอก */
		ExpenseTypeScreen expenseE00060_Screen1 = new ExpenseTypeScreen("ชื่อกิจกรรม", "Activity Name", "String", "subjectName", true,expenseE00060);         expenseTypeScreenRepository.save(expenseE00060_Screen1);

    /* E00061 : ค่าตัวอย่างน้ำตาล */
//		ExpenseT	ypeScreen expenseE00061_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00062 : ค่าจัดกิจกรรมส่งเสริมการขาย */
//		ExpenseTypeScreen expenseE00062_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00063 : ค่าของรางวัลกิจกรรมการตลาด */
		ExpenseTypeScreen expenseE00063_Screen1 = new ExpenseTypeScreen("ชื่อกิจกรรม", "Activity Name", "String", "subjectName", true,expenseE00063);         expenseTypeScreenRepository.save(expenseE00063_Screen1);

	/* E00064 : ค่ากระดาษและสำเนาเอกสาร */
//		ExpenseTypeScreen expenseE00064_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00065 : ค่ากระดาษและสำเนาเอกสาร */
//		ExpenseTypeScreen expenseE00065_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00066 : ค่าซ่อมแซมอาคาร */
//		ExpenseTypeScreen expenseE00066_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00067 : ค่าซ่อมแซมอุปกรณ์ */
//		ExpenseTypeScreen expenseE00067_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00068 : รักษาสภาพแวดล้อม */
//		ExpenseTypeScreen expenseE00068_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00069 : ค่าวิเคราะห์สิ่งแวดล้อม */
//		ExpenseTypeScreen expenseE00069_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00070 : ค่าใช้จ่ายในการรักษาสิ่งแวดล้อม */
//		ExpenseTypeScreen expenseE00070_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00071 : ค่าใช้จ่ายโครงการISO9002 */
//		ExpenseTypeScreen expenseE00071_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00072 : ค่าใช้จ่ายโครงการISO5001 */
//		ExpenseTypeScreen expenseE00072_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00073 : ค่าใบรับรองคุณภาพ */
//		ExpenseTypeScreen expenseE00073_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00074 : ค่าวิเคราะห์คุณภาพสินค้า */
//		ExpenseTypeScreen expenseE00074_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00075 : ค่าจัดกิจกรรมโครงการเพื่อพัฒนาระบบงาน */
//		ExpenseTypeScreen expenseE00075_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00076 : ค่าบำบัดสิ่งตกค้าง */
//		ExpenseTypeScreen expenseE00076_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00077 : ค่าส่งเครื่องมือและอุปกรณ์ไปสอบเทียบ */
//		ExpenseTypeScreen expenseE00077_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00078 : ค่าตรายาง */
//		ExpenseTypeScreen expenseE00078_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00079 : เครื่องตกแต่งสำนักงาน */
//		ExpenseTypeScreen expenseE00079_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00080 : สวัสดิการพนักงาน */
//		ExpenseTypeScreen expenseE00080_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00081 : จ่ายเงินกรณีซื้อทรัพย์สิน */
//		ExpenseTypeScreen expenseE00081_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00082 : ค่าใช้จ่ายจัดหาอ้อย-สนับสนุนแผนและนโยบาย */
//		ExpenseTypeScreen expenseE00082_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00083 : ค่าใช้จ่ายจัดหาอ้อย-ส่วนต่างจัดหาอ้อยและบริหาร */
//		ExpenseTypeScreen expenseE00083_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00084 : ค่าใช้จ่ายจัดหาอ้อย-ส่งเสริมและปกป้องอ้อย */
//		ExpenseTypeScreen expenseE00084_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00085 : ค่าใช้จ่ายจัดหาอ้อย-ชุมชนและชาวไร */
//		ExpenseTypeScreen expenseE00085_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);

	/* E00086 : ค่าใช้จ่ายจัดหาอ้อย-สนับสนุนคุณภาพอ้อย */
//		ExpenseTypeScreen expenseE00086_Screen1 = new ExpenseTypeScreen("ถึงเวลา", "Time To", "String", "time2", true,expenseE00036);         expenseTypeScreenRepository.save(expenseE00036_Screen5);



			/* Setup Reimburse*/

		/* E00001 : ค่าน้ำมัน(ตามสิทธ์ ที่นอกเหนือจาก feed card) */
		Reimburse expenseE00001_Reimburse1 = new Reimburse("USER",  expenseE00001);         reimburseRepository.save(expenseE00001_Reimburse1);

		/* E00002 : ค่าน้ำมันดีเซล ตามสิทธิ์ (ลิตร) */
		Reimburse expenseE00002_Reimburse2 = new Reimburse("USER",  expenseE00002);         reimburseRepository.save(expenseE00002_Reimburse2);

		/* E00003 : ค่าน้ำมันเบนซิน ตามสิทธิ์ (ลิตร) */
		Reimburse expenseE00003_Reimburse1 = new Reimburse("USER",  expenseE00003);         reimburseRepository.save(expenseE00003_Reimburse1);

		/* E00004 : ค่าสมาชิกหนังสือและวารสาร */
		Reimburse expenseE00004_Reimburse1 = new Reimburse("USER",  expenseE00004);         reimburseRepository.save(expenseE00004_Reimburse1);

		/* E00005 : ค่าตำราวิชาการ */
		Reimburse expenseE00005_Reimburse1 = new Reimburse("USER",  expenseE00005);         reimburseRepository.save(expenseE00005_Reimburse1);

		/* E00006 : เงินบริจาค */
		Reimburse expenseE00006_Reimburse1 = new Reimburse("USER",  expenseE00006);         reimburseRepository.save(expenseE00006_Reimburse1);

		/* E00007 : การบริจาค ที่ถือเป็นรายจ่ายไม่ได้ */
		Reimburse expenseE00007_Reimburse1 = new Reimburse("USER",  expenseE00007);         reimburseRepository.save(expenseE00007_Reimburse1);

		/* E00008 : เงินช่วยเหลือพนักงาน */
		Reimburse expenseE00008_Reimburse1 = new Reimburse("HR",    expenseE00008);         reimburseRepository.save(expenseE00008_Reimburse1);

		/* E00009 : ค่าซ่อมรถยนต์ */
		Reimburse expenseE00009_Reimburse1 = new Reimburse("OFFICE_ADMIN",   expenseE00009);         reimburseRepository.save(expenseE00009_Reimburse1);

		/* E00010 : ภาษีโรงเรือน */
		Reimburse expenseE000010_Reimburse1 = new Reimburse("USER",  expenseE00010);         reimburseRepository.save(expenseE000010_Reimburse1);

		/* E00011 : ภาษีบำรุงท้องที่ */
		Reimburse expenseE000011_Reimburse1 = new Reimburse("USER",  expenseE00011);         reimburseRepository.save(expenseE000011_Reimburse1);

		/* E00012 : ภาษีป้าย */
		Reimburse expenseE000012_Reimburse1 = new Reimburse("USER",  expenseE00012);         reimburseRepository.save(expenseE000012_Reimburse1);

		/* E00013 : ค่าภาษีอื่นๆ นอกจากข้างต้น */
		Reimburse expenseE000013_Reimburse1 = new Reimburse("USER",  expenseE00013);         reimburseRepository.save(expenseE000013_Reimburse1);

		/* E00014 : ค่าประกันภัยรถยนต์ */
		Reimburse expenseE000014_Reimburse1 = new Reimburse("USER",  expenseE00014);         reimburseRepository.save(expenseE000014_Reimburse1);

		/* E00015 : ค่าต่อทะเบียนรถยนต์ */
		Reimburse expenseE000015_Reimburse1 = new Reimburse("USER",  expenseE00015);         reimburseRepository.save(expenseE000015_Reimburse1);

		/* E00016 : ค่าโทรศัพท์ */
		Reimburse expenseE000016_Reimburse1 = new Reimburse("USER",  expenseE00016);         reimburseRepository.save(expenseE000016_Reimburse1);

		/* E00017 : ค่าคัดหนังสือรับรอง */
		Reimburse expenseE000017_Reimburse1 = new Reimburse("USER",  expenseE00017);         reimburseRepository.save(expenseE000017_Reimburse1);

		/* E00018 : ค่าใบอนุญาต */
		Reimburse expenseE000018_Reimburse1 = new Reimburse("USER",  expenseE00018);         reimburseRepository.save(expenseE000018_Reimburse1);

		/* E00019 : ค่าส่งเอกสาร */
		Reimburse expenseE000019_Reimburse1 = new Reimburse("USER",  expenseE00019);         reimburseRepository.save(expenseE000019_Reimburse1);

		/* E00020 : ค่าส่งไปรษณีย์ */
		Reimburse expenseE000020_Reimburse1 = new Reimburse("USER",  expenseE00020);         reimburseRepository.save(expenseE000020_Reimburse1);

		/* E00021 : ค่าน้ำประปา ค่าไฟฟ้า */
		Reimburse expenseE000021_Reimburse1 = new Reimburse("USER",  expenseE00021);         reimburseRepository.save(expenseE000021_Reimburse1);

		/* E00022 : ค่าไฟฟ้าเขต */
		Reimburse expenseE000022_Reimburse1 = new Reimburse("USER",  expenseE00022);         reimburseRepository.save(expenseE000022_Reimburse1);

		/* E00023 : ค่าอากรแสตมป์ */
		Reimburse expenseE000023_Reimburse1 = new Reimburse("USER",  expenseE00023);         reimburseRepository.save(expenseE000023_Reimburse1);

		/* E00024 : ค่ารับรองบุคคลภายนอก(ค่าอาหาร) */
		Reimburse expenseE000024_Reimburse1 = new Reimburse("USER",  expenseE00024);         reimburseRepository.save(expenseE000024_Reimburse1);

		/* E00025 : ค่ารับรองบุคคลภายนอก(ค่าของขวัญ) */
		Reimburse expenseE000025_Reimburse1 = new Reimburse("USER",  expenseE00025);         reimburseRepository.save(expenseE000025_Reimburse1);

		/* E00026 : ค่ารับรองบุคคลภายนอก อื่นๆ */
		Reimburse expenseE000026_Reimburse1 = new Reimburse("USER",  expenseE00026);         reimburseRepository.save(expenseE000026_Reimburse1);

		/* E00027 : ค่าใช้จ่ายที่ไม่มีใบเสร็จ */
		Reimburse expenseE000027_Reimburse1 = new Reimburse("USER",  expenseE00027);         reimburseRepository.save(expenseE000027_Reimburse1);

		/* E00028 : ค่าธรรมเนียมธนาคารเพื่อการดำเนินงาน */
		Reimburse expenseE000028_Reimburse1 = new Reimburse("USER",  expenseE00028);         reimburseRepository.save(expenseE000028_Reimburse1);

		/* E00029 : ค่าจัดกิจกรรมภายใน */
		Reimburse expenseE000029_Reimburse1 = new Reimburse("USER",  expenseE00029);         reimburseRepository.save(expenseE000029_Reimburse1);

			/* E00030 : ค่าเบี้ยเลี้ยงตำรวจ */
		Reimburse expenseE000030_Reimburse1 = new Reimburse("USER",  expenseE00030);         reimburseRepository.save(expenseE000030_Reimburse1);

		/* E00031 : วัสดุสิ้นเปลือง */
		Reimburse expenseE000031_Reimburse1 = new Reimburse("USER",  expenseE00031);         reimburseRepository.save(expenseE000031_Reimburse1);

		/* E00032 : ค่าธรรมเนียม ฟ้องดำเนินคดี */
		Reimburse expenseE000032_Reimburse1 = new Reimburse("USER",  expenseE00032);         reimburseRepository.save(expenseE000032_Reimburse1);

		/* E00033 : จ่ายค่าธรรมเนียม ให้กับหน่วยงานราชการี */
		Reimburse expenseE000033_Reimburse1 = new Reimburse("USER",  expenseE00033);         reimburseRepository.save(expenseE000033_Reimburse1);

		/* E00034 : ค่า Taxi ปฏิบัติงานระหว่างวัน */
		Reimburse expenseE000034_Reimburse1 = new Reimburse("USER",  expenseE00034);         reimburseRepository.save(expenseE000034_Reimburse1);

		/* E00035 : ค่า Taxiในประเทศ ก่อน 7.00  หรือ หลังเวลา 20.00 */
		Reimburse expenseE000035_Reimburse1 = new Reimburse("USER",  expenseE00035);         reimburseRepository.save(expenseE000035_Reimburse1);

			/* E00036 : ค่า Taxiในประเทศ */
		Reimburse expenseE000036_Reimburse1 = new Reimburse("USER",  expenseE00036);         reimburseRepository.save(expenseE000036_Reimburse1);

		/* E00037 : ค่าที่พักในประเทศ */
		Reimburse expenseE000037_Reimburse1 = new Reimburse("USER",  expenseE00037);         reimburseRepository.save(expenseE000037_Reimburse1);

			/* E00038 : ค่าน้ำมัน(รถเช่า) */
		Reimburse expenseE000038_Reimburse1 = new Reimburse("USER",  expenseE00038);         reimburseRepository.save(expenseE000038_Reimburse1);

			/* E00039 : ค่าเบี้ยเลี้ยงเดินทางในประเทศ */
		Reimburse expenseE000039_Reimburse1 = new Reimburse("USER",  expenseE00039);         reimburseRepository.save(expenseE000039_Reimburse1);

		/* E00040 : ค่าทางด่วน */
		Reimburse expenseE000040_Reimburse1 = new Reimburse("USER",  expenseE00040);         reimburseRepository.save(expenseE000040_Reimburse1);

		/* E00041 : ค่าที่จอดรถในประเทศ */
		Reimburse expenseE000041_Reimburse1 = new Reimburse("USER",  expenseE00041);         reimburseRepository.save(expenseE000041_Reimburse1);

		/* E00042 : ค่าทางด่วน, ค่าบัตรผ่านทาง Easy Pass (ปฏิบัติงานระหว่างวัน) */
		Reimburse expenseE000042_Reimburse1 = new Reimburse("USER",  expenseE00042);         reimburseRepository.save(expenseE000042_Reimburse1);

		/* E00043 : ค่าที่จอดรถในประเทศ (ปฏิบัติงานระหว่างวัน) */
		Reimburse expenseE000043_Reimburse1 = new Reimburse("USER",  expenseE00043);         reimburseRepository.save(expenseE000043_Reimburse1);

		 /* E00044 : ค่าตั๋วเครื่องบินต่างประเทศ */
		Reimburse expenseE000044_Reimburse1 = new Reimburse("USER",  expenseE00044);         reimburseRepository.save(expenseE000044_Reimburse1);

		 /* E00045 : ค่าเบี้ยเลี้ยงเดินทางต่างประเทศ */
		Reimburse expenseE000045_Reimburse1 = new Reimburse("USER",  expenseE00045);         reimburseRepository.save(expenseE000045_Reimburse1);

			/* E00046 : ค่าที่พักต่างประเทศ */
		Reimburse expenseE000046_Reimburse1 = new Reimburse("USER",  expenseE00046);         reimburseRepository.save(expenseE000046_Reimburse1);

		  /* E00047 : ค่ารถเดินทางไปกลับสนามบิน-ที่พัก */
		Reimburse expenseE000047_Reimburse1 = new Reimburse("USER",  expenseE00047);         reimburseRepository.save(expenseE000047_Reimburse1);

		/* E00048 : ค่าที่จอดรถต่างประเทศ */
		Reimburse expenseE000048_Reimburse1 = new Reimburse("USER",  expenseE00048);         reimburseRepository.save(expenseE000048_Reimburse1);

		 /* E00049 : ่่ค่า Taxi ต่างประเทศ */
		Reimburse expenseE000049_Reimburse1 = new Reimburse("USER",  expenseE00049);         reimburseRepository.save(expenseE000049_Reimburse1);

		/* E00050 : ค่าวัคซีนเดินทางต่างประเทศ */
		Reimburse expenseE000050_Reimburse1 = new Reimburse("USER",  expenseE00050);         reimburseRepository.save(expenseE000050_Reimburse1);

		/* E00051 : ค่าใช้จ่ายสื่อสารตปท */
		Reimburse expenseE000051_Reimburse1 = new Reimburse("USER",  expenseE00051);         reimburseRepository.save(expenseE000051_Reimburse1);

		/* E00052 : ค่าอาหารต่างประเทศ */
		Reimburse expenseE000052_Reimburse1 = new Reimburse("USER",  expenseE00052);         reimburseRepository.save(expenseE000052_Reimburse1);

			/* E00053 : ทำ VISA */
		Reimburse expenseE000053_Reimburse1 = new Reimburse("USER",  expenseE00053);         reimburseRepository.save(expenseE000053_Reimburse1);

		/* E00054 : ค่าอบรมภายนอก */
		Reimburse expenseE000054_Reimburse1 = new Reimburse("USER",  expenseE00054);         reimburseRepository.save(expenseE000054_Reimburse1);

		/* E00055 : ค่าใช้จ่ายในการฝึกอบรมต่างประเทศ */
		Reimburse expenseE000055_Reimburse1 = new Reimburse("USER",  expenseE00055);         reimburseRepository.save(expenseE000055_Reimburse1);

		/* E00056 : เงินช่วยเหลือน้ำมันกรณีนำรถยนต์ส่วนตัวใช้ปฏิบัติงาน */
		Reimburse expenseE000056_Reimburse1 = new Reimburse("USER",  expenseE00056);         reimburseRepository.save(expenseE000056_Reimburse1);

		/* E00057 : ค่าใช้จ่ายจัดประชุม */
		Reimburse expenseE000057_Reimburse1 = new Reimburse("USER",  expenseE00057);         reimburseRepository.save(expenseE000057_Reimburse1);

		/* E0005 : ค่าอาหารกรรมการและพนักงาน */
		Reimburse expenseE000058_Reimburse1 = new Reimburse("USER",  expenseE00058);         reimburseRepository.save(expenseE000058_Reimburse1);

		/* E00059 : ค่าซ่อมแซมเครื่องจักรและอุปกรณ์ */
		Reimburse expenseE000059_Reimburse1 = new Reimburse("USER",  expenseE00059);         reimburseRepository.save(expenseE000059_Reimburse1);

		/* E00060 : ค่าจัดกิจกรรมภายนอก */
		Reimburse expenseE000060_Reimburse1 = new Reimburse("USER",  expenseE00060);         reimburseRepository.save(expenseE000060_Reimburse1);

		/* E00061 : ค่าตัวอย่างน้ำตาล */
		Reimburse expenseE000061_Reimburse1 = new Reimburse("USER",  expenseE00061);         reimburseRepository.save(expenseE000061_Reimburse1);

		/* E00062 : ค่าจัดกิจกรรมส่งเสริมการขาย */
		Reimburse expenseE000062_Reimburse1 = new Reimburse("USER",  expenseE00062);         reimburseRepository.save(expenseE000062_Reimburse1);

		/* E00063 : ค่าของรางวัลกิจกรรมการตลาด */
		Reimburse expenseE000063_Reimburse1 = new Reimburse("USER",  expenseE00063);         reimburseRepository.save(expenseE000063_Reimburse1);

		/* E00064 : ค่ากระดาษและสำเนาเอกสาร */
		Reimburse expenseE000064_Reimburse1 = new Reimburse("USER",  expenseE00064);         reimburseRepository.save(expenseE000064_Reimburse1);

		/* E00065 : ค่ากระดาษและสำเนาเอกสาร */
		Reimburse expenseE000065_Reimburse1 = new Reimburse("USER",  expenseE00065);         reimburseRepository.save(expenseE000065_Reimburse1);

		/* E00066 : ค่าซ่อมแซมอาคาร */
		Reimburse expenseE000066_Reimburse1 = new Reimburse("USER",  expenseE00066);         reimburseRepository.save(expenseE000066_Reimburse1);

		/* E00067 : ค่าซ่อมแซมอุปกรณ์ */
		Reimburse expenseE000067_Reimburse1 = new Reimburse("USER",  expenseE00067);         reimburseRepository.save(expenseE000067_Reimburse1);

		/* E00068 : รักษาสภาพแวดล้อม */
		Reimburse expenseE000068_Reimburse1 = new Reimburse("USER",  expenseE00068);         reimburseRepository.save(expenseE000068_Reimburse1);

		/* E00069 : ค่าวิเคราะห์สิ่งแวดล้อม */
		Reimburse expenseE000069_Reimburse1 = new Reimburse("USER",  expenseE00069);         reimburseRepository.save(expenseE000069_Reimburse1);

		/* E00070 : ค่าใช้จ่ายในการรักษาสิ่งแวดล้อม */
		Reimburse expenseE000070_Reimburse1 = new Reimburse("USER",  expenseE00070);         reimburseRepository.save(expenseE000070_Reimburse1);

		/* E00071 : ค่าใช้จ่ายโครงการISO9002 */
		Reimburse expenseE000071_Reimburse1 = new Reimburse("USER",  expenseE00071);         reimburseRepository.save(expenseE000071_Reimburse1);

		/* E00072 : ค่าใช้จ่ายโครงการISO5001 */
		Reimburse expenseE000072_Reimburse1 = new Reimburse("USER",  expenseE00072);         reimburseRepository.save(expenseE000072_Reimburse1);

		/* E00073 : ค่าใบรับรองคุณภาพ */
		Reimburse expenseE000073_Reimburse1 = new Reimburse("USER",  expenseE00073);         reimburseRepository.save(expenseE000073_Reimburse1);

		/* E00074 : ค่าวิเคราะห์คุณภาพสินค้า */
		Reimburse expenseE000074_Reimburse1 = new Reimburse("USER",  expenseE00074);         reimburseRepository.save(expenseE000074_Reimburse1);

		/* E00075 : ค่าจัดกิจกรรมโครงการเพื่อพัฒนาระบบงาน */
		Reimburse expenseE000075_Reimburse1 = new Reimburse("USER",  expenseE00075);         reimburseRepository.save(expenseE000075_Reimburse1);

		/* E00076 : ค่าบำบัดสิ่งตกค้าง */
		Reimburse expenseE000076_Reimburse1 = new Reimburse("USER",  expenseE00076);         reimburseRepository.save(expenseE000076_Reimburse1);

		/* E00077 : ค่าส่งเครื่องมือและอุปกรณ์ไปสอบเทียบ */
		Reimburse expenseE000077_Reimburse1 = new Reimburse("USER",  expenseE00077);         reimburseRepository.save(expenseE000077_Reimburse1);

		/* E00078 : ค่าตรายาง */
		Reimburse expenseE000078_Reimburse1 = new Reimburse("USER",  expenseE00078);         reimburseRepository.save(expenseE000078_Reimburse1);

		/* E00079 : เครื่องตกแต่งสำนักงาน */
		Reimburse expenseE000079_Reimburse1 = new Reimburse("USER",  expenseE00079);         reimburseRepository.save(expenseE000079_Reimburse1);

		/* E00080 : สวัสดิการพนักงาน */
		Reimburse expenseE000080_Reimburse1 = new Reimburse("OFFICE_ADMIN",  expenseE00080);      reimburseRepository.save(expenseE000080_Reimburse1);

		/* E00081 : จ่ายเงินกรณีซื้อทรัพย์สิน */
		Reimburse expenseE000081_Reimburse1 = new Reimburse("USER",  expenseE00081);         reimburseRepository.save(expenseE000081_Reimburse1);

		/* E00082 : ค่าใช้จ่ายจัดหาอ้อย-สนับสนุนแผนและนโยบาย */
		Reimburse expenseE000082_Reimburse1 = new Reimburse("USER",  expenseE00082);         reimburseRepository.save(expenseE000082_Reimburse1);

		/* E00083 : ค่าใช้จ่ายจัดหาอ้อย-ส่วนต่างจัดหาอ้อยและบริหาร */
		Reimburse expenseE000083_Reimburse1 = new Reimburse("USER",  expenseE00083);         reimburseRepository.save(expenseE000083_Reimburse1);

		/* E00084 : ค่าใช้จ่ายจัดหาอ้อย-ส่งเสริมและปกป้องอ้อย */
		Reimburse expenseE000084_Reimburse1 = new Reimburse("USER",  expenseE00084);         reimburseRepository.save(expenseE000084_Reimburse1);

		/* E00085 : ค่าใช้จ่ายจัดหาอ้อย-ชุมชนและชาวไร */
		Reimburse expenseE000085_Reimburse1 = new Reimburse("USER",  expenseE00085);         reimburseRepository.save(expenseE000085_Reimburse1);

		/* E00086 : ค่าใช้จ่ายจัดหาอ้อย-สนับสนุนคุณภาพอ้อย */
		Reimburse expenseE000086_Reimburse1 = new Reimburse("USER",  expenseE00086);         reimburseRepository.save(expenseE000086_Reimburse1);








			/* Setup ApproveReference*/

		/* E00001 : ค่าน้ำมัน(ตามสิทธ์ ที่นอกเหนือจาก feed card) */
//		ExpenseTypeReference expenseE000001_Reference1 = new ExpenseTypeReference("","",expenseE00001); expenseTypeReferenceRepository.save(expenseE000001_Reference1);

		/* E00002 : ค่าน้ำมันดีเซล ตามสิทธิ์ (ลิตร) */
//		ExpenseTypeReference expenseE000002_Reference1 = new ExpenseTypeReference("","",expenseE00002); expenseTypeReferenceRepository.save(expenseE000002_Reference1);

		/* E00003 : ค่าน้ำมันเบนซิน ตามสิทธิ์ (ลิตร) */
//		ExpenseTypeReference expenseE000003_Reference1 = new ExpenseTypeReference("","",expenseE00003); expenseTypeReferenceRepository.save(expenseE000003_Reference1);

		/* E00004 : ค่าสมาชิกหนังสือและวารสาร */
//		ExpenseTypeReference expenseE000004_Reference1 = new ExpenseTypeReference("","",expenseE00004); expenseTypeReferenceRepository.save(expenseE000004_Reference1);

		/* E00005 : ค่าตำราวิชาการ */
//		ExpenseTypeReference expenseE000005_Reference1 = new ExpenseTypeReference("","",expenseE00005); expenseTypeReferenceRepository.save(expenseE000005_Reference1);

		/* E00006 : เงินบริจาค */
//		ExpenseTypeReference expenseE000006_Reference1 = new ExpenseTypeReference("","",expenseE00006); expenseTypeReferenceRepository.save(expenseE000006_Reference1);

		/* E00007 : การบริจาค ที่ถือเป็นรายจ่ายไม่ได้ */
//		ExpenseTypeReference expenseE000007_Reference1 = new ExpenseTypeReference("","",expenseE00007); expenseTypeReferenceRepository.save(expenseE000007_Reference1);

		/* E00008 : เงินช่วยเหลือพนักงาน */
//		ExpenseTypeReference expenseE000008_Reference1 = new ExpenseTypeReference("","",expenseE00008); expenseTypeReferenceRepository.save(expenseE000008_Reference1);

		/* E00009 : ค่าซ่อมรถยนต์ */
//		ExpenseTypeReference expenseE000009_Reference1 = new ExpenseTypeReference("","",expenseE00009); expenseTypeReferenceRepository.save(expenseE000009_Reference1);

		/* E00010 : ภาษีโรงเรือน */
//		ExpenseTypeReference expenseE0000010_Reference1 = new ExpenseTypeReference("","",expenseE00010); expenseTypeReferenceRepository.save(expenseE0000010_Reference1);

		/* E00011 : ภาษีบำรุงท้องที่ */
//		ExpenseTypeReference expenseE0000011_Reference1 = new ExpenseTypeReference("","",expenseE00011); expenseTypeReferenceRepository.save(expenseE0000011_Reference1);

		/* E00012 : ภาษีป้าย */
//		ExpenseTypeReference expenseE0000012_Reference1 = new ExpenseTypeReference("","",expenseE00012); expenseTypeReferenceRepository.save(expenseE0000012_Reference1);

		/* E00013 : ค่าภาษีอื่นๆ นอกจากข้างต้น */
//		ExpenseTypeReference expenseE0000013_Reference1 = new ExpenseTypeReference("","",expenseE00013); expenseTypeReferenceRepository.save(expenseE0000013_Reference1);

		/* E00014 : ค่าประกันภัยรถยนต์ */
//		ExpenseTypeReference expenseE0000014_Reference1 = new ExpenseTypeReference("","",expenseE00014); expenseTypeReferenceRepository.save(expenseE0000014_Reference1);

		/* E00015 : ค่าต่อทะเบียนรถยนต์ */
//		ExpenseTypeReference expenseE0000015_Reference1 = new ExpenseTypeReference("","",expenseE00015); expenseTypeReferenceRepository.save(expenseE0000015_Reference1);

		/* E00016 : ค่าโทรศัพท์ */
//		ExpenseTypeReference expenseE0000016_Reference1 = new ExpenseTypeReference("","",expenseE00016); expenseTypeReferenceRepository.save(expenseE0000016_Reference1);

		/* E00017 : ค่าคัดหนังสือรับรอง */
//		ExpenseTypeReference expenseE0000017_Reference1 = new ExpenseTypeReference("","",expenseE00017); expenseTypeReferenceRepository.save(expenseE0000017_Reference1);

		/* E00018 : ค่าใบอนุญาต */
//		ExpenseTypeReference expenseE0000018_Reference1 = new ExpenseTypeReference("","",expenseE00018); expenseTypeReferenceRepository.save(expenseE0000018_Reference1);

			/* E00019 : ค่าส่งเอกสาร */
//		ExpenseTypeReference expenseE0000019_Reference1 = new ExpenseTypeReference("","",expenseE00019); expenseTypeReferenceRepository.save(expenseE0000019_Reference1);

		/* E00020 : ค่าส่งไปรษณีย์ */
//		ExpenseTypeReference expenseE0000020_Reference1 = new ExpenseTypeReference("","",expenseE00020); expenseTypeReferenceRepository.save(expenseE0000020_Reference1);

		/* E00021 : ค่าน้ำประปา ค่าไฟฟ้า */
//		ExpenseTypeReference expenseE0000021_Reference1 = new ExpenseTypeReference("","",expenseE00021); expenseTypeReferenceRepository.save(expenseE0000021_Reference1);

		/* E00022 : ค่าไฟฟ้าเขต */
//		ExpenseTypeReference expenseE0000022_Reference1 = new ExpenseTypeReference("","",expenseE00022); expenseTypeReferenceRepository.save(expenseE0000022_Reference1);

		/* E00023 : ค่าอากรแสตมป์ */
//		ExpenseTypeReference expenseE0000023_Reference1 = new ExpenseTypeReference("","",expenseE00023); expenseTypeReferenceRepository.save(expenseE0000023_Reference1);

		/* E00024 : ค่ารับรองบุคคลภายนอก(ค่าอาหาร) */
//		ExpenseTypeReference expenseE0000024_Reference1 = new ExpenseTypeReference("","",expenseE00024); expenseTypeReferenceRepository.save(expenseE0000024_Reference1);

		/* E00025 : ค่ารับรองบุคคลภายนอก(ค่าของขวัญ) */
//		ExpenseTypeReference expenseE0000025_Reference1 = new ExpenseTypeReference("","",expenseE00025); expenseTypeReferenceRepository.save(expenseE0000025_Reference1);

		/* E00026 : ค่ารับรองบุคคลภายนอก อื่นๆ */
//		ExpenseTypeReference expenseE0000026_Reference1 = new ExpenseTypeReference("","",expenseE00026); expenseTypeReferenceRepository.save(expenseE0000026_Reference1);

		/* E00027 : ค่าใช้จ่ายที่ไม่มีใบเสร็จ */
//		ExpenseTypeReference expenseE0000027_Reference1 = new ExpenseTypeReference("","",expenseE00027); expenseTypeReferenceRepository.save(expenseE0000027_Reference1);

		/* E00028 : ค่าธรรมเนียมธนาคารเพื่อการดำเนินงาน */
//		ExpenseTypeReference expenseE0000028_Reference1 = new ExpenseTypeReference("","",expenseE00028); expenseTypeReferenceRepository.save(expenseE0000028_Reference1);

		/* E00029 : ค่าจัดกิจกรรมภายใน */
//		ExpenseTypeReference expenseE0000029_Reference1 = new ExpenseTypeReference("","",expenseE00029); expenseTypeReferenceRepository.save(expenseE0000029_Reference1);

		/* E00030 : ค่าเบี้ยเลี้ยงตำรวจ */
//		ExpenseTypeReference expenseE0000030_Reference1 = new ExpenseTypeReference("","",expenseE00030); expenseTypeReferenceRepository.save(expenseE0000030_Reference1);

		/* E00031 : วัสดุสิ้นเปลือง */
//		ExpenseTypeReference expenseE0000031_Reference1 = new ExpenseTypeReference("","",expenseE00031); expenseTypeReferenceRepository.save(expenseE0000031_Reference1);

		/* E00032 : ค่าธรรมเนียม ฟ้องดำเนินคดี */
//		ExpenseTypeReference expenseE0000032_Reference1 = new ExpenseTypeReference("","",expenseE00032); expenseTypeReferenceRepository.save(expenseE0000032_Reference1);

		/* E00033 : จ่ายค่าธรรมเนียม ให้กับหน่วยงานราชการี */
//		ExpenseTypeReference expenseE0000033_Reference1 = new ExpenseTypeReference("","",expenseE00033); expenseTypeReferenceRepository.save(expenseE0000033_Reference1);

		/* E00034 : ค่า Taxi ปฏิบัติงานระหว่างวัน */
//		ExpenseTypeReference expenseE0000034_Reference1 = new ExpenseTypeReference("","",expenseE00034); expenseTypeReferenceRepository.save(expenseE0000034_Reference1);

		/* E00035 : ค่า Taxiในประเทศ ก่อน 7.00  หรือ หลังเวลา 20.00 */
//		ExpenseTypeReference expenseE0000035_Reference1 = new ExpenseTypeReference("","",expenseE00035); expenseTypeReferenceRepository.save(expenseE0000035_Reference1);

		/* E00036 : ค่า Taxiในประเทศ */
//		ExpenseTypeReference expenseE0000036_Reference1 = new ExpenseTypeReference("","",expenseE00036); expenseTypeReferenceRepository.save(expenseE0000036_Reference1);

		/* E00037 : ค่าที่พักในประเทศ */
		ExpenseTypeReference expenseE0000037_Reference1 = new ExpenseTypeReference("0001","",expenseE00037); expenseTypeReferenceRepository.save(expenseE0000037_Reference1);

		/* E00038 : ค่าน้ำมัน(รถเช่า) */
		ExpenseTypeReference expenseE0000038_Reference1 = new ExpenseTypeReference("0001","APP001",expenseE00038); expenseTypeReferenceRepository.save(expenseE0000038_Reference1);

		/* E00039 : ค่าเบี้ยเลี้ยงเดินทางในประเทศ */
		ExpenseTypeReference expenseE0000039_Reference1 = new ExpenseTypeReference("0001","",expenseE00039); expenseTypeReferenceRepository.save(expenseE0000039_Reference1);

		/* E00040 : ค่าทางด่วน */
		ExpenseTypeReference expenseE0000040_Reference1 = new ExpenseTypeReference("0001","",expenseE00040); expenseTypeReferenceRepository.save(expenseE0000040_Reference1);

		/* E00041 : ค่าที่จอดรถในประเทศ */
		ExpenseTypeReference expenseE0000041_Reference1 = new ExpenseTypeReference("0001","",expenseE00041); expenseTypeReferenceRepository.save(expenseE0000041_Reference1);

		/* E00042 : ค่าทางด่วน, ค่าบัตรผ่านทาง Easy Pass (ปฏิบัติงานระหว่างวัน) */
//		ExpenseTypeReference expenseE0000042_Reference1 = new ExpenseTypeReference("","",expenseE00042); expenseTypeReferenceRepository.save(expenseE0000042_Reference1);

		/* E00043 : ค่าที่จอดรถในประเทศ (ปฏิบัติงานระหว่างวัน) */
//		ExpenseTypeReference expenseE0000043_Reference1 = new ExpenseTypeReference("","",expenseE00043); expenseTypeReferenceRepository.save(expenseE0000043_Reference1);

		 /* E00044 : ค่าตั๋วเครื่องบินต่างประเทศ */
		ExpenseTypeReference expenseE0000044_Reference1 = new ExpenseTypeReference("0002","",expenseE00044); expenseTypeReferenceRepository.save(expenseE0000044_Reference1);

		/* E00045 : ค่าเบี้ยเลี้ยงเดินทางต่างประเทศ */
		ExpenseTypeReference expenseE0000045_Reference1 = new ExpenseTypeReference("0002","",expenseE00045); expenseTypeReferenceRepository.save(expenseE0000045_Reference1);

		/* E00046 : ค่าที่พักต่างประเทศ */
		ExpenseTypeReference expenseE0000046_Reference1 = new ExpenseTypeReference("0002","",expenseE00046); expenseTypeReferenceRepository.save(expenseE0000046_Reference1);

		 /* E00047 : ค่ารถเดินทางไปกลับสนามบิน-ที่พัก */
		ExpenseTypeReference expenseE0000047_Reference1 = new ExpenseTypeReference("0002","",expenseE00047); expenseTypeReferenceRepository.save(expenseE0000047_Reference1);

		/* E00048 : ค่าที่จอดรถต่างประเทศ */
		ExpenseTypeReference expenseE0000048_Reference1 = new ExpenseTypeReference("0002","",expenseE00048); expenseTypeReferenceRepository.save(expenseE0000048_Reference1);

		 /* E00049 : ่่ค่า Taxi ต่างประเทศ */
		ExpenseTypeReference expenseE0000049_Reference1 = new ExpenseTypeReference("0002","",expenseE00049); expenseTypeReferenceRepository.save(expenseE0000049_Reference1);

		/* E00050 : ค่าวัคซีนเดินทางต่างประเทศ */
		ExpenseTypeReference expenseE0000050_Reference1 = new ExpenseTypeReference("0002","",expenseE00050); expenseTypeReferenceRepository.save(expenseE0000050_Reference1);

		/* E00051 : ค่าใช้จ่ายสื่อสารตปท */
		ExpenseTypeReference expenseE0000051_Reference1 = new ExpenseTypeReference("0002","",expenseE00051); expenseTypeReferenceRepository.save(expenseE0000051_Reference1);

		/* E00052 : ค่าอาหารต่างประเทศ */
		ExpenseTypeReference expenseE0000052_Reference1 = new ExpenseTypeReference("0002","",expenseE00052); expenseTypeReferenceRepository.save(expenseE0000052_Reference1);

		/* E00053 : ทำ VISA */
		ExpenseTypeReference expenseE0000053_Reference1 = new ExpenseTypeReference("0002","",expenseE00053); expenseTypeReferenceRepository.save(expenseE0000053_Reference1);

		/* E00054 : ค่าอบรมภายนอก */
//		ExpenseTypeReference expenseE0000054_Reference1 = new ExpenseTypeReference("","",expenseE00054); expenseTypeReferenceRepository.save(expenseE0000054_Reference1);

		/* E00055 : ค่าใช้จ่ายในการฝึกอบรมต่างประเทศ */
//		ExpenseTypeReference expenseE0000055_Reference1 = new ExpenseTypeReference("","",expenseE00055); expenseTypeReferenceRepository.save(expenseE0000055_Reference1);

		/* E00056 : เงินช่วยเหลือน้ำมันกรณีนำรถยนต์ส่วนตัวใช้ปฏิบัติงาน */
		ExpenseTypeReference expenseE0000056_Reference1 = new ExpenseTypeReference("0003","APP001",expenseE00056); expenseTypeReferenceRepository.save(expenseE0000056_Reference1);

		/* E00057 : ค่าใช้จ่ายจัดประชุม */
//		ExpenseTypeReference expenseE0000057_Reference1 = new ExpenseTypeReference("","",expenseE00057); expenseTypeReferenceRepository.save(expenseE0000057_Reference1);

		/* E00058 : ค่าอาหารกรรมการและพนักงาน */
//		ExpenseTypeReference expenseE0000058_Reference1 = new ExpenseTypeReference("","",expenseE00058); expenseTypeReferenceRepository.save(expenseE0000058_Reference1);

		/* E00059 : ค่าซ่อมแซมเครื่องจักรและอุปกรณ์ */
//		ExpenseTypeReference expenseE0000059_Reference1 = new ExpenseTypeReference("","",expenseE00059); expenseTypeReferenceRepository.save(expenseE0000059_Reference1);

		/* E00060 : ค่าจัดกิจกรรมภายนอก */
//		ExpenseTypeReference expenseE0000060_Reference1 = new ExpenseTypeReference("","",expenseE00060); expenseTypeReferenceRepository.save(expenseE0000060_Reference1);

		/* E00061 : ค่าตัวอย่างน้ำตาล */
//		ExpenseTypeReference expenseE0000061_Reference1 = new ExpenseTypeReference("","",expenseE00061); expenseTypeReferenceRepository.save(expenseE0000061_Reference1);

		/* E00062 : ค่าจัดกิจกรรมส่งเสริมการขาย */
//		ExpenseTypeReference expenseE0000062_Reference1 = new ExpenseTypeReference("","",expenseE00062); expenseTypeReferenceRepository.save(expenseE0000062_Reference1);

		/* E00063 : ค่าของรางวัลกิจกรรมการตลาด */
//		ExpenseTypeReference expenseE0000063_Reference1 = new ExpenseTypeReference("","",expenseE00063); expenseTypeReferenceRepository.save(expenseE0000063_Reference1);

		/* E00064 : ค่ากระดาษและสำเนาเอกสาร */
//		ExpenseTypeReference expenseE0000064_Reference1 = new ExpenseTypeReference("","",expenseE00064); expenseTypeReferenceRepository.save(expenseE0000064_Reference1);

		/* E00065 : ค่ากระดาษและสำเนาเอกสาร */
//		ExpenseTypeReference expenseE0000065_Reference1 = new ExpenseTypeReference("","",expenseE00065); expenseTypeReferenceRepository.save(expenseE0000065_Reference1);

		/* E00066 : ค่าซ่อมแซมอาคาร */
//		ExpenseTypeReference expenseE0000066_Reference1 = new ExpenseTypeReference("","",expenseE00066); expenseTypeReferenceRepository.save(expenseE0000066_Reference1);

		/* E00067 : ค่าซ่อมแซมอุปกรณ์ */
//		ExpenseTypeReference expenseE0000067_Reference1 = new ExpenseTypeReference("","",expenseE00067); expenseTypeReferenceRepository.save(expenseE0000067_Reference1);

		/* E00068 : รักษาสภาพแวดล้อม */
//		ExpenseTypeReference expenseE0000068_Reference1 = new ExpenseTypeReference("","",expenseE00068); expenseTypeReferenceRepository.save(expenseE0000068_Reference1);

		/* E00069 : ค่าวิเคราะห์สิ่งแวดล้อม */
//		ExpenseTypeReference expenseE0000069_Reference1 = new ExpenseTypeReference("","",expenseE00069); expenseTypeReferenceRepository.save(expenseE0000069_Reference1);

		/* E00070 : ค่าใช้จ่ายในการรักษาสิ่งแวดล้อม */
//		ExpenseTypeReference expenseE0000070_Reference1 = new ExpenseTypeReference("","",expenseE00070); expenseTypeReferenceRepository.save(expenseE0000070_Reference1);

		/* E00071 : ค่าใช้จ่ายโครงการISO9002 */
//		ExpenseTypeReference expenseE0000071_Reference1 = new ExpenseTypeReference("","",expenseE00071); expenseTypeReferenceRepository.save(expenseE0000071_Reference1);

		/* E00072 : ค่าใช้จ่ายโครงการISO5001 */
//		ExpenseTypeReference expenseE0000072_Reference1 = new ExpenseTypeReference("","",expenseE00072); expenseTypeReferenceRepository.save(expenseE0000072_Reference1);

		/* E00073 : ค่าใบรับรองคุณภาพ */
//		ExpenseTypeReference expenseE0000073_Reference1 = new ExpenseTypeReference("","",expenseE00073); expenseTypeReferenceRepository.save(expenseE0000073_Reference1);

		/* E00074 : ค่าวิเคราะห์คุณภาพสินค้า */
//		ExpenseTypeReference expenseE0000074_Reference1 = new ExpenseTypeReference("","",expenseE00074); expenseTypeReferenceRepository.save(expenseE0000074_Reference1);

		/* E00075 : ค่าจัดกิจกรรมโครงการเพื่อพัฒนาระบบงาน */
//		ExpenseTypeReference expenseE0000075_Reference1 = new ExpenseTypeReference("","",expenseE00075); expenseTypeReferenceRepository.save(expenseE0000075_Reference1);

		/* E00076 : ค่าบำบัดสิ่งตกค้าง */
//		ExpenseTypeReference expenseE0000076_Reference1 = new ExpenseTypeReference("","",expenseE00076); expenseTypeReferenceRepository.save(expenseE0000076_Reference1);

		/* E00077 : ค่าส่งเครื่องมือและอุปกรณ์ไปสอบเทียบ */
//		ExpenseTypeReference expenseE0000077_Reference1 = new ExpenseTypeReference("","",expenseE00077); expenseTypeReferenceRepository.save(expenseE0000077_Reference1);

		/* E00078 : ค่าตรายาง */
//		ExpenseTypeReference expenseE0000078_Reference1 = new ExpenseTypeReference("","",expenseE00078); expenseTypeReferenceRepository.save(expenseE0000078_Reference1);

		/* E00079 : เครื่องตกแต่งสำนักงาน */
//		ExpenseTypeReference expenseE0000079_Reference1 = new ExpenseTypeReference("","",expenseE00079); expenseTypeReferenceRepository.save(expenseE0000079_Reference1);

		/* E00080 : สวัสดิการพนักงาน */
//		ExpenseTypeReference expenseE0000080_Reference1 = new ExpenseTypeReference("","",expenseE00080); expenseTypeReferenceRepository.save(expenseE0000080_Reference1);

		/* E00081 : จ่ายเงินกรณีซื้อทรัพย์สิน */
//		ExpenseTypeReference expenseE0000081_Reference1 = new ExpenseTypeReference("","",expenseE00081); expenseTypeReferenceRepository.save(expenseE0000081_Reference1);

		/* E00082 : ค่าใช้จ่ายจัดหาอ้อย-สนับสนุนแผนและนโยบาย */
//		ExpenseTypeReference expenseE0000082_Reference1 = new ExpenseTypeReference("","",expenseE00082); expenseTypeReferenceRepository.save(expenseE0000082_Reference1);

		/* E00083 : ค่าใช้จ่ายจัดหาอ้อย-ส่วนต่างจัดหาอ้อยและบริหาร */
//		ExpenseTypeReference expenseE0000083_Reference1 = new ExpenseTypeReference("","",expenseE00083); expenseTypeReferenceRepository.save(expenseE0000083_Reference1);

		/* E00084 : ค่าใช้จ่ายจัดหาอ้อย-ส่งเสริมและปกป้องอ้อย */
//		ExpenseTypeReference expenseE0000084_Reference1 = new ExpenseTypeReference("","",expenseE00084); expenseTypeReferenceRepository.save(expenseE0000084_Reference1);

		/* E00085 : ค่าใช้จ่ายจัดหาอ้อย-ชุมชนและชาวไร */
//		ExpenseTypeReference expenseE0000085_Reference1 = new ExpenseTypeReference("","",expenseE00085); expenseTypeReferenceRepository.save(expenseE0000085_Reference1);

		/* E00086 : ค่าใช้จ่ายจัดหาอ้อย-สนับสนุนคุณภาพอ้อย */
//		ExpenseTypeReference expenseE0000086_Reference1 = new ExpenseTypeReference("","",expenseE00086); expenseTypeReferenceRepository.save(expenseE0000086_Reference1);





		//		<================================================================ Setup File ================================================================>
		/* E00001 : ค่าน้ำมัน(ตามสิทธ์ ที่นอกเหนือจาก feed card) */
		ExpenseTypeFile expenseE00001_File1 = new ExpenseTypeFile("M151",  true,expenseE00001);         expenseTypeFileRepository.save(expenseE00001_File1);


		/* E00002 : ค่าน้ำมันดีเซล ตามสิทธิ์ (ลิตร) */
		ExpenseTypeFile expenseE00002_File1 = new ExpenseTypeFile("M151",  true,expenseE00002);         expenseTypeFileRepository.save(expenseE00002_File1);


		/* E00003 : ค่าน้ำมันเบนซิน ตามสิทธิ์ (ลิตร) */
		ExpenseTypeFile expenseE00003_File1 = new ExpenseTypeFile("M151",  true,expenseE00003);         expenseTypeFileRepository.save(expenseE00003_File1);


		/* E00004 : ค่าสมาชิกหนังสือและวารสาร */
		ExpenseTypeFile expenseE00004_File1 = new ExpenseTypeFile("M151",  true,expenseE00004);         expenseTypeFileRepository.save(expenseE00004_File1);



		/* E00005 : ค่าตำราวิชาการ */
		ExpenseTypeFile expenseE00005_File1 = new ExpenseTypeFile("M151",  true,expenseE00005);         expenseTypeFileRepository.save(expenseE00005_File1);


		/* E00006 : เงินบริจาค */
		ExpenseTypeFile expenseE00006_File1 = new ExpenseTypeFile("M151",  true,expenseE00006);         expenseTypeFileRepository.save(expenseE00006_File1);


		/* E00007 : การบริจาค ที่ถือเป็นรายจ่ายไม่ได้ */
//		ExpenseTypeFile expenseE00007_File1 = new ExpenseTypeFile("M151",  true,expenseE00007);         expenseTypeFileRepository.save(expenseE00007_File1);


		/* E00008 : เงินช่วยเหลือพนักงาน */
		ExpenseTypeFile expenseE00008_File1 = new ExpenseTypeFile("M151",  true,expenseE00008);         expenseTypeFileRepository.save(expenseE00008_File1);



		/* E00009 : ค่าซ่อมรถยนต์ */
		ExpenseTypeFile expenseE00009_File1 = new ExpenseTypeFile("M151",  true,expenseE00009);         expenseTypeFileRepository.save(expenseE00009_File1);
		ExpenseTypeFile expenseE00009_File2 = new ExpenseTypeFile("M157",  true,expenseE00009);         expenseTypeFileRepository.save(expenseE00009_File2);


		/* E00010 : ภาษีโรงเรือน */
		ExpenseTypeFile expenseE00010_File1 = new ExpenseTypeFile("M151",  true,expenseE00010);         expenseTypeFileRepository.save(expenseE00010_File1);


		/* E00011 : ภาษีบำรุงท้องที่ */
		ExpenseTypeFile expenseE00011_File1 = new ExpenseTypeFile("M151",  true,expenseE00011);         expenseTypeFileRepository.save(expenseE00011_File1);


		/* E00012 : ภาษีป้าย */
		ExpenseTypeFile expenseE00012_File1 = new ExpenseTypeFile("M151",  true,expenseE00012);         expenseTypeFileRepository.save(expenseE00012_File1);


		/* E00013 : ค่าภาษีอื่นๆ นอกจากข้างต้น */
		ExpenseTypeFile expenseE00013_File1 = new ExpenseTypeFile("M151",  true,expenseE00013);         expenseTypeFileRepository.save(expenseE00013_File1);



		/* E00014 : ค่าประกันภัยรถยนต์ */
		ExpenseTypeFile expenseE00014_File1 = new ExpenseTypeFile("M151",  true,expenseE00014);         expenseTypeFileRepository.save(expenseE00014_File1);



		/* E00015 : ค่าต่อทะเบียนรถยนต์ */
		ExpenseTypeFile expenseE00015_File1 = new ExpenseTypeFile("M151",  true,expenseE00015);         expenseTypeFileRepository.save(expenseE00015_File1);



		/* E00016 : ค่าโทรศัพท์ */
		ExpenseTypeFile expenseE00016_File1 = new ExpenseTypeFile("M151",  true,expenseE00016);         expenseTypeFileRepository.save(expenseE00016_File1);



		/* E00017 : ค่าคัดหนังสือรับรอง */
		ExpenseTypeFile expenseE00017_File1 = new ExpenseTypeFile("M151",  true,expenseE00017);         expenseTypeFileRepository.save(expenseE00017_File1);



		/* E00018 : ค่าใบอนุญาต */
		ExpenseTypeFile expenseE00018_File1 = new ExpenseTypeFile("M151",  true,expenseE00018);         expenseTypeFileRepository.save(expenseE00018_File1);



		/* E00019 : ค่าส่งเอกสาร */
		ExpenseTypeFile expenseE00019_File1 = new ExpenseTypeFile("M151",  true,expenseE00019);         expenseTypeFileRepository.save(expenseE00019_File1);



		/* E00020 : ค่าส่งไปรษณีย์ */
		ExpenseTypeFile expenseE00020_File1 = new ExpenseTypeFile("M151",  true,expenseE00020);         expenseTypeFileRepository.save(expenseE00020_File1);



		/* E00021 : ค่าน้ำประปา ค่าไฟฟ้า */
		ExpenseTypeFile expenseE00021_File1 = new ExpenseTypeFile("M151",  true,expenseE00021);         expenseTypeFileRepository.save(expenseE00021_File1);


		/* E00022 : ค่าไฟฟ้าเขต */
		ExpenseTypeFile expenseE00022_File1 = new ExpenseTypeFile("M151",  true,expenseE00022);         expenseTypeFileRepository.save(expenseE00022_File1);


		/* E00023 : ค่าอากรแสตมป์ */
		ExpenseTypeFile expenseE00023_File1 = new ExpenseTypeFile("M151",  true,expenseE00023);         expenseTypeFileRepository.save(expenseE00023_File1);



		/* E00024 : ค่ารับรองบุคคลภายนอก(ค่าอาหาร) */
		ExpenseTypeFile expenseE00024_File1 = new ExpenseTypeFile("M151",  true,expenseE00024);         expenseTypeFileRepository.save(expenseE00024_File1);


		/* E00025 : ค่ารับรองบุคคลภายนอก(ค่าของขวัญ) */
		ExpenseTypeFile expenseE00025_File1 = new ExpenseTypeFile("M151",  true,expenseE00025);         expenseTypeFileRepository.save(expenseE00025_File1);



		/* E00026 : ค่ารับรองบุคคลภายนอก อื่นๆ */
		ExpenseTypeFile expenseE00026_File1 = new ExpenseTypeFile("M151",  true,expenseE00026);         expenseTypeFileRepository.save(expenseE00026_File1);



		/* E00027 : ค่าใช้จ่ายที่ไม่มีใบเสร็จ */
//		ExpenseTypeFile expenseE00027_File1 = new ExpenseTypeFile("M151",  true,expenseE00027);         expenseTypeFileRepository.save(expenseE00027_File1);


		/* E00028 : ค่าธรรมเนียมธนาคารเพื่อการดำเนินงาน */
		ExpenseTypeFile expenseE00028_File1 = new ExpenseTypeFile("M151",  true,expenseE00028);         expenseTypeFileRepository.save(expenseE00028_File1);



		/* E00029 : ค่าจัดกิจกรรมภายใน */
		ExpenseTypeFile expenseE00029_File1 = new ExpenseTypeFile("M151",  true,expenseE00029);         expenseTypeFileRepository.save(expenseE00029_File1);
		ExpenseTypeFile expenseE00029_File2 = new ExpenseTypeFile("M152",  true,expenseE00029);         expenseTypeFileRepository.save(expenseE00029_File2);


		/* E00030 : ค่าเบี้ยเลี้ยงตำรวจ */
		ExpenseTypeFile expenseE00030_File1 = new ExpenseTypeFile("M151",  true,expenseE00030);         expenseTypeFileRepository.save(expenseE00030_File1);


		/* E00031 : วัสดุสิ้นเปลือง */
		ExpenseTypeFile expenseE00031_File1 = new ExpenseTypeFile("M151",  true,expenseE00031);         expenseTypeFileRepository.save(expenseE00031_File1);


		/* E00032 : ค่าธรรมเนียม ฟ้องดำเนินคดี */
		ExpenseTypeFile expenseE00032_File1 = new ExpenseTypeFile("M151",  true,expenseE00032);         expenseTypeFileRepository.save(expenseE00032_File1);


		/* E00033 : จ่ายค่าธรรมเนียม ให้กับหน่วยงานราชการ */
		ExpenseTypeFile expenseE00033_File1 = new ExpenseTypeFile("M151",  true,expenseE00033);         expenseTypeFileRepository.save(expenseE00033_File1);



		/* E00034 : ค่า Taxi ปฏิบัติงานระหว่างวัน */
//		ExpenseTypeFile expenseE00034_File1 = new ExpenseTypeFile("M151",  true,expenseE00034);         expenseTypeFileRepository.save(expenseE00034_File1);



		/* E00035 : ค่า Taxiในประเทศ ก่อน 7.00  หรือ หลังเวลา 20.00 */
		ExpenseTypeFile expenseE00035_File1 = new ExpenseTypeFile("M158",  true,expenseE00035);         expenseTypeFileRepository.save(expenseE00035_File1);



		/* E00036 : ค่า Taxiในประเทศ */
		ExpenseTypeFile expenseE00036_File1 = new ExpenseTypeFile("M156",  false,expenseE00036);         expenseTypeFileRepository.save(expenseE00036_File1);



		/* E00037 : ค่าที่พักในประเทศ */
		ExpenseTypeFile expenseE00037_File1 = new ExpenseTypeFile("M151",  true,expenseE00037);         expenseTypeFileRepository.save(expenseE00037_File1);



		/* E00038 : ค่าน้ำมัน(รถเช่า) */
		ExpenseTypeFile expenseE00038_File1 = new ExpenseTypeFile("M151",  true,expenseE00038);         expenseTypeFileRepository.save(expenseE00038_File1);



		/* E00039 : ค่าเบี้ยเลี้ยงเดินทางในประเทศ */
//		ExpenseTypeFile expenseE00039_File1 = new ExpenseTypeFile("M151",  true,expenseE00039);         expenseTypeFileRepository.save(expenseE00039_File1);



		/* E00040 : ค่าทางด่วน */
		ExpenseTypeFile expenseE00040_File1 = new ExpenseTypeFile("M151",  true,expenseE00040);         expenseTypeFileRepository.save(expenseE00040_File1);


		/* E00041 : ค่าที่จอดรถในประเทศ */
		ExpenseTypeFile expenseE00041_File1 = new ExpenseTypeFile("M151",  true,expenseE00041);         expenseTypeFileRepository.save(expenseE00041_File1);


		/* E00042 : ค่าทางด่วน, ค่าบัตรผ่านทาง Easy Pass (ปฏิบัติงานระหว่างวัน) */
		ExpenseTypeFile expenseE00042_File1 = new ExpenseTypeFile("M151",  true,expenseE00042);         expenseTypeFileRepository.save(expenseE00042_File1);


		/* E00043 : ค่าที่จอดรถในประเทศ (ปฏิบัติงานระหว่างวัน) */
		ExpenseTypeFile expenseE00043_File1 = new ExpenseTypeFile("M151",  true,expenseE00043);         expenseTypeFileRepository.save(expenseE00043_File1);



        /* E00044 : ค่าตั๋วเครื่องบินต่างประเทศ */
		ExpenseTypeFile expenseE00044_File1 = new ExpenseTypeFile("M151",  true,expenseE00044);         expenseTypeFileRepository.save(expenseE00044_File1);


        /* E00045 : ค่าเบี้ยเลี้ยงเดินทางต่างประเทศ */
		ExpenseTypeFile expenseE00045_File1 = new ExpenseTypeFile("M156",  true,expenseE00045);         expenseTypeFileRepository.save(expenseE00045_File1);
		ExpenseTypeFile expenseE00045_File2 = new ExpenseTypeFile("M159",  true,expenseE00045);         expenseTypeFileRepository.save(expenseE00045_File2);


		/* E00046 : ค่าที่พักต่างประเทศ */
		ExpenseTypeFile expenseE00046_File1 = new ExpenseTypeFile("M151",  true,expenseE00046);         expenseTypeFileRepository.save(expenseE00046_File1);



       /* E00047 : ค่ารถเดินทางไปกลับสนามบิน-ที่พัก */
		ExpenseTypeFile expenseE00047_File1 = new ExpenseTypeFile("M151",  false,expenseE00047);         expenseTypeFileRepository.save(expenseE00047_File1);


		/* E00048 : ค่าที่จอดรถต่างประเทศ */
		ExpenseTypeFile expenseE00048_File1 = new ExpenseTypeFile("M151",  true,expenseE00048);         expenseTypeFileRepository.save(expenseE00048_File1);


		 /* E00049 : ่่ค่า Taxi ต่างประเทศ */
		ExpenseTypeFile expenseE00049_File1 = new ExpenseTypeFile("M151",  false,expenseE00049);         expenseTypeFileRepository.save(expenseE00049_File1);


		/* E00050 : ค่าวัคซีนเดินทางต่างประเทศ */
		ExpenseTypeFile expenseE00050_File1 = new ExpenseTypeFile("M151",  true,expenseE00050);         expenseTypeFileRepository.save(expenseE00050_File1);


		/* E00051 : ค่าใช้จ่ายสื่อสารตปท */
		ExpenseTypeFile expenseE00051_File1 = new ExpenseTypeFile("M151",  true,expenseE00051);         expenseTypeFileRepository.save(expenseE00051_File1);


		/* E00052 : ค่าอาหารต่างประเทศ */
		ExpenseTypeFile expenseE00052_File1 = new ExpenseTypeFile("M151",  true,expenseE00052);         expenseTypeFileRepository.save(expenseE00052_File1);


		/* E00053 : ทำ VISA */
		ExpenseTypeFile expenseE00053_File1 = new ExpenseTypeFile("M151",  true,expenseE00053);         expenseTypeFileRepository.save(expenseE00053_File1);


		/* E00054 : ค่าอบรมภายนอก */
		ExpenseTypeFile expenseE00054_File1 = new ExpenseTypeFile("M151",  true,expenseE00054);         expenseTypeFileRepository.save(expenseE00054_File1);
		ExpenseTypeFile expenseE00054_File2 = new ExpenseTypeFile("M155",  true,expenseE00054);         expenseTypeFileRepository.save(expenseE00054_File2);
		ExpenseTypeFile expenseE00054_File3 = new ExpenseTypeFile("M154",  true,expenseE00054);         expenseTypeFileRepository.save(expenseE00054_File3);


		/* E00055 : ค่าใช้จ่ายในการฝึกอบรมต่างประเทศ */
		ExpenseTypeFile expenseE00055_File1 = new ExpenseTypeFile("M151",  true,expenseE00055);         expenseTypeFileRepository.save(expenseE00055_File1);
		ExpenseTypeFile expenseE00055_File2 = new ExpenseTypeFile("M155",  true,expenseE00055);         expenseTypeFileRepository.save(expenseE00055_File2);
		ExpenseTypeFile expenseE00055_File3 = new ExpenseTypeFile("M154",  true,expenseE00055);         expenseTypeFileRepository.save(expenseE00055_File3);


		/* E00056 : เงินช่วยเหลือน้ำมันกรณีนำรถยนต์ส่วนตัวใช้ปฏิบัติงาน */
		ExpenseTypeFile expenseE00056_File1 = new ExpenseTypeFile("M151",  false,expenseE00056);         expenseTypeFileRepository.save(expenseE00056_File1);


		/* E00057 : ค่าใช้จ่ายจัดประชุม */
		ExpenseTypeFile expenseE00057_File1 = new ExpenseTypeFile("M151",  true,expenseE00057);         expenseTypeFileRepository.save(expenseE00057_File1);
		ExpenseTypeFile expenseE00057_File2 = new ExpenseTypeFile("M153",  true,expenseE00057);         expenseTypeFileRepository.save(expenseE00057_File2);


		/* E00058 : ค่าอาหารกรรมการและพนักงาน */
		ExpenseTypeFile expenseE00058_File1 = new ExpenseTypeFile("M151",  true,expenseE00058);         expenseTypeFileRepository.save(expenseE00058_File1);


		/* E00059 : ค่าซ่อมแซมเครื่องจักรและอุปกรณ์ */
		ExpenseTypeFile expenseE00059_File1 = new ExpenseTypeFile("M151",  true,expenseE00059);         expenseTypeFileRepository.save(expenseE00059_File1);

		/* E00060 : ค่าจัดกิจกรรมภายนอก */
		ExpenseTypeFile expenseE00060_File1 = new ExpenseTypeFile("M151",  true,expenseE00060);         expenseTypeFileRepository.save(expenseE00060_File1);
		ExpenseTypeFile expenseE00060_File2 = new ExpenseTypeFile("M152",  true,expenseE00060);         expenseTypeFileRepository.save(expenseE00060_File2);


		/* E00061 : ค่าตัวอย่างน้ำตาล */
		ExpenseTypeFile expenseE00061_File1 = new ExpenseTypeFile("M151",  true,expenseE00061);         expenseTypeFileRepository.save(expenseE00061_File1);


		/* E00062 : ค่าจัดกิจกรรมส่งเสริมการขาย */
		ExpenseTypeFile expenseE00062_File1 = new ExpenseTypeFile("M151",  true,expenseE00062);         expenseTypeFileRepository.save(expenseE00062_File1);


		/* E00063 : ค่าของรางวัลกิจกรรมการตลาด */
		ExpenseTypeFile expenseE00063_File1 = new ExpenseTypeFile("M151",  true,expenseE00063);         expenseTypeFileRepository.save(expenseE00063_File1);


		/* E00064 : ค่ากระดาษและสำเนาเอกสาร */
		ExpenseTypeFile expenseE00064_File1 = new ExpenseTypeFile("M151",  true,expenseE00064);         expenseTypeFileRepository.save(expenseE00064_File1);


		/* E00065 : ค่าใช้จ่ายเบ็ดเตล็ด มีใบเสร็จ */
		ExpenseTypeFile expenseE00065_File1 = new ExpenseTypeFile("M151",  true,expenseE00065);         expenseTypeFileRepository.save(expenseE00065_File1);


		/* E00066 : ค่าซ่อมแซมอาคาร */
		ExpenseTypeFile expenseE00066_File1 = new ExpenseTypeFile("M151",  true,expenseE00066);         expenseTypeFileRepository.save(expenseE00066_File1);


		/* E00067 : ค่าซ่อมแซมอุปกรณ์ */
		ExpenseTypeFile expenseE00067_File1 = new ExpenseTypeFile("M151",  true,expenseE00067);         expenseTypeFileRepository.save(expenseE00067_File1);


		/* E00068 : รักษาสภาพแวดล้อม */
		ExpenseTypeFile expenseE00068_File1 = new ExpenseTypeFile("M151",  true,expenseE00068);         expenseTypeFileRepository.save(expenseE00068_File1);


		/* E00069 : ค่าวิเคราะห์สิ่งแวดล้อม */
		ExpenseTypeFile expenseE00069_File1 = new ExpenseTypeFile("M151",  true,expenseE00069);         expenseTypeFileRepository.save(expenseE00069_File1);


		/* E00070 : ค่าใช้จ่ายในการรักษาสิ่งแวดล้อม */
		ExpenseTypeFile expenseE00070_File1 = new ExpenseTypeFile("M151",  true,expenseE00070);         expenseTypeFileRepository.save(expenseE00070_File1);


		/* E00071 : ค่าใช้จ่ายโครงการISO9002 */
		ExpenseTypeFile expenseE00071_File1 = new ExpenseTypeFile("M151",  true,expenseE00071);         expenseTypeFileRepository.save(expenseE00071_File1);

		/* E00072 : ค่าใช้จ่ายโครงการISO5001 */
		ExpenseTypeFile expenseE00072_File1 = new ExpenseTypeFile("M151",  true,expenseE00072);         expenseTypeFileRepository.save(expenseE00072_File1);

		/* E00073 : ค่าใบรับรองคุณภาพ */
		ExpenseTypeFile expenseE00073_File1 = new ExpenseTypeFile("M151",  true,expenseE00073);         expenseTypeFileRepository.save(expenseE00073_File1);

		/* E00074 : ค่าวิเคราะห์คุณภาพสินค้า */
		ExpenseTypeFile expenseE00074_File1 = new ExpenseTypeFile("M151",  true,expenseE00074);         expenseTypeFileRepository.save(expenseE00074_File1);

		/* E00075 : ค่าจัดกิจกรรมโครงการเพื่อพัฒนาระบบงาน */
		ExpenseTypeFile expenseE00075_File1 = new ExpenseTypeFile("M151",  true,expenseE00075);         expenseTypeFileRepository.save(expenseE00075_File1);


		/* E00076 : ค่าบำบัดสิ่งตกค้าง */
		ExpenseTypeFile expenseE00076_File1 = new ExpenseTypeFile("M151",  true,expenseE00076);         expenseTypeFileRepository.save(expenseE00076_File1);

		/* E00077 : ค่าส่งเครื่องมือและอุปกรณ์ไปสอบเทียบ */
		ExpenseTypeFile expenseE00077_File1 = new ExpenseTypeFile("M151",  true,expenseE00077);         expenseTypeFileRepository.save(expenseE00077_File1);

		/* E00078 : ค่าตรายาง */
		ExpenseTypeFile expenseE00078_File1 = new ExpenseTypeFile("M151",  true,expenseE00078);         expenseTypeFileRepository.save(expenseE00078_File1);

		/* E00079 : เครื่องตกแต่งสำนักงาน */
		ExpenseTypeFile expenseE00079_File1 = new ExpenseTypeFile("M151",  true,expenseE00079);         expenseTypeFileRepository.save(expenseE00079_File1);

		/* E00080 : สวัสดิการพนักงาน */
		ExpenseTypeFile expenseE00080_File1 = new ExpenseTypeFile("M151",  true,expenseE00080);         expenseTypeFileRepository.save(expenseE00080_File1);


		/* E00081 : จ่ายเงินกรณีซื้อทรัพย์สิน */
		ExpenseTypeFile expenseE00081_File1 = new ExpenseTypeFile("M151",  true,expenseE00081);         expenseTypeFileRepository.save(expenseE00081_File1);

		/* E00082 : ค่าใช้จ่ายจัดหาอ้อย-สนับสนุนแผนและนโยบาย */
		ExpenseTypeFile expenseE00082_File1 = new ExpenseTypeFile("M151",  true,expenseE00082);         expenseTypeFileRepository.save(expenseE00082_File1);

		/* E00083 : ค่าใช้จ่ายจัดหาอ้อย-ส่วนต่างจัดหาอ้อยและบริหาร */
		ExpenseTypeFile expenseE00083_File1 = new ExpenseTypeFile("M151",  true,expenseE00083);         expenseTypeFileRepository.save(expenseE00083_File1);

		/* E00084 : ค่าใช้จ่ายจัดหาอ้อย-ส่งเสริมและปกป้องอ้อย */
		ExpenseTypeFile expenseE00084_File1 = new ExpenseTypeFile("M151",  true,expenseE00084);         expenseTypeFileRepository.save(expenseE00084_File1);

		/* E00085 : ค่าใช้จ่ายจัดหาอ้อย-ชุมชนและชาวไร */
		ExpenseTypeFile expenseE00085_File1 = new ExpenseTypeFile("M151",  true,expenseE00085);         expenseTypeFileRepository.save(expenseE00085_File1);

		/* E00086 : ค่าใช้จ่ายจัดหาอ้อย-สนับสนุนคุณภาพอ้อย */
		ExpenseTypeFile expenseE00086_File1 = new ExpenseTypeFile("M151",  true,expenseE00086);         expenseTypeFileRepository.save(expenseE00086_File1);

		/* ExpenseTypeByCompany */
		List<ExpenseType> expenseTypeList = expenseTypeRepository.findAll();
		RestTemplate restTemplate = new RestTemplate();
		String resultPAString  = restTemplate.getForObject(OrgSysServer+"/profile/pa/%", String.class);
		Map mapPA = gson.fromJson(resultPAString, Map.class);
		List<Map<String,String>> paList = (List<Map<String, String>>) mapPA.get("restBodyList");
		
		String resultPSAString  = restTemplate.getForObject(OrgSysServer+"/profile/psa/%", String.class);
		Map mapPSA = gson.fromJson(resultPSAString, Map.class);
		List<Map<String,String>> psaList = (List<Map<String, String>>) mapPSA.get("restBodyList");
		//
			
		StringBuffer sb = new StringBuffer();
		for(ExpenseType expenseType:expenseTypeList){
			for(Map<String,String> paModel :paList){
				for(Map<String,String> psaModel :psaList){
					ExpenseTypeByCompany expenseCompany = null;
					if("0001".equals(psaModel.get("PsaCode")) && expenseType.getHeadOfficeGL() != null){
						expenseCompany = new ExpenseTypeByCompany(paModel.get("PaCode"),psaModel.get("PsaCode"),expenseType.getHeadOfficeGL(),expenseType);
					}else if(!"0001".equals(psaModel.get("PsaCode")) && expenseType.getFactoryGL() != null){
						expenseCompany = new ExpenseTypeByCompany(paModel.get("PaCode"),psaModel.get("PsaCode"),expenseType.getFactoryGL(),expenseType);
					}

					if(expenseCompany != null){
						expenseTypeByCompanyRepository.save(expenseCompany);
					}
				}
				
			}
		}
			
			
		
	}

}