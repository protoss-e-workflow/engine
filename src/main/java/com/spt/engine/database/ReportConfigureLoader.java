package com.spt.engine.database;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.spt.engine.entity.general.*;
import com.spt.engine.repository.general.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import com.spt.engine.entity.app.Company;
import com.spt.engine.entity.app.CostCenter;
import com.spt.engine.entity.app.Department;
import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentApprove;
import com.spt.engine.entity.app.DocumentApproveItem;
import com.spt.engine.entity.app.Location;
import com.spt.engine.entity.app.TravelDetail;
import com.spt.engine.entity.general.LocaleMessage;
import com.spt.engine.entity.general.MasterData;
import com.spt.engine.entity.general.MasterDataDetail;
import com.spt.engine.entity.general.Menu;
import com.spt.engine.entity.general.Parameter;
import com.spt.engine.entity.general.ParameterDetail;
import com.spt.engine.entity.general.ReportConfiguration;
import com.spt.engine.entity.general.ReportCriteria;
import com.spt.engine.entity.general.ReportProjection;
import com.spt.engine.entity.general.Role;
import com.spt.engine.entity.general.RunningType;
import com.spt.engine.entity.general.User;
import com.spt.engine.repository.app.CompanyRepository;
import com.spt.engine.repository.app.CostCenterRepository;
import com.spt.engine.repository.app.DepartmentRepository;
import com.spt.engine.repository.app.DocumentRepository;
import com.spt.engine.repository.app.LocationRepository;
import com.spt.engine.repository.app.TravelDetailRepository;
import com.spt.engine.repository.general.LocaleMessageRepository;
import com.spt.engine.repository.general.MasterDataDetailRepository;
import com.spt.engine.repository.general.MasterDataRepository;
import com.spt.engine.repository.general.MenuRepository;
import com.spt.engine.repository.general.ParameterDetailRepository;
import com.spt.engine.repository.general.ParameterRepository;
import com.spt.engine.repository.general.ReportConfigurationRepository;
import com.spt.engine.repository.general.ReportCriteriaRepository;
import com.spt.engine.repository.general.ReportProjectionRepository;
import com.spt.engine.repository.general.RunningTypeRepository;
import com.spt.engine.repository.general.UserRepository;
import com.spt.engine.repository.app.CompanyRepository;
import com.spt.engine.repository.app.CostCenterRepository;
import com.spt.engine.repository.app.DepartmentRepository;


//@Component
public class ReportConfigureLoader implements CommandLineRunner {

	
	private final ReportConfigurationRepository reportConfigurationRepository;
	private final ReportProjectionRepository reportProjectionRepository;
	private final ReportCriteriaRepository reportCriteriaRepository;
	
	@Autowired
	public ReportConfigureLoader(ReportConfigurationRepository reportConfigurationRepository,
			ReportProjectionRepository reportProjectionRepository,
			ReportCriteriaRepository reportCriteriaRepository
			) {
		this.reportConfigurationRepository = reportConfigurationRepository;
		this.reportProjectionRepository = reportProjectionRepository;
		this.reportCriteriaRepository = reportCriteriaRepository;
	}

	@Override
	public void run(String... strings) throws Exception {
		
		/* Set Report SQL */
		ReportConfiguration report001 = new ReportConfiguration();
		report001.setCode("REP001");
		report001.setName("KPI Approve Report");
		report001.setNameMessageCode("LABEL_KPI_APPROVE_REPORT");
		report001.setKeyOfParamTemplate("reportName"); //For PDF Template
		report001.setFileName("REP001.pdf");
		report001.setSqlStatement("SELECT * FROM app_user  WHERE account_non_expired = :accountNonExpired and a_test = :aTest and c_date = :cDate ");
		report001.setNumberOfCriteria(1);
		report001.setNumberOfProjection(1);
		report001.setReportType("PDF");
		//report001.setTemplateFileName("//Users//se//Desktop//report1.jasper"); //For PDF Template
		
		ReportCriteria report001Criteria01 = new ReportCriteria();
		report001Criteria01.setVariableName("Account Non Expired");
		report001Criteria01.setVariableType("BOOLEAN");
		report001Criteria01.setVariableParamName("accountNonExpired");
		report001Criteria01.setVariableFlagRequire(false);
		report001Criteria01.setSequenceDisplay(1);
		report001Criteria01.setColumnDisplay(6);
		
		ReportCriteria report001Criteria02 = new ReportCriteria();
		report001Criteria02.setVariableName("A Test");
		report001Criteria02.setVariableType("INTEGER");
		report001Criteria02.setVariableParamName("aTest");
		report001Criteria02.setVariableFlagRequire(true);
		report001Criteria02.setSequenceDisplay(2);
		report001Criteria02.setColumnDisplay(6);
		report001Criteria02.setOffsetColumnDisplay(6);
		
		ReportCriteria report001Criteria03 = new ReportCriteria();
		report001Criteria03.setVariableName("B Test");
		report001Criteria03.setVariableType("FLOAT");
		report001Criteria03.setVariableParamName("bTest");
		report001Criteria03.setVariableFlagRequire(true);
		report001Criteria03.setSequenceDisplay(3);
		report001Criteria03.setColumnDisplay(6);
		
		ReportCriteria report001Criteria04 = new ReportCriteria();
		report001Criteria04.setVariableName("C Date");
		report001Criteria04.setVariableType("DATE");
		report001Criteria04.setVariableParamName("cDate");
		report001Criteria04.setVariableFlagRequire(true);
		report001Criteria04.setSequenceDisplay(4);
		report001Criteria04.setColumnDisplay(6);
		
		ReportCriteria report001Criteria05 = new ReportCriteria();
		report001Criteria05.setVariableName("Email");
		report001Criteria05.setVariableType("STRING");
		report001Criteria05.setVariableParamName("email");
		report001Criteria05.setVariableFlagRequire(true);
		report001Criteria05.setSequenceDisplay(5);
		report001Criteria05.setColumnDisplay(6);
		report001Criteria05.setOffsetColumnDisplay(6);
		

		report001.setCriterias(new HashSet<ReportCriteria>(){{
            add(report001Criteria01);
            add(report001Criteria02);
            add(report001Criteria03);
            add(report001Criteria04);
            add(report001Criteria05);
        }});
		
		ReportProjection report001Projection = new ReportProjection();
		report001Projection.setColumnIndex("0");
		report001Projection.setColumnHeaderName("First Name");
		//report001Projection.setColumnHeaderKey("HEADER1"); //For PDF Template
		report001Projection.setKeyData("FIRST_NAME");
		//report001Projection.setKeyTemplate("VALUE1"); //For PDF Template
		report001Projection.setColumnType("STRING");
		report001Projection.setCellAlign("CENTER");
		
		
		
		report001.setProjections(new HashSet<ReportProjection>(){{
            add(report001Projection);
        }});
		
		reportConfigurationRepository.save(report001);
      
		report001Criteria01.setReportConfiguration(report001);
		reportCriteriaRepository.save(report001Criteria01);

		report001Criteria02.setReportConfiguration(report001);
		reportCriteriaRepository.save(report001Criteria02);

		report001Criteria03.setReportConfiguration(report001);
		reportCriteriaRepository.save(report001Criteria03);
		
		report001Criteria04.setReportConfiguration(report001);
		reportCriteriaRepository.save(report001Criteria04);
		
		report001Criteria05.setReportConfiguration(report001);
		reportCriteriaRepository.save(report001Criteria05);
		
		
		report001Projection.setReportConfiguration(report001);
		reportProjectionRepository.save(report001Projection);
		
	}

}