package com.spt.engine.database;

import java.sql.Timestamp;
import java.util.Date;

import com.spt.engine.entity.app.*;
import com.spt.engine.entity.general.*;
import com.spt.engine.repository.app.*;
import com.spt.engine.repository.general.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import com.spt.engine.entity.general.RunningType;
import com.spt.engine.repository.general.LocaleMessageRepository;
import com.spt.engine.repository.general.MasterDataDetailRepository;
import com.spt.engine.repository.general.MasterDataRepository;
import com.spt.engine.repository.general.MenuRepository;
import com.spt.engine.repository.general.ParameterDetailRepository;
import com.spt.engine.repository.general.ParameterRepository;
import com.spt.engine.repository.general.ReportConfigurationRepository;
import com.spt.engine.repository.general.ReportCriteriaRepository;
import com.spt.engine.repository.general.ReportProjectionRepository;
import com.spt.engine.repository.general.RunningTypeRepository;
import com.spt.engine.repository.general.UserRepository;
import com.spt.engine.repository.app.CompanyRepository;
import com.spt.engine.repository.app.CostCenterRepository;
import com.spt.engine.repository.app.DepartmentRepository;
import com.spt.engine.repository.app.ConditionalIORepository;


//@Component
public class MasterDataLoader implements CommandLineRunner {

	private final RunningTypeRepository runningTypeRepository;
	private final CostCenterRepository costCenterRepository; 
	private final DepartmentRepository departmentRepository;
	private final CompanyRepository companyRepository;
	private final LocationRepository locationRepository;
	private final TravelDetailRepository travelDetailRepository;
	private final DocumentRepository documentRepository;
	private final EmployeeRepository employeeRepository;
	private final RouteDistanceProcessCalculateRepository routeDistanceProcessCalculateRepository;
	private final ExpenseTypeRepository expenseTypeRepository;
	private final ExpenseTypeByCompanyRepository expenseTypeByCompanyRepository;
	private final ConditionalIORepository conditionalIORepository;
	private final FlowConfigureRepository flowConfigureRepository;
	private final PetrolAllowancePerMonthHistoryRepository petrolAllowancePerMonthHistoryRepository;
	private final DieselAllowancePerMonthHistoryRepository dieselAllowancePerMonthHistoryRepository;
	private final MonthlyPhoneBillHistoryRepository monthlyPhoneBillHistoryRepository;
	private final EmployeeReplacementRepository employeeReplacementRepository;
	private final DelegateRepository delegateRepository;
	private final InternalOrderRepository internalOrderRepository;
	private final DieselAllowancePerMonthRepository dieselAllowancePerMonthRepository;
	private final PetrolAllowancePerMonthRepository petrolAllowancePerMonthRepository;
	private final AdvanceOutstandingRepository advanceOutstandingRepository;
	private final VatRepository vatRepository;
	private final WithholdingTaxRepository withholdingTaxRepository;
	private final WitholdngTaxCodeRepository witholdngTaxCodeRepository;
	private final CurrencyRateRepository currencyRateRepository;


	@Autowired
	public MasterDataLoader(UserRepository repository,
			ParameterRepository parameterRepository,
			ParameterDetailRepository parameterDetailRepository,
			MasterDataRepository masterDataRepository,
			MasterDataDetailRepository masterDataDetailRepository,
			MenuRepository menuRepository,
			LocaleMessageRepository localeMessageRepository,
			ReportConfigurationRepository reportConfigurationRepository,
			ReportProjectionRepository reportProjectionRepository,
			ReportCriteriaRepository reportCriteriaRepository,
			RunningTypeRepository runningTypeRepository,
			CostCenterRepository costCenterRepository,
			DepartmentRepository departmentRepository,
			CompanyRepository companyRepository,
			LocationRepository locationRepository,
			TravelDetailRepository travelDetailRepository,
			DocumentRepository documentRepository,
		  	EmployeeRepository employeeRepository,
		  	RouteDistanceProcessCalculateRepository routeDistanceProcessCalculateRepository,
			ExpenseTypeRepository expenseTypeRepository,
			ExpenseTypeByCompanyRepository expenseTypeByCompanyRepository,
			ConditionalIORepository conditionalIORepository,
			FlowConfigureRepository flowConfigureRepository,
			PetrolAllowancePerMonthHistoryRepository petrolAllowancePerMonthHistoryRepository,
            DieselAllowancePerMonthHistoryRepository dieselAllowancePerMonthHistoryRepository,
            MonthlyPhoneBillHistoryRepository monthlyPhoneBillHistoryRepository,
			EmployeeReplacementRepository employeeReplacementRepository,
			DelegateRepository delegateRepository,
			InternalOrderRepository internalOrderRepository,
			DieselAllowancePerMonthRepository dieselAllowancePerMonthRepository,
			PetrolAllowancePerMonthRepository petrolAllowancePerMonthRepository,
			AdvanceOutstandingRepository advanceOutstandingRepository,
			VatRepository vatRepository,
			WithholdingTaxRepository withholdingTaxRepository,
			WitholdngTaxCodeRepository witholdngTaxCodeRepository,
			CurrencyRateRepository currencyRateRepository
	) {
		this.runningTypeRepository = runningTypeRepository;
		this.costCenterRepository = costCenterRepository;
		this.departmentRepository = departmentRepository;
		this.companyRepository = companyRepository;
		this.locationRepository = locationRepository;
		this.travelDetailRepository = travelDetailRepository;
		this.documentRepository = documentRepository;
		this.employeeRepository = employeeRepository;
		this.routeDistanceProcessCalculateRepository = routeDistanceProcessCalculateRepository;
		this.expenseTypeRepository = expenseTypeRepository;
		this.expenseTypeByCompanyRepository = expenseTypeByCompanyRepository;
		this.conditionalIORepository =  conditionalIORepository;
		this.flowConfigureRepository = flowConfigureRepository;
		this.petrolAllowancePerMonthHistoryRepository =  petrolAllowancePerMonthHistoryRepository;
		this.dieselAllowancePerMonthHistoryRepository =  dieselAllowancePerMonthHistoryRepository;
		this.monthlyPhoneBillHistoryRepository =  monthlyPhoneBillHistoryRepository;
		this.employeeReplacementRepository = employeeReplacementRepository;
		this.delegateRepository = delegateRepository;
		this.internalOrderRepository = internalOrderRepository;
		this.dieselAllowancePerMonthRepository = dieselAllowancePerMonthRepository;
		this.petrolAllowancePerMonthRepository = petrolAllowancePerMonthRepository;
		this.advanceOutstandingRepository = advanceOutstandingRepository;
		this.vatRepository = vatRepository;
		this.withholdingTaxRepository = withholdingTaxRepository;
		this.witholdngTaxCodeRepository = witholdngTaxCodeRepository;
		this.currencyRateRepository = currencyRateRepository;

	}

	@Override
	public void run(String... strings) throws Exception {
		
		/* RunningType */
		RunningType expenseDocumentRunningType = new RunningType();
		expenseDocumentRunningType.setCode("EXP");
		expenseDocumentRunningType.setName("Expense Document");
		runningTypeRepository.save(expenseDocumentRunningType);
		

		RunningType approveDocumentRunningType = new RunningType();
		approveDocumentRunningType.setCode("APP");
		approveDocumentRunningType.setRunningFormat("APP000000");
		approveDocumentRunningType.setName("Approve Document");
		runningTypeRepository.save(approveDocumentRunningType);

		RunningType advanceDocumentRunningType = new RunningType();
		advanceDocumentRunningType.setCode("ADV");
		advanceDocumentRunningType.setName("Advance Document");
		runningTypeRepository.save(advanceDocumentRunningType);
		
		
		/* CostCenter */
		CostCenter costCenter = new CostCenter();
		costCenter.setCode("1019201020");
		costCenter.setDescription("Digital Strategy");
		costCenter.setEmpUserName("karoons");
		this.costCenterRepository.save(costCenter);

		CostCenter costCenter1 = new CostCenter();
		costCenter1.setCode("2503350002");
		costCenter1.setDescription("Digital Strategy");
		costCenter1.setEmpUserName("nattapornt");
		this.costCenterRepository.save(costCenter1);

		CostCenter costCenter2 = new CostCenter();
		costCenter2.setCode("1013401000");
		costCenter2.setDescription("Digital Strategy");
		costCenter2.setEmpUserName("jaruneet");
		this.costCenterRepository.save(costCenter2);

		
		/* Department */
		Department department = new Department();
		department.setCode("1019201000");
		department.setDescription("Digital Strategy");
		this.departmentRepository.save(department);
		
		/* Company */
		Company company = new Company();
		company.setCode("1010");
		company.setDescription("บริษัทน้ำตาลมิตรผล");
		this.companyRepository.save(company);
		
		
		/* Employee */
		Employee employee1 = new Employee();
		employee1.setEmpCode("00001");
		employee1.setEmpName("นายธนชัย");
		employee1.setEmpLastName("ชมชื่นใจ");
		employee1.setEmpLevel("3");
		employee1.setCompany(company);
		this.employeeRepository.save(employee1);

		Employee employee2 = new Employee();
		employee2.setEmpCode("00002");
		employee2.setEmpName("นางสาวสิริรัตน์");
		employee2.setEmpLastName("ไชยภูมิ");
		employee2.setEmpLevel("4");
		employee2.setCompany(company);
		this.employeeRepository.save(employee2);

		Employee employee3 = new Employee();
		employee3.setEmpCode("00003");
		employee3.setEmpName("นายสุริยา");
		employee3.setEmpLastName("เอี่ยมเอิบ");
		employee3.setEmpLevel("5");
		employee3.setCompany(company);
		this.employeeRepository.save(employee3);

		Employee employee4 = new Employee();
		employee4.setEmpCode("00004");
		employee4.setEmpName("นายภานุพงค์");
		employee4.setEmpLastName("จันทกลาง");
		employee4.setEmpLevel("6");
		employee4.setCompany(company);
		this.employeeRepository.save(employee4);

		Employee employee5 = new Employee();
		employee5.setEmpCode("00005");
		employee5.setEmpName("นายการุณย์");
		employee5.setEmpLastName("ศิลปพันธ์");
		employee5.setEmpLevel("6");
		employee5.setCompany(company);
		employee5.setEmpUsername("karoons");
		this.employeeRepository.save(employee5);

		Employee employee6 = new Employee();
		employee6.setEmpCode("00004");
		employee6.setEmpName("นายบุญชัย");
		employee6.setEmpLastName("พิริยะสถิต");
		employee6.setEmpLevel("6");
		employee6.setCompany(company);
		employee6.setEmpUsername("boonchiap");
		this.employeeRepository.save(employee6);


		/* Location Domestic */
		Location location = new Location();
		location.setCode("0001");
		location.setDescription("ด่านช้าง");
		location.setLocationType("D");
		location.setFlagOther("N");
		location.setFlagActive(true);
		location.setPsaCode("0002");
		this.locationRepository.save(location);

		Location location1 = new Location();
		location1.setCode("0002");
		location1.setDescription("สิงห์บุรี");
		location1.setLocationType("D");
		location1.setFlagOther("N");
		location1.setFlagActive(true);
		location1.setPsaCode("0006");
		this.locationRepository.save(location1);
		
		Location location2 = new Location();
		location2.setCode("0003");
		location2.setDescription("สำนักงานใหญ่");
		location2.setLocationType("D");
		location2.setFlagOther("N");
		location2.setFlagActive(true);
		location2.setPsaCode("0001");
		this.locationRepository.save(location2);
		
		Location location3 = new Location();
		location3.setCode("0004");
		location3.setDescription("จังหวัดปัตตานี");
		location3.setSpeacialRate("200");
		location3.setLocationType("D");
		location3.setFlagOther("N");
		location3.setFlagActive(true);
		this.locationRepository.save(location3);
		
		Location location4 = new Location();
		location4.setCode("0005");
		location4.setDescription("จังหวัดยะลา");
		location4.setSpeacialRate("200");
		location4.setLocationType("D");
		location4.setFlagOther("N");
		location4.setFlagActive(true);
		this.locationRepository.save(location4);
		
		Location location5 = new Location();
		location5.setCode("0006");
		location5.setDescription("จังหวัดนราธิวาส");
		location5.setSpeacialRate("200");
		location5.setLocationType("D");
		location5.setFlagOther("N");
		location5.setFlagActive(true);
		this.locationRepository.save(location5);
		
		Location location6 = new Location();
		location6.setCode("0007");
		location6.setDescription("อำเภอสะบ้าย้อย จังหวัดสงขลา");
		location6.setSpeacialRate("200");
		location6.setLocationType("D");
		location6.setFlagOther("N");
		location6.setFlagActive(true);
		this.locationRepository.save(location6);
		
		Location location7 = new Location();
		location7.setCode("0008");
		location7.setDescription("อำเภอเทพา จังหวัดสงขลา");
		location7.setSpeacialRate("200");
		location7.setLocationType("D");
		location7.setFlagOther("N");
		location7.setFlagActive(true);
		this.locationRepository.save(location7);
		
		Location location8 = new Location();
		location8.setCode("0009");
		location8.setDescription("อำเภอจะนะ จังหวัดสงขลา");
		location8.setSpeacialRate("200");
		location8.setLocationType("D");
		location8.setFlagOther("N");
		location8.setFlagActive(true);
		this.locationRepository.save(location8);
		
		Location location9 = new Location();
		location9.setCode("0010");
		location9.setDescription("อำเภอนาทวี จังหวัดสงขลา");
		location9.setSpeacialRate("200");
		location9.setLocationType("D");
		location9.setFlagOther("N");
		location9.setFlagActive(true);
		this.locationRepository.save(location9);

		Location location213 = new Location();
		location213.setCode("0023");
		location213.setDescription("ภูเขียว");
		location213.setLocationType("D");
		location213.setFlagOther("N");
		location213.setFlagActive(true);
		location213.setPsaCode("0003");
		this.locationRepository.save(location213);

		Location location212 = new Location();
		location212.setCode("0024");
		location212.setDescription("ภูเวียง");
		location212.setLocationType("D");
		location212.setFlagOther("N");
		location212.setFlagActive(true);
		location212.setPsaCode("0004");
		this.locationRepository.save(location212);

		Location location211 = new Location();
		location211.setCode("0025");
		location211.setDescription("กาฬสินธุ์");
		location211.setLocationType("D");
		location211.setFlagOther("N");
		location211.setFlagActive(true);
		location211.setPsaCode("0005");
		this.locationRepository.save(location211);

		Location location210 = new Location();
		location210.setCode("0026");
		location210.setDescription("ภูหลวง");
		location210.setLocationType("D");
		location210.setFlagOther("N");
		location210.setFlagActive(true);
		location210.setPsaCode("0007");
		this.locationRepository.save(location210);

		Location location209 = new Location();
		location209.setCode("0027");
		location209.setDescription("แม่สอด");
		location209.setLocationType("D");
		location209.setFlagOther("N");
		location209.setFlagActive(true);
		location209.setPsaCode("0008");
		this.locationRepository.save(location209);

		Location location208 = new Location();
		location208.setCode("0028");
		location208.setDescription("นครราชสีมา");
		location208.setLocationType("D");
		location208.setFlagOther("N");
		location208.setFlagActive(true);
		location208.setPsaCode("0009");
		this.locationRepository.save(location208);

		Location location207 = new Location();
		location207.setCode("0029");
		location207.setDescription("หาดใหญ่");
		location207.setLocationType("D");
		location207.setFlagOther("N");
		location207.setFlagActive(true);
		location207.setPsaCode("0010");
		this.locationRepository.save(location207);

		Location location206 = new Location();
		location206.setCode("0030");
		location206.setDescription("สมุทรสาคร");
		location206.setLocationType("D");
		location206.setFlagOther("N");
		location206.setFlagActive(true);
		location206.setPsaCode("0011");
		this.locationRepository.save(location206);

		
		/* Location Foreign */

		Location location10 = new Location();
		location10.setCode("0019");
		location10.setDescription("กัมพูชา");
		location10.setLocationType("F");
		location10.setZoneCode("MM");
		this.locationRepository.save(location10);

		Location location11 = new Location();
		location11.setCode("0020");
		location11.setDescription("กาตาร์");
		location11.setLocationType("F");
		location11.setZoneCode("OT");
		this.locationRepository.save(location11);

		Location location12 = new Location();
		location12.setCode("0021");
		location12.setDescription("เกาหลีใต้");
		location12.setLocationType("F");
		location12.setZoneCode("OT");
		this.locationRepository.save(location12);

		Location location13 = new Location();
		location13.setCode("0022");
		location13.setDescription("เกาหลีเหนือ");
		location13.setLocationType("F");
		location13.setZoneCode("OT");
		this.locationRepository.save(location13);

		Location location14 = new Location();
		location14.setCode("0023");
		location14.setDescription("คาซัคสถาน");
		location14.setLocationType("F");
		location14.setZoneCode("OT");
		this.locationRepository.save(location14);

		Location location15 = new Location();
		location15.setCode("0024");
		location15.setDescription("คีร์กีซสถาน");
		location15.setLocationType("F");
		location15.setZoneCode("OT");
		this.locationRepository.save(location15);

		Location location16 = new Location();
		location16.setCode("0025");
		location16.setDescription("คูเวต");
		location16.setLocationType("F");
		location16.setZoneCode("OT");
		this.locationRepository.save(location16);

		Location location17 = new Location();
		location17.setCode("0026");
		location17.setDescription("จอร์เจีย");
		location17.setLocationType("F");
		location17.setZoneCode("OT");
		this.locationRepository.save(location17);

		Location location18 = new Location();
		location18.setCode("0027");
		location18.setDescription("จอร์แดน");
		location18.setLocationType("F");
		location18.setZoneCode("OT");
		this.locationRepository.save(location18);

		Location location19 = new Location();
		location19.setCode("0028");
		location19.setDescription("จีน");
		location19.setLocationType("F");
		location19.setZoneCode("OT");
		this.locationRepository.save(location19);

		Location location20 = new Location();
		location20.setCode("0029");
		location20.setDescription("ซาอุดีอาระเบีย");
		location20.setLocationType("F");
		location20.setZoneCode("OT");
		this.locationRepository.save(location20);

		Location location21 = new Location();
		location21.setCode("0030");
		location21.setDescription("ซีเรีย");
		location21.setLocationType("F");
		location21.setZoneCode("OT");
		this.locationRepository.save(location21);

		Location location22 = new Location();
		location22.setCode("0031");
		location22.setDescription("ไซปรัส");
		location22.setLocationType("F");
		location22.setZoneCode("OT");
		this.locationRepository.save(location22);

		Location location23 = new Location();
		location23.setCode("0032");
		location23.setDescription("  ญี่ปุ่น");
		location23.setLocationType("F");
		location23.setZoneCode("OT");
		this.locationRepository.save(location23);

		Location location24 = new Location();
		location24.setCode("0033");
		location24.setDescription("ติมอร์-เลสเต");
		location24.setLocationType("F");
		location24.setZoneCode("OT");
		this.locationRepository.save(location24);

		Location location25 = new Location();
		location25.setCode("0034");
		location25.setDescription("ตุรกี");
		location25.setLocationType("F");
		location25.setZoneCode("OT");
		this.locationRepository.save(location25);

		Location location26 = new Location();
		location26.setCode("0035");
		location26.setDescription("เติร์กเมนิสถาน");
		location26.setLocationType("F");
		location26.setZoneCode("OT");
		this.locationRepository.save(location26);

		Location location27 = new Location();
		location27.setCode("0036");
		location27.setDescription("ทาจิกิสถาน");
		location27.setLocationType("F");
		location27.setZoneCode("OT");
		this.locationRepository.save(location27);

		Location location28 = new Location();
		location28.setCode("0037");
		location28.setDescription("ไทย");
		location28.setLocationType("F");
		location28.setZoneCode("MM");
		location28.setPsaCode("0001");
		this.locationRepository.save(location28);

		Location location29 = new Location();
		location29.setCode("0038");
		location29.setDescription("เนปาล");
		location29.setLocationType("F");
		location29.setZoneCode("OT");
		this.locationRepository.save(location29);

		Location location30 = new Location();
		location30.setCode("0039");
		location30.setDescription("บรูไนดารุสซาลาม");
		location30.setLocationType("F");
		location30.setZoneCode("MM");
		this.locationRepository.save(location30);

		Location location31 = new Location();
		location31.setCode("0040");
		location31.setDescription("บังกลาเทศ");
		location31.setLocationType("F");
		location31.setZoneCode("OT");
		this.locationRepository.save(location31);

		Location location32 = new Location();
		location32.setCode("0041");
		location32.setDescription("บาห์เรน");
		location32.setLocationType("F");
		location32.setZoneCode("OT");
		this.locationRepository.save(location32);

		Location location33 = new Location();
		location33.setCode("0042");
		location33.setDescription("ปากีสถาน");
		location33.setLocationType("F");
		location33.setZoneCode("OT");
		this.locationRepository.save(location33);

		Location location34 = new Location();
		location34.setCode("0043");
		location34.setDescription("ปาเลสไตน์");
		location34.setLocationType("F");
		location34.setZoneCode("OT");
		this.locationRepository.save(location34);

		Location location35 = new Location();
		location35.setCode("0044");
		location35.setDescription("พม่า");
		location35.setLocationType("F");
		location35.setZoneCode("MM");
		this.locationRepository.save(location35);

		Location location36 = new Location();
		location36.setCode("0045");
		location36.setDescription("ฟิลิปปินส์");
		location36.setLocationType("F");
		location36.setZoneCode("MM");
		this.locationRepository.save(location36);

		Location location37 = new Location();
		location37.setCode("0046");
		location37.setDescription("ภูฏาน");
		location37.setLocationType("F");
		location37.setZoneCode("OT");
		this.locationRepository.save(location37);

		Location location38 = new Location();
		location38.setCode("0047");
		location38.setDescription("มองโกเลีย");
		location38.setLocationType("F");
		location38.setZoneCode("OT");
		this.locationRepository.save(location38);

		Location location39 = new Location();
		location39.setCode("0048");
		location39.setDescription("มัลดีฟส์");
		location39.setLocationType("F");
		location39.setZoneCode("OT");
		this.locationRepository.save(location39);

		Location location40 = new Location();
		location40.setCode("0049");
		location40.setDescription("มาเลเซีย");
		location40.setLocationType("F");
		location40.setZoneCode("MM");
		this.locationRepository.save(location40);

		Location location41 = new Location();
		location41.setCode("0050");
		location41.setDescription("เยเมน");
		location41.setLocationType("F");
		location41.setZoneCode("OT");
		this.locationRepository.save(location41);

		Location location42 = new Location();
		location42.setCode("0051");
		location42.setDescription("ลาว");
		location42.setLocationType("F");
		location42.setZoneCode("MM");
		this.locationRepository.save(location42);

		Location location43 = new Location();
		location43.setCode("0052");
		location43.setDescription("เลบานอน");
		location43.setLocationType("F");
		location43.setZoneCode("OT");
		this.locationRepository.save(location43);

		Location location44 = new Location();
		location44.setCode("0053");
		location44.setDescription("เวียดนาม");
		location44.setLocationType("F");
		location44.setZoneCode("MM");
		this.locationRepository.save(location44);

		Location location45 = new Location();
		location45.setCode("0054");
		location45.setDescription("ศรีลังกา");
		location45.setLocationType("F");
		location45.setZoneCode("OT");
		this.locationRepository.save(location45);

		Location location46 = new Location();
		location46.setCode("0055");
		location46.setDescription("สหรัฐอาหรับเอมิเรตส์");
		location46.setLocationType("F");
		location46.setZoneCode("OT");
		this.locationRepository.save(location46);

		Location location47 = new Location();
		location47.setCode("0056");
		location47.setDescription("สิงคโปร์");
		location47.setLocationType("F");
		location47.setZoneCode("MM");
		this.locationRepository.save(location47);

		Location location48 = new Location();
		location48.setCode("0057");
		location48.setDescription("อัฟกานิสถาน");
		location48.setLocationType("F");
		location48.setZoneCode("OT");
		this.locationRepository.save(location48);

		Location location49 = new Location();
		location49.setCode("0058");
		location49.setDescription("อาเซอร์ไบจาน");
		location49.setLocationType("F");
		location49.setZoneCode("OT");
		this.locationRepository.save(location49);

		Location location50 = new Location();
		location50.setCode("0059");
		location50.setDescription("อาร์มีเนีย");
		location50.setLocationType("F");
		location50.setZoneCode("OT");
		this.locationRepository.save(location50);

		Location location51 = new Location();
		location51.setCode("0060");
		location51.setDescription("อินเดีย");
		location51.setLocationType("F");
		location51.setZoneCode("OT");
		this.locationRepository.save(location51);

		Location location52 = new Location();
		location52.setCode("0061");
		location52.setDescription("อินโดนีเซีย");
		location52.setLocationType("F");
		location52.setZoneCode("MM");
		this.locationRepository.save(location52);

		Location location53 = new Location();
		location53.setCode("0062");
		location53.setDescription("อิรัก");
		location53.setLocationType("F");
		location53.setZoneCode("OT");
		this.locationRepository.save(location53);

		Location location54 = new Location();
		location54.setCode("0063");
		location54.setDescription("อิสราเอล");
		location54.setLocationType("F");
		location54.setZoneCode("OT");
		this.locationRepository.save(location54);

		Location location55 = new Location();
		location55.setCode("0064");
		location55.setDescription("อิหร่าน");
		location55.setLocationType("F");
		location55.setZoneCode("OT");
		this.locationRepository.save(location55);

		Location location56 = new Location();
		location56.setCode("0065");
		location56.setDescription("อุซเบกิสถาน");
		location56.setLocationType("F");
		location56.setZoneCode("OT");
		this.locationRepository.save(location56);

		Location location57 = new Location();
		location57.setCode("0066");
		location57.setDescription("โอมาน");
		location57.setLocationType("F");
		location57.setZoneCode("OT");
		this.locationRepository.save(location57);

		Location location58 = new Location();
		location58.setCode("0067");
		location58.setDescription("คิริบาส");
		location58.setLocationType("F");
		location58.setZoneCode("OT");
		this.locationRepository.save(location58);

		Location location59 = new Location();
		location59.setCode("0068");
		location59.setDescription("ซามัว");
		location59.setLocationType("F");
		location59.setZoneCode("OT");
		this.locationRepository.save(location59);

		Location location60 = new Location();
		location60.setCode("0069");
		location60.setDescription("ตองงา");
		location60.setLocationType("F");
		location60.setZoneCode("OT");
		this.locationRepository.save(location60);

		Location location61 = new Location();
		location61.setCode("0070");
		location61.setDescription("ตูวาลู");
		location61.setLocationType("F");
		location61.setZoneCode("OT");
		this.locationRepository.save(location61);

		Location location62 = new Location();
		location62.setCode("0071");
		location62.setDescription("นาอูรู");
		location62.setLocationType("F");
		location62.setZoneCode("OT");
		this.locationRepository.save(location62);

		Location location63 = new Location();
		location63.setCode("0072");
		location63.setDescription("นิวซีแลนด์");
		location63.setLocationType("F");
		location63.setZoneCode("OT");
		this.locationRepository.save(location63);

		Location location64 = new Location();
		location64.setCode("0073");
		location64.setDescription("ปาปัวนิวกินี");
		location64.setLocationType("F");
		location64.setZoneCode("OT");
		this.locationRepository.save(location64);

		Location location65 = new Location();
		location65.setCode("0074");
		location65.setDescription("ปาเลา");
		location65.setLocationType("F");
		location65.setZoneCode("OT");
		this.locationRepository.save(location65);

		Location location66 = new Location();
		location66.setCode("0075");
		location66.setDescription("ฟีจี");
		location66.setLocationType("F");
		location66.setZoneCode("OT");
		this.locationRepository.save(location66);

		Location location67 = new Location();
		location67.setCode("0076");
		location67.setDescription("ไมโครนีเซีย");
		location67.setLocationType("F");
		location67.setZoneCode("OT");
		this.locationRepository.save(location67);

		Location location68 = new Location();
		location68.setCode("0077");
		location68.setDescription("วานูวาตู");
		location68.setLocationType("F");
		location68.setZoneCode("OT");
		this.locationRepository.save(location68);

		Location location69 = new Location();
		location69.setCode("0078");
		location69.setDescription("หมู่เกาะโซโลมอน");
		location69.setLocationType("F");
		location69.setZoneCode("OT");
		this.locationRepository.save(location69);

		Location location70 = new Location();
		location70.setCode("0079");
		location70.setDescription("หมู่เกาะมาร์แชลล์");
		location70.setLocationType("F");
		location70.setZoneCode("OT");
		this.locationRepository.save(location70);

		Location location71 = new Location();
		location71.setCode("0080");
		location71.setDescription("ออสเตรเลีย");
		location71.setLocationType("F");
		location71.setZoneCode("OT");
		this.locationRepository.save(location71);

		Location location72 = new Location();
		location72.setCode("0081");
		location72.setDescription("กัวเตมาลา");
		location72.setLocationType("F");
		location72.setZoneCode("SA");
		this.locationRepository.save(location72);

		Location location73 = new Location();
		location73.setCode("0082");
		location73.setDescription("เกรเนดา");
		location73.setLocationType("F");
		location73.setZoneCode("SA");
		this.locationRepository.save(location73);

		Location location74 = new Location();
		location74.setCode("0083");
		location74.setDescription("คอสตาริกา");
		location74.setLocationType("F");
		location74.setZoneCode("SA");
		this.locationRepository.save(location74);

		Location location75 = new Location();
		location75.setCode("0084");
		location75.setDescription("คิวบา");
		location75.setLocationType("F");
		location75.setZoneCode("SA");
		this.locationRepository.save(location75);

		Location location76 = new Location();
		location76.setCode("0085");
		location76.setDescription("แคนาดา");
		location76.setLocationType("F");
		location76.setZoneCode("SA");
		this.locationRepository.save(location76);

		Location location77 = new Location();
		location77.setCode("0086");
		location77.setDescription("จาเมกา");
		location77.setLocationType("F");
		location77.setZoneCode("SA");
		this.locationRepository.save(location77);

		Location location78 = new Location();
		location78.setCode("0087");
		location78.setDescription("เซนต์คิตส์และเนวิส");
		location78.setLocationType("F");
		location78.setZoneCode("SA");
		this.locationRepository.save(location78);

		Location location79 = new Location();
		location79.setCode("0088");
		location79.setDescription("เซนต์ลูเชีย");
		location79.setLocationType("F");
		location79.setZoneCode("SA");
		this.locationRepository.save(location79);

		Location location80 = new Location();
		location80.setCode("0089");
		location80.setDescription("เซนต์วินเซนต์และเกรนาดีนส์");
		location80.setLocationType("F");
		location80.setZoneCode("SA");
		this.locationRepository.save(location80);

		Location location81 = new Location();
		location81.setCode("0090");
		location81.setDescription("ดอมินีกา");
		location81.setLocationType("F");
		location81.setZoneCode("SA");
		this.locationRepository.save(location81);

		Location location82 = new Location();
		location82.setCode("0091");
		location82.setDescription("นิการากัว");
		location82.setLocationType("F");
		location82.setZoneCode("SA");
		this.locationRepository.save(location82);

		Location location83 = new Location();
		location83.setCode("0092");
		location83.setDescription("บาร์เบโดส");
		location83.setLocationType("F");
		location83.setZoneCode("SA");
		this.locationRepository.save(location83);

		Location location84 = new Location();
		location84.setCode("0093");
		location84.setDescription("บาฮามาส");
		location84.setLocationType("F");
		location84.setZoneCode("SA");
		this.locationRepository.save(location84);

		Location location85 = new Location();
		location85.setCode("0094");
		location85.setDescription("เบลีซ");
		location85.setLocationType("F");
		location85.setZoneCode("SA");
		this.locationRepository.save(location85);

		Location location86 = new Location();
		location86.setCode("0095");
		location86.setDescription("ปานามา");
		location86.setLocationType("F");
		location86.setZoneCode("SA");
		this.locationRepository.save(location86);

		Location location87 = new Location();
		location87.setCode("0096");
		location87.setDescription("เม็กซิโก");
		location87.setLocationType("F");
		location87.setZoneCode("SA");
		this.locationRepository.save(location87);

		Location location88 = new Location();
		location88.setCode("0097");
		location88.setDescription("สหรัฐอเมริกา");
		location88.setLocationType("F");
		location88.setZoneCode("SA");
		this.locationRepository.save(location88);

		Location location89 = new Location();
		location89.setCode("0098");
		location89.setDescription("สาธารณรัฐโดมินิกัน");
		location89.setLocationType("F");
		location89.setZoneCode("SA");
		this.locationRepository.save(location89);

		Location location90 = new Location();
		location90.setCode("0099");
		location90.setDescription("เอลซัลวาดอร์");
		location90.setLocationType("F");
		location90.setZoneCode("SA");
		this.locationRepository.save(location90);

		Location location91 = new Location();
		location91.setCode("0100");
		location91.setDescription("แอนติกาและบาร์บูดา");
		location91.setLocationType("F");
		location91.setZoneCode("SA");
		this.locationRepository.save(location91);

		Location location92 = new Location();
		location92.setCode("0101");
		location92.setDescription("ฮอนดูรัส");
		location92.setLocationType("F");
		location92.setZoneCode("SA");
		this.locationRepository.save(location92);

		Location location93 = new Location();
		location93.setCode("0102");
		location93.setDescription("เฮติ");
		location93.setLocationType("F");
		location93.setZoneCode("SA");
		this.locationRepository.save(location93);

		Location location94 = new Location();
		location94.setCode("0103");
		location94.setDescription("กายอานา");
		location94.setLocationType("F");
		location94.setZoneCode("SA");
		this.locationRepository.save(location94);

		Location location95 = new Location();
		location95.setCode("0104");
		location95.setDescription("โคลอมเบีย");
		location95.setLocationType("F");
		location95.setZoneCode("SA");
		this.locationRepository.save(location95);

		Location location96 = new Location();
		location96.setCode("0105");
		location96.setDescription("ชิลี");
		location96.setLocationType("F");
		location96.setZoneCode("SA");
		this.locationRepository.save(location96);

		Location location97 = new Location();
		location97.setCode("0106");
		location97.setDescription("ซูรินาม");
		location97.setLocationType("F");
		location97.setZoneCode("SA");
		this.locationRepository.save(location97);

		Location location98 = new Location();
		location98.setCode("0107");
		location98.setDescription("ตรินิแดดและโตเบโก");
		location98.setLocationType("F");
		location98.setZoneCode("SA");
		this.locationRepository.save(location98);

		Location location99 = new Location();
		location99.setCode("0108");
		location99.setDescription("บราซิล");
		location99.setLocationType("F");
		location99.setZoneCode("SA");
		this.locationRepository.save(location99);

		Location location100 = new Location();
		location100.setCode("0109");
		location100.setDescription("โบลิเวีย");
		location100.setLocationType("F");
		location100.setZoneCode("SA");
		this.locationRepository.save(location100);

		Location location101 = new Location();
		location101.setCode("0110");
		location101.setDescription("ปารากวัย");
		location101.setLocationType("F");
		location101.setZoneCode("SA");
		this.locationRepository.save(location101);

		Location location102 = new Location();
		location102.setCode("0111");
		location102.setDescription("เปรู");
		location102.setLocationType("F");
		location102.setZoneCode("SA");
		this.locationRepository.save(location102);

		Location location103 = new Location();
		location103.setCode("0112");
		location103.setDescription("เวเนซุเอลา");
		location103.setLocationType("F");
		location103.setZoneCode("SA");
		this.locationRepository.save(location103);

		Location location104 = new Location();
		location104.setCode("0113");
		location104.setDescription("อาร์เจนตินา");
		location104.setLocationType("F");
		location104.setZoneCode("SA");
		this.locationRepository.save(location104);

		Location location105 = new Location();
		location105.setCode("0114");
		location105.setDescription("อุรุกวัย");
		location105.setLocationType("F");
		location105.setZoneCode("SA");
		this.locationRepository.save(location105);

		Location location106 = new Location();
		location106.setCode("0115");
		location106.setDescription("เอกวาดอร์");
		location106.setLocationType("F");
		location106.setZoneCode("SA");
		this.locationRepository.save(location106);

		Location location107 = new Location();
		location107.setCode("0116");
		location107.setDescription("กรีซ");
		location107.setLocationType("F");
		location107.setZoneCode("EU");
		this.locationRepository.save(location107);

		Location location108 = new Location();
		location108.setCode("0117");
		location108.setDescription("คอซอวอ");
		location108.setLocationType("F");
		location108.setZoneCode("EU");
		this.locationRepository.save(location108);

		Location location109 = new Location();
		location109.setCode("0118");
		location109.setDescription("โครเอเชีย");
		location109.setLocationType("F");
		location109.setZoneCode("EU");
		this.locationRepository.save(location109);

		Location location110 = new Location();
		location110.setCode("0119");
		location110.setDescription("เช็กเกีย");
		location110.setLocationType("F");
		location110.setZoneCode("EU");
		this.locationRepository.save(location110);

		Location location111 = new Location();
		location111.setCode("0120");
		location111.setDescription("ซานมารีโน");
		location111.setLocationType("F");
		location111.setZoneCode("EU");
		this.locationRepository.save(location111);

		Location location112 = new Location();
		location112.setCode("0121");
		location112.setDescription("เซอร์เบีย");
		location112.setLocationType("F");
		location112.setZoneCode("EU");
		this.locationRepository.save(location112);

		Location location113 = new Location();
		location113.setCode("0122");
		location113.setDescription("เดนมาร์ก");
		location113.setLocationType("F");
		location113.setZoneCode("EU");
		this.locationRepository.save(location113);

		Location location114 = new Location();
		location114.setCode("0123");
		location114.setDescription("นครรัฐวาติกัน");
		location114.setLocationType("F");
		location114.setZoneCode("EU");
		this.locationRepository.save(location114);

		Location location115 = new Location();
		location115.setCode("0124");
		location115.setDescription("นอร์เวย์");
		location115.setLocationType("F");
		location115.setZoneCode("EU");
		this.locationRepository.save(location115);

		Location location116 = new Location();
		location116.setCode("0125");
		location116.setDescription("เนเธอร์แลนด์");
		location116.setLocationType("F");
		location116.setZoneCode("EU");
		this.locationRepository.save(location116);

		Location location117 = new Location();
		location117.setCode("0126");
		location117.setDescription("บอสเนียและเฮอร์เซโกวีนา");
		location117.setLocationType("F");
		location117.setZoneCode("EU");
		this.locationRepository.save(location117);

		Location location118 = new Location();
		location118.setCode("0127");
		location118.setDescription("บัลแกเรีย");
		location118.setLocationType("F");
		location118.setZoneCode("EU");
		this.locationRepository.save(location118);

		Location location119 = new Location();
		location119.setCode("0128");
		location119.setDescription("เบลเยียม");
		location119.setLocationType("F");
		location119.setZoneCode("EU");
		this.locationRepository.save(location119);

		Location location120 = new Location();
		location120.setCode("0129");
		location120.setDescription("เบลารุส");
		location120.setLocationType("F");
		location120.setZoneCode("EU");
		this.locationRepository.save(location120);

		Location location121 = new Location();
		location121.setCode("0130");
		location121.setDescription("โปรตุเกส");
		location121.setLocationType("F");
		location121.setZoneCode("EU");
		this.locationRepository.save(location121);

		Location location122 = new Location();
		location122.setCode("0131");
		location122.setDescription(" โปแลนด์");
		location122.setLocationType("F");
		location122.setZoneCode("EU");
		this.locationRepository.save(location122);

		Location location123 = new Location();
		location123.setCode("0132");
		location123.setDescription("ฝรั่งเศส");
		location123.setLocationType("F");
		location123.setZoneCode("EU");
		this.locationRepository.save(location123);

		Location location124 = new Location();
		location124.setCode("0133");
		location124.setDescription("ฟินแลนด์");
		location124.setLocationType("F");
		location124.setZoneCode("EU");
		this.locationRepository.save(location124);

		Location location125 = new Location();
		location125.setCode("0134");
		location125.setDescription("มอนเตเนโกร");
		location125.setLocationType("F");
		location125.setZoneCode("EU");
		this.locationRepository.save(location125);

		Location location126 = new Location();
		location126.setCode("0135");
		location126.setDescription("มอลโดวา");
		location126.setLocationType("F");
		location126.setZoneCode("EU");
		this.locationRepository.save(location126);

		Location location127 = new Location();
		location127.setCode("0136");
		location127.setDescription("มอลตา");
		location127.setLocationType("F");
		location127.setZoneCode("EU");
		this.locationRepository.save(location127);

		Location location128 = new Location();
		location128.setCode("0137");
		location128.setDescription(" มาซิโดเนีย");
		location128.setLocationType("F");
		location128.setZoneCode("EU");
		this.locationRepository.save(location128);

		Location location129 = new Location();
		location129.setCode("0138");
		location129.setDescription(" โมนาโก");
		location129.setLocationType("F");
		location129.setZoneCode("EU");
		this.locationRepository.save(location129);

		Location location130 = new Location();
		location130.setCode("0139");
		location130.setDescription("ยูเครน");
		location130.setLocationType("F");
		location130.setZoneCode("EU");
		this.locationRepository.save(location130);

		Location location131 = new Location();
		location131.setCode("0140");
		location131.setDescription(" เยอรมนี");
		location131.setLocationType("F");
		location131.setZoneCode("EU");
		this.locationRepository.save(location131);

		Location location132 = new Location();
		location132.setCode("0141");
		location132.setDescription(" รัสเซีย");
		location132.setLocationType("F");
		location132.setZoneCode("EU");
		this.locationRepository.save(location132);

		Location location133 = new Location();
		location133.setCode("0142");
		location133.setDescription("โรมาเนีย");
		location133.setLocationType("F");
		location133.setZoneCode("EU");
		this.locationRepository.save(location133);

		Location location134 = new Location();
		location134.setCode("0143");
		location134.setDescription("ลักเซมเบิร์ก");
		location134.setLocationType("F");
		location134.setZoneCode("EU");
		this.locationRepository.save(location134);

		Location location135 = new Location();
		location135.setCode("0144");
		location135.setDescription("ลัตเวีย");
		location135.setLocationType("F");
		location135.setZoneCode("EU");
		this.locationRepository.save(location135);

		Location location136 = new Location();
		location136.setCode("0145");
		location136.setDescription("ลิกเตนสไตน์");
		location136.setLocationType("F");
		location136.setZoneCode("EU");
		this.locationRepository.save(location136);

		Location location137 = new Location();
		location137.setCode("0146");
		location137.setDescription("ลิทัวเนีย");
		location137.setLocationType("F");
		location137.setZoneCode("EU");
		this.locationRepository.save(location137);

		Location location138 = new Location();
		location138.setCode("0147");
		location138.setDescription("สเปน");
		location138.setLocationType("F");
		location138.setZoneCode("EU");
		this.locationRepository.save(location138);

		Location location139 = new Location();
		location139.setCode("0148");
		location139.setDescription("สโลวาเกีย");
		location139.setLocationType("F");
		location139.setZoneCode("EU");
		this.locationRepository.save(location139);

		Location location140 = new Location();
		location140.setCode("0149");
		location140.setDescription("สโลวีเนีย");
		location140.setLocationType("F");
		location140.setZoneCode("EU");
		this.locationRepository.save(location140);

		Location location141 = new Location();
		location141.setCode("0150");
		location141.setDescription("สวิตเซอร์แลนด์");
		location141.setLocationType("F");
		location141.setZoneCode("EU");
		this.locationRepository.save(location141);

		Location location142 = new Location();
		location142.setCode("0151");
		location142.setDescription("สวีเดน");
		location142.setLocationType("F");
		location142.setZoneCode("EU");
		this.locationRepository.save(location142);

		Location location143 = new Location();
		location143.setCode("0152");
		location143.setDescription("สหราชอาณาจักร");
		location143.setLocationType("F");
		location143.setZoneCode("EU");
		this.locationRepository.save(location143);

		Location location144 = new Location();
		location144.setCode("0153");
		location144.setDescription(" ออสเตรีย");
		location144.setLocationType("F");
		location144.setZoneCode("EU");
		this.locationRepository.save(location144);

		Location location145 = new Location();
		location145.setCode("0154");
		location145.setDescription("อันดอร์รา");
		location145.setLocationType("F");
		location145.setZoneCode("EU");
		this.locationRepository.save(location145);

		Location location146 = new Location();
		location146.setCode("0155");
		location146.setDescription("อิตาลี");
		location146.setLocationType("F");
		location146.setZoneCode("EU");
		this.locationRepository.save(location146);

		Location location147 = new Location();
		location147.setCode("0156");
		location147.setDescription("เอสโตเนีย");
		location147.setLocationType("F");
		location147.setZoneCode("EU");
		this.locationRepository.save(location147);

		Location location148 = new Location();
		location148.setCode("0157");
		location148.setDescription("แอลเบเนีย");
		location148.setLocationType("F");
		location148.setZoneCode("EU");
		this.locationRepository.save(location148);

		Location location149 = new Location();
		location149.setCode("0158");
		location149.setDescription("ไอซ์แลนด์");
		location149.setLocationType("F");
		location149.setZoneCode("EU");
		this.locationRepository.save(location149);

		Location location150 = new Location();
		location150.setCode("0159");
		location150.setDescription("ไอร์แลนด์");
		location150.setLocationType("F");
		location150.setZoneCode("EU");
		this.locationRepository.save(location150);

		Location location151 = new Location();
		location151.setCode("0160");
		location151.setDescription("ฮังการี");
		location151.setLocationType("F");
		location151.setZoneCode("EU");
		this.locationRepository.save(location151);

		Location location152 = new Location();
		location152.setCode("0161");
		location152.setDescription("กานา");
		location152.setLocationType("F");
		location152.setZoneCode("OT");
		this.locationRepository.save(location152);

		Location location153 = new Location();
		location153.setCode("0162");
		location153.setDescription("กาบอง");
		location153.setLocationType("F");
		location153.setZoneCode("OT");
		this.locationRepository.save(location153);

		Location location154 = new Location();
		location154.setCode("0163");
		location154.setDescription("กาบูเวร์ดี");
		location154.setLocationType("F");
		location154.setZoneCode("OT");
		this.locationRepository.save(location154);

		Location location155 = new Location();
		location155.setCode("0164");
		location155.setDescription("กินี");
		location155.setLocationType("F");
		location155.setZoneCode("OT");
		this.locationRepository.save(location155);

		Location location156 = new Location();
		location156.setCode("0165");
		location156.setDescription("กินี-บิสเซา");
		location156.setLocationType("F");
		location156.setZoneCode("OT");
		this.locationRepository.save(location156);

		Location location157 = new Location();
		location157.setCode("0166");
		location157.setDescription("แกมเบีย");
		location157.setLocationType("F");
		location157.setZoneCode("OT");
		this.locationRepository.save(location157);

		Location location158 = new Location();
		location158.setCode("0167");
		location158.setDescription("โกตดิวัวร์");
		location158.setLocationType("F");
		location158.setZoneCode("OT");
		this.locationRepository.save(location158);

		Location location159 = new Location();
		location159.setCode("0168");
		location159.setDescription("คอโมโรส");
		location159.setLocationType("F");
		location159.setZoneCode("OT");
		this.locationRepository.save(location159);

		Location location160 = new Location();
		location160.setCode("0169");
		location160.setDescription("เคนยา");
		location160.setLocationType("F");
		location160.setZoneCode("OT");
		this.locationRepository.save(location160);

		Location location161 = new Location();
		location161.setCode("0170");
		location161.setDescription("แคเมอรูน");
		location161.setLocationType("F");
		location161.setZoneCode("OT");
		this.locationRepository.save(location161);

		Location location162 = new Location();
		location162.setCode("0171");
		location162.setDescription("จิบูตี");
		location162.setLocationType("F");
		location162.setZoneCode("OT");
		this.locationRepository.save(location162);

		Location location163 = new Location();
		location163.setCode("0172");
		location163.setDescription("ชาด");
		location163.setLocationType("F");
		location163.setZoneCode("OT");
		this.locationRepository.save(location163);

		Location location164 = new Location();
		location164.setCode("0173");
		location164.setDescription("ซิมบับเว");
		location164.setLocationType("F");
		location164.setZoneCode("OT");
		this.locationRepository.save(location164);

		Location location165 = new Location();
		location165.setCode("0174");
		location165.setDescription("ซูดาน");
		location165.setLocationType("F");
		location165.setZoneCode("OT");
		this.locationRepository.save(location165);

		Location location166 = new Location();
		location166.setCode("0175");
		location166.setDescription("เซเชลส์");
		location166.setLocationType("F");
		location166.setZoneCode("OT");
		this.locationRepository.save(location166);

		Location location167 = new Location();
		location167.setCode("0176");
		location167.setDescription("เซเนกัล");
		location167.setLocationType("F");
		location167.setZoneCode("OT");
		this.locationRepository.save(location167);

		Location location168 = new Location();
		location168.setCode("0177");
		location168.setDescription("เซาตูเมและปรินซีปี");
		location168.setLocationType("F");
		location168.setZoneCode("OT");
		this.locationRepository.save(location168);

		Location location169 = new Location();
		location169.setCode("0178");
		location169.setDescription("เซาท์ซูดาน");
		location169.setLocationType("F");
		location169.setZoneCode("OT");
		this.locationRepository.save(location169);

		Location location170 = new Location();
		location170.setCode("0179");
		location170.setDescription("เซียร์ราลีโอน");
		location170.setLocationType("F");
		location170.setZoneCode("OT");
		this.locationRepository.save(location170);

		Location location171 = new Location();
		location171.setCode("0180");
		location171.setDescription("แซมเบีย");
		location171.setLocationType("F");
		location171.setZoneCode("OT");
		this.locationRepository.save(location171);

		Location location172 = new Location();
		location172.setCode("0181");
		location172.setDescription("โซมาเลีย");
		location172.setLocationType("F");
		location172.setZoneCode("OT");
		this.locationRepository.save(location172);

		Location location173 = new Location();
		location173.setCode("0182");
		location173.setDescription("ตูนิเซีย");
		location173.setLocationType("F");
		location173.setZoneCode("OT");
		this.locationRepository.save(location173);


		Location location174 = new Location();
		location174.setCode("0183");
		location174.setDescription("โตโก");
		location174.setLocationType("F");
		location174.setZoneCode("OT");
		this.locationRepository.save(location174);


		Location location175 = new Location();
		location175.setCode("0184");
		location175.setDescription("แทนซาเนีย");
		location175.setLocationType("F");
		location175.setZoneCode("OT");
		this.locationRepository.save(location175);


		Location location176 = new Location();
		location176.setCode("0185");
		location176.setDescription("นามิเบีย");
		location176.setLocationType("F");
		location176.setZoneCode("OT");
		this.locationRepository.save(location176);


		Location location177 = new Location();
		location177.setCode("0186");
		location177.setDescription("ไนจีเรีย");
		location177.setLocationType("F");
		location177.setZoneCode("OT");
		this.locationRepository.save(location177);


		Location location178 = new Location();
		location178.setCode("0187");
		location178.setDescription("ไนเจอร์");
		location178.setLocationType("F");
		location178.setZoneCode("OT");
		this.locationRepository.save(location178);


		Location location179 = new Location();
		location179.setCode("0188");
		location179.setDescription("บอตสวานา");
		location179.setLocationType("F");
		location179.setZoneCode("OT");
		this.locationRepository.save(location179);


		Location location180 = new Location();
		location180.setCode("0189");
		location180.setDescription("บุรุนดี");
		location180.setLocationType("F");
		location180.setZoneCode("OT");
		this.locationRepository.save(location180);


		Location location181 = new Location();
		location181.setCode("0190");
		location181.setDescription("บูร์กินาฟาโซ");
		location181.setLocationType("F");
		location181.setZoneCode("OT");
		this.locationRepository.save(location181);

		Location location182 = new Location();
		location182.setCode("0191");
		location182.setDescription("เบนิน");
		location182.setLocationType("F");
		location182.setZoneCode("OT");
		this.locationRepository.save(location182);

		Location location183 = new Location();
		location183.setCode("0192");
		location183.setDescription("มอริเชียส");
		location183.setLocationType("F");
		location183.setZoneCode("OT");
		this.locationRepository.save(location183);

		Location location184 = new Location();
		location184.setCode("0193");
		location184.setDescription("มอริเตเนีย");
		location184.setLocationType("F");
		location184.setZoneCode("OT");
		this.locationRepository.save(location184);

		Location location185 = new Location();
		location185.setCode("0194");
		location185.setDescription("มาดากัสการ์");
		location185.setLocationType("F");
		location185.setZoneCode("OT");
		this.locationRepository.save(location185);

		Location location186 = new Location();
		location186.setCode("0195");
		location186.setDescription("มาลาวี");
		location186.setLocationType("F");
		location186.setZoneCode("OT");
		this.locationRepository.save(location186);

		Location location187 = new Location();
		location187.setCode("0196");
		location187.setDescription("มาลี");
		location187.setLocationType("F");
		location187.setZoneCode("OT");
		this.locationRepository.save(location187);

		Location location188 = new Location();
		location188.setCode("0197");
		location188.setDescription("โมซัมบิก");
		location188.setLocationType("F");
		location188.setZoneCode("OT");
		this.locationRepository.save(location188);

		Location location189 = new Location();
		location189.setCode("0198");
		location189.setDescription("โมร็อกโก");
		location189.setLocationType("F");
		location189.setZoneCode("OT");
		this.locationRepository.save(location189);

		Location location190 = new Location();
		location190.setCode("0199");
		location190.setDescription("ยูกันดา");
		location190.setLocationType("F");
		location190.setZoneCode("OT");
		this.locationRepository.save(location190);

		Location location191 = new Location();
		location191.setCode("0200");
		location191.setDescription("รวันดา");
		location191.setLocationType("F");
		location191.setZoneCode("OT");
		this.locationRepository.save(location191);

		Location location192 = new Location();
		location192.setCode("0201");
		location192.setDescription("ลิเบีย");
		location192.setLocationType("F");
		location192.setZoneCode("OT");
		this.locationRepository.save(location192);

		Location location193 = new Location();
		location193.setCode("0202");
		location193.setDescription("เลโซโท");
		location193.setLocationType("F");
		location193.setZoneCode("OT");
		this.locationRepository.save(location193);

		Location location194 = new Location();
		location194.setCode("0203");
		location194.setDescription("ไลบีเรีย");
		location194.setLocationType("F");
		location194.setZoneCode("OT");
		this.locationRepository.save(location194);

		Location location195 = new Location();
		location195.setCode("0204");
		location195.setDescription("สวาซิแลนด์");
		location195.setLocationType("F");
		location195.setZoneCode("OT");
		this.locationRepository.save(location195);

		Location location196 = new Location();
		location196.setCode("0205");
		location196.setDescription("สาธารณรัฐคองโก");
		location196.setLocationType("F");
		location196.setZoneCode("OT");
		this.locationRepository.save(location196);

		Location location197 = new Location();
		location197.setCode("0206");
		location197.setDescription("สาธารณรัฐประชาธิปไตยคองโก");
		location197.setLocationType("F");
		location197.setZoneCode("OT");
		this.locationRepository.save(location197);

		Location location198 = new Location();
		location198.setCode("0207");
		location198.setDescription("สาธารณรัฐแอฟริกากลาง");
		location198.setLocationType("F");
		location198.setZoneCode("OT");
		this.locationRepository.save(location198);

		Location location199 = new Location();
		location199.setCode("0208");
		location199.setDescription("อิเควทอเรียลกินี");
		location199.setLocationType("F");
		location199.setZoneCode("OT");
		this.locationRepository.save(location199);

		Location location200 = new Location();
		location200.setCode("0209");
		location200.setDescription("อียิปต์");
		location200.setLocationType("F");
		location200.setZoneCode("OT");
		this.locationRepository.save(location200);

		Location location201 = new Location();
		location201.setCode("0210");
		location201.setDescription("เอธิโอเปีย");
		location201.setLocationType("F");
		location201.setZoneCode("OT");
		this.locationRepository.save(location201);

		Location location202 = new Location();
		location202.setCode("0211");
		location202.setDescription("เอริเทรีย");
		location202.setLocationType("F");
		location202.setZoneCode("OT");
		this.locationRepository.save(location202);


		Location location203 = new Location();
		location203.setCode("0212");
		location203.setDescription("แองโกลา");
		location203.setLocationType("F");
		location203.setZoneCode("OT");
		this.locationRepository.save(location203);


		Location location204 = new Location();
		location204.setCode("0213");
		location204.setDescription("แอฟริกาใต้");
		location204.setLocationType("F");
		location204.setZoneCode("OT");
		this.locationRepository.save(location204);


		Location location205 = new Location();
		location205.setCode("0214");
		location205.setDescription("แอลจีเรีย");
		location205.setLocationType("F");
		location205.setZoneCode("OT");
		this.locationRepository.save(location205);

		/* Location Place Domestic */
		Location locationPD1 = new Location();
		locationPD1.setCode("0031");
		locationPD1.setDescription("สนามบินสุวรรณภูมิ");
		locationPD1.setSpeacialRate("300");
		locationPD1.setLocationType("PD");
		locationPD1.setFlagOther("N");
		locationPD1.setFlagActive(true);
		this.locationRepository.save(locationPD1);

		Location locationPD2 = new Location();
		locationPD2.setCode("0032");
		locationPD2.setDescription("สนามบินดอนเมือง");
		locationPD2.setSpeacialRate("200");
		locationPD2.setLocationType("PD");
		locationPD2.setFlagOther("N");
		locationPD2.setFlagActive(true);
		this.locationRepository.save(locationPD2);

		Location locationPD3 = new Location();
		locationPD3.setCode("0033");
		locationPD3.setDescription("อื่นๆ");
		locationPD3.setSpeacialRate("300");
		locationPD3.setLocationType("PD");
		locationPD3.setFlagOther("N");
		locationPD3.setFlagActive(true);
		this.locationRepository.save(locationPD3);

		/* Location Place Domestic */
		Location locationPF1 = new Location();
		locationPF1.setCode("0031");
		locationPF1.setDescription("สนามบินสุวรรณภูมิ");
		locationPF1.setSpeacialRate("300");
		locationPF1.setLocationType("PF");
		locationPF1.setFlagOther("N");
		locationPF1.setFlagActive(true);
		this.locationRepository.save(locationPF1);

		Location locationPF2 = new Location();
		locationPF2.setCode("0032");
		locationPF2.setDescription("สนามบินดอนเมือง");
		locationPF2.setSpeacialRate("200");
		locationPF2.setLocationType("PF");
		locationPF2.setFlagOther("N");
		locationPF2.setFlagActive(true);
		this.locationRepository.save(locationPF2);

		Location locationPF3 = new Location();
		locationPF3.setCode("0033");
		locationPF3.setDescription("อื่นๆ");
		locationPF3.setSpeacialRate("300");
		locationPF3.setLocationType("PF");
		locationPF3.setFlagOther("N");
		locationPF3.setFlagActive(true);
		this.locationRepository.save(locationPD3);
		
		/* RouteDistance */
		RouteDistance routeDistance1 = new RouteDistance();
		routeDistance1.setLocationFrom(location);
		routeDistance1.setLocationTo(location1);
		routeDistance1.setDistance(200.0);
		routeDistance1.setFlagActive(true);
		routeDistanceProcessCalculateRepository.save(routeDistance1);


		RouteDistance routeDistance2 = new RouteDistance();
		routeDistance2.setLocationFrom(location2);
		routeDistance2.setLocationTo(location3);
		routeDistance2.setDistance(66.0);
		routeDistance2.setFlagActive(true);
		routeDistanceProcessCalculateRepository.save(routeDistance2);

		RouteDistance routeDistance3 = new RouteDistance();
		routeDistance3.setLocationFrom(location4);
		routeDistance3.setLocationTo(location5);
		routeDistance3.setDistance(0.0);
		routeDistance3.setFlagActive(true);
		routeDistanceProcessCalculateRepository.save(routeDistance3);

		RouteDistance routeDistance4 = new RouteDistance();
		routeDistance4.setLocationFrom(location6);
		routeDistance4.setLocationTo(location7);
		routeDistance4.setDistance(1.0);
		routeDistance4.setFlagActive(false);
		routeDistanceProcessCalculateRepository.save(routeDistance4);

		RouteDistance routeDistance5 = new RouteDistance();
		routeDistance5.setLocationFrom(location);
		routeDistance5.setLocationTo(location4);
		routeDistance5.setDistance(2.0);
		routeDistance5.setFlagActive(true);
		routeDistanceProcessCalculateRepository.save(routeDistance5);

		RouteDistance routeDistance6 = new RouteDistance();
		routeDistance6.setLocationFrom(location2);
		routeDistance6.setLocationTo(location);
		routeDistance6.setDistance(184.0);
		routeDistance6.setFlagActive(true);
		routeDistanceProcessCalculateRepository.save(routeDistance6);

		
		/* ConditionIO */
		ConditionalIO conditionalIO_1 = new ConditionalIO();
		conditionalIO_1.setCompany(company);
		conditionalIO_1.setGl("6*");
		conditionalIO_1.setIo("*U");
		conditionalIO_1.setFlagActive(true);
		conditionalIORepository.save(conditionalIO_1);
		
		/* FlowConfigure */
		FlowConfigure flowConfigureAPP001= new FlowConfigure("APP","MPG:DocFlowApprove:0.99-DocFlowApprove.FullApproveAdvance","APP001","Y","Y","Y","N","N","Y",1,2,3,null,null,4);  				flowConfigureRepository.save(flowConfigureAPP001);
		FlowConfigure flowConfigureAPP002= new FlowConfigure("APP","MPG:DocFlowApprove:0.99-DocFlowApprove.FullApproveAdvanceNoFinance","APP002","Y","Y","Y","N","N","N",1,2,3,null,null,null);  	flowConfigureRepository.save(flowConfigureAPP002);
		FlowConfigure flowConfigureAPP003= new FlowConfigure("APP","MPG:DocFlowApprove:0.99-DocFlowApprove.FullApproveNoAdmin","APP003","Y","Y","N","N","N","Y",1,2,null,null,null,3);  			flowConfigureRepository.save(flowConfigureAPP003);
		FlowConfigure flowConfigureAPP004= new FlowConfigure("APP","MPG:DocFlowApprove:0.99-DocFlowApprove.SimpleApprove","APP004","Y","Y","N","N","N","N",1,2,null,null,null,null);  				flowConfigureRepository.save(flowConfigureAPP004);
		FlowConfigure flowConfigureEXP001= new FlowConfigure("EXP","MPG:DocFlowExpense:0.99-DocFlowExpense.FullApproveAdvance","EXP001","Y","Y","Y","Y","Y","Y",1,2,3,4,5,6);  						flowConfigureRepository.save(flowConfigureEXP001);
		FlowConfigure flowConfigureEXP002= new FlowConfigure("EXP","MPG:DocFlowExpense:0.99-DocFlowExpense.FullApproveAdvanceNoAdmin","EXP002","Y","Y","N","Y","Y","Y",1,2,null,3,4,5);  			flowConfigureRepository.save(flowConfigureEXP002);
		FlowConfigure flowConfigureEXP003= new FlowConfigure("EXP","MPG:DocFlowExpense:0.99-DocFlowExpense.FullApproveAdvanceNoHR","EXP003","Y","Y","Y","N","Y","Y",1,2,3,null,4,5);  				flowConfigureRepository.save(flowConfigureEXP003);
		FlowConfigure flowConfigureEXP004= new FlowConfigure("EXP","MPG:DocFlowExpense:0.99-DocFlowExpense.FullApproveAdvanceNoHRAdmin","EXP004","Y","Y","N","N","Y","Y",1,2,null,null,3,4);  		flowConfigureRepository.save(flowConfigureEXP004);
		FlowConfigure flowConfigureEXP005= new FlowConfigure("EXP","MPG:DocFlowExpense:0.99-DocFlowExpense.FullApproveAdvanceNoAccount","EXP005","Y","Y","N","N","N","Y",1,2,null,null,null,3);  	flowConfigureRepository.save(flowConfigureEXP005);


		/* PetrolAllowancePerMonthHistory */
		PetrolAllowancePerMonthHistory petrolAllowancePerMonthHistory = new PetrolAllowancePerMonthHistory();
		petrolAllowancePerMonthHistory.setEmpCode("00003");
		petrolAllowancePerMonthHistory.setDetail("เบิกประจำเดือน");
		petrolAllowancePerMonthHistory.setMonth("09");
		petrolAllowancePerMonthHistory.setYear("2017");
		petrolAllowancePerMonthHistory.setLiterNumber(40.0);
		petrolAllowancePerMonthHistory.setBirthDate(Timestamp.valueOf("2017-09-30 10:24:31"));
		petrolAllowancePerMonthHistoryRepository.save(petrolAllowancePerMonthHistory);

		PetrolAllowancePerMonthHistory petrolAllowancePerMonthHistory2 = new PetrolAllowancePerMonthHistory();
		petrolAllowancePerMonthHistory2.setEmpCode("00003");
		petrolAllowancePerMonthHistory2.setDetail("เบิกประจำเดือน");
		petrolAllowancePerMonthHistory2.setMonth("08");
		petrolAllowancePerMonthHistory2.setYear("2017");
		petrolAllowancePerMonthHistory2.setLiterNumber(20.0);
		petrolAllowancePerMonthHistory2.setBirthDate(Timestamp.valueOf("2017-08-31 11:24:31"));
		petrolAllowancePerMonthHistoryRepository.save(petrolAllowancePerMonthHistory2);

		/* DieselAllowancePerMonthHistory */
		DieselAllowancePerMonthHistory dieselAllowancePerMonthHistory = new DieselAllowancePerMonthHistory();
		dieselAllowancePerMonthHistory.setEmpCode("00003");
		dieselAllowancePerMonthHistory.setDetail("เบิกประจำเดือน");
		dieselAllowancePerMonthHistory.setMonth("09");
		dieselAllowancePerMonthHistory.setYear("2017");
		dieselAllowancePerMonthHistory.setLiterNumber(50.0);
		dieselAllowancePerMonthHistory.setBirthDate(Timestamp.valueOf("2017-09-30 10:24:31"));
		dieselAllowancePerMonthHistoryRepository.save(dieselAllowancePerMonthHistory);

		DieselAllowancePerMonthHistory dieselAllowancePerMonthHistory2 = new DieselAllowancePerMonthHistory();
		dieselAllowancePerMonthHistory2.setEmpCode("00003");
		dieselAllowancePerMonthHistory2.setDetail("เบิกประจำเดือน");
		dieselAllowancePerMonthHistory2.setMonth("08");
		dieselAllowancePerMonthHistory2.setYear("2017");
		dieselAllowancePerMonthHistory2.setLiterNumber(40.0);
		dieselAllowancePerMonthHistory2.setBirthDate(Timestamp.valueOf("2017-08-31 11:24:31"));
		dieselAllowancePerMonthHistoryRepository.save(dieselAllowancePerMonthHistory2);

		/* MonthlyPhoneBillHistory */
        MonthlyPhoneBillHistory monthlyPhoneBillHistory = new MonthlyPhoneBillHistory();
        monthlyPhoneBillHistory.setEmpCode("00003");
        monthlyPhoneBillHistory.setDetail("เบิกประจำเดือน");
        monthlyPhoneBillHistory.setPhoneNumber("09877876656");
        monthlyPhoneBillHistory.setMonth("09");
        monthlyPhoneBillHistory.setYear("2017");
        monthlyPhoneBillHistory.setAmount(1500.0);
		monthlyPhoneBillHistory.setBirthDate(Timestamp.valueOf("2017-09-30 10:24:31"));
		monthlyPhoneBillHistoryRepository.save(monthlyPhoneBillHistory);

		MonthlyPhoneBillHistory monthlyPhoneBillHistory2 = new MonthlyPhoneBillHistory();
		monthlyPhoneBillHistory2.setEmpCode("00003");
		monthlyPhoneBillHistory2.setDetail("เบิกประจำเดือน");
		monthlyPhoneBillHistory2.setPhoneNumber("09877876656");
		monthlyPhoneBillHistory2.setMonth("08");
		monthlyPhoneBillHistory2.setYear("2017");
		monthlyPhoneBillHistory2.setAmount(1400.0);
		monthlyPhoneBillHistory2.setBirthDate(Timestamp.valueOf("2017-08-31 11:24:31"));
		monthlyPhoneBillHistoryRepository.save(monthlyPhoneBillHistory2);


		/* Employee Replacement */
//		EmployeeReplacement employeeReplacement = new EmployeeReplacement();
//		employeeReplacement.setEmpCodeReplace("5622364");
//		employeeReplacement.setEmpUserNameReplace("karoons");
//		employeeReplacement.setEmpCodeAgree("5617230");
//		employeeReplacement.setEmpUserNameAgree("jaruneet");
//		employeeReplacementRepository.save(employeeReplacement);
//
//
//		EmployeeReplacement employeeReplacement2 = new EmployeeReplacement();
//		employeeReplacement2.setEmpCodeReplace("5622364");
//		employeeReplacement2.setEmpUserNameReplace("karoons");
//		employeeReplacement2.setEmpCodeAgree("5619382");
//		employeeReplacement2.setEmpUserNameAgree("nattapornt");
//		employeeReplacementRepository.save(employeeReplacement2);


//
//		Delegate delegate1 = new Delegate();
//		delegate1.setEmpCodeAssignee("00001");
//		delegate1.setEmpCodeAssigner("00002");
//		delegate1.setFlowType("F001");
//		delegate1.setAmount(Double.valueOf("9000"));
//
//		delegateRepository.save(delegate1);
//
//
//		Delegate delegate2 = new Delegate();
//		delegate2.setEmpCodeAssignee("00001");
//		delegate2.setEmpCodeAssigner("00003");
//		delegate2.setFlowType("F001");
//		delegate2.setAmount(Double.valueOf("90000"));
//
//		delegateRepository.save(delegate2);
//
//
//
//		Delegate delegate3 = new Delegate();
//		delegate3.setEmpCodeAssignee("00001");
//		delegate3.setEmpCodeAssigner("00004");
//		delegate3.setFlowType("F001");
//		delegate3.setAmount(Double.valueOf("85000"));
//
//		delegateRepository.save(delegate3);
//
//
//
//
//
//		Delegate delegate4 = new Delegate();
//		delegate4.setEmpCodeAssignee("00002");
//		delegate4.setEmpCodeAssigner("00001");
//		delegate4.setFlowType("F002");
//		delegate4.setAmount(Double.valueOf("110010"));
//
//		delegateRepository.save(delegate4);
//
//
//		Delegate delegate5 = new Delegate();
//		delegate5.setEmpCodeAssignee("00002");
//		delegate5.setEmpCodeAssigner("00003");
//		delegate5.setFlowType("F002");
//		delegate5.setAmount(Double.valueOf("45600"));
//
//		delegateRepository.save(delegate5);
//
//
//
//		Delegate delegate6 = new Delegate();cat
//		delegate6.setEmpCodeAssignee("00002");
//		delegate6.setEmpCodeAssigner("00004");
//		delegate6.setFlowType("F002");
//		delegate6.setAmount(Double.valueOf("999999"));
//
//		delegateRepository.save(delegate6);

		/** VAT **/
		Vat vat1 = new Vat();
		vat1.setCode("V0");
		vat1.setDescription("MPSC:Purchase tax rate 0%");
		vat1.setValue(0d);
		vatRepository.save(vat1);

		Vat vat2 = new Vat();
		vat2.setCode("V1");
		vat2.setDescription("Purchase tax rate 10%");
		vat2.setValue(10d);
		vatRepository.save(vat2);

		Vat vat3 = new Vat();
		vat3.setCode("V7");
		vat3.setDescription("MPSC:Purchase tax rate 7%");
		vat3.setValue(7d);
		vatRepository.save(vat3);

		Vat vat4 = new Vat();
		vat4.setCode("VA");
		vat4.setDescription("MPSC:Purchase Tax rate (Avg. Rate)");
		vat4.setValue(7d);
		vatRepository.save(vat4);

		Vat vat5 = new Vat();
		vat5.setCode("VX");
		vat5.setDescription("MPSC:Purchase Tax rate 0% (non tax relevant)");
		vat5.setValue(7d);
		vatRepository.save(vat5);

		Vat vat6 = new Vat();
		vat6.setCode("D7");
		vat6.setDescription("D7");
		vat6.setValue(7d);
		vatRepository.save(vat6);

		/** WITHHOLDING TAX **/
		WithholdingTax withholdingTax1 = new WithholdingTax();
		withholdingTax1.setCode("01");
		withholdingTax1.setDescription("at Inv.& No certificate numbering");
		withholdingTaxRepository.save(withholdingTax1);

		WithholdingTax withholdingTax2 = new WithholdingTax();
		withholdingTax2.setCode("02");
		withholdingTax2.setDescription("at Inv.& No cert.no.(onecycle grossup)");
		withholdingTaxRepository.save(withholdingTax2);

		WithholdingTax withholdingTax3 = new WithholdingTax();
		withholdingTax3.setCode("11");
		withholdingTax3.setDescription("at Paymnt.&No cert.numbering");
		withholdingTaxRepository.save(withholdingTax3);

		WithholdingTax withholdingTax4 = new WithholdingTax();
		withholdingTax4.setCode("12");
		withholdingTax4.setDescription("at Paymnt.&No cert.no.(onecycle grossup)");
		withholdingTaxRepository.save(withholdingTax4);

		WithholdingTax withholdingTax5 = new WithholdingTax();
		withholdingTax5.setCode("I1");
		withholdingTax5.setDescription("หัก ณ ตอนตั้งหนี้ สำหรับ ภงด 01");
		withholdingTaxRepository.save(withholdingTax5);

		WithholdingTax withholdingTax6 = new WithholdingTax();
		withholdingTax6.setCode("I2");
		withholdingTax6.setDescription("หัก ณ ตอนตั้งหนี้ สำหรับ ภงด 02");
		withholdingTaxRepository.save(withholdingTax6);

		WithholdingTax withholdingTax7 = new WithholdingTax();
		withholdingTax7.setCode("I3");
		withholdingTax7.setDescription("หัก ณ ตอนตั้งหนี้ สำหรับ ภงด 03");
		withholdingTaxRepository.save(withholdingTax7);

		WithholdingTax withholdingTax8 = new WithholdingTax();
		withholdingTax8.setCode("I4");
		withholdingTax8.setDescription("หัก ณ ตอนตั้งหนี้ สำหรับ ภงด 53");
		withholdingTaxRepository.save(withholdingTax8);

		WithholdingTax withholdingTax9 = new WithholdingTax();
		withholdingTax9.setCode("I5");
		withholdingTax9.setDescription("หัก ณ ตอนตั้งหนี้ สำหรับ ภงด 54");
		withholdingTaxRepository.save(withholdingTax9);

		WithholdingTax withholdingTax10 = new WithholdingTax();
		withholdingTax10.setCode("P0");
		withholdingTax10.setDescription("หัก ณ ตอนจ่าย สำหรับ ภงด 02 อัตราที่ 3");
		withholdingTaxRepository.save(withholdingTax10);

		WithholdingTax withholdingTax11 = new WithholdingTax();
		withholdingTax11.setCode("P1");
		withholdingTax11.setDescription("หัก ณ ตอนจ่าย สำหรับ ภงด 01 อัตราที่ 1");
		withholdingTaxRepository.save(withholdingTax11);

		WithholdingTax withholdingTax12 = new WithholdingTax();
		withholdingTax12.setCode("P2");
		withholdingTax12.setDescription("หัก ณ ตอนจ่าย สำหรับ ภงด 02 อัตราที่ 1");
		withholdingTaxRepository.save(withholdingTax12);

		WithholdingTax withholdingTax13 = new WithholdingTax();
		withholdingTax13.setCode("P3");
		withholdingTax13.setDescription("หัก ณ ตอนจ่าย สำหรับ ภงด 03 อัตราที่ 1");
		withholdingTaxRepository.save(withholdingTax13);

		WithholdingTax withholdingTax14 = new WithholdingTax();
		withholdingTax14.setCode("P4");
		withholdingTax14.setDescription("หัก ณ ตอนจ่าย สำหรับ ภงด 53 อัตราที่ 1");
		withholdingTaxRepository.save(withholdingTax14);

		WithholdingTax withholdingTax15 = new WithholdingTax();
		withholdingTax15.setCode("P5");
		withholdingTax15.setDescription("หัก ณ ตอนจ่าย สำหรับ ภงด 54 อัตราที่ 1");
		withholdingTaxRepository.save(withholdingTax15);

		WithholdingTax withholdingTax16 = new WithholdingTax();
		withholdingTax16.setCode("P6");
		withholdingTax16.setDescription("หัก ณ ตอนจ่าย สำหรับ ภงด 02 อัตราที่ 2");
		withholdingTaxRepository.save(withholdingTax16);

		WithholdingTax withholdingTax17 = new WithholdingTax();
		withholdingTax17.setCode("P7");
		withholdingTax17.setDescription("หัก ณ ตอนจ่าย สำหรับ ภงด 03 อัตราที่ 2");
		withholdingTaxRepository.save(withholdingTax17);

		WithholdingTax withholdingTax18 = new WithholdingTax();
		withholdingTax18.setCode("P8");
		withholdingTax18.setDescription("หัก ณ ตอนจ่าย สำหรับ ภงด 53 อัตราที่ 2");
		withholdingTaxRepository.save(withholdingTax18);

		WithholdingTax withholdingTax19 = new WithholdingTax();
		withholdingTax19.setCode("P9");
		withholdingTax19.setDescription("หัก ณ ตอนจ่าย สำหรับ ภงด 54 อัตราที่ 2");
		withholdingTaxRepository.save(withholdingTax19);

		WithholdingTax withholdingTax20 = new WithholdingTax();
		withholdingTax20.setCode("PA");
		withholdingTax20.setDescription("หัก ณ ตอนจ่าย สำหรับ ภงด 53 อัตราที่ 3");
		withholdingTaxRepository.save(withholdingTax20);

		WithholdingTax withholdingTax21 = new WithholdingTax();
		withholdingTax21.setCode("R1");
		withholdingTax21.setDescription("ถูกหัก ณ ตอนจ่าย AR(03)");
		withholdingTaxRepository.save(withholdingTax21);

		WithholdingTax withholdingTax22 = new WithholdingTax();
		withholdingTax22.setCode("R2");
		withholdingTax22.setDescription("ถูกหัก ณ ตอนจ่าย AR(53) อัตราที่ 1");
		withholdingTaxRepository.save(withholdingTax22);

		WithholdingTax withholdingTax23 = new WithholdingTax();
		withholdingTax23.setCode("R3");
		withholdingTax23.setDescription("ถูกหัก ณ ตอนจ่าย AR(53) อัตราที่ 2");
		withholdingTaxRepository.save(withholdingTax23);

		WithholdingTax withholdingTax24 = new WithholdingTax();
		withholdingTax24.setCode("R4");
		withholdingTax24.setDescription("ถูกหัก ณ ตอนจ่าย AR(53) อัตราที่ 3");
		withholdingTaxRepository.save(withholdingTax24);

		WithholdingTax withholdingTax25 = new WithholdingTax();
		withholdingTax25.setCode("R5");
		withholdingTax25.setDescription("ถูกหัก ณ ตอนจ่าย AR(53) อัตราที่ 4");
		withholdingTaxRepository.save(withholdingTax24);

		/** WITHHOLDING TAX CODE **/
		WithholdingTaxCode withholdingTaxCode1 = new WithholdingTaxCode();
		withholdingTaxCode1.setCode("01");
		withholdingTaxCode1.setDescription("ค่าเบี้ยประชุม (5%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode1);

		WithholdingTaxCode withholdingTaxCode2 = new WithholdingTaxCode();
		withholdingTaxCode2.setCode("02");
		withholdingTaxCode2.setDescription("ค่าธรรมเนียม (3%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode2);

		WithholdingTaxCode withholdingTaxCode3 = new WithholdingTaxCode();
		withholdingTaxCode3.setCode("03");
		withholdingTaxCode3.setDescription("ค่าธรรมเนียม (10%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode3);

		WithholdingTaxCode withholdingTaxCode4 = new WithholdingTaxCode();
		withholdingTaxCode4.setCode("04");
		withholdingTaxCode4.setDescription("ค่าธรรมเนียม (15%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode4);

		WithholdingTaxCode withholdingTaxCode5 = new WithholdingTaxCode();
		withholdingTaxCode5.setCode("05");
		withholdingTaxCode5.setDescription("ค่านายหน้า (3%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode5);

		WithholdingTaxCode withholdingTaxCode6 = new WithholdingTaxCode();
		withholdingTaxCode6.setCode("06");
		withholdingTaxCode6.setDescription("ค่านายหน้า (5%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode1);

		WithholdingTaxCode withholdingTaxCode7 = new WithholdingTaxCode();
		withholdingTaxCode7.setCode("07");
		withholdingTaxCode7.setDescription("ค่านายหน้า (10%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode7);

		WithholdingTaxCode withholdingTaxCode8 = new WithholdingTaxCode();
		withholdingTaxCode8.setCode("08");
		withholdingTaxCode8.setDescription("ค่านายหน้า (15%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode8);

		WithholdingTaxCode withholdingTaxCode9 = new WithholdingTaxCode();
		withholdingTaxCode9.setCode("09");
		withholdingTaxCode9.setDescription("ค่าแห่งกู๊ดวิลล์ (3%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode9);

		WithholdingTaxCode withholdingTaxCode10 = new WithholdingTaxCode();
		withholdingTaxCode10.setCode("10");
		withholdingTaxCode10.setDescription("ค่าแห่งกู๊ดวิลล์ (10%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode10);

		WithholdingTaxCode withholdingTaxCode11 = new WithholdingTaxCode();
		withholdingTaxCode11.setCode("11");
		withholdingTaxCode11.setDescription("ค่าแห่งกู๊ดวิลล์ (15%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode11);

		WithholdingTaxCode withholdingTaxCode12 = new WithholdingTaxCode();
		withholdingTaxCode12.setCode("12");
		withholdingTaxCode12.setDescription("ค่าลิขสิทธิ์ (3%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode12);

		WithholdingTaxCode withholdingTaxCode13 = new WithholdingTaxCode();
		withholdingTaxCode13.setCode("13");
		withholdingTaxCode13.setDescription("ค่าลิขสิทธิ์ (10%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode13);

		WithholdingTaxCode withholdingTaxCode14 = new WithholdingTaxCode();
		withholdingTaxCode14.setCode("14");
		withholdingTaxCode14.setDescription("ค่าลิขสิทธิ์ (15%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode14);

		WithholdingTaxCode withholdingTaxCode15 = new WithholdingTaxCode();
		withholdingTaxCode15.setCode("15");
		withholdingTaxCode15.setDescription("ค่าดอกเบี้ยจ่าย (1%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode15);

		WithholdingTaxCode withholdingTaxCode16 = new WithholdingTaxCode();
		withholdingTaxCode16.setCode("16");
		withholdingTaxCode16.setDescription("ค่าดอกเบี้ยจ่าย (10%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode16);

		WithholdingTaxCode withholdingTaxCode17 = new WithholdingTaxCode();
		withholdingTaxCode17.setCode("17");
		withholdingTaxCode17.setDescription("ค่าดอกเบี้ยจ่าย (15%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode17);

		WithholdingTaxCode withholdingTaxCode18 = new WithholdingTaxCode();
		withholdingTaxCode18.setCode("18");
		withholdingTaxCode18.setDescription("เงินปันผล (10%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode18);

		WithholdingTaxCode withholdingTaxCode19 = new WithholdingTaxCode();
		withholdingTaxCode19.setCode("19");
		withholdingTaxCode19.setDescription("เงินส่วนแบ่งของกำไร (10%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode19);

		WithholdingTaxCode withholdingTaxCode20 = new WithholdingTaxCode();
		withholdingTaxCode20.setCode("20");
		withholdingTaxCode20.setDescription("ค่าเช่า (5%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode20);

		WithholdingTaxCode withholdingTaxCode21 = new WithholdingTaxCode();
		withholdingTaxCode21.setCode("21");
		withholdingTaxCode21.setDescription("ค่าเช่า (10%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode21);

		WithholdingTaxCode withholdingTaxCode22 = new WithholdingTaxCode();
		withholdingTaxCode22.setCode("22");
		withholdingTaxCode22.setDescription("ค่าเช่า (15%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode22);

		WithholdingTaxCode withholdingTaxCode23 = new WithholdingTaxCode();
		withholdingTaxCode23.setCode("23");
		withholdingTaxCode23.setDescription("ค่าเช่าเรือขนส่งระหว่างประเทศ (1%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode23);

		WithholdingTaxCode withholdingTaxCode24 = new WithholdingTaxCode();
		withholdingTaxCode24.setCode("24");
		withholdingTaxCode24.setDescription("ค่าวิชาชีพอิสระ (3%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode24);

		WithholdingTaxCode withholdingTaxCode25 = new WithholdingTaxCode();
		withholdingTaxCode25.setCode("25");
		withholdingTaxCode25.setDescription("ค่าวิชาชีพอิสระ (10%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode25);

		WithholdingTaxCode withholdingTaxCode26 = new WithholdingTaxCode();
		withholdingTaxCode26.setCode("26");
		withholdingTaxCode26.setDescription("ค่าวิชาชีพอิสระ (15%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode26);

		WithholdingTaxCode withholdingTaxCode27 = new WithholdingTaxCode();
		withholdingTaxCode27.setCode("27");
		withholdingTaxCode27.setDescription("ค่าจ้างทำของ (3%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode27);

		WithholdingTaxCode withholdingTaxCode28 = new WithholdingTaxCode();
		withholdingTaxCode28.setCode("28");
		withholdingTaxCode28.setDescription("ค่าจ้างทำของ (5%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode28);

		WithholdingTaxCode withholdingTaxCode29 = new WithholdingTaxCode();
		withholdingTaxCode29.setCode("30");
		withholdingTaxCode29.setDescription("ค่าบริการ (3%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode29);

		WithholdingTaxCode withholdingTaxCode30 = new WithholdingTaxCode();
		withholdingTaxCode30.setCode("31");
		withholdingTaxCode30.setDescription("ค่าส่งเสริมการขาย (3%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode30);

		WithholdingTaxCode withholdingTaxCode31 = new WithholdingTaxCode();
		withholdingTaxCode31.setCode("32");
		withholdingTaxCode31.setDescription("การชิงโชค (5%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode31);

		WithholdingTaxCode withholdingTaxCode32 = new WithholdingTaxCode();
		withholdingTaxCode32.setCode("33");
		withholdingTaxCode32.setDescription("เงินรางวัล (5%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode32);

		WithholdingTaxCode withholdingTaxCode33 = new WithholdingTaxCode();
		withholdingTaxCode33.setCode("34");
		withholdingTaxCode33.setDescription("นักแสดงสาธารณะ (5%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode33);

		WithholdingTaxCode withholdingTaxCode34 = new WithholdingTaxCode();
		withholdingTaxCode34.setCode("35");
		withholdingTaxCode34.setDescription("ค่าโฆษณา (2%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode34);

		WithholdingTaxCode withholdingTaxCode35 = new WithholdingTaxCode();
		withholdingTaxCode35.setCode("36");
		withholdingTaxCode35.setDescription("ค่าเบี้ยประกันวินาศภัย (1%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode35);

		WithholdingTaxCode withholdingTaxCode36 = new WithholdingTaxCode();
		withholdingTaxCode36.setCode("37");
		withholdingTaxCode36.setDescription("ค่าขนส่ง ไม่รวมค่าโดยสารขนส่งสาธารณะ (1%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode36);

		WithholdingTaxCode withholdingTaxCode37 = new WithholdingTaxCode();
		withholdingTaxCode37.setCode("38");
		withholdingTaxCode37.setDescription("ค่าสินค้าพืชผลทางการเกษตร (0.75%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode37);

		WithholdingTaxCode withholdingTaxCode38 = new WithholdingTaxCode();
		withholdingTaxCode38.setCode("39");
		withholdingTaxCode38.setDescription("ส่วนลด (3%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode38);

		WithholdingTaxCode withholdingTaxCode39 = new WithholdingTaxCode();
		withholdingTaxCode39.setCode("40");
		withholdingTaxCode39.setDescription("ส่วนลด (10%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode39);

		WithholdingTaxCode withholdingTaxCode40 = new WithholdingTaxCode();
		withholdingTaxCode40.setCode("41");
		withholdingTaxCode40.setDescription("ส่วนลด (15%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode40);

		WithholdingTaxCode withholdingTaxCode41 = new WithholdingTaxCode();
		withholdingTaxCode41.setCode("42");
		withholdingTaxCode41.setDescription("อื่นๆ");
		witholdngTaxCodeRepository.save(withholdingTaxCode41);

		WithholdingTaxCode withholdingTaxCode42 = new WithholdingTaxCode();
		withholdingTaxCode42.setCode("53");
		withholdingTaxCode42.setDescription("ค่าเบี้ยประชุม (10%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode42);

		WithholdingTaxCode withholdingTaxCode43 = new WithholdingTaxCode();
		withholdingTaxCode43.setCode("54");
		withholdingTaxCode43.setDescription("ค่าเบี้ยประชุม (30%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode43);

		WithholdingTaxCode withholdingTaxCode44 = new WithholdingTaxCode();
		withholdingTaxCode44.setCode("55");
		withholdingTaxCode44.setDescription("ค่าฝึกอบรมสัมมนา (3%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode44);

		WithholdingTaxCode withholdingTaxCode45 = new WithholdingTaxCode();
		withholdingTaxCode45.setCode("56");
		withholdingTaxCode45.setDescription("ค่าวิทยากร (5%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode45);

		WithholdingTaxCode withholdingTaxCode46 = new WithholdingTaxCode();
		withholdingTaxCode46.setCode("57");
		withholdingTaxCode46.setDescription("เงินปันผล (10%) อัตราร้อยละ 23");
		witholdngTaxCodeRepository.save(withholdingTaxCode46);

		WithholdingTaxCode withholdingTaxCode47 = new WithholdingTaxCode();
		withholdingTaxCode47.setCode("58");
		withholdingTaxCode47.setDescription("เงินส่วนแบ่งของกำไร (10%) อัตราร้อยละ 23");
		witholdngTaxCodeRepository.save(withholdingTaxCode47);

		WithholdingTaxCode withholdingTaxCode48 = new WithholdingTaxCode();
		withholdingTaxCode48.setCode("60");
		withholdingTaxCode48.setDescription("ค่าเบี้ยประชุม (1%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode48);

		WithholdingTaxCode withholdingTaxCode49 = new WithholdingTaxCode();
		withholdingTaxCode49.setCode("61");
		withholdingTaxCode49.setDescription("ค่าเบี้ยประชุม (20%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode49);

		WithholdingTaxCode withholdingTaxCode50 = new WithholdingTaxCode();
		withholdingTaxCode50.setCode("62");
		withholdingTaxCode50.setDescription("ค่าเบี้ยประชุม (37%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode50);

		WithholdingTaxCode withholdingTaxCode51 = new WithholdingTaxCode();
		withholdingTaxCode51.setCode("63");
		withholdingTaxCode51.setDescription("ค่าบริการต่างประเทศ (15%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode51);

		WithholdingTaxCode withholdingTaxCode52 = new WithholdingTaxCode();
		withholdingTaxCode52.setCode("64");
		withholdingTaxCode52.setDescription("ค่าเบี้ยประชุม (0%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode52);

		WithholdingTaxCode withholdingTaxCode53 = new WithholdingTaxCode();
		withholdingTaxCode53.setCode("65");
		withholdingTaxCode53.setDescription("ค่าเก็บรักษา (3%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode53);

		WithholdingTaxCode withholdingTaxCode54 = new WithholdingTaxCode();
		withholdingTaxCode54.setCode("66");
		withholdingTaxCode54.setDescription("ค่าขนส่ง (3%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode54);

		WithholdingTaxCode withholdingTaxCode55 = new WithholdingTaxCode();
		withholdingTaxCode55.setCode("67");
		withholdingTaxCode55.setDescription("ค่าขนถ่าย (3%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode55);

		WithholdingTaxCode withholdingTaxCode56 = new WithholdingTaxCode();
		withholdingTaxCode56.setCode("68");
		withholdingTaxCode56.setDescription("ค่าเบี้เลี้ยง (0%)");
		witholdngTaxCodeRepository.save(withholdingTaxCode56);

		CurrencyRate currencyRate1 = new CurrencyRate();
		currencyRate1.setRateBuy(33.12d);
		currencyRate1.setRateSell(33.12d);
		currencyRate1.setRateTranfer(33.12d);
		currencyRate1.setDateAction(new Timestamp(new Date().getTime()));
		currencyRateRepository.save(currencyRate1);
	}

}