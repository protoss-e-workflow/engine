package com.spt.engine.database;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.*;

import com.spt.engine.entity.general.*;
import com.spt.engine.repository.general.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import com.spt.engine.entity.app.Company;
import com.spt.engine.entity.app.CostCenter;
import com.spt.engine.entity.app.Department;
import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentApprove;
import com.spt.engine.entity.app.DocumentApproveItem;
import com.spt.engine.entity.app.Location;
import com.spt.engine.entity.app.TravelDetail;
import com.spt.engine.entity.general.LocaleMessage;
import com.spt.engine.entity.general.MasterData;
import com.spt.engine.entity.general.MasterDataDetail;
import com.spt.engine.entity.general.Menu;
import com.spt.engine.entity.general.Parameter;
import com.spt.engine.entity.general.ParameterDetail;
import com.spt.engine.entity.general.ReportConfiguration;
import com.spt.engine.entity.general.ReportCriteria;
import com.spt.engine.entity.general.ReportProjection;
import com.spt.engine.entity.general.Role;
import com.spt.engine.entity.general.RunningType;
import com.spt.engine.entity.general.User;
import com.spt.engine.repository.app.CompanyRepository;
import com.spt.engine.repository.app.CostCenterRepository;
import com.spt.engine.repository.app.DepartmentRepository;
import com.spt.engine.repository.app.DocumentRepository;
import com.spt.engine.repository.app.LocationRepository;
import com.spt.engine.repository.app.TravelDetailRepository;
import com.spt.engine.repository.general.LocaleMessageRepository;
import com.spt.engine.repository.general.MasterDataDetailRepository;
import com.spt.engine.repository.general.MasterDataRepository;
import com.spt.engine.repository.general.MenuRepository;
import com.spt.engine.repository.general.ParameterDetailRepository;
import com.spt.engine.repository.general.ParameterRepository;
import com.spt.engine.repository.general.ReportConfigurationRepository;
import com.spt.engine.repository.general.ReportCriteriaRepository;
import com.spt.engine.repository.general.ReportProjectionRepository;
import com.spt.engine.repository.general.RunningTypeRepository;
import com.spt.engine.repository.general.UserRepository;
import com.spt.engine.repository.app.CompanyRepository;
import com.spt.engine.repository.app.CostCenterRepository;
import com.spt.engine.repository.app.DepartmentRepository;
import org.springframework.web.client.RestTemplate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;


//@Component
public class ParameterLoader implements CommandLineRunner {

	private final ParameterRepository parameterRepository;
	private final ParameterDetailRepository parameterDetailRepository;

	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
		public Date deserialize(JsonElement json, Type typeOfT,
								JsonDeserializationContext context) throws JsonParseException {
			return json == null ? null : new Date(json.getAsLong());
		}
	};

	protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

	@Autowired
	public ParameterLoader(ParameterRepository parameterRepository,
			ParameterDetailRepository parameterDetailRepository
			) {

		this.parameterRepository = parameterRepository;
		this.parameterDetailRepository = parameterDetailRepository;
	}
	
	@Value("${OrgSysServer}")
	public  String OrgSysServer ;

	@Override
	public void run(String... strings) throws Exception {
		
		/* Setup Parameter */
		/* P100 : Path Image */
		Parameter parameter100 = new Parameter();
		parameter100.setCode("P100");
		parameter100.setName("Path Image");
		parameter100.setDescription("Path of Image");
		parameter100.setScreenType("1");
		parameter100.setVariableName1("PATH");
		parameter100.setNumberOfColumn(1);
		
		ParameterDetail parameterDetail100 = new ParameterDetail();
		parameterDetail100.setCode("100");
		parameterDetail100.setVariable1("/tmp/image/profile");
		
		parameter100.setDetails(new HashSet<ParameterDetail>(){{
            add(parameterDetail100);
        }});
		
		parameterRepository.save(parameter100);
		
		parameterDetail100.setParameter(parameter100);
		parameterDetailRepository.save(parameterDetail100);
		

		/* P101 : Path Image */
		Parameter parameter101 = new Parameter();
		parameter101.setCode("P101");
		parameter101.setName("Locale");
		parameter101.setDescription("Locale Language");
		parameter101.setScreenType("2");
		parameter101.setVariableName1("Lanuage");
		parameter101.setVariableName2("Short Name");
		parameter101.setVariableName3("Short Name lower");
		parameter101.setNumberOfColumn(3);
		
		ParameterDetail parameterDetail101EN = new ParameterDetail();
		parameterDetail101EN.setCode("EN");
		parameterDetail101EN.setVariable1("English");
		parameterDetail101EN.setVariable2("EN");
		parameterDetail101EN.setVariable3("en");
		

		ParameterDetail parameterDetail101TH = new ParameterDetail();
		parameterDetail101TH.setCode("TH");
		parameterDetail101TH.setVariable1("Thai");
		parameterDetail101TH.setVariable2("TH");
		parameterDetail101TH.setVariable3("th");
		
		parameter101.setDetails(new HashSet<ParameterDetail>(){{
            add(parameterDetail101EN);
            add(parameterDetail101TH);
        }});
		
		parameterRepository.save(parameter101);
		

		parameterDetail101EN.setParameter(parameter101);
		parameterDetailRepository.save(parameterDetail101EN);
		

		parameterDetail101TH.setParameter(parameter101);
		parameterDetailRepository.save(parameterDetail101TH);
		
		/* P102 : Path File Upload */
		Parameter parameter102 = new Parameter();
		parameter102.setCode("P103");
		parameter102.setName("Path File Upload");
		parameter102.setDescription("Path of Attachment");
		parameter102.setScreenType("1");
		parameter102.setVariableName1("PATH");
		parameter102.setNumberOfColumn(1);
		
		ParameterDetail parameterDetail102 = new ParameterDetail();
		parameterDetail102.setCode("100");
		parameterDetail102.setVariable1("/tmp/image/attachment/");
		
		parameter102.setDetails(new HashSet<ParameterDetail>(){{
            add(parameterDetail102);
        }});
		
		parameterRepository.save(parameter102);
		

		parameterDetail102.setParameter(parameter102);
		parameterDetailRepository.save(parameterDetail102);
		
		
		/* P202 : Long-Term Amount */
		Parameter parameter200 = new Parameter();
		parameter200.setCode("P200");
		parameter200.setName("Long-Term Amount");
		parameter200.setDescription("Long-Term Amount");
		parameter200.setScreenType("1");
		parameter200.setVariableName1("LONG TERM AMOUNT");
		parameter200.setNumberOfColumn(1);
		
		ParameterDetail parameterDetail200 = new ParameterDetail();
		parameterDetail200.setCode("P200");
		parameterDetail200.setVariable1("150");
		
		parameter200.setDetails(new HashSet<ParameterDetail>(){{
            add(parameterDetail200);
        }});
		
		parameterRepository.save(parameter200);
		
		parameterDetail200.setParameter(parameter200);
		parameterDetailRepository.save(parameterDetail200);
		
		/* P202 : Long-Term Amount */
		Parameter parameter201 = new Parameter();
		parameter201.setCode("P201");
		parameter201.setName("Nontax");
		parameter201.setDescription("Nontax Rate");
		parameter201.setScreenType("1");
		parameter201.setVariableName1("NONTAX");
		parameter201.setNumberOfColumn(1);
		
		ParameterDetail parameterDetail201 = new ParameterDetail();
		parameterDetail201.setCode("P201");
		parameterDetail201.setVariable1("240");
		
		parameter201.setDetails(new HashSet<ParameterDetail>(){{
            add(parameterDetail201);
        }});
		
		parameterRepository.save(parameter201);
		
		parameterDetail201.setParameter(parameter201);
		parameterDetailRepository.save(parameterDetail201);

		/* P104 : Confix advance duedate by PA */
		Parameter parameter104 = new Parameter();
		parameter104.setCode("P104");
		parameter104.setName("Configuration Advance DueDate");
		parameter104.setDescription("Configuration Advance DueDate");
		parameter104.setScreenType("1");
		parameter104.setVariableName1("Number of day");
		parameter104.setNumberOfColumn(1);



		ParameterDetail details1010 = new ParameterDetail();
		details1010.setCode("1010");
		details1010.setVariable1("15");

		ParameterDetail details1020 = new ParameterDetail();
		details1020.setCode("1020");
		details1020.setVariable1("15");

		ParameterDetail details1021 = new ParameterDetail();
		details1021.setCode("1021");
		details1021.setVariable1("15");

		ParameterDetail details1030 = new ParameterDetail();
		details1030.setCode("1030");
		details1030.setVariable1("15");

		ParameterDetail details1040 = new ParameterDetail();
		details1040.setCode("1040");
		details1040.setVariable1("15");

		ParameterDetail details1060 = new ParameterDetail();
		details1060.setCode("1060");
		details1060.setVariable1("15");

		ParameterDetail details1160 = new ParameterDetail();
		details1160.setCode("1160");
		details1160.setVariable1("15");

		ParameterDetail details2010 = new ParameterDetail();
		details2010.setCode("2010");
		details2010.setVariable1("15");

		ParameterDetail details2020 = new ParameterDetail();
		details2020.setCode("2020");
		details2020.setVariable1("15");

		ParameterDetail details2070 = new ParameterDetail();
		details2070.setCode("2070");
		details2070.setVariable1("15");

		ParameterDetail details2080 = new ParameterDetail();
		details2080.setCode("2080");
		details2080.setVariable1("15");

		ParameterDetail details2100 = new ParameterDetail();
		details2100.setCode("2100");
		details2100.setVariable1("15");

		ParameterDetail details2110 = new ParameterDetail();
		details2110.setCode("2110");
		details2110.setVariable1("15");

		ParameterDetail details2500 = new ParameterDetail();
		details2500.setCode("2500");
		details2500.setVariable1("15");

		ParameterDetail details2510 = new ParameterDetail();
		details2510.setCode("2510");
		details2510.setVariable1("15");

		ParameterDetail details2520 = new ParameterDetail();
		details2520.setCode("2520");
		details2520.setVariable1("15");

		ParameterDetail details2800 = new ParameterDetail();
		details2800.setCode("2800");
		details2800.setVariable1("15");

		ParameterDetail details3020 = new ParameterDetail();
		details3020.setCode("3020");
		details3020.setVariable1("15");

		ParameterDetail details3030 = new ParameterDetail();
		details3030.setCode("3030");
		details3030.setVariable1("15");

		ParameterDetail details3040 = new ParameterDetail();
		details3040.setCode("3040");
		details3040.setVariable1("15");

		ParameterDetail details4010 = new ParameterDetail();
		details4010.setCode("4010");
		details4010.setVariable1("15");

		ParameterDetail details4020 = new ParameterDetail();
		details4020.setCode("4020");
		details4020.setVariable1("15");

		ParameterDetail details4040 = new ParameterDetail();
		details4040.setCode("4040");
		details4040.setVariable1("15");

		parameter104.setDetails(new HashSet<ParameterDetail>(){{
			add(details1010);
			add(details1020);
			add(details1021);
			add(details1030);
			add(details1040);
			add(details1060);
			add(details1160);
			add(details2010);
			add(details2020);
			add(details2070);
			add(details2080);
			add(details2100);
			add(details2110);
			add(details2500);
			add(details2510);
			add(details2520);
			add(details2800);
			add(details3020);
			add(details3030);
			add(details3040);
			add(details4010);
			add(details4020);
			add(details4040);
		}});

		parameterRepository.save(parameter104);

		details1010.setParameter(parameter104);
		parameterDetailRepository.save(details1010);

		details1020.setParameter(parameter104);
		parameterDetailRepository.save(details1020);

		details1021.setParameter(parameter104);
		parameterDetailRepository.save(details1021);

		details1030.setParameter(parameter104);
		parameterDetailRepository.save(details1030);

		details1040.setParameter(parameter104);
		parameterDetailRepository.save(details1040);

		details1060.setParameter(parameter104);
		parameterDetailRepository.save(details1060);

		details1160.setParameter(parameter104);
		parameterDetailRepository.save(details1160);

		details2010.setParameter(parameter104);
		parameterDetailRepository.save(details2010);

		details2020.setParameter(parameter104);
		parameterDetailRepository.save(details2020);

		details2070.setParameter(parameter104);
		parameterDetailRepository.save(details2070);

		details2080.setParameter(parameter104);
		parameterDetailRepository.save(details2080);

		details2100.setParameter(parameter104);
		parameterDetailRepository.save(details2100);

		details2110.setParameter(parameter104);
		parameterDetailRepository.save(details2110);

		details2500.setParameter(parameter104);
		parameterDetailRepository.save(details2500);

		details2510.setParameter(parameter104);
		parameterDetailRepository.save(details2510);

		details2520.setParameter(parameter104);
		parameterDetailRepository.save(details2520);

		details2800.setParameter(parameter104);
		parameterDetailRepository.save(details2800);

		details3020.setParameter(parameter104);
		parameterDetailRepository.save(details3020);

		details3030.setParameter(parameter104);
		parameterDetailRepository.save(details3030);

		details3040.setParameter(parameter104);
		parameterDetailRepository.save(details3040);

		details4010.setParameter(parameter104);
		parameterDetailRepository.save(details4010);

		details4020.setParameter(parameter104);
		parameterDetailRepository.save(details4020);

		details4040.setParameter(parameter104);
		parameterDetailRepository.save(details4040);

		/* P105 : Set Color process day */

		RestTemplate restTemplate = new RestTemplate();
		String resultPAString  = restTemplate.getForObject(OrgSysServer+"/profile/pa/%", String.class);
		Map mapPA = gson.fromJson(resultPAString, Map.class);
		List<Map<String,String>> paList = (List<Map<String, String>>) mapPA.get("restBodyList");

		String resultPSAString  = restTemplate.getForObject(OrgSysServer+"/profile/psa/%", String.class);
		Map mapPSA = gson.fromJson(resultPSAString, Map.class);
		List<Map<String,String>> psaList = (List<Map<String, String>>) mapPSA.get("restBodyList");

		Parameter parameter105 = new Parameter();
		parameter105.setCode("P105");
		parameter105.setName("Color PA processDay");
		parameter105.setDescription("Color process day");
		parameter105.setScreenType("1");
		parameter105.setVariableName1("Color process day");
		parameter105.setNumberOfColumn(1);
		parameterRepository.save(parameter105);

		List<ParameterDetail> parameterDetailList_105 = new ArrayList<>();


		for(Map<String,String> paModel :paList){

			ParameterDetail parameterDetail105_1 = null;
			ParameterDetail parameterDetail105_2 = null;
			ParameterDetail parameterDetail105_3 = null;
			ParameterDetail parameterDetail105_4 = null;
			ParameterDetail parameterDetail105_5 = null;
			ParameterDetail parameterDetail105_6 = null;
			ParameterDetail parameterDetail105_7 = null;
			ParameterDetail parameterDetail105_8 = null;
			ParameterDetail parameterDetail105_9 = null;


			parameterDetail105_1 = new ParameterDetail("APP_" + paModel.get("PaCode"),"0","5","#000000",parameter105.getCode());
			parameterDetail105_2 = new ParameterDetail("APP_" + paModel.get("PaCode"),"6","9","#FFA500",parameter105.getCode());
			parameterDetail105_3 = new ParameterDetail("APP_" + paModel.get("PaCode"),"10","99","#FF0000",parameter105.getCode());
			parameterDetail105_4 = new ParameterDetail("EXP_" + paModel.get("PaCode"),"0","5","#000000",parameter105.getCode());
			parameterDetail105_5 = new ParameterDetail("EXP_" + paModel.get("PaCode"),"6","9","#FFA500",parameter105.getCode());
			parameterDetail105_6 = new ParameterDetail("EXP_" + paModel.get("PaCode"),"10","99","#FF0000",parameter105.getCode());
			parameterDetail105_7 = new ParameterDetail("ADV_" + paModel.get("PaCode"),"0","5","#000000",parameter105.getCode());
			parameterDetail105_8 = new ParameterDetail("ADV_" + paModel.get("PaCode"),"6","9","#FFA500",parameter105.getCode());
			parameterDetail105_9 = new ParameterDetail("ADV_" + paModel.get("PaCode"),"10","99","#FF0000",parameter105.getCode());

			parameterDetailList_105.add(parameterDetail105_1);
			parameterDetailList_105.add(parameterDetail105_2);
			parameterDetailList_105.add(parameterDetail105_3);
			parameterDetailList_105.add(parameterDetail105_4);
			parameterDetailList_105.add(parameterDetail105_5);
			parameterDetailList_105.add(parameterDetail105_6);
			parameterDetailList_105.add(parameterDetail105_7);
			parameterDetailList_105.add(parameterDetail105_8);
			parameterDetailList_105.add(parameterDetail105_9);

			parameter105.setDetails(new HashSet<>(parameterDetailList_105));

			parameterDetail105_1.setParameter(parameter105);
			parameterDetailRepository.save(parameterDetail105_1);
			parameterDetail105_2.setParameter(parameter105);
			parameterDetailRepository.save(parameterDetail105_2);
			parameterDetail105_3.setParameter(parameter105);
			parameterDetailRepository.save(parameterDetail105_3);
			parameterDetail105_4.setParameter(parameter105);
			parameterDetailRepository.save(parameterDetail105_4);
			parameterDetail105_5.setParameter(parameter105);
			parameterDetailRepository.save(parameterDetail105_5);
			parameterDetail105_6.setParameter(parameter105);
			parameterDetailRepository.save(parameterDetail105_6);
			parameterDetail105_7.setParameter(parameter105);
			parameterDetailRepository.save(parameterDetail105_7);
			parameterDetail105_8.setParameter(parameter105);
			parameterDetailRepository.save(parameterDetail105_8);
			parameterDetail105_9.setParameter(parameter105);
			parameterDetailRepository.save(parameterDetail105_9);


		}

		/* P106 : LINK RESERVE CAR  */
		Parameter parameter106 = new Parameter();
		parameter106.setCode("P106");
		parameter106.setName("Reserve Car Link");
		parameter106.setDescription("Reserve Car Link");
		parameter106.setScreenType("1");
		parameter106.setVariableName1("Link");
		parameter106.setNumberOfColumn(1);

		ParameterDetail parameterDetail106_1 = new ParameterDetail();
		parameterDetail106_1.setCode("C001");
		parameterDetail106_1.setDescription("ระบบจองรถสำนักงานใหญ่");
		parameterDetail106_1.setVariable1("http://10.1.2.165:28080/carmis/");

		ParameterDetail parameterDetail106_2 = new ParameterDetail();
		parameterDetail106_2.setCode("C002");
		parameterDetail106_2.setDescription("ระบบจองรถโรงงาน");
		parameterDetail106_2.setVariable1("http://10.1.2.33/cbs/login.asp");

		parameter106.setDetails(new HashSet<ParameterDetail>(){{
			add(parameterDetail106_1);
			add(parameterDetail106_2);
		}});

		parameterRepository.save(parameter106);

		parameterDetail106_1.setParameter(parameter106);
		parameterDetailRepository.save(parameterDetail106_1);

		parameterDetail106_2.setParameter(parameter106);
		parameterDetailRepository.save(parameterDetail106_2);

		/* P107 : HOTEL RESERVATION FILE DOWNLOAD  */
		Parameter parameter107 = new Parameter();
		parameter107.setCode("P107");
		parameter107.setName("Hotel Reservation File Download");
		parameter107.setDescription("Hotel Reservation File Download");
		parameter107.setScreenType("1");
		parameter107.setVariableName1("file name");
		parameter107.setVariableName2("path file");
		parameter107.setNumberOfColumn(2);

		ParameterDetail parameterDetail107_1 = new ParameterDetail();
		parameterDetail107_1.setCode("H001");
		parameterDetail107_1.setDescription("แบบฟอร์มจองที่พัก");
		parameterDetail107_1.setVariable1("HotelReservationTemplate.xls");
//		parameterDetail107_1.setVariable2("/home/imilkii/Desktop/");
		parameterDetail107_1.setVariable2("C:\\application\\fileDownload\\");

		parameter107.setDetails(new HashSet<ParameterDetail>(){{
			add(parameterDetail107_1);
		}});

		parameterRepository.save(parameter107);

		parameterDetail107_1.setParameter(parameter107);
		parameterDetailRepository.save(parameterDetail107_1);


		/* P108 : FLIGHT RESERVATION FILE DOWNLOAD  */
		Parameter parameter108 = new Parameter();
		parameter108.setCode("P108");
		parameter108.setName("Flight Reservation File Download");
		parameter108.setDescription("Flight Reservation File Download");
		parameter108.setScreenType("1");
		parameter108.setVariableName1("file name");
		parameter108.setVariableName2("path file");
		parameter108.setNumberOfColumn(2);

		ParameterDetail parameterDetail108_1 = new ParameterDetail();
		parameterDetail108_1.setCode("F001");
		parameterDetail108_1.setDescription("แบบฟอร์มจองตั๋วเครื่องบิน");
		parameterDetail108_1.setVariable1("FlightReservationTemplate.xls");
//		parameterDetail108_1.setVariable2("/home/imilkii/Desktop/");
		parameterDetail108_1.setVariable2("C:\\application\\fileDownload\\");

		parameter108.setDetails(new HashSet<ParameterDetail>(){{
			add(parameterDetail108_1);
		}});

		parameterRepository.save(parameter108);

		parameterDetail108_1.setParameter(parameter108);
		parameterDetailRepository.save(parameterDetail108_1);

		/* P109 : CAR RESERVATION FILE DOWNLOAD  */
		Parameter parameter109 = new Parameter();
		parameter109.setCode("P109");
		parameter109.setName("Car Reservation File Download");
		parameter109.setDescription("Car Reservation File Download");
		parameter109.setScreenType("1");
		parameter109.setVariableName1("file name");
		parameter109.setVariableName2("path file");
		parameter109.setNumberOfColumn(2);

		ParameterDetail parameterDetail109_1 = new ParameterDetail();
		parameterDetail109_1.setCode("C001");
		parameterDetail109_1.setDescription("แบบฟอร์มจองรถ");
		parameterDetail109_1.setVariable1("FlightReservationTemplate.xls");
//		parameterDetail109_1.setVariable2("/home/imilkii/Desktop/");
		parameterDetail109_1.setVariable2("C:\\application\\fileDownload\\");

		parameter109.setDetails(new HashSet<ParameterDetail>(){{
			add(parameterDetail109_1);
		}});

		parameterRepository.save(parameter109);

		parameterDetail109_1.setParameter(parameter109);
		parameterDetailRepository.save(parameterDetail109_1);

		/* P110 : User Manual FILE DOWNLOAD  */
		Parameter parameter110 = new Parameter();
		parameter110.setCode("P110");
		parameter110.setName("User Manual");
		parameter110.setDescription("User Manual");
		parameter110.setScreenType("1");
		parameter110.setVariableName1("file name");
		parameter110.setVariableName2("path file");
		parameter110.setNumberOfColumn(2);

		ParameterDetail parameterDetail110_1 = new ParameterDetail();
		parameterDetail110_1.setCode("UM001");
		parameterDetail110_1.setDescription("คู่มือสำหรับผู้ใช้งานทั่วไป");
		parameterDetail110_1.setVariable1("MPG_Expense_Approve_Manual.pdf");
//		parameterDetail110_1.setVariable2("/home/korrakote/Desktop/UserManual/");
		parameterDetail110_1.setVariable2("C:\\application\\fileDownload\\");

		ParameterDetail parameterDetail110_2 = new ParameterDetail();
		parameterDetail110_2.setCode("UM002");
		parameterDetail110_2.setDescription("คู่มือสำหรับผู้อนุมัติ");
		parameterDetail110_2.setVariable1("MPG_Expense_Approve_Manual_for_Approver.pdf");
//		parameterDetail110_2.setVariable2("/home/korrakote/Desktop/UserManual/");
		parameterDetail110_2.setVariable2("C:\\application\\fileDownload\\");

		parameter110.setDetails(new HashSet<ParameterDetail>(){{
			add(parameterDetail110_1);
			add(parameterDetail110_2);
		}});

		parameterRepository.save(parameter110);

		parameterDetail110_1.setParameter(parameter110);
		parameterDetail110_2.setParameter(parameter110);
		parameterDetailRepository.save(parameterDetail110_1);
		parameterDetailRepository.save(parameterDetail110_2);
	}

}