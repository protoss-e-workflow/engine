package com.spt.engine.database;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.spt.engine.entity.general.*;
import com.spt.engine.repository.general.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import com.spt.engine.entity.app.Company;
import com.spt.engine.entity.app.CostCenter;
import com.spt.engine.entity.app.Department;
import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentApprove;
import com.spt.engine.entity.app.DocumentApproveItem;
import com.spt.engine.entity.app.Location;
import com.spt.engine.entity.app.TravelDetail;
import com.spt.engine.entity.general.LocaleMessage;
import com.spt.engine.entity.general.MasterData;
import com.spt.engine.entity.general.MasterDataDetail;
import com.spt.engine.entity.general.Menu;
import com.spt.engine.entity.general.Parameter;
import com.spt.engine.entity.general.ParameterDetail;
import com.spt.engine.entity.general.ReportConfiguration;
import com.spt.engine.entity.general.ReportCriteria;
import com.spt.engine.entity.general.ReportProjection;
import com.spt.engine.entity.general.Role;
import com.spt.engine.entity.general.RunningType;
import com.spt.engine.entity.general.User;
import com.spt.engine.repository.app.CompanyRepository;
import com.spt.engine.repository.app.CostCenterRepository;
import com.spt.engine.repository.app.DepartmentRepository;
import com.spt.engine.repository.app.DocumentRepository;
import com.spt.engine.repository.app.LocationRepository;
import com.spt.engine.repository.app.TravelDetailRepository;
import com.spt.engine.repository.general.LocaleMessageRepository;
import com.spt.engine.repository.general.MasterDataDetailRepository;
import com.spt.engine.repository.general.MasterDataRepository;
import com.spt.engine.repository.general.MenuRepository;
import com.spt.engine.repository.general.ParameterDetailRepository;
import com.spt.engine.repository.general.ParameterRepository;
import com.spt.engine.repository.general.ReportConfigurationRepository;
import com.spt.engine.repository.general.ReportCriteriaRepository;
import com.spt.engine.repository.general.ReportProjectionRepository;
import com.spt.engine.repository.general.RunningTypeRepository;
import com.spt.engine.repository.general.UserRepository;
import com.spt.engine.repository.app.CompanyRepository;
import com.spt.engine.repository.app.CostCenterRepository;
import com.spt.engine.repository.app.DepartmentRepository;


//@Component
public class MasterSimpleLoader implements CommandLineRunner {

	
	private final MasterDataRepository masterDataRepository;
	private final MasterDataDetailRepository masterDataDetailRepository;
	
	

	@Autowired
	public MasterSimpleLoader(MasterDataRepository masterDataRepository,
			MasterDataDetailRepository masterDataDetailRepository
			) {
		this.masterDataRepository = masterDataRepository;
		this.masterDataDetailRepository = masterDataDetailRepository;
	}

	@Override
	public void run(String... strings) throws Exception {
		
		/*MasterData expenseType = new MasterData();
		expenseType.setCode("M001");
		expenseType.setName("Expense Type");
		expenseType.setNameMessageCode("LABEL_EXPENSE_TYPE");
		expenseType.setDescription("Expense Type by Company");
		expenseType.setFlagOrgCode(false);
		expenseType.setNumberOfColumn(15);
		expenseType.setVariableName1("Min Price");
		expenseType.setVariableName2("Max Price");
		expenseType.setVariableName3("Max 3");
		expenseType.setVariableName4("Max 4");
		expenseType.setVariableName5("Max 5");
		expenseType.setVariableName6("Max 6");
		expenseType.setVariableName7("Max 7");
		expenseType.setVariableName8("Max 8");
		expenseType.setVariableName9("Max 9");
		expenseType.setVariableName10("Date Active");
		expenseType.setVariableName11("Max 11");
		expenseType.setVariableName12("Max 12");
		expenseType.setVariableName13("Max 13");
		expenseType.setVariableName14("Max 14");
		expenseType.setVariableName15("Max 15");

		expenseType.setVariableType1("INTEGER");
		expenseType.setVariableType2("FLOAT");
		expenseType.setVariableFlagRequire13(true);
		expenseType.setVariableDefaultValue7("SPP");
		expenseType.setVariableDefaultValue1("2300");
		expenseType.setVariableDefaultValue2("99990");

		expenseType.setVariableDefaultValue10("30/05/2017");
		expenseType.setVariableDefaultValue14("true");
		expenseType.setVariableType14("BOOLEAN");
		expenseType.setVariableType10("DATE");
		expenseType.setVariableType8("DATE");
		
		MasterDataDetail expenseType1 = new  MasterDataDetail();
		expenseType1.setCode("X1");
		expenseType1.setOrgCode("1011");
		expenseType1.setName("Travel");
		expenseType1.setFlagActive(true);
		expenseType1.setVariable1("1900");
		expenseType1.setVariable2("4500");
		expenseType1.setVariable14("true");
		

		MasterDataDetail expenseType2 = new  MasterDataDetail();
		expenseType2.setCode("X2");
		expenseType2.setOrgCode("1011");
		expenseType2.setName("Healty");
		expenseType2.setFlagActive(true);
		expenseType2.setVariable1("1000");
		expenseType2.setVariable2("6000");
		
		expenseType.setDetails(new HashSet<MasterDataDetail>(){{
            add(expenseType1);
            add(expenseType2);
        }});
		masterDataRepository.save(expenseType);
		
		expenseType1.setMasterdata(expenseType);
		masterDataDetailRepository.save(expenseType1);
		
		expenseType2.setMasterdata(expenseType);
		masterDataDetailRepository.save(expenseType2);*/
		
//		MasterData approvedType = new MasterData();
//		approvedType.setCode("M002");
//		approvedType.setName("Approve Type");
//		approvedType.setFlagOrgCode(false);
//		approvedType.setNameMessageCode("LABEL_APPROVE_TYPE");
//		approvedType.setDescription("Approve Type by Company");
//		approvedType.setNumberOfColumn(1);
//		approvedType.setVariableName1("Check IO");
//		approvedType.setVariableType1("BOOLEAN");
//		
//		MasterDataDetail approveType1 = new  MasterDataDetail();
//		approveType1.setCode("0001");
//		approveType1.setName("Travel");
//		approveType1.setFlagActive(true);
//		approveType1.setOrgCode("HO");
//		approveType1.setVariable1("true");




			/* M001 : Flow Type */
		MasterData flowType = new MasterData();
		flowType.setCode("M001");
		flowType.setName("Flow Type");
		flowType.setFlagOrgCode(false);
		flowType.setNameMessageCode("LABEL_FLOW_TYPE");
		flowType.setDescription("Flow Type");
		flowType.setNumberOfColumn(1);
		flowType.setVariableName1("Name");
		flowType.setVariableName2("Code Flow Type");
		
		MasterDataDetail flowTypeDetailF001 = new MasterDataDetail("MPG:Auth:0.99-Auth.F001","F001","เลี้ยงรับรองและของขวัญ",																							"flow-image_4",			"Y","Y","N","N","Y","Y");
		MasterDataDetail flowTypeDetailF002 = new MasterDataDetail("MPG:Auth:0.99-Auth.F002","F002","เบิกเงินทดรองจ่ายต่อครั้ง",																							"flow-image_4",			"Y","Y","N","N","N","Y");
		MasterDataDetail flowTypeDetailF002_1 = new MasterDataDetail("MPG:Auth:0.99-Auth.F002-1","F002-1","เบิกเงินทดรองจ่าย แบบรวมชุด",																							"flow-image_4",			"Y","Y","N","N","N","Y");
		MasterDataDetail flowTypeDetailF003 = new MasterDataDetail("MPG:Auth:0.99-Auth.F003","F003","เบิกค่าใช้จ่ายที่ผู้ขายไม่มีใบเสร็จบิลเงินสดออกให้ต่อครั้ง",																"flow-image_4",			"Y","Y","N","N","Y","Y");
		MasterDataDetail flowTypeDetailF004 = new MasterDataDetail("MPG:Auth:0.99-Auth.F004","F004","การอนุมัติให้ชำระหนี้ตามข้อผูกพันหรือ โครงการที่ได้รับอนุมัติแล้วต่อครั้ง (มีเอกสารประกอบ)",								"flow-image_4",			"Y","Y","N","N","Y","Y");
		MasterDataDetail flowTypeDetailF005 = new MasterDataDetail("MPG:Auth:0.99-Auth.F005","F005","การอนุมัติให้ชำระหนี้ตามข้อผูกพันหรือ ดครงการที่ได้รับอนุมัติแล้วต่อครั้ง (ไม่มีเอกสารประกอบ)",							"flow-image_4",			"Y","Y","N","N","Y","Y");
		MasterDataDetail flowTypeDetailH001 = new MasterDataDetail("MPG:HRAuth:0.99-HRAuth.H001","H001","การไปปฏิบัติงานนอกเขตงานปกติในประเทศ <= C9",																"flow-image_2",			"Y","Y","N","N","N","N");
		MasterDataDetail flowTypeDetailH002 = new MasterDataDetail("MPG:HRAuth:0.99-HRAuth.H001","H002","การไปปฏิบัติงานนอกเขตงานปกติในประเทศ C10 , C11",															"flow-image_2",			"Y","Y","N","N","N","N");
		MasterDataDetail flowTypeDetailH003 = new MasterDataDetail("MPG:HRAuth:0.99-HRAuth.H003","H003","การไปปฏิบัติงานนอกเขตงานปกติต่างประเทศ ASEAN",																"flow-image_2",			"Y","Y","N","N","N","N");
		MasterDataDetail flowTypeDetailH004 = new MasterDataDetail("MPG:HRAuth:0.99-HRAuth.H004","H004","การไปปฏิบัติงานนอกเขตงานปกติต่างประเทศ <= UC2",																"flow-image_2",			"Y","Y","N","N","N","N");
		MasterDataDetail flowTypeDetailH005 = new MasterDataDetail("MPG:HRAuth:0.99-HRAuth.H004","H005","การไปปฏิบัติงานนอกเขตงานปกติต่างประเทศ > UC2",																"flow-image_2",			"Y","Y","N","N","N","N");
		MasterDataDetail flowTypeDetailH006 = new MasterDataDetail("MPG:HRAuth:0.99-HRAuth.H006","H006","เบี้ยเลี้ยง,ค่าที่พัก,ค่าใช้จ่ายเดินทางในประเทศ <= 30 วัน",															"flow-image_5_NoAdmin",	"Y","Y","N","Y","Y","Y");
		MasterDataDetail flowTypeDetailH007 = new MasterDataDetail("MPG:HRAuth:0.99-HRAuth.H007","H007","เบี้ยเลี้ยง,ค่าที่พัก,ค่าใช้จ่ายเดินทางในประเทศ > 30 วัน",															"flow-image_5_NoAdmin",	"Y","Y","N","Y","Y","Y");
		MasterDataDetail flowTypeDetailH008 = new MasterDataDetail("MPG:HRAuth:0.99-HRAuth.H008","H008","เบี้ยเลี้ยง,ค่าที่พัก,ค่าใช้จ่ายเดินทางต่างประเทศ",																	"flow-image_5_NoAdmin",	"Y","Y","N","Y","Y","Y");
		MasterDataDetail flowTypeDetailH009 = new MasterDataDetail("MPG:HRAuth:0.99-HRAuth.H009","H009","ค่าใช้จ่ายฝึกอบรมภายใน",																						"flow-image_5_NoAdmin",	"Y","Y","N","Y","Y","Y");
		MasterDataDetail flowTypeDetailH010 = new MasterDataDetail("MPG:HRAuth:0.99-HRAuth.H010","H010","ค่าใช้จ่ายฝึกอบรมภายนอก (ผ่านความเห็นชอบสถาบันพัฒนา) ในประเทศ",											"flow-image_5_NoAdmin",	"Y","Y","N","Y","Y","Y");
		MasterDataDetail flowTypeDetailH011 = new MasterDataDetail("MPG:HRAuth:0.99-HRAuth.H011","H011","ค่าใช้จ่ายฝึกอบรมภายนอก (ผ่านความเห็นชอบสถาบันพัฒนา) ต่างประเทศ",											"flow-image_5_NoAdmin",	"Y","Y","N","Y","Y","Y");
		MasterDataDetail flowTypeDetailH012 = new MasterDataDetail("MPG:HRAuth:0.99-HRAuth.H012","H012","การนำรถยนต์ส่วนตัวมาใช้ในการปฏิบัติงาน",																		"flow-image_4",			"Y","Y","N","N","Y","Y");
		MasterDataDetail flowTypeDetailH013 = new MasterDataDetail("MPG:HRAuth:0.99-HRAuth.H013","H013","ค่าใช้จ่ายในการประชุมภายใน (ค่าอหาร ค่าของว่าง ค่าเครื่องดื่ม)",													"flow-image_4",			"Y","Y","N","N","Y","Y");
		MasterDataDetail flowTypeDetailA001 = new MasterDataDetail("MPG:AdminAuth:0.99-AdminAuth.A001","A001","ยานพาหนะของบริษัท",																					"flow-image_3",			"Y","Y","Y","N","N","N");
		MasterDataDetail flowTypeDetailA002 = new MasterDataDetail("MPG:AdminAuth:0.99-AdminAuth.A002","A002","ค่าน้ำมันรถยนต์ประจำตำแหน่ง",																			"flow-image_6",			"Y","Y","Y","Y","Y","Y");
		MasterDataDetail flowTypeDetailA003 = new MasterDataDetail("MPG:AdminAuth:0.99-AdminAuth.A003","A003","ค่าบอกรับสมาชิก ตำราวิชาการ เอกสารอ้างอิง บริจาคการกุศล ของขวัญงานสมรส เปิดป้าย ทำบุญบ้าน ของเยี่ยม",	"flow-image_4",			"Y","Y","N","N","Y","Y");
		MasterDataDetail flowTypeDetailA004 = new MasterDataDetail("MPG:AdminAuth:0.99-AdminAuth.A004","A004","ค่าซ่อมแซมรถส่วนกลาง ประจำตำแหน่ง",																	"flow-image_5_HaveAdmin",	"Y","Y","Y","N","Y","Y");
		MasterDataDetail flowTypeDetailA005 = new MasterDataDetail("MPG:AdminAuth:0.99-AdminAuth.A005","A005","ค่าเบี้ยประกันรถยนต์ ภาษีรถยนต์ โอนรถ ภาษีป้าย",															"flow-image_4",			"Y","Y","N","N","Y","Y");
		MasterDataDetail flowTypeDetailA006 = new MasterDataDetail("MPG:AdminAuth:0.99-AdminAuth.A006","A006","ค่าโทรเลข โทรพิมพ์ ไปรษณียากร ค่าเช่าตู้ ปณ. ค่าไฟฟ้า น้ำประปา ค่าโทรศัพท์ ค่าต่อใบอนุญาติต่างๆต่อครั้ง",		"flow-image_4",			"Y","Y","N","N","Y","Y");
		MasterDataDetail flowTypeDetailA007 = new MasterDataDetail("MPG:AdminAuth:0.99-AdminAuth.A007","A007","ค่าน้ำมันรถยนต์ส่วนกลางต่อครั้ง",																			"flow-image_4",			"Y","Y","N","N","Y","Y");
		MasterDataDetail flowTypeDetailM001 = new MasterDataDetail("MPG:OtherAuth:0.99-OtherAuth.M001","M001","ซ่อมแซม ปรับปรุง ดัดแปลงเครื่องจกร เครื่องมือและอุปกรณ์ ต่อครั้ง",											"flow-image_4",			"Y","Y","N","N","Y","Y");
		MasterDataDetail flowTypeDetailP001 = new MasterDataDetail("MPG:OtherAuth:0.99-OtherAuth.P001","P001","การโฆษณา / การประชาสัมพันธ์ตามงบประมาณต่อครั้ง",														"flow-image_4",			"Y","Y","N","N","Y","Y");
		MasterDataDetail flowTypeDetailPC01 = new MasterDataDetail("MPG:OtherAuth:0.99-OtherAuth.PC01","PC01","การจัดหาสิ่งของอื่นๆไม่รวมทรัพย์สินถาวรโดยตรงของแต่ละหน่วยงานไม่ผ่านระบบการจัดซื้อต่อครั้ง",				"flow-image_4",			"Y","Y","N","N","Y","Y");
		MasterDataDetail flowTypeDetailRM01 = new MasterDataDetail("MPG:OtherAuth:0.99-OtherAuth.RM01","RM01","การอนุมัติค่าใช้จ่ายในการหาอ้อยต่อครั้ง",																	"flow-image_4",			"Y","Y","N","N","Y","Y");
		
		flowType.setDetails(new HashSet<MasterDataDetail>(){{
			add(flowTypeDetailF001);
			add(flowTypeDetailF002);
			add(flowTypeDetailF002_1);
			add(flowTypeDetailF003);
			add(flowTypeDetailF004);
			add(flowTypeDetailF005);
			add(flowTypeDetailH001);
			add(flowTypeDetailH002);
			add(flowTypeDetailH003);
			add(flowTypeDetailH004);
			add(flowTypeDetailH005);
			add(flowTypeDetailH006);
			add(flowTypeDetailH007);
			add(flowTypeDetailH008);
			add(flowTypeDetailH009);
			add(flowTypeDetailH010);
			add(flowTypeDetailH011);
			add(flowTypeDetailH012);
			add(flowTypeDetailH013);
			add(flowTypeDetailA001);
			add(flowTypeDetailA002);
			add(flowTypeDetailA003);
			add(flowTypeDetailA004);
			add(flowTypeDetailA005);
			add(flowTypeDetailA006);
			add(flowTypeDetailA007);
			add(flowTypeDetailM001);
			add(flowTypeDetailP001);
			add(flowTypeDetailPC01);
			add(flowTypeDetailRM01);
		}});
		
		masterDataRepository.save(flowType);
		
		flowTypeDetailF001.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailF001);
		flowTypeDetailF002.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailF002);
		flowTypeDetailF002_1.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailF002_1);
		flowTypeDetailF003.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailF003);
		flowTypeDetailF004.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailF004);
		flowTypeDetailF005.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailF005);
		flowTypeDetailH001.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailH001);
		flowTypeDetailH002.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailH002);
		flowTypeDetailH003.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailH003);
		flowTypeDetailH004.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailH004);
		flowTypeDetailH005.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailH005);
		flowTypeDetailH006.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailH006);
		flowTypeDetailH007.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailH007);
		flowTypeDetailH008.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailH008);
		flowTypeDetailH009.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailH009);
		flowTypeDetailH010.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailH010);
		flowTypeDetailH011.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailH011);
		flowTypeDetailH012.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailH012);
		flowTypeDetailH013.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailH013);
		flowTypeDetailA001.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailA001);
		flowTypeDetailA002.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailA002);
		flowTypeDetailA003.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailA003);
		flowTypeDetailA004.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailA004);
		flowTypeDetailA005.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailA005);
		flowTypeDetailA006.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailA006);
		flowTypeDetailA007.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailA007);
		flowTypeDetailM001.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailM001);
		flowTypeDetailP001.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailP001);
		flowTypeDetailPC01.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailPC01);
		flowTypeDetailRM01.setMasterdata(flowType);  masterDataDetailRepository.save(flowTypeDetailRM01);
		
//
//
//		MasterDataDetail flowType_1 = new  MasterDataDetail();
//		flowType_1.setCode("F001");
//		flowType_1.setFlagActive(true);
////		flowType_1.setVariable1("Domestic");
////		flowType_1.setVariable2("MPG:Auth:0.99-Auth.F001");
//
//		MasterDataDetail flowType_2 = new  MasterDataDetail();
//		flowType_2.setCode("F002");
//		flowType_2.setFlagActive(true);
////		flowType_2.setName("Foreign");
////		flowType_2.setVariable1("Foreign");
////		flowType_2.setVariable2("MPG:Auth:0.99-Auth.F001");
//
//		MasterDataDetail flowType_3 = new  MasterDataDetail();
//		flowType_3.setCode("F003");
//		flowType_3.setFlagActive(true);
////		flowType_3.setName("Car");
////		flowType_3.setVariable1("Car");
////		flowType_3.setVariable2("MPG:Auth:0.99-Auth.F001");
//
//		MasterDataDetail flowType_4 = new  MasterDataDetail();
//		flowType_4.setCode("F004");
//		flowType_4.setFlagActive(true);
////		flowType_4.setName("Hotel");
////		flowType_4.setVariable1("Hotel");
////		flowType_4.setVariable2("MPG:Auth:0.99-Auth.F001");
//
//		MasterDataDetail flowType_5 = new  MasterDataDetail();
//		flowType_5.setCode("F005");
//		flowType_5.setFlagActive(true);
////		flowType_5.setName("FightBooking");
////		flowType_5.setVariable1("FightBooking");
////		flowType_5.setVariable2("MPG:Auth:0.99-Auth.F001");
//
//		MasterDataDetail flowType_6 = new  MasterDataDetail();
//		flowType_6.setCode("F006");
//		flowType_6.setFlagActive(true);
////		flowType_6.setName("FightBooking");
////		flowType_6.setVariable1("FightBooking");
////		flowType_6.setVariable2("MPG:Auth:0.99-Auth.F001");
//
//		flowType.setDetails(new HashSet<MasterDataDetail>(){{
//			add(flowType_1);
//			add(flowType_2);
//			add(flowType_3);
//			add(flowType_4);
//			add(flowType_5);
//			add(flowType_6);
//
//		}});
//		masterDataRepository.save(flowType);
//
//		flowType_1.setMasterdata(flowType);
//		masterDataDetailRepository.save(flowType_1);
//
//		flowType_2.setMasterdata(flowType);
//		masterDataDetailRepository.save(flowType_2);
//
//		flowType_3.setMasterdata(flowType);
//		masterDataDetailRepository.save(flowType_3);
//
//		flowType_4.setMasterdata(flowType);
//		masterDataDetailRepository.save(flowType_4);
//
//		flowType_5.setMasterdata(flowType);
//		masterDataDetailRepository.save(flowType_5);
//
//		flowType_6.setMasterdata(flowType);
//		masterDataDetailRepository.save(flowType_6);
//
//
//		
		/* M002 : Approve Type */
		MasterData approvedType = new MasterData();
		approvedType.setCode("M002");
		approvedType.setName("Approve Type");
		approvedType.setFlagOrgCode(false);
		approvedType.setNameMessageCode("LABEL_APPROVE_TYPE");
		approvedType.setDescription("Approve Type");
		approvedType.setNumberOfColumn(1);
		approvedType.setVariableName1("Name");
		approvedType.setVariableName2("FlowType");
		
		
		MasterDataDetail approveType1 = new  MasterDataDetail("0001", "Domestic", "การเดินทางภายในประเทศ",  true, "Domestic","MPG:HRAuth:0.99-HRAuth.H001");
		MasterDataDetail approveType2 = new  MasterDataDetail("0002", "Foreign",  "การเดินทางภายต่างประเทศ", true, "Foreign", "MPG:HRAuth:0.99-HRAuth.H004");
		MasterDataDetail approveType3 = new  MasterDataDetail("0003", "Car", "การจองรถ", true, "Car","MPG:AdminAuth:0.99-AdminAuth.A001");
		MasterDataDetail approveType4 = new  MasterDataDetail("0004", "Hotel", "การจองโรงแรม", true, "Hotel","MPG:AdminAuth:0.99-AdminAuth.A001");
		MasterDataDetail approveType5 = new  MasterDataDetail("0005", "FightBooking", "การจองตั่วเครื่องบิน", true, "FightBooking","MPG:AdminAuth:0.99-AdminAuth.A001");
		MasterDataDetail approveType6 = new  MasterDataDetail("0006", "Set Domestic", "การเดินทางภายในประเทศรวมชุด", true, "Set Domestic","MPG:HRAuth:0.99-HRAuth.H001");
		MasterDataDetail approveType7 = new  MasterDataDetail("0007", "Set Foreign", "การเดินทางภายต่างประเทศรวมชุด", true, "Set Foreign","MPG:HRAuth:0.99-HRAuth.H004");
		MasterDataDetail approveType8 = new  MasterDataDetail("0008", "Advance", "เบิกเงินยืมทดรองจ่ายต่อครั้ง", true, "Advance","MPG:Auth:0.99-Auth.F002");
		MasterDataDetail approveType9 = new  MasterDataDetail("0009", "Advance by set", "เบิกเงินยืมทดรองจากการอนุมัติแบบชุด", true, "Advance by set","MPG:Auth:0.99-Auth.F002-1");

		approvedType.setDetails(new HashSet<MasterDataDetail>(){{
            add(approveType1);
            add(approveType2);
            add(approveType3);
            add(approveType4);
            add(approveType5);
            add(approveType6);
            add(approveType7);
            add(approveType8);
            add(approveType9);

        }});
		masterDataRepository.save(approvedType);
		
		approveType1.setMasterdata(approvedType);
		masterDataDetailRepository.save(approveType1);
		
		approveType2.setMasterdata(approvedType);
		masterDataDetailRepository.save(approveType2);
		
		approveType3.setMasterdata(approvedType);
		masterDataDetailRepository.save(approveType3);
		
		approveType4.setMasterdata(approvedType);
		masterDataDetailRepository.save(approveType4);
		
		approveType5.setMasterdata(approvedType);
		masterDataDetailRepository.save(approveType5);
		
		approveType6.setMasterdata(approvedType);
		masterDataDetailRepository.save(approveType6);
		
		approveType7.setMasterdata(approvedType);
		masterDataDetailRepository.save(approveType7);

		approveType8.setMasterdata(approvedType);
		masterDataDetailRepository.save(approveType8);

		approveType9.setMasterdata(approvedType);
		masterDataDetailRepository.save(approveType9);
		
		
		/* M003 : Per Diem Rate Domistic */
		MasterData perDiemRateDomistic	= new MasterData();
		perDiemRateDomistic.setCode("M003");
		perDiemRateDomistic.setName("Per Diem Rate Domistic");
		perDiemRateDomistic.setFlagOrgCode(false);
		perDiemRateDomistic.setNameMessageCode("LABEL_PER_DIEM_RATE_DOMESTIC");
		perDiemRateDomistic.setDescription("Per Diem Rate Domistic");
		perDiemRateDomistic.setNumberOfColumn(4);
		perDiemRateDomistic.setVariableName1("LevelFrom");
		perDiemRateDomistic.setVariableName2("LevelTo");
		perDiemRateDomistic.setVariableName3("HourFrom");
		perDiemRateDomistic.setVariableName4("HourTo");
		perDiemRateDomistic.setVariableName5("Amount");
		
		perDiemRateDomistic.setVariableType1("INTEGER");
		perDiemRateDomistic.setVariableType2("INTEGER");
		perDiemRateDomistic.setVariableType3("INTEGER");
		perDiemRateDomistic.setVariableType4("INTEGER");
		perDiemRateDomistic.setVariableType5("FLOAT");
		
		/* MASTER DATA DETAIL PER DIEM RATE DOMESTIC */
		MasterDataDetail perDiemRateDomisticDetail = new MasterDataDetail();
		perDiemRateDomisticDetail.setCode("PD001");
		perDiemRateDomisticDetail.setFlagActive(true);
		perDiemRateDomisticDetail.setVariable1("1");
		perDiemRateDomisticDetail.setVariable2("5");
		perDiemRateDomisticDetail.setVariable3("4");
		perDiemRateDomisticDetail.setVariable4("8");
		perDiemRateDomisticDetail.setVariable5("100");
		
		MasterDataDetail perDiemRateDomisticDetail1 = new MasterDataDetail();
		perDiemRateDomisticDetail1.setCode("PD002");
		perDiemRateDomisticDetail1.setFlagActive(true);
		perDiemRateDomisticDetail1.setVariable1("1");
		perDiemRateDomisticDetail1.setVariable2("5");
		perDiemRateDomisticDetail1.setVariable3("9");
		perDiemRateDomisticDetail1.setVariable4("12");
		perDiemRateDomisticDetail1.setVariable5("150");
		
		MasterDataDetail perDiemRateDomisticDetail2 = new MasterDataDetail();
		perDiemRateDomisticDetail2.setCode("PD003");
		perDiemRateDomisticDetail2.setFlagActive(true);
		perDiemRateDomisticDetail2.setVariable1("1");
		perDiemRateDomisticDetail2.setVariable2("5");
		perDiemRateDomisticDetail2.setVariable3("13");
		perDiemRateDomisticDetail2.setVariable4("24");
		perDiemRateDomisticDetail2.setVariable5("200");
		
		MasterDataDetail perDiemRateDomisticDetail3 = new MasterDataDetail();
		perDiemRateDomisticDetail3.setCode("PD004");
		perDiemRateDomisticDetail3.setFlagActive(true);
		perDiemRateDomisticDetail3.setVariable1("6");
		perDiemRateDomisticDetail3.setVariable2("7");
		perDiemRateDomisticDetail3.setVariable3("4");
		perDiemRateDomisticDetail3.setVariable4("8");
		perDiemRateDomisticDetail3.setVariable5("125");
		
		MasterDataDetail perDiemRateDomisticDetail4 = new MasterDataDetail();
		perDiemRateDomisticDetail4.setCode("PD005");
		perDiemRateDomisticDetail4.setFlagActive(true);
		perDiemRateDomisticDetail4.setVariable1("6");
		perDiemRateDomisticDetail4.setVariable2("7");
		perDiemRateDomisticDetail4.setVariable3("9");
		perDiemRateDomisticDetail4.setVariable4("12");
		perDiemRateDomisticDetail4.setVariable5("190");
		
		MasterDataDetail perDiemRateDomisticDetail5 = new MasterDataDetail();
		perDiemRateDomisticDetail5.setCode("PD006");
		perDiemRateDomisticDetail5.setFlagActive(true);
		perDiemRateDomisticDetail5.setVariable1("6");
		perDiemRateDomisticDetail5.setVariable2("7");
		perDiemRateDomisticDetail5.setVariable3("13");
		perDiemRateDomisticDetail5.setVariable4("24");
		perDiemRateDomisticDetail5.setVariable5("250");
		
		MasterDataDetail perDiemRateDomisticDetail6 = new MasterDataDetail();
		perDiemRateDomisticDetail6.setCode("PD007");
		perDiemRateDomisticDetail6.setFlagActive(true);
		perDiemRateDomisticDetail6.setVariable1("8");
		perDiemRateDomisticDetail6.setVariable2("9");
		perDiemRateDomisticDetail6.setVariable3("4");
		perDiemRateDomisticDetail6.setVariable4("8");
		perDiemRateDomisticDetail6.setVariable5("150");
		
		MasterDataDetail perDiemRateDomisticDetail7 = new MasterDataDetail();
		perDiemRateDomisticDetail7.setCode("PD008");
		perDiemRateDomisticDetail7.setFlagActive(true);
		perDiemRateDomisticDetail7.setVariable1("8");
		perDiemRateDomisticDetail7.setVariable2("9");
		perDiemRateDomisticDetail7.setVariable3("9");
		perDiemRateDomisticDetail7.setVariable4("12");
		perDiemRateDomisticDetail7.setVariable5("225");
		
		MasterDataDetail perDiemRateDomisticDetail8 = new MasterDataDetail();
		perDiemRateDomisticDetail8.setCode("PD008");
		perDiemRateDomisticDetail8.setFlagActive(true);
		perDiemRateDomisticDetail8.setVariable1("8");
		perDiemRateDomisticDetail8.setVariable2("9");
		perDiemRateDomisticDetail8.setVariable3("13");
		perDiemRateDomisticDetail8.setVariable4("24");
		perDiemRateDomisticDetail8.setVariable5("300");
		
		perDiemRateDomistic.setDetails(new HashSet<MasterDataDetail>(){{
            add(perDiemRateDomisticDetail);
            add(perDiemRateDomisticDetail1);
            add(perDiemRateDomisticDetail2);
            add(perDiemRateDomisticDetail3);
            add(perDiemRateDomisticDetail4);
            add(perDiemRateDomisticDetail5);
            add(perDiemRateDomisticDetail6);
            add(perDiemRateDomisticDetail7);
            add(perDiemRateDomisticDetail8);
        }});
		masterDataRepository.save(perDiemRateDomistic);
		
		perDiemRateDomisticDetail.setMasterdata(perDiemRateDomistic);
		masterDataDetailRepository.save(perDiemRateDomisticDetail);
		
		perDiemRateDomisticDetail1.setMasterdata(perDiemRateDomistic);
		masterDataDetailRepository.save(perDiemRateDomisticDetail1);
		
		perDiemRateDomisticDetail2.setMasterdata(perDiemRateDomistic);
		masterDataDetailRepository.save(perDiemRateDomisticDetail2);
		
		perDiemRateDomisticDetail3.setMasterdata(perDiemRateDomistic);
		masterDataDetailRepository.save(perDiemRateDomisticDetail3);
		
		perDiemRateDomisticDetail4.setMasterdata(perDiemRateDomistic);
		masterDataDetailRepository.save(perDiemRateDomisticDetail4);
		
		perDiemRateDomisticDetail5.setMasterdata(perDiemRateDomistic);
		masterDataDetailRepository.save(perDiemRateDomisticDetail5);
		
		perDiemRateDomisticDetail6.setMasterdata(perDiemRateDomistic);
		masterDataDetailRepository.save(perDiemRateDomisticDetail6);
		
		perDiemRateDomisticDetail7.setMasterdata(perDiemRateDomistic);
		masterDataDetailRepository.save(perDiemRateDomisticDetail7);
		
		perDiemRateDomisticDetail8.setMasterdata(perDiemRateDomistic);
		masterDataDetailRepository.save(perDiemRateDomisticDetail8);
		
		/* M004 : PerDiem Risk Rate */
		MasterData perDiemRiskRate	= new MasterData();
		perDiemRiskRate.setCode("M004");
		perDiemRiskRate.setName("PerDiem Risk Rate");
		perDiemRiskRate.setFlagOrgCode(false);
		perDiemRiskRate.setNameMessageCode("LABEL_PERDIEM_RISK_RATE");
		perDiemRiskRate.setDescription("PerDiem Risk Rate");
		perDiemRiskRate.setNumberOfColumn(3);
		perDiemRiskRate.setVariableName1("LevelFrom");
		perDiemRiskRate.setVariableName2("LevelTo");
		perDiemRiskRate.setVariableName3("Amount");
		
		perDiemRiskRate.setVariableType1("INTEGER");
		perDiemRiskRate.setVariableType2("INTEGER");
		perDiemRiskRate.setVariableType3("FLOAT");
		
		/* MASTER DATA DETAIL LOCATION DOMESTIC */
		MasterDataDetail perDiemRiskRateDetail = new MasterDataDetail();
		perDiemRiskRateDetail.setCode("M400");
		perDiemRiskRateDetail.setFlagActive(true);
		perDiemRiskRateDetail.setVariable1("1");
		perDiemRiskRateDetail.setVariable2("5");
		perDiemRiskRateDetail.setVariable3("4500");
		
		MasterDataDetail perDiemRiskRateDetail1 = new MasterDataDetail();
		perDiemRiskRateDetail1.setCode("M401");
		perDiemRiskRateDetail1.setFlagActive(true);
		perDiemRiskRateDetail1.setVariable1("6");
		perDiemRiskRateDetail1.setVariable2("7");
		perDiemRiskRateDetail1.setVariable3("5500");
		
		MasterDataDetail perDiemRiskRateDetail2 = new MasterDataDetail();
		perDiemRiskRateDetail2.setCode("M402");
		perDiemRiskRateDetail2.setFlagActive(true);
		perDiemRiskRateDetail2.setVariable1("8");
		perDiemRiskRateDetail2.setVariable2("9");
		perDiemRiskRateDetail2.setVariable3("6500");
		
		MasterDataDetail perDiemRiskRateDetail3 = new MasterDataDetail();
		perDiemRiskRateDetail3.setCode("M403");
		perDiemRiskRateDetail3.setFlagActive(true);
		perDiemRiskRateDetail3.setVariable1("10");
		perDiemRiskRateDetail3.setVariable2("11");
		perDiemRiskRateDetail3.setVariable3("7500");
		
		MasterDataDetail perDiemRiskRateDetail4 = new MasterDataDetail();
		perDiemRiskRateDetail4.setCode("M404");
		perDiemRiskRateDetail4.setFlagActive(true);
		perDiemRiskRateDetail4.setVariable1("12");
		perDiemRiskRateDetail4.setVariable2("99");
		perDiemRiskRateDetail4.setVariable3("8500");
		
		perDiemRiskRate.setDetails(new HashSet<MasterDataDetail>(){{
            add(perDiemRiskRateDetail);
            add(perDiemRiskRateDetail1);
            add(perDiemRiskRateDetail2);
            add(perDiemRiskRateDetail3);
            add(perDiemRiskRateDetail4);
        }});
		masterDataRepository.save(perDiemRiskRate);
		
		perDiemRiskRateDetail.setMasterdata(perDiemRiskRate);
		masterDataDetailRepository.save(perDiemRiskRateDetail);
		
		perDiemRiskRateDetail1.setMasterdata(perDiemRiskRate);
		masterDataDetailRepository.save(perDiemRiskRateDetail1);
		
		perDiemRiskRateDetail2.setMasterdata(perDiemRiskRate);
		masterDataDetailRepository.save(perDiemRiskRateDetail2);
		
		perDiemRiskRateDetail3.setMasterdata(perDiemRiskRate);
		masterDataDetailRepository.save(perDiemRiskRateDetail3);
		
		perDiemRiskRateDetail4.setMasterdata(perDiemRiskRate);
		masterDataDetailRepository.save(perDiemRiskRateDetail4);
		
		
		/* M007 : PerDiem Foreign Rate */
		MasterData perDiemForeignRate	= new MasterData();
		perDiemForeignRate.setCode("M007");
		perDiemForeignRate.setName("PerDiem Foreign Rate");
		perDiemForeignRate.setFlagOrgCode(false);
		perDiemForeignRate.setNameMessageCode("LABEL_PER_DIEM_RATE_FOREIGN");
		perDiemForeignRate.setDescription("PerDiem Foreign Rate");
		perDiemForeignRate.setNumberOfColumn(3);
		perDiemForeignRate.setVariableName1("LevelFrom");
		perDiemForeignRate.setVariableName2("LevelTo");
		perDiemForeignRate.setVariableName3("Amount");
		perDiemForeignRate.setVariableName4("ZoneStatus");
		
		perDiemForeignRate.setVariableType1("INTEGER");
		perDiemForeignRate.setVariableType2("INTEGER");
		perDiemForeignRate.setVariableType3("FLOAT");
		perDiemForeignRate.setVariableType4("STRING");
		
		MasterDataDetail perDiemForeignRateDetail = new MasterDataDetail();
		perDiemForeignRateDetail.setCode("M701");
		perDiemForeignRateDetail.setFlagActive(true);
		perDiemForeignRateDetail.setVariable1("1");
		perDiemForeignRateDetail.setVariable2("7");
		perDiemForeignRateDetail.setVariable3("70");
		perDiemForeignRateDetail.setVariable4("EU");
		
		MasterDataDetail perDiemForeignRateDetail1 = new MasterDataDetail();
		perDiemForeignRateDetail1.setCode("M702");
		perDiemForeignRateDetail1.setFlagActive(true);
		perDiemForeignRateDetail1.setVariable1("8");
		perDiemForeignRateDetail1.setVariable2("11");
		perDiemForeignRateDetail1.setVariable3("80");
		perDiemForeignRateDetail1.setVariable4("EU");
		
		MasterDataDetail perDiemForeignRateDetail2 = new MasterDataDetail();
		perDiemForeignRateDetail2.setCode("M703");
		perDiemForeignRateDetail2.setFlagActive(true);
		perDiemForeignRateDetail2.setVariable1("12");
		perDiemForeignRateDetail2.setVariable2("99");
		perDiemForeignRateDetail2.setVariable3("100");
		perDiemForeignRateDetail2.setVariable4("EU");
		
		MasterDataDetail perDiemForeignRateDetail3 = new MasterDataDetail();
		perDiemForeignRateDetail3.setCode("M704");
		perDiemForeignRateDetail3.setFlagActive(true);
		perDiemForeignRateDetail3.setVariable1("1");
		perDiemForeignRateDetail3.setVariable2("7");
		perDiemForeignRateDetail3.setVariable3("60");
		perDiemForeignRateDetail3.setVariable4("SA");
		
		MasterDataDetail perDiemForeignRateDetail4 = new MasterDataDetail();
		perDiemForeignRateDetail4.setCode("M705");
		perDiemForeignRateDetail4.setFlagActive(true);
		perDiemForeignRateDetail4.setVariable1("8");
		perDiemForeignRateDetail4.setVariable2("11");
		perDiemForeignRateDetail4.setVariable3("70");
		perDiemForeignRateDetail4.setVariable4("SA");
		
		MasterDataDetail perDiemForeignRateDetail5 = new MasterDataDetail();
		perDiemForeignRateDetail5.setCode("M706");
		perDiemForeignRateDetail5.setFlagActive(true);
		perDiemForeignRateDetail5.setVariable1("12");
		perDiemForeignRateDetail5.setVariable2("99");
		perDiemForeignRateDetail5.setVariable3("90");
		perDiemForeignRateDetail5.setVariable4("SA");
		
		MasterDataDetail perDiemForeignRateDetail6 = new MasterDataDetail();
		perDiemForeignRateDetail6.setCode("M707");
		perDiemForeignRateDetail6.setFlagActive(true);
		perDiemForeignRateDetail6.setVariable1("1");
		perDiemForeignRateDetail6.setVariable2("7");
		perDiemForeignRateDetail6.setVariable3("50");
		perDiemForeignRateDetail6.setVariable4("OT");
		
		MasterDataDetail perDiemForeignRateDetail7 = new MasterDataDetail();
		perDiemForeignRateDetail7.setCode("M708");
		perDiemForeignRateDetail7.setFlagActive(true);
		perDiemForeignRateDetail7.setVariable1("8");
		perDiemForeignRateDetail7.setVariable2("11");
		perDiemForeignRateDetail7.setVariable3("60");
		perDiemForeignRateDetail7.setVariable4("OT");
		
		MasterDataDetail perDiemForeignRateDetail8 = new MasterDataDetail();
		perDiemForeignRateDetail8.setCode("M709");
		perDiemForeignRateDetail8.setFlagActive(true);
		perDiemForeignRateDetail8.setVariable1("12");
		perDiemForeignRateDetail8.setVariable2("99");
		perDiemForeignRateDetail8.setVariable3("80");
		perDiemForeignRateDetail8.setVariable4("OT");
		
		MasterDataDetail perDiemForeignRateDetail9 = new MasterDataDetail();
		perDiemForeignRateDetail9.setCode("M710");
		perDiemForeignRateDetail9.setFlagActive(true);
		perDiemForeignRateDetail9.setVariable1("1");
		perDiemForeignRateDetail9.setVariable2("7");
		perDiemForeignRateDetail9.setVariable3("30");
		perDiemForeignRateDetail9.setVariable4("MM");
		
		MasterDataDetail perDiemForeignRateDetail10 = new MasterDataDetail();
		perDiemForeignRateDetail10.setCode("M711");
		perDiemForeignRateDetail10.setFlagActive(true);
		perDiemForeignRateDetail10.setVariable1("8");
		perDiemForeignRateDetail10.setVariable2("11");
		perDiemForeignRateDetail10.setVariable3("35");
		perDiemForeignRateDetail10.setVariable4("MM");
		
		MasterDataDetail perDiemForeignRateDetail11 = new MasterDataDetail();
		perDiemForeignRateDetail11.setCode("M712");
		perDiemForeignRateDetail11.setFlagActive(true);
		perDiemForeignRateDetail11.setVariable1("12");
		perDiemForeignRateDetail11.setVariable2("99");
		perDiemForeignRateDetail11.setVariable3("50");
		perDiemForeignRateDetail11.setVariable4("MM");
		
		perDiemForeignRate.setDetails(new HashSet<MasterDataDetail>(){{
            add(perDiemForeignRateDetail);
            add(perDiemForeignRateDetail1);
            add(perDiemForeignRateDetail2);
            add(perDiemForeignRateDetail3);
            add(perDiemForeignRateDetail4);
            add(perDiemForeignRateDetail5);
            add(perDiemForeignRateDetail6);
            add(perDiemForeignRateDetail7);
            add(perDiemForeignRateDetail8);
            add(perDiemForeignRateDetail9);
            add(perDiemForeignRateDetail10);
            add(perDiemForeignRateDetail11);
        }});
		masterDataRepository.save(perDiemForeignRate);
		
		perDiemForeignRateDetail.setMasterdata(perDiemForeignRate);
		masterDataDetailRepository.save(perDiemForeignRateDetail);
		
		perDiemForeignRateDetail1.setMasterdata(perDiemForeignRate);
		masterDataDetailRepository.save(perDiemForeignRateDetail1);
		
		perDiemForeignRateDetail2.setMasterdata(perDiemForeignRate);
		masterDataDetailRepository.save(perDiemForeignRateDetail2);
		
		perDiemForeignRateDetail3.setMasterdata(perDiemForeignRate);
		masterDataDetailRepository.save(perDiemForeignRateDetail3);
		
		perDiemForeignRateDetail4.setMasterdata(perDiemForeignRate);
		masterDataDetailRepository.save(perDiemForeignRateDetail4);
		
		perDiemForeignRateDetail5.setMasterdata(perDiemForeignRate);
		masterDataDetailRepository.save(perDiemForeignRateDetail5);
		
		perDiemForeignRateDetail6.setMasterdata(perDiemForeignRate);
		masterDataDetailRepository.save(perDiemForeignRateDetail6);
		
		perDiemForeignRateDetail7.setMasterdata(perDiemForeignRate);
		masterDataDetailRepository.save(perDiemForeignRateDetail7);
		
		perDiemForeignRateDetail8.setMasterdata(perDiemForeignRate);
		masterDataDetailRepository.save(perDiemForeignRateDetail8);
		
		perDiemForeignRateDetail9.setMasterdata(perDiemForeignRate);
		masterDataDetailRepository.save(perDiemForeignRateDetail9);
		
		perDiemForeignRateDetail10.setMasterdata(perDiemForeignRate);
		masterDataDetailRepository.save(perDiemForeignRateDetail10);
		
		perDiemForeignRateDetail11.setMasterdata(perDiemForeignRate);
		masterDataDetailRepository.save(perDiemForeignRateDetail11);
		
		/*  M008 : Document Type */
		MasterData documentType	= new MasterData();
		documentType.setCode("M008");
		documentType.setName("Document Type");
		documentType.setFlagOrgCode(false);
		documentType.setNameMessageCode("LABEL_DOCUMENT_TYPE");
		documentType.setDescription("Document Type");
		documentType.setNumberOfColumn(1);
		documentType.setVariableName1("Name");
		
		documentType.setVariableType1("STRING");
		
		MasterDataDetail documentTypeDetail = new MasterDataDetail();
		documentTypeDetail.setCode("APP");
		documentTypeDetail.setFlagActive(true);
		documentTypeDetail.setVariable1("Approve");
		
		MasterDataDetail documentTypeDetail1 = new MasterDataDetail();
		documentTypeDetail1.setCode("ADV");
		documentTypeDetail1.setFlagActive(true);
		documentTypeDetail1.setVariable1("Advance");
		
		MasterDataDetail documentTypeDetail2 = new MasterDataDetail();
		documentTypeDetail2.setCode("EXP");
		documentTypeDetail2.setFlagActive(true);
		documentTypeDetail2.setVariable1("Expense");
		
		MasterDataDetail documentTypeDetail3 = new MasterDataDetail();
		documentTypeDetail3.setCode("MEM");
		documentTypeDetail3.setFlagActive(true);
		documentTypeDetail3.setVariable1("Memo");
		
		documentType.setDetails(new HashSet<MasterDataDetail>(){{
            add(documentTypeDetail);
            add(documentTypeDetail1);
            add(documentTypeDetail2);
            add(documentTypeDetail3);
        }});
		masterDataRepository.save(documentType);
		
		documentTypeDetail.setMasterdata(documentType);
		masterDataDetailRepository.save(documentTypeDetail);
		
		documentTypeDetail1.setMasterdata(documentType);
		masterDataDetailRepository.save(documentTypeDetail1);
		
		documentTypeDetail2.setMasterdata(documentType);
		masterDataDetailRepository.save(documentTypeDetail2);
		
		documentTypeDetail3.setMasterdata(documentType);
		masterDataDetailRepository.save(documentTypeDetail3);
		
		/*  M009 : Document Status */
		MasterData documentStatus	= new MasterData();
		documentStatus.setCode("M009");
		documentStatus.setName("Document Status");
		documentStatus.setFlagOrgCode(false);
		documentStatus.setNameMessageCode("LABEL_DOCUMENT_STATUS");
		documentStatus.setDescription("Document Status");
		documentStatus.setNumberOfColumn(1);
		documentStatus.setVariableName1("Name");
		
		documentStatus.setVariableType1("STRING");
		
		MasterDataDetail documentStatusDetail = new MasterDataDetail();
		documentStatusDetail.setCode("DRF");
		documentStatusDetail.setDescription("Draft");
		documentStatusDetail.setFlagActive(true);
		documentStatusDetail.setVariable1("DRAFT");
		
		MasterDataDetail documentStatusDetail1 = new MasterDataDetail();
		documentStatusDetail1.setCode("ONP");
		documentStatusDetail1.setDescription("On Process");
		documentStatusDetail1.setFlagActive(true);
		documentStatusDetail1.setVariable1("ON_PROCESS");
		
		MasterDataDetail documentStatusDetail2 = new MasterDataDetail();
		documentStatusDetail2.setCode("CCL");
		documentStatusDetail2.setDescription("Cancel");
		documentStatusDetail2.setFlagActive(true);
		documentStatusDetail2.setVariable1("CANCEL");
		
		MasterDataDetail documentStatusDetail3 = new MasterDataDetail();
		documentStatusDetail3.setCode("REJ");
		documentStatusDetail3.setDescription("Reject");
		documentStatusDetail3.setFlagActive(true);
		documentStatusDetail3.setVariable1("REJECT");
		
		MasterDataDetail documentStatusDetail4 = new MasterDataDetail();
		documentStatusDetail4.setCode("CMP");
		documentStatusDetail4.setDescription("Complete");
		documentStatusDetail4.setFlagActive(true);
		documentStatusDetail4.setVariable1("COMPLETE");

		MasterDataDetail documentStatusDetail5 = new MasterDataDetail();
		documentStatusDetail5.setCode("LST");
		documentStatusDetail5.setDescription("Lost");
		documentStatusDetail5.setFlagActive(true);
		documentStatusDetail5.setVariable1("LOST");
		
		documentStatus.setDetails(new HashSet<MasterDataDetail>(){{
            add(documentStatusDetail);
            add(documentStatusDetail1);
            add(documentStatusDetail2);
            add(documentStatusDetail3);
            add(documentStatusDetail4);
            add(documentStatusDetail5);
        }});
		masterDataRepository.save(documentStatus);
		
		documentStatusDetail.setMasterdata(documentStatus);
		masterDataDetailRepository.save(documentStatusDetail);
		
		documentStatusDetail1.setMasterdata(documentStatus);
		masterDataDetailRepository.save(documentStatusDetail1);
		
		documentStatusDetail2.setMasterdata(documentStatus);
		masterDataDetailRepository.save(documentStatusDetail2);
		
		documentStatusDetail3.setMasterdata(documentStatus);
		masterDataDetailRepository.save(documentStatusDetail3);
		
		documentStatusDetail4.setMasterdata(documentStatus);
		masterDataDetailRepository.save(documentStatusDetail4);

		documentStatusDetail5.setMasterdata(documentStatus);
		masterDataDetailRepository.save(documentStatusDetail5);
		
		

		/*  M010 : Car Type */
		MasterData carType	= new MasterData();
		carType.setCode("M010");
		carType.setName("Car Type");
		carType.setFlagOrgCode(false);
		carType.setNameMessageCode("LABEL_CAR_TYPE");
		carType.setDescription("Car Type");
		carType.setNumberOfColumn(2);
		carType.setVariableName1("Path File");
		carType.setVariableName2("Flag Display");
		
		carType.setVariableType1("STRING");
		carType.setVariableType2("STRING");
		
		
		MasterDataDetail carTypeDetail = new MasterDataDetail();
		carTypeDetail.setCode("M101");
		carTypeDetail.setDescription("Van");
		carTypeDetail.setFlagActive(true);
		carTypeDetail.setVariable2("Y");
		
		MasterDataDetail carTypeDetail1 = new MasterDataDetail();
		carTypeDetail1.setCode("M102");
		carTypeDetail1.setDescription("Saloon");
		carTypeDetail1.setFlagActive(true);
		carTypeDetail1.setVariable2("Y");
		
		MasterDataDetail carTypeDetail2 = new MasterDataDetail();
		carTypeDetail2.setCode("M103");
		carTypeDetail2.setDescription("Pickup Tuck");
		carTypeDetail2.setFlagActive(true);
		carTypeDetail2.setVariable2("Y");
		
		MasterDataDetail carTypeDetail3 = new MasterDataDetail();
		carTypeDetail3.setCode("M104");
		carTypeDetail3.setDescription("Other");
		carTypeDetail3.setFlagActive(true);
		carTypeDetail3.setVariable2("Y");
		
		carType.setDetails(new HashSet<MasterDataDetail>(){{
            add(carTypeDetail);
            add(carTypeDetail1);
            add(carTypeDetail2);
            add(carTypeDetail3);
        }});
		masterDataRepository.save(carType);
		
		carTypeDetail.setMasterdata(carType);
		masterDataDetailRepository.save(carTypeDetail);
		
		carTypeDetail1.setMasterdata(carType);
		masterDataDetailRepository.save(carTypeDetail1);
		
		carTypeDetail2.setMasterdata(carType);
		masterDataDetailRepository.save(carTypeDetail2);
		
		carTypeDetail3.setMasterdata(carType);
		masterDataDetailRepository.save(carTypeDetail3);
		
		
		
		/*  M011 : Hotel */
		MasterData hotel	= new MasterData();
		hotel.setCode("M011");
		hotel.setName("Hotel");
		hotel.setFlagOrgCode(false);
		hotel.setNameMessageCode("LABEL_HOTEL");
		hotel.setDescription("Hotel");
		hotel.setVariableName1("Room Rate");
		hotel.setVariableType1("INTEGER");
		hotel.setVariableName2("Is Ibis Hotel");
		hotel.setVariableType2("BOOLEAN");
		hotel.setVariableName3("Room Type");
		hotel.setVariableType3("TEXT");
		hotel.setVariableName4("Special Zone");
		hotel.setVariableType4("TEXT");
		hotel.setNumberOfColumn(4);
		
		MasterDataDetail hotelDetail1 = new MasterDataDetail();
		hotelDetail1.setCode("H001");
		hotelDetail1.setDescription("โรงแรมบุษราคัม ขอนแก่น");
		hotelDetail1.setVariable1("800");
		
		MasterDataDetail hotelDetail2 = new MasterDataDetail();
		hotelDetail2.setCode("H002");
		hotelDetail2.setDescription("โรงแรมเชียงราย");
		hotelDetail2.setVariable1("800");
		
		MasterDataDetail hotelDetail3 = new MasterDataDetail();
		hotelDetail3.setCode("H003");
		hotelDetail3.setDescription("โรงแรมเชียงใหม่");
		hotelDetail3.setVariable1("800");
		
		MasterDataDetail hotelDetail4 = new MasterDataDetail();
		hotelDetail4.setCode("H004");
		hotelDetail4.setDescription("โรงแรมหาดใหญ่");
		hotelDetail4.setVariable1("800");


		MasterDataDetail hotelDetail5 = new MasterDataDetail();
		hotelDetail5.setCode("H005");
		hotelDetail5.setDescription("โรงแรมไอบิส");
		hotelDetail5.setVariable1("1000");
		hotelDetail5.setVariable2("true");
		hotelDetail5.setVariable3("Standard");
		hotelDetail5.setVariable4("A");

		MasterDataDetail hotelDetail6 = new MasterDataDetail();
		hotelDetail6.setCode("Other");
		hotelDetail6.setDescription("อื่นๆ");


		hotel.setDetails(new HashSet<MasterDataDetail>(){{
            add(hotelDetail1);
            add(hotelDetail2);
            add(hotelDetail3);
            add(hotelDetail4);
            add(hotelDetail5);
            add(hotelDetail6);
        }});
		masterDataRepository.save(hotel);
		
		hotelDetail1.setMasterdata(hotel);
		masterDataDetailRepository.save(hotelDetail1);
		
		hotelDetail2.setMasterdata(hotel);
		masterDataDetailRepository.save(hotelDetail2);
		
		hotelDetail3.setMasterdata(hotel);
		masterDataDetailRepository.save(hotelDetail3);
		
		hotelDetail4.setMasterdata(hotel);
		masterDataDetailRepository.save(hotelDetail4);

		hotelDetail5.setMasterdata(hotel);
		masterDataDetailRepository.save(hotelDetail5);

		hotelDetail6.setMasterdata(hotel);
		masterDataDetailRepository.save(hotelDetail6);
		
		
		
		/*  M013 : Distance Rate */
		MasterData rateDistance	= new MasterData();
		rateDistance.setCode("M013");
		rateDistance.setName("Distance Rate");
		rateDistance.setFlagOrgCode(true);
		rateDistance.setNameMessageCode("LABEL_RATE_DISTANCE");
		rateDistance.setDescription("Distance Rate");
		rateDistance.setNumberOfColumn(2);

		rateDistance.setVariableName1("Distance_1");
		rateDistance.setVariableName2("RateValue_1");

		rateDistance.setVariableType1("TEXT");
		rateDistance.setVariableType2("INTEGER");

		MasterDataDetail rateDistanceDetail_1_1 = new MasterDataDetail();
		rateDistanceDetail_1_1.setCode("RD001");
		rateDistanceDetail_1_1.setName("Mitphol Saraburi");
		rateDistanceDetail_1_1.setOrgCode("1010");
		rateDistanceDetail_1_1.setVariable1("50");
		rateDistanceDetail_1_1.setVariable2("8");

		MasterDataDetail rateDistanceDetail_1_2 = new MasterDataDetail();
		rateDistanceDetail_1_2.setCode("RD002");
		rateDistanceDetail_1_2.setName("Mitphol Saraburi");
		rateDistanceDetail_1_2.setOrgCode("1010");
		rateDistanceDetail_1_2.setVariable1("80");
		rateDistanceDetail_1_2.setVariable2("3");

		MasterDataDetail rateDistanceDetail_1_3 = new MasterDataDetail();
		rateDistanceDetail_1_3.setCode("RD003");
		rateDistanceDetail_1_3.setName("Mitphol Saraburi");
		rateDistanceDetail_1_3.setOrgCode("1010");
		rateDistanceDetail_1_3.setVariable1("OtherRate");
		rateDistanceDetail_1_3.setVariable2("1");
		
        //= Detail For CompanyCode = 10020 =
		MasterDataDetail rateDistanceDetail_2_1 = new MasterDataDetail();
		rateDistanceDetail_2_1.setCode("RD004");
		rateDistanceDetail_2_1.setName("Mitphol Head Office");
		rateDistanceDetail_2_1.setOrgCode("10020");
		rateDistanceDetail_2_1.setVariable1("100");
		rateDistanceDetail_2_1.setVariable2("15");

		MasterDataDetail rateDistanceDetail_2_2 = new MasterDataDetail();
		rateDistanceDetail_2_2.setCode("RD005");
		rateDistanceDetail_2_2.setName("Mitphol Head Office");
		rateDistanceDetail_2_2.setOrgCode("10020");
		rateDistanceDetail_2_2.setVariable1("OtherRate");
		rateDistanceDetail_2_2.setVariable2("10");

		//= Detail For CompanyCode = 10020 =
		MasterDataDetail rateDistanceDetail_3_1 = new MasterDataDetail();
		rateDistanceDetail_3_1.setCode("RD006");
		rateDistanceDetail_3_1.setName("Mitphol Singburi");
		rateDistanceDetail_3_1.setOrgCode("10000");
		rateDistanceDetail_3_1.setVariable1("OtherRate");
		rateDistanceDetail_3_1.setVariable2("9");

		rateDistance.setDetails(new HashSet<MasterDataDetail>(){{
			add(rateDistanceDetail_1_1);
			add(rateDistanceDetail_1_2);
			add(rateDistanceDetail_1_3);
			add(rateDistanceDetail_2_1);
			add(rateDistanceDetail_2_2);
			add(rateDistanceDetail_3_1);

		}});

		masterDataRepository.save(rateDistance);

		rateDistanceDetail_1_1.setMasterdata(rateDistance);
		rateDistanceDetail_1_2.setMasterdata(rateDistance);
		rateDistanceDetail_1_3.setMasterdata(rateDistance);
		rateDistanceDetail_2_1.setMasterdata(rateDistance);
		rateDistanceDetail_2_2.setMasterdata(rateDistance);
		rateDistanceDetail_3_1.setMasterdata(rateDistance);

		masterDataDetailRepository.save(rateDistanceDetail_1_1);
		masterDataDetailRepository.save(rateDistanceDetail_1_3);
		masterDataDetailRepository.save(rateDistanceDetail_1_2);
		masterDataDetailRepository.save(rateDistanceDetail_2_1);
		masterDataDetailRepository.save(rateDistanceDetail_2_2);
		masterDataDetailRepository.save(rateDistanceDetail_3_1);

		/*  M014 : Airline */
		MasterData airline	= new MasterData();
		airline.setCode("M014");
		airline.setName("Airline");
		airline.setFlagOrgCode(false);
		airline.setNameMessageCode("LABEL_AIR_LINE");
		airline.setDescription("Airline");
				
		MasterDataDetail airlineDetail1 = new MasterDataDetail();
		airlineDetail1.setCode("A001");
		airlineDetail1.setDescription("สายการบินนกแอร์");
		
		MasterDataDetail airlineDetail2 = new MasterDataDetail();
		airlineDetail2.setCode("A002");
		airlineDetail2.setDescription("สายการบินแอร์เอเชีย");
		
		MasterDataDetail airlineDetail3 = new MasterDataDetail();
		airlineDetail3.setCode("A003");
		airlineDetail3.setDescription("สายการบินBankok Airways");
		
		MasterDataDetail airlineDetail4 = new MasterDataDetail();
		airlineDetail4.setCode("A004");
		airlineDetail4.setDescription("สายการบินการบินไทย");
		
		airline.setDetails(new HashSet<MasterDataDetail>(){{
            add(airlineDetail1);
            add(airlineDetail2);
            add(airlineDetail3);
            add(airlineDetail4);
        }});
		masterDataRepository.save(airline);
		
		airlineDetail1.setMasterdata(airline);
		masterDataDetailRepository.save(airlineDetail1);
		
		airlineDetail2.setMasterdata(airline);
		masterDataDetailRepository.save(airlineDetail2);
		
		airlineDetail3.setMasterdata(airline);
		masterDataDetailRepository.save(airlineDetail3);
		
		airlineDetail4.setMasterdata(airline);
		masterDataDetailRepository.save(airlineDetail4);
		
		
		
		
		/*  M015 : Attachment Type */
		MasterData attachmentType = new MasterData();
		attachmentType.setCode("M015");
		attachmentType.setName("Attachment Type");
		attachmentType.setFlagOrgCode(false);
		attachmentType.setNameMessageCode("LABEL_ATTACHMENT_TYPE");
		attachmentType.setDescription("Attachment Type");
		attachmentType.setNumberOfColumn(1);
		attachmentType.setVariableName1("Extension File");
		
		attachmentType.setVariableType1("STRING");
		
		MasterDataDetail attachmentTypeDetail = new MasterDataDetail();
		attachmentTypeDetail.setCode("M151");
		attachmentTypeDetail.setDescription("Receipt / ใบเสร็จรับเงิน");
		attachmentTypeDetail.setName("Receipt");
		attachmentTypeDetail.setVariable1("pdf,jpg,JPG,png,PNG");
		
		MasterDataDetail attachmentTypeDetail1 = new MasterDataDetail();
		attachmentTypeDetail1.setCode("M152");
		attachmentTypeDetail1.setDescription("Detail/ รายละเอียดประกอบ");
		attachmentTypeDetail1.setName("Detail");
		attachmentTypeDetail1.setVariable1("pdf,jpg,JPG,png,PNG");
		
		MasterDataDetail attachmentTypeDetail2 = new MasterDataDetail();
		attachmentTypeDetail2.setCode("M153");
		attachmentTypeDetail2.setDescription("Attendee list / รายชื่อผู้เข้าร่วมประชุม");
		attachmentTypeDetail2.setName("Attendee list");
		attachmentTypeDetail2.setVariable1("pdf,jpg,JPG,png,PNG");
		
		MasterDataDetail attachmentTypeDetail3 = new MasterDataDetail();
		attachmentTypeDetail3.setCode("M154");
		attachmentTypeDetail3.setDescription("Course details / รายละเอียดหลักสูตร");
		attachmentTypeDetail3.setName("Course details");
		attachmentTypeDetail3.setVariable1("pdf,jpg,JPG,png,PNG");
		
		MasterDataDetail attachmentTypeDetail4 = new MasterDataDetail();
		attachmentTypeDetail4.setCode("M155");
		attachmentTypeDetail4.setDescription("MEMO / Memo อนุมัติโดย HR เห็นชอบ");
		attachmentTypeDetail4.setName("MEMO");
		attachmentTypeDetail4.setVariable1("pdf,jpg,JPG,png,PNG");

		MasterDataDetail attachmentTypeDetail5 = new MasterDataDetail();
		attachmentTypeDetail5.setCode("M156");
		attachmentTypeDetail5.setDescription("E-Ticket / ตั๋วเครื่องบิน");
		attachmentTypeDetail5.setName("E-Ticket");
		attachmentTypeDetail5.setVariable1("pdf,jpg,JPG,png,PNG");


		MasterDataDetail attachmentTypeDetail6 = new MasterDataDetail();
		attachmentTypeDetail6.setCode("M157");
		attachmentTypeDetail6.setDescription("Repair invoiced / ใบแจ้งซ่อม");
		attachmentTypeDetail6.setName("Repair invoiced");
		attachmentTypeDetail6.setVariable1("pdf,jpg,JPG,png,PNG");

		MasterDataDetail attachmentTypeDetail7 = new MasterDataDetail();
		attachmentTypeDetail7.setCode("M158");
		attachmentTypeDetail7.setDescription("Fingerprint Scan ESS / ใบสแกนลายนิ้วมือ ESS");
		attachmentTypeDetail7.setName("Fingerprint Scan ESS");
		attachmentTypeDetail7.setVariable1("pdf,jpg,JPG,png,PNG");

		MasterDataDetail attachmentTypeDetail8 = new MasterDataDetail();
		attachmentTypeDetail8.setCode("M159");
		attachmentTypeDetail8.setDescription("Boarding Pass / ตั๋วโดยสาร");
		attachmentTypeDetail8.setName("Boarding Pass");
		attachmentTypeDetail8.setVariable1("pdf,jpg,JPG,png,PNG");

		attachmentType.setDetails(new HashSet<MasterDataDetail>(){{
			add(attachmentTypeDetail);
			add(attachmentTypeDetail1);
			add(attachmentTypeDetail2);
			add(attachmentTypeDetail3);
			add(attachmentTypeDetail4);
			add(attachmentTypeDetail5);
			add(attachmentTypeDetail6);
			add(attachmentTypeDetail7);
			add(attachmentTypeDetail8);
		}});

		masterDataRepository.save(attachmentType);
		
		attachmentTypeDetail.setMasterdata(attachmentType);
		masterDataDetailRepository.save(attachmentTypeDetail);
		
		attachmentTypeDetail1.setMasterdata(attachmentType);
		masterDataDetailRepository.save(attachmentTypeDetail1);
		
		attachmentTypeDetail2.setMasterdata(attachmentType);
		masterDataDetailRepository.save(attachmentTypeDetail2);
		
		attachmentTypeDetail3.setMasterdata(attachmentType);
		masterDataDetailRepository.save(attachmentTypeDetail3);
		
		attachmentTypeDetail4.setMasterdata(attachmentType);
		masterDataDetailRepository.save(attachmentTypeDetail4);

		attachmentTypeDetail5.setMasterdata(attachmentType);
		masterDataDetailRepository.save(attachmentTypeDetail5);

		attachmentTypeDetail6.setMasterdata(attachmentType);
		masterDataDetailRepository.save(attachmentTypeDetail6);

		attachmentTypeDetail7.setMasterdata(attachmentType);
		masterDataDetailRepository.save(attachmentTypeDetail7);

		attachmentTypeDetail8.setMasterdata(attachmentType);
		masterDataDetailRepository.save(attachmentTypeDetail8);
		

		/*  M016 : Zone Location Code */
		MasterData zoneCode = new MasterData();
		zoneCode.setCode("M016");
		zoneCode.setName("Zone Location Code");
		zoneCode.setFlagOrgCode(false);
		zoneCode.setNameMessageCode("LABEL_ZONE_CODE");
		zoneCode.setDescription("Zone Location Code");
		
		MasterDataDetail zoneCodeDetail1 = new MasterDataDetail();
		zoneCodeDetail1.setCode("EU");
		zoneCodeDetail1.setDescription("แถบทวีปอเมริกา");
		
		MasterDataDetail zoneCodeDetail2 = new MasterDataDetail();
		zoneCodeDetail2.setCode("SA");
		zoneCodeDetail2.setDescription("แถบทวีปอเมริกาใต้");
		
		MasterDataDetail zoneCodeDetail3 = new MasterDataDetail();
		zoneCodeDetail3.setCode("OT");
		zoneCodeDetail3.setDescription("กลุ่มประเทศอื่นๆ");
		
		MasterDataDetail zoneCodeDetail4 = new MasterDataDetail();
		zoneCodeDetail4.setCode("MM");
		zoneCodeDetail4.setDescription("กลุ่มอินโดจีน");
		
		zoneCode.setDetails(new HashSet<MasterDataDetail>(){{
            add(zoneCodeDetail1);
            add(zoneCodeDetail2);
            add(zoneCodeDetail3);
            add(zoneCodeDetail4);
        }});
		masterDataRepository.save(zoneCode);
		
		zoneCodeDetail1.setMasterdata(zoneCode);
		masterDataDetailRepository.save(zoneCodeDetail1);
		
		zoneCodeDetail2.setMasterdata(zoneCode);
		masterDataDetailRepository.save(zoneCodeDetail2);
		
		zoneCodeDetail3.setMasterdata(zoneCode);
		masterDataDetailRepository.save(zoneCodeDetail3);
		
		zoneCodeDetail4.setMasterdata(zoneCode);
		masterDataDetailRepository.save(zoneCodeDetail4);

		/*  M005 : Request Status */
		MasterData requestStatus	= new MasterData();
		requestStatus.setCode("M005");
		requestStatus.setName("Request Status");
		requestStatus.setFlagOrgCode(false);
		requestStatus.setNameMessageCode("LABEL_REQUEST_STATUS");
		requestStatus.setDescription("Request Status");
		requestStatus.setNumberOfColumn(1);
		requestStatus.setVariableName1("Name");

		requestStatus.setVariableType1("STRING");

		MasterDataDetail requestStatusDetail = new MasterDataDetail();
		requestStatusDetail.setCode("INI");
		requestStatusDetail.setDescription("Initial");
		requestStatusDetail.setFlagActive(true);
		requestStatusDetail.setVariable1("INITIAL");

		MasterDataDetail requestStatusDetail1 = new MasterDataDetail();
		requestStatusDetail1.setCode("CRT");
		requestStatusDetail1.setDescription("Create");
		requestStatusDetail1.setFlagActive(true);
		requestStatusDetail1.setVariable1("CREATE");

		MasterDataDetail requestStatusDetail2 = new MasterDataDetail();
		requestStatusDetail2.setCode("VRF");
		requestStatusDetail2.setDescription("Verify");
		requestStatusDetail2.setFlagActive(true);
		requestStatusDetail2.setVariable1("VERIFY");

		MasterDataDetail requestStatusDetail3 = new MasterDataDetail();
		requestStatusDetail3.setCode("CXL");
		requestStatusDetail3.setDescription("Cancel");
		requestStatusDetail3.setFlagActive(true);
		requestStatusDetail3.setVariable1("CANCEL");

		MasterDataDetail requestStatusDetail4 = new MasterDataDetail();
		requestStatusDetail4.setCode("REJ");
		requestStatusDetail4.setDescription("Reject");
		requestStatusDetail4.setFlagActive(true);
		requestStatusDetail4.setVariable1("REJECT");

		MasterDataDetail requestStatusDetail5 = new MasterDataDetail();
		requestStatusDetail5.setCode("APR");
		requestStatusDetail5.setDescription("Approve");
		requestStatusDetail5.setFlagActive(true);
		requestStatusDetail5.setVariable1("APPROVE");

		MasterDataDetail requestStatusDetail6 = new MasterDataDetail();
		requestStatusDetail6.setCode("HR");
		requestStatusDetail6.setDescription("HR");
		requestStatusDetail6.setFlagActive(true);
		requestStatusDetail6.setVariable1("HR");

		MasterDataDetail requestStatusDetail7 = new MasterDataDetail();
		requestStatusDetail7.setCode("AD");
		requestStatusDetail7.setDescription("Admin");
		requestStatusDetail7.setFlagActive(true);
		requestStatusDetail7.setVariable1("ADMIN");

		MasterDataDetail requestStatusDetail8 = new MasterDataDetail();
		requestStatusDetail8.setCode("ACC");
		requestStatusDetail8.setDescription("Account");
		requestStatusDetail8.setFlagActive(true);
		requestStatusDetail8.setVariable1("ACCOUNT");

		MasterDataDetail requestStatusDetail9 = new MasterDataDetail();
		requestStatusDetail9.setCode("SAP");
		requestStatusDetail9.setDescription("SAP");
		requestStatusDetail9.setFlagActive(true);
		requestStatusDetail9.setVariable1("SAP");

		MasterDataDetail requestStatusDetail10 = new MasterDataDetail();
		requestStatusDetail10.setCode("CMP");
		requestStatusDetail10.setDescription("Complete");
		requestStatusDetail10.setFlagActive(true);
		requestStatusDetail10.setVariable1("COMPLETE");

		requestStatus.setDetails(new HashSet<MasterDataDetail>(){{
			add(requestStatusDetail);
			add(requestStatusDetail1);
			add(requestStatusDetail2);
			add(requestStatusDetail3);
			add(requestStatusDetail4);
			add(requestStatusDetail5);
			add(requestStatusDetail6);
			add(requestStatusDetail7);
			add(requestStatusDetail8);
			add(requestStatusDetail9);
			add(requestStatusDetail10);
		}});
		masterDataRepository.save(requestStatus);

		requestStatusDetail.setMasterdata(requestStatus);
		masterDataDetailRepository.save(requestStatusDetail);

		requestStatusDetail1.setMasterdata(requestStatus);
		masterDataDetailRepository.save(requestStatusDetail1);

		requestStatusDetail2.setMasterdata(requestStatus);
		masterDataDetailRepository.save(requestStatusDetail2);

		requestStatusDetail3.setMasterdata(requestStatus);
		masterDataDetailRepository.save(requestStatusDetail3);

		requestStatusDetail4.setMasterdata(requestStatus);
		masterDataDetailRepository.save(requestStatusDetail4);

		requestStatusDetail5.setMasterdata(requestStatus);
		masterDataDetailRepository.save(requestStatusDetail5);

		requestStatusDetail6.setMasterdata(requestStatus);
		masterDataDetailRepository.save(requestStatusDetail6);

		requestStatusDetail7.setMasterdata(requestStatus);
		masterDataDetailRepository.save(requestStatusDetail7);

		requestStatusDetail8.setMasterdata(requestStatus);
		masterDataDetailRepository.save(requestStatusDetail8);

		requestStatusDetail9.setMasterdata(requestStatus);
		masterDataDetailRepository.save(requestStatusDetail9);

		requestStatusDetail10.setMasterdata(requestStatus);
		masterDataDetailRepository.save(requestStatusDetail10);

		/*  M006 : Action Reason */
		MasterData actionReason	= new MasterData();
		actionReason.setCode("M006");
		actionReason.setName("Action Reason");
		actionReason.setFlagOrgCode(false);
		actionReason.setNameMessageCode("LABEL_ACTION_REASON");
		actionReason.setDescription("Action Reason");
		actionReason.setNumberOfColumn(1);
		actionReason.setVariableName1("Action Type");

		actionReason.setVariableType1("STRING");

		MasterDataDetail actionReasonDetail1 = new MasterDataDetail();
		actionReasonDetail1.setCode("APP001");
		actionReasonDetail1.setDescription("เอกสารครบถ้วน แต่ไม่มีรถว่าง");
		actionReasonDetail1.setFlagActive(true);
		actionReasonDetail1.setVariable1("Complete");

		MasterDataDetail actionReasonDetail2 = new MasterDataDetail();
		actionReasonDetail2.setCode("REJ001");
		actionReasonDetail2.setDescription("กรอกข้อมูลไม่ถูกต้อง");
		actionReasonDetail2.setFlagActive(true);
		actionReasonDetail2.setVariable1("Reject");

		MasterDataDetail actionReasonDetail3 = new MasterDataDetail();
		actionReasonDetail3.setCode("REJ002");
		actionReasonDetail3.setDescription("เอกสารแนบไม่ถูกต้อง");
		actionReasonDetail3.setFlagActive(true);
		actionReasonDetail3.setVariable1("Reject");

		actionReason.setDetails(new HashSet<MasterDataDetail>(){{
			add(actionReasonDetail1);
			add(actionReasonDetail2);
			add(actionReasonDetail3);
		}});
		masterDataRepository.save(actionReason);

		actionReasonDetail1.setMasterdata(actionReason);
		masterDataDetailRepository.save(actionReasonDetail1);

		actionReasonDetail2.setMasterdata(actionReason);
		masterDataDetailRepository.save(actionReasonDetail2);

		actionReasonDetail3.setMasterdata(actionReason);
		masterDataDetailRepository.save(actionReasonDetail3);

		/*  M017 : Car Licence */
		MasterData carLicence = new MasterData();
		carLicence.setCode("M017");
		carLicence.setName("Car Licence");
		carLicence.setFlagOrgCode(false);
		carLicence.setNameMessageCode("LABEL_CAR_LICENCE");
		carLicence.setDescription("Car Licence");
		carLicence.setNumberOfColumn(1);
		carLicence.setVariableName1("Car Licence");
		carLicence.setVariableType1("STRING");

		MasterDataDetail carLicenceDetail1 = new MasterDataDetail();
		carLicenceDetail1.setCode("CL001");
		carLicenceDetail1.setDescription("กฏ5523");
		carLicenceDetail1.setFlagActive(true);

		MasterDataDetail carLicenceDetail2 = new MasterDataDetail();
		carLicenceDetail2.setCode("CL002");
		carLicenceDetail2.setDescription("อื่นๆ");
		carLicenceDetail2.setFlagActive(true);

		carLicence.setDetails(new HashSet<MasterDataDetail>(){{
			add(carLicenceDetail1);
			add(carLicenceDetail2);
		}});
		masterDataRepository.save(carLicence);

		carLicenceDetail1.setMasterdata(carLicence);
		masterDataDetailRepository.save(carLicenceDetail1);

		carLicenceDetail2.setMasterdata(carLicence);
		masterDataDetailRepository.save(carLicenceDetail2);

		/*  M018 : Subvention */
		MasterData subvention = new MasterData();
		subvention.setCode("M018");
		subvention.setName("Subvention");
		subvention.setFlagOrgCode(false);
		subvention.setNameMessageCode("LABEL_SUBVENTION");
		subvention.setDescription("Subvention");
		subvention.setNumberOfColumn(1);
		subvention.setVariableName1("Name");
		subvention.setVariableType1("STRING");

		MasterDataDetail subventionDetail1 = new MasterDataDetail();
		subventionDetail1.setCode("SV001");
		subventionDetail1.setDescription("ค่าของขวัญเยี่ยมไข้");
		subventionDetail1.setFlagActive(true);

		MasterDataDetail subventionDetail2 = new MasterDataDetail();
		subventionDetail2.setCode("SV002");
		subventionDetail2.setDescription("อื่นๆ");
		subventionDetail2.setFlagActive(true);

		subvention.setDetails(new HashSet<MasterDataDetail>(){{
			add(subventionDetail1);
			add(subventionDetail2);
		}});
		masterDataRepository.save(subvention);

		subventionDetail1.setMasterdata(subvention);
		masterDataDetailRepository.save(subventionDetail1);

		subventionDetail2.setMasterdata(subvention);
		masterDataDetailRepository.save(subventionDetail2);
		
		/*  M019 : Branch by PSA */
		MasterData branceByPSA = new MasterData();
		branceByPSA.setCode("M019");
		branceByPSA.setName("Branch by PSA");
		branceByPSA.setFlagOrgCode(false);
		branceByPSA.setNameMessageCode("LABEL_MASTER_DATA");
		branceByPSA.setDescription("Branch by PSA");
		branceByPSA.setNumberOfColumn(1);
		branceByPSA.setVariableName1("Branch");
		branceByPSA.setVariableType1("STRING");

		MasterDataDetail d1010_0001_Detail1 = new MasterDataDetail("1010-0001", "เพิ่มผลผลิต-ด่านช้าง", "เพิ่มผลผลิต-ด่านช้าง",  "1010","0001","0000", "SCB03", "0140216", "ธ.ไทยพาณิชย์ จำกัด (มหาชน) / เพลินจิตเซ็นเตอร์","1C21", "1010702", "2163002692","CA-SCB-เพลินจิตเซ็นเตอร์-2163002692");
		


		branceByPSA.setDetails(new HashSet<MasterDataDetail>(){{
			add(d1010_0001_Detail1);
		}});
		masterDataRepository.save(branceByPSA);

		d1010_0001_Detail1.setMasterdata(branceByPSA);
		masterDataDetailRepository.save(d1010_0001_Detail1);

		/*  M020 : User Relation */
		MasterData user_relation = new MasterData();
		user_relation.setCode("M020");
		user_relation.setName("User Relation");
		user_relation.setNameMessageCode("LABEL_USER_RELATION");
		user_relation.setFlagActive(false);
		user_relation.setDescription("User relation");
		user_relation.setNumberOfColumn(1);
		user_relation.setVariableName1("Username");
		user_relation.setVariableType1("STRING");
		masterDataRepository.save(user_relation);


		MasterDataDetail user_head = new MasterDataDetail();
		user_head.setName("User Name");
		user_head.setCode("vorakita");
		user_head.setVariable1("praphaiporni");
		user_head.setMasterdata(user_relation);
		masterDataDetailRepository.save(user_head);

		/*  M021 : Per diem Rate Type */
		MasterData perDiemRateType = new MasterData();
		perDiemRateType.setCode("M021");
		perDiemRateType.setName("Per Diem Rate Type");
		perDiemRateType.setNameMessageCode("LABEL_PER_DIEM_RATE_TYPE");
		perDiemRateType.setFlagActive(false);
		perDiemRateType.setDescription("Per Diem Rate Type");
		perDiemRateType.setNumberOfColumn(1);
		perDiemRateType.setVariableName1("type");
		perDiemRateType.setVariableType1("STRING");
		perDiemRateType.setVariableName2("rate");
		perDiemRateType.setVariableType2("INTEGER");
		masterDataRepository.save(perDiemRateType);

		MasterDataDetail domesticRate = new MasterDataDetail();
		domesticRate.setCode("DR001");
		domesticRate.setVariable1("D");
		domesticRate.setVariable2("70");
		domesticRate.setMasterdata(perDiemRateType);
		masterDataDetailRepository.save(domesticRate);

		MasterDataDetail domesticRate1 = new MasterDataDetail();
		domesticRate1.setCode("DR002");
		domesticRate1.setVariable1("D");
		domesticRate1.setVariable2("100");
		domesticRate1.setMasterdata(perDiemRateType);
		masterDataDetailRepository.save(domesticRate1);

		MasterDataDetail foreignRate = new MasterDataDetail();
		foreignRate.setCode("FR001");
		foreignRate.setVariable1("F");
		foreignRate.setVariable2("30");
		foreignRate.setMasterdata(perDiemRateType);
		masterDataDetailRepository.save(foreignRate);

		MasterDataDetail foreignRate1 = new MasterDataDetail();
		foreignRate1.setCode("FR002");
		foreignRate1.setVariable1("F");
		foreignRate1.setVariable2("70");
		foreignRate1.setMasterdata(perDiemRateType);
		masterDataDetailRepository.save(foreignRate1);

		MasterDataDetail foreignRate2 = new MasterDataDetail();
		foreignRate2.setCode("FR003");
		foreignRate2.setVariable1("F");
		foreignRate2.setVariable2("100");
		foreignRate2.setMasterdata(perDiemRateType);
		masterDataDetailRepository.save(foreignRate2);
	}


}