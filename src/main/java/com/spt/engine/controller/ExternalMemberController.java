package com.spt.engine.controller;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.entity.app.DocumentApproveItem;
import com.spt.engine.entity.app.ExternalMember;
import com.spt.engine.repository.app.ExternalMemberRepository;

import flexjson.JSONSerializer;

@RestController
@RequestMapping("/externalMemberCustom")
public class ExternalMemberController {

	static final Logger LOGGER = LoggerFactory.getLogger(ExternalMemberController.class);
	
	@Autowired
	EntityManager entityManager;
	
	@Autowired
	ExternalMemberRepository externalMemberRepository;
	
	@Autowired 
    ObjectMapper objectMapper;
	
	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };
    
    protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();
    
    @Transactional
	@PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public void saveExternalMember(@RequestBody ExternalMember externalMember,
								 @RequestParam("documentApproveItem") Long documentApproveItem
								){
	
		LOGGER.info("><><>< SAVE AND UPDATE EXTERNAL MEMBER ><><><");
		try{
			if(externalMember.getId() != null){
				LOGGER.info("><><>< EXTERNAL MEMBER ID IS NOT NULL ><><><");
				
				ExternalMember externalMember2 = externalMemberRepository.findOne(externalMember.getId());
				ExternalMember member = objectMapper.readerForUpdating(externalMember2).readValue(gson.toJson(externalMember));
	            
	            entityManager.merge(member);
	            entityManager.flush();
	            
			}else{
				
				LOGGER.info("><><>< EXTERNAL MEMBER ID IS  NULL ><><><");
							
				DocumentApproveItem documentAppItem = entityManager.find(DocumentApproveItem.class, documentApproveItem);
				
				documentAppItem.setExternalMembers(new HashSet<ExternalMember>(){{
		            add(externalMember);
		        }});
				
				entityManager.persist(documentAppItem);
							
				externalMember.setDocumentApproveItem(documentAppItem);
				entityManager.persist(externalMember);	
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		LOGGER.info("><><>< AFTER PERSIST UPDATE EXTERNAL MEMBER ><><>< ");
	}
	
	@GetMapping(value="/getExternalMember/{id}/externalMembers",produces = "application/json; charset=utf-8")
    public String getExternalMember(@PathVariable Long id) {

		List<Long> documentApproveItemLs = Arrays.asList(id);
		List<ExternalMember> externalMembers = externalMemberRepository.findByDocumentApproveItemId(documentApproveItemLs);
		
		LOGGER.info("externalMembers = {} ",externalMembers.size());
		
		return (new JSONSerializer().exclude("*.class")
				.include("id")
				.include("memberName")
				.include("companyMember")
				.include("phoneNumber")
				.include("email")
				.include("remark").serialize(externalMembers));
    }
}
