package com.spt.engine.controller;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.entity.app.DocumentApproveItem;
import com.spt.engine.entity.app.Location;
import com.spt.engine.entity.app.TravelDetail;
import com.spt.engine.repository.app.LocationRepository;
import com.spt.engine.repository.app.TravelDetailRepository;

import flexjson.JSONSerializer;

@RestController
@RequestMapping("/travelDetailCustom")
public class TravelDetailController {

	static final Logger LOGGER = LoggerFactory.getLogger(TravelDetailController.class);

	@Autowired
	EntityManager entityManager;

	@Autowired
	LocationRepository locationRepository;

	@Autowired
	TravelDetailRepository travelDetailRepository;

	@Autowired
	ObjectMapper objectMapper;

	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
		public Date deserialize(JsonElement json, Type typeOfT,
								JsonDeserializationContext context) throws JsonParseException {
			return json == null ? null : new Date(json.getAsLong());
		}
	};


	protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();

	@Transactional
	@PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public void saveTravelDetail(@RequestBody TravelDetail travelDetail,
								 @RequestParam("origin") Location origin,
								 @RequestParam("destination") Long destination,
								 @RequestParam("documentApproveItem") Long documentApproveItem
	) {

		LOGGER.info("><><>< SAVE AND UPDATE TRAVEL DETAIL ><><><");

		try {
			Location destination1 = locationRepository.findOne(destination);

			if (travelDetail.getId() != null) {
				LOGGER.info("><><>< MERGE TRAVEL DETAIL ><><><");

				TravelDetail travelDetail2 = travelDetailRepository.findOne(travelDetail.getId());
				TravelDetail detail = objectMapper.readerForUpdating(travelDetail2).readValue(gson.toJson(travelDetail));

				detail.setOrigin(origin);
				detail.setDestination(destination1);

				entityManager.merge(detail);
				entityManager.flush();

			} else {

				LOGGER.info("><><>< PERSIST TRAVEL DETAIL ><><><");

				travelDetail.setOrigin(origin);
				travelDetail.setDestination(destination1);

				DocumentApproveItem documentAppItem = entityManager.find(DocumentApproveItem.class, documentApproveItem);

				documentAppItem.setTravelDetails(new HashSet<TravelDetail>() {{
					add(travelDetail);
				}});

				entityManager.persist(documentAppItem);

				travelDetail.setDocumentApproveItem(documentAppItem);
				entityManager.persist(travelDetail);
			}
			LOGGER.info("><><>< AFTER PERSIST AND MERGE TRAVEL DETAIL ><><>< ");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@GetMapping(value = "/getTravelDetail/{id}/travelDetails", produces = "application/json; charset=utf-8")
	public String getTravelDetail(@PathVariable Long id) {

		List<Long> documentApproveItemId = Arrays.asList(id);
		List<TravelDetail> travelDetailLs = travelDetailRepository.findByDocumentApproveItemId(documentApproveItemId);

		LOGGER.info("travelDetailLs = {} ", travelDetailLs.size());

		return (new JSONSerializer().exclude("*.class").include("id")
				.include("startTime")
				.include("endTime")
				.include("remark")
				.include("travelDays").serialize(travelDetailLs));
	}

	@Transactional
	@PostMapping(value = "/validateDateTimeOverlap/", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public String validateDateTimeOverlap(@RequestBody Map<String,String> jsonDocument) {

		String result = "FAIL";

		LOGGER.info("><<><><>< jsonDocument  : {} ",jsonDocument);

		String startDate = String.valueOf(jsonDocument.get("dateStartData"));
		String startTime = String.valueOf(jsonDocument.get("timeStartData"));
		String endDate = String.valueOf(jsonDocument.get("dateEndData"));
		String endTime = String.valueOf(jsonDocument.get("timeEndData"));
		String requester = String.valueOf(jsonDocument.get("requester"));
		String docNumberJson = String.valueOf(jsonDocument.get("docNumber"));

		String startDateTime = startDate + " " + startTime + ":00.0";
		String endDateTime = endDate + " " + endTime + ":00.0";

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		Date startDateTimeParsed = null;
		Date endDateTimeParsed = null;
		try {
			startDateTimeParsed = dateFormat.parse(startDateTime);
			endDateTimeParsed = dateFormat.parse(endDateTime);
		} catch (ParseException e) {
			result = "FAIL";
			e.printStackTrace();
		}
		Timestamp timestampStartDate = new Timestamp(startDateTimeParsed.getTime());
		Timestamp timestampEndDate = new Timestamp(endDateTimeParsed.getTime());

		LOGGER.info("><><>< timestampStartDate : {} ",timestampStartDate);
		LOGGER.info("><><>< timestampEndDate : {} ",timestampEndDate);

		List<String> detailStartLs 		= travelDetailRepository.validateTimeOverlapByDocIsActiveOrCompleteWithStartDate(timestampStartDate.toString(), requester);
		List<String> detailEndLs   		= travelDetailRepository.validateTimeOverlapByDocIsActiveOrCompleteWithEndDate(timestampEndDate.toString(), requester);
		List<String> detailDraftStartLs = travelDetailRepository.validateTimeOverlapByDocIsActiveOrCompleteStartDateWithDocNumber(timestampStartDate.toString(), requester,docNumberJson);
		List<String> detailDraftEndLs   = travelDetailRepository.validateTimeOverlapByDocIsActiveOrCompleteEndDateWithDocNumber(timestampEndDate.toString(), requester,docNumberJson);

		Set<String> docNumberSet = new HashSet<>();
		if(detailStartLs.size() == 0 && detailEndLs.size() == 0 && detailDraftStartLs.size() == 0 && detailDraftEndLs.size() == 0){
			result = "SUCCESS";
		}
		else{
			if (detailStartLs.size() > 0) {
				LOGGER.info("><><>< detailStartLs : {} ",detailStartLs);
				for (String docNumber : detailStartLs) {
					docNumberSet.add(docNumber);
				}
			}
			if(detailEndLs.size() > 0) {
				LOGGER.info("><><>< detailEndLs : {} ",detailEndLs);
				for (String docNumber : detailEndLs) {
					docNumberSet.add(docNumber);
				}
			}
			if(detailDraftStartLs.size() > 0){
				LOGGER.info("><><>< detailDraftStartLs : {} ",detailDraftStartLs);
				for(String docNumber : detailDraftStartLs){
					docNumberSet.add(docNumber);
				}
			}
			if(detailDraftEndLs.size() > 0){
				LOGGER.info("><><>< detailDraftEndLs : {} ",detailDraftEndLs);
				for(String docNumber : detailDraftEndLs){
					docNumberSet.add(docNumber);
				}
			}

			if(docNumberSet.size() > 0){
				for(String docNumber : docNumberSet){
					result += "#"+docNumber;
				}
			}
		}

		return (new JSONSerializer().serialize(result));
	}
}

