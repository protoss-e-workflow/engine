package com.spt.engine.controller;

import com.spt.engine.service.CheckAccommodationAuthorizeService;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/checkAccommodationCustom")
public class CheckAccommodationController {

    static final Logger LOGGER = LoggerFactory.getLogger(CheckAccommodationController.class);

    @Autowired
    CheckAccommodationAuthorizeService checkAccommodationAuthorizeService;

    @GetMapping(value="/checkAccommodationAuthorize",produces = "application/json; charset=utf-8")
    public String checkAccommodationAuthorize(@RequestParam("docNumber") String docNumber,
                                              @RequestParam("itemId") Long itemId,
                                              @RequestParam("isIbisStd") Boolean isIbisStd,
                                              @RequestParam("diffDays") String diffDays,
                                              @RequestParam("zoneType") String zoneType) {
        boolean result  = checkAccommodationAuthorizeService.checkAccommodationAuthorize(docNumber,itemId,isIbisStd,zoneType,diffDays);
        return (new JSONSerializer().exclude("*.class").serialize(result));
    }
}
