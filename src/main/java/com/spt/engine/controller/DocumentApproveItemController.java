package com.spt.engine.controller;

import javax.persistence.EntityManager;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;
import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.DocumentAttachment;
import com.spt.engine.entity.general.Parameter;
import com.spt.engine.entity.general.ParameterDetail;
import com.spt.engine.repository.general.ParameterRepository;
import org.hibernate.dialect.identity.Oracle12cGetGeneratedKeysDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentApproveItem;

import java.lang.reflect.Type;
import java.util.Date;

@RestController
@RequestMapping("/documentApproveItemCustom")
public class DocumentApproveItemController {

	static final Logger LOGGER = LoggerFactory.getLogger(DocumentApproveItemController.class);
	
	@Autowired
	EntityManager entityManager;

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	ParameterRepository parameterRepository;

	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
		public Date deserialize(JsonElement json, Type typeOfT,
								JsonDeserializationContext context) throws JsonParseException {
			return json == null ? null : new Date(json.getAsLong());
		}
	};


	protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();
	
	@Transactional
	@PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public DocumentApproveItem saveDocumentApproveItem(@RequestBody DocumentApproveItem documentApproveItem,
													   @RequestParam("document") Document document){
	
		LOGGER.info("><><>< SAVE OR UPDATE DOCUMENT APPROVE ITEM ><><><");
		try{
			if(documentApproveItem.getId() != null){
				LOGGER.info("><><>< MERGE DOCUMENT APPROVE ITEM ><><><");

				DocumentApproveItem approveItem = entityManager.find(DocumentApproveItem.class,documentApproveItem.getId());
				DocumentApproveItem item = objectMapper.readerForUpdating(approveItem).readValue(gson.toJson(documentApproveItem));

				document.setTitleDescription(item.getTravelReason());
				entityManager.merge(document);

				entityManager.merge(item);
				entityManager.flush();
			}else{
				LOGGER.info("><><>< PERSIST DOCUMENT APPROVE ITEM ><><><");

				documentApproveItem.setDocumentApprove(document.getDocumentApprove());
				entityManager.persist(documentApproveItem);
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		
		LOGGER.info("><><>< AFTER PERSIST DOCUMENT APPROVE ITEM ><><>< ");
		return documentApproveItem;
	}

	/* for download file by id */
	@GetMapping("/downloadFileReservation/{reservationType}")
	@ResponseBody
	public ResponseEntity<Resource> downloadFileReservation(@PathVariable String reservationType) {

		LOGGER.info("><><>< downloadFileReservation : {} ",reservationType);
		String filename = "";

		Parameter parameter = null;
		if(reservationType.equals(ConstantVariable.RESERVATION_TYPE_HOTEL)){
			parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_HOTEL_RESERVATION);
		}else if(reservationType.equals(ConstantVariable.RESERVATION_TYPE_FLIGHT)){
			parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_FLIGHT_RESERVATION);
		}else if(reservationType.equals(ConstantVariable.RESERVATION_TYPE_CAR)){
			parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_CAR_RESERVATION);
		}
		String pathFile = "";
		for(ParameterDetail detail:parameter.getDetails()){
			pathFile = detail.getVariable2();
			filename = detail.getVariable1();
		}

		LOGGER.info("><>< PATH FILE : {} ",pathFile);
		LOGGER.info("><>< FILE NAME : {} ",filename);

		return ResponseEntity
				.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+filename+"\"")
				.body(new FileSystemResource(pathFile+filename));
	}
}
