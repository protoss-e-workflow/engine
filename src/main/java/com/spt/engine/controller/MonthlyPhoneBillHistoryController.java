package com.spt.engine.controller;

import com.spt.engine.entity.app.DocumentExpenseItem;
import com.spt.engine.entity.app.MonthlyPhoneBill;
import com.spt.engine.entity.app.MonthlyPhoneBillHistory;
import com.spt.engine.entity.app.MonthlyPhoneBillPerYear;
import com.spt.engine.repository.app.DocumentExpenseItemRepository;
import com.spt.engine.repository.app.MonthlyPhoneBillHistoryRepository;
import com.spt.engine.repository.app.MonthlyPhoneBillPerYearRepository;
import com.spt.engine.repository.app.MonthlyPhoneBillRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;

@RestController
@RequestMapping("/monthlyPhoneBillHistoryCustom")
public class MonthlyPhoneBillHistoryController {

    static final Logger LOGGER = LoggerFactory.getLogger(MonthlyPhoneBillHistoryController.class);

    @Autowired
    MonthlyPhoneBillHistoryRepository monthlyPhoneBillHistoryRepository;

    @Autowired
    MonthlyPhoneBillRepository monthlyPhoneBillRepository;

    @Autowired
    DocumentExpenseItemRepository documentExpenseItemRepository;

    @Autowired
    MonthlyPhoneBillPerYearRepository monthlyPhoneBillPerYearRepository;

    @Autowired
    EntityManager entityManager;

    @Transactional
    @PostMapping(value="/saveMonthlyPhoneBillHistoryForExpense",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String saveMonthlyPhoneBillHistoryForExpense(@RequestBody MonthlyPhoneBillHistory monthlyPhoneBillHistory){

        LOGGER.info("><><>< Save Monthly Phone Bill History For Expense ><><><");
        String result = "";
        try{
            MonthlyPhoneBillPerYear monthlyPhoneBillPerYear = monthlyPhoneBillPerYearRepository.findByUserNameAndYear(monthlyPhoneBillHistory.getEmpUserName(),monthlyPhoneBillHistory.getYear());
            if(null != monthlyPhoneBillPerYear){
                Double amount = 0d;
                if("01".equals(monthlyPhoneBillHistory.getMonth())){ amount = monthlyPhoneBillPerYear.getAmountJan(); }
                if("02".equals(monthlyPhoneBillHistory.getMonth())){ amount = monthlyPhoneBillPerYear.getAmountFeb(); }
                if("03".equals(monthlyPhoneBillHistory.getMonth())){ amount = monthlyPhoneBillPerYear.getAmountMar(); }
                if("04".equals(monthlyPhoneBillHistory.getMonth())){ amount = monthlyPhoneBillPerYear.getAmountApr(); }
                if("05".equals(monthlyPhoneBillHistory.getMonth())){ amount = monthlyPhoneBillPerYear.getAmountMay(); }
                if("06".equals(monthlyPhoneBillHistory.getMonth())){ amount = monthlyPhoneBillPerYear.getAmountJun(); }
                if("07".equals(monthlyPhoneBillHistory.getMonth())){ amount = monthlyPhoneBillPerYear.getAmountJul(); }
                if("08".equals(monthlyPhoneBillHistory.getMonth())){ amount = monthlyPhoneBillPerYear.getAmountAug(); }
                if("09".equals(monthlyPhoneBillHistory.getMonth())){ amount = monthlyPhoneBillPerYear.getAmountSep(); }
                if("10".equals(monthlyPhoneBillHistory.getMonth())){ amount = monthlyPhoneBillPerYear.getAmountOct(); }
                if("11".equals(monthlyPhoneBillHistory.getMonth())){ amount = monthlyPhoneBillPerYear.getAmountNov(); }
                if("12".equals(monthlyPhoneBillHistory.getMonth())){ amount = monthlyPhoneBillPerYear.getAmountDec(); }

                if(amount >= monthlyPhoneBillHistory.getAmount()){
                    Double total = amount - monthlyPhoneBillHistory.getAmount();
                    if("01".equals(monthlyPhoneBillHistory.getMonth())){ monthlyPhoneBillPerYear.setAmountJan(total); }
                    if("02".equals(monthlyPhoneBillHistory.getMonth())){ monthlyPhoneBillPerYear.setAmountFeb(total); }
                    if("03".equals(monthlyPhoneBillHistory.getMonth())){ monthlyPhoneBillPerYear.setAmountMar(total); }
                    if("04".equals(monthlyPhoneBillHistory.getMonth())){ monthlyPhoneBillPerYear.setAmountApr(total); }
                    if("05".equals(monthlyPhoneBillHistory.getMonth())){ monthlyPhoneBillPerYear.setAmountMay(total); }
                    if("06".equals(monthlyPhoneBillHistory.getMonth())){ monthlyPhoneBillPerYear.setAmountJun(total); }
                    if("07".equals(monthlyPhoneBillHistory.getMonth())){ monthlyPhoneBillPerYear.setAmountJul(total); }
                    if("08".equals(monthlyPhoneBillHistory.getMonth())){ monthlyPhoneBillPerYear.setAmountAug(total); }
                    if("09".equals(monthlyPhoneBillHistory.getMonth())){ monthlyPhoneBillPerYear.setAmountSep(total); }
                    if("10".equals(monthlyPhoneBillHistory.getMonth())){ monthlyPhoneBillPerYear.setAmountOct(total); }
                    if("11".equals(monthlyPhoneBillHistory.getMonth())){ monthlyPhoneBillPerYear.setAmountNov(total); }
                    if("12".equals(monthlyPhoneBillHistory.getMonth())){ monthlyPhoneBillPerYear.setAmountDec(total); }
                    entityManager.merge(monthlyPhoneBillPerYear);
                    entityManager.flush();
                    result = "Success";
                }else{
                    result = "Over";
                }
            }else{
                result = "Success";
            }
            entityManager.persist(monthlyPhoneBillHistory);

        }catch (Exception e){
            result = "Error";
            e.printStackTrace();
        }

        return result;
    }

    @Transactional
    @PostMapping(value="/deleteMonthlyPhoneBillHistoryForExpense",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String deleteMonthlyPhoneBillHistoryForExpense(@RequestParam("expenseItem") Long documentExpenseItemId,
                                                          @RequestParam("month")String month,
                                                          @RequestParam("year")String year){

        LOGGER.info("><><>< Delete Monthly Phone Bill History For Expense ><><><");
        String result = "";
        try{
            DocumentExpenseItem documentExpenseItem = documentExpenseItemRepository.findOne(documentExpenseItemId);
            if(null != documentExpenseItem){
                Double oldNetAmount = documentExpenseItem.getNetAmount();
                Double totalAmount = 0d;

                String requester = documentExpenseItem.getDocumentExp().getDocument().getRequester();
                MonthlyPhoneBillPerYear monthlyPhoneBillPerYear = monthlyPhoneBillPerYearRepository.findByUserNameAndYear(requester,year);
                if(null != monthlyPhoneBillPerYear){
                    Double amount = 0d;
                    if("01".equals(month)){ amount = monthlyPhoneBillPerYear.getAmountJan(); }
                    if("02".equals(month)){ amount = monthlyPhoneBillPerYear.getAmountFeb(); }
                    if("03".equals(month)){ amount = monthlyPhoneBillPerYear.getAmountMar(); }
                    if("04".equals(month)){ amount = monthlyPhoneBillPerYear.getAmountApr(); }
                    if("05".equals(month)){ amount = monthlyPhoneBillPerYear.getAmountMay(); }
                    if("06".equals(month)){ amount = monthlyPhoneBillPerYear.getAmountJun(); }
                    if("07".equals(month)){ amount = monthlyPhoneBillPerYear.getAmountJul(); }
                    if("08".equals(month)){ amount = monthlyPhoneBillPerYear.getAmountAug(); }
                    if("09".equals(month)){ amount = monthlyPhoneBillPerYear.getAmountSep(); }
                    if("10".equals(month)){ amount = monthlyPhoneBillPerYear.getAmountOct(); }
                    if("11".equals(month)){ amount = monthlyPhoneBillPerYear.getAmountNov(); }
                    if("12".equals(month)){ amount = monthlyPhoneBillPerYear.getAmountDec(); }

                    totalAmount = amount+oldNetAmount;

                    if("01".equals(month)){ monthlyPhoneBillPerYear.setAmountJan(totalAmount); }
                    if("02".equals(month)){ monthlyPhoneBillPerYear.setAmountFeb(totalAmount); }
                    if("03".equals(month)){ monthlyPhoneBillPerYear.setAmountMar(totalAmount); }
                    if("04".equals(month)){ monthlyPhoneBillPerYear.setAmountApr(totalAmount); }
                    if("05".equals(month)){ monthlyPhoneBillPerYear.setAmountMay(totalAmount); }
                    if("06".equals(month)){ monthlyPhoneBillPerYear.setAmountJun(totalAmount); }
                    if("07".equals(month)){ monthlyPhoneBillPerYear.setAmountJul(totalAmount); }
                    if("08".equals(month)){ monthlyPhoneBillPerYear.setAmountAug(totalAmount); }
                    if("09".equals(month)){ monthlyPhoneBillPerYear.setAmountSep(totalAmount); }
                    if("10".equals(month)){ monthlyPhoneBillPerYear.setAmountOct(totalAmount); }
                    if("11".equals(month)){ monthlyPhoneBillPerYear.setAmountNov(totalAmount); }
                    if("12".equals(month)){ monthlyPhoneBillPerYear.setAmountDec(totalAmount); }
                    entityManager.merge(monthlyPhoneBillPerYear);
                    entityManager.flush();
                }

            }
            result = "Success";

        }catch (Exception e){
            result = "Error";
            e.printStackTrace();
        }

        return result;
    }

    @Transactional
    @PostMapping(value="/updateMonthlyPhoneBillHistoryForExpense",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String updateMonthlyPhoneBillHistoryForExpense(@RequestBody MonthlyPhoneBillHistory monthlyPhoneBillHistory,
                                                          @RequestParam("expenseItem") Long documentExpenseItemId,
                                                          @RequestParam("month")String month,
                                                          @RequestParam("year")String year){

        LOGGER.info("><><>< Update Monthly Phone Bill History For Expense ><><><");
        String result = "";
        try{
            DocumentExpenseItem documentExpenseItem = documentExpenseItemRepository.findOne(documentExpenseItemId);
            if(null != documentExpenseItem){
                Double newAmount = monthlyPhoneBillHistory.getAmount();
                Double oldAmount = 0d;
                Double totalAmount = 0d;

                String requester = documentExpenseItem.getDocumentExp().getDocument().getRequester();
                MonthlyPhoneBill monthlyPhoneBill = monthlyPhoneBillRepository.findByEmpUserName(requester);
                MonthlyPhoneBillPerYear monthlyPhoneBillPerYear = monthlyPhoneBillPerYearRepository.findByUserNameAndYear(requester,year);
                if(null != monthlyPhoneBillPerYear){
                    oldAmount = monthlyPhoneBill.getAmountPerMonth();
                    totalAmount = oldAmount-newAmount;

                    if("01".equals(month)){ monthlyPhoneBillPerYear.setAmountJan(totalAmount); }
                    if("02".equals(month)){ monthlyPhoneBillPerYear.setAmountFeb(totalAmount); }
                    if("03".equals(month)){ monthlyPhoneBillPerYear.setAmountMar(totalAmount); }
                    if("04".equals(month)){ monthlyPhoneBillPerYear.setAmountApr(totalAmount); }
                    if("05".equals(month)){ monthlyPhoneBillPerYear.setAmountMay(totalAmount); }
                    if("06".equals(month)){ monthlyPhoneBillPerYear.setAmountJun(totalAmount); }
                    if("07".equals(month)){ monthlyPhoneBillPerYear.setAmountJul(totalAmount); }
                    if("08".equals(month)){ monthlyPhoneBillPerYear.setAmountAug(totalAmount); }
                    if("09".equals(month)){ monthlyPhoneBillPerYear.setAmountSep(totalAmount); }
                    if("10".equals(month)){ monthlyPhoneBillPerYear.setAmountOct(totalAmount); }
                    if("11".equals(month)){ monthlyPhoneBillPerYear.setAmountNov(totalAmount); }
                    if("12".equals(month)){ monthlyPhoneBillPerYear.setAmountDec(totalAmount); }

                    entityManager.merge(monthlyPhoneBillPerYear);
                    entityManager.flush();
                }

            }
            result = "Success";

        }catch (Exception e){
            result = "Error";
            e.printStackTrace();
        }

        return result;
    }
}
