package com.spt.engine.controller;

import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentApprove;
import com.spt.engine.entity.app.DocumentApproveCostEstimate;
import com.spt.engine.entity.app.ExpenseType;
import com.spt.engine.repository.app.DocumentApproveCostEstimateRepository;
import com.spt.engine.repository.app.DocumentRepository;
import com.spt.engine.repository.app.ExpenseTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;

@RestController
@RequestMapping("/documentCostEstimateCustom")
public class DocumentApproveCostEstimateController {

    @Autowired
    DocumentApproveCostEstimateRepository documentApproveCostEstimateRepository;

    @Autowired
    ExpenseTypeRepository expenseTypeRepository;

    @Autowired
    EntityManager entityManager;

    @Autowired
    DocumentRepository documentRepository;


    @Transactional
    @PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public DocumentApproveCostEstimate saveDocumentApproveCostEstimate(@RequestBody DocumentApproveCostEstimate documentApproveCostEstimate,@RequestParam("document") String documentId){


        ExpenseType expenseType = expenseTypeRepository.getOne(Long.valueOf(documentApproveCostEstimate.getExpenseId()));
        Document document = documentRepository.getOne(Long.valueOf(documentId));
        DocumentApprove documentApprove = document.getDocumentApprove();

        if(null != documentApprove){
            documentApproveCostEstimate.setDocumentApprove(documentApprove);
        }
        documentApproveCostEstimate.setDescriptionEn(expenseType.getEngDescription());
        documentApproveCostEstimate.setDescriptionTh(expenseType.getDescription());
        entityManager.persist(documentApproveCostEstimate);

        return documentApproveCostEstimate;

    }

    @Transactional
    @PostMapping(value = "updateDocumentCostEstimate" , produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public DocumentApproveCostEstimate updateDocumentCostEstimate(@RequestBody DocumentApproveCostEstimate documentApproveCostEstimate,@RequestParam("document") String documentId){


        ExpenseType expenseType = expenseTypeRepository.getOne(Long.valueOf(documentApproveCostEstimate.getExpenseId()));
        Document document = documentRepository.getOne(Long.valueOf(documentId));
        DocumentApprove documentApprove = document.getDocumentApprove();

        DocumentApproveCostEstimate documentApproveCostEstimateOld = documentApproveCostEstimateRepository.getOne(documentApproveCostEstimate.getId());
        if(null != documentApprove){
            documentApproveCostEstimateOld.setDocumentApprove(documentApprove);
        }
        documentApproveCostEstimateOld.setDescriptionEn(expenseType.getEngDescription());
        documentApproveCostEstimateOld.setDescriptionTh(expenseType.getDescription());
        documentApproveCostEstimateOld.setRemark(documentApproveCostEstimate.getRemark());
        documentApproveCostEstimateOld.setAmount(documentApproveCostEstimate.getAmount());
        documentApproveCostEstimateOld.setExpenseId(documentApproveCostEstimate.getExpenseId());
        documentApproveCostEstimateOld.setGl(documentApproveCostEstimate.getGl());
        entityManager.merge(documentApproveCostEstimateOld);
        entityManager.flush();

        return documentApproveCostEstimateOld;

    }
}
