package com.spt.engine.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.spt.engine.entity.app.ExpenseType;
import com.spt.engine.entity.app.ExpenseTypeFile;
import com.spt.engine.repository.app.ExpenseTypeFileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;


@RestController
@RequestMapping("/expenseTypeFile")
public class ExpenseTypeFileController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(ExpenseTypeFileController.class);
	

	
	@Autowired
	EntityManager entityManager;
	
	@Autowired 
    ObjectMapper objectMapper;

	@Autowired
    ExpenseTypeFileRepository expenseTypeFileRepository;


	@Transactional
	@PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ExpenseTypeFile saveExpenseTypeFile(@RequestBody ExpenseTypeFile expenseTypeFile,
                                               @RequestParam("expenseType") ExpenseType expenseType){
	
		LOGGER.info("=============   Save ExpenseType File =================");
		LOGGER.info("=============   Expense Type =================  [{}]",expenseType);
		LOGGER.info("=============   Expense Type File =================  [{}]",expenseTypeFile);


						if( expenseTypeFile != null && expenseType != null){
							if(expenseTypeFile.getId() != null){

								ExpenseTypeFile expenseTypeFileUpdate = expenseTypeFileRepository.findOne(expenseTypeFile.getId());
								expenseTypeFileUpdate.setAttachmentTypeCode(expenseTypeFile.getAttachmentTypeCode());
								expenseTypeFileUpdate.setRequire(expenseTypeFile.getRequire());
								expenseTypeFileUpdate.setExpenseType(expenseType);
								entityManager.persist(expenseTypeFileUpdate);

								return expenseTypeFileUpdate;

							}else{

								expenseTypeFile.setExpenseType(expenseType);
								entityManager.persist(expenseTypeFile);

								return expenseTypeFile;

							}
						}else{
							return null;
						}







	}
	
	

}
