package com.spt.engine.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.spt.engine.entity.general.ReportConfiguration;
import com.spt.engine.entity.general.ReportProjection;
import com.spt.engine.repository.general.ParameterRepository;
import com.spt.engine.repository.general.ReportConfigurationRepository;
import com.spt.engine.util.GenReportJasperPDF;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;

@Controller
public class ReportCustom1Controller {
  
    static final Logger LOGGER = LoggerFactory.getLogger(ReportCustom1Controller.class);
    
    private final String TYPE_EXCEL   = "EXCEL";
    private final String TYPE_PDF     = "PDF";
    
    private final String ALIGN_LEFT   = "LEFT";
    private final String ALIGN_CENTER = "CENTER";
    private final String ALIGN_RIGHT  = "RIGHT";
    
    private final String FONT_TH_NORMAL      = "font/THSarabunNew.ttf";
    private final String FONT_TH_BOLD        = "font/THSarabunNew Bold.ttf";
    private final String FONT_TH_BOLD_ITALIC = "font/THSarabunNew BoldItalic.ttf";
    private final String FONT_TH_ITALIC      = "font/THSarabunNew Italic.ttf";
    
    @PersistenceContext
    private EntityManager entityManager;
  
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
  
    @Autowired
    ParameterRepository parameterRepository;
    
    @Autowired
    ReportConfigurationRepository reportConfigurationRepository;
  
  
    @GetMapping("/exportCustom/{reportCode}")
    public ResponseEntity<Resource> exportExcel(HttpServletRequest request,
    		@PathVariable String reportCode,
    		@RequestParam("startDate") String startDate,
    		@RequestParam("endDate") String endDate) {
    	
    	String dateSql = " ";
    	if( startDate!=null && startDate.trim().length() > 0  &&
    			endDate!=null && endDate.trim().length() > 0  ){
    		dateSql = "and    a.created_date > '"+startDate+"' and a.created_date < '"+endDate+" 23:59:59' ";
    	}else if( startDate!=null && startDate.trim().length() > 0){
    		dateSql = "and    a.created_date > '"+startDate+"' ";
    	}else if( endDate!=null && endDate.trim().length() > 0){
    		dateSql = "and    a.created_date < '"+endDate+" 23:59:59' ";
    	}
    	
    	
    	StringBuilder sql1 = new StringBuilder();
    	sql1.append("WITH datas AS (SELECT a.company_code, ");
    	sql1.append("count(1) AS approve , ");
    	sql1.append("       0 AS advance, ");
    	sql1.append("       0 AS expense ");
    	sql1.append("FROM document a ");
    	sql1.append("WHERE  a.document_type = 'APP' ");
    	sql1.append("and    LEN(a.company_code) = 4 ");
    	sql1.append("and    a.company_code <> 'null' ");
    	sql1.append(dateSql);
    	sql1.append("GROUP BY a.company_code ");
    	sql1.append("UNION ");
    	sql1.append("SELECT a.company_code, ");
    	sql1.append("       0 AS approve , ");
    	sql1.append("       count(1)  AS advance, ");
    	sql1.append("       0 AS expense ");
    	sql1.append("FROM document a ");
    	sql1.append("WHERE  a.document_type = 'ADV' ");
    	sql1.append("and    LEN(a.company_code) = 4 ");
    	sql1.append("and    a.company_code <> 'null' ");
    	sql1.append(dateSql);
    	sql1.append("GROUP BY a.company_code ");
    	sql1.append("UNION ");
    	sql1.append("SELECT a.company_code, ");
    	sql1.append("       0 AS approve , ");
    	sql1.append("       0 AS advance, ");
    	sql1.append("       count(1) AS expense ");
    	sql1.append("FROM document a ");
    	sql1.append("WHERE  a.document_type = 'EXP' ");
    	sql1.append("and    LEN(a.company_code) = 4 ");
    	sql1.append("and    a.company_code <> 'null' ");
    	sql1.append(dateSql);
    	sql1.append("GROUP BY a.company_code) ");
    	sql1.append("select b.company_code as company_code, ");
    	sql1.append("       SUM(b.approve) as  approve, ");
    	sql1.append("       SUM(b.advance) as  advance, ");
    	sql1.append("       SUM(b.expense) as  expense ");
    	sql1.append("from datas b ");
    	sql1.append("group by b.company_code ");
    	sql1.append("order by b.company_code ");
    	
    	StringBuilder sql2 = new StringBuilder();
    	sql2.append("WITH datas AS (SELECT a.company_code, ");
    	sql2.append("count(1) AS CMP , ");
    	sql2.append("       0 AS ONP, ");
    	sql2.append("       0 AS REJ, ");
    	sql2.append("       0 AS CCL, ");
    	sql2.append("       0 AS OTH ");
    	sql2.append("FROM document a ");
    	sql2.append("WHERE  a.document_status = 'CMP' ");
    	sql2.append("and    LEN(a.company_code) = 4 ");
    	sql2.append("and    a.company_code <> 'null' ");
    	sql2.append(dateSql);
    	sql2.append("GROUP BY a.company_code ");
    	sql2.append("UNION ");
    	sql2.append("SELECT a.company_code, ");
    	sql2.append(" 		0 AS CMP , ");
    	sql2.append("       count(1) AS ONP, ");
    	sql2.append("       0 AS REJ, ");
    	sql2.append("       0 AS CCL, ");
    	sql2.append("       0 AS OTH ");
    	sql2.append("FROM document a ");
    	sql2.append("WHERE  a.document_status = 'ONP' ");
    	sql2.append("and    LEN(a.company_code) = 4 ");
    	sql2.append("and    a.company_code <> 'null' ");
    	sql2.append(dateSql);
    	sql2.append("GROUP BY a.company_code ");
    	sql2.append("UNION ");
    	sql2.append("SELECT a.company_code, ");
    	sql2.append(" 		0 AS CMP , ");
    	sql2.append("       0 AS ONP, ");
    	sql2.append("       count(1) AS REJ, ");
    	sql2.append("       0 AS CCL, ");
    	sql2.append("       0 AS OTH ");
    	sql2.append("FROM document a ");
    	sql2.append("WHERE  a.document_status = 'REJ' ");
    	sql2.append("and    LEN(a.company_code) = 4 ");
    	sql2.append("and    a.company_code <> 'null' ");
    	sql2.append(dateSql);
    	sql2.append("GROUP BY a.company_code ");
    	sql2.append("UNION ");
    	sql2.append("SELECT a.company_code, ");
    	sql2.append(" 		0 AS CMP , ");
    	sql2.append("       0 AS ONP, ");
    	sql2.append("       0 AS REJ, ");
    	sql2.append("       count(1) AS CCL, ");
    	sql2.append("       0 AS OTH ");
    	sql2.append("FROM document a ");
    	sql2.append("WHERE  a.document_status = 'CCL' ");
    	sql2.append("and    LEN(a.company_code) = 4 ");
    	sql2.append("and    a.company_code <> 'null' ");
    	sql2.append(dateSql);
    	sql2.append("GROUP BY a.company_code ");
    	sql2.append("UNION ");
    	sql2.append("SELECT a.company_code, ");
    	sql2.append(" 		0 AS CMP , ");
    	sql2.append("       0 AS ONP, ");
    	sql2.append("       0 AS REJ, ");
    	sql2.append("       0 AS CCL, ");
    	sql2.append("       count(1) AS OTH ");
    	sql2.append("FROM document a ");
    	sql2.append("WHERE  a.document_status not in ('CMP','ONP','REJ','CCL') ");
    	sql2.append("and    LEN(a.company_code) = 4 ");
    	sql2.append("and    a.company_code <> 'null' ");
    	sql2.append(dateSql);
    	sql2.append("GROUP BY a.company_code) ");
    	sql2.append("select b.company_code as company_code, ");
    	sql2.append("       SUM(b.CMP) as  CMP, ");
    	sql2.append("       SUM(b.ONP) as  ONP, ");
    	sql2.append("       SUM(b.REJ) as  REJ, ");
    	sql2.append("       SUM(b.CCL) as  CCL, ");
    	sql2.append("       SUM(b.OTH) as  OTH ");
    	sql2.append("from datas b ");
    	sql2.append("group by b.company_code ");
    	sql2.append("order by b.company_code ");
    	

    	StringBuilder sql3 = new StringBuilder();
    	sql3.append("WITH datas AS (SELECT a.document_type, ");
    	sql3.append("count(1) AS CMP , ");
    	sql3.append("       0 AS ONP, ");
    	sql3.append("       0 AS REJ, ");
    	sql3.append("       0 AS CCL, ");
    	sql3.append("       0 AS OTH ");
    	sql3.append("FROM document a ");
    	sql3.append("WHERE  a.document_status = 'CMP' ");
    	sql3.append("and    a.document_type <> 'null' ");
    	sql3.append(dateSql);
    	sql3.append("GROUP BY a.document_type ");
    	sql3.append("UNION ");
    	sql3.append("SELECT a.document_type, ");
    	sql3.append(" 		0 AS CMP , ");
    	sql3.append("       count(1) AS ONP, ");
    	sql3.append("       0 AS REJ, ");
    	sql3.append("       0 AS CCL, ");
    	sql3.append("       0 AS OTH ");
    	sql3.append("FROM document a ");
    	sql3.append("WHERE  a.document_status = 'ONP' ");
    	sql3.append("and    a.document_type <> 'null' ");
    	sql3.append(dateSql);
    	sql3.append("GROUP BY a.document_type ");
    	sql3.append("UNION ");
    	sql3.append("SELECT a.document_type, ");
    	sql3.append(" 		0 AS CMP , ");
    	sql3.append("       0 AS ONP, ");
    	sql3.append("       count(1) AS REJ, ");
    	sql3.append("       0 AS CCL, ");
    	sql3.append("       0 AS OTH ");
    	sql3.append("FROM document a ");
    	sql3.append("WHERE  a.document_status = 'REJ' ");
    	sql3.append("and    a.document_type <> 'null' ");
    	sql3.append(dateSql);
    	sql3.append("GROUP BY a.document_type ");
    	sql3.append("UNION ");
    	sql3.append("SELECT a.document_type, ");
    	sql3.append(" 		0 AS CMP , ");
    	sql3.append("       0 AS ONP, ");
    	sql3.append("       0 AS REJ, ");
    	sql3.append("       count(1) AS CCL, ");
    	sql3.append("       0 AS OTH ");
    	sql3.append("FROM document a ");
    	sql3.append("WHERE  a.document_status = 'CCL' ");
    	sql3.append("and    a.document_type <> 'null' ");
    	sql3.append(dateSql);
    	sql3.append("GROUP BY a.document_type ");
    	sql3.append("UNION ");
    	sql3.append("SELECT a.document_type, ");
    	sql3.append(" 		0 AS CMP , ");
    	sql3.append("       0 AS ONP, ");
    	sql3.append("       0 AS REJ, ");
    	sql3.append("       0 AS CCL, ");
    	sql3.append("       count(1) AS OTH ");
    	sql3.append("FROM document a ");
    	sql3.append("WHERE  a.document_status not in ('CMP','ONP','REJ','CCL') ");
    	sql3.append("and    a.document_type <> 'null' ");
    	sql3.append(dateSql);
    	sql3.append("GROUP BY a.document_type) ");
    	sql3.append("select b.document_type as document_type, ");
    	sql3.append("       SUM(b.CMP) as  CMP, ");
    	sql3.append("       SUM(b.ONP) as  ONP, ");
    	sql3.append("       SUM(b.REJ) as  REJ, ");
    	sql3.append("       SUM(b.CCL) as  CCL, ");
    	sql3.append("       SUM(b.OTH) as  OTH ");
    	sql3.append("from datas b ");
    	sql3.append("group by b.document_type ");
    	sql3.append("order by b.document_type ");
    	
    	ReportConfiguration reportConfiguration = reportConfigurationRepository.findByCode(reportCode);

    	//Find Parameter
    	Map<String,String> sqlParam = new HashMap<>();
    	Pattern findParametersPattern = Pattern.compile("(?<!')(:[\\w]*)(?!')");
    	Matcher matcher = findParametersPattern.matcher(sql1.toString());
    	while (matcher.find()) { 
    		sqlParam.put(String.valueOf((matcher.group().substring(1))), "");
    	}
    	
    	/* Assign Parameter Value */
    	MapSqlParameterSource msps = new MapSqlParameterSource();
    	Map<String,String[]> mapParam = new HashMap<>(request.getParameterMap());
        if(sqlParam != null){
        	for(String key:sqlParam.keySet()){
        		String param = "";
        		try {
					param = mapParam.get(key)[0];
					if(param==null){
						param = "";
					}
				} catch (Exception e) { }
        		
        		msps.addValue(key, param);
        	}
        }          
        
        /* Fetch Data From Database */
        List<Map<String,Object>> dataList = namedParameterJdbcTemplate.queryForList(sql1.toString(), msps);
        
        /* Fetch Data From Database */
        List<Map<String,Object>> data2List = namedParameterJdbcTemplate.queryForList(sql2.toString(), msps);
        

        /* Fetch Data From Database */
        List<Map<String,Object>> data3List = namedParameterJdbcTemplate.queryForList(sql3.toString(), msps);
        
    	Workbook wb = new XSSFWorkbook();
        try {
        	
        	InputStream inputStream = reteriveByteArrayInputStream(new File(reportConfiguration.getTemplateFileName()));
        	
        	
			wb = new XSSFWorkbook(inputStream);
			Sheet dataSheet1 = wb.getSheetAt(1);
			int rowOfSheet1Index = 1;
	  	    for(Map<String,Object> model:dataList){
	  	    	Row rowOfSheet1 = dataSheet1.getRow(rowOfSheet1Index);
	  	    	if(rowOfSheet1==null){
	  	    		rowOfSheet1 = dataSheet1.createRow(rowOfSheet1Index);
	  	    	}
	  	    	
	  	    	Cell compCodeColumn = rowOfSheet1.createCell(0);
	  	    	compCodeColumn.setCellValue(String.valueOf(model.get("COMPANY_CODE")));
	  	    	

	  	    	Cell approveColumn = rowOfSheet1.createCell(1);
	  	    	approveColumn.setCellValue(Double.parseDouble(String.valueOf(model.get("APPROVE"))));
	  	    	

	  	    	Cell advanceColumn = rowOfSheet1.createCell(2);
	  	    	advanceColumn.setCellValue(Double.parseDouble(String.valueOf(model.get("ADVANCE"))));
	  	    	

	  	    	Cell expenseColumn = rowOfSheet1.createCell(3);
	  	    	expenseColumn.setCellValue(Double.parseDouble(String.valueOf(model.get("EXPENSE"))));
		          
	  	    	rowOfSheet1Index++;
	  	    }
	  	    
	  	  Sheet dataSheet2 = wb.getSheetAt(2);
			int rowOfSheet2Index = 1;
	  	    for(Map<String,Object> model:data2List){
	  	    	Row rowOfSheet2 = dataSheet2.getRow(rowOfSheet2Index);
	  	    	if(rowOfSheet2==null){
	  	    		rowOfSheet2 = dataSheet2.createRow(rowOfSheet2Index);
	  	    	}
	  	    	
	  	    	Cell compCodeColumn = rowOfSheet2.createCell(0);
	  	    	compCodeColumn.setCellValue(String.valueOf(model.get("COMPANY_CODE")));
	  	    	

	  	    	Cell cmpColumn = rowOfSheet2.createCell(1);
	  	    	cmpColumn.setCellValue(Double.parseDouble(String.valueOf(model.get("CMP"))));
	  	    	

	  	    	Cell onpColumn = rowOfSheet2.createCell(2);
	  	    	onpColumn.setCellValue(Double.parseDouble(String.valueOf(model.get("ONP"))));
	  	    	

	  	    	Cell rejColumn = rowOfSheet2.createCell(3);
	  	    	rejColumn.setCellValue(Double.parseDouble(String.valueOf(model.get("REJ"))));
	  	    	

	  	    	Cell cclColumn = rowOfSheet2.createCell(4);
	  	    	cclColumn.setCellValue(Double.parseDouble(String.valueOf(model.get("CCL"))));
	  	    	

	  	    	Cell othColumn = rowOfSheet2.createCell(5);
	  	    	othColumn.setCellValue(Double.parseDouble(String.valueOf(model.get("OTH"))));
		          
	  	    	rowOfSheet2Index++;
	  	    }
			
	  	    
	  	  Sheet dataSheet0 = wb.getSheetAt(0);
	  	  for(Map<String,Object> model:data3List){
	  		if("APP".equals(model.get("DOCUMENT_TYPE"))){
	  			dataSheet0.getRow(61).createCell(10).setCellValue(Double.parseDouble(String.valueOf(model.get("CMP"))));
	  			dataSheet0.getRow(62).createCell(10).setCellValue(Double.parseDouble(String.valueOf(model.get("ONP"))));
	  			dataSheet0.getRow(63).createCell(10).setCellValue(Double.parseDouble(String.valueOf(model.get("REJ"))));
	  			dataSheet0.getRow(64).createCell(10).setCellValue(Double.parseDouble(String.valueOf(model.get("CCL"))));
	  			dataSheet0.getRow(65).createCell(10).setCellValue(Double.parseDouble(String.valueOf(model.get("OTH"))));
	  		}else if("ADV".equals(model.get("DOCUMENT_TYPE"))){
	  			dataSheet0.getRow(81).createCell(10).setCellValue(Double.parseDouble(String.valueOf(model.get("CMP"))));
	  			dataSheet0.getRow(82).createCell(10).setCellValue(Double.parseDouble(String.valueOf(model.get("ONP"))));
	  			dataSheet0.getRow(83).createCell(10).setCellValue(Double.parseDouble(String.valueOf(model.get("REJ"))));
	  			dataSheet0.getRow(84).createCell(10).setCellValue(Double.parseDouble(String.valueOf(model.get("CCL"))));
	  			dataSheet0.getRow(85).createCell(10).setCellValue(Double.parseDouble(String.valueOf(model.get("OTH"))));
	  		}else if("EXP".equals(model.get("DOCUMENT_TYPE"))){
	  			dataSheet0.getRow(101).createCell(10).setCellValue(Double.parseDouble(String.valueOf(model.get("CMP"))));
	  			dataSheet0.getRow(102).createCell(10).setCellValue(Double.parseDouble(String.valueOf(model.get("ONP"))));
	  			dataSheet0.getRow(103).createCell(10).setCellValue(Double.parseDouble(String.valueOf(model.get("REJ"))));
	  			dataSheet0.getRow(104).createCell(10).setCellValue(Double.parseDouble(String.valueOf(model.get("CCL"))));
	  			dataSheet0.getRow(105).createCell(10).setCellValue(Double.parseDouble(String.valueOf(model.get("OTH"))));
	  		}
	  	  }
			
      	 
	          
	          
	          
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} //or new HSSFWorkbook();


		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			wb.write(outputStream);
			wb.close();
		} catch (Exception e) { LOGGER.error("ERROR ={}", e);  }

	  		  /* Set Header to */
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(new MediaType("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
		headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+reportConfiguration.getFileName()+"\"");//
		headers.setContentLength(outputStream.size());

		ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());

		return ResponseEntity
				.ok()
				.headers(headers)
				.body(new InputStreamResource(inputStream));
        
        
    }
    
    public  ByteArrayInputStream reteriveByteArrayInputStream(File file) throws IOException {
        return new ByteArrayInputStream(FileUtils.readFileToByteArray(file));
    }
    
} 
