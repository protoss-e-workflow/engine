package com.spt.engine.controller;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.entity.app.DocumentApproveItem;
import com.spt.engine.entity.app.TravelMember;
import com.spt.engine.repository.app.TravelMemberRepository;

import flexjson.JSONSerializer;

@RestController
@RequestMapping("/travelMemberCustom")
public class TravelMemberController {

	static final Logger LOGGER = LoggerFactory.getLogger(TravelMemberController.class);
	
	@Autowired
	EntityManager entityManager;
	
	@Autowired
	TravelMemberRepository travelMemberRepository;
	
	@Autowired 
    ObjectMapper objectMapper;
	
	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };
    
    protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();
	
	@Transactional
	@PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public String saveTravelMember(	@RequestBody TravelMember travelMember,
								 	@RequestParam("documentApproveItem") Long documentApproveItem
								){
	
		LOGGER.info("><><>< SAVE AND UPDATE TRAVEL MEMBER ><><><");
		String result = "";
		try{
			if(travelMember.getId() != null){
				LOGGER.info("><><>< TRAVEL MEMBER ID IS NOT NULL ><><><");
				
				TravelMember travelMember2 = travelMemberRepository.findOne(travelMember.getId());
				TravelMember member = objectMapper.readerForUpdating(travelMember2).readValue(gson.toJson(travelMember));

				List<TravelMember> travelMemberLs = travelMemberRepository.findByMemberUserAndDocumentApproveItem(member.getMemberUser(),documentApproveItem);
				if(travelMemberLs.size() > 0){
					result =  "DUPLICATE";
				}else{
					entityManager.merge(member);
					entityManager.flush();
					result = "SUCCESS";
				}
			}else{
				
				LOGGER.info("><><>< TRAVEL MEMBER ID IS  NULL ><><><");
				List<TravelMember> travelMemberLs = travelMemberRepository.findByMemberUserAndDocumentApproveItem(travelMember.getMemberUser(),documentApproveItem);
				if(travelMemberLs.size() > 0){
					result =  "DUPLICATE";
				}else{
					DocumentApproveItem documentAppItem = entityManager.find(DocumentApproveItem.class, documentApproveItem);

					documentAppItem.setTravelMembers(new HashSet<TravelMember>(){{
						add(travelMember);
					}});

					entityManager.persist(documentAppItem);

					travelMember.setDocumentApproveItem(documentAppItem);
					entityManager.persist(travelMember);

					result = "SUCCESS";
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		LOGGER.info("><><>< AFTER PERSIST TRAVEL DETAIL ><><>< ");
		return result;
	}
	
	@GetMapping(value="/getTravelMember/{id}/travelMembers",produces = "application/json; charset=utf-8")
    public String getTravelMember(@PathVariable Long id) {

		List<Long> documentApproveItemLs = Arrays.asList(id);
		List<TravelMember> travelMembers = travelMemberRepository.findByDocumentApproveItemId(documentApproveItemLs);
		
		LOGGER.info("travelMembers = {} ",travelMembers.size());
		
		return (new JSONSerializer().exclude("*.class").include("id")
				.include("memberName")
				.include("memberCode")
				.include("memberPersonalId")
				.include("positionMember")
				.include("departmentMember")
				.include("flagCar")
				.include("flagFlight")
				.include("flagBed")
				.include("flagBedTwin")
				.serialize(travelMembers));
    }
	
}
