package com.spt.engine.controller;

import java.io.BufferedReader;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentAdvance;
import com.spt.engine.entity.app.DocumentExpenseItem;
import com.spt.engine.entity.app.RequestApprover;
import com.spt.engine.repository.app.DocumentRepository;

@RestController
public class PaymentActionController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(PaymentActionController.class);
	
	@Autowired
	EntityManager entityManager;

	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
		public Date deserialize(JsonElement json, Type typeOfT,
								JsonDeserializationContext context) throws JsonParseException {
			return json == null ? null : new Date(json.getAsLong());
		}
	};


	protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();

	@Autowired
	DocumentRepository documentRepository;
	

    @Autowired
    RestTemplate restTemplate;
    
    @Value("${AppServer}")
	String AppServer;
    

    @Value("${EngineServer}")
	String EngineServer;
    
    

	
	@PostMapping(value="/paymentAction")
	@Transactional
    ResponseEntity<String> receivePaidAction(HttpServletRequest request) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
	
		boolean hasError = true;
		try {
			StringBuffer jb = new StringBuffer();
			  String line = null;
			  try {
			    BufferedReader reader = request.getReader();
			    while ((line = reader.readLine()) != null)
			      jb.append(line);
			  } catch (Exception e) { /*report an error*/ }
			String jsonString = jb.toString();
			LOGGER.info("receivePaidAction ={}",jsonString);
			
			Map dataPayMentDoc = gson.fromJson(jsonString, Map.class);
			String paymntDocumentNumber = (String) dataPayMentDoc.get("AC_DOC_NO");
			String companyCode          = (String) dataPayMentDoc.get("COMP_CODE");
			String valueDateStr         = (String) dataPayMentDoc.get("VALUE_DATE");
			Date valueDate = null;

			try{
				valueDate = format.parse(valueDateStr);
			}catch (Exception e){
				// ERROR
			}
			
			
			LOGGER.info("paymntDocumentNumber ={}",paymntDocumentNumber);

			Set<Document> documentSet  = new HashSet();
			Set<Document> documentExpenseSet  = new HashSet();
			Map tabPayment  = (Map) dataPayMentDoc.get("TAB_PAYMENT");
			
			if(tabPayment.get("item") instanceof List){
				List<Map<String,Object>> items = (List<Map<String, Object>>) tabPayment.get("item");
				for(Map<String,Object> item:items){
					String fiDocExpense = (String)item.get("AC_DOC_NO");
					Double paymentAmount= Double.valueOf(String.valueOf(item.get("AMT_DOCCUR")));
					if(paymentAmount.doubleValue() < 0){
						paymentAmount = paymentAmount.doubleValue() * -1;
					}
					if(fiDocExpense.startsWith("61")){//ADVANCE CASE
						DocumentAdvance documentAdvance = documentRepository.findDocumentAdvanceByExternalDocNumber(fiDocExpense,companyCode);
						if(null != documentAdvance && documentAdvance.getExternalPaymentDocNumber()==null){
							documentAdvance.setExternalPaymentDocNumber(paymntDocumentNumber);
							documentAdvance.setPaymentAmount(paymentAmount);
							documentAdvance.setPaymentStatus("PAY");
							if(null != valueDate)documentAdvance.setValueDate(new Timestamp(valueDate.getTime()));
							documentRepository.flush();
						}
						
						//Update FNC to Document
						documentSet.add(documentAdvance.getDocument());
					}else if(fiDocExpense.startsWith("62")){//EXPENSE CASE
						List<DocumentExpenseItem> documentExpenseItems = documentRepository.findDocumentExpenseItemByExternalDocNumber(fiDocExpense,companyCode);
						for(DocumentExpenseItem documentExpenseItem:documentExpenseItems){
							if(documentExpenseItem.getExternalPaymentDocNumber()==null){
								documentExpenseItem.setExternalPaymentDocNumber(paymntDocumentNumber);
								documentExpenseItem.setPaymentStatus("PAY");
								documentExpenseItem.setPaymentAmount(paymentAmount);
								if(null != valueDate)documentExpenseItem.setValueDate(new Timestamp(valueDate.getTime()));
								documentRepository.flush();
								documentExpenseSet.add(documentExpenseItem.getDocumentExp().getDocument());
							}
							
						}
					}else if(fiDocExpense.startsWith("68")){//CLEARING CASE
						List<DocumentExpenseItem> documentExpenseItems = documentRepository.findDocumentExpenseItemByExternalClearingDocNumber(fiDocExpense,companyCode);
						for(DocumentExpenseItem documentExpenseItem:documentExpenseItems){
							if(documentExpenseItem.getExternalPaymentDocNumber()==null){
								documentExpenseItem.setExternalPaymentDocNumber(paymntDocumentNumber);
								documentExpenseItem.setPaymentStatus("PAY");
								documentExpenseItem.setPaymentAmount(paymentAmount);
								if(null != valueDate)documentExpenseItem.setValueDate(new Timestamp(valueDate.getTime()));
								documentRepository.flush();
								documentExpenseSet.add(documentExpenseItem.getDocumentExp().getDocument());
							}
							
						}
					}
					
				}
			}else if(tabPayment.get("item") instanceof Map){
				Map<String,Object> item = (Map<String, Object>) tabPayment.get("item");
				String fiDocExpense = (String)item.get("AC_DOC_NO");
				Double paymentAmount= Double.valueOf(String.valueOf(item.get("AMT_DOCCUR")));
				
				if(paymentAmount.doubleValue() < 0){
					paymentAmount = paymentAmount.doubleValue() * -1;
				}
				if(fiDocExpense.startsWith("61")){//ADVANCE CASE
					DocumentAdvance documentAdvance = documentRepository.findDocumentAdvanceByExternalDocNumber(fiDocExpense,companyCode);
					if(null != documentAdvance && documentAdvance.getExternalPaymentDocNumber()==null){
						documentAdvance.setExternalPaymentDocNumber(paymntDocumentNumber);
						documentAdvance.setPaymentAmount(paymentAmount);
						documentAdvance.setPaymentStatus("PAY");
						if(null != valueDate)documentAdvance.setValueDate(new Timestamp(valueDate.getTime()));
						documentRepository.flush();
					}
					
					//Update FNC to Document
					documentSet.add(documentAdvance.getDocument());
				}else if(fiDocExpense.startsWith("62")){//EXPENSE CASE
					List<DocumentExpenseItem> documentExpenseItems = documentRepository.findDocumentExpenseItemByExternalDocNumber(fiDocExpense,companyCode);
					for(DocumentExpenseItem documentExpenseItem:documentExpenseItems){
						if(documentExpenseItem.getExternalPaymentDocNumber()==null){
							documentExpenseItem.setExternalPaymentDocNumber(paymntDocumentNumber);
							documentExpenseItem.setPaymentAmount(paymentAmount);
							documentExpenseItem.setPaymentStatus("PAY");
							if(null != valueDate)documentExpenseItem.setValueDate(new Timestamp(valueDate.getTime()));
							documentRepository.flush();
							documentExpenseSet.add(documentExpenseItem.getDocumentExp().getDocument());
						}
						
					}
				}else if(fiDocExpense.startsWith("68")){//CLEARING CASE
					List<DocumentExpenseItem> documentExpenseItems = documentRepository.findDocumentExpenseItemByExternalClearingDocNumber(fiDocExpense,companyCode);
					for(DocumentExpenseItem documentExpenseItem:documentExpenseItems){
						if(documentExpenseItem.getExternalPaymentDocNumber()==null){
							documentExpenseItem.setExternalPaymentDocNumber(paymntDocumentNumber);
							documentExpenseItem.setPaymentAmount(paymentAmount);
							documentExpenseItem.setPaymentStatus("PAY");
							if(null != valueDate)documentExpenseItem.setValueDate(new Timestamp(valueDate.getTime()));
							documentRepository.flush();
							documentExpenseSet.add(documentExpenseItem.getDocumentExp().getDocument());
						}
						
					}
				}
			}
			
			
			
			//Check Document Complete PAY
			for(Document document:documentExpenseSet){
				boolean isComplete = true;
				for(DocumentExpenseItem documentExpenseItem:document.getDocumentExpense().getDocumentExpenseItems()){
					if(!"PAY".equals(documentExpenseItem.getPaymentStatus()) && !"PAID".equals(documentExpenseItem.getPaymentStatus())){
						isComplete = false;
						break;
					}
				}
				
				if(isComplete){
					documentSet.add(document);
				}
			}
			
			//For Approve FNC
			for(Document document:documentSet){
				String userNameApprover = null;
				for(RequestApprover approver:document.getRequest().getRequestApprovers()){
					if(ConstantVariable.ACTION_STATE_FINANCE.equals(approver.getActionState())){
						userNameApprover = approver.getUserNameApprover();
						break;
					}
				}
				
				//Update FNC to Document
			    String userName       = userNameApprover;
			    String documentNumber = document.getDocNumber();
			    String docType        = document.getDocumentType();
			    String documentFlow   = document.getDocFlow();
			    String processId      = document.getProcessId();
			    String documentId     = ""+document.getId().intValue();
			    String externalDocChanel = document.getExternalDocChanel();
			    //
				HttpHeaders headers = new HttpHeaders();
		        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		        headers.add("Content-Type", "application/json; charset=utf-8");

		        HttpEntity<String> entity = new HttpEntity<String>("", headers);
				String url = "";
				if(null != externalDocChanel && "EMED".equals(externalDocChanel)){
					url = this.AppServer + "/requests/approveRequestEmedGet?userName="+userName+"&actionStatus=FNC&documentNumber="+documentNumber+"&docType="+docType+"&documentFlow="+documentFlow+"&processId="+processId+"&documentId="+documentId+"&actionReasonCode=&actionReasonDetail=";
				}else{
					url = this.AppServer + "/requests/approveRequestGet?userName="+userName+"&actionStatus=FNC&documentNumber="+documentNumber+"&docType="+docType+"&documentFlow="+documentFlow+"&processId="+processId+"&documentId="+documentId+"&actionReasonCode=&actionReasonDetail=";
				}
		        LOGGER.info(" request :{}", url);
		        restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
		        
		        //send Email
		        restTemplate.exchange(this.EngineServer+"/flowMail/"+documentNumber+"/PAY/A", HttpMethod.GET, entity, String.class);
				
			}
			
			
			hasError = false;
		} catch (Exception e) { /* Log Error */e.printStackTrace();}
		
		
		
		
		if(!hasError){
			Map mapDataRet = new HashMap(); 
			mapDataRet.put("item", "Payment successfully");
			
			Map mapResult = new HashMap();
			 Map mapData = new HashMap();
			
			  mapData.put("MESSAGE", "Success");
			  mapData.put("CODE", "200");
			  mapData.put("INFO", "Payment Transaction Completed");
			  mapData.put("DATA", mapDataRet);
			  mapResult.put("PAYMENT_MESSAGE", mapData);
			  String returnJson = gson.toJson(mapResult, Map.class);
			  return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(returnJson);
    	}else{
    		Map mapDataRet = new HashMap(); 
			mapDataRet.put("item", "Payment Transaction Fail");
			
			Map mapResult = new HashMap();
			Map mapData = new HashMap();
			
			  mapData.put("MESSAGE", "Fail");
			  mapData.put("CODE", "500");
			  mapData.put("INFO", "Payment Transaction Fail");
			  mapData.put("DATA", mapDataRet);
			  mapResult.put("PAYMENT_MESSAGE", mapData);
			  String returnJson = gson.toJson(mapResult, Map.class);
			  return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON_UTF8).body(returnJson);
    	}
		
	}

}
