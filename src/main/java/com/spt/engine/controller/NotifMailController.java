package com.spt.engine.controller;

import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import com.spt.engine.entity.app.*;
import com.spt.engine.repository.app.EmployeeReplacementRepository;
import lombok.Synchronized;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.parser.node.SimpleNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.general.MasterDataDetail;
import com.spt.engine.entity.general.Parameter;
import com.spt.engine.entity.general.ParameterDetail;
import com.spt.engine.repository.app.DocumentRepository;
import com.spt.engine.repository.app.EmailTransactionRepository;
import com.spt.engine.repository.general.MasterDataDetailRepository;
import com.spt.engine.repository.general.ParameterRepository;
import com.spt.engine.service.EmplyeeProfileService;
import com.spt.engine.service.SendMailService;
import com.spt.engine.util.NumberUtil;

@RestController
public class NotifMailController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(NotifMailController.class);
	
	@Autowired
	EntityManager entityManager;

	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
		public Date deserialize(JsonElement json, Type typeOfT,
								JsonDeserializationContext context) throws JsonParseException {
			return json == null ? null : new Date(json.getAsLong());
		}
	};


	protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();

	@Autowired
	DocumentRepository documentRepository;
	

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
	MasterDataDetailRepository masterDataDetailRepository;
	
    @Autowired
    EmplyeeProfileService employeeProfileService;
    
    @Autowired
    EmailTransactionRepository emailTransactionRepository;
    
    @Autowired
    SendMailService sendMailService;
    

	@Autowired
	ParameterRepository parameterRepository;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
	EmployeeReplacementRepository employeeReplacementRepository;
    
    @Value("${AppServer}")
	String AppServer;
    
    
    @Value("${EmailServer}")
	String EmailServer;
    @Value("${EmailFrom}")
    String EmailFrom ;
    @Value("${EmailFromAddress}")
    String EmailFromAddress ;
    @Value("${EmailToAddress}")
    String EmailToAddress ;
    

    private RuntimeServices runtimeServices;            
    private StringReader stringReader;
    private SimpleNode simpleNode;
	private Template template;
	
	private  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private  DecimalFormat formatter = new DecimalFormat("#,##0.00");


	@GetMapping("/flowMail/{docNo}/{flowStep}/{flowAction}")
    ResponseEntity<String> flowMail(@PathVariable(value = "docNo") String docNumber,
    		@PathVariable(value = "flowStep") String flowStep,
    		@PathVariable(value = "flowAction") String flowAction,
    		HttpServletRequest request) { 
	
		LOGGER.info("===== flowMail=|{}| |{}| |{}|",docNumber,flowStep,flowAction);
		boolean hasError = true;
		try {
			/* Prepare Data */
			Document document        = documentRepository.findByDocNumber(docNumber);
			if(document==null){
				document        = documentRepository.findByTmpDocNumber(docNumber);
			}
			
			Set<String> userRejectCc = new HashSet();
			
			String emailTemplateCode = document.getDocumentType()+"_"+flowStep+"_"+flowAction;

			String linkDetail = ""; 
			Map requesterMap         = getMapFromUsername(document.getRequester());
			
			String requester         = getEmailAddressFromUsername(requesterMap) ;
			String requesterName     = getNameFromUsername(requesterMap) ;
			
			Map createrMap         = getMapFromUsername(document.getCreatedBy());
			String creater         = getEmailAddressFromUsername(createrMap) ;
			userRejectCc.add(creater);
			String nextApprover      = null;
			String nextApproverName  = null;
			String nextApproverUserName  = null;
			
			
			
			/* Build Content Approver */
			StringBuilder approverBuilder = new StringBuilder();
			if(null != document.getRequest().getRequestApprovers()) {
				
				List<RequestApprover> approverList = new ArrayList<>(document.getRequest().getRequestApprovers());
	            Collections.sort(approverList, new Comparator<RequestApprover>() {
	                @Override
	                public int compare(final RequestApprover object1, final RequestApprover object2) {
	                    return object1.getSequence().compareTo(object2.getSequence());
	                }
	            } );
	            
	            if("I".equals(flowAction)){
	            	String color = "green";
	            	for(RequestApprover approver:approverList){
	            		if(flowStep.equals(approver.getActionState())){
	            			Map approverMap         = getMapFromUsername(approver.getUserNameApprover());
	            			nextApprover            = getEmailAddressFromUsername(approverMap) ; 
	            			nextApproverName        = approver.getApprover();
	            			nextApproverUserName    = approver.getUserNameApprover();
	            		}
	            		
	            		String actionStateName = approver.getActionStateName();
	            		String approverName = approver.getApprover();
	            		if("null".equals(String.valueOf(approverName))){
	            			approverName = "";
	            		}
	            		if("Verify".equals(actionStateName)){
	            			actionStateName = "ผู้ตรวจสอบ";
	            		}else if("Approver".equals(actionStateName)){
	            			actionStateName = "ผู้มีอำนาจอนุมัติ";
	            		}else if("Account".equals(actionStateName)){
	            			actionStateName  = "แผนกบัญชี";
	            		}else if("Finance".equals(actionStateName)){
	            			actionStateName = "แผนกการเงิน";
	            		}else if("HR".equals(actionStateName)){
	            			actionStateName = "แผนกบุคคล";
	            		}else if("Admin".equals(actionStateName)){
	            			actionStateName = "แผนกธุรการ";
	            		}else if("Paid".equals(actionStateName)){
	            			actionStateName = "โอนเงินให้พนักงาน";
	            			approverName = "ธนาคาร";
	            		}else{
	            			actionStateName = "";
	            		}
	            		
	            		
	            		if(flowStep.equals(approver.getActionState())){
            				color = "blue";
            			}
	            		
	            		
	            		if("green".equals(color)){
	            			approverBuilder.append("<tr><td><div align=\"center\">"+actionStateName+"</div></td><td><div align=\"left\">"+approverName+"</div></td><td><div align=\"center\" style=\"color:#00CC33; font-weight: bold;\">อนุมัติ</div></td></tr>");
	            		}else if("blue".equals(color)){
	            			approverBuilder.append("<tr><td><div align=\"center\"  style=\"color:#0000FF; font-weight: bold;\">"+actionStateName+"</div></td><td><div align=\"left\"  style=\"color:#0000FF; font-weight: bold;\">"+approverName+"</div></td><td><div align=\"center\"  style=\"color:#0000FF; font-weight: bold;\">รอพิจารณา</div></td></tr>");
	            			color = "none";
	            		}else {
	            			approverBuilder.append("<tr><td><div align=\"center\">"+actionStateName+"</div></td><td><div align=\"left\">"+approverName+"</div></td><td><div align=\"center\" ></div></td></tr>");
	            		}
	            		
					}
	            }else if("R".equals(flowAction)){
	            	String color = "green";
	            	for(RequestApprover approver:approverList){
	            		if(flowStep.equals(approver.getActionState())){
	            			Map approverMap         = getMapFromUsername(approver.getUserNameApprover());
	            			nextApprover            = getEmailAddressFromUsername(approverMap) ; 
	            			nextApproverName        = approver.getApprover();
	            			nextApproverUserName    = approver.getUserNameApprover();
	            		}
	            		
	            		String actionStateName = approver.getActionStateName();
	            		String approverName = approver.getApprover();
	            		if("null".equals(String.valueOf(approverName))){
	            			approverName = "";
	            		}
	            		if("Verify".equals(actionStateName)){
	            			actionStateName = "ผู้ตรวจสอบ";
	            		}else if("Approver".equals(actionStateName)){
	            			actionStateName = "ผู้มีอำนาจอนุมัติ";
	            		}else if("Account".equals(actionStateName)){
	            			actionStateName  = "แผนกบัญชี";
	            		}else if("Finance".equals(actionStateName)){
	            			actionStateName = "แผนกการเงิน";
	            		}else if("HR".equals(actionStateName)){
	            			actionStateName = "แผนกบุคคล";
	            		}else if("Admin".equals(actionStateName)){
	            			actionStateName = "แผนกธุรการ";
	            		}else if("Paid".equals(actionStateName)){
	            			actionStateName = "โอนเงินให้พนักงาน";
	            			approverName = "ธนาคาร";
	            		}else{
	            			actionStateName = "";
	            		}
	            		
	            		
	            		if(flowStep.equals(approver.getActionState())){
            				color = "blue";
            			}
	            		
	            		
	            		if("green".equals(color)){
	            			Map approverMap         = getMapFromUsername(approver.getUserNameApprover());
	            			userRejectCc.add(getEmailAddressFromUsername(approverMap));
	            			approverBuilder.append("<tr><td><div align=\"center\">"+actionStateName+"</div></td><td><div align=\"left\">"+approverName+"</div></td><td><div align=\"center\" style=\"color:#00CC33; font-weight: bold;\">อนุมัติ</div></td></tr>");
	            		}else if("blue".equals(color)){
	            			approverBuilder.append("<tr><td><div align=\"center\"  style=\"color:#FF0000; font-weight: bold;\">"+actionStateName+"</div></td><td><div align=\"left\"  style=\"color:#FF0000; font-weight: bold;\">"+approverName+"</div></td><td><div align=\"center\"  style=\"color:#FF0000; font-weight: bold;\">ไม่อนุมัติ</div></td></tr>");
	            			color = "none";
	            		}else {
	            			approverBuilder.append("<tr><td><div align=\"center\">"+actionStateName+"</div></td><td><div align=\"left\">"+approverName+"</div></td><td><div align=\"center\" ></div></td></tr>");
	            		}
	            		
					}
	            }else if("A".equals(flowAction)){
	            	String color = "green";
	            	for(RequestApprover approver:approverList){
	            		if(flowStep.equals(approver.getActionState())){
	            			Map approverMap         = getMapFromUsername(approver.getUserNameApprover());
	            			nextApprover            = getEmailAddressFromUsername(approverMap) ; 
	            			nextApproverName        = approver.getApprover();
	            			nextApproverUserName    = approver.getUserNameApprover();
	            		}
	            		
	            		String actionStateName = approver.getActionStateName();
	            		String approverName = approver.getApprover();
	            		if("null".equals(String.valueOf(approverName))){
	            			approverName = "";
	            		}
	            		if("Verify".equals(actionStateName)){
	            			actionStateName = "ผู้ตรวจสอบ";
	            		}else if("Approver".equals(actionStateName)){
	            			actionStateName = "ผู้มีอำนาจอนุมัติ";
	            		}else if("Account".equals(actionStateName)){
	            			actionStateName  = "แผนกบัญชี";
	            		}else if("Finance".equals(actionStateName)){
	            			actionStateName = "แผนกการเงิน";
	            		}else if("HR".equals(actionStateName)){
	            			actionStateName = "แผนกบุคคล";
	            		}else if("Admin".equals(actionStateName)){
	            			actionStateName = "แผนกธุรการ";
	            		}else if("Paid".equals(actionStateName)){
	            			actionStateName = "โอนเงินให้พนักงาน";
	            			approverName = "ธนาคาร";
	            		}else{
	            			actionStateName = "";
	            		}
	            		
	            		
	            		if(flowStep.equals(approver.getActionState())){
	        				color = "blue";
	        			}
	            		
	            		
	            		if("green".equals(color)){
	            			approverBuilder.append("<tr><td><div align=\"center\">"+actionStateName+"</div></td><td><div align=\"left\">"+approverName+"</div></td><td><div align=\"center\" style=\"color:#00CC33; font-weight: bold;\">อนุมัติ</div></td></tr>");
	            		}else if("blue".equals(color)){
	            			approverBuilder.append("<tr><td><div align=\"center\"  style=\"color:#0000FF; font-weight: bold;\">"+actionStateName+"</div></td><td><div align=\"left\"  style=\"color:#0000FF; font-weight: bold;\">"+approverName+"</div></td><td><div align=\"center\"  style=\"color:#0000FF; font-weight: bold;\">รอพิจารณา</div></td></tr>");
	            			color = "none";
	            		}else {
	            			approverBuilder.append("<tr><td><div align=\"center\">"+actionStateName+"</div></td><td><div align=\"left\">"+approverName+"</div></td><td><div align=\"center\" ></div></td></tr>");
	            		}
	            		
					}
	            }
			}
			
			/* Build Content Detail */
			
			
		  
			Double totalAmount = document.getTotalAmount();
		  
		  

			StringBuilder detailBuilder = new StringBuilder();
			if(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_EXPENSE.equals(document.getDocumentType())){
				if("EXP_PAID_A".equals(emailTemplateCode)){
					totalAmount = 0d;
					for(DocumentExpenseGroup group:document.getDocumentExpense().getDocumentExpenseGroups()){
						String expenseTypeDescriotion = group.getExpenseTypeByCompany().getExpenseType().getDescription();
						String expenseTypeCode = group.getExpenseTypeByCompany().getExpenseType().getCode();
						detailBuilder.append("<tr><td bgcolor=\"#0066CC\">&nbsp;</td><td colspan=\"3\" bgcolor=\"#0066CC\"><span style=\"color: #FFFFFF\">"+expenseTypeDescriotion+"</span></td></tr>");
						detailBuilder.append("<tr><td width=\"22\"><div align=\"center\"></div></td><td width=\"425\"><div align=\"center\">รายการ</div></td><td width=\"37\"><div align=\"center\"></div></td><td width=\"352\"><div align=\"center\">จำนวนเงิน(บาท)</div></td></tr>");
						
						for(DocumentExpenseItem item:document.getDocumentExpense().getDocumentExpenseItems()){
							if(expenseTypeCode.equals(item.getExpenseTypeByCompanys().getExpenseType().getCode())){
//								detailBuilder.append("<tr><td>&nbsp;</td><td><div align=\"right\">"+item.getRemark()+"</div></td><td>&nbsp;</td><td><div align=\"right\">"+NumberUtil.getDouble(item.getPaymentAmount())+"</div></td></tr>");
								detailBuilder.append("<tr><td>&nbsp;</td><td><div align=\"right\">"+item.getRemark()+"</div></td><td>&nbsp;</td><td><div align=\"right\">"+(NumberUtil.getDouble(item.getNetAmount())+NumberUtil.getDouble(item.getWhtAmount()))+"</div></td></tr>");
								totalAmount += NumberUtil.getDouble((NumberUtil.getDouble(item.getNetAmount())+NumberUtil.getDouble(item.getWhtAmount())));
							}
						}
						detailBuilder.append("<tr><td>&nbsp;</td><td><div align=\"right\"></div></td><td>&nbsp;</td><td><div align=\"right\"></div></td></tr>");
						
					}
					if(flowStep.equals("ACC") || flowStep.equals("HR")){
						linkDetail = AppServer+"/expense/clearExpenseDetail?doc="+document.getId();
					}else{
						linkDetail = AppServer+"/expense/expenseDetail?doc="+document.getId();
					}
				}else{
					for(DocumentExpenseGroup group:document.getDocumentExpense().getDocumentExpenseGroups()){
						String expenseTypeDescriotion = group.getExpenseTypeByCompany().getExpenseType().getDescription();
						String expenseTypeCode = group.getExpenseTypeByCompany().getExpenseType().getCode();
						detailBuilder.append("<tr><td bgcolor=\"#0066CC\">&nbsp;</td><td colspan=\"3\" bgcolor=\"#0066CC\"><span style=\"color: #FFFFFF\">"+expenseTypeDescriotion+"</span></td></tr>");
						detailBuilder.append("<tr><td width=\"22\"><div align=\"center\"></div></td><td width=\"425\"><div align=\"center\">รายการ</div></td><td width=\"37\"><div align=\"center\"></div></td><td width=\"352\"><div align=\"center\">จำนวนเงิน(บาท)</div></td></tr>");
						
						for(DocumentExpenseItem item:document.getDocumentExpense().getDocumentExpenseItems()){
							if(expenseTypeCode.equals(item.getExpenseTypeByCompanys().getExpenseType().getCode())){
								detailBuilder.append("<tr><td>&nbsp;</td><td><div align=\"right\">"+item.getRemark()+"</div></td><td>&nbsp;</td><td><div align=\"right\">"+item.getAmount()+"</div></td></tr>");
							}
						}
						detailBuilder.append("<tr><td>&nbsp;</td><td><div align=\"right\"></div></td><td>&nbsp;</td><td><div align=\"right\"></div></td></tr>");
						
					}
					if(flowStep.equals("ACC") || flowStep.equals("HR")){
						linkDetail = AppServer+"/expense/clearExpenseDetail?doc="+document.getId();
					}else{
						linkDetail = AppServer+"/expense/expenseDetail?doc="+document.getId();
					}
				}
				  
				
				
			}else if(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_ADVANCE.equals(document.getDocumentType())){
				String titleDescription = document.getTitleDescription();
				if(titleDescription == null){
					titleDescription = "";
				}
				detailBuilder.append("เงินยืมทดรองสำหรับ "+titleDescription);
				linkDetail = AppServer+"/advance/advanceDetail?doc="+document.getId();
			}else if(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_APPROVE.equals(document.getDocumentType())){
				
				String titleDescription = document.getTitleDescription();
				if(titleDescription == null){
					titleDescription = "";
				}
				
				String propose = "";
				List<String> approveTypeParameter = new ArrayList<String>();
				approveTypeParameter.add("M002");
				
				MasterDataDetail masterDataDetail;
				try {
					masterDataDetail = masterDataDetailRepository.findByMasterdataInAndMasterDataDetailCodeOrderByCode(approveTypeParameter, document.getApproveType());
					propose = propose+masterDataDetail.getDescription().replaceAll("รวมชุด", "");
				} catch (Exception e) { }
				
				propose = propose+" "+titleDescription;
				detailBuilder.append(propose);
				linkDetail = AppServer+"/approve/viewCreateDocSetDetail?doc="+document.getId();
			}
			
			/* Get Account Team */
			if(flowStep.equals("ACC")){
				List<String> userAccountRelationParameter = new ArrayList<String>();
				userAccountRelationParameter.add(ConstantVariable.MASTER_DATA_ACCOUNT_TEAM);
				
				try {
					List<MasterDataDetail> masterDataDetailLs = masterDataDetailRepository.findByMasterdataInOrderByCode(userAccountRelationParameter);
					if(masterDataDetailLs!=null) for(MasterDataDetail masterDataDetail:masterDataDetailLs){
						if(masterDataDetail.getVariable1() != null && masterDataDetail.getVariable1().equals(nextApproverUserName)){
							nextApprover            += ","+getEmailAddressFromUsername(getMapFromUsername(masterDataDetail.getCode())) ; 
						}
					}
				} catch (Exception e) { }
			}
			
			
	
			/* Prepare Template */
			String subjectTemplate = "";
			String contentTemplate = "";
			List<String> emailTemplateParameter = new ArrayList<String>();
			emailTemplateParameter.add(ConstantVariable.MASTER_DATA_EMAIL_TEMPLATE);
			
			MasterDataDetail masterDataDetail;
			try {
				masterDataDetail = masterDataDetailRepository.findByMasterdataInAndMasterDataDetailCodeOrderByCode(emailTemplateParameter, emailTemplateCode);
				subjectTemplate = masterDataDetail.getVariable1();
				contentTemplate = masterDataDetail.getVariable2();

			} catch (Exception e) { /* Log Error */}
			
			/*Adjust some value*/
			if(flowStep.equals("ACC")){
				nextApproverName = "แผนกบัญชี";
			}
			
			/* Prepare Attach File */
			ArrayList<String> filePath = new ArrayList();
			ArrayList<String> fileName = new ArrayList();
			String attachmentPath = getAttachmentPath();
			if(document.getDocumentAttachment() !=null ) for (DocumentAttachment documentAttachment :document.getDocumentAttachment()){
				if(documentAttachment.getRealFileName()!=null){
					String abslutePath = attachmentPath+documentAttachment.getRealFileName();
					filePath.add(abslutePath);

					fileName.add(documentAttachment.getFileName());
				}
			}
			
			if(document.getDocumentExpense() !=null && document.getDocumentExpense().getDocumentExpenseItems()!=null ) for(DocumentExpenseItem item:document.getDocumentExpense().getDocumentExpenseItems()){
				if(item.getdocExpItemAttachment()!=null) for(DocumentExpItemAttachment itemAttachment :item.getdocExpItemAttachment()){
					if(itemAttachment.getRealFileName() != null){
						String abslutePath = attachmentPath+itemAttachment.getRealFileName();
						filePath.add(abslutePath);

						fileName.add(itemAttachment.getFileName());
					}
				}
			}
			
			if("APP_APR_A".equals(emailTemplateCode)){
				if(document.getCcMailAddress() != null){
					nextApproverName = "แผนกธุรการ";
				}
				
    		}
			

			
			/*Build Data*/
			Map databind = new HashMap();
			databind.put("REQUESTER", requesterName);
			databind.put("APPROVER", nextApproverName);
			databind.put("DOCNUMBER", document.getDocNumber());
			databind.put("TOTALAMOUNT", totalAmount);
			databind.put("CONTENT_APPROVER", approverBuilder.toString());
			databind.put("CONTENT_DETAIL", detailBuilder);
			databind.put("LINK_DETAIL", linkDetail);
			databind.put("LINK_APPROVE", AppServer+"/requests/approveRequestRedirect/"+document.getDocNumber()+"/"+nextApproverUserName+"/"+flowStep);
			databind.put("LINK_REJECT", AppServer+"/requests/rejectRequestRedirect/"+document.getDocNumber()+"/"+nextApproverUserName+"/"+flowStep);
			
			/* Prepare Mail*/
			String from = this.EmailFrom;
			String fromAddress        = this.EmailFromAddress;
			
			String subject     = getContentEmail(subjectTemplate,databind);
			String addressTo   = nextApprover;
			String contentText = getContentEmail(contentTemplate,databind);
			String ccAddress   = creater;
			
			/* Send to Account When Complete*/
			if("APP_APR_A".equals(emailTemplateCode)){
				if(document.getCcMailAddress() != null && document.getCcMailAddress().trim().length() >0){
					addressTo = document.getCcMailAddress();
				}else{
					contentText = null;
				}
			}

			if("R".equals(flowAction)){
				addressTo = creater;
			}
			
			List<String> personalAssistParameter = new ArrayList<String>();
			personalAssistParameter.add(ConstantVariable.MASTER_DATA_PERSONAL_ASSISTANCE);
			
			MasterDataDetail personalAssistMasterDataDetail;
			try {
				personalAssistMasterDataDetail = masterDataDetailRepository.findByMasterdataInAndMasterDataDetailCodeOrderByCode(personalAssistParameter, nextApproverUserName);
				ccAddress = ccAddress + ","+personalAssistMasterDataDetail.getVariable1();


			} catch (Exception e) {/* Log Error */}
			
			
			if("R".equals(flowAction)){
				String userRejectCcStr = "";
				for(String cc:userRejectCc){
					userRejectCcStr += ","+cc;
				}
				ccAddress = userRejectCcStr.substring(1);
			}
			
			if("PAID".equals(flowStep)){
				addressTo = requester;
				ccAddress = creater;
			}
			

			LOGGER.info("nextApproverUserName={}",nextApproverUserName);
			LOGGER.info("Simulate Send Email={}",nextApprover);
			LOGGER.info("Simulate addressTo Send Email={}",addressTo);
			LOGGER.info("Simulate ccAddress Send Email={}",ccAddress);
			
			
			EmailToAddress = getDefaultEmailTo();
			
			if(EmailToAddress!=null && EmailToAddress.trim().length() > 0){
				addressTo = this.EmailToAddress;
				ccAddress = this.EmailToAddress;
				String ccDefault = getDefaultEmailCc();
				if(ccDefault!=null && ccDefault.trim().length() > 0){
					ccAddress = ccDefault ;
				}
			}
			
			LOGGER.info("addressTo Send Email={}",addressTo);
			LOGGER.info("ccAddress Send Email={}",ccAddress);

			if(contentText!=null&&contentText.trim().length() > 0){
				/* Mail to Requester */
			    emailTransactionRepository.saveAndFlush(new EmailTransaction(fromAddress, addressTo, ccAddress, new Timestamp((new Date()).getTime()),document.getDocNumber(), emailTemplateCode, "TO_APPROVER"));
				LOGGER.info("=========== Before send mail =============");
				sendMailService.sendMail(from, fromAddress, addressTo, subject, contentText, ccAddress,filePath.toArray(new String[0]),fileName.toArray(new String[0]));
				LOGGER.info("=========== After send mail =============");
			}else{
				LOGGER.info("NO CONTENT TEMPLATE EMAIL FOR {}",emailTemplateCode);
			}
			
			LOGGER.info("===== SEND MAIL in emailTemplateCode={}",emailTemplateCode);
			
			
			

			
			
			hasError = false;
		} catch (Exception e) { /* Log Error */e.printStackTrace();}
		
		
		
		
		if(!hasError){
			Map mapDataRet = new HashMap(); 
			mapDataRet.put("item", "Send Mail successfully");
			
			Map mapResult = new HashMap();
			 Map mapData = new HashMap();
			
			  mapData.put("MESSAGE", "Success");
			  mapData.put("CODE", "200");
			  mapData.put("INFO", "Send Mail Completed");
			  mapData.put("DATA", mapDataRet);
			  mapResult.put("SENDMAIL_MESSAGE", mapData);
			  String returnJson = gson.toJson(mapResult, Map.class);
			  return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(returnJson);
    	}else{
    		Map mapDataRet = new HashMap(); 
			mapDataRet.put("item", "Payment Transaction Fail");
			
			Map mapResult = new HashMap();
			Map mapData = new HashMap();
			
			  mapData.put("MESSAGE", "Fail");
			  mapData.put("CODE", "500");
			  mapData.put("INFO", "Send Mail Fail");
			  mapData.put("DATA", mapDataRet);
			  mapResult.put("SENDMAIL_MESSAGE", mapData);
			  String returnJson = gson.toJson(mapResult, Map.class);
			  return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON_UTF8).body(returnJson);
    	}
		
	}
	

	
	private String getAttachmentPath(){
		Parameter parameter = parameterRepository.findByCode("P103");
		String pathFile = "";
		for(ParameterDetail detail:parameter.getDetails()){
			pathFile = detail.getVariable1();
			break;
		}
		
		return pathFile;
	}
	

	
	private String getDefaultEmailTo(){
		Parameter parameter = parameterRepository.findByCode("P102");
		String defaultEmailTo = "";
		for(ParameterDetail detail:parameter.getDetails()){
			defaultEmailTo = detail.getVariable1();
			break;
		}
		
		return defaultEmailTo;
	}
	

	private String getDefaultEmailCc(){
		Parameter parameter = parameterRepository.findByCode("P102");
		String defaultEmailTo = "";
		for(ParameterDetail detail:parameter.getDetails()){
			defaultEmailTo = detail.getVariable2();
			break;
		}
		
		return defaultEmailTo;
	}
	

	private Map getMapFromUsername(String username){
		ResponseEntity<String> response = employeeProfileService.findEmployeeProfileByEmployeeCode(username);
		return gson.fromJson(response.getBody(), Map.class);
	}
	
	private String getEmailAddressFromUsername(Map dataProfile){
		return String.valueOf(dataProfile.get("Email")).toLowerCase();
	}

	private String getEmailAddressFromUsernameForSummary(Map dataProfile){
		String email = "null";
		if("204".equals(String.valueOf(dataProfile.get("Attorney_ID")))){
			List<EmployeeReplacement> employeeReplacement = employeeReplacementRepository.findByEmpCodeReplace(String.valueOf(dataProfile.get("User_name")));
			if(employeeReplacement.size() >0){
				ResponseEntity<String> response = employeeProfileService.findEmployeeProfileByEmployeeCode(employeeReplacement.get(0).getEmpCodeAgree());
				Map dataNew = gson.fromJson(response.getBody(), Map.class);
				email = String.valueOf(dataNew.get("Email")).toLowerCase();
			}
		}else{
			email = String.valueOf(dataProfile.get("Email")).toLowerCase();
		}
		return email;
	}
	
	private String getNameFromUsername(Map dataProfile){
		return String.valueOf(dataProfile.get("FOA")) + String.valueOf(dataProfile.get("FNameTH")) + " " + String.valueOf(dataProfile.get("LNameTH"));
	}
	
	private String getEngNameFromUsername(Map dataProfile){
		return String.valueOf(dataProfile.get("NameEN"));
	}
	
	private String getContentEmail(String templateContent,Map mapContext) throws Exception{
		if(templateContent!=null&&templateContent.trim().length() > 0){
			runtimeServices = RuntimeSingleton.getRuntimeServices();            
			stringReader = new StringReader(templateContent);
			simpleNode = runtimeServices.parse(stringReader, "Email Template");
			 
			template = new Template();
			template.setRuntimeServices(runtimeServices);
			template.setData(simpleNode);
			template.initDocument();
			  
	        /*  add that list to a VelocityContext  */
	        VelocityContext context = new VelocityContext(mapContext);
	        
	        /*  get the Template  */
	        /*  now render the template into a Writer  */
	        StringWriter writer = new StringWriter();
	        template.merge( context, writer );
			return writer.toString();
		}else{
			return templateContent;
		}
		
	}
	
	@Transactional
	@GetMapping("/sendMailAccount/{docAccountRemark}")
    ResponseEntity<String> sendMailAccount(@PathVariable(value = "docAccountRemark")  DocumentAccountRemark documentAccountRemark) { 
		
		LOGGER.info("===== sendMailAccount=|{}|",documentAccountRemark);
		boolean hasError = true;
		try {
			String remarkDescription = documentAccountRemark.getRemarkDescription();
			Document document = documentRepository.getOne(documentAccountRemark.getDocumentId());
            Map requesterMap         = getMapFromUsername(document.getRequester());
			String requester         = getEmailAddressFromUsername(requesterMap) ;
			String requesterName     = getNameFromUsername(requesterMap) ;
			
			Map createrMap         = getMapFromUsername(document.getCreatedBy());
			String creater         = getEmailAddressFromUsername(createrMap) ;
			
			Map accountanceMap         = getMapFromUsername(documentAccountRemark.getCreatedBy());
			String accountance         = getEmailAddressFromUsername(accountanceMap) ;
			String accountanceName     = getNameFromUsername(accountanceMap) ;
			
			String emailTemplateCode = "ACCOUNT_NOTIFICATION";

			/* Prepare Template */
			String subjectTemplate = "";
			String contentTemplate = "";
			List<String> emailTemplateParameter = new ArrayList<String>();
			emailTemplateParameter.add(ConstantVariable.MASTER_DATA_EMAIL_TEMPLATE);
			
			String linkDetail = ""; 
			MasterDataDetail masterDataDetail;
			try {
				masterDataDetail = masterDataDetailRepository.findByMasterdataInAndMasterDataDetailCodeOrderByCode(emailTemplateParameter, emailTemplateCode);
				subjectTemplate = masterDataDetail.getVariable1();
				contentTemplate = masterDataDetail.getVariable2();

			} catch (Exception e) { /* Log Error */}
			
			Double totalAmount = document.getTotalAmount();
			StringBuilder detailBuilder = new StringBuilder();
			if(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_EXPENSE.equals(document.getDocumentType())){
				totalAmount = 0d;
				for(DocumentExpenseGroup group:document.getDocumentExpense().getDocumentExpenseGroups()){
					String expenseTypeDescriotion = group.getExpenseTypeByCompany().getExpenseType().getDescription();
					String expenseTypeCode = group.getExpenseTypeByCompany().getExpenseType().getCode();
					detailBuilder.append("<tr><td bgcolor=\"#0066CC\">&nbsp;</td><td colspan=\"3\" bgcolor=\"#0066CC\"><span style=\"color: #FFFFFF\">"+expenseTypeDescriotion+"</span></td></tr>");
					detailBuilder.append("<tr><td width=\"22\"><div align=\"center\"></div></td><td width=\"425\"><div align=\"center\">รายการ</div></td><td width=\"37\"><div align=\"center\"></div></td><td width=\"352\"><div align=\"center\">จำนวนเงิน(บาท)</div></td></tr>");
					
					for(DocumentExpenseItem item:document.getDocumentExpense().getDocumentExpenseItems()){
						if(expenseTypeCode.equals(item.getExpenseTypeByCompanys().getExpenseType().getCode())){
//							detailBuilder.append("<tr><td>&nbsp;</td><td><div align=\"right\">"+item.getRemark()+"</div></td><td>&nbsp;</td><td><div align=\"right\">"+NumberUtil.getDouble(item.getPaymentAmount())+"</div></td></tr>");
							detailBuilder.append("<tr><td>&nbsp;</td><td><div align=\"right\">"+item.getRemark()+"</div></td><td>&nbsp;</td><td><div align=\"right\">"+(NumberUtil.getDouble(item.getNetAmount())+NumberUtil.getDouble(item.getWhtAmount()))+"</div></td></tr>");
							totalAmount += NumberUtil.getDouble((NumberUtil.getDouble(item.getNetAmount())+NumberUtil.getDouble(item.getWhtAmount())));
						}

					}
					detailBuilder.append("<tr><td>&nbsp;</td><td><div align=\"right\"></div></td><td>&nbsp;</td><td><div align=\"right\"></div></td></tr>");
					
				}
				
				linkDetail = AppServer+"/expense/expenseDetail?doc="+document.getId();
			}else if(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_ADVANCE.equals(document.getDocumentType())){
				linkDetail = AppServer+"/advance/advanceDetail?doc="+document.getId();
			}else if(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_APPROVE.equals(document.getDocumentType())){
				linkDetail = AppServer+"/approve/viewCreateDocSetDetail?doc="+document.getId();
			}
			
			/*Build Data*/
			Map databind = new HashMap();
			databind.put("REQUESTER", requesterName);
			databind.put("ACCOUNT", accountanceName);
			databind.put("DOCNUMBER", document.getDocNumber());
			databind.put("TOTALAMOUNT", totalAmount);
			databind.put("CONTENT_DETAIL", remarkDescription);
			databind.put("REMARK", remarkDescription);
			databind.put("LINK_DETAIL", linkDetail);

			
			/* Prepare Mail*/
			String from = this.EmailFrom;
			String fromAddress= this.EmailFromAddress;
			
			String subject     = getContentEmail(subjectTemplate,databind);
			String addressTo   = creater;
			String contentText = getContentEmail(contentTemplate,databind);
			String ccAddress   = requester;
			
			
			
			
			LOGGER.info("Simulate ACCOUNT_NOTIFICATION Send Email={}",creater);
			
			EmailToAddress = getDefaultEmailTo();
			
			if(EmailToAddress!=null && EmailToAddress.trim().length() > 0){
				addressTo = this.EmailToAddress;
				ccAddress = this.EmailToAddress;
			}
			
			LOGGER.info("addressTo Send Email={}",addressTo);
			LOGGER.info("ccAddress Send Email={}",ccAddress);

			if(contentText!=null&&contentText.trim().length() > 0){
				/* Mail to Requester */
			    emailTransactionRepository.saveAndFlush(new EmailTransaction(fromAddress, addressTo, ccAddress, new Timestamp((new Date()).getTime()),document.getDocNumber(), emailTemplateCode, "TO_APPROVER"));
				sendMailService.sendMail(from, fromAddress, addressTo, subject, contentText, ccAddress,null,null);
			}else{
				LOGGER.info("NO CONTENT TEMPLATE EMAIL FOR {}",emailTemplateCode);
			}
			
			LOGGER.info("===== SEND MAIL in ACCOUNT_NOTIFICATION={}",emailTemplateCode);
			
			
			hasError = false;
		} catch (Exception e) { /* Log Error */e.printStackTrace();}
		
		if(!hasError){
			Map mapDataRet = new HashMap(); 
			mapDataRet.put("item", "Send Mail successfully");
			
			Map mapResult = new HashMap();
			 Map mapData = new HashMap();
			
			  mapData.put("MESSAGE", "Success");
			  mapData.put("CODE", "200");
			  mapData.put("INFO", "Send Mail Completed");
			  mapData.put("DATA", mapDataRet);
			  mapResult.put("SENDMAIL_MESSAGE", mapData);
			  String returnJson = gson.toJson(mapResult, Map.class);
			  return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(returnJson);
    	}else{
    		Map mapDataRet = new HashMap(); 
			mapDataRet.put("item", "Payment Transaction Fail");
			
			Map mapResult = new HashMap();
			Map mapData = new HashMap();
			
			  mapData.put("MESSAGE", "Fail");
			  mapData.put("CODE", "500");
			  mapData.put("INFO", "Send Mail Fail");
			  mapData.put("DATA", mapDataRet);
			  mapResult.put("SENDMAIL_MESSAGE", mapData);
			  String returnJson = gson.toJson(mapResult, Map.class);
			  return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON_UTF8).body(returnJson);
    	}
		
		
	}

	@Transactional
	@GetMapping("/summaryMailApprove")
    ResponseEntity<String> summaryMailApprove() { 
		
		LOGGER.info("===== summaryMailApprove=|{}|",new Date());
		boolean hasError = true;
		try {

			Set<String> personSet = new HashSet<String>();
			
			/* Find Data for Approver */
			Map<String,Map> mapDataPersonOfApprove = new HashMap<String,Map>();
			String sqlOfApprove = "SELECT COUNT(1) as COUNT,R.next_approver as NEXT_APPROVER,D.document_type AS DOCUMENT_TYPE  FROM document D LEFT JOIN request R ON R.id = D.request_id WHERE D.document_status = 'ONP' GROUP BY R.next_approver,D.document_type ";
			/* Fetch Data From Database */
			
	        List<Map<String,Object>> dataListOfApprove = namedParameterJdbcTemplate.queryForList(sqlOfApprove, new MapSqlParameterSource());
	        if(dataListOfApprove !=null ) for(Map<String,Object> modelResult:dataListOfApprove){
	        	String apprver = String.valueOf(modelResult.get("NEXT_APPROVER")) ;
	        	Map<String,Object> dataPerson = mapDataPersonOfApprove.get(apprver);
	        	if(dataPerson == null){
	        		dataPerson = new HashMap<String,Object>();
	        	}
	        	String docType = String.valueOf(modelResult.get("DOCUMENT_TYPE"));
	        	if(docType.equals("APP")){
	        		dataPerson.put("APPROVE", String.valueOf(modelResult.get("COUNT")));
	        	}else if(docType.equals("EXP")){
	        		dataPerson.put("EXPENSE", String.valueOf(modelResult.get("COUNT")));
	        	}else if(docType.equals("ADV")){
	        		dataPerson.put("ADVANCE", String.valueOf(modelResult.get("COUNT")));
	        	}
	        	
	        	mapDataPersonOfApprove.put(apprver, dataPerson);
	        	personSet.add(apprver);
	        }
	        
	        /* Find Data for Requester */
	        Map<String,Map> mapDataPersonOfRequest = new HashMap<String,Map>();
			String sqlOfRequest = "SELECT COUNT(1) as COUNT,D.requester as NEXT_APPROVER,D.document_type AS DOCUMENT_TYPE FROM document D  LEFT JOIN request R ON R.id = D.request_id  WHERE D.document_status = 'ONP'  AND EXISTS ( SELECT 1 FROM request_approver B WHERE B.request = R.id AND B.action_state NOT IN ('ACC','FNC','PAID') AND B.request_status_code IS NULL ) GROUP BY D.requester,D.document_type ";
			
			/* Fetch Data From Database */
	        List<Map<String,Object>> dataListOfRequest = namedParameterJdbcTemplate.queryForList(sqlOfRequest, new MapSqlParameterSource());
	        if(dataListOfRequest !=null ) for(Map<String,Object> modelResult:dataListOfRequest){
	        	String requester = String.valueOf(modelResult.get("NEXT_APPROVER")) ;
	        	Map<String,Object> dataPerson = mapDataPersonOfRequest.get(requester);
	        	if(dataPerson == null){
	        		dataPerson = new HashMap<String,Object>();
	        	}
	        	String docType = String.valueOf(modelResult.get("DOCUMENT_TYPE"));
	        	if(docType.equals("APP")){
	        		dataPerson.put("APPROVE", String.valueOf(modelResult.get("COUNT")));
	        	}else if(docType.equals("EXP")){
	        		dataPerson.put("EXPENSE", String.valueOf(modelResult.get("COUNT")));
	        	}else if(docType.equals("ADV")){
	        		dataPerson.put("ADVANCE", String.valueOf(modelResult.get("COUNT")));
	        	}
	        	
	        	mapDataPersonOfRequest.put(requester, dataPerson);
	        	personSet.add(requester);
	        }
	        
	        
	        //Prepare Mail to
	        for(String person:personSet){
	        	Map userMap         = getMapFromUsername(person);
	        	String userAddress  = getEmailAddressFromUsernameForSummary(userMap) ;
	        	String userFullName = getNameFromUsername(userMap) ;
	        	
	        	String requestApprove = mapDataPersonOfRequest.get(person)!=null ? (String) mapDataPersonOfRequest.get(person).get("APPROVE"):"0"; if(requestApprove==null) {requestApprove="0";}
	        	String requestAdvance = mapDataPersonOfRequest.get(person)!=null ? (String) mapDataPersonOfRequest.get(person).get("ADVANCE"):"0"; if(requestAdvance==null) {requestAdvance="0";}
	        	String requestExpense = mapDataPersonOfRequest.get(person)!=null ? (String) mapDataPersonOfRequest.get(person).get("EXPENSE"):"0"; if(requestExpense==null) {requestExpense="0";}
	        	
	        	String approverApprove = mapDataPersonOfApprove.get(person)!=null ? (String) mapDataPersonOfApprove.get(person).get("APPROVE"):"0"; if(approverApprove==null) {approverApprove="0";}
	        	String approverAdvance = mapDataPersonOfApprove.get(person)!=null ? (String) mapDataPersonOfApprove.get(person).get("ADVANCE"):"0"; if(approverAdvance==null) {approverAdvance="0";}
	        	String approverExpense = mapDataPersonOfApprove.get(person)!=null ? (String) mapDataPersonOfApprove.get(person).get("EXPENSE"):"0"; if(approverExpense==null) {approverExpense="0";}
	        	
	        	
				String from = this.EmailFrom;
				String fromAddress= this.EmailFromAddress;
				
				String subject     = "[EWF] การแจ้งเตือนจากระบบ E-WORKFLOW กลุ่มมิตรผล ";
				String addressTo   = userAddress;
				String contentText = "เรียน "+userFullName+
						"<br><br><br> มีรายการเอกสารของท่านที่สร้างในระบบ E-WORKFLOW และรออยู่ระหว่างดำเนินการ ดังนี้"+
						"<br>"+
						"<table width=\"400\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\" bordercolor=\"#000000\"><tr><td bgcolor=\"#3366FF\"><p align=\"center\" style=\"color: #FFFFFF\" >Approve</p></td><td bgcolor=\"#0066FF\"><p align=\"center\" style=\"color: #FFFFFF\">Advance</p></td><td bgcolor=\"#3366FF\"><p align=\"center\" style=\"color: #FFFFFF\">Expense</p></td></tr>"+
						"<tr><td><div align=\"center\">"+requestApprove+"</div></td><td><div align=\"center\">"+requestAdvance+"</div></td><td><div align=\"center\">"+requestExpense+"</div></td></tr>"+
						"</table>"+
						"<br>"+
						"<br>มีรายการเอกสารที่รอการพิจารณาอนุมัติจากท่านในระบบ E-WORKFLOW ดังนี้"+
						"<br>"+
						"<table width=\"400\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\" bordercolor=\"#000000\"><tr><td bgcolor=\"#3366FF\"><p align=\"center\" style=\"color: #FFFFFF\" >Approve</p></td><td bgcolor=\"#0066FF\"><p align=\"center\" style=\"color: #FFFFFF\">Advance</p></td><td bgcolor=\"#3366FF\"><p align=\"center\" style=\"color: #FFFFFF\">Expense</p></td></tr>"+
						"<tr><td><div align=\"center\">"+approverApprove+"</div></td><td><div align=\"center\">"+approverAdvance+"</div></td><td><div align=\"center\">"+approverExpense+"</div></td></tr>"+
						"</table>"+
						"<br>"+
						"<a href=\"http://ewf.mitrphol.com:8001/GWF\">Link to E-WORKFLOW</a>"+
						"<br><br><br>ขอแสดงความนับถือ"+
						"<br><br><br> Your request lists in E-Workflow system are on process as follows:"+
						"<br>"+
						"<table width=\"400\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\" bordercolor=\"#000000\"><tr><td bgcolor=\"#3366FF\"><p align=\"center\" style=\"color: #FFFFFF\" >Approve</p></td><td bgcolor=\"#0066FF\"><p align=\"center\" style=\"color: #FFFFFF\">Advance</p></td><td bgcolor=\"#3366FF\"><p align=\"center\" style=\"color: #FFFFFF\">Expense</p></td></tr>"+
						"<tr><td><div align=\"center\">"+requestApprove+"</div></td><td><div align=\"center\">"+requestAdvance+"</div></td><td><div align=\"center\">"+requestExpense+"</div></td></tr>"+
						"</table>"+
						"<br>"+
						"<br>Request lists in E-Workflow system are pending for your approval as follows:"+
						"<br>"+
						"<table width=\"400\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\" bordercolor=\"#000000\"><tr><td bgcolor=\"#3366FF\"><p align=\"center\" style=\"color: #FFFFFF\" >Approve</p></td><td bgcolor=\"#0066FF\"><p align=\"center\" style=\"color: #FFFFFF\">Advance</p></td><td bgcolor=\"#3366FF\"><p align=\"center\" style=\"color: #FFFFFF\">Expense</p></td></tr>"+
						"<tr><td><div align=\"center\">"+approverApprove+"</div></td><td><div align=\"center\">"+approverAdvance+"</div></td><td><div align=\"center\">"+approverExpense+"</div></td></tr>"+
						"</table>"+
						"<br>"+
						"<a href=\"http://ewf.mitrphol.com:8001/GWF\">Link to E-WORKFLOW</a>"+
						"<br><br><br>Best Regards."+
						"<br>E-WORKFLOW  of Mitr Phol Group";
				String ccAddress   = "";
	        	
	        	if(!"Paid".equals(person) && !userAddress.equals("null")){
	        		sendMailService.sendMail(from, fromAddress, addressTo, subject, contentText, ccAddress,null,null);
	        	}
	        }
			
			
			hasError = false;
		} catch (Exception e) { /* Log Error */e.printStackTrace();}
		
		if(!hasError){
			Map mapDataRet = new HashMap(); 
			mapDataRet.put("item", "Send Mail successfully");
			
			Map mapResult = new HashMap();
			 Map mapData = new HashMap();
			
			  mapData.put("MESSAGE", "Success");
			  mapData.put("CODE", "200");
			  mapData.put("INFO", "Send Mail Completed");
			  mapData.put("DATA", mapDataRet);
			  mapResult.put("SENDMAIL_MESSAGE", mapData);
			  String returnJson = gson.toJson(mapResult, Map.class);
			  return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(returnJson);
    	}else{
    		Map mapDataRet = new HashMap(); 
			mapDataRet.put("item", "Payment Transaction Fail");
			
			Map mapResult = new HashMap();
			Map mapData = new HashMap();
			
			  mapData.put("MESSAGE", "Fail");
			  mapData.put("CODE", "500");
			  mapData.put("INFO", "Send Mail Fail");
			  mapData.put("DATA", mapDataRet);
			  mapResult.put("SENDMAIL_MESSAGE", mapData);
			  String returnJson = gson.toJson(mapResult, Map.class);
			  return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON_UTF8).body(returnJson);
    	}
	}
	
	@Transactional
	@GetMapping("/notiAdvanceOutstanding")
    ResponseEntity<String> notiAdvanceOutstanding() { 
		
		LOGGER.info("===== notiAdvanceOutstanding=|{}|",new Date());
		boolean hasError = true;
		try {

			Set<String> personSet = new HashSet<String>();
			
			/* Find Data for Approver */
			Map<String,Map> mapDataPersonOfApprove = new HashMap<String,Map>();

			String sqlOfApprove = "SELECT d.requester AS REQUESTER,SUM(da.amount) AS AMOUNT FROM document d LEFT JOIN document_advance da ON da.id = d.document_advance_id WHERE d.document_status = 'CMP' AND    da.external_payment_doc_number IS NOT NULL AND    da.external_clearing_doc_number IS NULL AND    da.due_date <= '"+sdf.format(new Date())+" 00:00:00.0000000' GROUP BY d.requester ";
			/* Fetch Data From Database */
			
	        List<Map<String,Object>> dataListOfApprove = namedParameterJdbcTemplate.queryForList(sqlOfApprove, new MapSqlParameterSource());
	        if(dataListOfApprove !=null ) for(Map<String,Object> modelResult:dataListOfApprove){
	        	String apprver = String.valueOf(modelResult.get("REQUESTER")) ;
	        	Map<String,Object> dataPerson = mapDataPersonOfApprove.get(apprver);
	        	if(dataPerson == null){
	        		dataPerson = new HashMap<String,Object>();
	        	}
	        	String amount = String.valueOf(modelResult.get("AMOUNT"));
	        	dataPerson.put("AMOUNT", amount);
	        	
	        	mapDataPersonOfApprove.put(apprver, dataPerson);
	        	personSet.add(apprver);
	        }
	        
	        
	        //Prepare Mail to
	        for(String person:personSet){
	        	Map userMap         = getMapFromUsername(person);
	        	String userAddress  = getEmailAddressFromUsername(userMap) ;
	        	String userFullName = getNameFromUsername(userMap) ;
	        	
	        	String amount = mapDataPersonOfApprove.get(person)!=null ? (String) mapDataPersonOfApprove.get(person).get("AMOUNT"):"0"; if(amount==null) {amount="0";}
	        	
	        	
				String from = this.EmailFrom;
				String fromAddress= this.EmailFromAddress;
				
				String subject     = "[EWF] การแจ้งเตือนครบกำหนดคืนเงินยืมทดรองจากระบบ E-WORKFLOW กลุ่มมิตรผล ";
				String addressTo   = userAddress;
				String contentText = "เรียน "+userFullName+
						"<br><br><br>ท่านมียอดเงินยืมทดรองครบกำหนด เป็นจำนวนเงิน "+formatter.format(new BigDecimal(amount))+" บาท "+
						"<br>"+
						"<br>กรุณาเคลียร์เงินยืมทดรอง"+
						"<br>"+
						"<a href=\"http://ewf.mitrphol.com:8001/GWF\">Link to E-WORKFLOW</a>"+
						"<br><br><br>ขอแสดงความนับถือ"+
						"<br>E-WORKFLOW  of Mitr Phol Group";
				String ccAddress   = "";
	        	
				if(!"Paid".equals(person) && !userAddress.equals("null")){
	        		sendMailService.sendMail(from, fromAddress, addressTo, subject, contentText, ccAddress,null,null);
	        	}
	        }
			
			
			hasError = false;
		} catch (Exception e) { /* Log Error */e.printStackTrace();}
		
		if(!hasError){
			Map mapDataRet = new HashMap(); 
			mapDataRet.put("item", "Send Mail successfully");
			
			Map mapResult = new HashMap();
			 Map mapData = new HashMap();
			
			  mapData.put("MESSAGE", "Success");
			  mapData.put("CODE", "200");
			  mapData.put("INFO", "Send Mail Completed");
			  mapData.put("DATA", mapDataRet);
			  mapResult.put("SENDMAIL_MESSAGE", mapData);
			  String returnJson = gson.toJson(mapResult, Map.class);
			  return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(returnJson);
    	}else{
    		Map mapDataRet = new HashMap(); 
			mapDataRet.put("item", "Payment Transaction Fail");
			
			Map mapResult = new HashMap();
			Map mapData = new HashMap();
			
			  mapData.put("MESSAGE", "Fail");
			  mapData.put("CODE", "500");
			  mapData.put("INFO", "Send Mail Fail");
			  mapData.put("DATA", mapDataRet);
			  mapResult.put("SENDMAIL_MESSAGE", mapData);
			  String returnJson = gson.toJson(mapResult, Map.class);
			  return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON_UTF8).body(returnJson);
    	}
	}
}
