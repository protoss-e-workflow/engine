package com.spt.engine.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.spt.engine.entity.app.Company;
import com.spt.engine.entity.app.ExpenseType;
import com.spt.engine.entity.app.ExpenseTypeByCompany;
import com.spt.engine.entity.general.Role;
import com.spt.engine.entity.general.User;
import com.spt.engine.repository.app.ExpenseTypeByCompanyRepository;
import com.spt.engine.repository.general.UserRepository;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/expenseTypeCompany")
public class ExpenseTypeCompanyController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(ExpenseTypeCompanyController.class);
	

	
	@Autowired
	EntityManager entityManager;
	
	@Autowired 
    ObjectMapper objectMapper;

	@Autowired
    ExpenseTypeByCompanyRepository expenseTypeByCompanyRepository;

	@Autowired
	UserRepository userRepository;


	@Transactional
	@PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ExpenseTypeByCompany saveExpenseTypeCompany(@RequestBody ExpenseTypeByCompany expenseTypeByCompany,
                                                       @RequestParam("expenseType") ExpenseType expenseType,
                                                       @RequestParam("company") Company company){
	
		LOGGER.info("=============   Save ExpenseType Company =================");



				if(expenseTypeByCompany.getId() != null){

					ExpenseTypeByCompany expenseTypeReferenceUpdate = expenseTypeByCompanyRepository.findOne(expenseTypeByCompany.getId());
					expenseTypeReferenceUpdate.setCompany(company);
					expenseTypeReferenceUpdate.setExpenseType(expenseType);
					expenseTypeReferenceUpdate.setGlCode(expenseTypeByCompany.getGlCode());
					expenseTypeReferenceUpdate.setPa(expenseTypeByCompany.getPa());
					expenseTypeReferenceUpdate.setPsa(expenseTypeByCompany.getPsa());
					entityManager.persist(expenseTypeReferenceUpdate);

					return expenseTypeReferenceUpdate;
				}else{
					expenseTypeByCompany.setCompany(company);
					expenseTypeByCompany.setExpenseType(expenseType);
					entityManager.persist(expenseTypeByCompany);
					return expenseTypeByCompany;
				}



	}

	@GetMapping(value="/findExpenseTypeByCompanyByPaAndPsaAndKeySearchAndHeadOfficeGLAndFactoryGLAndRole",produces = "application/json; charset=utf-8")
	public String findExpenseTypeByCompanyByPaAndPsaAndKeySearchAndHeadOfficeGLAndFactoryGLAndRole(@RequestParam("pa") String pa,
																		@RequestParam("psa") String psa,
																		@RequestParam("keySearch") String keySearch,
																		@RequestParam("headOfficeGL") String headGl,
																		@RequestParam("factoryGL") String facGl,
																		@RequestParam("userName") String userName) {
		User user = userRepository.findByUsername(userName);
		List<String> stringList = new ArrayList<>();
		if(null != user){
			List<Role> roleList = new ArrayList<>(user.getRoles());
			for(Role r : roleList){
				stringList.add(r.getRoleName());
			}
		}
		List<ExpenseTypeByCompany> expenseTypeByCompanyList= expenseTypeByCompanyRepository.findExpenseTypeByCompanyByPaAndPsaAndKeySearchAndHeadOfficeGLAndFactoryGLAndRole(pa,psa,keySearch,headGl,facGl,stringList,new PageRequest(0,20));

		return (new JSONSerializer().exclude("*.class").serialize(expenseTypeByCompanyList));
	}

	@GetMapping(value="/findExpenseTypeByCompanyByPaAndPsaAndFavoriteAndRole",produces = "application/json; charset=utf-8")
	public String findExpenseTypeByCompanyByPaAndPsaAndFavoriteAndRole(@RequestParam("pa") String pa,
																		@RequestParam("psa") String psa,
																		@RequestParam("userName") String userName) {
		User user = userRepository.findByUsername(userName);
		List<String> stringList = new ArrayList<>();
		if(null != user){
			List<Role> roleList = new ArrayList<>(user.getRoles());
			for(Role r : roleList){
				stringList.add(r.getRoleName());
			}
		}
		List<ExpenseTypeByCompany> expenseTypeByCompanyList= expenseTypeByCompanyRepository.findExpenseTypeByCompanyByPaAndPsaAndFavoriteAndRole(pa,psa,stringList,new PageRequest(0,20));

		return (new JSONSerializer().exclude("*.class").serialize(expenseTypeByCompanyList));
	}




	

}
