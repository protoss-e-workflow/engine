package com.spt.engine.controller;


import com.spt.engine.entity.app.Delegate;
import com.spt.engine.entity.app.EmployeeReplacement;
import com.spt.engine.repository.app.DelegateRepository;
import com.spt.engine.repository.app.EmployeeReplacementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/employeeReplacementCustom")
public class EmployeeReplacementController {

    static final Logger LOGGER = LoggerFactory.getLogger(EmployeeReplacementController.class);



    @Autowired
    EmployeeReplacementRepository employeeReplacementRepository;


    @GetMapping
    public Page<EmployeeReplacement> findByEmpCodeReplaceIgnoreCaseContaining(@Param("empCodeReplace") String empCodeReplace,
                                                                              @RequestParam( value="page" ,required=false) Integer page,
                                                                              @RequestParam( value="size" ,required=false) Integer size){
        return employeeReplacementRepository.findByEmpCodeReplaceIgnoreCaseContaining(empCodeReplace, new PageRequest(page, size));
    }


}