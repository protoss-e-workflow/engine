package com.spt.engine.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.general.MasterDataDetail;
import com.spt.engine.entity.general.ReportConfiguration;
import com.spt.engine.entity.general.ReportProjection;
import com.spt.engine.repository.general.MasterDataDetailRepository;
import com.spt.engine.repository.general.ParameterRepository;
import com.spt.engine.repository.general.ReportConfigurationRepository;
import com.spt.engine.util.GenReportJasperPDF;
import com.spt.engine.util.ZipUtil;

import flexjson.JSONSerializer;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;

@Controller
public class ExportMasterDataController {
  
    static final Logger LOGGER = LoggerFactory.getLogger(ExportMasterDataController.class);
    
    @PersistenceContext
    private EntityManager entityManager;
  
    @Autowired
	MasterDataDetailRepository masterDataDetailRepository;
  
  
    @GetMapping("/exportMasterData")
    public ResponseEntity<Resource> exportExcel(HttpServletRequest request) {
    	
    	ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    	Map<String,InputStream> sources = new HashMap<String,InputStream>();
    	
    	for(String key:ConstantVariable.EXPORT_MASTER_DATA.keySet()){
    		List<MasterDataDetail> masterDataDetailList = masterDataDetailRepository.findByMasterdataInOrderByCode(new ArrayList<String>(){{add(key);}});

    		String data = (new JSONSerializer().exclude("*.class").include("id")
    				.exclude("masterdata")
    				.exclude("createdBy")
    				.exclude("createdDate")
    				.exclude("updateBy")
    				.exclude("updateDate")
    				.serialize(masterDataDetailList));
    		LOGGER.info("==============================result {}={}",key,data);
    		
    		try {
    			InputStream is = new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8.name()));
    			sources.put(ConstantVariable.EXPORT_MASTER_DATA.get(key),is);
    		} catch (Exception e1) {
    			// TODO Auto-generated catch block
    			e1.printStackTrace();
    		}
    	}
    	
    	
    	try {
			ZipUtil.packZip(outputStream, sources);
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	
    	/* Set Header to */
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(new MediaType("application", "zip"));
    	headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"data.zip\"");
    	headers.setContentLength(outputStream.size());
    	  	      
    	ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
    	  		  return ResponseEntity
    	    	              .ok()
    	    	              .headers(headers)
    	    	              .body(new InputStreamResource(inputStream));
            
    }
} 
