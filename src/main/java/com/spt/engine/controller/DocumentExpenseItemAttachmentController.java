package com.spt.engine.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;
import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentAttachment;
import com.spt.engine.entity.app.DocumentExpItemAttachment;
import com.spt.engine.entity.app.DocumentExpenseItem;
import com.spt.engine.entity.general.Parameter;
import com.spt.engine.entity.general.ParameterDetail;
import com.spt.engine.repository.app.DocumentExpItemAttachmentRepository;
import com.spt.engine.repository.app.DocumentExpenseItemAttachmentRepository;
import com.spt.engine.repository.general.ParameterRepository;
import com.spt.engine.util.EncryptRealFileNameForMD5;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/documentExpenseItemAttachmentCustom")
public class DocumentExpenseItemAttachmentController {

    static final Logger LOGGER = LoggerFactory.getLogger(DocumentExpenseItemAttachmentController.class);

    @Autowired
    EntityManager entityManager;

    @Autowired
    ParameterRepository parameterRepository;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    DocumentExpenseItemAttachmentRepository documentExpenseItemAttachmentRepository;


    @Autowired
    DocumentExpItemAttachmentRepository documentExpItemAttachmentRepository;




    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };


    protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();

    @Transactional
    @PostMapping(value="/uploadFile")
    public DocumentExpItemAttachment uploadFileAttachment (@RequestParam("file") MultipartFile file,
                                                    @RequestParam("name") String filename,
                                                    @RequestParam("attachmentCode") String attachmentType,
                                                    @RequestParam("documentExpenseItem") DocumentExpenseItem documentExpenseItem){

        LOGGER.info("><><>< UPLOAD FILE DOCUMENT EXPENSE ITEM ATTACHMENT ><><><");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        Parameter parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_PATH_FILE_ATTACHMENT);
        String pathFile = "";
        for(ParameterDetail detail:parameter.getDetails()){
            pathFile = detail.getVariable1();
        }

        LOGGER.info("><><><> path file : {} ",pathFile);

        DocumentExpItemAttachment documentExpItemAttachment = new DocumentExpItemAttachment();
        documentExpItemAttachment.setAttachmentCode(attachmentType);

        documentExpenseItem.setDocExpItemAttachments(new HashSet<DocumentExpItemAttachment>(){{
            add(documentExpItemAttachment);
        }});
        entityManager.persist(documentExpenseItem);

		/* set real file name */
        String realFileName = documentExpenseItem.getId()+ConstantVariable.UNDER_SCORE+
                ConstantVariable.DOCUMENT_STATUS_DRAFT+ConstantVariable.UNDER_SCORE+
                EncryptRealFileNameForMD5.generateEncryptFromStringToMD5(String.valueOf(documentExpItemAttachment.getId()));

        Date date = new Date();
        String uploadTime = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US).format(date);

        File convFile = new File(pathFile+realFileName);

        try {
            documentExpItemAttachment.setDocumentExpenseItem(documentExpenseItem);
            documentExpItemAttachment.setFileName(filename);
            documentExpItemAttachment.setRealFileName(realFileName);
            documentExpItemAttachment.setUploadTime(uploadTime);
            entityManager.persist(documentExpItemAttachment);


            file.transferTo(convFile);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return documentExpItemAttachment;
    }


    @GetMapping(value="/getDocumentExpItemAttachmentByDocumentExpenseItemId/{id}",produces = "application/json; charset=utf-8")
    public String getDocumentExpItemAttachment(@PathVariable Long id) {

        List<DocumentExpItemAttachment> documentExpItemAttachmentList = documentExpenseItemAttachmentRepository.findDocumentExpenseItemAttachmentByDocumentExpenseItemId(id);

        LOGGER.info("documentExpItemAttachmentList = {} ",documentExpItemAttachmentList.size());

        return (new JSONSerializer().exclude("*.class").include("id")
                .include("attachmentCode")
                .include("fileName").serialize(documentExpItemAttachmentList));
    }

    /* for download file by id */
    @GetMapping("/downloadFileAttachment/{id}")
    @ResponseBody
    public ResponseEntity<Resource> downloadFileAttachment(@PathVariable Long id) {
        LOGGER.info("===== Download FileExpItemAttchment id = {}",id);

        String realFileName = "";
        String filename = "";

        DocumentExpItemAttachment documentAttachmentLs = documentExpItemAttachmentRepository.findOne(id);
        LOGGER.info("===== documentAttachmentLs = {}",documentAttachmentLs);
        Parameter parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_PATH_FILE_ATTACHMENT);
        String pathFile = "";
        for(ParameterDetail detail:parameter.getDetails()){
            pathFile = detail.getVariable1();
        }

        if(documentAttachmentLs != null){
            realFileName = documentAttachmentLs.getRealFileName();
            filename     = documentAttachmentLs.getFileName();
        }
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+filename+"\"")
                .body(new FileSystemResource(pathFile+realFileName));
    }
}
