package com.spt.engine.controller;

import java.lang.reflect.Type;
import java.util.Date;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.entity.app.DocumentApproveItem;
import com.spt.engine.entity.app.HotelBooking;

@RestController
@RequestMapping("/hotelBookingCustom")
public class HotelBookingController {

	static final Logger LOGGER = LoggerFactory.getLogger(HotelBookingController.class);
	
	@Autowired
	EntityManager entityManager;

	@Autowired
	ObjectMapper objectMapper;

	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
		public Date deserialize(JsonElement json, Type typeOfT,
								JsonDeserializationContext context) throws JsonParseException {
			return json == null ? null : new Date(json.getAsLong());
		}
	};


	protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();
	
	@Transactional
	@PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public HotelBooking saveHotelBooking(@RequestBody HotelBooking hotelBooking,
										 @RequestParam("documentApproveItem") Long documentApproveItem
									  ){
	
		LOGGER.info("><><>< SAVE AND UPDATE HOTEL BOOKING ><><><");
		LOGGER.info("><><>< Hotel id : {} ",hotelBooking.getId());
		
		try{
						
			if(hotelBooking.getId() != null){
				LOGGER.info("><><>< MERGE HOTEL BOOKING ><><><");
				
				HotelBooking hotelBooking2 = entityManager.find(HotelBooking.class, hotelBooking.getId());
				HotelBooking detail = objectMapper.readerForUpdating(hotelBooking2).readValue(gson.toJson(hotelBooking));
	            
	            entityManager.merge(detail);
	            entityManager.flush();
			}else{
				LOGGER.info("><><>< PERSIST HOTEL BOOKING ><><><");
				
				DocumentApproveItem documentAppItem = entityManager.find(DocumentApproveItem.class, documentApproveItem);
				
				documentAppItem.setHotelBooking(hotelBooking);
				
				entityManager.persist(documentAppItem);
							
				hotelBooking.setDocumentApproveItem(documentAppItem);
				entityManager.persist(hotelBooking);	
			}
			
			
			LOGGER.info("><><>< AFTER PERSIST AND MERGE HOTEL BOOKING ><><>< ");
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return hotelBooking;
	}
}
