package com.spt.engine.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.spt.engine.entity.general.ReportConfiguration;
import com.spt.engine.entity.general.ReportProjection;
import com.spt.engine.repository.general.ParameterRepository;
import com.spt.engine.repository.general.ReportConfigurationRepository;
import com.spt.engine.util.GenReportJasperPDF;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;

@Controller
public class ExportReportController {
  
    static final Logger LOGGER = LoggerFactory.getLogger(ExportReportController.class);
    
    private final String TYPE_EXCEL   = "EXCEL";
    private final String TYPE_PDF     = "PDF";
    
    private final String ALIGN_LEFT   = "LEFT";
    private final String ALIGN_CENTER = "CENTER";
    private final String ALIGN_RIGHT  = "RIGHT";
    
    private final String FONT_TH_NORMAL      = "font/THSarabunNew.ttf";
    private final String FONT_TH_BOLD        = "font/THSarabunNew Bold.ttf";
    private final String FONT_TH_BOLD_ITALIC = "font/THSarabunNew BoldItalic.ttf";
    private final String FONT_TH_ITALIC      = "font/THSarabunNew Italic.ttf";
    
    @PersistenceContext
    private EntityManager entityManager;
  
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
  
    @Autowired
    ParameterRepository parameterRepository;
    
    @Autowired
    ReportConfigurationRepository reportConfigurationRepository;
  
  
    @GetMapping("/export/{reportCode}")
    public ResponseEntity<Resource> exportExcel(HttpServletRequest request,@PathVariable String reportCode) {
    	
    	ReportConfiguration reportConfiguration = reportConfigurationRepository.findByCode(reportCode);
    	
    	String sql = reportConfiguration.getSqlStatement();
    	//Find Parameter
    	Map<String,String> sqlParam = new HashMap<>();
    	Pattern findParametersPattern = Pattern.compile("(?<!')(:[\\w]*)(?!')");
    	Matcher matcher = findParametersPattern.matcher(sql);
    	while (matcher.find()) { 
    		sqlParam.put(String.valueOf((matcher.group().substring(1))), "");
    	}
    	
    	/* Assign Parameter Value */
    	MapSqlParameterSource msps = new MapSqlParameterSource();
    	Map<String,String[]> mapParam = new HashMap<>(request.getParameterMap());
        if(sqlParam != null){
        	for(String key:sqlParam.keySet()){
        		String param = "";
        		try {
					param = mapParam.get(key)[0];
					if(param==null){
						param = "";
					}
				} catch (Exception e) { }
        		
        		msps.addValue(key, param);
        	}
        }          
        
        /* Fetch Data From Database */
        List<Map<String,Object>> dataList = namedParameterJdbcTemplate.queryForList(sql, msps);
        
        if(!dataList.isEmpty()){
        	/* Generate Excel */
            if(TYPE_EXCEL.endsWith(reportConfiguration.getReportType())){
            	  Workbook wb = new XSSFWorkbook(); //or new HSSFWorkbook();
            	  Sheet sheet = wb.createSheet("REPORT");
            	  
            	  
            	  /* Set Header */
    	  	      Font fontHeader = wb.createFont();
    	  	      fontHeader.setFontName("Arial");
    	  	      fontHeader.setBold(true);
    	  	      
    	  	      
            	  CellStyle styleReportName = wb.createCellStyle();
            	  styleReportName.setFillForegroundColor(IndexedColors.WHITE.getIndex());
            	  styleReportName.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            	  styleReportName.setAlignment(HorizontalAlignment.CENTER);
            	  styleReportName.setFont(fontHeader);
            	  
            	  Row rowReportName = sheet.createRow(1);
            	  Cell cellReportName = rowReportName.createCell(4);
	  	          cellReportName.setCellValue(new XSSFRichTextString(reportConfiguration.getName()));
	  	          cellReportName.setCellStyle(styleReportName);
            	  
            	  
    	  	      // Create a row and put some cells in it. Rows are 0 based.
    	  	      Row row = sheet.createRow(4);
    	  	      
    	  	      
    	  	      
    	  	      
    	  	      CellStyle styleHeader = wb.createCellStyle();
    	  	      styleHeader.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
    	  	      styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    	  	      styleHeader.setAlignment(HorizontalAlignment.CENTER);
    	  	      styleHeader.setFont(fontHeader);
    	  	    
    	  	      
    	  	      Cell cell = null;
    	  	      Map<String,Integer> mapKeyIndex = new HashMap(); 
    	  	      Map<String,String>  mapKeyType  = new HashMap(); 
    	  	      int countColumn = 0;
    	  	      for(ReportProjection reportProjection:reportConfiguration.getProjections()){
    	  	      	cell = row.createCell(Integer.parseInt(reportProjection.getColumnIndex()));
    	  	          cell.setCellValue(new XSSFRichTextString(reportProjection.getColumnHeaderName()));
    	  	          cell.setCellStyle(styleHeader);
    	  	          
    	  	          mapKeyIndex.put(reportProjection.getKeyData(), Integer.parseInt(reportProjection.getColumnIndex()));
    	  	          mapKeyType.put(reportProjection.getKeyData(), reportProjection.getCellAlign());
    	  	          
    	  	          countColumn++;
    	  	      }
    	  	      
    	  	      /*Set Data*/
    	  	      int rowIndex = 5;
    	  	      for(Map<String,Object> model:dataList){
    	  	      	Row rowData = sheet.createRow(rowIndex);
    	  	      	
    	  	      	for(String key:mapKeyIndex.keySet()){
    	  	      		cell = rowData.createCell(mapKeyIndex.get(key));
    	  	      		  String content = String.valueOf(model.get(key));
    	  	      		  if("null".equals(content)){
    	  	      			content = null;
    	  	      		  }
    	  	              cell.setCellValue(new XSSFRichTextString( content));
    	  	              
    	  	              CellStyle styleCell = wb.createCellStyle();
    	  	              if(ALIGN_LEFT.equals(mapKeyType.get(key))){
    	  	              	styleCell.setAlignment(HorizontalAlignment.LEFT);
    	  	              	cell.setCellStyle(styleCell);
    	  	              }else if(ALIGN_RIGHT.equals(mapKeyType.get(key))){
    	  	              	styleCell.setAlignment(HorizontalAlignment.RIGHT);
    	  	              	cell.setCellStyle(styleCell);
    	  	              }else if(ALIGN_CENTER.equals(mapKeyType.get(key))){
    	  	              	styleCell.setAlignment(HorizontalAlignment.CENTER);
    	  	              	cell.setCellStyle(styleCell);
    	  	              }
    	  	      	}
    	  	      	rowIndex++;
    	  	      }
    	  	      
    	  	      //autoSizeColumn
    	  	      for(int colNum = 0; colNum<countColumn;colNum++)   
    	  	    	  wb.getSheetAt(0).autoSizeColumn(colNum);
    	  	
    	  	      /* Write to outPutStream */
    	  	      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    	  		  try {
    	  				wb.write(outputStream);
    	  		        wb.close();
    	  		  } catch (Exception e) { LOGGER.error("ERROR ={}", e);  }
      	      
    	  		  /* Set Header to */
    	  		  HttpHeaders headers = new HttpHeaders();
    	  		  headers.setContentType(new MediaType("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
    	  	      headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+reportConfiguration.getFileName()+"\"");
    	  	      headers.setContentLength(outputStream.size());
    	  	      
    	  		  ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
    	  		  return ResponseEntity
    	    	              .ok()
    	    	              .headers(headers)
    	    	              .body(new InputStreamResource(inputStream));
            }else   if(TYPE_PDF.endsWith(reportConfiguration.getReportType()) && reportConfiguration.getTemplateFileName() != null){
            	/* Set Jasper Environment */
            	JasperPrint jasperPrint = new JasperPrint();
        		String jasperFileName   = reportConfiguration.getTemplateFileName();
        		
        		/* Set Report Name*/
        		Map<String, Object> mapParameter = new HashMap<String, Object>();
        		mapParameter.put(reportConfiguration.getKeyOfParamTemplate(), reportConfiguration.getName());
        		
        		/* Set Header Name*/
        		Map<String, String> mapData  = new HashMap<String, String>();
        		for(ReportProjection reportProjection:reportConfiguration.getProjections()){
          	        mapParameter.put(reportProjection.getColumnHeaderKey(), reportProjection.getColumnHeaderName());
          	        mapData.put(reportProjection.getKeyData(),reportProjection.getKeyTemplate());
          	    }
        		
        		/* Set Data */
        		List<Map<String,Object>> dataMappingList = new ArrayList();
        		Map<String, Object> dataMapping  = null;
        		for(Map<String,Object> model:dataList){
        			
        			dataMapping  = new HashMap<String, Object>();
        			for(String key:model.keySet()){
        				String keyOfTemplate = mapData.get(key);
        				if(keyOfTemplate != null){
        					dataMapping.put(keyOfTemplate, model.get(key));
        				}
        			}
        			
        			dataMappingList.add(dataMapping);
        		}
        		
        		/* Pass Parameter Jasper */
        		jasperPrint = GenReportJasperPDF.exportReport(jasperFileName,Arrays.asList(dataMappingList),mapParameter);
        		
        		ByteArrayOutputStream baos = new ByteArrayOutputStream();
                JRPdfExporter exporter = new JRPdfExporter();
                exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
                
                /* Generate Jasper */
                try {
        			exporter.exportReport();
        		} catch (Exception e) { LOGGER.error("ERROR ={}", e);  }
            	
                /* Write to outPutStream */
                ByteArrayInputStream inputStream = new ByteArrayInputStream(baos.toByteArray());
                HttpHeaders headers = new HttpHeaders();
      			headers.setContentType(new MediaType("application", "pdf"));
      	        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+reportConfiguration.getFileName()+"\"");
      	        headers.setContentLength(baos.size());
                return ResponseEntity
      	              .ok()
      	              .headers(headers)
      	              .body(new InputStreamResource(inputStream));
                
            }else if(TYPE_PDF.endsWith(reportConfiguration.getReportType())){
            	ByteArrayOutputStream baos = new ByteArrayOutputStream();
            	try {
    				
    				// step 1 : Define Document
    				Document document = new Document(PageSize.A4.rotate());
    				// step 2 : Initial Writer
    				PdfWriter writer = PdfWriter.getInstance(document, baos);
    				// step 3 : Open Ducument
    				document.open();
    				// step 4 : Define font
    				BaseFont bf = BaseFont.createFont(FONT_TH_NORMAL, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
    				com.itextpdf.text.Font headFont = new com.itextpdf.text.Font(bf, 26);
    				
    				// step 5 : Set Report Name
    				Paragraph reportName = new Paragraph(reportConfiguration.getName(),headFont); 
    				reportName.setAlignment(Element.ALIGN_CENTER);
    				document.add(reportName);
    				document.add(new Paragraph(" "));

    				// step 6 : Create table
    				Integer numOfProjection = reportConfiguration.getNumberOfProjection();
    				PdfPTable table = new PdfPTable(numOfProjection);
    				if(numOfProjection > 3){
    					table.setWidthPercentage(100f);
    				}else{
    					table.setWidthPercentage(20f);
    				}
    				
    				// step 7 : Set Header
    				PdfPCell cell = null;
                    Map<String,Integer> mapKeyIndex = new HashMap(); 
                    Map<String,String> mapKeyType = new HashMap();
    				for(ReportProjection reportProjection:reportConfiguration.getProjections()){
    				  	cell = new PdfPCell();
    					cell.setPhrase(new Paragraph(reportProjection.getColumnHeaderName(),new com.itextpdf.text.Font(bf, 18)));
    				    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    				    cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
    				    table.addCell(cell);
    				    
    				    mapKeyIndex.put(reportProjection.getKeyData(), Integer.parseInt(reportProjection.getColumnIndex()));
    				    mapKeyType.put(reportProjection.getKeyData(), reportProjection.getCellAlign());
    				  }
    				
    				// step 8 : Set Data
    				table.getDefaultCell();
    				for(Map<String,Object> model:dataList){
    					for(String key:mapKeyIndex.keySet()){
    						cell = new PdfPCell();
    						cell.setPhrase(new Paragraph(String.valueOf(model.get(key)),new com.itextpdf.text.Font(bf, 16)));
    			  	      	
    		  	              if(ALIGN_LEFT.equals(mapKeyType.get(key))){
    		  	            	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
    		  	              }else if(ALIGN_RIGHT.equals(mapKeyType.get(key))){
    		  	            	cell.setHorizontalAlignment(Element.ALIGN_RIGHT );
    		  	              }else if(ALIGN_CENTER.equals(mapKeyType.get(key))){
    		  	            	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    		  	              }
    		  	            table.addCell(cell);
    		  	      	}
    		  	      
    				}
    				document.add(table);
    				
    				// step 9: Close Document
    				document.close();
    			} catch (Exception e) { LOGGER.error("ERROR ={}", e);  }
            	
            	/* Write to outPutStream */
            	ByteArrayInputStream inputStream = new ByteArrayInputStream(baos.toByteArray());
                HttpHeaders headers = new HttpHeaders();
      			headers.setContentType(new MediaType("application", "pdf"));
      	        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+reportConfiguration.getFileName()+"\"");
      	        headers.setContentLength(baos.size());
                return ResponseEntity
      	              .ok()
      	              .headers(headers)
      	              .body(new InputStreamResource(inputStream));
            }else{
				Workbook wb = new XSSFWorkbook(); //or new HSSFWorkbook();
				Sheet sheet = wb.createSheet("REPORT");

				// Create a row and put some cells in it. Rows are 0 based.
				Row row = sheet.createRow(0);

    	  	      /* Set Header */
				Font fontHeader = wb.createFont();
				fontHeader.setFontName("Arial");
				fontHeader.setBold(true);

				CellStyle styleHeader = wb.createCellStyle();
				styleHeader.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
				styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
				styleHeader.setAlignment(HorizontalAlignment.CENTER);
				styleHeader.setFont(fontHeader);

				Cell cell = null;
				Map<String,Integer> mapKeyIndex = new HashMap();
				Map<String,String>  mapKeyType  = new HashMap();
				for(ReportProjection reportProjection:reportConfiguration.getProjections()){
					cell = row.createCell(Integer.parseInt(reportProjection.getColumnIndex()));
					cell.setCellValue(new XSSFRichTextString(reportProjection.getColumnHeaderName()));
					cell.setCellStyle(styleHeader);

					mapKeyIndex.put(reportProjection.getKeyData(), Integer.parseInt(reportProjection.getColumnIndex()));
					mapKeyType.put(reportProjection.getKeyData(), reportProjection.getCellAlign());
				}

				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				try {
					wb.write(outputStream);
					wb.close();
				} catch (Exception e) { LOGGER.error("ERROR ={}", e);  }

    	  		  /* Set Header to */
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(new MediaType("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
				headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+reportConfiguration.getFileName()+"\"");
				headers.setContentLength(outputStream.size());

				ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
				return ResponseEntity
						.ok()
						.headers(headers)
						.body(new InputStreamResource(inputStream));
//            	return ResponseEntity.noContent().build();
            }
        }else{

			Workbook wb = new XSSFWorkbook(); //or new HSSFWorkbook();
			Sheet sheet = wb.createSheet("REPORT");
			// Create a row and put some cells in it. Rows are 0 based.
			Row row = sheet.createRow(0);

    	  	      /* Set Header */
			Font fontHeader = wb.createFont();
			fontHeader.setFontName("Arial");
			fontHeader.setBold(true);

			CellStyle styleHeader = wb.createCellStyle();
			styleHeader.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
			styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			styleHeader.setAlignment(HorizontalAlignment.CENTER);
			styleHeader.setFont(fontHeader);

			Cell cell = null;
			Map<String,Integer> mapKeyIndex = new HashMap();
			Map<String,String>  mapKeyType  = new HashMap();
			for(ReportProjection reportProjection:reportConfiguration.getProjections()){
				cell = row.createCell(Integer.parseInt(reportProjection.getColumnIndex()));
				cell.setCellValue(new XSSFRichTextString(reportProjection.getColumnHeaderName()));
				cell.setCellStyle(styleHeader);

				mapKeyIndex.put(reportProjection.getKeyData(), Integer.parseInt(reportProjection.getColumnIndex()));
				mapKeyType.put(reportProjection.getKeyData(), reportProjection.getCellAlign());
			}

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			try {
				wb.write(outputStream);
				wb.close();
			} catch (Exception e) { LOGGER.error("ERROR ={}", e);  }

    	  		  /* Set Header to */
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(new MediaType("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
			headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+reportConfiguration.getFileName()+"\"");
			headers.setContentLength(outputStream.size());

			ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());

			return ResponseEntity
					.ok()
					.headers(headers)
					.body(new InputStreamResource(inputStream));

			//        	return ResponseEntity.noContent().build();
        }
        
        
        
    }
} 
