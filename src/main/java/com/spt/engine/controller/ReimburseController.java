package com.spt.engine.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.spt.engine.entity.app.ExpenseType;
import com.spt.engine.entity.app.Reimburse;
import com.spt.engine.repository.app.ReimburseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;


@RestController
@RequestMapping("/reimburse")
public class ReimburseController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(ReimburseController.class);
	

	
	@Autowired
	EntityManager entityManager;
	
	@Autowired 
    ObjectMapper objectMapper;

	@Autowired
    ReimburseRepository reimburseRepository;


	@Transactional
	@PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public Reimburse saveReimBurse(@RequestBody Reimburse reimburse,
                                   @RequestParam("expenseType") ExpenseType expenseType){
	
		LOGGER.info("=============   Save Reimburse =================");

				if(reimburse.getId() != null){
					Reimburse reimburseUpdate = reimburseRepository.findOne(reimburse.getId());
					reimburseUpdate.setRoleCode(reimburse.getRoleCode());
					reimburseUpdate.setExpenseType(expenseType);
					entityManager.persist(reimburseUpdate);

					return reimburseUpdate;
				}else{
					reimburse.setExpenseType(expenseType);
					entityManager.persist(reimburse);

					return reimburse;
				}



	}
	
	

}
