package com.spt.engine.controller;


import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.persistence.EntityManager;

import com.spt.engine.entity.app.*;
import com.spt.engine.entity.general.*;
import com.spt.engine.repository.app.DelegateRepository;
import com.spt.engine.repository.app.DocumentRepository;
import com.spt.engine.repository.general.*;
import com.spt.engine.util.EncryptRealFileNameForMD5;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.ConstantVariable;
import com.spt.engine.repository.app.custom.DocumentRepositoryCustom;
import com.spt.engine.repository.general.custom.RunningNumberRepositoryCustom;


@RestController
@RequestMapping("/delegateCustom")
public class DelegateController {

    static final Logger LOGGER = LoggerFactory.getLogger(DelegateController.class);


    @Autowired
    DelegateRepository delegateRepository;


    @GetMapping
    public Page<Delegate> findByEmpCodeAssigneeIgnoreCaseContaining(@Param("empCodeAssignee") String empCodeAssignee,
                                                                    @RequestParam( value="page" ,required=false) Integer page,
                                                                    @RequestParam( value="size" ,required=false) Integer size){
        return delegateRepository.findByEmpCodeAssigneeIgnoreCaseContaining(empCodeAssignee, new PageRequest(page, size));
    }


}