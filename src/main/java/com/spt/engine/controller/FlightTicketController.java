package com.spt.engine.controller;

import java.lang.reflect.Type;
import java.util.Date;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.entity.app.DocumentApproveItem;
import com.spt.engine.entity.app.FlightTicket;
import com.spt.engine.entity.app.HotelBooking;

@RestController
@RequestMapping("/flightTicketCustom")
public class FlightTicketController {

	static final Logger LOGGER = LoggerFactory.getLogger(FlightTicketController.class);
	
	@Autowired
	EntityManager entityManager;

	@Autowired
	ObjectMapper objectMapper;

	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
		public Date deserialize(JsonElement json, Type typeOfT,
								JsonDeserializationContext context) throws JsonParseException {
			return json == null ? null : new Date(json.getAsLong());
		}
	};


	protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();
	
	@Transactional
	@PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public FlightTicket saveFlightTicket(@RequestBody FlightTicket flightTicket,
										 @RequestParam("documentApproveItem") Long documentApproveItem
									  ){
	
		LOGGER.info("><><>< SAVE AND UPDATE FLIGHT TICKET ><><><");
		LOGGER.info("><><>< Flight Ticket id : {} ",flightTicket.getId());
		
		try{
						
			if(flightTicket.getId() != null){
				LOGGER.info("><><>< MERGE FLIGHT TICKET ><><><");
				
				FlightTicket flightTicket2 = entityManager.find(FlightTicket.class, flightTicket.getId());
				FlightTicket ticket = objectMapper.readerForUpdating(flightTicket2).readValue(gson.toJson(flightTicket));
	            
	            entityManager.merge(ticket);
	            entityManager.flush();
			}else{
				LOGGER.info("><><>< PERSIST FLIGHT TICKET ><><><");
				
				DocumentApproveItem documentAppItem = entityManager.find(DocumentApproveItem.class, documentApproveItem);
				
				documentAppItem.setFlightTicket(flightTicket);
				
				entityManager.persist(documentAppItem);
							
				flightTicket.setDocumentApproveItem(documentAppItem);
				entityManager.persist(flightTicket);	
			}
			
			
			LOGGER.info("><><>< AFTER PERSIST AND MERGE FLIGHT TICKET ><><>< ");
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return flightTicket;
	}
}
