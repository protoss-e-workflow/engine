package com.spt.engine.controller;

import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.general.Parameter;
import com.spt.engine.entity.general.ParameterDetail;
import com.spt.engine.repository.general.ParameterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/downloadUserManual")
public class DownloadUserManualController {

    static final Logger LOGGER = LoggerFactory.getLogger(DownloadUserManualController.class);

    @Autowired
    ParameterRepository parameterRepository;

    @GetMapping("/user")
    public ResponseEntity<Resource> user() {
        LOGGER.info("DOWNLOAD USER");

        String filename = "";

        Parameter parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_USER_MANUAL);

        String pathFile = "";
        for(ParameterDetail detail:parameter.getDetails()){
            if("UM001".equals(detail.getCode())){
                pathFile = detail.getVariable2();
                filename = detail.getVariable1();
            }
        }

        LOGGER.info("><>< PATH FILE : {} ",pathFile);
        LOGGER.info("><>< FILE NAME : {} ",filename);

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+filename+"\"")
                .body(new FileSystemResource(pathFile+filename));
    }

    @GetMapping("/user2")
    public ResponseEntity<Resource> user2() {
        LOGGER.info("DOWNLOAD USER 2.0");

        String filename = "";

        Parameter parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_USER_MANUAL);

        String pathFile = "";
        for(ParameterDetail detail:parameter.getDetails()){
            if("UM003".equals(detail.getCode())){
                pathFile = detail.getVariable2();
                filename = detail.getVariable1();
            }
        }

        LOGGER.info("><>< PATH FILE : {} ",pathFile);
        LOGGER.info("><>< FILE NAME : {} ",filename);

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+filename+"\"")
                .body(new FileSystemResource(pathFile+filename));
    }

    @GetMapping("/approver")
    public ResponseEntity<Resource> approver() {
        LOGGER.info("DOWNLOAD APPROVE");
        String filename = "";

        Parameter parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_USER_MANUAL);

        String pathFile = "";
        for(ParameterDetail detail:parameter.getDetails()){
            if("UM002".equals(detail.getCode())){
                pathFile = detail.getVariable2();
                filename = detail.getVariable1();
            }
        }

        LOGGER.info("><>< PATH FILE : {} ",pathFile);
        LOGGER.info("><>< FILE NAME : {} ",filename);

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+filename+"\"")
                .body(new FileSystemResource(pathFile+filename));
    }



    @GetMapping("/account")
    public ResponseEntity<Resource> account() {
        LOGGER.info("DOWNLOAD ACCOUNT");
        String filename = "";

        Parameter parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_USER_MANUAL);

        String pathFile = "";
        for(ParameterDetail detail:parameter.getDetails()){
            if("UM003".equals(detail.getCode())){
                pathFile = detail.getVariable2();
                filename = detail.getVariable1();
            }
        }

        LOGGER.info("><>< PATH FILE : {} ",pathFile);
        LOGGER.info("><>< FILE NAME : {} ",filename);

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+filename+"\"")
                .body(new FileSystemResource(pathFile+filename));
    }
}
