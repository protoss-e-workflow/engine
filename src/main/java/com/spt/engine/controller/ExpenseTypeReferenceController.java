package com.spt.engine.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.spt.engine.entity.app.ExpenseType;
import com.spt.engine.entity.app.ExpenseTypeReference;
import com.spt.engine.repository.app.ExpenseTypeReferenceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;


@RestController
@RequestMapping("/expenseTypeReference")
public class ExpenseTypeReferenceController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(ExpenseTypeReferenceController.class);
	

	
	@Autowired
	EntityManager entityManager;
	
	@Autowired 
    ObjectMapper objectMapper;

	@Autowired
    ExpenseTypeReferenceRepository expenseTypeReferenceRepository;


	@Transactional
	@PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ExpenseTypeReference saveExpenseTypeRef(@RequestBody ExpenseTypeReference expenseTypeReference,
                                                   @RequestParam("expenseType") ExpenseType expenseType){
	
		LOGGER.info("=============   Save ExpenseType Reference=================");

				if(expenseTypeReference.getId() != null){

					ExpenseTypeReference expenseTypeReferenceUpdate = expenseTypeReferenceRepository.findOne(expenseTypeReference.getId());
					expenseTypeReferenceUpdate.setApproveTypeCode(expenseTypeReference.getApproveTypeCode());
					expenseTypeReferenceUpdate.setCompleteReason(expenseTypeReference.getCompleteReason());
					expenseTypeReferenceUpdate.setExpenseType(expenseType);
					entityManager.persist(expenseTypeReferenceUpdate);

					return expenseTypeReferenceUpdate;

				}else{

					expenseTypeReference.setExpenseType(expenseType);
					entityManager.persist(expenseTypeReference);

					return expenseTypeReference;

				}


	}
	
	

}
