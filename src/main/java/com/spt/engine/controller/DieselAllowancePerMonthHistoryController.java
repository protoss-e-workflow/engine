package com.spt.engine.controller;

import com.spt.engine.entity.app.DieselAllowancePerMonth;
import com.spt.engine.entity.app.DieselAllowancePerMonthHistory;
import com.spt.engine.entity.app.DocumentExpenseItem;
import com.spt.engine.repository.app.DieselAllowancePerMonthHistoryRepository;
import com.spt.engine.repository.app.DieselAllowancePerMonthRepository;
import com.spt.engine.repository.app.DocumentExpenseItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;

@RestController
@RequestMapping("/dieselAllowancePerMonthHistoryCustom")
public class DieselAllowancePerMonthHistoryController {

    static final Logger LOGGER = LoggerFactory.getLogger(DieselAllowancePerMonthHistoryController.class);

    @Autowired
    DieselAllowancePerMonthRepository dieselAllowancePerMonthRepository;

    @Autowired
    DieselAllowancePerMonthHistoryRepository dieselAllowancePerMonthHistoryRepository;

    @Autowired
    DocumentExpenseItemRepository documentExpenseItemRepository;

    @Autowired
    EntityManager entityManager;

    @Transactional
    @PostMapping(value="/saveDieselAllowancePerMonthHistoryForExpense",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String saveDieselAllowancePerMonthHistoryForExpense(@RequestBody DieselAllowancePerMonthHistory dieselAllowancePerMonthHistory){

        LOGGER.info("><><>< Save Petrol Allowance Per Month History For Expense ><><><");
        String result = "";
        LOGGER.info("===========   DieselAllowancePerMonthHistory   :    {}",dieselAllowancePerMonthHistory);
        try{
            DieselAllowancePerMonth dieselAllowancePerMonth = dieselAllowancePerMonthRepository.findByEmpUserName(dieselAllowancePerMonthHistory.getEmpUserName());
            LOGGER.info("============  dieselAllowancePerMonth   :   {}",dieselAllowancePerMonth);
            if(null != dieselAllowancePerMonth){
                Double dieselBalance = dieselAllowancePerMonth.getBalanceLiterPerMonth();
                Double dieselAmount = dieselAllowancePerMonthHistory.getLiterNumber();
                Double total = dieselBalance - dieselAmount;

                dieselAllowancePerMonth.setBalanceLiterPerMonth(total);
                entityManager.merge(dieselAllowancePerMonth);
                entityManager.flush();
            }

            entityManager.persist(dieselAllowancePerMonthHistory);
            result = "Success";

        }catch (Exception e){
            result = "Error";
            e.printStackTrace();
        }

        return result;
    }

    @Transactional
    @PostMapping(value="/deleteDieselAllowancePerMonthHistoryForExpense",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String deleteDieselAllowancePerMonthHistoryForExpense(@RequestParam("expenseItem") Long documentExpenseItemId){

        LOGGER.info("><><>< Delete Diesel Allowance Per Month History For Expense ><><><");
        String result = "";
        try{
            DocumentExpenseItem documentExpenseItem = documentExpenseItemRepository.findOne(documentExpenseItemId);
            if(null != documentExpenseItem){
                Double oldLiter = Double.valueOf(documentExpenseItem.getDocExpItemDetail().getLiter());
                Double balanceLiter = 0d;

                String requester = documentExpenseItem.getDocumentExp().getDocument().getRequester();
                DieselAllowancePerMonth dieselAllowancePerMonth = dieselAllowancePerMonthRepository.findByEmpUserName(requester);
                if(null != dieselAllowancePerMonth){
                    balanceLiter = dieselAllowancePerMonth.getBalanceLiterPerMonth();
                    dieselAllowancePerMonth.setBalanceLiterPerMonth(oldLiter+balanceLiter);
                    entityManager.merge(dieselAllowancePerMonth);
                    entityManager.flush();
                }

            }
            result = "Success";

        }catch (Exception e){
            result = "Error";
            e.printStackTrace();
        }

        return result;
    }

    @Transactional
    @PostMapping(value="/updateDieselAllowancePerMonthHistoryForExpense",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String updateDieselAllowancePerMonthHistoryForExpense(@RequestBody DieselAllowancePerMonthHistory dieselAllowancePerMonthHistory,
                                                                 @RequestParam("expenseItem") Long documentExpenseItemId){

        LOGGER.info("><><>< Update Diesel Allowance Per Month History For Expense ><><><");
        String result = "";
        try{
            DocumentExpenseItem documentExpenseItem = documentExpenseItemRepository.findOne(documentExpenseItemId);
            if(null != documentExpenseItem){
                Double newLiter = dieselAllowancePerMonthHistory.getLiterNumber();
                Double oldLiter = 0d;
                Double balanceLiter = 0d;

                String requester = documentExpenseItem.getDocumentExp().getDocument().getRequester();
                DieselAllowancePerMonth dieselAllowancePerMonth = dieselAllowancePerMonthRepository.findByEmpUserName(requester);

                if(null != dieselAllowancePerMonth){
                    oldLiter = dieselAllowancePerMonth.getLiterPerMonth();
                    balanceLiter = oldLiter-newLiter;
                    dieselAllowancePerMonth.setBalanceLiterPerMonth(balanceLiter);
                    entityManager.merge(dieselAllowancePerMonth);
                    entityManager.flush();
                }

            }
            result = "Success";

        }catch (Exception e){
            result = "Error";
            e.printStackTrace();
        }

        return result;
    }
}
