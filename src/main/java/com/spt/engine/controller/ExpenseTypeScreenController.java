package com.spt.engine.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.spt.engine.entity.app.ExpenseType;
import com.spt.engine.entity.app.ExpenseTypeScreen;
import com.spt.engine.repository.app.ExpenseTypeScreenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;


@RestController
@RequestMapping("/expenseTypeScreen")
public class ExpenseTypeScreenController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(ExpenseTypeScreenController.class);
	

	
	@Autowired
	EntityManager entityManager;
	
	@Autowired 
    ObjectMapper objectMapper;

	@Autowired
    ExpenseTypeScreenRepository expenseTypeScreenRepository;


	@Transactional
	@PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ExpenseTypeScreen saveExpenseTypeScreen(@RequestBody ExpenseTypeScreen expenseTypeScreen,
                                                   @RequestParam("expenseType") ExpenseType expenseType){
	
		LOGGER.info("=============   Save ExpenseType Screen =================");

				if(expenseTypeScreen.getId() != null){

					ExpenseTypeScreen expenseTypeScreenUpdate = expenseTypeScreenRepository.findOne(expenseTypeScreen.getId());
					expenseTypeScreenUpdate.setNameENG(expenseTypeScreen.getNameENG());
					expenseTypeScreenUpdate.setNameTH(expenseTypeScreen.getNameTH());
					expenseTypeScreenUpdate.setStructureField(expenseTypeScreen.getStructureField());
					expenseTypeScreenUpdate.setType(expenseTypeScreen.getType());
					expenseTypeScreenUpdate.setRequire(expenseTypeScreen.getRequire()	);
					expenseTypeScreenUpdate.setExpenseType(expenseType);
					entityManager.persist(expenseTypeScreenUpdate);

					return expenseTypeScreenUpdate;

				}else{

					expenseTypeScreen.setExpenseType(expenseType);
					entityManager.persist(expenseTypeScreen);

					return expenseTypeScreen;

				}


	}
	
	

}
