package com.spt.engine.controller;

import java.lang.reflect.Type;
import java.util.*;

import javax.persistence.EntityManager;

import com.spt.engine.ConstantVariable;
import com.spt.engine.repository.app.DocumentReferenceRepository;
import com.spt.engine.repository.app.DocumentRepository;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentReference;
import com.spt.engine.repository.app.LocationRepository;

@RestController
@RequestMapping("/documentReferenceCustom")
public class DocumentReferenceController {

	static final Logger LOGGER = LoggerFactory.getLogger(DocumentReference.class);
	
	@Autowired
	EntityManager entityManager;

	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	LocationRepository locationRepository;

	@Autowired
	DocumentRepository documentRepository;

	@Autowired
	DocumentReferenceRepository documentReferenceRepository;


	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
		public Date deserialize(JsonElement json, Type typeOfT,
								JsonDeserializationContext context) throws JsonParseException {
			return json == null ? null : new Date(json.getAsLong());
		}
	};


	protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();
	
	@Transactional
	@PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public DocumentReference saveDocumentReference (@RequestBody DocumentReference documentReference,
								 					@RequestParam("document") Document document
													){
	
		LOGGER.info("><><>< SAVE DOCUMENT REFERENCE ><><><");
		
		try{

			document.setDocumentReferences(new HashSet<DocumentReference>(){{
	            add(documentReference);
	        }});

			entityManager.persist(document);

			documentReference.setDocument(document);
			entityManager.persist(documentReference);

			LOGGER.info("><><>< AFTER PERSIST DOCUMENT REFERENCE ><><>< ");
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return documentReference;
	}

	@Transactional
	@PostMapping(value="/updateDocumentReference",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public DocumentReference updateDocumentReference (@RequestBody DocumentReference documentReference,
													@RequestParam("document") Document document
	){

		LOGGER.info("><><>< UPDATE DOCUMENT REFERENCE ><><><");

		try{
			Document documentOld = documentRepository.findOne(document.getId());
			/** remove old document reference **/
			if(0 != documentOld.getDocumentReferences().size()){
				for(DocumentReference df:documentOld.getDocumentReferences()){
					entityManager.remove(df);
				}
			}

			documentOld.setDocumentReferences(new HashSet<DocumentReference>(){{
				add(documentReference);
			}});

			entityManager.persist(documentOld);

			documentReference.setDocument(documentOld);
			entityManager.persist(documentReference);

			LOGGER.info("><><>< AFTER PERSIST DOCUMENT REFERENCE ><><>< ");
		}catch (Exception e) {
			e.printStackTrace();
		}

		return documentReference;
	}

	@Transactional
	@PostMapping(value="/updateDocumentReferenceList",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public List<DocumentReference> updateDocumentReferenceList (@RequestParam("documentReference") String json,
													  			@RequestParam("document") Document document
	){

		LOGGER.info("><><>< UPDATE DOCUMENT REFERENCE LIST ><><><");
		LOGGER.info("><><>< JSON ><><><   :   {}",json);
		List<String> documentReferenceList = Arrays.asList(json.split("\\s*,\\s*"));
		LOGGER.info("><><>< LIST ><><><   :   {}",documentReferenceList);
		List<DocumentReference> documentReferences = new ArrayList<>();
		try{
			Document documentOld = documentRepository.findOne(document.getId());
			/** remove old document reference **/
			if(0 != documentOld.getDocumentReferences().size()){
				for(DocumentReference df:documentOld.getDocumentReferences()){
					entityManager.remove(df);
				}
			}

			if(0 != documentReferenceList.size()){
				for(String docNumber:documentReferenceList){
					List<String> stringList = Arrays.asList(docNumber.split(":"));

					DocumentReference df = new DocumentReference();
					df.setDocument(documentOld);
					df.setDocReferenceNumber(stringList.get(0));
					df.setDocumentTypeCode(stringList.get(1));

					entityManager.persist(df);

					documentReferences.add(df);

					documentOld.setDocumentReferences(new HashSet<DocumentReference>(documentReferences));
				}
			}

			entityManager.persist(documentOld);

			LOGGER.info("><><>< AFTER PERSIST DOCUMENT REFERENCE LIST><><>< ");
		}catch (Exception e) {
			e.printStackTrace();
		}

		return documentReferences;
	}

	@GetMapping(value="/findDocRefByDocTypeExp",produces = "application/json; charset=utf-8")
	public String findDocRefByDocTypeExp(@RequestParam("documentNumber") String documentNumber) {

		LOGGER.info("><><>< findDocRefByDocTypeExp ><><><");

		List<DocumentReference> documentReferenceLs  = documentReferenceRepository.findByDocReferenceNumberAndAndDocumentTypeCode(documentNumber,ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_EXPENSE);

		return (new JSONSerializer().exclude("*.class").include("docNumber").exclude("*").serialize(documentReferenceLs));
	}
}
