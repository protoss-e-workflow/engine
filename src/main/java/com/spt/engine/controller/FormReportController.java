package com.spt.engine.controller;


import com.spt.engine.service.ReportService;

import com.spt.engine.util.AbstractReportJasperPDF;
import net.sf.jasperreports.engine.JasperPrint;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
public class FormReportController {

    static final Logger LOGGER = LoggerFactory.getLogger(FormReportController.class);

    @Autowired
    ReportService reportService;


    @PostMapping("/report/printApproveReport")
    public ResponseEntity<String> printApproveReport(@RequestBody String json,
                                                     HttpServletResponse response)throws ServletException {

        LOGGER.info("printApproveReport[Controller]");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ByteArrayInputStream inputStream = null;
        OutputStream out = null;
        try{

            JSONObject object = new JSONObject(json);
            String docNum = String.valueOf(object.get("docNum").toString());
            String mapMessage = object.get("mapMessage").toString();
            String language = object.get("language").toString();

            String value = mapMessage;
            value = value.substring(1, value.length()-1);
            String[] keyValuePairs = value.split(",");
            Map<String,String> map = new HashMap<>();
            for(String pair : keyValuePairs) {
                String[] entry = pair.split("=");
                map.put(entry[0].trim(), entry[1].trim());
            }

            List<JasperPrint> jasperPrintList   = new ArrayList<>();
            JasperPrint jasperPrint             = null;

            if(docNum != null){
                jasperPrint = reportService.formReportApprove(docNum,map,language);
                jasperPrintList.add(jasperPrint);
                byte[] b = AbstractReportJasperPDF.generateReport(jasperPrintList);
                inputStream = new ByteArrayInputStream(b);
                out = response.getOutputStream();
                IOUtils.copy(inputStream, out);
            }
            return new ResponseEntity<String>(headers, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<String>("{\"ERROR\":" + e.getMessage() + "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }finally{
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(out);
        }
    }




    @PostMapping("/report/printAdvanceReport")
    public ResponseEntity<String> printAdvanceReport(@RequestBody String json,
                                                     HttpServletResponse response)throws ServletException {

        LOGGER.info("printApproveReport[Controller]");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ByteArrayInputStream inputStream = null;
        OutputStream out = null;
        try{

            JSONObject object = new JSONObject(json);
            String docNum = String.valueOf(object.get("docNum").toString());
            String mapMessage = object.get("mapMessage").toString();
            String language = object.get("language").toString();

            String value = mapMessage;
            value = value.substring(1, value.length()-1);
            String[] keyValuePairs = value.split(",");
            Map<String,String> map = new HashMap<>();
            for(String pair : keyValuePairs) {
                String[] entry = pair.split("=");
                map.put(entry[0].trim(), entry[1].trim());
            }

            List<JasperPrint> jasperPrintList   = new ArrayList<>();
            JasperPrint jasperPrint             = null;

            if(docNum != null){
                jasperPrint = reportService.formReportAdvance(docNum,map,language);
                jasperPrintList.add(jasperPrint);
                byte[] b = AbstractReportJasperPDF.generateReport(jasperPrintList);
                inputStream = new ByteArrayInputStream(b);
                out = response.getOutputStream();
                IOUtils.copy(inputStream, out);
            }
            return new ResponseEntity<String>(headers, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<String>("{\"ERROR\":" + e.getMessage() + "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }finally{
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(out);
        }
    }





    @PostMapping("/report/printExpenseReport")
    public ResponseEntity<String> printExpenseReport(@RequestBody String json,
                                                     HttpServletResponse response)throws ServletException {

        LOGGER.info("printApproveReport[Controller]");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ByteArrayInputStream inputStream = null;
        OutputStream out = null;
        try{

            JSONObject object = new JSONObject(json);
            String docNum = String.valueOf(object.get("docNum").toString());
            String mapMessage = object.get("mapMessage").toString();
            String language = object.get("language").toString();

            String value = mapMessage;
            value = value.substring(1, value.length()-1);
            String[] keyValuePairs = value.split(",");
            Map<String,String> map = new HashMap<>();
            for(String pair : keyValuePairs) {
                String[] entry = pair.split("=");
                map.put(entry[0].trim(), entry[1].trim());
            }

            List<JasperPrint> jasperPrintList   = new ArrayList<>();
            JasperPrint jasperPrint             = null;

            if(docNum != null){
                jasperPrint = reportService.formReportExpense(docNum,map,language);
                jasperPrintList.add(jasperPrint);
                byte[] b = AbstractReportJasperPDF.generateReport(jasperPrintList);
                inputStream = new ByteArrayInputStream(b);
                out = response.getOutputStream();
                IOUtils.copy(inputStream, out);
            }
            return new ResponseEntity<String>(headers, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<String>("{\"ERROR\":" + e.getMessage() + "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }finally{
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(out);
        }
    }




}
