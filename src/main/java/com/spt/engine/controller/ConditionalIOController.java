package com.spt.engine.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;
import com.spt.engine.entity.app.Company;
import com.spt.engine.entity.app.ConditionalIO;
import com.spt.engine.repository.app.CompanyRepository;
import com.spt.engine.repository.app.ConditionalIORepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import java.lang.reflect.Type;
import java.util.Date;


@RestController
@RequestMapping("/conditionalIOCustom")
public class ConditionalIOController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(ConditionalIOController.class);
	

	@Autowired
	EntityManager entityManager;
	
	@Autowired 
    ObjectMapper objectMapper;
	
	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

	
	protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();
	
	@Transactional
	@PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ConditionalIO saveConditionalIO(@RequestBody ConditionalIO conditionalIO,
								 @RequestParam("company") Company company){

		conditionalIO.setCompany(company);

		if(conditionalIO.getId()==null){
			LOGGER.info("><><>< SAVE ConditionalIO ><><><");
			entityManager.persist(conditionalIO);
		} else {
			updateConditionalIO(conditionalIO);
		}

		
		
		
		LOGGER.info("><><>< AFTER PERSIST ConditionalIO ><><>< ");
		return conditionalIO;
	}
	
	
	@Transactional
	@PostMapping(value="/updateConditionalIO",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ConditionalIO updateConditionalIO(@RequestBody ConditionalIO conditionalIO){
	
		LOGGER.info("><><>< UPDATE ConditionalIO ><><><");
		
		try {
			ConditionalIO oldConditionalIO = entityManager.find(ConditionalIO.class, conditionalIO.getId());
			ConditionalIO newConditionalIO = objectMapper.readerForUpdating(oldConditionalIO).readValue(gson.toJson(conditionalIO));

	        entityManager.merge(newConditionalIO);
	        entityManager.flush();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conditionalIO;
	}
	

}
