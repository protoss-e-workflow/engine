package com.spt.engine.controller;

import com.spt.engine.entity.app.DocumentExpenseItem;
import com.spt.engine.repository.app.DocumentExpenseItemRepository;
import com.spt.engine.repository.app.PerDiemProcessCalculateRepository;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/perDiemProcessCalculateCustom")
public class PerDiemProcessCalculateController {

    static final Logger LOGGER = LoggerFactory.getLogger(PerDiemProcessCalculateController.class);

    @Autowired
    PerDiemProcessCalculateRepository perDiemProcessCalculateRepository;

    @Autowired
    DocumentExpenseItemRepository documentExpenseItemRepository;

    @Transactional
    @PostMapping(value="/processCalculatePerDiemForeign",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity<String> processCalculatePerDiemForeign(@RequestBody Map<String,Object> data,
                                                                 @RequestParam("empLevel") Long empLevel,
                                                                 @RequestParam("sellingRate") BigDecimal sellingRate,
                                                                 @RequestParam("documentExpense") Long documentExpenseId){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        LOGGER.info("><><>< processCalculatePerDiemForeign ><><><");
        List<Map<String,Object>> list = new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            List<DocumentExpenseItem> documentExpenseItemList = documentExpenseItemRepository.findByDocumentExpForPerDiemForeign(documentExpenseId);
            if(documentExpenseItemList.size()>0){
                for(DocumentExpenseItem dei:documentExpenseItemList){
                    Map<String,Object> map = new HashMap<>();
                    map.put("startDate",simpleDateFormat.format(dei.getDocExpItemDetail().getDate1()));
                    map.put("endDate",simpleDateFormat.format(dei.getDocExpItemDetail().getDate2()));
                    map.put("startTime",dei.getDocExpItemDetail().getTime1());
                    map.put("endTime",dei.getDocExpItemDetail().getTime2());
                    map.put("rateType",dei.getDocExpItemDetail().getVariable1());
                    map.put("location",dei.getDocExpItemDetail().getPlace1());
                    list.add(map);
                }
                list.add(data);
            }else{
                list.add(data);
            }
            Map<String,Object> result = perDiemProcessCalculateRepository.processCalculatePerDiemForeign(empLevel,sellingRate,list);
            return new ResponseEntity<String>(new JSONSerializer()
                    .exclude("*.class")
                    .serialize(result),headers, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<String>("{\"ERROR\":" +e.getMessage()+ "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Transactional
    @PostMapping(value="/processCalculatePerDiemDomestic",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity<String> processCalculatePerDiemDomestic(@RequestBody Map<String,Object> data,
                                                                  @RequestParam("empLevel") Long empLevel,
                                                                  @RequestParam("documentExpense") Long documentExpenseId){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        LOGGER.info("><><>< processCalculatePerDiemDomestic ><><><");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        List<Map<String,Object>> list = new ArrayList<>();
        try {
            List<DocumentExpenseItem> documentExpenseItemList = documentExpenseItemRepository.findByDocumentExpForPerDiemDomestic(documentExpenseId);
            if(documentExpenseItemList.size()>0){
                for(DocumentExpenseItem dei:documentExpenseItemList){
                    Map<String,Object> map = new HashMap<>();
                    map.put("startDate",simpleDateFormat.format(dei.getDocExpItemDetail().getDate1()));
                    map.put("endDate",simpleDateFormat.format(dei.getDocExpItemDetail().getDate2()));
                    map.put("startTime",dei.getDocExpItemDetail().getTime1());
                    map.put("endTime",dei.getDocExpItemDetail().getTime2());
                    map.put("rateType",dei.getDocExpItemDetail().getVariable1());
                    map.put("location",dei.getDocExpItemDetail().getPlace1());
                    list.add(map);
                }
                list.add(data);
            }else{
                list.add(data);
            }
            Map<String,Object> result = perDiemProcessCalculateRepository.processCalculatePerDiemDomestic(empLevel,list);
            return new ResponseEntity<String>(new JSONSerializer()
                    .exclude("*.class")
                    .serialize(result),headers, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<String>("{\"ERROR\":" +e.getMessage()+ "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
