package com.spt.engine.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;
import com.spt.engine.entity.app.*;
import com.spt.engine.repository.app.DocumentExpenseGroupRepository;
import com.spt.engine.repository.app.DocumentExpenseItemRepository;
import com.spt.engine.repository.app.DocumentRepository;
import com.spt.engine.repository.app.ExpenseTypeByCompanyRepository;
import com.spt.engine.repository.app.custom.DocumentExpenseItemRepositoryCustom;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import java.lang.reflect.Type;
import java.util.*;

@RestController
@RequestMapping("/documentExpenseItemCustom")
public class DocumentExpenseItemController {

    static final Logger LOGGER = LoggerFactory.getLogger(DocumentExpenseItemController.class);

    @Autowired
    EntityManager entityManager;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    ExpenseTypeByCompanyRepository expenseTypeByCompanyRepository;

    @Autowired
    DocumentExpenseGroupRepository documentExpenseGroupRepository;

    @Autowired
    DocumentExpenseItemRepository documentExpenseItemRepository;

    @Autowired
    DocumentRepository documentRepository;

    @Autowired
    DocumentExpenseItemRepositoryCustom documentExpenseItemRepositoryCustom;

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };


    protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();

    @Transactional
    @PostMapping(value="/saveDocumentExpenseItem",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity<String> saveDocumentExpenseItem(@RequestParam("expenseItem") String documentExpenseItem,
                                                          @RequestParam("document") Document document){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        LOGGER.info("><><>< SAVE DOCUMENT EXPENSE ITEM ><><><");
        List<String> list = null;

        try {
            if (!documentExpenseItem.isEmpty()) {
                list = Arrays.asList(documentExpenseItem.split(","));
            }

            if (null != list) {
                if(list.size() == 1){
                    ExpenseTypeByCompany expenseTypeByCompany = expenseTypeByCompanyRepository.findOne(Long.valueOf(list.get(0)));
                    DocumentExpenseGroup documentExpenseGroup = new DocumentExpenseGroup();
                    documentExpenseGroup.setExpTypeByCompanys(expenseTypeByCompany);
                    documentExpenseGroup.setDocumentExp(document.getDocumentExpense());
                    entityManager.persist(documentExpenseGroup);
                    entityManager.flush();

                    List<DocumentExpenseGroup> documentExpenseGroupList = documentExpenseGroupRepository.findByDocumentExp(document.getDocumentExpense().getId());

                    if(documentExpenseGroupList.size() == 1){
                        document.setTitleDescription(documentExpenseGroupList.get(0).getExpTypeByCompanys().getExpenseTypes().getDescription());
                        entityManager.merge(document);
                        entityManager.flush();
                    }else if(documentExpenseGroupList.size() > 1){
                        document.setTitleDescription(documentExpenseGroupList.get(0).getExpTypeByCompanys().getExpenseTypes().getDescription()+"....");
                        entityManager.merge(document);
                        entityManager.flush();
                    }

                }else{
                    for (int i = 0; i < list.size(); i++) {
                        try {
                            ExpenseTypeByCompany expenseTypeByCompany = expenseTypeByCompanyRepository.findOne(Long.valueOf(list.get(i)));
                            DocumentExpenseGroup documentExpenseGroup = new DocumentExpenseGroup();
                            documentExpenseGroup.setExpTypeByCompanys(expenseTypeByCompany);
                            documentExpenseGroup.setDocumentExp(document.getDocumentExpense());
                            entityManager.persist(documentExpenseGroup);

                        } catch (Exception e) {
                            LOGGER.error("{} is not a number", list.get(i));
                        }
                    }

                    List<DocumentExpenseGroup> documentExpenseGroupList = documentExpenseGroupRepository.findByDocumentExp(document.getDocumentExpense().getId());

                    if(documentExpenseGroupList.size() == 1){
                        document.setTitleDescription(documentExpenseGroupList.get(0).getExpTypeByCompanys().getExpenseTypes().getDescription());
                        entityManager.merge(document);
                        entityManager.flush();
                    }else if(documentExpenseGroupList.size() > 1){
                        document.setTitleDescription(documentExpenseGroupList.get(0).getExpTypeByCompanys().getExpenseTypes().getDescription()+"....");
                        entityManager.merge(document);
                        entityManager.flush();
                    }
                }

            }
            LOGGER.info("><><>< AFTER PERSIST DOCUMENT EXPENSE ITEM ><><>< ");
            return new ResponseEntity<String>(headers, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<String>("{\"ERROR\":" +e.getMessage()+ "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Transactional
    @PostMapping(value="/saveDocumentExpenseItemDetail",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity<String> saveDocumentExpenseItemDetail(@RequestBody DocumentExpenseItem documentExpenseItem,
                                                                @RequestParam("expTypeByCompany") Long expenseTypeByCompany,
                                                                @RequestParam("document") Document document){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        LOGGER.info("><><>< SAVE DOCUMENT EXPENSE ITEM DETAIL ><><><");
        try {
            ExpenseTypeByCompany expenseTypeByCompany1 = expenseTypeByCompanyRepository.findOne(expenseTypeByCompany);
            documentExpenseItem.setExpTypeByCompany(expenseTypeByCompany1);
            documentExpenseItem.setDocumentExp(document.getDocumentExpense());
            entityManager.persist(documentExpenseItem.getDocExpItemDetail());
            entityManager.persist(documentExpenseItem);
            entityManager.flush();

            document.setDocumentExpense(document.getDocumentExpense());
            entityManager.merge(document);
            entityManager.flush();

            /**  Set total amount in Document  **/
            DocumentExpense documentExpenseOld = document.getDocumentExpense();
            List<DocumentExpenseItem> documentExpenseItemList = new ArrayList<DocumentExpenseItem>(documentExpenseOld.getDocumentExpenseItems());
            if(documentExpenseItemList.size()>0){
                Double totalAmount = 0d;
                for (DocumentExpenseItem d:documentExpenseItemList){
                    Double netAmount = d.getNetAmount()==null?0:d.getNetAmount();
                    Double whtAmount = d.getWhtAmount()==null?0:d.getWhtAmount();
                    totalAmount += (netAmount+whtAmount);

                }
                document.setTotalAmount(totalAmount);
                entityManager.persist(document);
                entityManager.flush();
            }

            LOGGER.info("><><>< AFTER PERSIST DOCUMENT EXPENSE ITEM DETAIL ><><>< ");
            return new ResponseEntity<String>(new JSONSerializer()
                    .exclude("*.class")
                    .include("id")
                    .exclude("*")
                    .serialize(documentExpenseItem),headers, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<String>("{\"ERROR\":" +e.getMessage()+ "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Transactional
    @PostMapping(value="/updateDocumentExpenseItemDetail",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity<String> updateDocumentExpenseItemDetail(@RequestBody DocumentExpenseItem documentExpenseItem,@RequestParam("document") Document document){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        LOGGER.info("><><>< UPDATE DOCUMENT EXPENSE ITEM DETAIL ><><><");
        try {
            DocumentExpenseItem oldDocument = entityManager.find(DocumentExpenseItem.class, documentExpenseItem.getId());
            oldDocument.setInternalOrder(documentExpenseItem.getInternalOrder());
            oldDocument.setRemark(documentExpenseItem.getRemark());
            oldDocument.setDocNumber(documentExpenseItem.getDocNumber());
            oldDocument.setSeller(documentExpenseItem.getSeller());
            oldDocument.setTaxIDNumber(documentExpenseItem.getTaxIDNumber());
            oldDocument.setBranch(documentExpenseItem.getBranch());
            oldDocument.setAddress(documentExpenseItem.getAddress());
            oldDocument.setAmount(documentExpenseItem.getAmount());
            oldDocument.setBaseAmount(documentExpenseItem.getBaseAmount());
            oldDocument.setVat(documentExpenseItem.getVat());
            oldDocument.setNetAmount(documentExpenseItem.getNetAmount());
            oldDocument.setWhtAmount(documentExpenseItem.getWhtAmount());
            oldDocument.setDocumentDate(documentExpenseItem.getDocumentDate());
            oldDocument.setVatCode(documentExpenseItem.getVatCode());
            oldDocument.setWhtType(documentExpenseItem.getWhtType());
            oldDocument.setVatCodeValue(documentExpenseItem.getVatCodeValue());
            oldDocument.setWhtCode(documentExpenseItem.getWhtCode());
            oldDocument.setGlCode(documentExpenseItem.getGlCode());
            oldDocument.setExchangeRate(documentExpenseItem.getExchangeRate());
            oldDocument.setBusinessPlace(documentExpenseItem.getBusinessPlace());
            oldDocument.setProfitCenter(documentExpenseItem.getProfitCenter());
            oldDocument.setWbs(documentExpenseItem.getWbs());
            oldDocument.setAssignment(documentExpenseItem.getAssignment());
            oldDocument.setQuantity(documentExpenseItem.getQuantity());
            oldDocument.setUnit(documentExpenseItem.getUnit());
            oldDocument.setStreet(documentExpenseItem.getStreet());
            oldDocument.setCity(documentExpenseItem.getCity());
            oldDocument.setPostcode(documentExpenseItem.getPostcode());

            oldDocument.getDocExpItemDetail().setEmployee(documentExpenseItem.getDocExpItemDetail().getEmployee());
            oldDocument.getDocExpItemDetail().setReceiver(documentExpenseItem.getDocExpItemDetail().getReceiver());
            oldDocument.getDocExpItemDetail().setReceiverCompany(documentExpenseItem.getDocExpItemDetail().getReceiverCompany());
            oldDocument.getDocExpItemDetail().setDescription(documentExpenseItem.getDocExpItemDetail().getDescription());
            oldDocument.getDocExpItemDetail().setHotelCode(documentExpenseItem.getDocExpItemDetail().getHotelCode());
            oldDocument.getDocExpItemDetail().setPlace1(documentExpenseItem.getDocExpItemDetail().getPlace1());
            oldDocument.getDocExpItemDetail().setPlace2(documentExpenseItem.getDocExpItemDetail().getPlace2());
            oldDocument.getDocExpItemDetail().setSubjectName(documentExpenseItem.getDocExpItemDetail().getSubjectName());
            oldDocument.getDocExpItemDetail().setCountry(documentExpenseItem.getDocExpItemDetail().getCountry());
            oldDocument.getDocExpItemDetail().setTime1(documentExpenseItem.getDocExpItemDetail().getTime1());
            oldDocument.getDocExpItemDetail().setTime2(documentExpenseItem.getDocExpItemDetail().getTime2());
            oldDocument.getDocExpItemDetail().setCarLicence(documentExpenseItem.getDocExpItemDetail().getCarLicence());
            oldDocument.getDocExpItemDetail().setMonth(documentExpenseItem.getDocExpItemDetail().getMonth());
            oldDocument.getDocExpItemDetail().setDate1(documentExpenseItem.getDocExpItemDetail().getDate1());
            oldDocument.getDocExpItemDetail().setDate2(documentExpenseItem.getDocExpItemDetail().getDate2());
            oldDocument.getDocExpItemDetail().setPlaceOther1(documentExpenseItem.getDocExpItemDetail().getPlaceOther1());
            oldDocument.getDocExpItemDetail().setPlaceOther2(documentExpenseItem.getDocExpItemDetail().getPlaceOther2());
            oldDocument.getDocExpItemDetail().setCarLicenceOther(documentExpenseItem.getDocExpItemDetail().getCarLicenceOther());

            oldDocument.getDocExpItemDetail().setVariable1(documentExpenseItem.getDocExpItemDetail().getVariable1());
            oldDocument.getDocExpItemDetail().setVariable2(documentExpenseItem.getDocExpItemDetail().getVariable2());
            oldDocument.getDocExpItemDetail().setVariable3(documentExpenseItem.getDocExpItemDetail().getVariable3());
            oldDocument.getDocExpItemDetail().setVariable4(documentExpenseItem.getDocExpItemDetail().getVariable4());
            oldDocument.getDocExpItemDetail().setVariable5(documentExpenseItem.getDocExpItemDetail().getVariable5());
            oldDocument.getDocExpItemDetail().setVariable6(documentExpenseItem.getDocExpItemDetail().getVariable6());
            oldDocument.getDocExpItemDetail().setVariable7(documentExpenseItem.getDocExpItemDetail().getVariable7());
            oldDocument.getDocExpItemDetail().setVariable8(documentExpenseItem.getDocExpItemDetail().getVariable8());
            oldDocument.getDocExpItemDetail().setVariable9(documentExpenseItem.getDocExpItemDetail().getVariable9());
            oldDocument.getDocExpItemDetail().setVariable10(documentExpenseItem.getDocExpItemDetail().getVariable10());
            oldDocument.getDocExpItemDetail().setVariable11(documentExpenseItem.getDocExpItemDetail().getVariable11());
            oldDocument.getDocExpItemDetail().setVariable12(documentExpenseItem.getDocExpItemDetail().getVariable12());
            oldDocument.getDocExpItemDetail().setVariable13(documentExpenseItem.getDocExpItemDetail().getVariable13());
            oldDocument.getDocExpItemDetail().setVariable14(documentExpenseItem.getDocExpItemDetail().getVariable14());
            oldDocument.getDocExpItemDetail().setVariable15(documentExpenseItem.getDocExpItemDetail().getVariable15());
            oldDocument.getDocExpItemDetail().setVariable16(documentExpenseItem.getDocExpItemDetail().getVariable16());
            oldDocument.getDocExpItemDetail().setVariable17(documentExpenseItem.getDocExpItemDetail().getVariable17());
            oldDocument.getDocExpItemDetail().setVariable18(documentExpenseItem.getDocExpItemDetail().getVariable18());
            oldDocument.getDocExpItemDetail().setVariable19(documentExpenseItem.getDocExpItemDetail().getVariable19());
            oldDocument.getDocExpItemDetail().setVariable20(documentExpenseItem.getDocExpItemDetail().getVariable20());

            entityManager.merge(oldDocument);
            entityManager.flush();

            /**  Set total amount in Document  **/
            DocumentExpense documentExpenseOld = document.getDocumentExpense();
            List<DocumentExpenseItem> documentExpenseItemList = new ArrayList<>(documentExpenseOld.getDocumentExpenseItems());
            if(documentExpenseItemList.size()>0){
                Double totalAmount = 0d;
                for (DocumentExpenseItem d:documentExpenseItemList){
                    if(null!=d.getAmount()){
                        Double netAmount = d.getNetAmount()==null?0:d.getNetAmount();
                        Double whtAmount = d.getWhtAmount()==null?0:d.getWhtAmount();
                        totalAmount += (netAmount+whtAmount);
                    }
                }
                document.setTotalAmount(totalAmount);
                entityManager.merge(document);
                entityManager.flush();
            }

            LOGGER.info("><><>< AFTER UPDATE DOCUMENT EXPENSE ITEM DETAIL ><><>< ");
            return new ResponseEntity<String>(headers, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<String>("{\"ERROR\":" +e.getMessage()+ "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Transactional
    @DeleteMapping(value="/delete/{id}",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String deleteExpenseGroup(@PathVariable Long id){

        LOGGER.info("><><>< DELETE DOCUMENT EXPENSE GROUP ><><><");
        String resultString = "";
        try {
            DocumentExpenseItem documentExpenseItem =  documentExpenseItemRepository.findOne(id);
            Long idDocumentExpense = documentExpenseItem.getDocumentExp().getId();
            Long idDocument = documentExpenseItem.getDocumentExp().getDocument().getId();

            entityManager.remove(documentExpenseItem);
            entityManager.flush();

            Document document = documentRepository.findOne(idDocument);

            /** set new total amount **/
            List<DocumentExpenseItem> documentExpenseItemList1 = documentExpenseItemRepository.findByDocumentExp(idDocumentExpense);
            if(documentExpenseItemList1.size() > 0){
                Double totalAmount = 0d;
                for (DocumentExpenseItem d:documentExpenseItemList1){
                    Double netAmount = d.getNetAmount()==null?0:d.getNetAmount();
                    Double whtAmount = d.getWhtAmount()==null?0:d.getWhtAmount();
                    totalAmount += (netAmount+whtAmount);

                }
                document.setTotalAmount(totalAmount);
                entityManager.merge(document);
                entityManager.flush();
            }else{
                document.setTotalAmount(0d);
                entityManager.merge(document);
                entityManager.flush();
            }

            resultString = "Success";
        } catch (Exception e) {
            e.printStackTrace();
            resultString = "Error";
        }
        return resultString;
    }

    @GetMapping(value="/checkBudgetInWorkflow",produces = "application/json; charset=utf-8")
    public ResponseEntity<String> checkBudgetInWorkflow() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        try{
            List<Map<String,Object>> map = documentExpenseItemRepositoryCustom.checkBudgetInWorkflow("1017920010","811710","DRAFTEXP1010190509D0002","");
            return new ResponseEntity<String>((new JSONSerializer().serialize(map)), headers, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<String>((new JSONSerializer().serialize(e.getMessage())), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
