package com.spt.engine.controller;


import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.print.Doc;
import javax.servlet.http.HttpServletRequest;

import com.spt.engine.entity.app.*;
import com.spt.engine.entity.general.*;
import com.spt.engine.repository.app.*;
import com.spt.engine.repository.app.custom.DocumentApproveItemRepositoryCustom;
import com.spt.engine.repository.general.*;
import com.spt.engine.service.DocumentService;
import com.spt.engine.service.EmplyeeProfileService;
import com.spt.engine.util.EncryptRealFileNameForMD5;
import flexjson.JSONSerializer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.ConstantVariable;




@RestController
@RequestMapping("/documentCustom")
public class DocumentController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(DocumentController.class);
	
	@Autowired
	RunningTypeRepository runningTypeRepository;
	
	@Autowired
	RunningNumberRepository runningNumberRepository;
	
	@Autowired
	MasterDataRepository masterDataRepository;
	
	@Autowired
	EntityManager entityManager;
	
	@Autowired 
    ObjectMapper objectMapper;

	@Autowired
	ParameterRepository parameterRepository;

	@Autowired
	DocumentRepository documentRepository;

	@Autowired
	MasterDataDetailRepository masterDataDetailRepository;

	@Autowired
	TravelMemberRepository travelMemberRepository;

	@Autowired
	ExternalMemberRepository externalMemberRepository;

	@Autowired
	TravelDetailRepository travelDetailRepository;

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	RequestApproverRepository requestApproverRepository;

    @Autowired
    EmplyeeProfileService employeeProfileService;

	@Autowired
	CarBookingRepository carBookingRepository;

	@Autowired
	DocumentApproveItemRepository documentApproveItemRepository;

	@Autowired
	ExpenseTypeByCompanyRepository expenseTypeByCompanyRepository;

	@Autowired
	DocumentExpenseItemRepository documentExpenseItemRepository;

	@Autowired
	DocumentExpenseGroupRepository documentExpenseGroupRepository;

	@Autowired
	RequestRepository requestRepository;

	@Autowired
	InterfaceActiveLogRepository interfaceActiveLogRepository;

	@Autowired
	DocumentService documentService;

	
	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

	
	protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();
	
	
	
	@Transactional
	@PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public Document saveDocument(@RequestBody Document document){
	
		LOGGER.info("><><>< SAVE DOCUMENT ><><><");
		RunningType runningType = runningTypeRepository.findByCode(document.getDocumentType());
		String prefix = "";
		String documentNumber = "";


		if(runningType != null ){
			prefix = runningNumberRepository.generateRunningNumber(ConstantVariable.DOCUMENT_STATUS_DRAFT_PREFIX, document.getDocumentType(), document.getCompanyCode(), ConstantVariable.DOCUMENT_STATUS_DRAFT);
			documentNumber = runningNumberRepository.generateRunningNumber(runningType, prefix);
		}

		document.setDocNumber(documentNumber);
		document.setTmpDocNumber(documentNumber);
		document.setRequesterName(getNameFromUsername(getMapFromUsername(document.getRequester())));
		document.setRequesterNameEn(getNameFromUsernameEn(getMapFromUsername(document.getRequester())));

		entityManager.persist(document);
		entityManager.flush();

		/**  Set bank number  **/
		DocumentAdvance documentAdvance = document.getDocumentAdvance();
		if(null != documentAdvance && !"".equals(documentAdvance.getBankNumber())){
			Employee employee = employeeRepository.findByEmpUsername(document.getRequester());
			if(null != employee){
				employee.setBankNumber(documentAdvance.getBankNumber());
				entityManager.merge(employee);
				entityManager.flush();
			}

		}

		if(null != documentAdvance){
			documentAdvance.setDocument(document);
			entityManager.merge(documentAdvance);
		}

		DocumentApprove documentApprove = document.getDocumentApprove();
		if(null != documentApprove){
			documentApprove.setDocument(document);
			entityManager.merge(documentApprove);
		}

		DocumentExpense documentExpense = document.getDocumentExpense();
		if(null != documentExpense){
			documentExpense.setDocument(document);
			entityManager.merge(documentExpense);
		}
		entityManager.flush();

		LOGGER.info("><><>< AFTER PERSIST DOCUMENT ><><>< ");
		return document;
	}
	
	
	@Transactional
	@PostMapping(value="/updateDocumentStatus",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public Document updateDocumentStatus(@RequestBody Document document){
	
		LOGGER.info("><><>< UPDATE DOCUMENT STATUS ><><><");
		
		try {
			Document oldDocument = entityManager.find(Document.class, document.getId());
	        Document newDocument = objectMapper.readerForUpdating(oldDocument).readValue(gson.toJson(document));
	
	        entityManager.merge(newDocument);
	        entityManager.flush();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return document;
	}
	
	
	@Transactional
	@PostMapping(value="/updateDocumentApprove",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public Document updateDocument(@RequestBody Document document){
	
		LOGGER.info("><><>< UPDATE DOCUMENT APPROVE ><><><");
		
		try {
			Document oldDocument = entityManager.find(Document.class, document.getId());
	        Document newDocument = objectMapper.readerForUpdating(oldDocument).readValue(gson.toJson(document));

			if(newDocument.getDocumentAdvance()!= null && !"".equals(newDocument.getDocumentAdvance().getBankNumber())){
				Employee employee = employeeRepository.findByEmpUsername(newDocument.getRequester());
				if(null != employee){
					employee.setBankNumber(newDocument.getDocumentAdvance().getBankNumber());
					entityManager.merge(employee);
					entityManager.flush();
				}else{
					employee = new Employee();
					employee.setBankNumber(newDocument.getDocumentAdvance().getBankNumber());
					employee.setEmpUsername(newDocument.getRequester());
					entityManager.persist(employee);
				}
			}

			entityManager.merge(newDocument);
			entityManager.flush();

			/** set document to document advance **/
			if(newDocument.getDocumentAdvance()!= null){
				DocumentAdvance documentAdvance = newDocument.getDocumentAdvance();
				documentAdvance.setDocument(newDocument);
				entityManager.merge(documentAdvance);
				entityManager.flush();
			}


			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return document;
	}

	/* copy document */
	@Transactional
	@PostMapping(value="/copyDocument",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public String copyDocument(@RequestBody Document document) {

		LOGGER.info("><><>< COPY DOCUMENT ><><><");

		Document newDocument = new Document();

		try {
			Document oldDocument = entityManager.find(Document.class, document.getId());
			MasterData documentStatusMs = masterDataRepository.findByCode(ConstantVariable.MASTER_DATA_DOCUMENT_STATUS);
			List<MasterDataDetail> masterDataDetailLs = new ArrayList<>(documentStatusMs.getDetails());
			String documentStatus = "";
			for (MasterDataDetail dataDetail : masterDataDetailLs) {
				if (dataDetail.getCode().equals(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_DRAFT)) {
					documentStatus = dataDetail.getCode();
				}
			}


			String[] ignoreProperties = {"id", "request","createdDate","updateDate","updateBy","sendDate"};
			BeanUtils.copyProperties(oldDocument, newDocument, ignoreProperties);

	        /* generate document number */
			RunningType runningType = runningTypeRepository.findByCode(oldDocument.getDocumentType());
			String prefix = "";
			String documentNumber = "";


			if (runningType != null) {
				prefix = runningNumberRepository.generateRunningNumber(ConstantVariable.DOCUMENT_STATUS_DRAFT_PREFIX, oldDocument.getDocumentType(), oldDocument.getCompanyCode(), ConstantVariable.DOCUMENT_STATUS_DRAFT);
				documentNumber = runningNumberRepository.generateRunningNumber(runningType, prefix);
			}

			/* set document status are DRAFT */
			newDocument.setDocNumber(documentNumber);
			newDocument.setDocumentStatus(documentStatus);
			newDocument.setVersion(0l);
			newDocument.setCreatedDate(new Timestamp(new Date().getTime()));

			/* set sub document */
			DocumentAttachment documentAttachment = null;
			Set<DocumentAttachment> setDocumentAttachment = new HashSet<>();
			for (DocumentAttachment attachment : oldDocument.getDocumentAttachment()) {
				documentAttachment = new DocumentAttachment();
				BeanUtils.copyProperties(attachment, documentAttachment, new String[]{});
				documentAttachment.setDocument(newDocument);
				documentAttachment.setId(null);
				setDocumentAttachment.add(documentAttachment);
			}

			newDocument.setDocumentAttachment(setDocumentAttachment);

			DocumentReference documentReference = null;
			Set<DocumentReference> setDocumentReference = new HashSet<>();
			for (DocumentReference reference : oldDocument.getDocumentReferences()) {
				documentReference = new DocumentReference();
				BeanUtils.copyProperties(reference, documentReference, new String[]{});
				documentReference.setDocument(newDocument);
				documentReference.setId(null);
				setDocumentReference.add(documentReference);
			}

			/** Set new document approve **/
			if(null != oldDocument.getDocumentApprove()){
				DocumentApprove newDocumentApprove = new DocumentApprove();
				BeanUtils.copyProperties(oldDocument.getDocumentApprove(), newDocumentApprove, new String[]{});
				newDocumentApprove.setDocument(newDocument);
				newDocumentApprove.setId(null);

				Set<DocumentApproveItem> newDocumentApproveItemSet = new HashSet<>();

				if(null != oldDocument.getDocumentApprove().getDocumentApproveItems()){
					for(DocumentApproveItem approveItem : oldDocument.getDocumentApprove().getDocumentApproveItems()){
						DocumentApproveItem newDocumentApproveItem = new DocumentApproveItem();
						BeanUtils.copyProperties(approveItem, newDocumentApproveItem, new String[]{});
						newDocumentApproveItem.setDocumentApprove(newDocumentApprove);
						newDocumentApproveItem.setId(null);

						Set<TravelDetail> newTravelDetailSet = new HashSet<>();
						if(null != approveItem.getTravelDetail()){
							for(TravelDetail travelDetail : approveItem.getTravelDetail()){
								TravelDetail newTravelDetail = new TravelDetail();
								BeanUtils.copyProperties(travelDetail, newTravelDetail, new String[]{});
								newTravelDetail.setDocumentApproveItem(newDocumentApproveItem);
								newTravelDetail.setId(null);
								newTravelDetailSet.add(newTravelDetail);
							}
							newDocumentApproveItem.setTravelDetails(newTravelDetailSet);
						}

						Set<TravelMember> newTravelMemberSet = new HashSet<>();
						if(null != approveItem.getTravelMember()){
							for(TravelMember travelMember : approveItem.getTravelMember()){
								TravelMember newTravelMember = new TravelMember();
								BeanUtils.copyProperties(travelMember, newTravelMember, new String[]{});
								newTravelMember.setDocumentApproveItem(newDocumentApproveItem);
								newTravelMember.setId(null);
								newTravelMemberSet.add(newTravelMember);
							}
							newDocumentApproveItem.setTravelMembers(newTravelMemberSet);
						}

						Set<ExternalMember> newExternalMemberSet = new HashSet<>();
						if(null != approveItem.getExternalMember()){
							for(ExternalMember externalMember : approveItem.getExternalMember()){
								ExternalMember newExternalMember = new ExternalMember();
								BeanUtils.copyProperties(externalMember, newExternalMember, new String[]{});
								newExternalMember.setDocumentApproveItem(newDocumentApproveItem);
								newExternalMember.setId(null);
								newExternalMemberSet.add(newExternalMember);
							}
							newDocumentApproveItem.setExternalMembers(newExternalMemberSet);
						}

						if(null != approveItem.getCarBookings()){
							CarBooking newCarBooking = new CarBooking();
							BeanUtils.copyProperties(approveItem.getCarBookings(), newCarBooking, new String[]{});
							newCarBooking.setDocumentApproveItem(newDocumentApproveItem);
							newCarBooking.setId(null);
							newDocumentApproveItem.setCarBooking(newCarBooking);
						}

						if(null != approveItem.getHotelBooking()){
							HotelBooking newHotelBooking = new HotelBooking();
							BeanUtils.copyProperties(approveItem.getHotelBooking(), newHotelBooking, new String[]{});
							newHotelBooking.setDocumentApproveItem(newDocumentApproveItem);
							newHotelBooking.setId(null);
							newDocumentApproveItem.setHotelBooking(newHotelBooking);
						}

						if(null != approveItem.getFlightTicket()){
							FlightTicket newFlightTicket = new FlightTicket();
							BeanUtils.copyProperties(approveItem.getFlightTicket(), newFlightTicket, new String[]{});
							newFlightTicket.setDocumentApproveItem(newDocumentApproveItem);
							newFlightTicket.setId(null);
							newDocumentApproveItem.setFlightTicket(newFlightTicket);
						}


						newDocumentApproveItemSet.add(newDocumentApproveItem);

					}

					newDocumentApprove.setDocumentApproveItems(newDocumentApproveItemSet);
				}

				newDocument.setDocumentApprove(newDocumentApprove);

			}

			/** Set new document advance **/
			if(null != oldDocument.getDocumentAdvance()){
				DocumentAdvance newDocumentAdvance = new DocumentAdvance();
				BeanUtils.copyProperties(oldDocument.getDocumentAdvance(), newDocumentAdvance, new String[]{});
				newDocumentAdvance.setDocument(newDocument);
				newDocumentAdvance.setId(null);
				newDocument.setDocumentAdvance(newDocumentAdvance);
			}

			/** Set new document expense **/
			if(null != oldDocument.getDocumentExpense()){
				DocumentExpense newDocumentExpense = new DocumentExpense();
				BeanUtils.copyProperties(oldDocument.getDocumentExpense(),newDocumentExpense,new String[]{});
				newDocumentExpense.setDocument(newDocument);
				newDocumentExpense.setId(null);

				/** expense group **/
				if(oldDocument.getDocumentExpense().getDocumentExpenseGroups().size() > 0){
					Set<DocumentExpenseGroup> newDocumentExpenseGroupSet = new HashSet<>();
					for(DocumentExpenseGroup dxg :oldDocument.getDocumentExpense().getDocumentExpenseGroups()){
						DocumentExpenseGroup newDocumentExpenseGroup = new DocumentExpenseGroup();
						BeanUtils.copyProperties(dxg,newDocumentExpenseGroup,new String[]{});
						newDocumentExpenseGroup.setDocumentExp(newDocumentExpense);
						newDocumentExpenseGroup.setId(null);
						newDocumentExpenseGroupSet.add(newDocumentExpenseGroup);
					}
					newDocumentExpense.setDocumentExpenseGroups(newDocumentExpenseGroupSet);
				}

				/** expense item **/
				if(oldDocument.getDocumentExpense().getDocumentExpenseItems().size() > 0){
//					DocumentExpenseItemDetail newDocumentExpenseItemDetail = new DocumentExpenseItemDetail();
					Set<DocumentExpenseItem> newDocumentExpenseItemSet = new HashSet<>();
					for(DocumentExpenseItem dxi:oldDocument.getDocumentExpense().getDocumentExpenseItems()){
						DocumentExpenseItem newDocumentExpenseItem = new DocumentExpenseItem();
						BeanUtils.copyProperties(dxi,newDocumentExpenseItem,new String[]{});
						newDocumentExpenseItem.setId(null);
						newDocumentExpenseItem.setDocumentExp(newDocumentExpense);
						newDocumentExpenseItemSet.add(newDocumentExpenseItem);

						if(dxi.getDocExpItemAttachments().size() > 0){
							Set<DocumentExpItemAttachment> newDocumentExpItemAttachmentSet = new HashSet<>();
							for(DocumentExpItemAttachment d : dxi.getDocExpItemAttachments()) {
								DocumentExpItemAttachment newDocumentExpItemAttachment = new DocumentExpItemAttachment();
								BeanUtils.copyProperties(d,newDocumentExpItemAttachment,new String[]{});
								newDocumentExpItemAttachment.setId(null);
								newDocumentExpItemAttachment.setDocumentExpenseItem(newDocumentExpenseItem);
								newDocumentExpItemAttachmentSet.add(newDocumentExpItemAttachment);
							}
							newDocumentExpenseItem.setDocExpItemAttachments(newDocumentExpItemAttachmentSet);
						}

//						if(null != dxi.getDocExpItemDetail()){
//							BeanUtils.copyProperties(dxi.getDocExpItemDetail(),newDocumentExpenseItemDetail,new String[]{});
//							newDocumentExpenseItemDetail.setId(null);
//							newDocumentExpenseItemDetail.setDocExpenseItem(newDocumentExpenseItem);
//						}
//						newDocumentExpenseItem.setDocExpItemDetail(newDocumentExpenseItemDetail);

					}
					newDocumentExpense.setDocumentExpenseItems(newDocumentExpenseItemSet);
				}

				newDocumentExpense.setDocExpItemHistories(null);
				newDocumentExpense.setDocumentExpAttachments(null);
				newDocument.setDocumentExpense(newDocumentExpense);
			}

			newDocument.setRequest(null);

			newDocument.setDocumentReferences(setDocumentReference);

			entityManager.persist(newDocument);

			/* setNewRealFileAttachment */
			setNewFileAttachment(newDocument, oldDocument);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return (new JSONSerializer()
				.exclude("*.class")
				.include("id")																	.include("docNumber")
				.include("documentType")														.include("documentStatus")
				.include("documentApprove.id")													.include("documentApprove.remark")
				.include("documentApprove.documentApproveItem.id")								.include("documentApprove.documentApproveItem.approveType")
				.include("documentApprove.documentApproveItem.travelReason")					.include("documentApprove.documentApproveItem.carBooking")
				.include("documentApprove.documentApproveItem.hotelBooking")					.include("documentApprove.documentApproveItem.flightTicket")
				.include("documentApprove.documentApproveItem.externalMembers")					.include("documentApprove.documentApproveItem.travelMembers")
				.include("documentApprove.documentApproveItem.travelDetails")					.include("documentApprove.documentApproveItem.documentReferences")
				.include("documentApprove.documentApproveItem.documentAttachment")				.include("documentAdvance.id")
				.include("documentAdvance.advancePurpose")										.include("documentAdvance.requiredReference")
				.include("documentAdvance.amount")												.include("documentAdvance.startDate")
				.include("documentAdvance.bankNumber")											.include("documentAdvance.remark")
				.include("documentAdvance.endDate")												.include("documentAdvance.dueDate")
				.include("documentAdvance.documentAdvanceItems.id")
				.include("documentExpense.id")													.include("documentExpense.documentExpenseItems.id")
				.include("documentExpense.documentExpenseItems.internalOrder")					.include("documentExpense.documentExpenseItems.remark")
				.include("documentExpense.documentExpenseItems.expTypeByCompany.id")			.include("documentExpense.documentExpenseItems.expTypeByCompany.glCode")
				.include("documentExpense.documentExpenseItems.expTypeByCompany.favorite")		.include("documentExpense.documentExpenseItems.expTypeByCompany.pa")
				.include("documentExpense.documentExpenseItems.expTypeByCompany.psa")			.include("documentExpense.documentExpenseItems.expTypeByCompany.company.id")
				.include("documentExpense.documentExpenseItems.expTypeByCompany.company.code")	.include("documentExpense.documentExpenseItems.expTypeByCompany.company.description")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.id")			.include("documentExpense.documentExpenseItems.docExpItemDetail.employee")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.receiver")		.include("documentExpense.documentExpenseItems.docExpItemDetail.receiverCompany")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.description")	.include("documentExpense.documentExpenseItems.docExpItemDetail.hotelCode")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.place1")		.include("documentExpense.documentExpenseItems.docExpItemDetail.place2")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.subjectName")	.include("documentExpense.documentExpenseItems.docExpItemDetail.country")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.time1")			.include("documentExpense.documentExpenseItems.docExpItemDetail.time2")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.carLicence")	.include("documentExpense.documentExpenseItems.docExpItemDetail.month")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.phoneNumber")	.include("documentExpense.documentExpenseItems.docExpItemDetail.date1")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.date2")			.include("documentExpense.documentExpenseItems.docExpItemDetail.variable1")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable2")		.include("documentExpense.documentExpenseItems.docExpItemDetail.variable3")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable4")		.include("documentExpense.documentExpenseItems.docExpItemDetail.variable5")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable6")		.include("documentExpense.documentExpenseItems.docExpItemDetail.variable7")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable8")		.include("documentExpense.documentExpenseItems.docExpItemDetail.variable9")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable10")	.include("documentExpense.documentExpenseItems.docExpItemDetail.variable11")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable12")	.include("documentExpense.documentExpenseItems.docExpItemDetail.variable13")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable14")	.include("documentExpense.documentExpenseItems.docExpItemDetail.variable15")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable16")	.include("documentExpense.documentExpenseItems.docExpItemDetail.variable17")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable18")	.include("documentExpense.documentExpenseItems.docExpItemDetail.variable19")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable20")
				.exclude("*")
				.serialize(newDocument));
	}


	public void setNewFileAttachment(Document document,Document oldDocument){

		String realFileName = "";
		Date date = new Date();
		String uploadTime = "";
		String fileName = "";
		String oldRealFileName = "";
		File oldFile = null;
		File newFile = null;

		Parameter parameter = parameterRepository.findByCode("P103");
		String pathFile = "";
		for(ParameterDetail detail:parameter.getDetails()){
			pathFile = detail.getVariable1();
		}
		try {
			List<DocumentAttachment> documentAttachmentOldLs = new ArrayList<>(oldDocument.getDocumentAttachment());
			List<DocumentAttachment> documentAttachmentNewLs = new ArrayList<>(oldDocument.getDocumentAttachment());
			if(documentAttachmentOldLs.size() > 0 && documentAttachmentNewLs.size() > 0){
				for(DocumentAttachment oldAttachment : documentAttachmentOldLs){
					oldRealFileName = oldAttachment.getRealFileName();
					oldFile = new File(pathFile + oldRealFileName);

					for(DocumentAttachment newAttachment : documentAttachmentNewLs){

						/* set real file name */
						realFileName = document.getDocNumber()+ConstantVariable.UNDER_SCORE+
								ConstantVariable.DOCUMENT_STATUS_DRAFT+ConstantVariable.UNDER_SCORE+
								EncryptRealFileNameForMD5.generateEncryptFromStringToMD5(String.valueOf(newAttachment.getId()));

						newFile = new File(pathFile + realFileName);

						uploadTime = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US).format(date);

						newAttachment.setRealFileName(realFileName);
						newAttachment.setUploadTime(uploadTime);
						entityManager.merge(newAttachment);

						FileCopyUtils.copy(oldFile,newFile);
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Transactional
	@PostMapping(value="/generateDocumentNumber",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public String generateDocumentNumber(@RequestBody Document document) {

		LOGGER.info("><><>< GENERATE DOCUMENT NUMBER ><><><");
		String prefix = "";
		String documentNumber = "";

		Document oldDocument = entityManager.find(Document.class, document.getId());
		RunningType runningType = runningTypeRepository.findByCode(oldDocument.getDocumentType());

		if (runningType != null ) {
			prefix = runningNumberRepository.generateRealRunningNumber(oldDocument.getDocumentType(), oldDocument.getCompanyCode(), ConstantVariable.DOCUMENT_STATUS_REAL);
			documentNumber = runningNumberRepository.generateRunningNumber(runningType, prefix);
		}


		oldDocument.setTmpDocNumber(documentNumber);
		entityManager.merge(oldDocument);
		entityManager.flush();


		return (new JSONSerializer()
				.exclude("*.class")
				.include("id").include("docNumber")
				.include("documentType").include("documentStatus")
				.include("documentApprove.id").include("documentApprove.remark")
				.include("documentApprove.documentApproveItem.id").include("documentApprove.documentApproveItem.approveType")
				.include("documentApprove.documentApproveItem.travelReason").include("documentApprove.documentApproveItem.carBooking")
				.include("documentApprove.documentApproveItem.hotelBooking").include("documentApprove.documentApproveItem.flightTicket")
				.include("documentApprove.documentApproveItem.externalMembers").include("documentApprove.documentApproveItem.travelMembers")
				.include("documentApprove.documentApproveItem.travelDetails").include("documentApprove.documentApproveItem.documentReferences")
				.include("documentApprove.documentApproveItem.documentAttachment").include("documentAdvance.id")
				.include("documentAdvance.advancePurpose").include("documentAdvance.requiredReference")
				.include("documentAdvance.amount").include("documentAdvance.startDate")
				.include("documentAdvance.bankNumber").include("documentAdvance.remark")
				.include("documentAdvance.endDate").include("documentAdvance.dueDate")
				.include("documentAdvance.documentAdvanceItems.id").include("documentType")
				.include("documentExpense.id").include("documentExpense.documentExpenseItems.id")
				.include("documentExpense.documentExpenseItems.internalOrder").include("documentExpense.documentExpenseItems.remark")
				.include("documentExpense.documentExpenseItems.expTypeByCompany.id").include("documentExpense.documentExpenseItems.expTypeByCompany.glCode")
				.include("documentExpense.documentExpenseItems.expTypeByCompany.favorite").include("documentExpense.documentExpenseItems.expTypeByCompany.pa")
				.include("documentExpense.documentExpenseItems.expTypeByCompany.psa").include("documentExpense.documentExpenseItems.expTypeByCompany.company.id")
				.include("documentExpense.documentExpenseItems.expTypeByCompany.company.code").include("documentExpense.documentExpenseItems.expTypeByCompany.company.description")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.id").include("documentExpense.documentExpenseItems.docExpItemDetail.employee")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.receiver").include("documentExpense.documentExpenseItems.docExpItemDetail.receiverCompany")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.description").include("documentExpense.documentExpenseItems.docExpItemDetail.hotelCode")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.place1").include("documentExpense.documentExpenseItems.docExpItemDetail.place2")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.subjectName").include("documentExpense.documentExpenseItems.docExpItemDetail.country")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.time1").include("documentExpense.documentExpenseItems.docExpItemDetail.time2")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.carLicence").include("documentExpense.documentExpenseItems.docExpItemDetail.month")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.phoneNumber").include("documentExpense.documentExpenseItems.docExpItemDetail.date1")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.date2").include("documentExpense.documentExpenseItems.docExpItemDetail.variable1")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable2").include("documentExpense.documentExpenseItems.docExpItemDetail.variable3")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable4").include("documentExpense.documentExpenseItems.docExpItemDetail.variable5")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable6").include("documentExpense.documentExpenseItems.docExpItemDetail.variable7")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable8").include("documentExpense.documentExpenseItems.docExpItemDetail.variable9")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable10").include("documentExpense.documentExpenseItems.docExpItemDetail.variable11")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable12").include("documentExpense.documentExpenseItems.docExpItemDetail.variable13")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable14").include("documentExpense.documentExpenseItems.docExpItemDetail.variable15")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable16").include("documentExpense.documentExpenseItems.docExpItemDetail.variable17")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable18").include("documentExpense.documentExpenseItems.docExpItemDetail.variable19")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable20").include("tmpDocNumber")
				.exclude("*")
				.serialize(oldDocument));

	}

	@GetMapping(value="/findByDocumentTypeAndDocumentStatusAndApproveType",produces = "application/json; charset=utf-8")
	public String findByDocumentTypeAndDocumentStatusAndApproveType(@RequestParam("requester") String requester) {

		LOGGER.info("><><>< findDocumentByDocStatusCompleteAndAppTypeTravelDetail ><><><");
		List<String> appTypeLs	 = Arrays.asList(ConstantVariable.MASTER_DATA_DETAIL_APPROVE_TYPE_DOMESTIC,ConstantVariable.MASTER_DATA_DETAIL_APPROVE_TYPE_FOREIGN);

		List<Document> documentLs  = documentRepository.findByDocumentTypeAndDocumentStatusAndApproveTypeAndRequester(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_APPROVE,ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE,appTypeLs,requester);

		LOGGER.info("documentLs = {} ",documentLs.size());

		return (new JSONSerializer().exclude("*.class").include("docNumber").exclude("*").serialize(documentLs));
	}

	@GetMapping(value="/findByDocumentTypeAndDocumentStatusAndApproveTypeAndKeySearch",produces = "application/json; charset=utf-8")
	public String findByDocumentTypeAndDocumentStatusAndApproveType(@RequestParam("requester") String requester,@RequestParam("keySearch") String keySearch) {

		LOGGER.info("><><>< findDocumentByDocStatusCompleteAndAppTypeTravelDetail ><><><");
		List<String> appTypeLs	 = Arrays.asList(ConstantVariable.MASTER_DATA_DETAIL_APPROVE_TYPE_DOMESTIC,ConstantVariable.MASTER_DATA_DETAIL_APPROVE_TYPE_FOREIGN);

		List<Document> documentLs  = documentRepository.findByDocumentTypeAndDocumentStatusAndApproveTypeAndRequesterAndKeySearch(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_APPROVE,ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE,appTypeLs,requester,keySearch);

		LOGGER.info("documentLs = {} ",documentLs.size());

		return (new JSONSerializer().exclude("*.class").include("docNumber").exclude("*").serialize(documentLs));
	}

	@GetMapping(value="/findByDocTypeAndDocStatusCompleteAndAppTypeTravelAndTravelMemberUser",produces = "application/json; charset=utf-8")
	public String findByDocTypeAndDocStatusCompleteAndAppTypeTravelAndTravelMemberUser(@RequestParam("userName") String userName) {

		LOGGER.info("><><>< findByDocTypeAndDocStatusCompleteAndAppTypeTravelAndTravelMemberUser ><><><");

		List<Document> documentLs  = documentRepository.findByDocTypeAndDocStatusCompleteAndAppTypeTravelAndTravelMemberUser(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_APPROVE,ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE,userName);

		LOGGER.info("documentLs = {} ",documentLs.size());

		return (new JSONSerializer().exclude("*.class").include("id").include("docNumber").exclude("*").serialize(documentLs));
	}

	@GetMapping(value="/findByDocTypeAndDocStatusCompleteAndUser",produces = "application/json; charset=utf-8")
	public String findByDocTypeAndDocStatusCompleteAndUser(@RequestParam("userName") String userName) {

		LOGGER.info("><><>< findByDocTypeAndDocStatusCompleteAndUser ><><><");

		List<Document> documentLs  = documentRepository.findByDocTypeAndDocStatusCompleteAndUser(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_ADVANCE,ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE,userName);

		LOGGER.info("documentLs = {} ",documentLs.size());

		return (new JSONSerializer().exclude("*.class")
				.include("id")
				.include("docNumber")
				.include("documentAdvance.id")
				.include("documentAdvance.amount")
				.include("documentReferences.id")
				.include("documentReferences.docReferenceNumber")
				.exclude("*").serialize(documentLs));
	}

	@GetMapping(value="/findByDocTypeAndDocNumberAndDocStatusCompleteAndUser",produces = "application/json; charset=utf-8")
	public String findByDocTypeAndDocNumberAndDocStatusCompleteAndUser(@RequestParam("userName") String userName,@RequestParam("docNumber") String docNumber) {

		LOGGER.info("><><>< findByDocTypeAndDocNumberAndDocStatusCompleteAndUser ><><><");

		List<Document> documentLs  = documentRepository.findByDocTypeAndDocNumberAndDocStatusCompleteAndUser(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_ADVANCE,docNumber,ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE,userName);

		LOGGER.info("documentLs = {} ",documentLs.size());

		return (new JSONSerializer().exclude("*.class")
				.include("id")
				.include("docNumber")
				.include("documentAdvance.id")
				.include("documentAdvance.amount")
				.include("documentReferences.id")
				.include("documentReferences.docReferenceNumber")
				.exclude("*").serialize(documentLs));
	}

	@Transactional
	@PostMapping(value="/updateDocumentAdvance",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public String updateDocumentAdvance(@RequestBody Document document){

		LOGGER.info("><><>< UPDATE DOCUMENT ADVANCE ><><><");
		Document oldDocument = entityManager.find(Document.class, document.getId());
		try {
			oldDocument.setRequester(document.getRequester());
			oldDocument.setDepartmentCode(document.getDepartmentCode());
			oldDocument.setCompanyCode(document.getCompanyCode());
			oldDocument.setTitleDescription(document.getTitleDescription());
			oldDocument.setCostCenterCode(document.getCostCenterCode());
			oldDocument.setTotalAmount(document.getDocumentAdvance().getAmount());

			oldDocument.getDocumentAdvance().setAdvancePurpose(document.getDocumentAdvance().getAdvancePurpose());
			oldDocument.getDocumentAdvance().setAmount(document.getDocumentAdvance().getAmount());
			oldDocument.getDocumentAdvance().setBankNumber(document.getDocumentAdvance().getBankNumber());
			oldDocument.getDocumentAdvance().setRemark(document.getDocumentAdvance().getRemark());
			oldDocument.getDocumentAdvance().setStartDate(document.getDocumentAdvance().getStartDate());
			oldDocument.getDocumentAdvance().setEndDate(document.getDocumentAdvance().getEndDate());
			oldDocument.getDocumentAdvance().setDueDate(document.getDocumentAdvance().getDueDate());

			if(!"".equals(document.getDocumentAdvance().getBankNumber())){
				Employee employee = employeeRepository.findByEmpUsername(document.getRequester());
				if(null != employee){
					employee.setBankNumber(document.getDocumentAdvance().getBankNumber());
					entityManager.merge(employee);
					entityManager.flush();
				}
			}

			entityManager.merge(oldDocument);
			entityManager.flush();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (new JSONSerializer()
				.exclude("*.class")
				.include("id").include("docNumber")
				.include("documentType").include("documentStatus")
				.include("documentApprove.id").include("documentApprove.remark")
				.include("documentApprove.documentApproveItem.id").include("documentApprove.documentApproveItem.approveType")
				.include("documentApprove.documentApproveItem.travelReason").include("documentApprove.documentApproveItem.carBooking")
				.include("documentApprove.documentApproveItem.hotelBooking").include("documentApprove.documentApproveItem.flightTicket")
				.include("documentApprove.documentApproveItem.externalMembers").include("documentApprove.documentApproveItem.travelMembers")
				.include("documentApprove.documentApproveItem.travelDetails").include("documentApprove.documentApproveItem.documentReferences")
				.include("documentApprove.documentApproveItem.documentAttachment").include("documentAdvance.id")
				.include("documentAdvance.advancePurpose").include("documentAdvance.requiredReference")
				.include("documentAdvance.amount").include("documentAdvance.startDate")
				.include("documentAdvance.bankNumber").include("documentAdvance.remark")
				.include("documentAdvance.endDate").include("documentAdvance.dueDate")
				.include("documentAdvance.documentAdvanceItems.id").include("documentType")
				.include("documentExpense.id").include("documentExpense.documentExpenseItems.id")
				.include("documentExpense.documentExpenseItems.internalOrder").include("documentExpense.documentExpenseItems.remark")
				.include("documentExpense.documentExpenseItems.expTypeByCompany.id").include("documentExpense.documentExpenseItems.expTypeByCompany.glCode")
				.include("documentExpense.documentExpenseItems.expTypeByCompany.favorite").include("documentExpense.documentExpenseItems.expTypeByCompany.pa")
				.include("documentExpense.documentExpenseItems.expTypeByCompany.psa").include("documentExpense.documentExpenseItems.expTypeByCompany.company.id")
				.include("documentExpense.documentExpenseItems.expTypeByCompany.company.code").include("documentExpense.documentExpenseItems.expTypeByCompany.company.description")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.id").include("documentExpense.documentExpenseItems.docExpItemDetail.employee")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.receiver").include("documentExpense.documentExpenseItems.docExpItemDetail.receiverCompany")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.description").include("documentExpense.documentExpenseItems.docExpItemDetail.hotelCode")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.place1").include("documentExpense.documentExpenseItems.docExpItemDetail.place2")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.subjectName").include("documentExpense.documentExpenseItems.docExpItemDetail.country")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.time1").include("documentExpense.documentExpenseItems.docExpItemDetail.time2")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.carLicence").include("documentExpense.documentExpenseItems.docExpItemDetail.month")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.phoneNumber").include("documentExpense.documentExpenseItems.docExpItemDetail.date1")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.date2").include("documentExpense.documentExpenseItems.docExpItemDetail.variable1")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable2").include("documentExpense.documentExpenseItems.docExpItemDetail.variable3")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable4").include("documentExpense.documentExpenseItems.docExpItemDetail.variable5")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable6").include("documentExpense.documentExpenseItems.docExpItemDetail.variable7")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable8").include("documentExpense.documentExpenseItems.docExpItemDetail.variable9")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable10").include("documentExpense.documentExpenseItems.docExpItemDetail.variable11")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable12").include("documentExpense.documentExpenseItems.docExpItemDetail.variable13")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable14").include("documentExpense.documentExpenseItems.docExpItemDetail.variable15")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable16").include("documentExpense.documentExpenseItems.docExpItemDetail.variable17")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable18").include("documentExpense.documentExpenseItems.docExpItemDetail.variable19")
				.include("documentExpense.documentExpenseItems.docExpItemDetail.variable20").include("tmpDocNumber")
				.exclude("*")
				.serialize(oldDocument));
	}

	@GetMapping(value="/findTravelMemberAndExternalMemberBydocRef",produces = "application/json; charset=utf-8")
	public String findTravelMemberAndExternalMemberBydocRef(@RequestParam("documentNumber") String documentNumber) {

		LOGGER.info("><><>< findTravelMemberAndExternalMemberBydocRef ><><><");

		String result = "0";
		Document document = documentRepository.findByDocNumber(documentNumber);

		if(document != null && document.getDocumentApprove().getDocumentApproveItems() != null){
			List<DocumentApproveItem> documentApproveItems = new ArrayList<>(document.getDocumentApprove().getDocumentApproveItems());
			List<Long> docApproveItemIdLs = new ArrayList<>();
			for(DocumentApproveItem documentApproveItem : documentApproveItems){
				docApproveItemIdLs.add(documentApproveItem.getId());
			}
			List<TravelMember> travelMemberLs = travelMemberRepository.findByDocumentApproveItemId(docApproveItemIdLs);
			List<ExternalMember> externalMemberLs = externalMemberRepository.findByDocumentApproveItemId(docApproveItemIdLs);

			result = String.valueOf(travelMemberLs.size() + externalMemberLs.size());
		}

		return (new JSONSerializer().serialize(result));
	}

	@GetMapping(value="/findTravelDetailsByDocRef",produces = "application/json; charset=utf-8")
	public String findTravelDetailsByDocRef(@RequestParam("documentNumber") String documentNumber) {

		LOGGER.info("><><>< findTravelDetailsBydocRef ><><><");

		String result = "";
		Document document = documentRepository.findByDocNumber(documentNumber);
		List<Long> docApproveItemIdLs = new ArrayList<>();
		if(document != null && document.getDocumentApprove() != null && document.getDocumentApprove().getDocumentApproveItems() != null){
			List<DocumentApproveItem> documentApproveItems = new ArrayList<>(document.getDocumentApprove().getDocumentApproveItems());

			for(DocumentApproveItem documentApproveItem : documentApproveItems){
				docApproveItemIdLs.add(documentApproveItem.getId());
			}
		}
		List<TravelDetail> travelDetailLs = travelDetailRepository.findByDocumentApproveItemId(docApproveItemIdLs);

		return (new JSONSerializer().exclude("*.class").serialize(travelDetailLs));
	}

	@GetMapping(value="/findMasterDataHotelByCode",produces = "application/json; charset=utf-8")
	public String findMasterDataHotelByCode() {

		LOGGER.info("><><>< findMasterDataHotelByCode ><><><");

		MasterData masterData = masterDataRepository.findByCode(ConstantVariable.MASTER_DATA_HOTEL_CODE);
		List<MasterDataDetail> masterDataDetailLs = new ArrayList<>(masterData.getDetails());

		return (new JSONSerializer().exclude("*.class").serialize(masterDataDetailLs));
	}

	@GetMapping(value="/findTravelMemberByDocRef",produces = "application/json; charset=utf-8")
	public String findTravelMemberByDocRef(@RequestParam("documentNumber") String documentNumber) {

		LOGGER.info("><><>< findTravelMemberByDocRef ><><><");

		String result = "0";
		Document document = documentRepository.findByDocNumber(documentNumber);

		List<Long> docApproveItemIdLs = new ArrayList<>();
		if(document != null && document.getDocumentApprove().getDocumentApproveItems() != null){
			List<DocumentApproveItem> documentApproveItems = new ArrayList<>(document.getDocumentApprove().getDocumentApproveItems());
			for(DocumentApproveItem documentApproveItem : documentApproveItems){
				docApproveItemIdLs.add(documentApproveItem.getId());
			}
		}

		List<TravelMember> travelMemberLs = travelMemberRepository.findByDocumentApproveItemId(docApproveItemIdLs);
		return (new JSONSerializer().exclude("*.class").serialize(travelMemberLs));
	}

	@GetMapping(value="/findMasterDataAirlineByCode",produces = "application/json; charset=utf-8")
	public String findMasterDataAirlineByCode() {

		LOGGER.info("><><>< findMasterDataAirlineByCode ><><><");

		MasterData masterData = masterDataRepository.findByCode(ConstantVariable.MASTER_DATA_AIRLINE_CODE);
		List<MasterDataDetail> masterDataDetailLs = new ArrayList<>(masterData.getDetails());

		return (new JSONSerializer().exclude("*.class").serialize(masterDataDetailLs));
	}


	@GetMapping(value="/search/findDocumentApproveByUserNameApproverAndDocType",produces = "application/json; charset=utf-8")
	public String findDocumentApproveByUserNameApproverAndDocType(@RequestParam("approver") String approver,
																  @RequestParam("docNumber") String docNumber,
																  @RequestParam("titleDescription") String titleDescription,
																  @RequestParam("docStatus") String docStatus,
																  @RequestParam("docType") String documentType,
																  @RequestParam("firstResult") Integer firstResult,
																  @RequestParam("maxResult") Integer maxResult,
																  @RequestParam("page") Integer page) {

		LOGGER.info("><><><>< findDocumentApproveByUserNameApproverAndDocType ><><><");

//		List<Request> approverList = requestApproverRepository.findByUserNameApprover(approver);

//		List<Long> request = new ArrayList<>();
//		for(Request requestObj : approverList){
//			request.add(requestObj.getId());
//		}

		List<String> requesterList = new ArrayList<>();
		String[] userName = approver.split(",");
		for (String i: userName) {
			requesterList.add(i);
		}

		List<String> documentStatus = new ArrayList<>();
		if(docStatus.equals("null")){
			documentStatus.add(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_DRAFT);
			documentStatus.add(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_ON_PROCESS);
			documentStatus.add(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_CANCEL);
			documentStatus.add(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_REJECT);
			documentStatus.add(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE);
		}else{
			String[] str = docStatus.split(",");
			for (String i: str) {
				documentStatus.add(i);
			}

		}

//		List<Document> documentLs = documentRepository.findByDocNumberAndTitleDescriptionAndDocumentTypeAndDocumentStatusInAndRequestInOrderBySendDateDesc(docNumber,titleDescription,documentType,documentStatus,request);
		Page<Document> documentLs = documentRepository.findByDocNumberAndTitleDescriptionAndDocumentTypeAndDocumentStatusInAndRequestInOrderBySendDateByDocNumberDesc(docNumber,titleDescription,documentType,documentStatus,requesterList,new PageRequest(page,maxResult));

		return (new JSONSerializer().exclude("*.class").include("id")
				.include("id")
				.include("docNumber")
				.include("requester")
				.include("titleDescription")
				.include("documentType")
				.include("approveType")
				.include("sendDate")
				.include("requesterName")
				.include("requesterNameEn")
				.include("request.id")
				.include("request.requestDate")
				.include("request.lastActionTime")
				.include("request.requestApprovers.id")
				.include("request.requestApprovers.actionState")
				.include("request.requestApprovers.actionStateName")
				.include("request.requestApprovers.actionTime")
				.include("request.requestApprovers.requestStatusCode")
				.include("request.requestApprovers.sequence")
				.exclude("*").serialize(documentLs));
	}

	@GetMapping(value="/search/findDocumentApproveByUserNameApproverAndDocTypeSize",produces = "application/json; charset=utf-8")
	public String findDocumentApproveByUserNameApproverAndDocTypeSize(@RequestParam("approver") String approver,
																  @RequestParam("docNumber") String docNumber,
																  @RequestParam("titleDescription") String titleDescription,
																  @RequestParam("docStatus") String docStatus,
																  @RequestParam("docType") String documentType) {

		LOGGER.info("><><><>< findDocumentApproveByUserNameApproverAndDocType ><><><");

//		List<Request> approverList = requestApproverRepository.findByUserNameApprover(approver);

//		List<Long> request = new ArrayList<>();
//		for(Request requestObj : approverList){
//			request.add(requestObj.getId());
//		}

		List<String> requesterList = new ArrayList<>();
		String[] userName = approver.split(",");
		for (String i: userName) {
			requesterList.add(i);
		}

		List<String> documentStatus = new ArrayList<>();
		if(docStatus.equals("null")){
			documentStatus.add(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_DRAFT);
			documentStatus.add(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_ON_PROCESS);
			documentStatus.add(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_CANCEL);
			documentStatus.add(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_REJECT);
			documentStatus.add(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE);
		}else{
			String[] str = docStatus.split(",");
			for (String i: str) {
				documentStatus.add(i);
			}

		}

		Long size = documentRepository.countByDocNumberAndTitleDescriptionAndDocumentTypeAndDocumentStatusInAndRequestIn(docNumber,titleDescription,documentType,documentStatus,requesterList);
//		Long size = documentRepository.findByDocNumberAndTitleDescriptionAndDocumentTypeAndDocumentStatusInAndRequestInOrderBySendDateByDocNumberDesc(docNumber,titleDescription,documentType,documentStatus,approver,new PageRequest(0,1)).getTotalElements();

		Map<String,Long> map = new HashMap<>();
		map.put("size",size);

		return (new JSONSerializer().exclude("*.class").serialize(map));
	}

	@Transactional
	@PostMapping(value="/unlockAdvance",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public String unlockAdvance(@RequestBody Document document){

		LOGGER.info("======== UNLOCK DOCUMENT ADVANCE ========");
		String result = "";
		Document oldDocument = entityManager.find(Document.class, document.getId());
		try {
			List<Document> documentList = documentRepository.findDocumentForCheckUnlockAdvance(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_ADVANCE,ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_COMPLETE,oldDocument.getRequester());
			if(documentList.size()>0){
				String docNumber = "";
				for(Document d:documentList){
					docNumber += d.getDocNumber()+" ";
				}
				result += "ไม่สามารถปลดล็อกเอกสารได้ เนื่องจากมีเอกสารที่ปลดล็อกแล้ว : "+docNumber;
			} else {
				DocumentAdvance documentAdvance = oldDocument.getDocumentAdvance();
				if(null != documentAdvance){
					documentAdvance.setFlagUnlock("Y");
					entityManager.merge(documentAdvance);
					entityManager.flush();
				}
				result = "Success";
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = "Error";
		}

		return (new JSONSerializer().serialize(result));
	}

	@GetMapping(value = "/viewDocAdmin",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity<String> viewDocumentAdmin(
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "creator", required = false) String creator,
			@RequestParam(value = "documentNumber", required = false) String documentNumber,
			@RequestParam(value = "documentType", required = false) String documentType,
			@RequestParam(value = "documentStatus", required = false) String documentStatus,
			@RequestParam(value = "firstResult", required = true) Long first,
			@RequestParam(value = "maxResult", required = true) Long max,
			HttpServletRequest request)
	{

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		List<String> documentTypeList = new ArrayList<>();
		for(String str : documentType.split(",")){
			documentTypeList.add(str);
		}

		startDate = startDate + " 00:00:00";
		endDate = endDate + " 23:59:59";

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		Date startDateParse = null;
		Date endDateParse = null;

		LOGGER.info("documentStatus : {}",documentStatus);
		String documentStatusArray[] = documentStatus.split(",");

		List<String> documentStatusList = new ArrayList<>();
		for(String str : documentStatusArray){
			documentStatusList.add(str);
		}

		try {
			startDateParse = dateFormat.parse(startDate);
			endDateParse = dateFormat.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Timestamp timestampStartDate = new Timestamp(startDateParse.getTime());
		Timestamp timestampEndDate = new Timestamp(endDateParse.getTime());

		try{
			List<Document> result = documentRepository.findByCreatedDateStartAndCreatedDateEndAndCreatorAndDocNumberAndDocumentType(
					timestampStartDate.toString(),timestampEndDate.toString(),creator,documentNumber,documentTypeList,documentStatusList,first,max);

			headers.add("statusValidate","0");

			return new ResponseEntity<String>(new JSONSerializer().prettyPrint(true).serialize(result),headers,HttpStatus.OK);

		}catch (Exception e){
			LOGGER.error("viewDocumentAdmin {}",e.getMessage());
			e.printStackTrace();

			return new ResponseEntity<String>(e.getMessage(),headers,HttpStatus.OK);
		}
	}

	@GetMapping(value = "/viewDocAdminSize",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity<String> viewDocumentAdminPageSize(
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "creator", required = false) String creator,
			@RequestParam(value = "documentNumber", required = false) String documentNumber,
			@RequestParam(value = "documentType", required = false) String documentType,
			@RequestParam(value = "documentStatus", required = false) String documentStatus,
			HttpServletRequest request)
	{

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		String documentStatusArray[] = documentStatus.split(",");
		List<String> documentStatusList = new ArrayList<>();
		for(String str : documentStatusArray){
			documentStatusList.add(str);
		}

		List<String> documentTypeList = new ArrayList<>();
		for(String str : documentType.split(",")){
			documentTypeList.add(str);
		}

		startDate = startDate + " 00:00:00";
		endDate = endDate + " 23:59:59";

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		Date startDateParse = null;
		Date endDateParse = null;

		try {
			startDateParse = dateFormat.parse(startDate);
			endDateParse = dateFormat.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Timestamp timestampStartDate = new Timestamp(startDateParse.getTime());
		Timestamp timestampEndDate = new Timestamp(endDateParse.getTime());

		try{
			Long size = documentRepository.pagingFindByCreatedDateStartAndCreatedDateEndAndCreatorAndDocNumberAndDocumentType(
					timestampStartDate.toString(),timestampEndDate.toString(),creator,documentNumber,documentTypeList,documentStatusList);
			Map<String,Integer> sizeMap = new HashMap<>();
			sizeMap.put("size",size.intValue());
			sizeMap.put("totalElements",size.intValue());
			headers.add("statusValidate","0");

			return new ResponseEntity<String>((new JSONSerializer().serialize(sizeMap)),headers, HttpStatus.OK);
		}catch (Exception e){
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(),headers, HttpStatus.OK);
		}

	}
	
	@GetMapping(value="/getDocumentAccessPerson",produces = "application/json; charset=utf-8")
	public Set getDocumentAccessPerson(@RequestParam("doc")  Document document) {

		LOGGER.info("><><>< getDocumentAccessPerson ><><><");
		
		Set<String> accessPerson = new HashSet<String>();
		accessPerson.add(document.getCreatedBy());
		
		
		
		//Office Admin
		if(document.getCcMailAddress()!=null ){ 
			accessPerson.add(document.getCcMailAddress().split("@")[0]);
		}
		

		accessPerson.add(document.getRequester());
		for(String person:getPersonalAssistanc(document.getRequester())){
			accessPerson.add(person);
		}
		
		if(document.getRequest()!=null && document.getRequest().getRequestApprovers() !=null){
			for(RequestApprover approver:document.getRequest().getRequestApprovers()){
				for(String person:getPersonalAssistanc(approver.getUserNameApprover())){
					accessPerson.add(person);
				}
				
				for(String person:getAccountTeam(approver.getUserNameApprover())){
					accessPerson.add(person);
				}
				
			}
		}
		
		if(document.getUserNameMember()!=null){
			for(String person:document.getUserNameMember().split(",")){
				accessPerson.add(person);
				for(String personAssist:getPersonalAssistanc(person)){
					accessPerson.add(personAssist);
				}
			}
		}
		
		return accessPerson;
	}
	
	public Set<String> getPersonalAssistanc(String usernamePersonal) {
		Set<String> personalAssistance = new HashSet<String>();
		personalAssistance.add(usernamePersonal);
		List<String> personalAssistParameter = new ArrayList<String>();
		personalAssistParameter.add(ConstantVariable.MASTER_DATA_PERSONAL_ASSISTANCE);
		
		MasterDataDetail personalAssistMasterDataDetail;
		try {
			personalAssistMasterDataDetail = masterDataDetailRepository.findByMasterdataInAndMasterDataDetailCodeOrderByCode(personalAssistParameter, usernamePersonal);
			personalAssistance.add(personalAssistMasterDataDetail.getVariable1());
		} catch (Exception e) {/* Log Error */}
		return personalAssistance;
	}
	

	public Set<String> getAccountTeam(String usernameAccount) {
		Set<String> personalAccountTeam = new HashSet<String>();
		personalAccountTeam.add(usernameAccount);
		List<String> userAccountRelationParameter = new ArrayList<String>();
		userAccountRelationParameter.add(ConstantVariable.MASTER_DATA_ACCOUNT_TEAM);
		
		try {
			List<MasterDataDetail> masterDataDetailLs = masterDataDetailRepository.findByMasterdataInOrderByCode(userAccountRelationParameter);
			if(masterDataDetailLs!=null) for(MasterDataDetail masterDataDetail:masterDataDetailLs){
				if(masterDataDetail.getVariable1() != null && masterDataDetail.getVariable1().equals(usernameAccount)){
					personalAccountTeam.add(masterDataDetail.getCode());
				}
			}
		} catch (Exception e) { }
		return personalAccountTeam;
	}
	

	private Map getMapFromUsername(String username){
		ResponseEntity<String> response = employeeProfileService.findEmployeeProfileByEmployeeCode(username);
		return gson.fromJson(response.getBody(), Map.class);
	}
	
	private String getNameFromUsername(Map dataProfile){
		return String.valueOf(dataProfile.get("FOA")) + String.valueOf(dataProfile.get("FNameTH")) + " " + String.valueOf(dataProfile.get("LNameTH"));
	}

	private String getNameFromUsernameEn(Map dataProfile){
		return String.valueOf(dataProfile.get("NameEN"));
	}

	@GetMapping("/findMyRequestNotification")
	public String findMyRequestNotification(
			@RequestParam("requester") String requester,
			@RequestParam("docNumber") String docNumber,
			@RequestParam("titleDescription") String titleDescription,
			@RequestParam("documentStatus") List<String> documentStatus
	){

		Long appSize = documentRepository.countByRequesterAndDocNumberIgnoreCaseContainingAndTitleDescriptionIgnoreCaseContainingAndDocumentTypeAndDocumentStatusIn(
				requester,
				docNumber,
				titleDescription,
				"APP",
				documentStatus
		);

		Long expSize = documentRepository.countByRequesterAndDocNumberIgnoreCaseContainingAndTitleDescriptionIgnoreCaseContainingAndDocumentTypeAndDocumentStatusIn(
				requester,
				docNumber,
				titleDescription,
				"EXP",
				documentStatus
		);

		Long advSize = documentRepository.countByRequesterAndDocNumberIgnoreCaseContainingAndTitleDescriptionIgnoreCaseContainingAndDocumentTypeAndDocumentStatusIn(
				requester,
				docNumber,
				titleDescription,
				"ADV",
				documentStatus
		);

		Map<String,Long> map = new HashMap<>();
		map.put("appSize",appSize);
		map.put("expSize",expSize);
		map.put("advSize",advSize);


		return (new JSONSerializer().exclude("*.class").serialize(map));
	}

	/**
	 * Create by iMilkii 2018.02.02
	 * Method delete approve item
	 **/
	@Transactional
	@GetMapping(value="/deleteDocumentApproveItem",produces = "application/json; charset=utf-8")
	public String deleteDocumentApproveItem(@RequestParam("docId") Long docId,@RequestParam("approveType") String approveType){
		LOGGER.info("===== deleteDocumentApproveItem : {} {}",docId,approveType);
		Long docApproveId = null;
		try{
			Document document = documentRepository.findOne(docId);
			Set<DocumentApproveItem> documentApproveItems = document.getDocumentApprove().getDocumentApproveItem();
			List<DocumentApproveItem> documentApproveItemLs = new ArrayList<>(documentApproveItems);

			Set<DocumentApproveItem> newApproveItems = new HashSet<>();
			if(documentApproveItemLs.size() > 0){
				for(DocumentApproveItem documentApproveItem : documentApproveItemLs){
					if(documentApproveItem.getApproveType().equals(approveType)){
						LOGGER.info("><><>< IN CASE ");
						LOGGER.info("><><>< ID  :  {} ",documentApproveItem.getId());
						documentApproveItem.setDocumentApprove(null);
						documentApproveItemRepository.saveAndFlush(documentApproveItem);
					}else{
						newApproveItems.add(documentApproveItem);
					}
				}
			}

			document.getDocumentApprove().setDocumentApproveItems(null);
			documentRepository.saveAndFlush(document);

		}catch (Exception e){
			e.printStackTrace();
		}

		return "SUCCESS";
	}

	/**
	 * Create by iMilkii 2018.02.13
	 * Method cancel Document Manual
	 **/
	@GetMapping(value="/cancelDocumentManual",produces = "application/json; charset=utf-8")
	public String cancelDocumentManual(@RequestParam("documentNumber") String documentNumber) {
		LOGGER.info("===== cancelDocumentManual : {} ", documentNumber);
		String status = null;
		try{
			status = documentRepository.cancelDocumentManual(documentNumber);
		}catch (Exception e){
			e.printStackTrace();
		}
		return status;
	}

	/**
	 * Create by iMilkii 2018.02.13
	 * Method clear Document Manual
	 **/
	@GetMapping(value="/clearDocumentManual",produces = "application/json; charset=utf-8")
	public String clearDocumentManual(@RequestParam("documentNumber") String documentNumber) {
		LOGGER.info("===== clearDocumentManual : {} ", documentNumber);
		String status = null;
		try{
			status = documentRepository.clearDocumentManual(documentNumber);
		}catch (Exception e){
			e.printStackTrace();
		}
		return status;
	}


	@PostMapping(value="/createDocumentFromEmed",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity<String> createDocumentFromEmed(@RequestBody String document){

		LOGGER.info("><><>< Create Document From E-med ><><><");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		try{
			JSONObject jsonFromEmed = new JSONObject(document);
			InterfaceActiveLog interfaceActiveLog = new InterfaceActiveLog();
			interfaceActiveLog.setCreatedDate(new Timestamp(new Date().getTime()));
			interfaceActiveLog.setInterfaceChanel(jsonFromEmed.getString("source"));
			interfaceActiveLog.setAmount(jsonFromEmed.getDouble("amount"));
			interfaceActiveLog.setRemark(jsonFromEmed.getString("remark"));
			interfaceActiveLog.setLocation(jsonFromEmed.getString("location"));
			interfaceActiveLog.setEmpId(jsonFromEmed.getString("empId"));
			interfaceActiveLog.setCostCenter(jsonFromEmed.getString("costCenter"));
			interfaceActiveLog.setWithdrawFlag(jsonFromEmed.getString("withdrawFlag"));
			interfaceActiveLog.setDocNumber(jsonFromEmed.getString("docNumber"));
			interfaceActiveLog.setCompCode(jsonFromEmed.getString("compCode"));
			interfaceActiveLog.setInternalOrder(jsonFromEmed.getString("internalOrder"));
			interfaceActiveLog.setVertify(jsonFromEmed.getString("approver1"));
			interfaceActiveLog.setVertifyDate(jsonFromEmed.getString("approverTime1"));
			interfaceActiveLog.setApprover(jsonFromEmed.getString("approver2"));
			interfaceActiveLog.setApproverDate(jsonFromEmed.getString("approverTime2"));
			interfaceActiveLogRepository.saveAndFlush(interfaceActiveLog);

			Map<String,String> map = documentRepository.createDocumentFromEmed(document);
			return new ResponseEntity<String>((new JSONSerializer().serialize(map)), headers, HttpStatus.OK);
		}catch (Exception e){
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@PostMapping(value = "/updateCostCenterCode",headers = "Accept=application/json; charset=UTF-8;")
	public ResponseEntity<String> updateCostCenterCode(@RequestBody String json){
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type","application/json");
		try {
			Map map = gson.fromJson(json,Map.class);
			documentService.updateCostCenterCode(map);
			return new ResponseEntity<>("OK",headers,HttpStatus.OK);
		}catch (Exception e){
			e.printStackTrace();
			return new ResponseEntity<>("ERROR",headers,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/updateDocumentExpenseItemAndDocumentExpenseGroupByExpenseTypeByCompany",headers = "Accept=application/json; charset=UTF-8;")
	public ResponseEntity<String> updateDocumentExpenseItemAndDocumentExpenseGroupByExpenseTypeByCompany(@RequestBody String json){
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type","application/json");
		try {
			Map map = gson.fromJson(json,Map.class);
			documentService.updateDocumentExpenseItemAndDocumentExpenseGroupByExpenseTypeByCompany(map);
			return new ResponseEntity<>("OK",headers,HttpStatus.OK);
		}catch (Exception e){
			e.printStackTrace();
			return new ResponseEntity<>("ERROR",headers,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
