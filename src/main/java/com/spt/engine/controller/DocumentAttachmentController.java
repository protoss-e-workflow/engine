package com.spt.engine.controller;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;

import com.spt.engine.entity.app.DocumentAdvance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentAttachment;
import com.spt.engine.entity.app.TravelDetail;
import com.spt.engine.entity.general.Parameter;
import com.spt.engine.entity.general.ParameterDetail;
import com.spt.engine.repository.app.DocumentAttachmentRepository;
import com.spt.engine.repository.app.LocationRepository;
import com.spt.engine.repository.general.ParameterRepository;
import com.spt.engine.util.EncryptRealFileNameForMD5;

import flexjson.JSONSerializer;

@RestController
@RequestMapping("/documentAttachmentCustom")
public class DocumentAttachmentController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(DocumentAttachmentController.class);
	
	@Autowired
	EntityManager entityManager;

	@Autowired
	ParameterRepository parameterRepository;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	LocationRepository locationRepository;
	
	@Autowired
	DocumentAttachmentRepository documentAttachmentRepository;

	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
		public Date deserialize(JsonElement json, Type typeOfT,
								JsonDeserializationContext context) throws JsonParseException {
			return json == null ? null : new Date(json.getAsLong());
		}
	};


	protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();
	
	@Transactional
	@PostMapping(value="/uploadFileAttachment")
	public DocumentAttachment uploadFileAttachment (  @RequestParam("file") MultipartFile file,
													  @RequestParam("name") String filename,
													  @RequestParam("attachmentType") String attachmentType,
													  @RequestParam("document") Document document){
	
		LOGGER.info("><><>< UPLOAD FILE  ATTACHMENT ><><><");
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        
        Parameter parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_PATH_FILE_ATTACHMENT);
		String pathFile = "";
		for(ParameterDetail detail:parameter.getDetails()){
			pathFile = detail.getVariable1();
		}

		LOGGER.info("><><><> path file : {} ",pathFile);
		
		DocumentAttachment documentAttachment = new DocumentAttachment();
		documentAttachment.setAttachmentType(attachmentType);
		
		document.setDocumentAttachment(new HashSet<DocumentAttachment>(){{
            add(documentAttachment);
        }});
		entityManager.persist(document);
		
		
		/* set real file name */
		String realFileName = document.getDocNumber()+ConstantVariable.UNDER_SCORE+
				ConstantVariable.DOCUMENT_STATUS_DRAFT+ConstantVariable.UNDER_SCORE+
				EncryptRealFileNameForMD5.generateEncryptFromStringToMD5(String.valueOf(documentAttachment.getId()));
		
		Date date = new Date();
		String uploadTime = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US).format(date);

		File convFile = new File(pathFile+realFileName);

		try {
		
			documentAttachment.setDocument(document);
			documentAttachment.setFileName(filename);
			documentAttachment.setRealFileName(realFileName);
			documentAttachment.setUploadTime(uploadTime);
			entityManager.persist(documentAttachment);
		
			
			file.transferTo(convFile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		
		return documentAttachment;
	}
	
	
	@GetMapping(value="/getDocumentAttachment/{id}/documentAttachments",produces = "application/json; charset=utf-8")
    public String getDocumentAttachment(@PathVariable Long id) {

		List<DocumentAttachment> documentAttachmentLs = documentAttachmentRepository.findByDocumentId(id);
		
		LOGGER.info("documentAttachmentLs = {} ",documentAttachmentLs.size());
		
		return (new JSONSerializer().exclude("*.class").include("id")
				.include("attachmentType")
				.include("fileName").serialize(documentAttachmentLs));
    }

    /* for download file by id */
	@GetMapping("/downloadFileAttachment/{id}")
	@ResponseBody
	public ResponseEntity<Resource> downloadFileAttachment(@PathVariable Long id) {

		String realFileName = "";
		String filename = "";

		DocumentAttachment documentAttachmentLs = documentAttachmentRepository.findOne(id);

		Parameter parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_PATH_FILE_ATTACHMENT);
		String pathFile = "";
		for(ParameterDetail detail:parameter.getDetails()){
			pathFile = detail.getVariable1();
		}

		if(documentAttachmentLs != null){
			realFileName = documentAttachmentLs.getRealFileName();
			filename     = documentAttachmentLs.getFileName();
		}
		return ResponseEntity
				.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+filename+"\"")
				.body(new FileSystemResource(pathFile+realFileName));
	}

	@Transactional
	@PostMapping(value="/saveSapManual")
	public DocumentAttachment saveSapManual (  @RequestParam("file") MultipartFile file,
											   @RequestParam("name") String filename,
											   @RequestParam("attachmentType") String attachmentType,
											   @RequestParam("document") Document document,
											   @RequestParam("docClear") String docClear){

		LOGGER.info("><><>< UPLOAD FILE ATTACHMENT AND SAVE SAP MANUAL ><><><");

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		Parameter parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_PATH_FILE_ATTACHMENT);
		String pathFile = "";
		for(ParameterDetail detail:parameter.getDetails()){
			pathFile = detail.getVariable1();
		}

		LOGGER.info("><><><> path file : {} ",pathFile);

		DocumentAttachment documentAttachment = new DocumentAttachment();
		documentAttachment.setAttachmentType(attachmentType);

		document.setDocumentAttachment(new HashSet<DocumentAttachment>(){{
			add(documentAttachment);
		}});
		entityManager.persist(document);


		/* set real file name */
		String realFileName = document.getDocNumber()+ConstantVariable.UNDER_SCORE+
				ConstantVariable.DOCUMENT_STATUS_DRAFT+ConstantVariable.UNDER_SCORE+
				EncryptRealFileNameForMD5.generateEncryptFromStringToMD5(String.valueOf(documentAttachment.getId()));

		Date date = new Date();
		String uploadTime = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US).format(date);

		File convFile = new File(pathFile+realFileName);

		try {

			DocumentAdvance documentAdvance = document.getDocumentAdvance();
			documentAdvance.setExternalClearingDocNumber(docClear);
			entityManager.merge(documentAdvance);

			documentAttachment.setDocument(document);
			documentAttachment.setFileName(filename);
			documentAttachment.setRealFileName(realFileName);
			documentAttachment.setUploadTime(uploadTime);
			entityManager.persist(documentAttachment);

			file.transferTo(convFile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return documentAttachment;
	}

}
