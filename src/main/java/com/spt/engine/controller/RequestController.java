package com.spt.engine.controller;

import com.google.gson.*;
import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.*;
import com.spt.engine.repository.app.AdvanceOutstandingRepository;
import com.spt.engine.repository.app.DocumentRepository;
import com.spt.engine.repository.app.ExternalMemberAccomodationDetailRepository;
import com.spt.engine.repository.app.InternalMemberAccomodationDetailRepository;
import com.spt.engine.repository.general.RequestApproverRepository;
import com.spt.engine.repository.general.RequestRepository;
import com.spt.engine.service.AbstractSAPEngineService;
import com.spt.engine.service.EmplyeeProfileService;
import com.spt.engine.service.ExpenseTranferService;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/requestCustom")
public class RequestController extends AbstractSAPEngineService {

    static final Logger LOGGER = LoggerFactory.getLogger(RequestController.class);

    @Autowired
    EntityManager entityManager;

    @Autowired
    DocumentRepository documentRepository;

    @Autowired
    RequestRepository requestRepository;

    @Autowired
    RequestApproverRepository requestApproverRepository;

    @Autowired
    AdvanceOutstandingRepository advanceOutstandingRepository;

    @Autowired
    InternalMemberAccomodationDetailRepository internalMemberAccomodationDetailRepository;

    @Autowired
    ExternalMemberAccomodationDetailRepository externalMemberAccomodationDetailRepository;

    @Autowired
    ExpenseTranferService expenseTranferService;

    @Autowired
    EmplyeeProfileService employeeProfileService;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    NotifMailController notifMailController;

    @Value("${EmedServer}")
    String urlEmedRest ;

    @Value("${EbudgetServer}")
    String urlEbudgetRest ;

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    private ThreadLocal<SimpleDateFormat> sdf = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
    };

    @SuppressWarnings("Duplicates")
    @Transactional
    @GetMapping(value="/createRequest",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String createRequest( @RequestParam("document") String documentObject,
                                  @RequestParam("processId") String processId,
                                  @RequestParam("docFlow") String docFlow,
                                  @RequestParam("lineApprove") String lineApprove){

        Document document = documentRepository.findOne(Long.valueOf(documentObject));
        List<Map> mapList = gson.fromJson(lineApprove,List.class);
        Request newRequest = new Request();
        Set<RequestApprover> requestApproverSet = new HashSet<>();

        try {

            /** create request **/
            newRequest.setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_CREATE);
            newRequest.setRequestNumber(processId);
            newRequest.setDocFlow(docFlow);
            newRequest.setDocument(document);
            entityManager.persist(newRequest);

            boolean checkFinance = false;
            /** create request approver **/
            if(0 != mapList.size()){
                for (int i = 0; i < mapList.size(); i++) {
                    RequestApprover requestApprover = new RequestApprover();
                    requestApprover.setSequence(i+1);
                    requestApprover.setApprover(mapList.get(i).get("name").toString());
                    requestApprover.setApprover(mapList.get(i).get("name_en").toString());
                    requestApprover.setActionState(mapList.get(i).get("actionRoleCode").toString());
                    requestApprover.setActionStateName(mapList.get(i).get("actionRoleName").toString());
                    requestApprover.setRequest(newRequest);
                    requestApprover.setUserNameApprover(mapList.get(i).get("userName").toString());
                    requestApprover.setCreatedDate(new Timestamp(new Date().getTime()));
                    entityManager.persist(requestApprover);
                    requestApproverSet.add(requestApprover);
                    LOGGER.info(" Action Role Code =====> {}",mapList.get(i).get("actionRoleCode").toString());
                    if((mapList.get(i).get("actionRoleCode").toString().equals(ConstantVariable.ACTION_STATE_FINANCE))){
                        checkFinance =true;
                    }

                }

                /** Set Paid State **/
                if(checkFinance){
                    RequestApprover requestApproverFinance = new RequestApprover();
                    requestApproverFinance.setActionState(ConstantVariable.ACTION_STATE_PAID);
                    requestApproverFinance.setActionStateName(ConstantVariable.ACTION_STATE_NAME_PAID);
                    requestApproverFinance.setSequence(mapList.size()+1);
                    requestApproverFinance.setRequest(newRequest);
                    requestApproverFinance.setCreatedDate(new Timestamp(new Date().getTime()));
                    entityManager.persist(requestApproverFinance);
                    requestApproverSet.add(requestApproverFinance);
                }

                newRequest.setRequestApprovers(requestApproverSet);
                entityManager.merge(newRequest);
            }

            if(!document.getDocNumber().equals(document.getTmpDocNumber())){
                /** set new document number to internal member accommodation and external member accommodation **/
                List<InternalMemberAccomodationDetail> internalMemberAccomodationDetailList = internalMemberAccomodationDetailRepository.findByDocNumber(document.getDocNumber());
                if(internalMemberAccomodationDetailList.size()>0){
                    for(InternalMemberAccomodationDetail i:internalMemberAccomodationDetailList){
                        i.setDocNumber(document.getTmpDocNumber());
                        entityManager.merge(i);
                    }
                    entityManager.flush();
                }

                List<ExternalMemberAccomodationDetail> externalMemberAccomodationDetailList = externalMemberAccomodationDetailRepository.findByDocNumber(document.getDocNumber());
                if(externalMemberAccomodationDetailList.size()>0){
                    for(ExternalMemberAccomodationDetail e:externalMemberAccomodationDetailList){
                        e.setDocNumber(document.getTmpDocNumber());
                        entityManager.merge(e);
                    }
                    entityManager.flush();
                }

                document.setDocNumber(document.getTmpDocNumber());
                document.setRequest(newRequest);
                document.setProcessId(processId);
                document.setSendDate(new Timestamp(System.currentTimeMillis()));
                document.setDocFlow(docFlow);
                document.setDocumentStatus(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_ON_PROCESS);
                entityManager.persist(document);
            }

            /** validate set next approve */
            List<RequestApprover> approverList = new ArrayList<>(newRequest.getRequestApprovers());
            Collections.sort(approverList, new Comparator<RequestApprover>() {
                @Override
                public int compare(final RequestApprover object1, final RequestApprover object2) {
                    return object1.getSequence().compareTo(object2.getSequence());
                }
            } );

            String checkSelfApprove = "N";
            String checkFlightTicket = "N";
            int i=0;
            while(i<approverList.size()){
                if(!document.getRequester().equals(approverList.get(i).getUserNameApprover())){
                    newRequest.setNextApprover(approverList.get(i).getUserNameApprover());
                    break;
                }else if(document.getRequester().equals(approverList.get(i).getUserNameApprover())
                        && approverList.get(i).getActionState().equals(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_VERIFY)){
                    approverList.get(i).setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_VERIFY);
                    approverList.get(i).setActionTime(new Timestamp(new Date().getTime()));

                    newRequest.setLastActionTime(new Timestamp(new Date().getTime()));
                    entityManager.merge(newRequest);


                }else if(document.getRequester().equals(approverList.get(i).getUserNameApprover())
                        && approverList.get(i).getActionState().equals(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_APPROVE)){
                    approverList.get(i).setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_APPROVE);
                    approverList.get(i).setActionTime(new Timestamp(new Date().getTime()));

                    newRequest.setLastActionTime(new Timestamp(new Date().getTime()));
                    entityManager.merge(newRequest);

                    /* check case line approve no one except myself */
                    if(approverList.get(i).getSequence() == approverList.size()){
                        newRequest.setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_COMPLETE);
                        entityManager.merge(newRequest);

                        document.setDocumentStatus(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE);
                        entityManager.merge(document);
                        if(document.getDocumentType().equals(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_APPROVE)){
                            checkSelfApprove = "Y";
                        }
                    }
                }

                i++;
            }

            if(null != document.getDocumentApprove() && null != document.getDocumentApprove().getDocumentApproveItem()){
                for (DocumentApproveItem documentApproveItem : document.getDocumentApprove().getDocumentApproveItem()){
                    if(null != documentApproveItem.getFlightTicket()){
                        checkFlightTicket = "Y";
                    }
                }
            }

            if("Y".equals(checkSelfApprove) && "Y".equals(checkFlightTicket)){
                notifMailController.flowMail(document.getDocNumber(),"APR","A", null);
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return (new JSONSerializer()
                .exclude("*.class")
                .include("id")
                .include("requestNumber")
                .include("requestTypeCode")
                .include("requestStatusCode")
                .include("document")
                .include("document.id")
                .include("document.docNumber")
                .include("document.documentType")
                .include("document.documentStatus")
                .include("document.requester")
                .include("document.tmpDocNumber")
                .include("document.approveType")
                .include("requestApprovers.id")
                .include("requestApprovers.requestStatusCode")
                .include("requestApprovers.rejectReasonCode")
                .include("requestApprovers.actionState")
                .include("requestApprovers.approver")
                .exclude("*")
                .serialize(newRequest));
    }

    /* update by iMilkii 2019.09.21 for get request Approver by document id */
    @GetMapping(value="/findRequestByDocument/{id}",produces = "application/json; charset=utf-8")
    public String findRequestByDocument(@PathVariable Long id) {

        LOGGER.info("><><>< findRequestApproverByDocument ><><><");
        Request request = requestRepository.findByDocument(id);

        return (new JSONSerializer().exclude("*.class")
                .include("id")
                .include("requestNumber")
                .include("requestTypeCode")
                .include("requestStatusCode")
                .include("nextApprover")
                .include("docFlow")
                .exclude("*").serialize(request));
    }

    /* update by iMilkii 2019.09.21 for get request Approver by document id */
    @GetMapping(value="/findRequestApproverByRequest/{id}",produces = "application/json; charset=utf-8")
    public String findRequestApproverByDocument(@PathVariable Long id) {

        LOGGER.info("><><>< findRequestApproverByDocument ><><><");
        List<RequestApprover> requestApproverLs = requestApproverRepository.findByRequest(id);

        return (new JSONSerializer().exclude("*.class")
                .include("id")
                .include("requestStatusCode")
                .include("actionReasonCode")
                .include("actionReasonDetail")
                .include("actionState")
                .include("approver")
                .include("actionStateName")
                .include("sequence")
                .include("userNameApprover")
                .include("approver_en")
                .exclude("*").serialize(requestApproverLs));
    }

    /* update by iMilkii 2017.09.21 for cancel document */
    @Transactional
    @GetMapping(value="/cancelRequest",produces = "application/json; charset=utf-8")
    public String cancelRequest( @RequestParam("document") String documentObject,
                                 @RequestParam("result") String result){

        Document document = documentRepository.findOne(Long.valueOf(documentObject));
        Request request = requestRepository.findByDocument(document.getId());
        Set<RequestApprover> requestApproverSet = new HashSet<>(request.getRequestApprovers());

        LOGGER.info("><><>< CANCEL REQUEST ><><>< ");
        LOGGER.info("><><>< documentObject : {} ",documentObject);
        LOGGER.info("><><>< result  : {} ",result);
        try {
            /** update request status is cancel **/
            if(result.equals(ConstantVariable.FLOW_STATUS_COMPLETE)){

                request.setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_CANCEL);
                request.setDocument(document);
                request.setNextApprover(null);
                entityManager.merge(request);
                entityManager.flush();
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return (new JSONSerializer()
                .exclude("*.class")
                .include("id")
                .include("requestNumber")
                .include("requestTypeCode")
                .include("requestStatusCode")
                .include("document")
                .include("document.id")
                .include("document.docNumber")
                .include("document.documentType")
                .include("document.documentStatus")
                .include("document.requester")
                .include("document.tmpDocNumber")
                .include("document.approveType")
                .include("requestApprovers.id")
                .include("requestApprovers.requestStatusCode")
                .include("requestApprovers.rejectReasonCode")
                .include("requestApprovers.actionState")
                .include("requestApprovers.approver")
                .exclude("*")
                .serialize(request));
    }

    /** update by iMilkii 2019.09.21 for approve request **/

    @Transactional
    @GetMapping(value="/approveRequest",produces = "application/json; charset=utf-8")
    public String approveRequest( @RequestParam("document") String documentObject,
                                  @RequestParam("state") String state,
                                  @RequestParam("flowActive") String flowActive,
                                  @RequestParam("actionReason") String actionReason,
                                  @RequestParam("actionReasonDetail") String actionReasonDetail){

        Document document = documentRepository.findOne(Long.valueOf(documentObject));
        Request request = requestRepository.findByDocument(document.getId());
        List<RequestApprover> approverLs = new ArrayList<>(request.getRequestApprovers());
        String checkCaseAutoPost = "N";

        LOGGER.info("><><>< APPROVE REQUEST ><><>< ");
        LOGGER.info("><><>< documentObject : {} ",documentObject);
        LOGGER.info("><><>< state  : {} ",state);

        Collections.sort(approverLs, new Comparator<RequestApprover>() {
            @Override
            public int compare(final RequestApprover object1, final RequestApprover object2) {
                return object1.getSequence().compareTo(object2.getSequence());
            }
        } );

        try {
            /** update request status is cancel **/
            for(int i=0;i<approverLs.size();i++){
                if(state.equals(ConstantVariable.JBPM_STATE_ADMIN)){
                    System.out.println("----------- State Admin -----------");
                    if(approverLs.get(i).getActionStateName().equals(ConstantVariable.ROLE_OFFICE_ADMIN)){
                        if(flowActive.equals(ConstantVariable.FLOW_STATUS_ACTIVE)){
                            System.out.println("1. Flow Active ");
                            request.setNextApprover(approverLs.get(i).getUserNameApprover());
                            entityManager.merge(request);
                            entityManager.flush();
                        }else{
                            System.out.println("2. Flow Inactive ");
                            approverLs.get(i).setActionTime(new Timestamp(new Date().getTime()));
                            approverLs.get(i).setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE);
                            entityManager.merge(approverLs.get(i));
                            entityManager.flush();

                            request.setNextApprover(null);
                            request.setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE);
                            entityManager.merge(request);
                            entityManager.flush();

                            document.setDocumentStatus(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE);
                            entityManager.merge(document);
                            entityManager.flush();
                        }
                    }else{
                        System.out.println("3. Not Role Admin ");
                        setNextApprover(approverLs,state,actionReason,actionReasonDetail);
                    }
                }
                else if(state.equals(approverLs.get(i).getActionStateName())){
                    System.out.println("----------- Not State Admin -----------");
                    if(flowActive.equals(ConstantVariable.FLOW_STATUS_ACTIVE)){
                        System.out.println("1. Flow Active ");
                        if(state.equals((ConstantVariable.JBPM_STATE_FINANCE))){
                            System.out.println("2. State Finance ");
                            LOGGER.info("><><>< BEFORE CASE");
                            /* send to SAP modify by iMilkii 2017.10.23 */
                            if(document.getDocumentType().equals(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_APPROVE) || document.getDocumentType().equals(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_ADVANCE)){
                                LOGGER.info("><><>< CASE SEND TO SAP");
                                checkCaseAutoPost = "Y";
                                Map param = new HashMap();
                                param.put("id",document.getId());

                                String result = expenseTranferService.advanceTranfer(param);
                                DocumentAdvance documentAdvance = document.getDocumentAdvance();

                                List<Map<String,String>> returnList = gson.fromJson(result, List.class);
                                //Example:
                                //[{"sapDocNumber":"","returnClass":"ZCCA1","returnMessage":"FI - ระบุ Branch Code เป็นค่า NVAT หรื่อตัวเลข 5 หลักเท่านั้น","returnNumber":"036","returnType":"E"}]
                                for(Map<String,String> returnData :returnList){
                                    if("S".equals(returnData.get("returnType"))){
                                        documentAdvance.setExternalDocNumber(String.valueOf(returnData.get("sapDocNumber")));
                                        document.setPostingDate(new Timestamp(new Date().getTime()));
                                        entityManager.merge(document);
                                        entityManager.merge(documentAdvance);

                                        document.setInterfaceOutDate(new Timestamp(new Date().getTime()));
                                    }else if("E".equals(returnData.get("returnType"))){
                                        LOGGER.error(" : {} ",returnData.get("returnMessage"));
                                    }
                                }
                                String checkFlightTicket = "N";
                                if(null != document.getDocumentApprove() && null != document.getDocumentApprove().getDocumentApproveItem()){
                                    for (DocumentApproveItem documentApproveItem : document.getDocumentApprove().getDocumentApproveItem()){
                                        if(null != documentApproveItem.getFlightTicket()){
                                            checkFlightTicket = "Y";
                                        }
                                    }
                                }

                                if("Y".equals(checkFlightTicket)){
                                    notifMailController.flowMail(document.getDocNumber(),"APR","A", null);
                                }
                            }
                        }

                        request.setNextApprover(approverLs.get(i).getUserNameApprover());
                        entityManager.merge(request);
                        entityManager.flush();
                    }else{

                        if(!state.equals(ConstantVariable.JBPM_STATE_FINANCE)){
                            System.out.println("3. Not State Finance ");
                            approverLs.get(i).setActionTime(new Timestamp(new Date().getTime()));
                            approverLs.get(i).setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE);
                            entityManager.merge(approverLs.get(i));
                            entityManager.flush();

                            request.setNextApprover(null);
                            request.setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE);
                            entityManager.merge(request);
                            entityManager.flush();

                            document.setDocumentStatus(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE);
                            entityManager.merge(document);
                            entityManager.flush();

                            if(document.getDocumentType().equals(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_APPROVE) && document.getDocumentApprove().getDocumentApproveCostEstimate().size()>0){
                                String jsonFormat = convertToEBudget(document);

                                MediaType mediaType = new MediaType("application", "json", Charset.forName("UTF-8"));
                                HttpHeaders headers = new HttpHeaders();
                                headers.setContentType(mediaType);

                                HttpEntity<String> entity = new HttpEntity<String>(jsonFormat, headers);
                                ResponseEntity<String> reponseEntity = restTemplate.exchange(urlEbudgetRest+"/ebgdocuments/processDocumentWorkflow", HttpMethod.POST, entity, String.class);
                                LOGGER.info("============ Result  :   {} ",reponseEntity.getBody());
                            }
                        }else{
                            System.out.println("4. Else ");
                            approverLs.get(i).setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE);
                            approverLs.get(i).setActionTime(new Timestamp(new Date().getTime()));
                            entityManager.merge(approverLs.get(i));
                            entityManager.flush();

                            request.setNextApprover(ConstantVariable.ACTION_STATE_NAME_PAID);
                            entityManager.merge(request);
                            entityManager.flush();

                        }
                    }
                }else{
                    setNextApprover(approverLs,state,actionReason,actionReasonDetail);
                }
            }

            if("Y".equals(checkCaseAutoPost)){
                addAccountToDocumentAdvance(approverLs,document,request);
            }

            Date date = new Date();

            request.setLastActionTime(new Timestamp(date.getTime()));
            request.setDocument(document);
            entityManager.merge(request);
            entityManager.flush();

        }catch (Exception e){
            e.printStackTrace();
        }

        return (new JSONSerializer()
                .exclude("*.class")
                .include("id")
                .include("requestNumber")
                .include("requestTypeCode")
                .include("requestStatusCode")
                .include("document")
                .include("document.id")
                .include("document.docNumber")
                .include("document.documentType")
                .include("document.documentStatus")
                .include("document.requester")
                .include("document.tmpDocNumber")
                .include("document.approveType")
                .include("requestApprovers.id")
                .include("requestApprovers.requestStatusCode")
                .include("requestApprovers.rejectReasonCode")
                .include("requestApprovers.actionState")
                .include("requestApprovers.approver")
                .exclude("*")
                .serialize(request));
    }

    public void addAccountToDocumentAdvance(List<RequestApprover> approverList,Document document,Request request){
        /** find Account **/
        ResponseEntity<String> accountByPaPsa = employeeProfileService.findAccountByPaPsaAndKeySearch(document.getCompanyCode()+"-"+document.getPsa(), document.getRequester());
        Map accountByPaPsaMap = gson.fromJson(accountByPaPsa.getBody(), Map.class);
        String userNameAccount = accountByPaPsaMap.get("profile").toString().split(":")[1].split("-")[0];
        ResponseEntity<String> accountData = employeeProfileService.findEmployeeProfileByKeySearch(userNameAccount);
        Map accountMap = gson.fromJson(accountData.getBody(), Map.class);
        List accountList = (List) accountMap.get("restBodyList");
        accountMap = (Map) accountList.get(0);
        String accountName = accountMap.get("FOA").toString() + " " + accountMap.get("FNameTH").toString() + " " + accountMap.get("LNameTH").toString();
        String accountNameEN = accountMap.get("NameEN").toString();

        RequestApprover requestApproverACC = new RequestApprover();
        requestApproverACC.setActionState("ACC");
        requestApproverACC.setActionStateName("Account");
        requestApproverACC.setActionTime(new Timestamp(new Date().getTime()));
        requestApproverACC.setCreatedDate(new Timestamp(new Date().getTime()));
        requestApproverACC.setApprover(accountNameEN);
        requestApproverACC.setRequestStatusCode("ACC");
        requestApproverACC.setSequence(3);
        requestApproverACC.setUserNameApprover(userNameAccount.toLowerCase());
        requestApproverACC.setRequest(request);
        entityManager.persist(requestApproverACC);


        for (RequestApprover approver : approverList){
            if("FNC".equals(approver.getActionState())){
                approver.setSequence(4);
            }
            if("PAID".equals(approver.getActionState())){
                approver.setSequence(5);
            }

            entityManager.merge(approver);
        }


    }

    /** update by iMilkii 2019.09.21 for reject request **/

    @SuppressWarnings("Duplicates")
    @Transactional
    @GetMapping(value="/rejectRequest",produces = "application/json; charset=utf-8")
    public String rejectRequest( @RequestParam("document") String documentObject,
                                  @RequestParam("state") String state,
                                  @RequestParam("flowActive") String flowActive,
                                  @RequestParam("actionReason") String actionReason,
                                 @RequestParam("actionReasonDetail") String actionReasonDetail){

        Document document = documentRepository.findOne(Long.valueOf(documentObject));
        Request request = requestRepository.findByDocument(document.getId());
        List<RequestApprover> approverLs = new ArrayList<>(request.getRequestApprovers());
        SimpleDateFormat sdfYYYYMMDD = new SimpleDateFormat("YYYY-MM-dd");
        LOGGER.info("><><>< REJECT REQUEST ><><>< ");
        LOGGER.info("><><>< documentObject : {} ",documentObject);
        LOGGER.info("><><>< state  : {} ",state);
        LOGGER.info("><><>< flowActive  : {} ",flowActive);
        try {
            for(RequestApprover approver : approverLs){

                if(state.equals(ConstantVariable.JBPM_STATE_VERIFY) && approver.getActionStateName().equals(ConstantVariable.ROLE_OFFICE_VERIFY)){
                    setNextApproverReject(request,approver,ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_REJECT,actionReason,actionReasonDetail,flowActive);

                } else if(state.equals(ConstantVariable.JBPM_STATE_APPROVE) && approver.getActionStateName().equals(ConstantVariable.JBPM_STATE_APPROVE) ) {
                    setNextApproverReject(request,approver,ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_REJECT,actionReason,actionReasonDetail,flowActive);

                }else if(state.equals(ConstantVariable.JBPM_STATE_ADMIN) && approver.getActionStateName().equals(ConstantVariable.ROLE_OFFICE_ADMIN)){

                    setNextApproverReject(request,approver,ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_REJECT,actionReason,actionReasonDetail,flowActive);

                }else if(state.equals(ConstantVariable.JBPM_STATE_ACCOUNT) && approver.getActionStateName().equals(ConstantVariable.JBPM_STATE_ACCOUNT)){

                    setNextApproverReject(request,approver,ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_REJECT,actionReason,actionReasonDetail,flowActive);

                }else if(state.equals(ConstantVariable.JBPM_STATE_FINANCE) && approver.getActionStateName().equals(ConstantVariable.JBPM_STATE_FINANCE)){
                    setNextApproverReject(request,approver,ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_REJECT,actionReason,actionReasonDetail,flowActive);
                }
            }

            if(flowActive.equals(ConstantVariable.FLOW_STATUS_INACTIVE)){
                document.setDocumentStatus(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_REJECT);
            }
            entityManager.merge(document);
            entityManager.flush();

            request.setDocument(document);
            entityManager.merge(request);
            entityManager.flush();

            if(null!=document.getExternalDocReference()){

                Map<String, String> map = new HashMap<>();
                map.put("docNumber",document.getExternalDocReference());
                map.put("remark",actionReasonDetail);
                map.put("employeeId",document.getPersonalId());

                LOGGER.info("Reject to Emed 540  :   {}",map);

                MediaType mediaType = new MediaType("application", "json", Charset.forName("UTF-8"));
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(mediaType);

                HttpEntity<String> entity = new HttpEntity<String>(gson.toJson(map), headers);
                ResponseEntity<String> reponseEntity = restTemplate.exchange(urlEmedRest+"/common/json/rejectfromWorkflow", HttpMethod.POST, entity, String.class);
                LOGGER.info("============ Result  :   {} ",reponseEntity.getBody());
            }

            List<String> reverseDocList = new ArrayList<>();
            String checkDocument = "";

            if(null != document.getDocumentAdvance() && null != document.getDocumentAdvance().getExternalDocNumber()
                    && null == document.getDocumentAdvance().getExternalPaymentDocNumber() && null == document.getDocumentAdvance().getExternalClearingDocNumber()){
                //reverse advance
                reverseDocList.add(document.getDocumentAdvance().getExternalDocNumber());
                checkDocument = "A";

            }

            if(null != document.getDocumentExpense() && null != document.getDocumentExpense().getDocumentExpenseItems()){
                //reverse expense
                List<DocumentExpenseItem> documentExpenseItemList = new ArrayList<>(document.getDocumentExpense().getDocumentExpenseItems());
                for (DocumentExpenseItem documentExpenseItem : documentExpenseItemList){
                    if(null != documentExpenseItem.getExternalDocNumber() && null == documentExpenseItem.getExternalPaymentDocNumber() && null == documentExpenseItem.getExternalClearingDocNumber())
                    reverseDocList.add(documentExpenseItem.getExternalDocNumber());
                }
                checkDocument = "E";
            }


            Map<String,Map<String,List<Map<String,String>>>> mapReverse = new HashMap();
            Map<String,List<Map<String,String>>> mapReverseDocSet = new HashMap();
            List<Map<String,String>> itemReverseList = new ArrayList<>();
            String urlParam = "/reverseDoc";
            if(reverseDocList.size()>0){
                String cancelDoc = "";
                for (String doc : reverseDocList){
                    Map<String,String> mapReverseDoc = new HashMap<>();
                    mapReverseDoc.put("ACC_DOCNO",String.valueOf(doc));
                    mapReverseDoc.put("COMP_CODE",String.valueOf(document.getCompanyCode()));
                    mapReverseDoc.put("FISC_YEAR","2019");
                    mapReverseDoc.put("PSTNG_DATE",sdfYYYYMMDD.format(document.getPostingDate()));
                    itemReverseList.add(mapReverseDoc);
                }

                mapReverseDocSet.put("item",itemReverseList);
                mapReverse.put("I_ITAB",mapReverseDocSet);

                String jsonData =gson.toJson(mapReverse,Map.class);
                LOGGER.info("==== Reverse  :   {}",jsonData);
                ResponseEntity<String> responseData = postJsonData(urlParam,jsonData);
                Map<String,Map<String,String>> returnMap = gson.fromJson(responseData.getBody(), Map.class);

                if(null != returnMap.get("T_RETURN")){
                    Map mapT_Return = returnMap.get("T_RETURN");

                    if(mapT_Return.get("item") instanceof Map){
                        Map<String,String> map = (Map<String, String>) mapT_Return.get("item");
                        if("S".equals(map.get("TYPE"))){
                            try {
                                cancelDoc = String.valueOf(map.get("MESSAGE").split("Document ")[1].split(" ")[0]);

                                if("A".equals(checkDocument)){
                                    document.getDocumentAdvance().setExternalCancelDocNumber(cancelDoc);
                                    entityManager.merge(document);
                                }else{
                                    List<DocumentExpenseItem> documentExpenseItemList = new ArrayList<>(document.getDocumentExpense().getDocumentExpenseItems());
                                    for (DocumentExpenseItem documentExpenseItem : documentExpenseItemList){
                                        documentExpenseItem.setExternalCancelDocNumber(cancelDoc);
                                        entityManager.merge(documentExpenseItem);
                                    }
                                }

                            } catch (Exception e) { /* Ignore */}
                        }
                    }else if(mapT_Return.get("item") instanceof List){
                        List<Map<String,String>> map = (List<Map<String,String>>) mapT_Return.get("item");
                        for (Map<String,String> m : map){
                            if("S".equals(m.get("TYPE"))){
                                try {
                                    cancelDoc = String.valueOf(m.get("MESSAGE").split("Document ")[1].split(" ")[0]);
                                    if("A".equals(checkDocument)){
                                        document.getDocumentAdvance().setExternalCancelDocNumber(cancelDoc);
                                        entityManager.merge(document);
                                    }else{
                                        List<DocumentExpenseItem> documentExpenseItemList = new ArrayList<>(document.getDocumentExpense().getDocumentExpenseItems());
                                        for (DocumentExpenseItem documentExpenseItem : documentExpenseItemList){
                                            documentExpenseItem.setExternalCancelDocNumber(cancelDoc);
                                            entityManager.merge(documentExpenseItem);
                                        }
                                    }
                                } catch (Exception e) { /* Ignore */}
                            }
                        }
                    }
                }

                entityManager.flush();
            }


        }catch (Exception e){
            e.printStackTrace();
        }

        return (new JSONSerializer()
                .exclude("*.class")
                .include("id")
                .include("requestNumber")
                .include("requestTypeCode")
                .include("requestStatusCode")
                .include("document")
                .include("document.id")
                .include("document.docNumber")
                .include("document.documentType")
                .include("document.documentStatus")
                .include("document.requester")
                .include("document.tmpDocNumber")
                .include("document.approveType")
                .include("requestApprovers.id")
                .include("requestApprovers.requestStatusCode")
                .include("requestApprovers.rejectReasonCode")
                .include("requestApprovers.actionState")
                .include("requestApprovers.approver")
                .exclude("*")
                .serialize(request));
    }


    public void setNextApprover(List<RequestApprover> approverLs,String statusCode,String actionReason,String actionReasonDetail){

        LOGGER.info("><><>< STATUS CODE : {}",statusCode);
        Integer sequence = 0;
        for(RequestApprover approver : approverLs){
            if(statusCode.equals(ConstantVariable.JBPM_STATE_ADMIN)){
                if(approver.getActionStateName().equals(ConstantVariable.ROLE_OFFICE_ADMIN)){
                    sequence = approver.getSequence();
                }
            }
            else if(approver.getActionStateName().equals(statusCode)){
                sequence = approver.getSequence();
            }
        }

        Collections.sort(approverLs, new Comparator<RequestApprover>() {
            @Override
            public int compare(final RequestApprover object1, final RequestApprover object2) {
                return object1.getSequence().compareTo(object2.getSequence());
            }
        } );

        for(int i=0;i<(sequence-1);i++){
            LOGGER.info("============= ACTION STATE    :    {}",approverLs.get(i).getActionState());
            if(approverLs.get(i).getActionState().equals(ConstantVariable.ACTION_STATE_VERIFY)){
                setActionReason(approverLs.get(i),ConstantVariable.ACTION_STATE_VERIFY,actionReason,actionReasonDetail);
            }else if(approverLs.get(i).getActionState().equals(ConstantVariable.ACTION_STATE_APPROVE)){
                setActionReason(approverLs.get(i),ConstantVariable.ACTION_STATE_APPROVE,actionReason,actionReasonDetail);
            }else if(approverLs.get(i).getActionState().equals(ConstantVariable.ACTION_STATE_ACCOUNT)){
                setActionReason(approverLs.get(i),ConstantVariable.ACTION_STATE_ACCOUNT,actionReason,actionReasonDetail);
            }else if(approverLs.get(i).getActionState().equals(ConstantVariable.ACTION_STATE_FINANCE)){
                setActionReason(approverLs.get(i),ConstantVariable.ACTION_STATE_FINANCE,actionReason,actionReasonDetail);
            }else if(approverLs.get(i).getActionState().equals(ConstantVariable.ACTION_STATE_HR)){
                setActionReason(approverLs.get(i),ConstantVariable.ACTION_STATE_HR,actionReason,actionReasonDetail);
            }else if(approverLs.get(i).getActionState().equals(ConstantVariable.ACTION_STATE_ADMIN)){
                setActionReason(approverLs.get(i),ConstantVariable.ACTION_STATE_ADMIN,actionReason,actionReasonDetail);
            }
        }
    }

    public void setActionReason(RequestApprover approver,String requestStatus,String actionReason,String actionReasonDetail){

        approver.setRequestStatusCode(requestStatus);
        if(null == approver.getActionTime()){
            approver.setActionTime(new Timestamp(new Date().getTime()));
        }

        if(!actionReason.isEmpty()){
            approver.setActionReasonCode(actionReason);
        }
        if(!actionReasonDetail.isEmpty()){
            approver.setActionReasonDetail(actionReasonDetail);
        }

        entityManager.merge(approver);
        entityManager.flush();
    }

    public void setNextApproverReject(Request request,RequestApprover approver,String statusCode,String actionReason,String actionReasonDetail,String flowActive){

        LOGGER.info("><><>< setNextApproverReject ><>< ");
        LOGGER.info("><><>< action state : {}  ",approver.getActionState());
        LOGGER.info("><><>< username : {}  ",approver.getUserNameApprover());
        LOGGER.info("><><>< sequence : {}  ",approver.getSequence());
        List<RequestApprover> approverList = new ArrayList<>(request.getRequestApprovers());

        Collections.sort(approverList, new Comparator<RequestApprover>() {
            @Override
            public int compare(final RequestApprover object1, final RequestApprover object2) {
                return object1.getSequence().compareTo(object2.getSequence());
            }
        } );

        Integer countSequence = approver.getSequence();

        for(RequestApprover requestApprover : approverList){

            if(requestApprover.getSequence() < countSequence && flowActive.equals(ConstantVariable.FLOW_STATUS_INACTIVE)) {
                requestApprover.setRequestStatusCode(requestApprover.getActionState());
                requestApprover.setActionTime(new Timestamp(new Date().getTime()));
                if (!actionReason.isEmpty()) {
                    requestApprover.setActionReasonCode(actionReason);
                }
                if(!actionReasonDetail.isEmpty()){
                    requestApprover.setActionReasonDetail(actionReasonDetail);
                }

                requestApprover.setRequest(request);
                entityManager.merge(requestApprover);
                entityManager.flush();
            }

            else if(requestApprover.getSequence() < countSequence && flowActive.equals(ConstantVariable.FLOW_STATUS_ACTIVE)){
                requestApprover.setRequestStatusCode(statusCode);
                requestApprover.setActionTime(new Timestamp(new Date().getTime()));
                if (!actionReason.isEmpty()) {
                    requestApprover.setActionReasonCode(actionReason);
                }
                if(!actionReasonDetail.isEmpty()){
                    requestApprover.setActionReasonDetail(actionReasonDetail);
                }

                requestApprover.setRequest(request);
                entityManager.merge(requestApprover);
                entityManager.flush();
            }

            else if(requestApprover.getSequence() == countSequence && flowActive.equals(ConstantVariable.FLOW_STATUS_INACTIVE)){
                requestApprover.setRequestStatusCode(statusCode);
                requestApprover.setActionTime(new Timestamp(new Date().getTime()));

                if (!actionReason.isEmpty()) {
                    requestApprover.setActionReasonCode(actionReason);
                }
                if(!actionReasonDetail.isEmpty()){
                    requestApprover.setActionReasonDetail(actionReasonDetail);
                }

                request.setRequestStatusCode(statusCode);
                request.setNextApprover(null);
                request.setLastActionTime(new Timestamp(new Date().getTime()));
                entityManager.merge(request);

                requestApprover.setRequest(request);
                entityManager.merge(requestApprover);
                entityManager.flush();
            }
            else if(flowActive.equals(ConstantVariable.FLOW_STATUS_ACTIVE) && approver.getSequence().equals(requestApprover.getSequence())){
                request.setNextApprover(requestApprover.getUserNameApprover());
                entityManager.merge(request);
            }
        }


    }

    @Transactional
    @GetMapping(value="/resendFlow",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String resendFlow( @RequestParam("document") String documentObject,
                                 @RequestParam("processId") String processId,
                                 @RequestParam("docFlow") String docFlow,
                                 @RequestParam("lineApprove") String lineApprove){

        Document document = documentRepository.findOne(Long.valueOf(documentObject));
        List<Map> mapList = gson.fromJson(lineApprove,List.class);
        Set<RequestApprover> requestApproverSet = new HashSet<>();
        Request newRequest = new Request();
        try {
            /** remove old request **/
            Request requestOld = document.getRequest();
            if(null != requestOld){
                requestOld.setDocument(null);
                entityManager.merge(requestOld);
                entityManager.flush();
                entityManager.remove(requestOld);
            }

            /** create new request **/
            newRequest.setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_CREATE);
            newRequest.setRequestNumber(processId);
            newRequest.setDocFlow(docFlow);
            newRequest.setDocument(document);
            entityManager.persist(newRequest);

            boolean checkFinance = false;
            /** create request approver **/
            if(0 != mapList.size()){
                for (int i = 0; i < mapList.size(); i++) {
                    RequestApprover requestApprover = new RequestApprover();
                    requestApprover.setSequence(i+1);
                    requestApprover.setApprover(mapList.get(i).get("name").toString());
                    requestApprover.setApprover_en(mapList.get(i).get("name_en").toString());
                    requestApprover.setActionState(mapList.get(i).get("actionRoleCode").toString());
                    requestApprover.setActionStateName(mapList.get(i).get("actionRoleName").toString());
                    requestApprover.setRequest(newRequest);
                    requestApprover.setUserNameApprover(mapList.get(i).get("userName").toString());
                    requestApprover.setCreatedDate(new Timestamp(new Date().getTime()));
                    entityManager.persist(requestApprover);
                    requestApproverSet.add(requestApprover);
                    LOGGER.info(" Action Role Code =====> {}",mapList.get(i).get("actionRoleCode").toString());
                    if((mapList.get(i).get("actionRoleCode").toString().equals(ConstantVariable.ACTION_STATE_FINANCE))){
                        checkFinance =true;
                    }

                }

                /** Set Paid State **/
                if(checkFinance){
                    RequestApprover requestApproverFinance = new RequestApprover();
                    requestApproverFinance.setActionState(ConstantVariable.ACTION_STATE_PAID);
                    requestApproverFinance.setActionStateName(ConstantVariable.ACTION_STATE_NAME_PAID);
                    requestApproverFinance.setSequence(mapList.size()+1);
                    requestApproverFinance.setRequest(newRequest);
                    requestApproverFinance.setCreatedDate(new Timestamp(new Date().getTime()));
                    entityManager.persist(requestApproverFinance);
                    requestApproverSet.add(requestApproverFinance);
                }

                newRequest.setRequestApprovers(requestApproverSet);
                entityManager.merge(newRequest);
            }

            /** set new document number to internal member accommodation and external member accommodation **/
            List<InternalMemberAccomodationDetail> internalMemberAccomodationDetailList = internalMemberAccomodationDetailRepository.findByDocNumber(document.getDocNumber());
            if(internalMemberAccomodationDetailList.size()>0){
                for(InternalMemberAccomodationDetail i:internalMemberAccomodationDetailList){
                    i.setDocNumber(document.getTmpDocNumber());
                    entityManager.merge(i);
                }
                entityManager.flush();
            }

            List<ExternalMemberAccomodationDetail> externalMemberAccomodationDetailList = externalMemberAccomodationDetailRepository.findByDocNumber(document.getDocNumber());
            if(externalMemberAccomodationDetailList.size()>0){
                for(ExternalMemberAccomodationDetail e:externalMemberAccomodationDetailList){
                    e.setDocNumber(document.getTmpDocNumber());
                    entityManager.merge(e);
                }
                entityManager.flush();
            }

            document.setDocNumber(document.getTmpDocNumber());
            document.setRequest(newRequest);
            document.setProcessId(processId);
            document.setSendDate(new Timestamp(new Date().getTime()));
            document.setDocFlow(docFlow);
            document.setDocumentStatus(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_ON_PROCESS);
            entityManager.merge(document);


            /** validate set next approve */
            List<RequestApprover> approverList = new ArrayList<>(newRequest.getRequestApprovers());
            Collections.sort(approverList, new Comparator<RequestApprover>() {
                @Override
                public int compare(final RequestApprover object1, final RequestApprover object2) {
                    return object1.getSequence().compareTo(object2.getSequence());
                }
            } );

            int i=0;
            while(i<approverList.size()){
                if(!document.getRequester().equals(approverList.get(i).getUserNameApprover())){
                    newRequest.setNextApprover(approverList.get(i).getUserNameApprover());
                    break;
                }else if(document.getRequester().equals(approverList.get(i).getUserNameApprover())
                        && approverList.get(i).getActionState().equals(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_VERIFY)){
                    approverList.get(i).setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_VERIFY);
                    approverList.get(i).setActionTime(new Timestamp(new Date().getTime()));

                    newRequest.setLastActionTime(new Timestamp(new Date().getTime()));
                    entityManager.merge(newRequest);


                }else if(document.getRequester().equals(approverList.get(i).getUserNameApprover())
                        && approverList.get(i).getActionState().equals(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_APPROVE)){
                    approverList.get(i).setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_APPROVE);
                    approverList.get(i).setActionTime(new Timestamp(new Date().getTime()));

                    newRequest.setLastActionTime(new Timestamp(new Date().getTime()));
                    entityManager.merge(newRequest);

                    /* check case line approve no one except myself */
                    if(approverList.get(i).getSequence() == approverList.size()){
                        newRequest.setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_COMPLETE);
                        entityManager.merge(newRequest);

                        document.setDocumentStatus(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE);
                        entityManager.merge(document);
                    }
                }

                i++;
            }
            entityManager.flush();

        }catch (Exception e){
            e.printStackTrace();
        }

        return (new JSONSerializer()
                .exclude("*.class")
                .include("id")
                .include("requestNumber")
                .include("requestTypeCode")
                .include("requestStatusCode")
                .include("document")
                .include("document.id")
                .include("document.docNumber")
                .include("document.documentType")
                .include("document.documentStatus")
                .include("document.requester")
                .include("document.tmpDocNumber")
                .include("document.approveType")
                .include("requestApprovers.id")
                .include("requestApprovers.requestStatusCode")
                .include("requestApprovers.rejectReasonCode")
                .include("requestApprovers.actionState")
                .include("requestApprovers.approver")
                .exclude("*")
                .serialize(newRequest));
    }

    @Transactional
    @GetMapping(value="/rejectRequestForEmed",produces = "application/json; charset=utf-8")
    public String rejectRequest( @RequestParam("docNumber") String docNumber,
                                 @RequestParam("remark") String remark,
                                 @RequestParam("from") String from){

        Document document = documentRepository.findByDocNumber(docNumber);
        Request request = requestRepository.findByDocument(document.getId());
        List<RequestApprover> approverLs = new ArrayList<>(request.getRequestApprovers());

        LOGGER.info("><><>< REJECT REQUEST FOR EMED ><><>< ");
        LOGGER.info("><><>< docNumber : {} ",docNumber);
        LOGGER.info("><><>< remark  : {} ",remark);
        try {

            request.setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_REJECT);
            request.setNextApprover(null);
            request.setLastActionTime(new Timestamp(new Date().getTime()));
            entityManager.merge(request);

            Collections.sort(approverLs, new Comparator<RequestApprover>() {
                @Override
                public int compare(final RequestApprover object1, final RequestApprover object2) {
                    return object1.getSequence().compareTo(object2.getSequence());
                }
            } );

            for(RequestApprover approver : approverLs){
                if(null == approver.getActionTime()){
                    approver.setActionTime(new Timestamp(new Date().getTime()));
                    approver.setActionReasonDetail(remark);
                    approver.setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_REJECT);
                    approver.setRequest(request);
                    entityManager.merge(approver);
                    break;
                }

            }

            document.setDocumentStatus(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_REJECT);

            entityManager.merge(document);
            entityManager.flush();

            request.setDocument(document);
            entityManager.merge(request);
            entityManager.flush();

            if(!"EMED".equalsIgnoreCase(from)) {
                /** reject to emed **/
                Map<String, String> map = new HashMap<>();
                map.put("docNumber",document.getExternalDocReference());
                map.put("remark","");
                map.put("employeeId",document.getPersonalId());

                LOGGER.info("Reject to emed  936 :  {}",map);

                MediaType mediaType = new MediaType("application", "json", Charset.forName("UTF-8"));
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(mediaType);

                HttpEntity<String> entity = new HttpEntity<String>(gson.toJson(map), headers);
                ResponseEntity<String> reponseEntity = restTemplate.exchange(urlEmedRest+"/common/json/rejectfromWorkflow", HttpMethod.POST, entity, String.class);
                LOGGER.info("============ Result  :   {} ",reponseEntity.getBody());
            }


        }catch (Exception e){
            e.printStackTrace();
        }

        return (new JSONSerializer()
                .exclude("*.class")
                .include("id")
                .include("requestNumber")
                .include("requestTypeCode")
                .include("requestStatusCode")
                .include("document")
                .include("document.id")
                .include("document.docNumber")
                .include("document.documentType")
                .include("document.documentStatus")
                .include("document.requester")
                .include("document.tmpDocNumber")
                .include("document.approveType")
                .include("requestApprovers.id")
                .include("requestApprovers.requestStatusCode")
                .include("requestApprovers.rejectReasonCode")
                .include("requestApprovers.actionState")
                .include("requestApprovers.approver")
                .exclude("*")
                .serialize(request));
    }

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    public String convertToEBudget(Document document){
        Map mapHeader = new HashMap();


        mapHeader.put("docNumber",document.getDocNumber());
        mapHeader.put("requester",document.getRequester());
        mapHeader.put("companyCode",document.getCompanyCode());
        mapHeader.put("psa",document.getPsa());
        mapHeader.put("createdBy",document.getCreatedBy());
        mapHeader.put("costCenterCode",document.getCostCenterCode());

        Map mapEbgDocPurpose = null;
        List<Map<String,String>> listEbgDocPurpose = new ArrayList<Map<String,String>>();
        for(DocumentApproveCostEstimate i:document.getDocumentApprove().getDocumentApproveCostEstimate()){
            mapEbgDocPurpose = new HashMap();
            mapEbgDocPurpose.put("glCode",i.getGl());
            mapEbgDocPurpose.put("glDesc",i.getDescriptionTh());
            mapEbgDocPurpose.put("purposeAmount",i.getAmount().toString());
            mapEbgDocPurpose.put("statusType","I");
            listEbgDocPurpose.add(mapEbgDocPurpose);
        }

        mapHeader.put("ebgDocPurpose",listEbgDocPurpose);

        Map mapRequestApprover = null;
        List<Map<String,String>> listRequestApprover = new ArrayList<Map<String,String>>();
        List<RequestApprover> requestApproverList = new ArrayList<>(document.getRequest().getRequestApprovers());
        Collections.sort(requestApproverList, new Comparator<RequestApprover>() {
            @Override
            public int compare(final RequestApprover object1, final RequestApprover object2) {
                return object1.getSequence().compareTo(object2.getSequence());
            }
        } );
        for(RequestApprover r : requestApproverList){
            if("VRF".equalsIgnoreCase(r.getActionState()) || "APR".equalsIgnoreCase(r.getActionState())){
                mapRequestApprover = new HashMap();
                mapRequestApprover.put("sequence",r.getSequence().toString());
                mapRequestApprover.put("approver",r.getApprover());
                mapRequestApprover.put("actionState",r.getActionState());
                mapRequestApprover.put("actionStateName",r.getActionStateName());
                mapRequestApprover.put("actionTime",sdf.get().format(r.getActionTime()));
                mapRequestApprover.put("userNameApprover",r.getUserNameApprover());
                mapRequestApprover.put("actionReasonCode",null == r.getActionReasonCode()?"":r.getActionReasonCode());
                mapRequestApprover.put("actionReasonDetail",null == r.getActionReasonDetail()?"":r.getActionReasonDetail());
                mapRequestApprover.put("requestStatusCode",r.getRequestStatusCode());
                listRequestApprover.add(mapRequestApprover);
            }
        }

        mapHeader.put("requestApprover",listRequestApprover);
        System.out.println(gson.toJson(mapHeader,Map.class));
        return gson.toJson(mapHeader,Map.class);
    }
}
