package com.spt.engine.controller;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.persistence.EntityManager;

import com.spt.engine.entity.app.*;
import com.spt.engine.repository.app.CarBookingRepository;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.repository.app.LocationRepository;

@RestController
@RequestMapping("/carBookingCustom")
public class CarBookingController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(CarBookingController.class);
	
	@Autowired
	EntityManager entityManager;

	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	LocationRepository locationRepository;

	@Autowired
	CarBookingRepository carBookingRepository;

	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
		public Date deserialize(JsonElement json, Type typeOfT,
								JsonDeserializationContext context) throws JsonParseException {
			return json == null ? null : new Date(json.getAsLong());
		}
	};


	protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();


	@Transactional
	@PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public CarBooking saveCarBooking(	 @RequestBody CarBooking carBooking,
										 @RequestParam(value = "origin" , required=false) Location origin,
										 @RequestParam(value = "destination" , required = false) Long destination,
										 @RequestParam("documentApproveItem") Long documentApproveItem
									){
	
		LOGGER.info("><><>< SAVE AND UPDATE TRAVEL DETAIL ><><><");
		
		
		try{
			Location destination1 = null;
			if(destination != null){
				destination1 = locationRepository.findOne(destination);
			}
			
			
			if(carBooking.getId() != null){
				LOGGER.info("><><>< MERGE CAR BOOKING ><><><");
				
				CarBooking carBooking2 = entityManager.find(CarBooking.class, carBooking.getId());
				CarBooking detail = objectMapper.readerForUpdating(carBooking2).readValue(gson.toJson(carBooking));
				
				detail.setOrigin(origin);
				detail.setDestination(destination1);
	            
	            entityManager.merge(detail);
	            entityManager.flush();
			}else{
				LOGGER.info("><><>< PERSIST CAR BOOKING ><><><");
				
				carBooking.setOrigin(origin);
				carBooking.setDestination(destination1);
							
				DocumentApproveItem documentAppItem = entityManager.find(DocumentApproveItem.class, documentApproveItem);
				
				documentAppItem.setCarBooking(carBooking);
				
				entityManager.persist(documentAppItem);
							
				carBooking.setDocumentApproveItem(documentAppItem);
				entityManager.persist(carBooking);	
				
				LOGGER.info("><><>< AFTER PERSIST CAR BOOKING ><><>< ");
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return carBooking;
	}

	@GetMapping(value="/getCarBooking/{id}/carBookings",produces = "application/json; charset=utf-8")
	public String getTravelDetail(@PathVariable Long id) {

		List<CarBooking> carBookingLs = carBookingRepository.findByDocumentApproveItemId(id);

		LOGGER.info("carBooking = {} ",carBookingLs.size());

		return (new JSONSerializer().exclude("*.class").include("id")
				.include("startTime")
				.include("endTime")
				.include("otherLocation")
				.include("remark")
				.include("carTypeCode")
				.include("numMember")
				.include("otherCarType")
				.include("origin.id")
				.include("destination.id")
				.serialize(carBookingLs));
	}
	
}
