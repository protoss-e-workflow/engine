package com.spt.engine.controller;

import com.spt.engine.entity.app.CurrencyRate;
import com.spt.engine.repository.app.CurrencyRateRepository;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/currencyRateCustom")
public class CurrencyRateController {

    static final Logger LOGGER = LoggerFactory.getLogger(CurrencyRateController.class);

    @Autowired
    CurrencyRateRepository currencyRateRepository;

    @GetMapping(value="/findByActionDateFromAndActionDateTo",produces = "application/json; charset=utf-8")
    public String findByActionDateFromAndActionDateTo(@RequestParam("actionDateFrom") String dateFromStr,@RequestParam("actionDateTo") String dateToStr) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date dateFrom = null;
        Date dateTo = null;
        CurrencyRate currencyRate = null;
        try {
            dateFrom = format.parse(dateFromStr);
            dateTo = format.parse(dateToStr);
            currencyRate = currencyRateRepository.findByActionDateFromActionDateTo(dateFrom,dateTo);
        }catch (Exception e){
            LOGGER.error("ERROR PARSE DATE IN findByActionDate ");
        }

        return (new JSONSerializer().exclude("*.class").serialize(currencyRate));
    }

    @GetMapping(value="/findByActionDate",produces = "application/json; charset=utf-8")
    public String findByActionDate(@RequestParam("actionDate") String actionDateStr) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date actionDate = null;
        CurrencyRate currencyRate = null;
        try {
            actionDate = format.parse(actionDateStr+" 23:59:59");
            List<CurrencyRate> currencyRateList = currencyRateRepository.findByActionDate(actionDate);

            if(currencyRateList.size()>0){
                currencyRate = currencyRateList.get(0);
            }
        }catch (Exception e){
            LOGGER.error("ERROR PARSE DATE IN findByActionDate ");
        }

        return (new JSONSerializer().exclude("*.class").serialize(currencyRate));
    }
}
