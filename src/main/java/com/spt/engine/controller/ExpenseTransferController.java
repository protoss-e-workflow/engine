package com.spt.engine.controller;

import com.google.gson.*;
import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentAdvance;
import com.spt.engine.entity.app.DocumentExpenseItem;
import com.spt.engine.repository.app.DocumentRepository;
import com.spt.engine.service.ExpenseTranferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/expenseTransferCustom")
public class ExpenseTransferController {

    static final Logger LOGGER = LoggerFactory.getLogger(ExpenseTransferController.class);

    @Autowired
    ExpenseTranferService expenseTranferService;
    
	@Autowired
	DocumentRepository documentRepository;

    @PersistenceContext
    private EntityManager entityManager;

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    @Transactional
    @PostMapping(value="/advanceTransfer",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String advanceTransfer(@RequestBody Map param){

        LOGGER.info("><><>< ADVANCE TRANSFER ><><><");
        Date currentDate = new Date();
        String result = null;
        String messageReturn = "";
        try {
            result = expenseTranferService.advanceTranfer(param);
            Long documentId  = Long.valueOf(param.get("id").toString());
            Document document = entityManager.find(Document.class, documentId);
            DocumentAdvance documentAdvance = document.getDocumentAdvance();
            LOGGER.info("advance return ={}",result);

            List<Map<String,String>> returnList = gson.fromJson(result, List.class);
            //Example:
            //[{"sapDocNumber":"","returnClass":"ZCCA1","returnMessage":"FI - ระบุ Branch Code เป็นค่า NVAT หรื่อตัวเลข 5 หลักเท่านั้น","returnNumber":"036","returnType":"E"}]
            for(Map<String,String> returnData :returnList){
                if("S".equals(returnData.get("returnType"))){
                    documentAdvance.setExternalDocNumber(String.valueOf(returnData.get("sapDocNumber")));
                    entityManager.merge(documentAdvance);
                    
                    document.setInterfaceOutDate(new Timestamp(currentDate.getTime()));
                    entityManager.merge(document);
        			entityManager.flush();
                }else if("E".equals(returnData.get("returnType"))){
                    messageReturn += returnData.get("returnMessage")+"<br/>";
                }
            }
            LOGGER.info("><><>< ADVANCE TRANSFER SUCCESS ><><><");
        } catch (Exception e) {
            e.printStackTrace();
            messageReturn = "Error";
        }

        return messageReturn;
    }

    @Transactional
    @PostMapping(value="/expenseTransfer",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String expenseTransfer(@RequestBody Map param){

    	Date currentDate = new Date();
        LOGGER.info("><><>< EXPENSE TRANSFER ><><><");
        String result = null;
        String messageReturn = "";
        try {
            Map retMap = expenseTranferService.expenseTranfer(param);
            result = (String) retMap.get("returnStr");
            Long documentId  = Long.valueOf(param.get("id").toString());
            Document document = entityManager.find(Document.class, documentId);
            LOGGER.info("================== expenseTranfer={}",result);
            List<Map<String,String>> dataReturn = gson.fromJson(result, List.class);
            
            
            Map<String,String> instanceItemIdMap = (Map) retMap.get("instanceItemId");
            for(String key:instanceItemIdMap.keySet()){
            	LOGGER.info("==================instanceItemIdMap key({}) = {}",key,instanceItemIdMap.get(key));
            }
            
            String clearingNumber =null;
            String advanceSapDocNumber =null;
            Set<DocumentExpenseItem> setOfUpdate = new HashSet<DocumentExpenseItem>();
			for(Map<String,String> dataMap:dataReturn){
				if("S".equals(dataMap.get("returnType"))){
					if(String.valueOf(dataMap.get("sapDocNumber")).startsWith("62")){
						for(String itemId:String.valueOf(dataMap.get("expenseItemId")).split(",")){
							DocumentExpenseItem item = documentRepository.findDocumentExpenseItemByDocumentIdAndSequence(documentId, itemId);//entityManager.find(DocumentExpenseItem.class, Long.valueOf(itemId));
							if(item.getExternalDocNumber()==null){
								item.setExternalDocNumber(dataMap.get("sapDocNumber"));
								advanceSapDocNumber = dataMap.get("advanceSapDocNumber");
								entityManager.merge(item);
								setOfUpdate.add(item);
								
								document.setInterfaceOutDate(new Timestamp(currentDate.getTime()));
			                    entityManager.merge(document);
			        			entityManager.flush();
							}
							
						}
					}
				}else if("C".equals(dataMap.get("returnType"))){
					 clearingNumber = dataMap.get("sapDocNumber");
				}else if("B".equals(dataMap.get("returnType"))){
					if(String.valueOf(dataMap.get("sapDocNumber")).startsWith("68")){
						for(String itemId:String.valueOf(dataMap.get("expenseItemId")).split(",")){
							DocumentExpenseItem item = documentRepository.findDocumentExpenseItemByDocumentIdAndSequence(documentId, itemId);//entityManager.find(DocumentExpenseItem.class, Long.valueOf(itemId));
							if(item.getExternalBankReceiveDocNumber()==null){
								item.setExternalBankReceiveDocNumber(dataMap.get("sapDocNumber"));
								entityManager.merge(item);
								setOfUpdate.add(item);
							}
							
						}
					}
					 
				}else if("E".equals(dataMap.get("returnType"))){
					 messageReturn += dataMap.get("returnMessage")+"<br/>";
				}
			}
			
			if(clearingNumber!=null){
				DocumentAdvance documentAdvance = documentRepository.findDocumentAdvanceByExternalDocNumber(advanceSapDocNumber,document.getCompanyCode());
				documentAdvance.setExternalClearingDocNumber(clearingNumber);
				entityManager.merge(documentAdvance);
				
				for(DocumentExpenseItem item:setOfUpdate){
					item.setExternalClearingDocNumber(clearingNumber);
					entityManager.merge(item);
				}
			}
			entityManager.flush();

        } catch (Exception e) {
            e.printStackTrace();
            messageReturn = "ERROR";
        }
        LOGGER.info("><><>< EXPENSE TRANSFER SUCCESS ><><><");
        return messageReturn;
    }
}
