package com.spt.engine.controller;

import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.AdvanceOutstanding;
import com.spt.engine.entity.app.Document;
import com.spt.engine.repository.app.AdvanceOutstandingRepository;
import com.spt.engine.repository.app.DocumentRepository;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/advanceOutstandingCustom")
public class AdvanceOutstandingController {

    static final Logger LOGGER = LoggerFactory.getLogger(AdvanceOutstandingController.class);

    @Autowired
    AdvanceOutstandingRepository advanceOutstandingRepository;

    @Autowired
    DocumentRepository documentRepository;
    

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @GetMapping(value="/findAdvanceOutstanding",produces = "application/json; charset=utf-8")
    public String findAdvanceOutstanding(@RequestParam("venderNo") String venderNo,
                                         @RequestParam("userName") String userName){

        Double totalAmountAdvance = 0d;
        Map<String,Double> result = new HashMap<>();

        /** find from sap **/
        List<AdvanceOutstanding> advanceOutstandingList = advanceOutstandingRepository.findByVenderNo(venderNo);

        /** find from gwf **/
        List<Document> documentList = documentRepository.findByRequesterForCheckAdvanceOutstanding(userName,ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE,ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_ON_PROCESS);

        if(advanceOutstandingList.size() > 0){
            for (AdvanceOutstanding ao:advanceOutstandingList){
                totalAmountAdvance += ao.getAmount();
            }
        }

        if(documentList.size() > 0){
            for (Document d : documentList){
                totalAmountAdvance += d.getTotalAmount();
            }
        }

        result.put("amount",totalAmountAdvance);

        return (new JSONSerializer().serialize(result));
    }

    @Transactional
    @GetMapping(value ="/clear" )
    public void clear(){
        try{
            LOGGER.info("========   Start Process clearInternalOrder  ========{}",new Date());
            advanceOutstandingRepository.deleteAll();
            LOGGER.info("========   End Process   clearInternalOrder  ========{}",new Date());
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.info("========   ERROR[Controller] deleteAll   ========");
        }

    }
    

    @Transactional
    @GetMapping(value ="/clearAdvanceDataNotStable" )
    public void clearAdvanceDataNotStable(){
        try{
        	LOGGER.info("========   Start Process clearAdvanceDataNotStable  ========{}",new Date());
        	String sql = 	"update advance_outstanding "+
        					"set amount=0 "+
        					"where id in (select ao.id "+
							"from document_advance da "+
							"LEFT JOIN document d on d.document_advance_id = da.id "+
							"LEFT JOIN advance_outstanding ao on CONCAT('M',SUBSTRING(d.personal_id, 2, 8)) = ao.vendor_no and d.company_code = ao.company_code "+
							"where da.external_doc_number is not NULL "+
							"and   da.external_clearing_doc_number is null "+
							"and   d.document_status not in ('CCL','REJ','SSDEL') "+
							"and   ao.amount <> 0 "+
							"and   ao.amount = da.amount) ";
        	namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource());
            LOGGER.info("========   End Process   clearAdvanceDataNotStable  ========{}",new Date());
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.info("========   ERROR[Controller] clearAdvanceDataNotStable   ========");
        }

    }
}
