package com.spt.engine.controller;

import com.spt.engine.entity.app.MonthlyPhoneBill;
import com.spt.engine.repository.app.MonthlyPhoneBillRepository;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import java.util.List;

@RestController
@RequestMapping("/monthlyPhoneBillCustom")
public class MonthlyPhoneBillController {

    static final Logger LOGGER = LoggerFactory.getLogger(MonthlyPhoneBillController.class);

    @Autowired
    EntityManager entityManager;

    @Autowired
    MonthlyPhoneBillRepository monthlyPhoneBillRepository;

    @GetMapping(value="/resetMonthlyPhoneBill",produces = "application/json; charset=utf-8")
    public String resetMonthlyPhoneBill() {

        LOGGER.info("><><>< resetMonthlyPhoneBill ><><><");

        List<MonthlyPhoneBill> monthlyPhoneBillList = monthlyPhoneBillRepository.findAll();

        LOGGER.info("MonthlyPhoneBillList Size    :   {}",monthlyPhoneBillList.size());

        try {
            if(monthlyPhoneBillList.size() > 0){
                for (MonthlyPhoneBill m : monthlyPhoneBillList){
                    m.setBalance(m.getAmountPerMonth());
                    entityManager.merge(m);
                }
                entityManager.flush();
            }
            return "Success";
        }catch (Exception e){
            e.printStackTrace();
            return "Error";
        }
    }
}
