package com.spt.engine.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.spt.engine.entity.general.Parameter;
import com.spt.engine.entity.general.ParameterDetail;
import com.spt.engine.repository.general.ParameterRepository;

@Controller
public class FileUploadController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(FileUploadController.class);
	
	@PersistenceContext
    private EntityManager entityManager;
	
	@Autowired
	ParameterRepository parameterRepository;
	
	@PostMapping("/fileUpload")
    public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file,@RequestParam("name") String filename) {

		LOGGER.info("handleFileUpload ");
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        
        Parameter parameter = parameterRepository.findByCode("P100");
		String pathFile = "";
		for(ParameterDetail detail:parameter.getDetails()){
			pathFile = detail.getVariable1();
		}
		//
		File convFile = new File(pathFile+filename);
		try {
			file.transferTo(convFile);
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<String>("ERROR", headers, HttpStatus.OK);
		}
    }
	

	@PostMapping("/imageUpload")
    public ResponseEntity<String> handleImageUpload(@RequestParam("file") String base64,@RequestParam("filename") String filename) {
		
		LOGGER.info("handleImageUpload ");
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		
		try {
			// create a buffered image
			BufferedImage image = null;
			byte[] imageByte;

			String base64Image = base64.split(",")[1];
			imageByte = Base64.decodeBase64(base64Image);
			ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
			image = ImageIO.read(bis);
			bis.close();

			Parameter parameter = parameterRepository.findByCode("P100");
			String pathFile = "";
			for(ParameterDetail detail:parameter.getDetails()){
				pathFile = detail.getVariable1();
			}
			
			
			
			// write the image to a file
			File outputfile = new File(pathFile+filename);
			ImageIO.write(image, "png", outputfile);
			
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<String>("ERROR", headers, HttpStatus.OK);
		}
    }
	
	@GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

		
		Parameter parameter = parameterRepository.findByCode("P100");
		String pathFile = "";
		for(ParameterDetail detail:parameter.getDetails()){
			pathFile = detail.getVariable1();
		}
		
		return ResponseEntity
                .ok()
                //.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+file.getFilename()+"\"")
                .body(new FileSystemResource(pathFile+filename));
    }
	
}
