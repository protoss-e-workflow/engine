package com.spt.engine.controller;

import com.spt.engine.entity.app.DocumentExpenseItem;
import com.spt.engine.entity.app.PetrolAllowancePerMonth;
import com.spt.engine.entity.app.PetrolAllowancePerMonthHistory;
import com.spt.engine.repository.app.DocumentExpenseItemRepository;
import com.spt.engine.repository.app.PetrolAllowancePerMonthHistoryRepository;
import com.spt.engine.repository.app.PetrolAllowancePerMonthRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;


@RestController
@RequestMapping("/petrolAllowancePerMonthHistoryCustom")
public class PetrolAllowancePerMonthHistoryController {

    static final Logger LOGGER = LoggerFactory.getLogger(PetrolAllowancePerMonthHistoryController.class);

    @Autowired
    PetrolAllowancePerMonthRepository petrolAllowancePerMonthRepository;

    @Autowired
    PetrolAllowancePerMonthHistoryRepository petrolAllowancePerMonthHistoryRepository;

    @Autowired
    DocumentExpenseItemRepository documentExpenseItemRepository;

    @Autowired
    EntityManager entityManager;

    @Transactional
    @PostMapping(value="/savePetrolAllowancePerMonthHistoryForExpense",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String savePetrolAllowancePerMonthHistoryForExpense(@RequestBody PetrolAllowancePerMonthHistory petrolAllowancePerMonthHistory){

        LOGGER.info("><><>< Save Petrol Allowance Per Month History For Expense ><><><");
        String result = "";
        try{
            PetrolAllowancePerMonth petrolAllowancePerMonth = petrolAllowancePerMonthRepository.findByEmpUserName(petrolAllowancePerMonthHistory.getEmpUserName());
            if(null != petrolAllowancePerMonth){
                Double petrolBalance = petrolAllowancePerMonth.getBalanceLiterPerMonth();
                Double petrolAmount = petrolAllowancePerMonthHistory.getLiterNumber();
                Double total = petrolBalance - petrolAmount;

                petrolAllowancePerMonth.setBalanceLiterPerMonth(total);
                entityManager.merge(petrolAllowancePerMonth);
                entityManager.flush();
            }

            entityManager.persist(petrolAllowancePerMonthHistory);
            result = "Success";

        }catch (Exception e){
            result = "Error";
            e.printStackTrace();
        }

        return result;
    }

    @Transactional
    @PostMapping(value="/deletePetrolAllowancePerMonthHistoryForExpense",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String deletePetrolAllowancePerMonthHistoryForExpense(@RequestParam("expenseItem") Long documentExpenseItemId){

        LOGGER.info("><><>< Delete Petrol Allowance Per Month History For Expense ><><><");
        String result = "";
        try{
            DocumentExpenseItem documentExpenseItem = documentExpenseItemRepository.findOne(documentExpenseItemId);
            if(null != documentExpenseItem){
                Double oldLiter = Double.valueOf(documentExpenseItem.getDocExpItemDetail().getLiter());
                Double balanceLiter = 0d;

                String requester = documentExpenseItem.getDocumentExp().getDocument().getRequester();
                PetrolAllowancePerMonth petrolAllowancePerMonth = petrolAllowancePerMonthRepository.findByEmpUserName(requester);
                if(null != petrolAllowancePerMonth){
                    balanceLiter = petrolAllowancePerMonth.getBalanceLiterPerMonth();
                    petrolAllowancePerMonth.setBalanceLiterPerMonth(oldLiter+balanceLiter);
                    entityManager.merge(petrolAllowancePerMonth);
                    entityManager.flush();
                }

            }
            result = "Success";

        }catch (Exception e){
            result = "Error";
            e.printStackTrace();
        }

        return result;
    }

    @Transactional
    @PostMapping(value="/updatePetrolAllowancePerMonthHistoryForExpense",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String updatePetrolAllowancePerMonthHistoryForExpense(@RequestBody PetrolAllowancePerMonthHistory petrolAllowancePerMonthHistory,
                                                                 @RequestParam("expenseItem") Long documentExpenseItemId){

        LOGGER.info("><><>< Update Petrol Allowance Per Month History For Expense ><><><");
        String result = "";
        try{
            DocumentExpenseItem documentExpenseItem = documentExpenseItemRepository.findOne(documentExpenseItemId);
            if(null != documentExpenseItem){
                Double newLiter = petrolAllowancePerMonthHistory.getLiterNumber();
                Double oldLiter = 0d;
                Double balanceLiter = 0d;

                String requester = documentExpenseItem.getDocumentExp().getDocument().getRequester();
                PetrolAllowancePerMonth petrolAllowancePerMonth = petrolAllowancePerMonthRepository.findByEmpUserName(requester);

                if(null != petrolAllowancePerMonth){
                    oldLiter = petrolAllowancePerMonth.getLiterPerMonth();
                    balanceLiter = oldLiter-newLiter;
                    petrolAllowancePerMonth.setBalanceLiterPerMonth(balanceLiter);
                    entityManager.merge(petrolAllowancePerMonth);
                    entityManager.flush();
                }

            }
            result = "Success";

        }catch (Exception e){
            result = "Error";
            e.printStackTrace();
        }

        return result;
    }
}