package com.spt.engine.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;
import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.*;
import com.spt.engine.repository.app.LocationRepository;
import com.spt.engine.repository.app.RouteDistanceProcessCalculateRepository;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import java.lang.reflect.Type;
import java.util.Date;


@RestController
@RequestMapping("/routeDistanceCustom")
public class RouteDistanceController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(RouteDistanceController.class);
	

	
	@Autowired
	RouteDistanceProcessCalculateRepository routeDistanceProcessCalculateRepository;
	
	@Autowired
	LocationRepository locationRepository;
	
	@Autowired
	EntityManager entityManager;
	
	@Autowired 
    ObjectMapper objectMapper;
	
	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

	
	protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();
	
	@Transactional
	@PostMapping(produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public RouteDistance saveRouteDistance(@RequestBody RouteDistance routeDistance,
								 @RequestParam("locationFrom") Location locationFrom,
								 @RequestParam("locationTo") Location locationTo){

		routeDistance.setLocationFrom(locationFrom);
		routeDistance.setLocationTo(locationTo);
		if(routeDistance.getId()==null){
			LOGGER.info("><><>< SAVE RouteDistance ><><><");
			entityManager.persist(routeDistance);
		} else {
			updateRouteDistance(routeDistance);
		}



		
		LOGGER.info("><><>< AFTER PERSIST RouteDistance ><><>< ");
		return routeDistance;
	}
	
	
	@Transactional
	@PostMapping(value="/updateRouteDistance",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public RouteDistance updateRouteDistance(@RequestBody RouteDistance routeDistance){
	
		LOGGER.info("><><>< UPDATE RouteDistance ><><><");
		
		try {
			RouteDistance oldRouteDistance = entityManager.find(RouteDistance.class, routeDistance.getId());
			RouteDistance newRouteDistance = objectMapper.readerForUpdating(oldRouteDistance).readValue(gson.toJson(routeDistance));
	        entityManager.merge(newRouteDistance);
	        entityManager.flush();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return routeDistance;
	}

	@GetMapping(value="/calculateRouteDistanceFromLocation",produces = "application/json; charset=utf-8")
	public String calculateRouteDistanceFromLocation(@RequestParam("locationFrom") String locationFrom,@RequestParam("locationTo") String locationTo,@RequestParam("companyCode") String companyCode) {

		LOGGER.info("><><>< calculateRouteDistance ><><><");

		Location location1 = locationRepository.findByCode(locationFrom);
		Location location2 = locationRepository.findByCode(locationTo);

		Double amount = routeDistanceProcessCalculateRepository.processCalculateTotalAmountDistanceRoute(location1.getId(),location2.getId(),companyCode);

		LOGGER.info("amount = {} ",amount);

		return (new JSONSerializer().serialize(amount));
	}

	@GetMapping(value="/calculateRouteDistanceFromDistance",produces = "application/json; charset=utf-8")
	public String calculateRouteDistanceFromDistance(@RequestParam("distance") Double distance,@RequestParam("companyCode") String companyCode) {

		LOGGER.info("><><>< calculateRouteDistanceFromDistance ><><><");

		Double amount = routeDistanceProcessCalculateRepository.getAmountFromDistanceSendOnlyDistance(companyCode,distance);

		LOGGER.info("amount = {} ",amount);

		return (new JSONSerializer().serialize(amount));
	}
}
