package com.spt.engine.controller;

import com.spt.engine.service.QRCodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.util.concurrent.TimeUnit;

@Controller
@EnableAsync
@EnableCaching
@EnableScheduling
public class QRCodeController {

//    public static final long THIRTY_MUNITES = 1800000;
    private static final Logger LOGGER = LoggerFactory.getLogger(QRCodeController.class);

    @Autowired
    QRCodeService qrCodeService;

    @GetMapping(value = "/qrcode")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@RequestParam("text") String text) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","image/png");
        headers.add("Content-Disposition","attachment; filename*=UTF8''" + text.substring(text.lastIndexOf("/") + 1) + ".png");

        try{
            return ResponseEntity.ok()
                    .cacheControl(CacheControl.maxAge(30, TimeUnit.MINUTES))
                    .headers(headers)
                    .body(new ByteArrayResource(qrCodeService.generateQRCode(text, 256, 256)));
        }catch (Exception ex){
            throw new InternalServerError("Error While generating QRCode {}",ex);
        }
    }

//    @Scheduled(fixedRate = THIRTY_MUNITES)
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    @DeleteMapping(value = "/qrcode")
//    public void deleteAllCachedQRCode(){
//        qrCodeService.purgeCache();
//    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public class InternalServerError extends RuntimeException{
        private static final long serialVersionUID = 1L;
        public InternalServerError(final String message,final Throwable cause){
            super(message);
            LOGGER.info("INTERNAL_SERVER_ERROR {}",message);
        }
    }
}
