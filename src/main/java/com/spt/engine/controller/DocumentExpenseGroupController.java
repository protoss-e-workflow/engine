package com.spt.engine.controller;

import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentExpenseGroup;
import com.spt.engine.entity.app.DocumentExpenseItem;
import com.spt.engine.repository.app.DocumentExpenseGroupRepository;
import com.spt.engine.repository.app.DocumentExpenseItemRepository;
import com.spt.engine.repository.app.DocumentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import java.util.List;

@RestController
@RequestMapping("/documentExpenseGroupCustom")
public class DocumentExpenseGroupController {

    static final Logger LOGGER = LoggerFactory.getLogger(DocumentExpenseGroupController.class);

    @Autowired
    EntityManager entityManager;

    @Autowired
    DocumentExpenseGroupRepository documentExpenseGroupRepository;

    @Autowired
    DocumentRepository documentRepository;

    @Autowired
    DocumentExpenseItemRepository documentExpenseItemRepository;

    @Transactional
    @DeleteMapping(value="/delete/{id}",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String deleteExpenseGroup(@PathVariable Long id){

        LOGGER.info("><><>< DELETE DOCUMENT EXPENSE GROUP ><><><");
        String resultString = "";
        try {
            DocumentExpenseGroup documentExpenseGroup =  documentExpenseGroupRepository.findOne(id);
            Long idDocumentExpense = documentExpenseGroup.getDocumentExp().getId();
            Long idDocument = documentExpenseGroup.getDocumentExp().getDocument().getId();
            String expenseTypeCode = documentExpenseGroup.getExpTypeByCompanys().getExpenseType().getCode();

            entityManager.remove(documentExpenseGroup);
            entityManager.flush();

            List<DocumentExpenseItem> documentExpenseItemList = documentExpenseItemRepository.findByDocumentExpAndExpenseTypeCode(idDocumentExpense,expenseTypeCode);
            if(documentExpenseItemList.size() > 0){
                for (DocumentExpenseItem dei:documentExpenseItemList){
                    entityManager.remove(dei);
                }
                entityManager.flush();
            }

            List<DocumentExpenseGroup> documentExpenseGroupList = documentExpenseGroupRepository.findByDocumentExp(idDocumentExpense);
            Document document = documentRepository.findOne(idDocument);
            if(documentExpenseGroupList.size() == 1){
                document.setTitleDescription(documentExpenseGroupList.get(0).getExpTypeByCompanys().getExpenseTypes().getDescription());
                entityManager.merge(document);
                entityManager.flush();
            }else if(documentExpenseGroupList.size() > 1){
                document.setTitleDescription(documentExpenseGroupList.get(0).getExpTypeByCompanys().getExpenseTypes().getDescription()+"....");
                entityManager.merge(document);
                entityManager.flush();
            }else {
                document.setTitleDescription("");
                entityManager.merge(document);
                entityManager.flush();
            }

            /** set new total amount **/
            List<DocumentExpenseItem> documentExpenseItemList1 = documentExpenseItemRepository.findByDocumentExp(idDocumentExpense);
            if(documentExpenseItemList1.size() > 0){
                Double totalAmount = 0d;
                for (DocumentExpenseItem d:documentExpenseItemList1){
                    Double netAmount = d.getNetAmount()==null?0:d.getNetAmount();
                    Double whtAmount = d.getWhtAmount()==null?0:d.getWhtAmount();
                    totalAmount += (netAmount+whtAmount);

                }
                document.setTotalAmount(totalAmount);
                entityManager.merge(document);
                entityManager.flush();
            }else{
                document.setTotalAmount(0d);
                entityManager.merge(document);
                entityManager.flush();
            }

            resultString = "Success";
        } catch (Exception e) {
            e.printStackTrace();
            resultString = "Error";
        }
        return resultString;
    }
}
