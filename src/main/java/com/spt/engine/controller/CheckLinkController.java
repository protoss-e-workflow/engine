package com.spt.engine.controller;


import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.Delegate;
import com.spt.engine.entity.app.Document;
import com.spt.engine.repository.app.DelegateRepository;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/checkLinkCustom")
public class CheckLinkController {

    static final Logger LOGGER = LoggerFactory.getLogger(CheckLinkController.class);


    @Value("${SAPServer}")
    protected String SAPServer = "";

    @Value("${AppServer}")
    protected String AppServer = "";

    @Value("${OrgSysServer}")
    protected String OrgSysServer = "";

    @Value("${EmailServer}")
    protected String EmailServer = "";

    @Value("${EngineServer}")
    protected String EngineServer = "";


    @Autowired
    RestTemplate restTemplate;


    @GetMapping(value="/checkEndpointProperties",produces = "application/json; charset=utf-8")
    public ResponseEntity<String> checkLink() {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

            String SAPServer = this.SAPServer;
            String AppServer = this.AppServer;
            String OrgSysServer = this.OrgSysServer;
            String EmailServer = this.EmailServer;
            String EngineServer = this.EngineServer;

        Map<String,Object> mapLink = new HashMap<>();
        mapLink.put("AppServer",AppServer);
        mapLink.put("EngineServer",EngineServer);
        mapLink.put("SAPServer",SAPServer);
        mapLink.put("OrgSysServer",OrgSysServer);
        mapLink.put("EmailServer",EmailServer);


//        List<Map<String,Object>> resultFromSAP  = restTemplate.getForObject(SAPServer+"/checkLinkCustom/sendListEndPoint", List.class);
//        LOGGER.error("Result From SAP "+resultFromSAP.get(0).get("EngineServer"));

        return new ResponseEntity<String>(new JSONSerializer().prettyPrint(true).serialize(mapLink),headers, HttpStatus.OK);
    }

}