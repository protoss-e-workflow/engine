package com.spt.engine.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.spt.engine.entity.app.InternalOrder;
import com.spt.engine.repository.app.InternalOrderRepository;
import com.spt.engine.service.InternalOrderService;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/internalOrderCustom")
public class InternalOrderController {
	
	static final Logger LOGGER = LoggerFactory.getLogger(InternalOrderController.class);

	@Autowired
	EntityManager entityManager;
	
	@Autowired 
    ObjectMapper objectMapper;

	@Autowired
	InternalOrderRepository internalOrderRepository;

	@Autowired
	InternalOrderService internalOrderService;

	@Transactional
	@GetMapping(value ="/saveInternalOrderFromSAP" ,produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public void saveInternalOrder_From_SAP_Interface(){


		try{


			internalOrderRepository.saveInternalOrder_From_SAP_Interface();

		}catch(Exception e){

			e.printStackTrace();
			LOGGER.info("========   ERROR[Controller] saveInternalOrderFromSAP   ========");

		}



	}

	@Transactional
	@GetMapping(value ="/clear" )
	public void clear(){
		try{
			LOGGER.info("========   Start Process clearInternalOrder  ========{}",new Date());
			internalOrderRepository.deleteAll();
			LOGGER.info("========   End Process   clearInternalOrder  ========{}",new Date());
		}catch(Exception e){
			e.printStackTrace();
			LOGGER.info("========   ERROR[Controller] deleteAll   ========");
		}

	}

	@GetMapping(value="/checkIoRealtime",produces = "application/json; charset=utf-8")
	public String checkIoRealtime(@RequestParam("gl") String gl,@RequestParam("costCenter") String costCenter,@RequestParam("io") String io,
								  @RequestParam("manOrder") String manOrder,@RequestParam("wbs") String wbs,@RequestParam("docNumber") String docNumber,@RequestParam("expItemId") String expItemId){

		try{
			Map<String,String> result = internalOrderService.checkToRealtime(gl, costCenter, io, manOrder, wbs,docNumber,expItemId);
			return (new JSONSerializer().serialize(result));
		}catch(Exception e){
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@GetMapping(value="/checkBudgetInEwf",produces = "application/json; charset=utf-8")
	public String checkBudgetInEwf(@RequestParam("gl") String gl,@RequestParam("costCenter") String costCenter){

		try{
			List<Map<String,Object>> result = internalOrderService.checkBudgetInEwf(gl, costCenter);
			return (new JSONSerializer().serialize(result));
		}catch(Exception e){
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

}
