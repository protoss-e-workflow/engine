package com.spt.engine;

import com.spt.engine.entity.app.*;
import com.spt.engine.entity.general.*;
import com.spt.engine.repository.general.projection.HaveDocument;
import com.spt.engine.repository.general.projection.HaveRequestApprover;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

@Configuration
public class RepositoryConfig extends RepositoryRestConfigurerAdapter {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Menu.class);
        config.exposeIdsFor(User.class);
        config.exposeIdsFor(Role.class);
        config.exposeIdsFor(Parameter.class);
        config.exposeIdsFor(ParameterDetail.class);
        config.exposeIdsFor(MasterData.class);
        config.exposeIdsFor(MasterDataDetail.class);
        config.exposeIdsFor(LocaleMessage.class);
        config.exposeIdsFor(Document.class);
        config.exposeIdsFor(CostCenter.class);
        config.exposeIdsFor(Department.class);
        config.exposeIdsFor(Company.class);
        config.exposeIdsFor(Location.class);
        config.exposeIdsFor(TravelDetail.class);
        config.exposeIdsFor(DocumentApprove.class);
        config.exposeIdsFor(DocumentApproveItem.class);
        config.exposeIdsFor(TravelMember.class);
        config.exposeIdsFor(DocumentAdvance.class);
        config.exposeIdsFor(CarBooking.class);
        config.exposeIdsFor(HotelBooking.class);
        config.exposeIdsFor(FlightTicket.class);
        config.exposeIdsFor(DocumentAttachment.class);
        config.exposeIdsFor(RouteDistance.class);
        config.exposeIdsFor(ExpenseType.class);
        config.exposeIdsFor(HotelBooking.class);
        config.exposeIdsFor(DocumentExpenseItem.class);
        config.exposeIdsFor(ExpenseTypeReference.class);
        config.exposeIdsFor(ExpenseTypeByCompany.class);
        config.exposeIdsFor(Reimburse.class);
        config.exposeIdsFor(ExpenseTypeScreen.class);
        config.exposeIdsFor(ExpenseTypeFile.class);
        config.exposeIdsFor(ConditionalIO.class);
        config.exposeIdsFor(PetrolAllowancePerMonth.class);
        config.exposeIdsFor(PetrolAllowancePerMonthHistory.class);
        config.exposeIdsFor(DieselAllowancePerMonth.class);
        config.exposeIdsFor(MonthlyPhoneBill.class);
        config.exposeIdsFor(DieselAllowancePerMonthHistory.class);
        config.exposeIdsFor(MonthlyPhoneBillHistory.class);
        config.exposeIdsFor(ConditionalGL.class);
        config.exposeIdsFor(Delegate.class);
        config.exposeIdsFor(Employee.class);
        config.exposeIdsFor(Request.class);
        config.exposeIdsFor(RequestApprover.class);
        config.exposeIdsFor(DocumentExpenseGroup.class);
        config.exposeIdsFor(DocumentExpense.class);
        config.exposeIdsFor(EmployeeReplacement.class);
        config.exposeIdsFor(DocumentExpItemAttachment.class);
        config.exposeIdsFor(Vat.class);
        config.exposeIdsFor(WithholdingTax.class);
        config.exposeIdsFor(WithholdingTaxCode.class);
        config.exposeIdsFor(InternalMemberAccomodationDetail.class);
        config.exposeIdsFor(ExternalMemberAccomodationDetail.class);
        config.exposeIdsFor(DocumentAccountRemark.class);
        config.exposeIdsFor(EmailTransaction.class);
        config.exposeIdsFor(DocumentApproveCostEstimate.class);

        

        config.getProjectionConfiguration().addProjection(HaveDocument.class,"haveDoc",Request.class);
        config.getProjectionConfiguration().addProjection(HaveRequestApprover.class,"haveRequestApprovers",Request.class);

    }
}
