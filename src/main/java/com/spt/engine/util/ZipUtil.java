package com.spt.engine.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ZipUtil
{
	static final Logger LOGGER = LoggerFactory.getLogger(ZipUtil.class);
    
    public static void packZip(ByteArrayOutputStream output, Map<String,InputStream> sources) throws IOException
    {
        ZipOutputStream zipOut = new ZipOutputStream(output);
        zipOut.setLevel(Deflater.DEFAULT_COMPRESSION);

        for (String  key : sources.keySet()){
        	zipFile(zipOut, "", key,sources.get(key));
        }
        zipOut.flush();
        zipOut.close();
    }

    private static String buildPath(String path, String file)
    {
        if (path == null || path.isEmpty())
        {
            return file;
        } else
        {
            return path + "/" + file;
        }
    }


    private static void zipFile(ZipOutputStream zos, String path, String filename,InputStream io) throws IOException{
        zos.putNextEntry(new ZipEntry(buildPath(path, filename)));


        byte[] buffer = new byte[4092];
        int byteCount = 0;
        while ((byteCount = io.read(buffer)) != -1)
        {
            zos.write(buffer, 0, byteCount);
        }
        System.out.println();

        io.close();
        zos.closeEntry();
    }
}
