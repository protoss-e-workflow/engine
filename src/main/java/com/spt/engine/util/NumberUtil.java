package com.spt.engine.util;

import java.math.BigDecimal;

public class NumberUtil {

	public static BigDecimal getBigDecimal(Object obj){
		BigDecimal ret = new BigDecimal(0);
		try {
			ret = new BigDecimal(String.valueOf(obj));
		} catch (Exception e) { }
		return ret;
	}
	
	public static Double getDouble(Double obj){
		Double ret = new Double(0);
		try {
			ret = obj;
			if(ret ==null){
				ret = 0d;
			}
		} catch (Exception e) { }
		return ret;
	}
}
