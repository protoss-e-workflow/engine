package com.spt.engine.util;

import java.util.List;
import java.util.Map;

import org.springframework.core.io.FileSystemResource;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

public class GenReportJasperPDF {
	

	public static JasperPrint exportReport(String jasperFileName,List list,Map<String,Object> params){
		
		
		FileSystemResource fsr = new FileSystemResource(jasperFileName);
		JasperPrint jasperPrint = null;

        try{
            
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(fsr.getFile());

            JRDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(list);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, beanCollectionDataSource);

            return jasperPrint;

        }catch (Exception e) {e.printStackTrace(); }
		return jasperPrint;
		
	}
}
