package com.spt.engine.util;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class AbstractReportJasperPDF {

    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractReportJasperPDF.class);

    public static JasperPrint exportReport(String jasperFileName,List list,  Map<String,Object> params){

        JasperPrint jasperPrint = null;
        InputStream jasperStream = null;

        try{
            Class cls = Class.forName("com.spt.engine.util.AbstractReportJasperPDF");
            ClassLoader cLoader = cls.getClassLoader();

            jasperStream = cLoader.getResourceAsStream("jasperreports/"+jasperFileName);

            LOGGER.info("<><><> jasperStream : {} <><><>",jasperStream);
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);

            JRDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(list);
            LOGGER.debug("JRDataSource : {}",beanCollectionDataSource);
//            jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource(1));
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, beanCollectionDataSource);

            return jasperPrint;

        }catch (Exception e) {
            LOGGER.error("Error : {}", e);
            e.printStackTrace();
            return jasperPrint;
        }finally{
            IOUtils.closeQuietly(jasperStream);
        }
    }

    public static byte[] generateReport(List<JasperPrint> jasperPrintList)  throws JRException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JRPdfExporter pdfExporter = new JRPdfExporter();
        pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
        pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
        pdfExporter.exportReport();
        return baos.toByteArray();
    }

}
