package com.spt.engine.service;

import java.util.List;
import java.util.Map;

public interface InternalOrderService {



    public List<Map<String,Object>> readDataInternalOrderFromSapInterface();
    public Map<String,String> checkToRealtime(String gl,String costCenter,String io,String manOrder,String wbs,String docNumber,String expItemId);
    public List<Map<String,Object>>  checkBudgetInEwf(String gl,String costCenter);
    public void moveFileWhenfetchAndSaveDataSuccess();
}
