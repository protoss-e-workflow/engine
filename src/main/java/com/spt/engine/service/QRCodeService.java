package com.spt.engine.service;

public interface QRCodeService {
    public byte[] generateQRCode(String text,int width,int height);
//    public void purgeCache();
}
