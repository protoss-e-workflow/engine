package com.spt.engine.service;

import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service
public interface ReportService {
    JasperPrint formReportApprove(String docNumber,Map<String, String> mapParam,String language);
    JasperPrint formReportAdvance(String docNumber,Map<String, String> mapParam,String language);
    JasperPrint formReportExpense(String docNumber,Map<String, String> mapParam,String language);
}