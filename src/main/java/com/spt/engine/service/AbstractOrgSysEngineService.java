package com.spt.engine.service;

import java.io.File;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

@Configuration
public abstract class  AbstractOrgSysEngineService {
	protected static Logger LOGGER = LoggerFactory.getLogger(AbstractOrgSysEngineService.class);

	@Value("${OrgSysServer}")
	protected String EngineServer = "";//"localhost:9007/OrgSysEngine/rest";
    protected static Properties connectProperties = null;

    protected String webServicesString = "";
    protected String resultString = "";

    protected RestTemplate restTemplate;
    
    public abstract void setRestTemplate(RestTemplate restTemplate);

	protected JsonParser parser = new JsonParser();

    JsonSerializer<Date> ser = new JsonSerializer<Date>() {
        public JsonElement serialize(Date src, Type typeOfSrc,
                                     JsonSerializationContext context) {
            return src == null ? null : new JsonPrimitive(src.getTime());
        }
    };
    
    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    
    public AbstractOrgSysEngineService(){
    }
    
    public String getWebServicesString() {
        return webServicesString;
    }
    
    public AbstractOrgSysEngineService setWebServicesString(String webServicesString) {
        this.webServicesString = webServicesString;
        return this;
    }
    
    public String getResultString() {
        LOGGER.debug("request :{}",getWebServicesString());
        resultString  = restTemplate.getForObject(getWebServicesString(), String.class);
        return resultString;
    }
    
    public String getResultString(String url) {
        LOGGER.debug("request :{}",url);
        resultString  = restTemplate.getForObject(url, String.class);
        return resultString;
    }
    
    public ResponseEntity<String> getResultString(String urlParam,HttpEntity<String> entity) {
    	String url = this.EngineServer +urlParam;
        LOGGER.info(" request :{}",url);
        return restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
    }
    
    public ResponseEntity<String> getResultByExchange(String urlParam) {
    	HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
    	String url = this.EngineServer +urlParam;
        LOGGER.info(" request :{}",url);
        return restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
    }
    
    public ResponseEntity<String> getResultStringByTypeHttpMethodAndBodyContent(String json,HttpMethod httpMethod,String url,RestTemplate restTemplate) {
        LOGGER.debug("url :{}", url);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");



        HttpEntity<String> entity = new HttpEntity<String>(json, headers);
        if(httpMethod==null){
            httpMethod = HttpMethod.GET;
        }
        ResponseEntity<String> reponseEntity = restTemplate.exchange(url, httpMethod, entity, String.class);
        return reponseEntity;
    }
    
    public ResponseEntity<String> getResultStringByTypeHttpMethodAndBodyContent(HttpMethod httpMethod,String urlParam) {
    	String url = this.EngineServer +urlParam;
        LOGGER.info("url :{}", url);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");



        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        if(httpMethod==null){
            httpMethod = HttpMethod.GET;
        }
        ResponseEntity<String> reponseEntity = restTemplate.exchange(url, httpMethod, entity, String.class);
        return reponseEntity;
    }
    
    
    
}
