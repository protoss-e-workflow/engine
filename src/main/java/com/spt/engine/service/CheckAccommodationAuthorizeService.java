package com.spt.engine.service;

import java.util.List;
import java.util.Map;

public interface CheckAccommodationAuthorizeService {



   public boolean checkAccommodationAuthorize(String docNumber,Long itemId,boolean isIbisStd,String zoneType, String diffDays);

}
