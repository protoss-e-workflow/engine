package com.spt.engine.service.Impl;

import com.google.gson.*;
import com.spt.engine.ConstantVariable;
import com.spt.engine.repository.app.custom.DocumentExpenseItemRepositoryCustom;
import com.spt.engine.service.AbstractSAPEngineService;
import com.spt.engine.service.InternalOrderService;
import org.exolab.castor.types.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service("InternalOrderService")
public class InternalOrderServiceImpl extends AbstractSAPEngineService implements InternalOrderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(InternalOrderServiceImpl.class);

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    @Autowired
    DocumentExpenseItemRepositoryCustom documentExpenseItemRepositoryCustom;

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    @Override
    public List<Map<String, Object>> readDataInternalOrderFromSapInterface() {

     LOGGER.info("========= START Fetching Data IO From Interface SAP ============");

        String csvFile = ConstantVariable.PATCH_FILE_TEST_IO.getAbsolutePath();
        String cvsSplitBy = ConstantVariable.PARAMETER_TO_SPLIT_CSV_FILE_IO;
        String line = "";
        BufferedReader br = null;
        List<Map<String,Object>> dataResult = new ArrayList<>();
        Map dataInternal_Order;


        try {

            br = new BufferedReader(new FileReader(csvFile));
            br.readLine();

            while ((line = br.readLine()) != null) {
                LOGGER.info("DATA in Line ====> {}",line);


                String[] internalOrderData = line.split(cvsSplitBy);
                LOGGER.info("COMPANY CODE ====> {}",internalOrderData[0]);
                LOGGER.info("IO CODE ====> {}",internalOrderData[1]);
                LOGGER.info("BALANCE AMOUNT ====> {}",internalOrderData[2]);
                LOGGER.info("DATE ====> {}",internalOrderData[3]);
                LOGGER.info("TIME ====> {}",internalOrderData[4]);

                dataInternal_Order = new HashMap();
                dataInternal_Order.put("companyCode",internalOrderData[0]);
                dataInternal_Order.put("ioCode",internalOrderData[1]);
                dataInternal_Order.put("balanceAmount",internalOrderData[2]);
                dataInternal_Order.put("date",internalOrderData[3]);
                dataInternal_Order.put("time",internalOrderData[4]);


                LOGGER.info("Result Company Code ==> {}",dataInternal_Order.get("companyCode"));
                LOGGER.info("Result ioCode ==> {}",dataInternal_Order.get("ioCode"));
                LOGGER.info("Result balanceAmount ==> {}",dataInternal_Order.get("balanceAmount"));
                LOGGER.info("Result date ==> {}",dataInternal_Order.get("date"));
                LOGGER.info("Result time ==> {}",dataInternal_Order.get("time"));








                dataResult.add(dataInternal_Order);




            }


            LOGGER.info("========= Finish Fetching Data IO From Interface SAP ============");
            return dataResult;


        } catch (FileNotFoundException e) {
            LOGGER.error("========= ERROR File Not Found !!! =========");
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            LOGGER.error("========= ERROR IO Exception !!! =========");
            e.printStackTrace();
            return null;
        } finally {
            if (br != null) {
                try {
                    br.close();
                    LOGGER.info("========= CLOSE FILE SUCCESS !!! =========");
                } catch (IOException e) {
                    LOGGER.error("========= ERROR CANNOT CLOSE FILE !!! =========");
                    e.printStackTrace();
                }
            }
        }






    }

    @Override
    public void moveFileWhenfetchAndSaveDataSuccess() {



        try{
            String pathBeforeMove = ConstantVariable.PATCH_FILE_TEST_IO.getPath();
            String pathAfterMoveMove = ConstantVariable.PATCH_FILE_TEST_TO_MOVE_BACKUP_DATA.getPath();

            LOGGER.info("============= Path File Before Move ============= : {}",pathBeforeMove);
            LOGGER.info("============= Path File After Move ============= : {}",pathAfterMoveMove);


            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date dateConvert = new Date();
            DateTime dateTime = new DateTime(dateConvert);
            String today = dateFormat.format(dateTime.toDate());


            File afile =new File(pathBeforeMove);

            String[] seperrateFile = afile.getName().split("\\.");
            String newNameFile = seperrateFile[0]+"_"+today+"."+seperrateFile[1];


            if(afile.renameTo(new File(pathAfterMoveMove +"/" +newNameFile))){

                LOGGER.info("========== File is moved successful! ==========");

            }else{

                LOGGER.info("========== File is failed to move! ==========");
            }

        }catch(Exception e){

            LOGGER.error("========== ERROR!!! in Method Move File !!! ==========");
            e.printStackTrace();
        }



    }

    @SuppressWarnings("Duplicates")
    @Override
    public Map<String, String> checkToRealtime(String gl, String costCenter, String io, String manOrder, String wbs,String docNumber,String expItemId) {
        LOGGER.info("========== checkToRealtime ==========");
        Map<String,String> mapCheckIo = new HashMap<>();
        mapCheckIo.put("GL_ACCOUNT",gl);
        mapCheckIo.put("COSTCENTER",costCenter);
        mapCheckIo.put("ORDER",io);
        mapCheckIo.put("MAN_ORDER",manOrder);
        mapCheckIo.put("WBS_NO",wbs);

        BigDecimal amountFromSap = BigDecimal.ZERO;
        BigDecimal amountFromEwf = BigDecimal.ZERO;
        BigDecimal amountReturn  = BigDecimal.ZERO;
        try{
            String urlParam = "/internalOrderRealtime";
            LOGGER.info("----------- Check IO From SAP -----------");
            String jsonData =gson.toJson(mapCheckIo,Map.class);
            ResponseEntity<String> responseData = postJsonData(urlParam,jsonData);
            Map<String,String> map = gson.fromJson(responseData.getBody(), Map.class);
            amountFromSap = new BigDecimal(map.get("AVAILABLE")).setScale(2,BigDecimal.ROUND_FLOOR);
            LOGGER.info("Amount From Sap    :    {}",amountFromSap);
            LOGGER.info("----------- Check IO From EWF -----------");
            List<Map<String,Object>> list = documentExpenseItemRepositoryCustom.checkBudgetInWorkflow(costCenter, gl, docNumber, expItemId);
            if(list.size()>0){
                for(Map<String,Object> objectMap : list){
                    BigDecimal amount = new BigDecimal(objectMap.get("AMOUNT").toString());
                    amountFromEwf = amountFromEwf.add(amount).setScale(2,BigDecimal.ROUND_FLOOR);
                }
            }
            LOGGER.info("Amount From EWF    :    {}",amountFromEwf);
            LOGGER.info("--------------------------------------------");
            amountReturn = amountFromSap.subtract(amountFromEwf).setScale(2,BigDecimal.ROUND_FLOOR);
            if (amountReturn.compareTo(BigDecimal.ZERO) > 0){
                map.put("AVAILABLE",String.valueOf(amountReturn));
            }else{
                map.put("AVAILABLE","0");
            }

            return map;
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }

    }

    @SuppressWarnings("Duplicates")
    @Override
    public List<Map<String,Object>>  checkBudgetInEwf(String gl, String costCenter) {
        LOGGER.info("========== checkBudgetInEwf ==========");
        try{
            List<Map<String,Object>> list = documentExpenseItemRepositoryCustom.checkBudgetInEwf(gl, costCenter);
            return list;
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }

    }
}

