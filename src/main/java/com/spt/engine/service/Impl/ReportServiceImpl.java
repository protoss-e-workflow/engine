package com.spt.engine.service.Impl;

import com.google.common.reflect.TypeToken;
import com.google.gson.*;
import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.*;
import com.spt.engine.entity.general.LocaleMessage;
import com.spt.engine.entity.general.Parameter;
import com.spt.engine.entity.general.ParameterDetail;
import com.spt.engine.repository.app.DocumentRepository;
import com.spt.engine.repository.general.LocaleMessageRepository;
import com.spt.engine.repository.general.ParameterRepository;
import com.spt.engine.service.EmplyeeProfileService;
import com.spt.engine.service.ReportService;
import com.spt.engine.util.AbstractReportJasperPDF;
import com.spt.engine.util.DateUtil;
import com.spt.engine.util.StringUtil;
import flexjson.JSONSerializer;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.*;

@Service("ReportService")
public class ReportServiceImpl implements ReportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportServiceImpl.class);



    @Autowired
    DocumentRepository documentRepository;

    @Autowired
    LocaleMessageRepository localeMessageRepository;


    @Autowired
    EmplyeeProfileService employeeProfileService;


    @Autowired
    ParameterRepository parameterRepository;

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws com.google.gson.JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };


    protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();


    @Override
    public JasperPrint formReportApprove(String docNumber,
                                         Map<String,String> mapParam,
                                         String language) {
        JasperPrint jasperPrint = new JasperPrint();
        try {


            Document document = documentRepository.findByDocNumber(docNumber);
            Map<String,Object> map = new HashMap<String,Object>();

            LOGGER.info("Map Param    ===> {}",new JSONSerializer().deepSerialize(mapParam));


            /*Fetching Message*/


            mapParam.put("REPORT_APPROVE",findLocaleMessageByCode(mapParam.get("REPORT_APPROVE"),language));
            mapParam.put("LABEL_DOC_NUMBER",findLocaleMessageByCode(mapParam.get("LABEL_DOC_NUMBER"),language));
            mapParam.put("LABEL_EMPLOYEE_NAME",findLocaleMessageByCode(mapParam.get("LABEL_EMPLOYEE_NAME"),language));
            mapParam.put("LABEL_POSITION",findLocaleMessageByCode(mapParam.get("LABEL_POSITION"),language));
            mapParam.put("LABEL_DOCUMENT_CREATOR_NAME",findLocaleMessageByCode(mapParam.get("LABEL_DOCUMENT_CREATOR_NAME"),language));
            mapParam.put("LABEL_DOCUMENT_DATE",findLocaleMessageByCode(mapParam.get("LABEL_DOCUMENT_DATE"),language));
            mapParam.put("LABEL_COMPANY_AGENCY",findLocaleMessageByCode(mapParam.get("LABEL_COMPANY_AGENCY"),language));
            mapParam.put("LABEL_PSA_DETAIL",findLocaleMessageByCode(mapParam.get("LABEL_PSA_DETAIL"),language));
            mapParam.put("LABEL_COST_CENTER",findLocaleMessageByCode(mapParam.get("LABEL_COST_CENTER"),language));
            mapParam.put("LABEL_TRAVEL_INFORMATION",findLocaleMessageByCode(mapParam.get("LABEL_TRAVEL_INFORMATION"),language));
            mapParam.put("LABEL_ORIGIN",findLocaleMessageByCode(mapParam.get("LABEL_ORIGIN"),language));
            mapParam.put("LABEL_DESTINATION",findLocaleMessageByCode(mapParam.get("LABEL_DESTINATION"),language));
            mapParam.put("LABEL_START_DATE",findLocaleMessageByCode(mapParam.get("LABEL_START_DATE"),language));
            mapParam.put("LABEL_END_DATE",findLocaleMessageByCode(mapParam.get("LABEL_END_DATE"),language));
            mapParam.put("LABEL_REMARK",findLocaleMessageByCode(mapParam.get("LABEL_REMARK"),language));
            mapParam.put("LABEL_TRAVEL_MEMBER_NAME",findLocaleMessageByCode(mapParam.get("LABEL_TRAVEL_MEMBER_NAME"),language));
            mapParam.put("LABEL_EMPLOYEE_CODE",findLocaleMessageByCode(mapParam.get("LABEL_EMPLOYEE_CODE"),language));
            mapParam.put("LABEL_ADVANCE",findLocaleMessageByCode(mapParam.get("LABEL_ADVANCE"),language));
            mapParam.put("LABEL_MONEY_AMOUNT_BATH",findLocaleMessageByCode(mapParam.get("LABEL_MONEY_AMOUNT_BATH"),language));
            mapParam.put("LABEL_BORROW_DATE",findLocaleMessageByCode(mapParam.get("LABEL_BORROW_DATE"),language));
            mapParam.put("LABEL_DUE_DATE",findLocaleMessageByCode(mapParam.get("LABEL_DUE_DATE"),language));
            mapParam.put("LABEL_BANK_NUMBER",findLocaleMessageByCode(mapParam.get("LABEL_BANK_NUMBER"),language));
            mapParam.put("LABEL_CONTRACT_ACCOUNT",findLocaleMessageByCode(mapParam.get("LABEL_CONTRACT_ACCOUNT"),language));
            mapParam.put("LABEL_PAYMENT_TYPE",findLocaleMessageByCode(mapParam.get("LABEL_PAYMENT_TYPE"),language));
            mapParam.put("LABEL_ADMIN_INFORMATION",findLocaleMessageByCode(mapParam.get("LABEL_ADMIN_INFORMATION"),language));
            mapParam.put("LABEL_RESERVE_CAR",findLocaleMessageByCode(mapParam.get("LABEL_RESERVE_CAR"),language));
            mapParam.put("LABEL_BOOKING_HOTEL",findLocaleMessageByCode(mapParam.get("LABEL_BOOKING_HOTEL"),language));
            mapParam.put("LABEL_TICKET_BOOKING",findLocaleMessageByCode(mapParam.get("LABEL_TICKET_BOOKING"),language));
            mapParam.put("LABEL_CONTRACT_ADMIN",findLocaleMessageByCode(mapParam.get("LABEL_CONTRACT_ADMIN"),language));
            mapParam.put("LABEL_LINE_APPROVER",findLocaleMessageByCode(mapParam.get("LABEL_LINE_APPROVER"),language));
            mapParam.put("LABEL_ROLES",findLocaleMessageByCode(mapParam.get("LABEL_ROLES"),language));
            mapParam.put("LABEL_STATUS",findLocaleMessageByCode(mapParam.get("LABEL_STATUS"),language));
            mapParam.put("LABEL_TRAVEL_TYPE",findLocaleMessageByCode(mapParam.get("LABEL_TRAVEL_TYPE"),language));
            mapParam.put("LABEL_COMPANY_AGENCY_CODE",findLocaleMessageByCode(mapParam.get("LABEL_COMPANY_AGENCY_CODE"),language));
            mapParam.put("LABEL_COMPANY_AGENCY_NAME",findLocaleMessageByCode(mapParam.get("LABEL_COMPANY_AGENCY_NAME"),language));
            mapParam.put("LABEL_LOCATION_EMPLOYE",findLocaleMessageByCode(mapParam.get("LABEL_LOCATION_EMPLOYE"),language));
            mapParam.put("LABEL_COST_DETAIL_AND_ADVANCE",findLocaleMessageByCode(mapParam.get("LABEL_COST_DETAIL_AND_ADVANCE    "),language));
            mapParam.put("LABEL_LIST_COST",findLocaleMessageByCode(mapParam.get("LABEL_LIST_COST"),language));
            mapParam.put("LABEL_LOCATION_EMPLOYEE",findLocaleMessageByCode(mapParam.get("LABEL_LOCATION_EMPLOYEE"),language));






            if(docNumber != null){
                LOGGER.info("==-- Found DocNumber : [{}] --==",docNumber);


                /*Found employee data information*/
                ResponseEntity<String> ownerDoc = employeeProfileService.findEmployeeProfileByKeySearch(document.getRequester());
                Map employeeMap = gson.fromJson(ownerDoc.getBody(), Map.class);
                List employeeList = (List) employeeMap.get("restBodyList");
                employeeMap = (Map) employeeList.get(0);




                Request request = document.getRequest();
                List<RequestApprover> approverList = null;
                if(request != null){
                    approverList = new ArrayList<>(request.getRequestApprovers());
                }


                /*set Default picture Check Reservation*/
                map.put("carValue","0");
                map.put("hotelValue","0");
                map.put("flightTicketValue","0");

                List<DocumentApproveItem> documentApproveItem = new ArrayList<>(document.getDocumentApprove().getDocumentApproveItem());
                List<TravelDetail> listTravelDetail = null;
                List<TravelMember> listTravelMember = null;
                List<ExternalMember> listExternalTravelMember = null;
                String travelPurpose = "";

                for(DocumentApproveItem item : documentApproveItem){

                    if(item.getTravelDetail().size() > 0){
                        listTravelDetail = new ArrayList<>(item.getTravelDetails());
                    }

                    if(item.getTravelMember().size() > 0){
                        listTravelMember = new ArrayList<>(item.getTravelMember());
                    }

                    if(item.getExternalMember().size() > 0){
                        listExternalTravelMember = new ArrayList<>(item.getExternalMember());
                    }


                    travelPurpose = String.valueOf(item.getTravelReason());

                    String approveType = item.getApproveType();
//                        LOGGER.info("Approve Type : [{}]",approveType);



                    switch (approveType){
                        case ("0003") :
                            map.put("carValue","1");
                            break;
                        case ("0004") :
                            map.put("hotelValue","1");
                            break;
                        case ("0005") :
                            map.put("flightTicketValue","1");
                            break;
                        case ("0006") :
                            LOGGER.info("Approve Type  : [{}]",approveType);
                            map.put("docTypeValue",language.equals("en") ?
                                    "Domestic" : "ในประเทศ"
                            );
                            break;
                        case ("0007") :
                            LOGGER.info("Approve Type  : [{}]",approveType);
                            map.put("docTypeValue",language.equals("en") ?
                                    "Foreign" : "นอกประเทศ"
                            );
                            break;
                        default:
                            map.put("docTypeValue","");
                            map.put("carValue","");
                            map.put("hotelValue","");
                            map.put("flightTicketValue","");
                            break;
                    }


                }


                /*Fetching TravelDetail*/
                List<Map<String,String>> travelInformationList = new ArrayList<>();

                for(TravelDetail data : listTravelDetail){

                    Map<String,String> mapTravelDetail = new HashMap<>();
                    mapTravelDetail.put("origin",language.equals("en") ? data.getOrigin().getDescriptionEn() : data.getOrigin().getDescription());
                    mapTravelDetail.put("destination",language.equals("en") ? data.getDestination().getDescriptionEn() : data.getDestination().getDescription());
                    mapTravelDetail.put("startDate",String.valueOf(data.getStartDate()).substring(0,10));
                    mapTravelDetail.put("endDate",String.valueOf(data.getEndDate()).substring(0,10));
                    mapTravelDetail.put("remark",String.valueOf(data.getRemark()));

                    travelInformationList.add(mapTravelDetail);
                }

                /*Fetching TravelMember*/
                List<Map<String,String>> travelMemberList = new ArrayList<>();
                Map travelMemberMap = null;
                for(TravelMember data : listTravelMember){

                    Map<String,String> mapTravelDetail = new HashMap<>();


                    ResponseEntity<String> travelMember = employeeProfileService.findEmployeeProfileByKeySearch(data.getMemberUser());
                    travelMemberMap = gson.fromJson(travelMember.getBody(), Map.class);
                    List travelMemberLists = (List) travelMemberMap.get("restBodyList");
                    travelMemberMap = (Map) travelMemberLists.get(0);

                    mapTravelDetail.put("name",language.equals("en") ?
                            (String) travelMemberMap.get("NameEN") :
                            travelMemberMap.get("FOA")+" "+travelMemberMap.get("FNameTH")+" "+travelMemberMap.get("LNameTH")
                    );

                    mapTravelDetail.put("empCode", (String) travelMemberMap.get("Personal_ID"));
                    mapTravelDetail.put("position", (String) (language.equals("en") ?
                            travelMemberMap.get("PositionEN") :
                            travelMemberMap.get("PositionTH"))

                    );

                    travelMemberList.add(mapTravelDetail);
                }

                /*Fetching External Member*/
                if(listExternalTravelMember != null){

                    for(ExternalMember data : listExternalTravelMember){

                        Map<String,String> mapExternalMember = new HashMap<>();
                        mapExternalMember.put("name",data.getMemberName());
                        mapExternalMember.put("empCode","");
                        mapExternalMember.put("position","");
                        travelMemberList.add(mapExternalMember);

                    }
                }

                /*Fetching LineApprove*/
                List<Map<String,String>> lineApproverList = new ArrayList<>();
                Map approverMap = null;
                if(approverList.size() > 0){
                    LOGGER.info(" Approve List Size = [{}]",approverList.size());
                    Map<String,String> mapLineApprover = new HashMap<>();

                    /*Set First Approver is Requester*/
                    mapLineApprover.put("empName",language.equals("en") ?
                            (String) employeeMap.get("NameEN") :
                            employeeMap.get("FOA")+" "+employeeMap.get("FNameTH")+" "+employeeMap.get("LNameTH")
                    );
                    mapLineApprover.put("position", (String) (language.equals("en") ?
                            employeeMap.get("PositionEN") :
                            employeeMap.get("PositionTH")));
                    mapLineApprover.put("role",language.equals("en") ? "Requester" : "ผู้ขออนุมัติ");
                    mapLineApprover.put("status","");
                    lineApproverList.add(mapLineApprover);

                    Parameter parameter = null;
                    parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_IMG_LOGO_EWF);
                    String logoImg = "";
                    for(ParameterDetail detail:parameter.getDetails()){
                        logoImg = detail.getVariable2();
                    }

                    Collections.sort(approverList, new Comparator<RequestApprover>() {
                        @Override
                        public int compare(RequestApprover lhs, RequestApprover rhs) {
                            return lhs.getSequence().compareTo(rhs.getSequence());
                        }
                    });

                    for(RequestApprover data : approverList){
                        mapLineApprover = new HashMap<>();

                        String userNameApprover = data.getUserNameApprover();



                        if(userNameApprover != null){
                            ResponseEntity<String> approverJson = employeeProfileService.findEmployeeProfileByKeySearch(userNameApprover);
                            LOGGER.info("userNameApprover Line Approve ==> {}",userNameApprover);
//                            LOGGER.info("approverJson Line Approve ==> {}",approverJson);
                            approverMap = gson.fromJson(approverJson.getBody(), Map.class);
                            List approverListJson = (List) approverMap.get("restBodyList");
                            approverMap = (Map) approverListJson.get(0);


                            mapLineApprover.put("empName",language.equals("en") ?
                                    (String) approverMap.get("NameEN") :
                                    approverMap.get("FOA")+" "+approverMap.get("FNameTH")+" "+approverMap.get("LNameTH")
                            );
                            mapLineApprover.put("position", (String) (language.equals("en") ?
                                    approverMap.get("PositionEN") :
                                    approverMap.get("PositionTH")));
                            mapLineApprover.put("role",data.getActionStateName());
                            mapLineApprover.put("status",data.getActionTime() != null ? (language.equals("en") || language.equals("en_US") ? "Approved" : "อนุมัติแล้ว") : (language.equals("en") || language.equals("en_US")? "Waiting" : "รอการอนุมัติ"));
                            mapLineApprover.put("statusImage",data.getActionTime() != null ? logoImg.concat("icon_approve.png") : logoImg.concat("icon_waiting.png"));


                        }else{


                            mapLineApprover.put("empName", "");
                            mapLineApprover.put("position","");
                            mapLineApprover.put("role",data.getActionStateName());
                            mapLineApprover.put("status",data.getActionTime() != null ? (language.equals("en") || language.equals("en_US") ? "Approved" : "อนุมัติแล้ว") : (language.equals("en") || language.equals("en_US")? "Waiting" : "รอการอนุมัติ"));
                            mapLineApprover.put("statusImage",data.getActionTime() != null ? logoImg.concat("icon_approve.png") : logoImg.concat("icon_waiting.png"));


                        }




                        lineApproverList.add(mapLineApprover);
                    }
                }


                /*Fetching Advance Data*/






                /*Reservation admin*/




//                LOGGER.info("userName : {}",document.getRequest());
//                LOGGER.info("Data Employee ===> [{}]",new JSONSerializer().deepSerialize(employeeMap));
                LOGGER.info("Doc Date : [{}]",document.getCreatedDate());

                /*Header Form Report [Information Section]  */

                /*Employee data information*/



                Map creatorMap = null;
                Map accountMap = null;
                Map admintMap = null;
                String contractAccount = "";
                String contractAdmin = "";
                if(document !=  null){

                    ResponseEntity<String> ownerDocCreator = employeeProfileService.findEmployeeProfileByKeySearch(document.getCreatedBy());
                    creatorMap = gson.fromJson(ownerDocCreator.getBody(), Map.class);
                    List creatorList = (List) creatorMap.get("restBodyList");
                    creatorMap = (Map) creatorList.get(0);

                    String papsa = employeeMap.get("Position_PA_ID")+"-"+employeeMap.get("Personal_PSA_ID");
                    String personalId = String.valueOf(employeeMap.get("Position_PA_ID"));


                    ResponseEntity<String> ownerAccount = employeeProfileService.findAccountByPaPsaAndKeySearch(papsa,personalId);
                    accountMap = gson.fromJson(ownerAccount.getBody(), Map.class);


                    String accountData = String.valueOf(accountMap.get("profile"));
                    String accountDataSplit[] =  accountData.split(":");
                    if(!accountDataSplit[1].equals("0")){
                        String accountName = (accountDataSplit[1].split("-"))[0];
                        LOGGER.info("accountName ==> {}",accountName);


                        Map accMap = null;
                        ResponseEntity<String> contractAcc = employeeProfileService.findEmployeeProfileByKeySearch(accountName);
                        accMap = gson.fromJson(contractAcc.getBody(), Map.class);
                        List accList = (List) accMap.get("restBodyList");
                        accMap = (Map) accList.get(0);

                        contractAccount = language.equals("th") ?
                                accMap.get("FOA")+" "+accMap.get("FNameTH")+" "+accMap.get("LNameTH") :
                                String.valueOf(accMap.get("NameEN"));



                    }


                    ResponseEntity<String> ownerAdmin = employeeProfileService.findAdminByPaPsaAndKeySearch(papsa,document.getRequester());
                    admintMap = gson.fromJson(ownerAdmin.getBody(), Map.class);
                    LOGGER.info("ownerAdmin ==> {}",admintMap);

                    String adminData = String.valueOf(admintMap.get("profile"));
                    String adminDataSplit[] =  adminData.split(":");

                    if(!adminDataSplit[1].equals("0")){
                        String adminName = (adminDataSplit[1].split("-"))[0];
                        LOGGER.info("accountName ==> {}",adminName);


                        Map adminMap = null;
                        ResponseEntity<String> contractAcc = employeeProfileService.findEmployeeProfileByKeySearch(adminName);
                        adminMap = gson.fromJson(contractAcc.getBody(), Map.class);
                        List adminList = (List) adminMap.get("restBodyList");
                        adminMap = (Map) adminList.get(0);

                        contractAdmin = language.equals("th") ?
                                adminMap.get("FOA")+" "+adminMap.get("FNameTH")+" "+adminMap.get("LNameTH") :
                                String.valueOf(adminMap.get("NameEN"));



                    }




                }



                DocumentAdvance documentAdvance = document.getDocumentAdvance();
                if(documentAdvance != null && (documentAdvance.getAmount() >0) ){
                    map.put("advanceAmountValue", StringUtils.defaultString(String.valueOf(documentAdvance.getAmount())));
                    map.put("advancePaymentTypeValue", language.equals("en") ? StringUtils.defaultString("Tranfer to Account") :
                            StringUtils.defaultString("โอนเข้าบัญชีพนักงาน"));
                    map.put("advanceBookbankValue", StringUtils.defaultString(documentAdvance.getBankNumber()));
                    map.put("advanceBorrowDateValue", StringUtils.defaultString(String.valueOf(documentAdvance.getStartDate())).length() > 10 ? (StringUtils.defaultString(String.valueOf(documentAdvance.getStartDate()))).substring(0,10) : (""));
                    map.put("advanceDuedateValue", StringUtils.defaultString(String.valueOf(documentAdvance.getDueDate())).length() > 10 ? (StringUtils.defaultString(String.valueOf(documentAdvance.getDueDate())).substring(0,10)) : (""));
                    map.put("advanceContractValue",contractAccount);
                }else{
                    map.put("advanceAmountValue","");
                    map.put("advancePaymentTypeValue","");
                    map.put("advanceBookbankValue","");
                    map.put("advanceBorrowDateValue","");
                    map.put("advanceDuedateValue","");
                    map.put("advanceContractValue","");
                }

                map.put("reservationContractValue",contractAdmin);



                map.put("reportName",mapParam.get("REPORT_APPROVE"));

                map.put("docDateValue", String.valueOf(DateUtil.getDateWithRemoveTime(document.getCreatedDate())).length() >=10
                        ? String.valueOf(DateUtil.getDateWithRemoveTime(document.getCreatedDate())).substring(0,10)
                        : "" );

                if(creatorMap != null){
                    map.put("documentCreatorValue",language.equals("en") ?
                            creatorMap.get("NameEN") :
                            creatorMap.get("FOA")+" "+employeeMap.get("FNameTH")+" "+employeeMap.get("LNameTH")
                    );
                }else {
                    map.put("documentCreatorValue","");
                }




                map.put("docNumberValue",docNumber);
                map.put("employeeCodeValue",employeeMap.get("Personal_ID"));
                map.put("compaynyCodeValue",employeeMap.get("Position_PA_ID"));
                map.put("companyNameValue",employeeMap.get("Position_PA_Name"));
                map.put("costCenterValue",employeeMap.get("Position_Cost_Center"));
                map.put("locationValue",employeeMap.get("Personal_PSA_ID") +" : "+employeeMap.get("Personal_PSA_Name"));
                map.put("employeeValue",language.equals("th") ?
                        employeeMap.get("FOA")+" "+employeeMap.get("FNameTH")+" "+employeeMap.get("LNameTH") :
                        employeeMap.get("NameEN")
                );

                map.put("positionValue", language.equals("en") ?
                        employeeMap.get("PositionEN") :
                        employeeMap.get("PositionTH")

                );



                /* Set Message Label */
                map.put("docDate",mapParam.get("LABEL_DOCUMENT_DATE"));
                map.put("documentCreator",mapParam.get("LABEL_DOCUMENT_CREATOR_NAME"));
                map.put("docNumber", mapParam.get("LABEL_DOC_NUMBER"));
                map.put("docType", mapParam.get("LABEL_TRAVEL_TYPE"));
                map.put("employeeName",mapParam.get("LABEL_EMPLOYEE_NAME"));
                map.put("employeeCode", mapParam.get("LABEL_EMPLOYEE_CODE"));
                map.put("position", mapParam.get("LABEL_POSITION"));
                map.put("compaynyCode", mapParam.get("LABEL_COMPANY_AGENCY_CODE"));
                map.put("companyName", mapParam.get("LABEL_COMPANY_AGENCY_NAME"));
                map.put("costCenter", mapParam.get("LABEL_COST_CENTER"));
                map.put("location", mapParam.get("LABEL_LOCATION_EMPLOYEE"));
                map.put("travelDetail", mapParam.get("LABEL_TRAVEL_INFORMATION"));
                map.put("travelMember", mapParam.get("LABEL_TRAVEL_MEMBER_NAME"));
                map.put("travelCost",mapParam.get("LABEL_COST_DETAIL_AND_ADVANCE"));
                map.put("advanceDetail", mapParam.get("LABEL_ADVANCE"));
                map.put("advanceAmount", mapParam.get("LABEL_MONEY_AMOUNT_BATH"));
                map.put("advancePaymentType", mapParam.get("LABEL_PAYMENT_TYPE"));
                map.put("advanceBookbank", mapParam.get("LABEL_BANK_NUMBER"));
                map.put("advanceBorrowDate", mapParam.get("LABEL_BORROW_DATE"));
                map.put("advanceDuedate", mapParam.get("LABEL_DUE_DATE"));
                map.put("advanceContract", mapParam.get("LABEL_CONTRACT_ACCOUNT"));
                map.put("reservationDetail",mapParam.get("LABEL_ADMIN_INFORMATION"));
                map.put("hotel", mapParam.get("LABEL_BOOKING_HOTEL"));
                map.put("car", mapParam.get("LABEL_RESERVE_CAR"));
                map.put("flightTicket", mapParam.get("LABEL_TICKET_BOOKING"));
                map.put("reservationContract", mapParam.get("LABEL_CONTRACT_ADMIN"));
                map.put("lineapprove", mapParam.get("LABEL_LINE_APPROVER"));
                map.put("headerTravelorigin",mapParam.get("LABEL_ORIGIN"));
                map.put("headerTravelDestination",mapParam.get("LABEL_DESTINATION"));
                map.put("headerTravelStartDate",mapParam.get("LABEL_START_DATE"));
                map.put("headerTravelEndDate",mapParam.get("LABEL_END_DATE"));
                map.put("headerTravelRemark",mapParam.get("LABEL_REMARK"));
                map.put("headerMemberName",mapParam.get("LABEL_EMPLOYEE_NAME"));
                map.put("headerMemberCode",mapParam.get("LABEL_EMPLOYEE_CODE"));
                map.put("headerMemberPosition",mapParam.get("LABEL_POSITION"));
                map.put("headerNameLineApprove",mapParam.get("LABEL_EMPLOYEE_NAME"));
                map.put("headerPositionLineApprove",mapParam.get("LABEL_POSITION"));
                map.put("headerRoleLineApprove",mapParam.get("LABEL_ROLES"));
                map.put("headerStatusLineApprove",mapParam.get("LABEL_STATUS"));
                map.put("headerListCost",mapParam.get("LABEL_LIST_COST"));
                map.put("headerAmountCost",mapParam.get("LABEL_MONEY_AMOUNT_BATH"));


                /*Fetching List Cost Travel*/
                List<Map<String,String>> listTravelCost = new ArrayList<>();
                Map<String,String> mapTravelCost = new HashMap<>();
                mapTravelCost.put("list","");
                mapTravelCost.put("amount","");
                listTravelCost.add(mapTravelCost);

                map.put("listTravelInformation",new JRBeanCollectionDataSource(travelInformationList));
                map.put("listTravelMember",new JRBeanCollectionDataSource(travelMemberList));
                map.put("listTravelCost",new JRBeanCollectionDataSource(listTravelCost));
                map.put("listLineapprove",new JRBeanCollectionDataSource(lineApproverList));



                Parameter parameter = null;

                parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_IMG_CHECK);
                String imgCheck = "";
                for(ParameterDetail detail:parameter.getDetails()){
                    imgCheck = detail.getVariable2()+detail.getVariable1();
                }

                parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_IMG_CHECKED);
                String imgChecked = "";
                for(ParameterDetail detail:parameter.getDetails()){
                    imgChecked = detail.getVariable2()+detail.getVariable1();
                }

                parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_IMG_LOGO_EWF);
                String logoImg = "";
                String pathImg = "";
                for(ParameterDetail detail:parameter.getDetails()){
                    logoImg = detail.getVariable2()+detail.getVariable1();
                    pathImg = detail.getVariable2();
                }



                map.put("logoImg",logoImg);
                map.put("imgCheck",imgCheck);
                map.put("imgChecked",imgChecked);
                map.put("imgApproved",pathImg + "icon_approve.png");
                map.put("imgWaiting",pathImg + "icon_waiting.png");
                map.put("detailImgApproved",(language.equals("en") || language.equals("en_US") ? "Approved" : "อนุมัติแล้ว"));
                map.put("detailImgWaiting",(language.equals("en") || language.equals("en_US")? "Waiting" : "รอการอนุมัติ"));

            }

            ExpenseType aa = new ExpenseType();

            String jasperFileName = "formApprove.jasper";
            jasperPrint = AbstractReportJasperPDF.exportReport(jasperFileName,Arrays.asList(aa), map);
        }catch (Exception e){
            LOGGER.error("ERROR : {}",e.getMessage());
            e.printStackTrace();
        }
        return  jasperPrint;
    }




    @Override
    public JasperPrint formReportAdvance(String docNumber,
                                         Map<String,String> mapParam,
                                         String language) {
        JasperPrint jasperPrint = new JasperPrint();
        try {


            Document document = documentRepository.findByDocNumber(docNumber);
            Map<String,Object> map = new HashMap<String,Object>();



            /*Fetching Message*/


            mapParam.put("REPORT_ADVANCE",findLocaleMessageByCode(mapParam.get("REPORT_ADVANCE"),language));
            mapParam.put("LABEL_DOC_NUMBER",findLocaleMessageByCode(mapParam.get("LABEL_DOC_NUMBER"),language));
            mapParam.put("LABEL_EMPLOYEE_NAME",findLocaleMessageByCode(mapParam.get("LABEL_EMPLOYEE_NAME"),language));
            mapParam.put("LABEL_POSITION",findLocaleMessageByCode(mapParam.get("LABEL_POSITION"),language));
            mapParam.put("LABEL_DOCUMENT_CREATOR_NAME",findLocaleMessageByCode(mapParam.get("LABEL_DOCUMENT_CREATOR_NAME"),language));
            mapParam.put("LABEL_DOCUMENT_DATE",findLocaleMessageByCode(mapParam.get("LABEL_DOCUMENT_DATE"),language));
            mapParam.put("LABEL_COMPANY_AGENCY",findLocaleMessageByCode(mapParam.get("LABEL_COMPANY_AGENCY"),language));
            mapParam.put("LABEL_PSA_DETAIL",findLocaleMessageByCode(mapParam.get("LABEL_PSA_DETAIL"),language));
            mapParam.put("LABEL_COST_CENTER",findLocaleMessageByCode(mapParam.get("LABEL_COST_CENTER"),language));
            mapParam.put("LABEL_TRAVEL_INFORMATION",findLocaleMessageByCode(mapParam.get("LABEL_TRAVEL_INFORMATION"),language));
            mapParam.put("LABEL_ORIGIN",findLocaleMessageByCode(mapParam.get("LABEL_ORIGIN"),language));
            mapParam.put("LABEL_DESTINATION",findLocaleMessageByCode(mapParam.get("LABEL_DESTINATION"),language));
            mapParam.put("LABEL_START_DATE",findLocaleMessageByCode(mapParam.get("LABEL_START_DATE"),language));
            mapParam.put("LABEL_END_DATE",findLocaleMessageByCode(mapParam.get("LABEL_END_DATE"),language));
            mapParam.put("LABEL_REMARK",findLocaleMessageByCode(mapParam.get("LABEL_REMARK"),language));
            mapParam.put("LABEL_TRAVEL_MEMBER_NAME",findLocaleMessageByCode(mapParam.get("LABEL_TRAVEL_MEMBER_NAME"),language));
            mapParam.put("LABEL_EMPLOYEE_CODE",findLocaleMessageByCode(mapParam.get("LABEL_EMPLOYEE_CODE"),language));
            mapParam.put("LABEL_ADVANCE",findLocaleMessageByCode(mapParam.get("LABEL_ADVANCE"),language));
            mapParam.put("LABEL_MONEY_AMOUNT_BATH",findLocaleMessageByCode(mapParam.get("LABEL_MONEY_AMOUNT_BATH"),language));
            mapParam.put("LABEL_BORROW_DATE",findLocaleMessageByCode(mapParam.get("LABEL_BORROW_DATE"),language));
            mapParam.put("LABEL_DUE_DATE",findLocaleMessageByCode(mapParam.get("LABEL_DUE_DATE"),language));
            mapParam.put("LABEL_BANK_NUMBER",findLocaleMessageByCode(mapParam.get("LABEL_BANK_NUMBER"),language));
            mapParam.put("LABEL_CONTRACT_ACCOUNT",findLocaleMessageByCode(mapParam.get("LABEL_CONTRACT_ACCOUNT"),language));
            mapParam.put("LABEL_PAYMENT_TYPE",findLocaleMessageByCode(mapParam.get("LABEL_PAYMENT_TYPE"),language));
            mapParam.put("LABEL_ADMIN_INFORMATION",findLocaleMessageByCode(mapParam.get("LABEL_ADMIN_INFORMATION"),language));
            mapParam.put("LABEL_RESERVE_CAR",findLocaleMessageByCode(mapParam.get("LABEL_RESERVE_CAR"),language));
            mapParam.put("LABEL_BOOKING_HOTEL",findLocaleMessageByCode(mapParam.get("LABEL_BOOKING_HOTEL"),language));
            mapParam.put("LABEL_TICKET_BOOKING",findLocaleMessageByCode(mapParam.get("LABEL_TICKET_BOOKING"),language));
            mapParam.put("LABEL_CONTRACT_ADMIN",findLocaleMessageByCode(mapParam.get("LABEL_CONTRACT_ADMIN"),language));
            mapParam.put("LABEL_LINE_APPROVER",findLocaleMessageByCode(mapParam.get("LABEL_LINE_APPROVER"),language));
            mapParam.put("LABEL_ROLES",findLocaleMessageByCode(mapParam.get("LABEL_ROLES"),language));
            mapParam.put("LABEL_STATUS",findLocaleMessageByCode(mapParam.get("LABEL_STATUS"),language));
            mapParam.put("LABEL_TRAVEL_TYPE",findLocaleMessageByCode(mapParam.get("LABEL_TRAVEL_TYPE"),language));
            mapParam.put("LABEL_COMPANY_AGENCY_CODE",findLocaleMessageByCode(mapParam.get("LABEL_COMPANY_AGENCY_CODE"),language));
            mapParam.put("LABEL_COMPANY_AGENCY_NAME",findLocaleMessageByCode(mapParam.get("LABEL_COMPANY_AGENCY_NAME"),language));
            mapParam.put("LABEL_LOCATION_EMPLOYE",findLocaleMessageByCode(mapParam.get("LABEL_LOCATION_EMPLOYE"),language));
            mapParam.put("LABEL_COST_DETAIL_AND_ADVANCE",findLocaleMessageByCode(mapParam.get("LABEL_COST_DETAIL_AND_ADVANCE    "),language));
            mapParam.put("LABEL_LIST_COST",findLocaleMessageByCode(mapParam.get("LABEL_LIST_COST"),language));
            mapParam.put("LABEL_LOCATION_EMPLOYEE",findLocaleMessageByCode(mapParam.get("LABEL_LOCATION_EMPLOYEE"),language));
            mapParam.put("LABEL_DOC_REF",findLocaleMessageByCode(mapParam.get("LABEL_DOC_REF"),language));
            mapParam.put("LABEL_ADVANCE_REMARK",findLocaleMessageByCode(mapParam.get("LABEL_ADVANCE_REMARK"),language));
            mapParam.put("LABEL_ADVANCE_PURPOSE",findLocaleMessageByCode(mapParam.get("LABEL_ADVANCE_PURPOSE"),language));






            if(docNumber != null){
                LOGGER.info("==-- Found DocNumber : [{}] --==",docNumber);


                /*Found employee data information*/
                ResponseEntity<String> ownerDoc = employeeProfileService.findEmployeeProfileByKeySearch(document.getRequester());
                Map employeeMap = gson.fromJson(ownerDoc.getBody(), Map.class);
                List employeeList = (List) employeeMap.get("restBodyList");
                employeeMap = (Map) employeeList.get(0);




                Request request = document.getRequest();
                List<RequestApprover> approverList = null;
                List<RequestApprover> approverListTemp =new ArrayList<>();
                if(request != null){
                    approverList = new ArrayList<>(request.getRequestApprovers());

                    int sizeApproveerList = approverList.size();
                    int count=1;

                    while(count-1 != sizeApproveerList){
                        for(int i =0; i<sizeApproveerList; i++){

                            if(Integer.valueOf(approverList.get(i).getSequence()) == count){
                                approverListTemp.add(approverList.get(i));
                                count++;
                                break;

                            }

                        }




                    }

                }





                /*Fetching LineApprove*/
                List<Map<String,String>> lineApproverList = new ArrayList<>();
                Map approverMap = null;
                if(approverListTemp != null){
                    LOGGER.info(" Approve List Size = [{}]",approverListTemp.size());
                    Map<String,String> mapLineApprover = new HashMap<>();

                    /*Set First Approver is Requester*/
                    mapLineApprover.put("empName",language.equals("en") ?
                            (String) employeeMap.get("NameEN") :
                            employeeMap.get("FOA")+" "+employeeMap.get("FNameTH")+" "+employeeMap.get("LNameTH")
                    );
                    mapLineApprover.put("position", (String) (language.equals("en") ?
                            employeeMap.get("PositionEN") :
                            employeeMap.get("PositionTH")));
                    mapLineApprover.put("role",language.equals("en") ? "Requester" : "ผู้ขออนุมัติ");
                    mapLineApprover.put("status","");
                    lineApproverList.add(mapLineApprover);

                    Parameter parameter = null;
                    parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_IMG_LOGO_EWF);
                    String logoImg = "";
                    for(ParameterDetail detail:parameter.getDetails()){
                        logoImg = detail.getVariable2();
                    }

                    Collections.sort(approverListTemp, new Comparator<RequestApprover>() {
                        @Override
                        public int compare(RequestApprover lhs, RequestApprover rhs) {
                            return lhs.getSequence().compareTo(rhs.getSequence());
                        }
                    });


                    for(int i=0; i<approverListTemp.size(); i++){

                        mapLineApprover = new HashMap<>();
                        String userNameApprover = approverListTemp.get(i).getUserNameApprover();


                        if(userNameApprover != null){
                            ResponseEntity<String> approverJson = employeeProfileService.findEmployeeProfileByKeySearch(userNameApprover);
                            LOGGER.info("userNameApprover Line Approve ==> {}",userNameApprover);
//                            LOGGER.info("approverJson Line Approve ==> {}",approverJson);
                            approverMap = gson.fromJson(approverJson.   getBody(), Map.class);
                            List approverListJson = (List) approverMap.get("restBodyList");
                            approverMap = (Map) approverListJson.get(0);


                            mapLineApprover.put("empName",language.equals("en") ?
                                    (String) approverMap.get("NameEN") :
                                    approverMap.get("FOA")+" "+approverMap.get("FNameTH")+" "+approverMap.get("LNameTH")
                            );
                            mapLineApprover.put("position", (String) (language.equals("en") ?
                                    approverMap.get("PositionEN") :
                                    approverMap.get("PositionTH")));
                            mapLineApprover.put("role",approverListTemp.get(i).getActionStateName());
                            mapLineApprover.put("status",approverListTemp.get(i).getActionTime() != null ? (language.equals("en") || language.equals("en_US") ? "Approved" : "อนุมัติแล้ว") : (language.equals("en") ? "Waiting" : "รอการอนุมัติ"));
                            mapLineApprover.put("statusImage",approverListTemp.get(i).getActionTime()  != null ? logoImg.concat("icon_approve.png") : logoImg.concat("icon_waiting.png"));

                        }else{




                            mapLineApprover.put("empName", "");
                            mapLineApprover.put("position","");
                            mapLineApprover.put("role",approverListTemp.get(i).getActionStateName());
                            mapLineApprover.put("status",approverListTemp.get(i).getActionTime() != null ? (language.equals("en") || language.equals("en_US") ? "Approved" : "อนุมัติแล้ว") : (language.equals("en") ? "Waiting" : "รอการอนุมัติ"));
                            mapLineApprover.put("statusImage",approverListTemp.get(i).getActionTime() != null ? logoImg.concat("icon_approve.png") : logoImg.concat("icon_waiting.png"));


                        }


                        lineApproverList.add(mapLineApprover);


                    }


                }


                /*Fetching Advance Data*/






                /*Reservation admin*/
                map.put("reservationContractValue","");



//                LOGGER.info("userName : {}",document.getRequest());
//                LOGGER.info("Data Employee ===> [{}]",new JSONSerializer().deepSerialize(employeeMap));
                LOGGER.info("Doc Date : [{}]",document.getCreatedDate());

                /*Header Form Report [Information Section]  */

                /*Employee data information*/



                Map creatorMap = null;
                Map accountMap = null;
                String contractAccount = "";
                if(document !=  null){

                    ResponseEntity<String> ownerDocCreator = employeeProfileService.findEmployeeProfileByKeySearch(document.getCreatedBy());
                    creatorMap = gson.fromJson(ownerDocCreator.getBody(), Map.class);
                    List creatorList = (List) creatorMap.get("restBodyList");
                    creatorMap = (Map) creatorList.get(0);

                    String papsa = employeeMap.get("Position_PA_ID")+"-"+employeeMap.get("Personal_PSA_ID");
                    String personalId = String.valueOf(employeeMap.get("Position_PA_ID"));


                    ResponseEntity<String> ownerAccount = employeeProfileService.findAccountByPaPsaAndKeySearch(papsa,personalId);
                    accountMap = gson.fromJson(ownerAccount.getBody(), Map.class);


                    String accountData = String.valueOf(accountMap.get("profile"));
                    String accountDataSplit[] =  accountData.split(":");
                    if(!accountDataSplit[1].equals("0")){
                        String accountName = (accountDataSplit[1].split("-"))[0];
                        LOGGER.info("accountName ==> {}",accountName);


                        Map accMap = null;
                        ResponseEntity<String> contractAcc = employeeProfileService.findEmployeeProfileByKeySearch(accountName);
                        accMap = gson.fromJson(contractAcc.getBody(), Map.class);
                        List accList = (List) accMap.get("restBodyList");
                        accMap = (Map) accList.get(0);

                        contractAccount = language.equals("th") ?
                                accMap.get("FOA")+" "+accMap.get("FNameTH")+" "+accMap.get("LNameTH") :
                                String.valueOf(accMap.get("NameEN"));



                    }

                }



                DocumentAdvance documentAdvance = document.getDocumentAdvance();
                if(documentAdvance != null && documentAdvance.getAmount() >0 ){




                    map.put("advancePurposeValue", StringUtils.defaultString(String.valueOf(documentAdvance.getAdvancePurpose())));
                    map.put("advanceRemarkValue", StringUtils.defaultString(String.valueOf(documentAdvance.getRemark())));
                    map.put("advanceRefValue",  document.getDocumentReference().size() > 0   ?   StringUtils.defaultString(String.valueOf(new ArrayList<>(document.getDocumentReference()).get(0).getDocReferenceNumber()))   :
                            "");
                    map.put("advanceAmountValue", StringUtils.defaultString(String.valueOf(documentAdvance.getAmount())));
                    map.put("advancePaymentTypeValue", language.equals("en") ? StringUtils.defaultString("Tranfer to Account") :
                            StringUtils.defaultString("โอนเข้าบัญชีพนักงาน"));
                    map.put("advanceBookbankValue", StringUtils.defaultString(documentAdvance.getBankNumber()));
                    map.put("advanceBorrowDateValue", StringUtils.defaultString(String.valueOf(documentAdvance.getStartDate())).length() > 10 ? (StringUtils.defaultString(String.valueOf(documentAdvance.getStartDate()))).substring(0,10) : (""));
                    map.put("advanceDuedateValue", StringUtils.defaultString(String.valueOf(documentAdvance.getDueDate())).length() > 10 ? (StringUtils.defaultString(String.valueOf(documentAdvance.getDueDate())).substring(0,10)) : (""));
                    map.put("advanceContractValue",contractAccount);

                    if(documentAdvance.getExternalPaymentDocNumber()  != null){
                        map.put("advancePaymentNumber", StringUtils.defaultString(String.valueOf(documentAdvance.getExternalPaymentDocNumber())));

                    }else{
                        map.put("advancePaymentNumber", "");

                    }

                }else{
                    map.put("advancePurposeValue","");
                    map.put("advanceRemarkValue","");
                    map.put("advanceRefValue","");
                    map.put("advancePaymentNumber","");
                    map.put("advanceAmountValue","");
                    map.put("advancePaymentTypeValue","");
                    map.put("advanceBookbankValue","");
                    map.put("advanceBorrowDateValue","");
                    map.put("advanceDuedateValue","");
                    map.put("advanceContractValue","");
                }



                map.put("reportName",mapParam.get("REPORT_ADVANCE"));

                map.put("docDateValue", String.valueOf(DateUtil.getDateWithRemoveTime(document.getCreatedDate())).length() >=10
                        ? String.valueOf(DateUtil.getDateWithRemoveTime(document.getCreatedDate())).substring(0,10)
                        : "" );

                if(creatorMap != null){
                    map.put("documentCreatorValue",language.equals("en") ?
                            creatorMap.get("NameEN") :
                            creatorMap.get("FOA")+" "+employeeMap.get("FNameTH")+" "+employeeMap.get("LNameTH")
                    );
                }else {
                    map.put("documentCreatorValue","");
                }


                map.put("docNumberValue",docNumber);
                map.put("employeeCodeValue",employeeMap.get("Personal_ID"));
                map.put("compaynyCodeValue",employeeMap.get("Position_PA_ID"));
                map.put("companyNameValue",employeeMap.get("Position_PA_Name"));
                map.put("costCenterValue",employeeMap.get("Position_Cost_Center"));
                map.put("locationValue",employeeMap.get("Personal_PSA_ID") +" : "+employeeMap.get("Personal_PSA_Name"));
                map.put("employeeValue",language.equals("th") ?
                        employeeMap.get("FOA")+" "+employeeMap.get("FNameTH")+" "+employeeMap.get("LNameTH") :
                        employeeMap.get("NameEN")
                );

                map.put("positionValue", language.equals("en") ?
                        employeeMap.get("PositionEN") :
                        employeeMap.get("PositionTH")

                );



                /* Set Message Label */
                map.put("docDate",mapParam.get("LABEL_DOCUMENT_DATE"));
                map.put("documentCreator",mapParam.get("LABEL_DOCUMENT_CREATOR_NAME"));
                map.put("docNumber", mapParam.get("LABEL_DOC_NUMBER"));
                map.put("docType", mapParam.get("LABEL_TRAVEL_TYPE"));
                map.put("employeeName",mapParam.get("LABEL_EMPLOYEE_NAME"));
                map.put("employeeCode", mapParam.get("LABEL_EMPLOYEE_CODE"));
                map.put("position", mapParam.get("LABEL_POSITION"));
                map.put("compaynyCode", mapParam.get("LABEL_COMPANY_AGENCY_CODE"));
                map.put("companyName", mapParam.get("LABEL_COMPANY_AGENCY_NAME"));
                map.put("costCenter", mapParam.get("LABEL_COST_CENTER"));
                map.put("location", mapParam.get("LABEL_LOCATION_EMPLOYEE"));
                map.put("travelDetail", mapParam.get("LABEL_TRAVEL_INFORMATION"));
                map.put("travelMember", mapParam.get("LABEL_TRAVEL_MEMBER_NAME"));
                map.put("travelCost",mapParam.get("LABEL_COST_DETAIL_AND_ADVANCE"));
                map.put("advanceDetail", mapParam.get("LABEL_ADVANCE"));
                map.put("advanceAmount", mapParam.get("LABEL_MONEY_AMOUNT_BATH"));
                map.put("advancePaymentType", mapParam.get("LABEL_PAYMENT_TYPE"));
                map.put("advanceBookbank", mapParam.get("LABEL_BANK_NUMBER"));
                map.put("advanceBorrowDate", mapParam.get("LABEL_BORROW_DATE"));
                map.put("advanceDuedate", mapParam.get("LABEL_DUE_DATE"));
                map.put("advanceContract", mapParam.get("LABEL_CONTRACT_ACCOUNT"));
                map.put("reservationDetail",mapParam.get("LABEL_ADMIN_INFORMATION"));
                map.put("hotel", mapParam.get("LABEL_BOOKING_HOTEL"));
                map.put("car", mapParam.get("LABEL_RESERVE_CAR"));
                map.put("flightTicket", mapParam.get("LABEL_TICKET_BOOKING"));
                map.put("reservationContract", mapParam.get("LABEL_CONTRACT_ADMIN"));
                map.put("lineapprove", mapParam.get("LABEL_LINE_APPROVER"));

                map.put("headerNameLineApprove",mapParam.get("LABEL_EMPLOYEE_NAME"));
                map.put("headerPositionLineApprove",mapParam.get("LABEL_POSITION"));
                map.put("headerRoleLineApprove",mapParam.get("LABEL_ROLES"));
                map.put("headerStatusLineApprove",mapParam.get("LABEL_STATUS"));
                map.put("headerListCost",mapParam.get("LABEL_LIST_COST"));
                map.put("headerAmountCost",mapParam.get("LABEL_MONEY_AMOUNT_BATH"));

                map.put("advanceRef",mapParam.get("LABEL_DOC_REF"));
                map.put("advanceRemark",mapParam.get("LABEL_ADVANCE_REMARK"));
                map.put("advancePurpose",mapParam.get("LABEL_ADVANCE_PURPOSE"));


                map.put("listLineapprove",new JRBeanCollectionDataSource(lineApproverList));






                Parameter parameter = null;

                parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_IMG_LOGO_EWF);
                String logoImg = "";
                String pathImg = "";
                for(ParameterDetail detail:parameter.getDetails()){
                    logoImg = detail.getVariable2()+detail.getVariable1();
                    pathImg = detail.getVariable2();
                }

                map.put("logoImg",logoImg);
                map.put("imgApproved",pathImg + "icon_approve.png");
                map.put("imgWaiting",pathImg + "icon_waiting.png");
                map.put("detailImgApproved",(language.equals("en") || language.equals("en_US") ? "Approved" : "อนุมัติแล้ว"));
                map.put("detailImgWaiting",(language.equals("en") || language.equals("en_US")? "Waiting" : "รอการอนุมัติ"));


            }

            String jasperFileName = "formAdvance.jasper";
            jasperPrint = AbstractReportJasperPDF.exportReport(jasperFileName,Arrays.asList(document), map);
        }catch (Exception e){
            LOGGER.error("ERROR : {}",e.getMessage());
            e.printStackTrace();
        }
        return  jasperPrint;
    }

    @Override
    public JasperPrint formReportExpense(String docNumber,
                                         Map<String,String> mapParam,
                                         String language) {
        JasperPrint jasperPrint = new JasperPrint();
        try {


            Document document = documentRepository.findByDocNumber(docNumber);
            Map<String,Object> map = new HashMap<String,Object>();



            /*Fetching Message*/


            mapParam.put("REPORT_EXPENSE",findLocaleMessageByCode(mapParam.get("REPORT_EXPENSE"),language));
            mapParam.put("LABEL_DOC_NUMBER",findLocaleMessageByCode(mapParam.get("LABEL_DOC_NUMBER"),language));
            mapParam.put("LABEL_EMPLOYEE_NAME",findLocaleMessageByCode(mapParam.get("LABEL_EMPLOYEE_NAME"),language));
            mapParam.put("LABEL_POSITION",findLocaleMessageByCode(mapParam.get("LABEL_POSITION"),language));
            mapParam.put("LABEL_DOCUMENT_CREATOR_NAME",findLocaleMessageByCode(mapParam.get("LABEL_DOCUMENT_CREATOR_NAME"),language));
            mapParam.put("LABEL_DOCUMENT_DATE",findLocaleMessageByCode(mapParam.get("LABEL_DOCUMENT_DATE"),language));
            mapParam.put("LABEL_COMPANY_AGENCY",findLocaleMessageByCode(mapParam.get("LABEL_COMPANY_AGENCY"),language));
            mapParam.put("LABEL_PSA_DETAIL",findLocaleMessageByCode(mapParam.get("LABEL_PSA_DETAIL"),language));
            mapParam.put("LABEL_COST_CENTER",findLocaleMessageByCode(mapParam.get("LABEL_COST_CENTER"),language));
            mapParam.put("LABEL_TRAVEL_INFORMATION",findLocaleMessageByCode(mapParam.get("LABEL_TRAVEL_INFORMATION"),language));
            mapParam.put("LABEL_ORIGIN",findLocaleMessageByCode(mapParam.get("LABEL_ORIGIN"),language));
            mapParam.put("LABEL_DESTINATION",findLocaleMessageByCode(mapParam.get("LABEL_DESTINATION"),language));
            mapParam.put("LABEL_START_DATE",findLocaleMessageByCode(mapParam.get("LABEL_START_DATE"),language));
            mapParam.put("LABEL_END_DATE",findLocaleMessageByCode(mapParam.get("LABEL_END_DATE"),language));
            mapParam.put("LABEL_REMARK",findLocaleMessageByCode(mapParam.get("LABEL_REMARK"),language));
            mapParam.put("LABEL_TRAVEL_MEMBER_NAME",findLocaleMessageByCode(mapParam.get("LABEL_TRAVEL_MEMBER_NAME"),language));
            mapParam.put("LABEL_EMPLOYEE_CODE",findLocaleMessageByCode(mapParam.get("LABEL_EMPLOYEE_CODE"),language));
            mapParam.put("LABEL_ADVANCE",findLocaleMessageByCode(mapParam.get("LABEL_ADVANCE"),language));
            mapParam.put("LABEL_BORROW_DATE",findLocaleMessageByCode(mapParam.get("LABEL_BORROW_DATE"),language));
            mapParam.put("LABEL_DUE_DATE",findLocaleMessageByCode(mapParam.get("LABEL_DUE_DATE"),language));
            mapParam.put("LABEL_BANK_NUMBER",findLocaleMessageByCode(mapParam.get("LABEL_BANK_NUMBER"),language));
            mapParam.put("LABEL_CONTRACT_ACCOUNT",findLocaleMessageByCode(mapParam.get("LABEL_CONTRACT_ACCOUNT"),language));
            mapParam.put("LABEL_PAYMENT_TYPE",findLocaleMessageByCode(mapParam.get("LABEL_PAYMENT_TYPE"),language));
            mapParam.put("LABEL_ADMIN_INFORMATION",findLocaleMessageByCode(mapParam.get("LABEL_ADMIN_INFORMATION"),language));
            mapParam.put("LABEL_RESERVE_CAR",findLocaleMessageByCode(mapParam.get("LABEL_RESERVE_CAR"),language));
            mapParam.put("LABEL_BOOKING_HOTEL",findLocaleMessageByCode(mapParam.get("LABEL_BOOKING_HOTEL"),language));
            mapParam.put("LABEL_TICKET_BOOKING",findLocaleMessageByCode(mapParam.get("LABEL_TICKET_BOOKING"),language));
            mapParam.put("LABEL_CONTRACT_ADMIN",findLocaleMessageByCode(mapParam.get("LABEL_CONTRACT_ADMIN"),language));
            mapParam.put("LABEL_LINE_APPROVER",findLocaleMessageByCode(mapParam.get("LABEL_LINE_APPROVER"),language));
            mapParam.put("LABEL_ROLES",findLocaleMessageByCode(mapParam.get("LABEL_ROLES"),language));
            mapParam.put("LABEL_STATUS",findLocaleMessageByCode(mapParam.get("LABEL_STATUS"),language));
            mapParam.put("LABEL_TRAVEL_TYPE",findLocaleMessageByCode(mapParam.get("LABEL_TRAVEL_TYPE"),language));
            mapParam.put("LABEL_COMPANY_AGENCY_CODE",findLocaleMessageByCode(mapParam.get("LABEL_COMPANY_AGENCY_CODE"),language));
            mapParam.put("LABEL_COMPANY_AGENCY_NAME",findLocaleMessageByCode(mapParam.get("LABEL_COMPANY_AGENCY_NAME"),language));
            mapParam.put("LABEL_LOCATION_EMPLOYE",findLocaleMessageByCode(mapParam.get("LABEL_LOCATION_EMPLOYE"),language));
            mapParam.put("LABEL_COST_DETAIL_AND_ADVANCE",findLocaleMessageByCode(mapParam.get("LABEL_COST_DETAIL_AND_ADVANCE    "),language));
            mapParam.put("LABEL_LIST_COST",findLocaleMessageByCode(mapParam.get("LABEL_LIST_COST"),language));
            mapParam.put("LABEL_LOCATION_EMPLOYEE",findLocaleMessageByCode(mapParam.get("LABEL_LOCATION_EMPLOYEE"),language));
            mapParam.put("LABEL_DOC_REF",findLocaleMessageByCode(mapParam.get("LABEL_DOC_REF"),language));
            mapParam.put("LABEL_ADVANCE_REMARK",findLocaleMessageByCode(mapParam.get("LABEL_ADVANCE_REMARK"),language));
            mapParam.put("LABEL_ADVANCE_PURPOSE",findLocaleMessageByCode(mapParam.get("LABEL_ADVANCE_PURPOSE"),language));

            mapParam.put("LABEL_DOCUMENT_REFERENCE",findLocaleMessageByCode(mapParam.get("LABEL_DOCUMENT_REFERENCE"),language));
            mapParam.put("LABEL_APPROVE_DOCUMENT_REFERENCE",findLocaleMessageByCode(mapParam.get("LABEL_APPROVE_DOCUMENT_REFERENCE"),language));
            mapParam.put("LABEL_DOCUMENT_ADVANCE",findLocaleMessageByCode(mapParam.get("LABEL_DOCUMENT_ADVANCE"),language));
            mapParam.put("LABEL_EXPENSE_DETAIL",findLocaleMessageByCode(mapParam.get("LABEL_EXPENSE_DETAIL"),language));
            mapParam.put("LABEL_LIST_EXPENSE",findLocaleMessageByCode(mapParam.get("LABEL_LIST_EXPENSE"),language));
            mapParam.put("LABEL_MONEY_AMOUNT_BATH",findLocaleMessageByCode(mapParam.get("LABEL_MONEY_AMOUNT_BATH"),language));






            if(docNumber != null){
                LOGGER.info("==-- Found DocNumber : [{}] --==",docNumber);


                /*Found employee data information*/
                ResponseEntity<String> ownerDoc = employeeProfileService.findEmployeeProfileByKeySearch(document.getRequester());
                Map employeeMap = gson.fromJson(ownerDoc.getBody(), Map.class);
                List employeeList = (List) employeeMap.get("restBodyList");
                employeeMap = (Map) employeeList.get(0);


                List<DocumentExpenseItem> documentExpenseItemList =  new ArrayList<>(document.getDocumentExpense().getDocumentExpenseItems());


                List<DocumentReference> listDocRef = new ArrayList<>();

                if(document.getDocumentReference().size() > 0){

                    String res = new JSONSerializer()
                            .include("documentReferences.*")
                            .exclude("document")
                            .deepSerialize(document.getDocumentReference());
                    listDocRef = gson.fromJson(res, new TypeToken<List<DocumentReference>>(){}.getType());

                }



                Request request = document.getRequest();
                List<RequestApprover> approverList = null;
                List<RequestApprover> approverListTemp =new ArrayList<>();
                if(request != null){
                    approverList = new ArrayList<>(request.getRequestApprovers());

                    int sizeApproveerList = approverList.size();
                    int count=1;

                    while(count-1 != sizeApproveerList){
                        for(int i =0; i<sizeApproveerList; i++){

                            if(Integer.valueOf(approverList.get(i).getSequence()) == count){
                                approverListTemp.add(approverList.get(i));
                                count++;
                                break;

                            }

                        }




                    }

                }

                map.put("documentApproveRefValue","");
                map.put("documentAdvanceRefValue","");
                /*Fetching DocRef*/

                for(DocumentReference data : listDocRef){


                    if(data.getDocumentTypeCode().equals("APP")){
                        map.put("documentApproveRefValue",data.getDocReferenceNumber());
                    }
                    if(data.getDocumentTypeCode().equals("ADV")){
                        map.put("documentAdvanceRefValue",data.getDocReferenceNumber());
                    }


                }



                /*Fetching Expense Detail*/

                List<Map<String,String>> expenseDetailList = new ArrayList<>();
                Map<String,String> expenseMap = null;
                if(documentExpenseItemList.size() > 0){
                    for(DocumentExpenseItem data : documentExpenseItemList){
                        expenseMap = new HashMap<>();
                        expenseMap.put("expenseType",language.equals("en") ? data.getExpTypeByCompany().getExpenseType().getEngDescription() : data.getExpTypeByCompany().getExpenseType().getDescription());
                        expenseMap.put("amount",String.valueOf(data.getAmount()));
                        expenseDetailList.add(expenseMap);
                    }
                }


                /*Fetching LineApprove*/
                List<Map<String,String>> lineApproverList = new ArrayList<>();
                Map approverMap = null;
                if(approverListTemp.size() > 0){
                    LOGGER.info(" Approve List Size = [{}]",approverListTemp.size());
                    Map<String,String> mapLineApprover = new HashMap<>();

                    /*Set First Approver is Requester*/
                    mapLineApprover.put("empName",language.equals("en") ?
                            (String) employeeMap.get("NameEN") :
                            employeeMap.get("FOA")+" "+employeeMap.get("FNameTH")+" "+employeeMap.get("LNameTH")
                    );
                    mapLineApprover.put("position", (String) (language.equals("en") ?
                            employeeMap.get("PositionEN") :
                            employeeMap.get("PositionTH")));
                    mapLineApprover.put("role",language.equals("en") ? "Requester" : "ผู้ขออนุมัติ");
                    mapLineApprover.put("status","");
                    lineApproverList.add(mapLineApprover);

                    Parameter parameter = null;
                    parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_IMG_LOGO_EWF);
                    String logoImg = "";
                    for(ParameterDetail detail:parameter.getDetails()){
                        logoImg = detail.getVariable2();
                    }

                    Collections.sort(approverListTemp, new Comparator<RequestApprover>() {
                        @Override
                        public int compare(RequestApprover lhs, RequestApprover rhs) {
                            return lhs.getSequence().compareTo(rhs.getSequence());
                        }
                    });

                    for(RequestApprover data : approverListTemp){
                        mapLineApprover = new HashMap<>();

                        String userNameApprover = data.getUserNameApprover();



                        if(userNameApprover != null){
                            ResponseEntity<String> approverJson = employeeProfileService.findEmployeeProfileByKeySearch(userNameApprover);
                            LOGGER.info("userNameApprover Line Approve ==> {}",userNameApprover);
//                            LOGGER.info("approverJson Line Approve ==> {}",approverJson);
                            approverMap = gson.fromJson(approverJson.getBody(), Map.class);
                            List approverListJson = (List) approverMap.get("restBodyList");
                            approverMap = (Map) approverListJson.get(0);


                            mapLineApprover.put("empName",language.equals("en") ?
                                    (String) approverMap.get("NameEN") :
                                    approverMap.get("FOA")+" "+approverMap.get("FNameTH")+" "+approverMap.get("LNameTH")
                            );
                            mapLineApprover.put("position", (String) (language.equals("en") ?
                                    approverMap.get("PositionEN") :
                                    approverMap.get("PositionTH")));
                            mapLineApprover.put("role",data.getActionStateName());
                            mapLineApprover.put("status",data.getActionTime() != null ? (language.equals("en") || language.equals("en_US") ? "Approved" : "อนุมัติแล้ว") : (language.equals("en") || language.equals("en_US")? "Waiting" : "รอการอนุมัติ"));
                            mapLineApprover.put("statusImage",data.getActionTime() != null ? logoImg.concat("icon_approve.png") : logoImg.concat("icon_waiting.png"));


                        }else{


                            mapLineApprover.put("empName", "");
                            mapLineApprover.put("position","");
                            mapLineApprover.put("role",data.getActionStateName());
                            mapLineApprover.put("status",data.getActionTime() != null ? (language.equals("en") || language.equals("en_US") ? "Approved" : "อนุมัติแล้ว") : (language.equals("en") || language.equals("en_US")? "Waiting" : "รอการอนุมัติ"));
                            mapLineApprover.put("statusImage",data.getActionTime() != null ? logoImg.concat("icon_approve.png") : logoImg.concat("icon_waiting.png"));


                        }




                        lineApproverList.add(mapLineApprover);
                    }
                }






                Map creatorMap = null;
                if(document !=  null){

                    ResponseEntity<String> ownerDocCreator = employeeProfileService.findEmployeeProfileByKeySearch(document.getCreatedBy());
                    creatorMap = gson.fromJson(ownerDocCreator.getBody(), Map.class);
                    List creatorList = (List) creatorMap.get("restBodyList");
                    creatorMap = (Map) creatorList.get(0);

                }



                map.put("reportName",mapParam.get("REPORT_EXPENSE"));

                map.put("docDateValue", String.valueOf(DateUtil.getDateWithRemoveTime(document.getCreatedDate())).length() >=10
                        ? String.valueOf(DateUtil.getDateWithRemoveTime(document.getCreatedDate())).substring(0,10)
                        : "" );

                if(creatorMap != null){
                    map.put("documentCreatorValue",language.equals("en") ?
                            creatorMap.get("NameEN") :
                            creatorMap.get("FOA")+" "+employeeMap.get("FNameTH")+" "+employeeMap.get("LNameTH")
                    );
                }else {
                    map.put("documentCreatorValue","");
                }


                map.put("docNumberValue",docNumber);
                map.put("employeeCodeValue",employeeMap.get("Personal_ID"));
                map.put("compaynyCodeValue",employeeMap.get("Position_PA_ID"));
                map.put("companyNameValue",employeeMap.get("Position_PA_Name"));
                map.put("costCenterValue",employeeMap.get("Position_Cost_Center"));
                map.put("locationValue",employeeMap.get("Personal_PSA_ID") +" : "+employeeMap.get("Personal_PSA_Name"));
                map.put("employeeValue",language.equals("th") ?
                        employeeMap.get("FOA")+" "+employeeMap.get("FNameTH")+" "+employeeMap.get("LNameTH") :
                        employeeMap.get("NameEN")
                );

                map.put("positionValue", language.equals("en") ?
                        employeeMap.get("PositionEN") :
                        employeeMap.get("PositionTH")

                );



                /* Set Message Label */
                map.put("docDate",mapParam.get("LABEL_DOCUMENT_DATE"));
                map.put("documentCreator",mapParam.get("LABEL_DOCUMENT_CREATOR_NAME"));
                map.put("docNumber", mapParam.get("LABEL_DOC_NUMBER"));
                map.put("docType", mapParam.get("LABEL_TRAVEL_TYPE"));
                map.put("employeeName",mapParam.get("LABEL_EMPLOYEE_NAME"));
                map.put("employeeCode", mapParam.get("LABEL_EMPLOYEE_CODE"));
                map.put("position", mapParam.get("LABEL_POSITION"));
                map.put("compaynyCode", mapParam.get("LABEL_COMPANY_AGENCY_CODE"));
                map.put("companyName", mapParam.get("LABEL_COMPANY_AGENCY_NAME"));
                map.put("costCenter", mapParam.get("LABEL_COST_CENTER"));
                map.put("location", mapParam.get("LABEL_LOCATION_EMPLOYEE"));
                map.put("travelDetail", mapParam.get("LABEL_TRAVEL_INFORMATION"));
                map.put("travelMember", mapParam.get("LABEL_TRAVEL_MEMBER_NAME"));
                map.put("travelCost",mapParam.get("LABEL_COST_DETAIL_AND_ADVANCE"));
                map.put("advanceDetail", mapParam.get("LABEL_ADVANCE"));
                map.put("advancePaymentType", mapParam.get("LABEL_PAYMENT_TYPE"));
                map.put("advanceBookbank", mapParam.get("LABEL_BANK_NUMBER"));
                map.put("advanceBorrowDate", mapParam.get("LABEL_BORROW_DATE"));
                map.put("advanceDuedate", mapParam.get("LABEL_DUE_DATE"));
                map.put("advanceContract", mapParam.get("LABEL_CONTRACT_ACCOUNT"));
                map.put("reservationDetail",mapParam.get("LABEL_ADMIN_INFORMATION"));
                map.put("hotel", mapParam.get("LABEL_BOOKING_HOTEL"));
                map.put("car", mapParam.get("LABEL_RESERVE_CAR"));
                map.put("flightTicket", mapParam.get("LABEL_TICKET_BOOKING"));
                map.put("reservationContract", mapParam.get("LABEL_CONTRACT_ADMIN"));
                map.put("lineapprove", mapParam.get("LABEL_LINE_APPROVER"));

                map.put("headerNameLineApprove",mapParam.get("LABEL_EMPLOYEE_NAME"));
                map.put("headerPositionLineApprove",mapParam.get("LABEL_POSITION"));
                map.put("headerRoleLineApprove",mapParam.get("LABEL_ROLES"));
                map.put("headerStatusLineApprove",mapParam.get("LABEL_STATUS"));
                map.put("headerListCost",mapParam.get("LABEL_LIST_COST"));


                map.put("documentRefDetail",mapParam.get("LABEL_DOCUMENT_REFERENCE"));
                map.put("documentApproveRef",mapParam.get("LABEL_APPROVE_DOCUMENT_REFERENCE"));
                map.put("documentAdvanceRef",mapParam.get("LABEL_DOCUMENT_ADVANCE"));
                map.put("expenseDetail",mapParam.get("LABEL_EXPENSE_DETAIL"));
                map.put("headerExpenseType",mapParam.get("LABEL_LIST_EXPENSE"));
                map.put("headerExpenseAmount",mapParam.get("LABEL_MONEY_AMOUNT_BATH"));


                map.put("listExpense",new JRBeanCollectionDataSource(expenseDetailList));
                map.put("listLineapprove",new JRBeanCollectionDataSource(lineApproverList));


                Parameter parameter = null;
                parameter = parameterRepository.findByCode(ConstantVariable.PARAMETER_IMG_LOGO_EWF);
                String logoImg = "";
                String pathImg = "";
                for(ParameterDetail detail:parameter.getDetails()){
                    logoImg = detail.getVariable2()+detail.getVariable1();
                    pathImg = detail.getVariable2();
                }

                map.put("logoImg",logoImg);
                map.put("imgApproved",pathImg + "icon_approve.png");
                map.put("imgWaiting",pathImg + "icon_waiting.png");
                map.put("detailImgApproved",(language.equals("en") || language.equals("en_US") ? "Approved" : "อนุมัติแล้ว"));
                map.put("detailImgWaiting",(language.equals("en") || language.equals("en_US")? "Waiting" : "รอการอนุมัติ"));
            }

            String jasperFileName = "formExpense.jasper";
            jasperPrint = AbstractReportJasperPDF.exportReport(jasperFileName,Arrays.asList(document), map);
        }catch (Exception e){
            LOGGER.error("ERROR : {}",e.getMessage());
            e.printStackTrace();
        }
        return  jasperPrint;
    }


    public static boolean isNotNull(Object object) {
        boolean returnValue = false;
        if (object != null) {
            returnValue = true;
        }
        return returnValue;
    }


    public  String findLocaleMessageByCode(String code,String lang){

        List<LocaleMessage> localeMessage =  localeMessageRepository.findLocaleMessageByCode(code);

        if(localeMessage.size() >0){

            if(lang.equals("en")){
                return localeMessage.get(0).getVariableName1();
            }else if(lang.equals("th")){
                return localeMessage.get(0).getVariableName2();
            }else{
                return "";
            }



        }

        return "";


    }


}

