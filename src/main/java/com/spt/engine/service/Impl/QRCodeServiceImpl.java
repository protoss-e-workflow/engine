package com.spt.engine.service.Impl;

import com.google.zxing.BarcodeFormat;

import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.spt.engine.service.QRCodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;

@Service
//@Cacheable(cacheNames = "qr-code-cache",sync = true)
public class QRCodeServiceImpl implements QRCodeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QRCodeServiceImpl.class);

    @Override
    public byte[] generateQRCode(String text, int width, int height) {
        Assert.hasText(text,"Text Must not empty");
        Assert.isTrue(width > 0, "width must greater than zero");
        Assert.isTrue(height > 0, "height must greater than zero");

        LOGGER.info("Will generate image text=[{}],width=[{}],height=[{}]",text,width,height);

        ByteArrayOutputStream tempBAOS;
        ByteArrayOutputStream baos;

        try {
            tempBAOS = new ByteArrayOutputStream();
            baos = new ByteArrayOutputStream();

            BitMatrix matrix = new MultiFormatWriter().encode(text, BarcodeFormat.QR_CODE,width,height);
            MatrixToImageWriter.writeToStream(matrix, MediaType.IMAGE_PNG.getSubtype(),tempBAOS,new MatrixToImageConfig());

            InputStream inputStream = new ByteArrayInputStream(tempBAOS.toByteArray());
            BufferedImage image = ImageIO.read(inputStream);

            Graphics g = image.getGraphics();

            FontMetrics fontMetrics = g.getFontMetrics();
            g.setFont(new Font("Helvetica",Font.BOLD,18));
            g.setColor(Color.BLACK);

            if(text.substring(text.lastIndexOf("/")).length() < 17)
                g.drawString(text.substring(text.lastIndexOf("/") + 1),(256 - fontMetrics.stringWidth(text.substring(text.lastIndexOf("/") + 1)))/3,245);
            else
                g.drawString(text.substring(text.lastIndexOf("/") + 1),(256 - fontMetrics.stringWidth(text.substring(text.lastIndexOf("/") + 1)))/4,245);

            g.dispose();

            ImageIO.write(image,"png",baos);

            tempBAOS = null;

            return baos.toByteArray();
        }catch (WriterException ex){
            LOGGER.info("WriterException {}",ex);
            return null;
        }catch (IOException ex){
            LOGGER.info("WriterException {}",ex);
            return null;
        }catch (Exception ex){
            LOGGER.info("WriterException {}",ex);
            return null;
        }

    }


//    @CacheEvict(cacheNames = "qr-code-cache",allEntries = true)
//    @Override
//    public void purgeCache() {
//        LOGGER.info("Purging cache");
//    }
}

