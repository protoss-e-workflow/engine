package com.spt.engine.service.Impl;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.converter.AdvanceSAPConverter;
import com.spt.engine.converter.ExpenseSAPConverter;
import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentReference;
import com.spt.engine.entity.general.MasterDataDetail;
import com.spt.engine.repository.app.DocumentRepository;
import com.spt.engine.repository.general.MasterDataDetailRepository;
import com.spt.engine.repository.general.MasterDataRepository;
import com.spt.engine.service.AbstractEmailService;
import com.spt.engine.service.SendMailService;

@Service("SendMailService")
public class SendMailServiceImpl extends AbstractEmailService implements SendMailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SendMailServiceImpl.class);

    @Value("${AppServer}")
	public  String AppServer ;

	@PersistenceContext
    private EntityManager entityManager;
	

	@Autowired
	DocumentRepository documentRepository;

	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {
		super.restTemplate = restTemplate;
	}


	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    @Autowired
    MasterDataRepository masterDataRepository;
    @Autowired
	MasterDataDetailRepository masterDataDetailRepository;

	@Override
	public ResponseEntity<String> sendMail(String from, String fromAddress, String addressTo, String subject,
			String contentText, String ccAddress) {
		// TODO Auto-generated method stub
		return sendMail("/sendMail",from,fromAddress,addressTo, subject,contentText,ccAddress,null,null);
	}

	@Override
	public ResponseEntity<String> sendMail(String from, String fromAddress, String addressTo, String subject,
			String contentText, String ccAddress,String[] pathfileLs, String[] fileNameLs) {
		// TODO Auto-generated method stub
		return sendMail("/sendMail",from,fromAddress,addressTo, subject,contentText,ccAddress,pathfileLs,fileNameLs);
	}

	
}

