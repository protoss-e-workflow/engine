package com.spt.engine.service.Impl;

import com.spt.engine.entity.app.*;
import com.spt.engine.repository.app.DocumentExpenseGroupRepository;
import com.spt.engine.repository.app.DocumentExpenseItemRepository;
import com.spt.engine.repository.app.DocumentRepository;
import com.spt.engine.repository.app.ExpenseTypeByCompanyRepository;
import com.spt.engine.service.DocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service("DocumentService")
public class DocumentServiceImpl implements DocumentService {

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private DocumentExpenseItemRepository documentExpenseItemRepository;

    @Autowired
    private DocumentExpenseGroupRepository documentExpenseGroupRepository;

    @Autowired
    private ExpenseTypeByCompanyRepository expenseTypeByCompanyRepository;

    @Override
    public void updateCostCenterCode(Map map) {
        try {
            Document document = documentRepository.findOne(Long.valueOf(map.get("id").toString()));
            document.setCostCenterCode(map.get("costCenterCode").toString());
            document.setUpdateBy(map.get("updateBy").toString());
            document.setUpdateDate(new Timestamp(new Date().getTime()));
            documentRepository.saveAndFlush(document);
        }catch (Exception e){
            e.printStackTrace();
            LOGGER.error("{}",e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public void updateDocumentExpenseItemAndDocumentExpenseGroupByExpenseTypeByCompany(Map map) {
        try {
            ExpenseTypeByCompany expenseTypeByCompany = expenseTypeByCompanyRepository.findOne(Long.valueOf(map.get("dataExpenseCompanyId").toString()));
            DocumentExpenseGroup documentExpenseGroup = documentExpenseGroupRepository.findOne(Long.valueOf(map.get("dataDocumentExpenseGroupId").toString()));
            List<DocumentExpenseItem> documentExpenseItem = documentExpenseItemRepository.findByDocumentExp(documentExpenseGroup.getDocumentExp().getId());
            boolean checkExpenseType = false;
            if(documentExpenseItem.size()>0){
                for (DocumentExpenseItem dei : documentExpenseItem){
                    if(dei.getExpTypeByCompany().getId().equals(documentExpenseGroup.getExpTypeByCompanys().getId())){
                        checkExpenseType = true;
                        dei.setExpTypeByCompany(expenseTypeByCompany);
                        if(dei.getGlCode().startsWith("8")){
                            dei.setGlCode(expenseTypeByCompany.getExpenseType().getHeadOfficeGL());
                        }else{
                            dei.setGlCode(expenseTypeByCompany.getExpenseType().getFactoryGL());
                        }
                        documentExpenseItemRepository.saveAndFlush(dei);
                    }
                }
            }

            if(checkExpenseType){
                LOGGER.info("updateDocumentExpenseItemAndDocumentExpenseGroupByExpenseTypeByCompany [IF]");
                documentExpenseGroupRepository.delete(documentExpenseGroup);
                documentExpenseGroupRepository.flush();
            }else{
                LOGGER.info("updateDocumentExpenseItemAndDocumentExpenseGroupByExpenseTypeByCompany [ELSE]");
                documentExpenseGroup.setExpTypeByCompanys(expenseTypeByCompany);
                documentExpenseGroupRepository.saveAndFlush(documentExpenseGroup);
            }

        }catch (Exception e){
            e.printStackTrace();
            LOGGER.error("{}",e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }
}
