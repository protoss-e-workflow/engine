package com.spt.engine.service.Impl;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.RequestApprover;
import com.spt.engine.entity.app.SapTransaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.converter.AdvanceSAPConverter;
import com.spt.engine.converter.ExpenseSAPConverter;
import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentExpenseItem;
import com.spt.engine.entity.app.DocumentReference;
import com.spt.engine.entity.general.MasterDataDetail;
import com.spt.engine.repository.app.DocumentRepository;
import com.spt.engine.repository.general.MasterDataDetailRepository;
import com.spt.engine.repository.general.MasterDataRepository;
import com.spt.engine.service.AbstractSAPEngineService;
import com.spt.engine.service.ExpenseTranferService;

@Service("ExpenseTranferService")
public class ExpenseTranferServiceImpl extends AbstractSAPEngineService implements ExpenseTranferService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExpenseTranferServiceImpl.class);

    @Value("${AppServer}")
	public  String AppServer ;

	@PersistenceContext
    private EntityManager entityManager;
	

	@Autowired
	DocumentRepository documentRepository;

	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {
		super.restTemplate = restTemplate;
	}


	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    @Autowired
    MasterDataRepository masterDataRepository;
    @Autowired
	MasterDataDetailRepository masterDataDetailRepository;

	@Override
	public String advanceTranfer(Map param) {
		// TODO Auto-generated method stub
		String urlParam = "/advance";
		Long documentId  = Long.valueOf(param.get("id").toString());//(Long) param.get("id");
		Document documentAdvance = entityManager.find(Document.class, documentId);
		param.put("appServer", AppServer);
		List<String> branchParameter = new ArrayList<String>();
		branchParameter.add("M019");
		
		MasterDataDetail masterDataDetail;
		try {
			masterDataDetail = masterDataDetailRepository.findByMasterdataInAndMasterDataDetailCodeOrderByCode(branchParameter, documentAdvance.getCompanyCode()+"-"+documentAdvance.getPsa());
			LOGGER.info("businessPlace={}",masterDataDetail.getVariable3());
			LOGGER.info("glBankReceive={}",masterDataDetail.getVariable8());
			LOGGER.info("CompanyCode={}",documentAdvance.getCompanyCode());
			LOGGER.info("Psa={}",documentAdvance.getPsa());
			param.put("businessPlace", masterDataDetail.getVariable3());
			param.put("glBankReceive", masterDataDetail.getVariable8());
		} catch (Exception e) {
			e.printStackTrace();
			param.put("businessPlace", "0000");
		}
		
		
		String jsonString = AdvanceSAPConverter.toSAPStructure(documentAdvance,param);
		ResponseEntity<String> responseData = postJsonData(urlParam,jsonString);
		Map<String,Map<String,String>> responseStream = gson.fromJson(responseData.getBody(), Map.class);
		
		
		return AdvanceSAPConverter.sapReturnJson(responseStream);
	}


	@Override
	@Transactional
	public Map expenseTranfer(Map param) {
		// TODO Auto-generated method stub
		String urlParam = "/expense";
		Long documentId  = Long.valueOf(param.get("id").toString());//(Long) param.get("id");
		Document documentExpense = entityManager.find(Document.class, documentId);
		Date currentDate = new Date();
		
		
		param.put("appServer", AppServer);
		
		List<String> branchParameter = new ArrayList<String>();
		branchParameter.add("M019");
		
		MasterDataDetail masterDataDetail;
		try {
			masterDataDetail = masterDataDetailRepository.findByMasterdataInAndMasterDataDetailCodeOrderByCode(branchParameter, documentExpense.getCompanyCode()+"-"+documentExpense.getPsa());
			LOGGER.info("MasterDataDetail={}",masterDataDetail.getVariable1());
			LOGGER.info("businessPlace={}",masterDataDetail.getVariable3());
			LOGGER.info("glBankReceive={}",masterDataDetail.getVariable8());
			LOGGER.info("CompanyCode={}",documentExpense.getCompanyCode());
			LOGGER.info("Psa={}",documentExpense.getPsa());
			param.put("businessPlace", masterDataDetail.getVariable3());
			param.put("glBankReceive", masterDataDetail.getVariable8());
		} catch (Exception e) {
			param.put("businessPlace", "0000");
			e.printStackTrace();
		}
		
		if(documentExpense.getDocumentExpense()!=null && documentExpense.getDocumentExpense().getDocumentExpenseItems()!=null) {
			int indexSeq = 0;
			for(DocumentExpenseItem model:documentExpense.getDocumentExpense().getDocumentExpenseItems()){
				model.setDocumentId(documentId);
				indexSeq++;
				model.setSeqKey(""+indexSeq);
			}
		}
		
		
		/* Check Advance Doc */
		String docAdvanceRef = "";
		Double advanceTotalAmount = null;
		Set<DocumentReference> documentRefferences = documentExpense.getDocumentReference();
		boolean isCompleteStamp = false;
		boolean isRemoveRequestApprover = false;
		Integer count = 0;
		if(documentRefferences!=null) for(DocumentReference docRef:documentRefferences){
			LOGGER.debug("count :{} ",count);
			count++;
			if("ADV".equals(docRef.getDocumentTypeCode()) ){
				docAdvanceRef = docRef.getDocReferenceNumber();
				LOGGER.info("================== docAdvanceRef={}",docAdvanceRef);
				List<Document> documentAdvanceLs = documentRepository.findSAPDocumentNumberDocumentAdvanceByRequestNumber(docAdvanceRef);
				param.put("advanceDocNumber", docAdvanceRef);
				param.put("advanceSapDocNumber", documentAdvanceLs.get(0).getDocNumber());
				advanceTotalAmount = documentAdvanceLs.get(0).getTotalAmount();
				int retval = Double.compare(advanceTotalAmount, documentExpense.getTotalAmount());
				if(retval < 0){
					param.put("clearingCase", "A<E");
					param.put("expenseOverAmount", ""+(documentExpense.getTotalAmount() - advanceTotalAmount));
					LOGGER.info("================ CASE  A<E ==================");
				} else if(retval>0){
					param.put("clearingCase", "A>E");
					param.put("clearingDiffAmount", ""+(advanceTotalAmount - documentExpense.getTotalAmount()));
					
					LOGGER.info("================ CASE  A>E ==================");
					isCompleteStamp = true;
					isRemoveRequestApprover = true;

				}else{
					param.put("clearingCase", "A=E");
					LOGGER.info("================ CASE  A=E ==================");
					isCompleteStamp = true;
					isRemoveRequestApprover = true;

				}
				break;
			}
//			else{
//				LOGGER.info("============= CASE APP HAVE ADVANCE");
//				Document document = documentRepository.findByDocNumber(docRef.getDocReferenceNumber());
//				if(document != null && document.getDocumentAdvance() != null){
//					docAdvanceRef = docRef.getDocReferenceNumber();
//					LOGGER.info("================== docAdvanceRef=|{}|",docAdvanceRef);
//					List<Document> documentAdvanceLs = documentRepository.findSAPDocumentNumberDocumentAdvanceByRequestNumber(docAdvanceRef);
//					//LOGGER.info("============= documentAdvanceLs   : {} ",documentAdvanceLs);
//					param.put("advanceDocNumber", docAdvanceRef);
//					param.put("advanceSapDocNumber", documentAdvanceLs.get(0).getDocNumber());
//					advanceTotalAmount = documentAdvanceLs.get(0).getTotalAmount();
//					//LOGGER.info("================= advanceTotalAmount     :  {}",advanceTotalAmount);
//					//LOGGER.info("================= documentAmount     :  {}",documentExpense.getTotalAmount());
//					int retval = Double.compare(advanceTotalAmount, documentExpense.getTotalAmount());
//					if(retval<0){
//						param.put("clearingCase", "A<E");
//						LOGGER.info("================ CASE  A<E ==================");
//						param.put("expenseOverAmount", ""+(documentExpense.getTotalAmount() - advanceTotalAmount));
//					} else if(retval>0){
//						param.put("clearingCase", "A>E");
//						param.put("clearingDiffAmount", ""+(advanceTotalAmount - documentExpense.getTotalAmount()));
//						LOGGER.info("================ CASE  A>E ==================");
//						// remove request approver state is null
//						Set<RequestApprover> newRequestApprover = new HashSet<RequestApprover>();
//						for(RequestApprover model :documentExpense.getRequest().getRequestApprovers()){
//							if(!model.getActionState().equals("PAID") &&
//									!model.getActionState().equals("FNC")){
//								newRequestApprover.add(model);
//							}
//						}
//
//						documentExpense.getRequest().getRequestApprovers().clear();
//						for(RequestApprover model :newRequestApprover){
//							if(model.getActionState().equals("ACC")){
//								model.setActionTime(new Timestamp(currentDate.getTime()));
//								model.setRequestStatusCode("ACC");
//							}
//							documentExpense.getRequest().getRequestApprovers().add(model);
//						}
//						entityManager.merge(documentExpense.getRequest());
//						entityManager.flush();
//
//						isCompleteStamp = true;
//
//						entityManager.merge(documentExpense);
//						entityManager.flush();
//
//
//					}else{
//						param.put("clearingCase", "A=E");
//						LOGGER.info("================ CASE  A=E ==================");
//						// remove request approver state is null
//						Set<RequestApprover> newRequestApprover = new HashSet<RequestApprover>();
//						for(RequestApprover model :documentExpense.getRequest().getRequestApprovers()){
//							if(!model.getActionState().equals("PAID") &&
//									!model.getActionState().equals("FNC")){
//								newRequestApprover.add(model);
//							}
//						}
//
//						documentExpense.getRequest().getRequestApprovers().clear();
//						for(RequestApprover model :newRequestApprover){
//							if(model.getActionState().equals("ACC")){
//								model.setActionTime(new Timestamp(currentDate.getTime()));
//								model.setRequestStatusCode("ACC");
//							}
//							documentExpense.getRequest().getRequestApprovers().add(model);
//						}
//						entityManager.merge(documentExpense.getRequest());
//						entityManager.flush();
//
//						isCompleteStamp = true;
//
//						entityManager.merge(documentExpense);
//						entityManager.flush();
//
//					}
//					break;
//				}
//			}
		}

		LOGGER.info("================== expenseTranfer");
		
		Map resultMap = ExpenseSAPConverter.toSAPStructure(documentExpense,param);
		String jsonString = (String) resultMap.get("jsonData");
		SapTransaction sapTransaction = new SapTransaction("EXPENSE",jsonString,documentExpense.getDocNumber());
		entityManager.persist(sapTransaction);
		entityManager.flush();
		
		ResponseEntity<String> responseData = postJsonData(urlParam,jsonString);

		LOGGER.info("================== responseData.getBody()={}",responseData.getBody());
		Map<String,Map<String,String>> responseStream = gson.fromJson(responseData.getBody(), Map.class);
		String returnStr = ExpenseSAPConverter.sapReturnJson(responseStream,resultMap);
		List<Map<String,String>> dataReturn = gson.fromJson(returnStr, List.class);
		boolean isError = false;
		for(Map<String,String> dataMap:dataReturn){
			if("E".equals(dataMap.get("returnType"))){
				isError = true;
				break;
			}
		}

		LOGGER.info("isRemoveRequestApprover   :   {}",isRemoveRequestApprover);
		
		if(!isError && isCompleteStamp){

			if(isRemoveRequestApprover) {
				// remove request approver state is null
				Set<RequestApprover> newRequestApprover = new HashSet<RequestApprover>();
				for (RequestApprover model : documentExpense.getRequest().getRequestApprovers()) {
					if (!model.getActionState().equals("PAID") &&
							!model.getActionState().equals("FNC")) {
						newRequestApprover.add(model);
					}
				}

				documentExpense.getRequest().getRequestApprovers().clear();
				for (RequestApprover model : newRequestApprover) {
					if (model.getActionState().equals("ACC")) {
						model.setActionTime(new Timestamp(currentDate.getTime()));
						model.setRequestStatusCode("ACC");
					}

					documentExpense.getRequest().getRequestApprovers().add(model);
				}
				entityManager.merge(documentExpense.getRequest());
				entityManager.flush();

				entityManager.merge(documentExpense);
				entityManager.flush();
			}


			// change status document complete
			documentExpense.setDocumentStatus(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE);

			// change status request complete
			if(documentExpense.getRequests() !=null){
				documentExpense.getRequests().setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_COMPLETE);
			}
			
			entityManager.merge(documentExpense);
			entityManager.flush();
		}
		
		resultMap.remove("jsonData");
		Map returnMap = new HashMap();
		returnMap.put("returnStr", returnStr);
		returnMap.put("instanceItemId", resultMap);
		
		
		return returnMap;
	}
	
	
}

