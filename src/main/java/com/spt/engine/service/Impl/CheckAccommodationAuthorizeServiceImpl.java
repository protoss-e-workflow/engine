package com.spt.engine.service.Impl;

import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.*;
import com.spt.engine.repository.app.*;
import com.spt.engine.service.CheckAccommodationAuthorizeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

@Service("CheckAccommodationAuthorizeService")
public class CheckAccommodationAuthorizeServiceImpl implements CheckAccommodationAuthorizeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CheckAccommodationAuthorizeServiceImpl.class);



    @Autowired
    InternalMemberAccomodationDetailRepository internalMemberAccomodationDetailRepository;

    @Autowired
    ExternalMemberAccomodationDetailRepository externalMemberAccomodationDetailRepository;

    @Autowired
    DocumentExpenseItemRepository documentExpenseItemRepository;

    @Autowired
    EntityManager entityManager;


    @Override
    public boolean checkAccommodationAuthorize(String docNumber,Long itemId,boolean isIbisStd,String zoneType, String diffDays) {

        LOGGER.info("Start Service checkAccommodationAuthorize[CheckAccommodationAuthorizeServiceImpl] ==> DocNumber : {}, Itemid : {}, isBis : {}, zoneType : {}  ",docNumber,itemId,isIbisStd,zoneType);

        List<InternalMemberAccomodationDetail> internalMemberAccomodationDetails = null;
        List<ExternalMemberAccomodationDetail> externalMemberAccomodationDetails = null;
        Double sumCostExteral = Double.valueOf(0);
        Double sumCostInternal = Double.valueOf(0);
        Double sumAllCost = Double.valueOf(0);
        Double sumCost_By_lvEmployee = Double.valueOf(0);
        Double maxCost = null;
        Double locationSpecial = null;
        Integer lvEmp =null;

        Double days = Double.valueOf(1);
        DocumentExpenseItem documentExpenseItemDetail = documentExpenseItemRepository.findOne(itemId);


        try
        {
            Double.parseDouble(diffDays);
            days = Double.valueOf(diffDays);
        }
        catch(NumberFormatException nfe)
        {
            days = Double.valueOf(1);
        }

        LOGGER.info(" =========   Days Travel = {} =========",days);

        try{

                /*When Hotel is Ibis and Room Type is Standard*/
            if(isIbisStd == true){

                LOGGER.info("=============== This Place is Ibis Hotel and Room Type Standard ===============");
                return true;

            }else{





                internalMemberAccomodationDetails = internalMemberAccomodationDetailRepository.findByDocNumberAndItemId(docNumber,itemId);

                if(!internalMemberAccomodationDetails.isEmpty() ){

                     /*Find Employee Lv more than and Get Sum Cost InternalMember */
                    for(InternalMemberAccomodationDetail data : internalMemberAccomodationDetails){
                        lvEmp = Integer.valueOf(data.getLvEmp().substring(1));

                         /*When Travel Member Have employeeLv more than 10*/
                        if(lvEmp >= 10){

                            LOGGER.info("============== This Travel have Employee Lv more than 10 ==============[Service : CheckAccommodationAuthorizeServiceImpl]");
                            return true;

                        }else{

                            sumCostInternal = sumCostInternal+ data.getCost();
                        }


                    }

                    externalMemberAccomodationDetails = externalMemberAccomodationDetailRepository.findByDocNumberAndItemId(docNumber,itemId);

                    if(!externalMemberAccomodationDetails.isEmpty()){
                        for(ExternalMemberAccomodationDetail data : externalMemberAccomodationDetails){

                            sumCostExteral = sumCostExteral+data.getCost();
                        }

                    }


                    for(InternalMemberAccomodationDetail data : internalMemberAccomodationDetails){

                        lvEmp = Integer.valueOf(data.getLvEmp().substring(1));
                        maxCost = Double.valueOf(0);
                        locationSpecial = Double.valueOf(0);

                        LOGGER.info("---------------------------------");
                        LOGGER.info("---------- ZONE TYPE  :  {} -------------- ",zoneType);

                        if(zoneType != null && !"".equals(zoneType)){

                            if(zoneType.equals(ConstantVariable.LOCATION_ZONE_TYPE_A)){
                                LOGGER.info("---------- IF ------------ ");

                                locationSpecial= ConstantVariable.RATE_ZONE_TYPE_A;

                            }else if(zoneType.equals(ConstantVariable.LOCATION_ZONE_TYPE_B)){
                                LOGGER.info("---------- ELSE IF ------------- ");

                                locationSpecial= ConstantVariable.RATE_ZONE_TYPE_B;

                            }else{
                                LOGGER.info("---------- ELSE ------------ ");
                                locationSpecial = Double.valueOf(0);
                            }
                        }


                        if(lvEmp >=1 && lvEmp <= 5){

                            maxCost = ConstantVariable.MAX_COST_C1_TO_C5;
                            maxCost = (maxCost+locationSpecial)*days;
                            sumCost_By_lvEmployee = sumCost_By_lvEmployee +maxCost;

                        }else if(lvEmp >=6 && lvEmp <= 7){

                            maxCost = ConstantVariable.MAX_COST_C6_TO_C7;
                            maxCost = (maxCost+locationSpecial)*days;
                            sumCost_By_lvEmployee = sumCost_By_lvEmployee +maxCost;


                        }else if(lvEmp >=8 && lvEmp <= 9){

                            maxCost = ConstantVariable.MAX_COST_C8_TO_C9;
                            maxCost = (maxCost+locationSpecial)*days;
                            sumCost_By_lvEmployee = sumCost_By_lvEmployee +maxCost;

                        }else{

                        }
                        LOGGER.info("========== {} have Cost =  {} Bath. ==========",data.getEmpUser(),maxCost);

                    }

                    sumAllCost = sumCostExteral+sumCostInternal;

                    LOGGER.info("========== SUM COST by Lv Employee======== {}",sumCost_By_lvEmployee);
                    LOGGER.info("========== SUM COST Internal Member======== {}",sumCostInternal);
                    LOGGER.info("========== SUM COST External Member======== {}",sumCostExteral);
                    LOGGER.info("========== SUM ALL COST======== {}",sumAllCost);

                    if(sumAllCost <=  sumCost_By_lvEmployee){
                        LOGGER.info("================ SUCCESS !!! ================[Service : CheckAccommodationAuthorizeServiceImpl]");
                        DocumentExpenseItem documentExpenseItem = documentExpenseItemRepository.findOne(itemId);
                        documentExpenseItem.setAccommodaionStatus(true);
                        documentExpenseItemRepository.saveAndFlush(documentExpenseItem);
                        return true;
                    }else{
                        LOGGER.info("================ Over Cost !!! ================[Service : CheckAccommodationAuthorizeServiceImpl]");
                        DocumentExpenseItem documentExpenseItem = documentExpenseItemRepository.findOne(itemId);
                        documentExpenseItem.setAccommodaionStatus(false);
                        documentExpenseItemRepository.saveAndFlush(documentExpenseItem);
                        return false;
                    }

                }else{

                    LOGGER.info("========== NOT FOUND!!! Data ========== [Service : CheckAccommodationAuthorizeServiceImpl]");
                    DocumentExpenseItem documentExpenseItem = documentExpenseItemRepository.findOne(itemId);
                    documentExpenseItem.setAccommodaionStatus(false);
                    documentExpenseItemRepository.saveAndFlush(documentExpenseItem);
                    return false;
                }
            }



        }catch (Exception e){

            e.printStackTrace();
            LOGGER.error("========== ERROR !!! to Check Authorize ========== [Service : CheckAccommodationAuthorizeServiceImpl]");
            return false;

        }





    }
}

