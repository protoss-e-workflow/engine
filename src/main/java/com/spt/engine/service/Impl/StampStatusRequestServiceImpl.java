package com.spt.engine.service.Impl;

import com.spt.engine.ConstantVariable;
import com.spt.engine.service.StampStatusRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;

@Service("StampStatusRequestService")
public class StampStatusRequestServiceImpl implements StampStatusRequestService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StampStatusRequestServiceImpl.class);


    @Override
    public List<Map<String, Object>> readDataRequest() {
        LOGGER.info("========= START Fetching Data Request From Bank ============");

        String csvFile = ConstantVariable.PATCH_FILE_TEST_REQUEST_FILE_TO_STAMP_STATUS.getAbsolutePath();
        String cvsSplitBy = ConstantVariable.PARAMETER_TO_SPLIT_CSV_FILE;
        String line = "";
        BufferedReader br = null;
        List<Map<String,Object>> dataResult = new ArrayList<>();
        Map dataInternal_Order;


        try {

            br = new BufferedReader(new FileReader(csvFile));



            while ((line = br.readLine()) != null) {


                String[] requestData = line.split(cvsSplitBy);

                dataInternal_Order = new HashMap();
                dataInternal_Order.put("requestNumber",requestData[0]);
                dataInternal_Order.put("amount",requestData[1]);

                dataResult.add(dataInternal_Order);


            }


            LOGGER.info("========= Finish Fetching Data Request From Bank ============");
            return dataResult;


        } catch (FileNotFoundException e) {
            LOGGER.error("========= ERROR File Not Found !!! =========");
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            LOGGER.error("========= ERROR IO Exception !!! =========");
            e.printStackTrace();
            return null;
        } finally {
            if (br != null) {
                try {
                    br.close();
                    LOGGER.info("========= CLOSE FILE SUCCESS !!! =========");
                } catch (IOException e) {
                    LOGGER.error("========= ERROR CANNOT CLOSE FILE !!! =========");
                    e.printStackTrace();
                }
            }
        }
    }
}

