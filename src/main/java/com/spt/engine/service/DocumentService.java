package com.spt.engine.service;

import java.util.Map;

public interface DocumentService {
    void updateCostCenterCode(Map map);
    void updateDocumentExpenseItemAndDocumentExpenseGroupByExpenseTypeByCompany(Map map);
}
