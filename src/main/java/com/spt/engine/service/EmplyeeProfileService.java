package com.spt.engine.service;

import org.springframework.http.ResponseEntity;

public interface EmplyeeProfileService {
	
	public ResponseEntity<String>   findEmployeeProfileByEmployeeCode(String employeeCode);
	public ResponseEntity<String>   findEmployeeProfileByKeySearch(String keySearch);
	public ResponseEntity<String>   findAccountByPaPsaAndKeySearch(String papsa,String keySearch);
	public ResponseEntity<String>   findFinanceByPaPsaAndKeySearch(String papsa,String keySearch);
	public ResponseEntity<String>   findAdminByPaPsaAndKeySearch(String papsa,String requester);
}
