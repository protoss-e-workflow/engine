package com.spt.engine.service;

import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

@Configuration
public abstract class  AbstractSAPEngineService {
	protected static Logger LOGGER = LoggerFactory.getLogger(AbstractSAPEngineService.class);

	@Value("${SAPServer}")
	protected String SAPServer ;
    protected static Properties connectProperties = null;

    protected String webServicesString = "";
    protected String resultString = "";

    protected RestTemplate restTemplate;
    
    public abstract void setRestTemplate(RestTemplate restTemplate);

	protected JsonParser parser = new JsonParser();

    JsonSerializer<Date> ser = new JsonSerializer<Date>() {
        public JsonElement serialize(Date src, Type typeOfSrc,
                                     JsonSerializationContext context) {
            return src == null ? null : new JsonPrimitive(src.getTime());
        }
    };
    
    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    
    public AbstractSAPEngineService(){
    }
    
    public String getWebServicesString() {
        return webServicesString;
    }
    
    public AbstractSAPEngineService setWebServicesString(String webServicesString) {
        this.webServicesString = webServicesString;
        return this;
    }
    
    public ResponseEntity<String> postJsonData(String urlParam, String jsonString) {
        String url = this.SAPServer + urlParam;
        LOGGER.info(" request     URL:{}", url);
        LOGGER.info(" jsonString json:{}", jsonString);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");
        headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
    	HttpEntity<String> entity = new HttpEntity<String>(jsonString, headers);
    	return restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
    }
    
    
    
    
    
    
    
}
