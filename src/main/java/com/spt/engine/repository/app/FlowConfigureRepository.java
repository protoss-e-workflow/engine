package com.spt.engine.repository.app;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.app.Company;
import com.spt.engine.entity.app.CostCenter;
import com.spt.engine.entity.app.FlowConfigure;
import com.spt.engine.entity.general.RunningType;

public interface FlowConfigureRepository  extends JpaSpecificationExecutor<FlowConfigure>, JpaRepository<FlowConfigure, Long>, PagingAndSortingRepository<FlowConfigure, Long> {

	FlowConfigure findByCode(@Param("code") String code);
	
	
	FlowConfigure findByTypeCodeAndStateVerifyAndStateAproveAndStateAdminAndStateHrAndStateAccountAndStateFinance(
			@Param("typeCode") String typeCode,
			@Param("stateVerify") String stateVerify,
			@Param("stateAprove") String stateAprove,
			@Param("stateAdmin") String stateAdmin,
			@Param("stateHr") String stateHr,
			@Param("stateAccount") String stateAccount,
			@Param("stateFinance") String stateFinance
			      );
	
}
