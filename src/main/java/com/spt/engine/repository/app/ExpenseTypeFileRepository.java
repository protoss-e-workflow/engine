package com.spt.engine.repository.app;

import com.spt.engine.entity.app.ExpenseTypeFile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ExpenseTypeFileRepository extends JpaSpecificationExecutor<ExpenseTypeFile>, JpaRepository<ExpenseTypeFile, Long>, PagingAndSortingRepository<ExpenseTypeFile, Long> {

	ExpenseTypeFile findByAttachmentTypeCode(@Param("code") String code);


	@Query("select DISTINCT u from ExpenseTypeFile u  where  u.expenseType.id = :expenseType")
	Page<ExpenseTypeFile> findExpenseTypeFileByExpenseTypeId(
            @Param("expenseType") Long expenseType,
            Pageable pageable);



}
