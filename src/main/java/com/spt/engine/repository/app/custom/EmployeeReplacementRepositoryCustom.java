package com.spt.engine.repository.app.custom;

import com.spt.engine.entity.app.Delegate;
import com.spt.engine.entity.app.EmployeeReplacement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

public interface EmployeeReplacementRepositoryCustom {

    Page<EmployeeReplacement> findByEmpCodeReplaceIgnoreCaseContaining(@Param("empCodeReplace") String empCodeReplace, Pageable pageable);

}