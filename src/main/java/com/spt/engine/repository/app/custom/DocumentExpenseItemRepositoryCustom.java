package com.spt.engine.repository.app.custom;

import java.util.List;
import java.util.Map;

public interface DocumentExpenseItemRepositoryCustom {

    List<Map<String,Object>> checkBudgetInWorkflow(String costCenter,String gl,String docNumber,String expItemId);
    List<Map<String,Object>> checkBudgetInEwf(String gl,String costCenter);
}
