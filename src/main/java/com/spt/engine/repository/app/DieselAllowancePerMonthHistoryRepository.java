package com.spt.engine.repository.app;

import com.spt.engine.entity.app.DieselAllowancePerMonthHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;


public interface DieselAllowancePerMonthHistoryRepository extends JpaSpecificationExecutor<DieselAllowancePerMonthHistory>, JpaRepository<DieselAllowancePerMonthHistory, Long>, PagingAndSortingRepository<DieselAllowancePerMonthHistory, Long>  {


	@Query("select DISTINCT p from DieselAllowancePerMonthHistory p where  lower(p.empCode) like CONCAT('%',lower(:empCode),'%') and "
			+ "  lower(p.month) like CONCAT('%',lower(:month),'%') and  lower(p.year) like CONCAT('%',lower(:year),'%') ")
	Page<DieselAllowancePerMonthHistory> findByEmpCodeIgnoreCaseContainingAndMonthIgnoreCaseContainingAndYearIgnoreCaseContaining(
            @Param("empCode") String empCode,
            @Param("month") String month,
            @Param("year") String year,
            Pageable pageable);


}
