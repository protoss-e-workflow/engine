package com.spt.engine.repository.app;

import com.spt.engine.entity.app.MonthlyPhoneBill;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;


public interface MonthlyPhoneBillRepository extends JpaSpecificationExecutor<MonthlyPhoneBill>, JpaRepository<MonthlyPhoneBill, Long>, PagingAndSortingRepository<MonthlyPhoneBill, Long>  {


	@Query("select DISTINCT m from MonthlyPhoneBill m where  lower(m.empCode) like CONCAT('%',lower(:empCode),'%')" )
	Page<MonthlyPhoneBill> findByEmpCodeIgnoreCaseContaining(
            @Param("empCode") String empCode,
            Pageable pageable);

	MonthlyPhoneBill findByEmpCode(@Param("empCode") String empCode);

	MonthlyPhoneBill findByEmpUserName(@Param("empUserName") String empUserName);
}
