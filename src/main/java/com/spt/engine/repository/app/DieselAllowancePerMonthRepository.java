package com.spt.engine.repository.app;

import com.spt.engine.entity.app.DieselAllowancePerMonth;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;


public interface DieselAllowancePerMonthRepository extends JpaSpecificationExecutor<DieselAllowancePerMonth>, JpaRepository<DieselAllowancePerMonth, Long>, PagingAndSortingRepository<DieselAllowancePerMonth, Long>  {


	@Query("select DISTINCT d from DieselAllowancePerMonth d where  lower(d.empCode) like CONCAT('%',lower(:empCode),'%') ")
	Page<DieselAllowancePerMonth> findByEmpCodeIgnoreCaseContaining(
            @Param("empCode") String empCode,
            Pageable pageable);

//	@Query("select DISTINCT d from DieselAllowancePerMonth d where  lower(d.empCode) like CONCAT('%',lower(:empCode),'%')" )
	DieselAllowancePerMonth findByEmpCode(@Param("empCode") String empCode);

	DieselAllowancePerMonth findByEmpUserName(@Param("empUserName") String empUserName);

}
