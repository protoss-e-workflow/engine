package com.spt.engine.repository.app;

import com.spt.engine.entity.app.InternalMemberAccomodationDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InternalMemberAccomodationDetailRepository extends JpaSpecificationExecutor<InternalMemberAccomodationDetail>, JpaRepository<InternalMemberAccomodationDetail, Long>, PagingAndSortingRepository<InternalMemberAccomodationDetail, Long> {


			List<InternalMemberAccomodationDetail> findByDocNumberAndItemId(@Param("docNumber")String docNumber,
																			 @Param("itemId")Long itemId);

	List<InternalMemberAccomodationDetail> findByDocNumber(@Param("docNumber")String docNumber);

}
