package com.spt.engine.repository.app;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.app.ExternalMember;


public interface ExternalMemberRepository extends JpaSpecificationExecutor<ExternalMember>, JpaRepository<ExternalMember, Long>, PagingAndSortingRepository<ExternalMember, Long> {

	@Query("select DISTINCT u from ExternalMember u left join u.documentApproveItem r where r.id in :documentApproveItem order by u.id ")
	List<ExternalMember> findByDocumentApproveItemId( @Param("documentApproveItem") List<Long> documentApproveItem);
}
