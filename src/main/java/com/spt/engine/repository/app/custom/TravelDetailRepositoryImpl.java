package com.spt.engine.repository.app.custom;

import com.spt.engine.controller.TravelDetailController;
import com.spt.engine.entity.app.TravelDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class TravelDetailRepositoryImpl implements TravelDetailRepositoryCustom{

    static final Logger LOGGER = LoggerFactory.getLogger(TravelDetailRepositoryImpl.class);

    @Autowired
    EntityManager entityManager;

    @Override
    public List<String> validateTimeOverlapByDocIsActiveOrCompleteWithStartDate(String startDate, String requester) {

        List<String> travelDetailLs = new ArrayList<>();

        LOGGER.info("><><>< validateTimeOverlapByDocIsActiveOrCompleteWithStartDate ><><><>");
        try{
            travelDetailLs = entityManager.createNativeQuery(
                    "SELECT d.doc_number " +
                            "FROM travel_detail t, document_approve_item dai,document_approve da, document d " +
                            "WHERE t.document_approve_item = dai.id " +
                            "AND dai.document_approve = da.id " +
                            "AND da.document_id = d.id " +
                            "AND d.requester = :requester "+
                            "AND d.document_status IN ('ONP','CMP') " +
                            "AND CONVERT(datetime,:startDate, 110) " +
                            "BETWEEN CONVERT (datetime,CONVERT (nvarchar(10),CAST (t.start_date as date),110)+' '+t.start_time+':00',110) " +
                            "AND CONVERT (datetime,CONVERT(nvarchar(10),CAST(t.end_date as date),110)+' '+t.end_time+':00',110) " )
                    .setParameter("requester", requester)
                    .setParameter("startDate", startDate).getResultList();
        }catch (Exception e){
            e.printStackTrace();
        }
        return travelDetailLs;
    }

    @Override
    public List<String> validateTimeOverlapByDocIsActiveOrCompleteWithEndDate(String endDate, String requester) {
        List<String> travelDetailLs = new ArrayList<>();

        LOGGER.info("><><>< validateTimeOverlapByDocIsActiveOrCompleteWithEndDate ><><><>");
        try{
            travelDetailLs = entityManager.createNativeQuery(
                    "SELECT d.doc_number " +
                            "FROM travel_detail t, document_approve_item dai,document_approve da, document d " +
                            "WHERE t.document_approve_item = dai.id " +
                            "AND dai.document_approve = da.id " +
                            "AND da.document_id = d.id " +
                            "AND d.requester = :requester "+
                            "AND d.document_status IN ('ONP','CMP') " +
                            "AND CONVERT(datetime,:endDate, 110) BETWEEN " +
                            "CONVERT (datetime,CONVERT (nvarchar(10),CAST (t.start_date as date),110)+' '+t.start_time+':00',110) " +
                            "AND CONVERT( datetime,CONVERT(nvarchar(10),CAST(t.end_date as date),110)+' '+t.end_time+':00',110) " )
                    .setParameter("requester", requester)
                    .setParameter("endDate", endDate).getResultList();
        }catch (Exception e){
            e.printStackTrace();
        }

        return travelDetailLs;
    }

    @Override
    public List<String> validateTimeOverlapByDocIsActiveOrCompleteStartDateWithDocNumber(String startDate, String requester,String docNumber) {
        List<String> travelDetailLs = new ArrayList<>();

        LOGGER.info("><><>< validateTimeOverlapByDocIsActiveOrCompleteWithEndDate ><><><>");
        try{
            travelDetailLs = entityManager.createNativeQuery(
                    "SELECT d.doc_number " +
                            "FROM travel_detail t, document_approve_item dai,document_approve da, document d " +
                            "WHERE t.document_approve_item = dai.id " +
                            "AND dai.document_approve = da.id " +
                            "AND da.document_id = d.id " +
                            "AND d.doc_number != :docNumber "+
                            "AND d.requester = :requester "+
                            "AND d.document_status IN ('DRF') " +
                            "AND CONVERT(datetime,:startDate, 110) BETWEEN " +
                            "CONVERT (datetime,CONVERT (nvarchar(10),CAST (t.start_date as date),110)+' '+t.start_time+':00',110) " +
                            "AND CONVERT( datetime,CONVERT(nvarchar(10),CAST(t.end_date as date),110)+' '+t.end_time+':00',110) " )
                    .setParameter("requester", requester)
                    .setParameter("docNumber", docNumber)
                    .setParameter("startDate", startDate).getResultList();
        }catch (Exception e){
            e.printStackTrace();
        }

        return travelDetailLs;
    }

    @Override
    public List<String> validateTimeOverlapByDocIsActiveOrCompleteEndDateWithDocNumber(String endDate, String requester,String docNumber) {
        List<String> travelDetailLs = new ArrayList<>();

        LOGGER.info("><><>< validateTimeOverlapByDocIsActiveOrCompleteWithEndDate ><><><>");
        try{
            travelDetailLs = entityManager.createNativeQuery(
                    "SELECT d.doc_number " +
                            "FROM travel_detail t, document_approve_item dai,document_approve da, document d " +
                            "WHERE t.document_approve_item = dai.id " +
                            "AND dai.document_approve = da.id " +
                            "AND da.document_id = d.id " +
                            "AND d.doc_number != :docNumber "+
                            "AND d.requester = :requester "+
                            "AND d.document_status IN ('DRF') " +
                            "AND CONVERT(datetime,:endDate, 110) BETWEEN " +
                            "CONVERT (datetime,CONVERT (nvarchar(10),CAST (t.start_date as date),110)+' '+t.start_time+':00',110) " +
                            "AND CONVERT( datetime,CONVERT(nvarchar(10),CAST(t.end_date as date),110)+' '+t.end_time+':00',110) " )
                    .setParameter("requester", requester)
                    .setParameter("docNumber", docNumber)
                    .setParameter("endDate", endDate).getResultList();
        }catch (Exception e){
            e.printStackTrace();
        }

        return travelDetailLs;
    }
}
