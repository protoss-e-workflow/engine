package com.spt.engine.repository.app.custom;

import com.spt.engine.entity.app.TravelDetail;

import java.util.List;

public interface TravelDetailRepositoryCustom {
    List<String> validateTimeOverlapByDocIsActiveOrCompleteWithStartDate(String startDate,String requester);
    List<String> validateTimeOverlapByDocIsActiveOrCompleteWithEndDate(String endDate,String requester);
    List<String> validateTimeOverlapByDocIsActiveOrCompleteStartDateWithDocNumber(String startDate,String requester,String docNumber);
    List<String> validateTimeOverlapByDocIsActiveOrCompleteEndDateWithDocNumber(String endDate,String requester,String docNumber);
}
