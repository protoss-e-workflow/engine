package com.spt.engine.repository.app.custom;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.InternalOrder;
import com.spt.engine.entity.app.RouteDistance;
import com.spt.engine.entity.general.MasterDataDetail;
import com.spt.engine.repository.app.InternalOrderRepository;
import com.spt.engine.repository.app.RouteDistanceProcessCalculateRepository;
import com.spt.engine.repository.general.MasterDataDetailRepository;
import com.spt.engine.repository.general.MasterDataRepository;
import com.spt.engine.service.InternalOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

public class InternalOrderRepositoryImpl implements InternalOrderRepositoryCustom {

    static final Logger LOGGER = LoggerFactory.getLogger(InternalOrderRepositoryImpl.class);


    @Autowired
    InternalOrderService internalOrderService;

   @Autowired
    InternalOrderRepository internalOrderRepository;


    @Transactional
    @Override
    public void saveInternalOrder_From_SAP_Interface() {



        LOGGER.info("========== Start Save InternalOrder ==========");

        try{

            InternalOrder internalOrder=null;

            List<Map<String,Object>> listResult = internalOrderService.readDataInternalOrderFromSapInterface();
//            ObjectMapper mapper = new ObjectMapper();
            for(Map mapData : listResult){


                String date = String.valueOf(mapData.get("date"));
                String time = String.valueOf(mapData.get("time"));

                String dateFormat = date.substring(0,4)+"-"+date.substring(4,6)+"-"+date.substring(6)+" "+time.substring(0,2)+":"+time.substring(2,4)+":"+time.substring(4);
                java.sql.Timestamp ts = java.sql.Timestamp.valueOf( dateFormat ) ;


                internalOrder = new InternalOrder();
                internalOrder.setCompanyCode( String.valueOf(mapData.get("companyCode")) );
                internalOrder.setIoCode( String.valueOf(mapData.get("ioCode")) );
                internalOrder.setBalanceAmount(Double.parseDouble(String.valueOf(mapData.get("balanceAmount"))) );
                internalOrder.setDateTime(ts);

//                internalOrder = mapper.convertValue(mapData,InternalOrder.class);

                internalOrderRepository.saveAndFlush(internalOrder);

            }


            LOGGER.info("========== Finish Save InternalOrder ==========");


//            internalOrderService.moveFileWhenfetchAndSaveDataSuccess();


            LOGGER.info("========== Finish Move Data to Backup ==========");

        }catch(Exception e){

            LOGGER.error("=============== ERROR In InternalOrderRepositoryImpl[saveInternalOrder_From_SAP_Interface] =============== ");
            e.printStackTrace();

        }

    }
}
