package com.spt.engine.repository.app.custom;

import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.RouteDistance;
import com.spt.engine.entity.general.MasterDataDetail;
import com.spt.engine.repository.app.RouteDistanceProcessCalculateRepository;
import com.spt.engine.repository.general.MasterDataDetailRepository;
import com.spt.engine.repository.general.MasterDataRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.*;

public class RouteDistanceProcessCalculateRepositoryImpl implements RouteDistanceProcessCalculateRepositoryCustom {

    static final Logger LOGGER = LoggerFactory.getLogger(RouteDistanceProcessCalculateRepositoryImpl.class);

    @Autowired
    MasterDataRepository masterDataRepository;

    @Autowired
    MasterDataDetailRepository masterDataDetailRepository;

    @Autowired
    RouteDistanceProcessCalculateRepository routeDistanceProcessCalculateRepository;


    @Override
    public Double processCalculateTotalAmountDistanceRoute(Long locationFrom, Long locationTo, String companyCode) {
        Double result = null;

        try {
            LOGGER.info("====== IN RouteDistanceProcessCalculateRepositoryImpl[Method : processCalculateTotalAmountDistanceRoute] ===== Value[ LocationFrom: {}, LocationTo:{}, CompanyCode :{}] ", locationFrom, locationTo, companyCode);


            RouteDistance routeDistance = routeDistanceProcessCalculateRepository.findByLocationFromAndLocationTo(locationFrom, locationTo);


            if (routeDistance != null) {

                Double distance = null;
                distance = routeDistance.getDistance();
                if (distance > 0.0) {
                    LOGGER.debug("======  DEBUG =====> Distance[{}]", distance);


                    Set<MasterDataDetail> setItem = masterDataDetailRepository.findByOrgCodeOrderByVariable1Asc(companyCode);
                    if (setItem != null && setItem.size() > 0) {

                        List<Map<String, Object>> mapValue = convertMasterDataDistanceToMapData(setItem);
                        if (mapValue != null) {
                            result = getAmountFromDistance(mapValue, distance);
                        } else {
                            LOGGER.info("====== Fail to Convert Data =====  ");

                            return -1.0;
                        }


                    } else {
                        LOGGER.info("====== Not Found CompanyCode For Calculate Distance =====  ");
                        return -1.0;
                    }

                    LOGGER.info("====== OUT RouteDistanceProcessCalculateRepositoryImpl[Method : processCalculateTotalAmountDistanceRoute] ===== Value[ Result: {} ] ", result);

                    return result;

                } else {
                    return 0.0;
                }

            } else {
                return 0.0;
            }


        } catch (NumberFormatException e) {
            e.printStackTrace();
            LOGGER.error(" ERROR CASE NumberFormatException : [{}]", e.getMessage());


        } catch (NullPointerException e) {
            e.printStackTrace();
            LOGGER.error(" ERROR CASE NullPointerException : [{}]", e.getMessage());
        }
        return result;
    }


    @Override
    public List<Map<String, Object>> convertMasterDataDistanceToMapData(Set<MasterDataDetail> item) {

        LOGGER.info("====== IN RouteDistanceProcessCalculateRepositoryImpl[Method : convertMasterDataDistanceToMapData]  ===== ");

        List<Map<String, Object>> list = new ArrayList<>();

        try {

            Map<String, Object> map;

            for (MasterDataDetail o : item) {
                map = new HashMap<>();

                if (((!"".equals(o.getVariable1()) && null != o.getVariable1()) &&
                        (!"".equals(o.getVariable2()) && null != o.getVariable2()))) {

                    map.put("rate", o.getVariable1());
                    map.put("value", o.getVariable2());
                    list.add(map);
                    map = null;

                } else {
                    LOGGER.debug(" ========  FAIL TO PUT DATA IN MAP ======== ");

                    return null;

                }


            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            LOGGER.error(" ERROR CASE NumberFormatException : [{}]", e.getMessage());
            throw new NumberFormatException(e.getMessage());


        } catch (NullPointerException e) {
            e.printStackTrace();
            LOGGER.error(" ERROR CASE NullPointerException : [{}]", e.getMessage());
            throw new NullPointerException(e.getMessage());
        }


        LOGGER.info("====== Out RouteDistanceProcessCalculateRepositoryImpl[Method : convertMasterDataDistanceToMapData]  ===== ");
//		LOGGER.info("=========================> DATA LIST ===========+> [{}]",list);

        return list;

    }

    @Override
    public Double getAmountFromDistance(List<Map<String, Object>> list, Double distances) {
        LOGGER.info("====== IN RouteDistanceProcessCalculateRepositoryImpl[Method : getAmountFromDistance]  ===== values : distance [{}] ", distances);


        try {
            Double distance = Double.valueOf(distances);
            Long numberRounded = Math.round(distance);


            if (numberRounded == 0) {
                return 0.0;
            }

            BigDecimal amount = BigDecimal.ZERO;
            BigDecimal count = BigDecimal.ZERO;
            BigDecimal countRoundNum = BigDecimal.ZERO;
            BigDecimal increseAmount = BigDecimal.ZERO;
            BigDecimal loopcheck = new BigDecimal(list.size());
            BigDecimal check = BigDecimal.ZERO;
            BigDecimal checkLoop = BigDecimal.ZERO;


            for (int i = 0, convLoop = loopcheck.intValueExact(); i < convLoop; i++) {


                if (ConstantVariable.OTHER_DISTANCE.equals(String.valueOf((list.get(i).get("rate"))))) {
                    countRoundNum = new BigDecimal(ConstantVariable.OTHER_RATE_DISTANCE_COUNT);
                } else {
                    countRoundNum = new BigDecimal(String.valueOf(list.get(i).get("rate")));

                }


                increseAmount = new BigDecimal(String.valueOf(list.get(i).get("value")));
                count = countRoundNum.subtract(count);
                LOGGER.debug("==========>LOOP COUNT========[{}]", count);


                for (int j = 0, convLoopAmount = count.intValueExact(); j < convLoopAmount; j++) {


                    amount = amount.add(increseAmount);

                    LOGGER.debug("==========>AMOUNT========[{}]", amount);
                    check = check.add(new BigDecimal(1));
                    LOGGER.debug("==========>CHECK VALUE ========[{}] ", check);


                    LOGGER.debug("==========>CHECK ========[{}] : [{}]", check, numberRounded);
                    if (check.equals(new BigDecimal(numberRounded))) {
                        checkLoop = new BigDecimal("-1");
                        break;
                    }
                }
                if (!checkLoop.equals(new BigDecimal("0"))) {
                    break;
                }


            }


            count = null;
            check = null;
            checkLoop = null;
            countRoundNum = null;
            increseAmount = null;


            return Double.valueOf(amount.doubleValue());


        } catch (NumberFormatException e) {
            e.printStackTrace();
            LOGGER.error(" ERROR CASE NumberFormatException : [{}]", e.getMessage());
            throw new NumberFormatException(e.getMessage());


        } catch (NullPointerException e) {
            e.printStackTrace();
            LOGGER.error(" ERROR CASE NullPointerException : [{}]", e.getMessage());
            throw new NullPointerException(e.getMessage());
        }


    }

    @Override
    public Double getAmountFromDistanceSendOnlyDistance(String companyCode, Double distances) {


        Double result = null;

        try {
            LOGGER.info("====== IN RouteDistanceProcessCalculateRepositoryImpl[Method : processCalculateTotalAmountDistanceRoute] ===== Value[ CompanyCode :{}] ", companyCode);




            if (distances != null) {

                Double distance = null;
                distance = distances;
                if (distance > 0.0) {
                    LOGGER.debug("======  DEBUG =====> Distance[{}]", distance);


                    Set<MasterDataDetail> setItem = masterDataDetailRepository.findByOrgCodeOrderByVariable1Asc(companyCode);
                    if (setItem != null && setItem.size() > 0) {

                        List<Map<String, Object>> mapValue = convertMasterDataDistanceToMapData(setItem);
                        if (mapValue != null) {
                            result = getAmountFromDistance(mapValue, distance);
                        } else {
                            LOGGER.info("====== Fail to Convert Data =====  ");

                            return -1.0;
                        }


                    } else {
                        LOGGER.info("====== Not Found CompanyCode For Calculate Distance =====  ");
                        return -1.0;
                    }

                    LOGGER.info("====== OUT RouteDistanceProcessCalculateRepositoryImpl[Method : processCalculateTotalAmountDistanceRoute] ===== Value[ Result: {} ] ", result);

                    return result;

                } else {
                    return 0.0;
                }

            } else {
                return 0.0;
            }


        } catch (NumberFormatException e) {
            e.printStackTrace();
            LOGGER.error(" ERROR CASE NumberFormatException : [{}]", e.getMessage());


        } catch (NullPointerException e) {
            e.printStackTrace();
            LOGGER.error(" ERROR CASE NullPointerException : [{}]", e.getMessage());
        }
        return result;

    }


}
