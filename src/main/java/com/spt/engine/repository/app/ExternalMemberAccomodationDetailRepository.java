package com.spt.engine.repository.app;

import com.spt.engine.entity.app.ExternalMemberAccomodationDetail;
import com.spt.engine.entity.app.InternalMemberAccomodationDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ExternalMemberAccomodationDetailRepository extends JpaSpecificationExecutor<ExternalMemberAccomodationDetail>, JpaRepository<ExternalMemberAccomodationDetail, Long>, PagingAndSortingRepository<ExternalMemberAccomodationDetail, Long> {

	List<ExternalMemberAccomodationDetail> findByDocNumberAndItemId(@Param("docNumber") String docNumber, @Param("itemId") Long itemId);

	List<ExternalMemberAccomodationDetail> findByDocNumber(@Param("docNumber") String docNumber);
}
