package com.spt.engine.repository.app;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.app.Delegate;
import com.spt.engine.repository.app.custom.DelegateRepositoryCustom;

public interface DelegateRepository extends JpaSpecificationExecutor<Delegate>, JpaRepository<Delegate, Long>, PagingAndSortingRepository<Delegate, Long>,DelegateRepositoryCustom {











//    Page<Delegate> findByEmpCodeAssigneeIgnoreCaseContaining(
//            @Param("empCodeAssignee") String empCodeAssignee,
//            Pageable pageable);



    @Query("select DISTINCT u from Delegate u  where  (u.empCodeAssignee = :empCodeAssignee) and u.empCodeAssigner like CONCAT('%',:empCodeAssigner,'%') and u.flowType like CONCAT('%',:flowType,'%')  order by u.empCodeAssigner ")
    Page<Delegate> findByEmpCodeAssigneeAndFlowType(
            @Param("empCodeAssignee") String empCodeAssignee,
            @Param("empCodeAssigner") String empCodeAssigner,
            @Param("flowType") String flowType,
            Pageable pageable);


    @Query("select DISTINCT u from Delegate u  where  u.empCodeAssignee = :empCodeAssignee and u.empCodeAssigner = :empCodeAssigner and u.flowType = :flowType and u.startDate like CONCAT(:startDate,'%') and u.endDate like CONCAT(:endDate,'%') ")
    Delegate findByEmpCodeAssigneeAndEmpCodeAssignerAndFlowTypeAndStartDateAndEndDate(@Param("empCodeAssignee")String empCodeAssignee,
                                                                         @Param("empCodeAssigner")String empCodeAssigner,
                                                                         @Param("flowType")String flowType,
                                                                         @Param("startDate")String startDate,
                                                                         @Param("endDate")String endDate

    );



    List<Delegate> findAllDelegateByEmpCodeAssignee(@Param("empCodeAssignee")String empCodeAssignee);

    List<Delegate> findByEmpCodeAssignee(@Param("empCodeAssignee")String empCodeAssignee);
    
    @Query("select DISTINCT u from Delegate u  where  u.empUserNameAssignee = :empUserNameAssigner and u.flowType = :flowType  and CURRENT_TIMESTAMP between u.startDate and u.endDate ")
    Delegate findByEmpUserNameAssignerAndFlowTypeAndStartDate(@Param("empUserNameAssigner")String empUserNameAssigner,
                                                                         @Param("flowType")String flowType
    );

}
