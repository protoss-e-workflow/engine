package com.spt.engine.repository.app;

/**
 * 	Create by RujiphatR. 2017.09.13
 *
 */

import com.spt.engine.entity.app.ConditionalIO;
import com.spt.engine.repository.app.projection.InlineConditionalIO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@RepositoryRestResource(excerptProjection = InlineConditionalIO.class)

public interface ConditionalIORepository extends JpaSpecificationExecutor<ConditionalIO>, JpaRepository<ConditionalIO, Long>, PagingAndSortingRepository<ConditionalIO, Long> {


    @Query("select DISTINCT c from ConditionalIO c  where  c.company.id = :company ")
    Page<ConditionalIO> findByCompany(@Param("company") Long company, Pageable pageable);


    @Query("select DISTINCT c from ConditionalIO c  where lower(c.gl) like CONCAT('%',lower(:gl),'%') and "
            + "  lower(c.io) like CONCAT('%',lower(:io),'%') and " +
            " c.company.id = :company ")
    ConditionalIO findByGlAndIoAndCompany(@Param("gl") String gl
            ,@Param("io") String io
            ,@Param("company") Long company);



    @Query("select DISTINCT u from ConditionalIO  u left join u.company c where c.code = :code")
    List<ConditionalIO> findByCompanyCode(@Param("code")String code);

}
