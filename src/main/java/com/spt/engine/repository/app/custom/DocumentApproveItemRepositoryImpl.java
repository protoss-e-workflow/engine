package com.spt.engine.repository.app.custom;

import com.spt.engine.entity.app.DocumentApproveItem;
import com.spt.engine.repository.app.DocumentApproveItemRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Created by dev-spt on 2/2/2561.
 */
public class DocumentApproveItemRepositoryImpl implements DocumentApproveItemRepositoryCustom{

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public void deleteDocumentApproveItem(Long id) {
        DocumentApproveItem documentApproveItem = entityManager.find(DocumentApproveItem.class,id);
        entityManager.remove(documentApproveItem);
        entityManager.flush();
    }
}
