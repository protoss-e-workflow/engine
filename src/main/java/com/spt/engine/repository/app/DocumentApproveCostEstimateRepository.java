package com.spt.engine.repository.app;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.spt.engine.entity.app.DocumentApproveCostEstimate;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DocumentApproveCostEstimateRepository extends JpaSpecificationExecutor<DocumentApproveCostEstimate>, JpaRepository<DocumentApproveCostEstimate, Long>, PagingAndSortingRepository<DocumentApproveCostEstimate, Long> {

    @Query("select DISTINCT u from DocumentApproveCostEstimate u where u.documentApprove.id = :documentApprove")
    List<DocumentApproveCostEstimate> findByDocumentApproveEquals(@Param("documentApprove") Long documentApprove);
}
