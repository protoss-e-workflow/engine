package com.spt.engine.repository.app;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.app.Company;
import com.spt.engine.entity.app.CostCenter;
import com.spt.engine.entity.general.RunningType;

import java.util.List;

public interface CompanyRepository  extends JpaSpecificationExecutor<Company>, JpaRepository<Company, Long>, PagingAndSortingRepository<Company, Long> {

	Company findByCode(@Param("code") String code);

	@Query("select DISTINCT u from Company u order by u.code")
	List<Company> findAllCompany();

}
