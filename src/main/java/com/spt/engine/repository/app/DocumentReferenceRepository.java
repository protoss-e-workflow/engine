package com.spt.engine.repository.app;

import com.spt.engine.entity.app.DocumentReference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DocumentReferenceRepository extends JpaSpecificationExecutor<DocumentReference>, JpaRepository<DocumentReference, Long>, PagingAndSortingRepository<DocumentReference, Long> {

    List<DocumentReference> findByDocReferenceNumberAndAndDocumentTypeCode(@Param ("docReferenceNumber") String docReferenceNumber,@Param("documentTypeCode") String documentTypeCode);
}
