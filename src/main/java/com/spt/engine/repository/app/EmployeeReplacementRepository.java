package com.spt.engine.repository.app;

import com.spt.engine.entity.app.Delegate;
import com.spt.engine.entity.app.EmployeeReplacement;
import com.spt.engine.repository.app.custom.EmployeeReplacementRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EmployeeReplacementRepository extends JpaSpecificationExecutor<EmployeeReplacement>, JpaRepository<EmployeeReplacement, Long>, PagingAndSortingRepository<EmployeeReplacement, Long>,EmployeeReplacementRepositoryCustom {

	List<EmployeeReplacement> findByEmpUserNameReplace(@Param("empUserNameReplace") String empUserNameReplace);
	List<EmployeeReplacement> findByEmpUserNameAgree(@Param("empUserNameAgree") String empUserNameAgree);








	@Query("select DISTINCT u from EmployeeReplacement u  where  (u.empCodeReplace = :empCodeReplace) and u.empCodeAgree like CONCAT('%',:empCodeAgree,'%')  order by u.empCodeAgree ")
	Page<EmployeeReplacement> findByEmpCodeReplaceAndEmpCodeAgreeIgnoreCaseContaining(
			@Param("empCodeReplace") String empCodeReplace,
			@Param("empCodeAgree") String empCodeAgree,
			Pageable pageable);


	List<EmployeeReplacement> findByEmpCodeReplace(@Param("empCodeReplace")String empCodeReplace);

	EmployeeReplacement findByEmpCodeReplaceAndEmpCodeAgree(@Param("empCodeReplace")String empCodeReplace,
																  @Param("empCodeAgree")String empCodeAgree);


}
