package com.spt.engine.repository.app;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.app.Company;
import com.spt.engine.entity.app.CostCenter;
import com.spt.engine.entity.app.Location;
import com.spt.engine.entity.general.RunningType;

import java.util.List;

public interface LocationRepository  extends JpaSpecificationExecutor<Location>, JpaRepository<Location, Long>, PagingAndSortingRepository<Location, Long> {

	Location findByCode(@Param("code") String code);
	List<Location> findByLocationType(@Param("locationType") String locationType);
	Location findByCodeAndLocationType(@Param("code") String code, @Param("locationType") String locationType);

	@Query("select DISTINCT l from Location l where  lower(l.code) like CONCAT('%',lower(:code),'%') and "
			+ "  lower(l.description) like CONCAT('%',lower(:description),'%')      and "
			+ " l.locationType = :locationType ")
	Page<Location> findByCodeIgnoreCaseContainingAndDescriptionIgnoreCaseContainingAndLocationType(
			@Param("code") String code,
			@Param("description") String description,
			@Param("locationType") String locationType,
			Pageable pageable);


	Location findByPsaCode(@Param("psaCode")String psaCode);

	@Query("select l from Location l where l.locationType = :locationType and lower(l.description) like CONCAT('%',lower(:description),'%')")
	List<Location> findByLocationTypeAndDescription(@Param("locationType") String locationType, @Param("description") String description,Pageable pageable);

}



