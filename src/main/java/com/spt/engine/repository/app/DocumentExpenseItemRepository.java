package com.spt.engine.repository.app;

import com.spt.engine.entity.app.DocumentExpense;
import com.spt.engine.entity.app.DocumentExpenseItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;

public interface DocumentExpenseItemRepository extends JpaSpecificationExecutor<DocumentExpenseItem>, JpaRepository<DocumentExpenseItem, Long>, PagingAndSortingRepository<DocumentExpenseItem, Long> {

    @Query("select de from DocumentExpenseItem de where de.documentExp.id = :documentExp")
    List<DocumentExpenseItem> findByDocumentExp(@Param("documentExp") Long documentExp);

    @Query("select de from DocumentExpenseItem de " +
            "left join de.expTypeByCompany etc " +
            "left join etc.expenseType et " +
            "where de.documentExp.id = :documentExp " +
            "and et.fixCode = 'EXPF_004'")
    List<DocumentExpenseItem> findByDocumentExpForPerDiemForeign(@Param("documentExp") Long documentExp);

    @Query("select de from DocumentExpenseItem de " +
            "left join de.expTypeByCompany etc " +
            "left join etc.expenseType et " +
            "where de.documentExp.id = :documentExp " +
            "and et.fixCode = 'EXPF_006'")
    List<DocumentExpenseItem> findByDocumentExpForPerDiemDomestic(@Param("documentExp") Long documentExp);

    @Query("select de from DocumentExpenseItem de " +
            "left join de.expTypeByCompany etc " +
            "left join etc.expenseType et " +
            "where de.documentExp.id = :documentExp " +
            "and et.code = :expenseTypeCode" )
    List<DocumentExpenseItem> findByDocumentExpAndExpenseTypeCode(@Param("documentExp") Long documentExp,@Param("expenseTypeCode")String expenseTypeCode);
}
