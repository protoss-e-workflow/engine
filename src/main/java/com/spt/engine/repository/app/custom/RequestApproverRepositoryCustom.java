package com.spt.engine.repository.app.custom;

import com.spt.engine.entity.app.Request;

import java.util.List;

public interface RequestApproverRepositoryCustom {
    List<Request> findByApproverUsernameAndRequestStatusCodeNotNull(String userName);
}
