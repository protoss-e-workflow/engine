package com.spt.engine.repository.app.projection;

import com.spt.engine.entity.app.Company;
import com.spt.engine.entity.app.ConditionalIO;
import org.springframework.data.rest.core.config.Projection;

import java.sql.Timestamp;

@Projection(name = "inlineConditionalIO",types = ConditionalIO.class)
public interface InlineConditionalIO {

    Long getId();
    Long getVersion();
    String getCreatedBy();
    String getUpdateBy();
    Timestamp getCreatedDate();
    Timestamp getUpdateDate();
    String getGl();
    Boolean getFlagActive();

    String getIo();

    Company getCompany();


}
