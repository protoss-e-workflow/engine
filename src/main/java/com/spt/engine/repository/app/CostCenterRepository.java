package com.spt.engine.repository.app;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.app.CostCenter;

import java.util.List;

public interface CostCenterRepository  extends JpaSpecificationExecutor<CostCenter>, JpaRepository<CostCenter, Long>, PagingAndSortingRepository<CostCenter, Long> {

	CostCenter findByCode(@Param("code") String code);

	List<CostCenter> findByEmpUserName(@Param("empUserName") String empUserName);
//	@Query("select  u from CostCenter u where u.empUserName = :empUserName")
//	List<CostCenter> findByEmpUserName(@Param("empUserName") String empUserName);

}
