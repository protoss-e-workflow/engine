package com.spt.engine.repository.app;

import com.spt.engine.entity.app.ExpenseTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ExpenseTypeReferenceRepository extends JpaSpecificationExecutor<ExpenseTypeReference>, JpaRepository<ExpenseTypeReference, Long>, PagingAndSortingRepository<ExpenseTypeReference, Long> {



	@Query("select DISTINCT u from ExpenseTypeReference u left join u.expenseType r where r.id = :expenseType and u.completeReason = :completeReason")
	ExpenseTypeReference findByCompleteReason(@Param("completeReason") String completeReason,
											  @Param("expenseType") Long expenseType);


	@Query("select DISTINCT u from ExpenseTypeReference u  where  u.expenseType.id = :expenseType")
	Page<ExpenseTypeReference> findExpenseTypeReferenceByExpenseTypeId(
            @Param("expenseType") Long expenseType,
            Pageable pageable);
}
