package com.spt.engine.repository.app.custom;

import com.spt.engine.entity.general.MasterDataDetail;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 	Create by SiriradC. 2017.07.17
 *  Process Calculate PerDiem Interface
 */

public interface InternalOrderRepositoryCustom {


	public void saveInternalOrder_From_SAP_Interface();



}
