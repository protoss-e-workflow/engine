package com.spt.engine.repository.app.custom;

import com.spt.engine.entity.general.MasterDataDetail;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 	Create by SiriradC. 2017.07.17
 *  Process Calculate PerDiem Interface
 */

public interface RouteDistanceProcessCalculateRepositoryCustom {


	public Double processCalculateTotalAmountDistanceRoute(Long locationFrom,Long locationTo, String companyCode);
	public List<Map<String,Object>> convertMasterDataDistanceToMapData(Set<MasterDataDetail> item);
	public  Double getAmountFromDistance(List<Map<String,Object>> list, Double distances);
	public  Double getAmountFromDistanceSendOnlyDistance(String companyCode,Double distances);



}
