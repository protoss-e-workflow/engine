package com.spt.engine.repository.app;

import com.spt.engine.entity.app.DocumentExpItemAttachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DocumentExpenseItemAttachmentRepository extends JpaSpecificationExecutor<DocumentExpItemAttachment>, JpaRepository<DocumentExpItemAttachment, Long>, PagingAndSortingRepository<DocumentExpItemAttachment, Long> {

    @Query("select DISTINCT u from DocumentExpItemAttachment u left join u.documentExpenseItem r where r.id in :documentExpenseItem order by u.id ")
    List<DocumentExpItemAttachment> findDocumentExpenseItemAttachmentByDocumentExpenseItemId(@Param("documentExpenseItem") Long documentExpenseItem);
}
