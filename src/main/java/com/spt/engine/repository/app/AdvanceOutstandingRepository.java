package com.spt.engine.repository.app;

import com.spt.engine.entity.app.AdvanceOutstanding;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AdvanceOutstandingRepository extends JpaSpecificationExecutor<AdvanceOutstanding>, JpaRepository<AdvanceOutstanding, Long>, PagingAndSortingRepository<AdvanceOutstanding, Long> {

    @Query("select DISTINCT a from AdvanceOutstanding a where a.vendorNo like CONCAT('%',:venderNo) order by a.dateTime desc")
    List<AdvanceOutstanding> findByVenderNo(@Param("venderNo") String venderNo);

}