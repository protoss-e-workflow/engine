package com.spt.engine.repository.app.custom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class DocumentExpenseItemRepositoryImpl implements DocumentExpenseItemRepositoryCustom {

    static final Logger LOGGER = LoggerFactory.getLogger(DocumentExpenseItemRepositoryImpl.class);

    @Autowired
    DataSource dataSource;

    @Override
    public List<Map<String,Object>> checkBudgetInWorkflow(String costCenter, String gl, String docNumber, String expItemId) {
        JdbcTemplate jdbcTemplate = null;
        List<Map<String,Object>> results = new ArrayList<>();
        try{
            String sql = "select c.doc_number AS WF_DOC_NUMBER, " +
                    "       c.company_code AS COMP_CODE, " +
                    "       c.cost_center_code AS COST_CENTER, " +
                    "       a.gl_code AS GL_CODE, " +
                    "       SUM(a.amount) AS AMOUNT " +
                    "from document_expense_item a "+
                    "LEFT JOIN document_expense b on a.document_exp = b.id " +
                    "LEFT JOIN document c on b.document_id = c.id " +
                    "LEFT JOIN request d on c.request_id = d.id " +
                    "LEFT JOIN request_approver e on e.request = d.id " +
                    "where e.request_status_code IS NULL " +
                    "and   e.action_state        = 'ACC' " +
                    "and   c.document_status     = 'ONP' " +
                    "and   c.cost_center_code    =  '"+costCenter+"' " +
                    "and   a.gl_code             =  '"+gl+"' " +
                    "and   c.doc_number          <>  '"+docNumber+"' " +
                    "GROUP BY c.doc_number,c.company_code,c.cost_center_code,a.gl_code " +
                    "UNION ALL " +
                    "select c.doc_number AS WF_DOC_NUMBER, " +
                    "       c.company_code AS COMP_CODE, " +
                    "       c.cost_center_code AS COST_CENTER, " +
                    "       a.gl_code AS GL_CODE, " +
                    "       SUM(a.amount) AS AMOUNT " +
                    "from document_expense_item a " +
                    "LEFT JOIN document_expense b on a.document_exp = b.id " +
                    "LEFT JOIN document c on b.document_id = c.id " +
                    "LEFT JOIN request d on c.request_id = d.id " +
                    "LEFT JOIN request_approver e on e.request = d.id " +
                    "where e.request_status_code IS NULL " +
                    "and   e.action_state        =  'ACC' " +
                    "and   c.document_status     = 'ONP' " +
                    "and   c.cost_center_code    =  '"+costCenter+"' " +
                    "and   a.gl_code             =  '"+gl+"' " +
                    "and   c.doc_number          =  '"+docNumber+"' " +
                    "and   a.id                  <> '"+expItemId+"' " +
                    "GROUP BY c.doc_number,c.company_code,c.cost_center_code,a.gl_code " +
                    "UNION ALL " +
                    "select c.doc_number AS WF_DOC_NUMBER, " +
                    "       c.company_code AS COMP_CODE, " +
                    "       c.cost_center_code AS COST_CENTER, " +
                    "       a.gl_code AS GL_CODE, " +
                    "       SUM(a.amount) AS AMOUNT " +
                    "from document_expense_item a " +
                    "LEFT JOIN document_expense b on a.document_exp = b.id " +
                    "LEFT JOIN document c on b.document_id = c.id " +
                    "where c.document_status     =  'DRF' " +
                    "and   c.cost_center_code    =  '"+costCenter+"' " +
                    "and   a.gl_code             =  '"+gl+"' " +
                    "and   c.doc_number          =  '"+docNumber+"' " +
                    "and   a.id                  <> '"+expItemId+"' " +
                    "GROUP BY c.doc_number,c.company_code,c.cost_center_code,a.gl_code";
            jdbcTemplate = new JdbcTemplate(dataSource);
            results = jdbcTemplate.queryForList(sql);
        }catch (Exception e){
            jdbcTemplate = null;
            e.printStackTrace();
        }finally {
            jdbcTemplate = null ;
        }

        return results;
    }

    @Override
    public List<Map<String,Object>> checkBudgetInEwf(String gl,String costCenter) {
        JdbcTemplate jdbcTemplate = null;
        List<Map<String,Object>> results = new ArrayList<>();
        try{
            String sql = "select c.doc_number AS WF_DOC_NUMBER, " +
                    "       c.company_code AS COMP_CODE, " +
                    "       c.cost_center_code AS COST_CENTER, " +
                    "       a.gl_code AS GL_CODE, " +
                    "       SUM(a.amount) AS AMOUNT " +
                    "from document_expense_item a "+
                    "LEFT JOIN document_expense b on a.document_exp = b.id " +
                    "LEFT JOIN document c on b.document_id = c.id " +
                    "LEFT JOIN request d on c.request_id = d.id " +
                    "LEFT JOIN request_approver e on e.request = d.id " +
                    "where e.request_status_code IS NULL " +
                    "and   e.action_state        = 'ACC' " +
                    "and   c.document_status     = 'ONP' " +
                    "and   c.cost_center_code    =  '"+costCenter+"' " +
                    "and   a.gl_code             =  '"+gl+"' " +
                    "GROUP BY c.doc_number,c.company_code,c.cost_center_code,a.gl_code ";

            LOGGER.info("SQL   :  {}",sql);
            jdbcTemplate = new JdbcTemplate(dataSource);
            results = jdbcTemplate.queryForList(sql);
        }catch (Exception e){
            jdbcTemplate = null;
            e.printStackTrace();
        }finally {
            jdbcTemplate = null ;
        }

        return results;
    }
}
