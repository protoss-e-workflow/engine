package com.spt.engine.repository.app;

import com.spt.engine.entity.general.Vat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface VatRepository extends JpaSpecificationExecutor<Vat>, JpaRepository<Vat, Long>, PagingAndSortingRepository<Vat, Long> {
}
