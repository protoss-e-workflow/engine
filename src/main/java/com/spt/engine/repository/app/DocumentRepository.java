package com.spt.engine.repository.app;

import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.Request;
import com.spt.engine.entity.app.TravelDetail;
import com.spt.engine.repository.app.custom.DocumentRepositoryCustom;
import com.sun.tracing.dtrace.ProviderAttributes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface DocumentRepository extends JpaSpecificationExecutor<Document>, JpaRepository<Document, Long>, PagingAndSortingRepository<Document, Long> , DocumentRepositoryCustom {

    @Query("select DISTINCT u " +
            "from Document u " +
            "where u.documentType = :documentType and u.documentStatus = :documentStatus and CONCAT(',',u.userNameMember,',') like CONCAT('%,',:user,',%') " +
            "order by u.id desc ")
    List<Document> findByDocTypeAndDocStatusCompleteAndAppTypeTravelAndTravelMemberUser(@Param("documentType") String documentType, @Param("documentStatus") String documentStatus,@Param("user")String user);

    @Query("select DISTINCT u " +
            "from Document u " +
            "left join u.documentAdvance da " +
            "where u.documentType in (:documentType,'APP') and u.documentStatus = :documentStatus and u.requester = :user " +
            "and da.amount is not null and da.amount > 0 " +
            "and (da.externalClearingDocNumber is null or da.externalClearingDocNumber = '') " +
            "order by u.id desc ")
    List<Document> findByDocTypeAndDocStatusCompleteAndUser(@Param("documentType") String documentType, @Param("documentStatus") String documentStatus,@Param("user")String user);

    @Query("select DISTINCT u " +
            "from Document u " +
            "left join u.documentReferences dr " +
            "left join u.documentAdvance da " +
            "where u.documentType in (:documentType,'APP') and u.documentStatus = :documentStatus and u.requester = :user and dr.docReferenceNumber = :documentNumber " +
            "and da.amount is not null and da.amount > 0 " +
            "and (da.externalClearingDocNumber is null or da.externalClearingDocNumber = '') " +
            "order by u.id desc ")
    List<Document> findByDocTypeAndDocNumberAndDocStatusCompleteAndUser(@Param("documentType") String documentType,@Param("documentNumber") String documentNumber, @Param("documentStatus") String documentStatus,@Param("user")String user);

    List<Document>findByRequesterAndDocumentType(@Param("requester")String requester,@Param("documentType")String documentType);

    List<Document>findByRequesterOrderByDocNumber(@Param("requester")String requester);


    Document findByDocNumber(@Param("docNumber")String docNumber);
    Document findByTmpDocNumber(@Param("tmpDocNumber")String tmpDocNumber);
    
    Document findByRequest(@Param("request")Request request);

    //    @Query("select distinct u from Document u left join u.request r where r.nextApprove = :nextApprover and " +
//     "u.documentType = :documentType order by u.id desc")
    List<Document> findByDocumentTypeAndRequest_NextApproverOrderByIdDesc(
            @Param("documentType") List<String> documentType,
            @Param("nextApprover") String nextApprover);

        @Query("select DISTINCT d from Document d " +
            "where d.requester = :requester " +
            "and d.documentType = :documentType " +
            "and d.docNumber like CONCAT('%',:docNumber,'%') "+
            "and (d.titleDescription like CONCAT('%',:titleDescription,'%') or d.titleDescription is null) "+
            "and d.documentStatus in :documentStatus order by d.createdDate desc")
    Page<Document> findByRequesterAndDocNumberIgnoreCaseContainingAndTitleDescriptionIgnoreCaseContainingAndDocumentTypeAndDocumentStatusInOrderByCreatedDateDesc(
            @Param("requester") String requester,
            @Param("docNumber") String docNumber,
            @Param("titleDescription") String titleDescription,
            @Param("documentType") String documentType,
            @Param("documentStatus") List<String> documentStatus,
            Pageable pageable
    );

    @Query("select count(DISTINCT d) from Document d " +
            "where d.requester = :requester " +
            "and d.documentType = :documentType " +
            "and d.docNumber like CONCAT('%',:docNumber,'%') "+
            "and (d.titleDescription like CONCAT('%',:titleDescription,'%') or d.titleDescription is null) "+
            "and d.documentStatus in :documentStatus")
    Long countByRequesterAndDocNumberIgnoreCaseContainingAndTitleDescriptionIgnoreCaseContainingAndDocumentTypeAndDocumentStatusIn(
            @Param("requester") String requester,
            @Param("docNumber") String docNumber,
            @Param("titleDescription") String titleDescription,
            @Param("documentType") String documentType,
            @Param("documentStatus") List<String> documentStatus
    );


    //    @Query("select DISTINCT d from Document d "+
//            "where d.requester = :requester "+
//            "and d.documentType = :documentType "+
//            "and d.docNumber like CONCAT('%',:docNumber,'%') "+
//            "and (d.titleDescription like CONCAT('%',:titleDescription,'%') or :titleDescription is null)"+
//            "order by d.createdDate desc")
    @Query(value = "SELECT DISTINCT * FROM DOCUMENT WHERE REQUESTER = ?1 AND DOCUMENT_TYPE = ?2 AND DOC_NUMBER LIKE %?3% AND (TITLE_DESCRIPTION IS NULL OR TITLE_DESCRIPTION LIKE %?4%) ORDER BY CREATED_DATE DESC ", nativeQuery =true)
    List<Document> findByRequesterAndDocumentTypeAndDocNumberIgnoreCaseContainingAndTitleDescriptionIgnoreCaseContainingOrderByCreatedDateDesc(
            @Param("requester") String requester,
            @Param("documentType") String documentType,
            @Param("docNumber") String docNumber,
            @Param("titleDescription") String titleDescription
    );

        @Query("SELECT DISTINCT u " +
            "from Document u " +
            "left join u.request r " +
            "where u.docNumber like CONCAT('%',:docNumber,'%') " +
            "and (u.titleDescription like CONCAT('%',:titleDescription,'%') or u.titleDescription is null) " +
            "and u.documentType = :documentType "+
            "and u.documentStatus in :documentStatus " +
            "and r.id in :request order by u.sendDate desc ")
//    @Query(value = "SELECT DISTINCT * FROM DOCUMENT D INNER JOIN REQUEST R ON D.REQUEST_ID = R.ID WHERE D.DOC_NUMBER LIKE %?1% AND (D.TITLE_DESCRIPTION IS NULL OR D.TITLE_DESCRIPTION LIKE %?2%) AND D.DOCUMENT_TYPE = ?3 AND DOCUMENT_STATUS IN (?4) AND R.ID IN (?5) ORDER BY D.SEND_DATE DESC",nativeQuery = true)
    List<Document> findByDocNumberAndTitleDescriptionAndDocumentTypeAndDocumentStatusInAndRequestInOrderBySendDateDesc(
            @Param("docNumber") String docNumber,
            @Param("titleDescription") String titleDescription,
            @Param("documentType") String documentType,
            @Param("documentStatus") List<String> documentStatus,
            @Param("request") List<Long> request
    );

//    @Query("SELECT count(u) " +
//            "from Document u " +
//            "left join u.request r " +
//            "where u.docNumber like CONCAT('%',:docNumber,'%') " +
//            "and (u.titleDescription like CONCAT('%',:titleDescription,'%') or u.titleDescription is null)" +
//            "and u.documentType = :documentType "+
//            "and u.documentStatus in :documentStatus " +
//            "and r.id in :request")
////    @Query(value = "SELECT DISTINCT * FROM DOCUMENT D INNER JOIN REQUEST R ON D.REQUEST_ID = R.ID WHERE D.DOC_NUMBER LIKE %?1% AND (D.TITLE_DESCRIPTION IS NULL OR D.TITLE_DESCRIPTION LIKE %?2%) AND D.DOCUMENT_TYPE = ?3 AND DOCUMENT_STATUS IN (?4) AND R.ID IN (?5) ORDER BY D.SEND_DATE DESC",nativeQuery = true)
//    Long countByDocNumberAndTitleDescriptionAndDocumentTypeAndDocumentStatusInAndRequestIn(
//            @Param("docNumber") String docNumber,
//            @Param("titleDescription") String titleDescription,
//            @Param("documentType") String documentType,
//            @Param("documentStatus") List<String> documentStatus,
//            @Param("request") List<Long> request
//    );

    @Query("select d from Document d " +
            "left join d.documentAdvance da " +
            "where d.requester = :requester " +
            "and d.documentAdvance is not null " +
            "and ((d.documentStatus = :statusComplete " +
            "and (da.externalDocNumber is not null or da.externalDocNumber <> '') " +
            "and (da.externalPaymentDocNumber is not null or da.externalPaymentDocNumber <> '' ) " +
            "and (da.externalClearingDocNumber is null or da.externalClearingDocNumber = '') " +
//            "and (da.paymentStatus is null or da.paymentStatus <> 'PAID') " +
            "and (da.flagUnlock is null or da.flagUnlock <> 'Y' )) or d.documentStatus = :statusOnProcess) ")
    List<Document> findByRequesterForCheckAdvanceOutstanding(@Param("requester") String requester,@Param("statusComplete") String statusComplete,@Param("statusOnProcess") String statusOnProcess);

    @Query("select u " +
            "from Document u " +
            "left join u.documentAdvance da " +
            "where u.documentType in (:documentType) and u.documentStatus = :documentStatus and u.requester = :user and da.flagUnlock = 'Y' " +
            "order by u.id desc ")
    List<Document> findDocumentForCheckUnlockAdvance(@Param("documentType") String documentType, @Param("documentStatus") String documentStatus,@Param("user")String user);

    @Query("SELECT DISTINCT u " +
            "from Document u " +
            "left join u.request r " +
            "left join r.requestApprovers ra " +
            "where u.docNumber like CONCAT('%',:docNumber,'%') " +
            "and (u.titleDescription like CONCAT('%',:titleDescription,'%') or u.titleDescription is null) " +
            "and u.documentType = :documentType "+
            "and u.documentStatus in :documentStatus " +
            "and ra.userNameApprover in :userNameApprover " +
            "and ra.requestStatusCode is not null " +
            "order by u.sendDate desc ")
//    @Query(value = "SELECT DISTINCT * FROM DOCUMENT D INNER JOIN REQUEST R ON D.REQUEST_ID = R.ID WHERE D.DOC_NUMBER LIKE %?1% AND (D.TITLE_DESCRIPTION IS NULL OR D.TITLE_DESCRIPTION LIKE %?2%) AND D.DOCUMENT_TYPE = ?3 AND DOCUMENT_STATUS IN (?4) AND R.ID IN (?5) ORDER BY D.SEND_DATE DESC",nativeQuery = true)
    Page<Document> findByDocNumberAndTitleDescriptionAndDocumentTypeAndDocumentStatusInAndRequestInOrderBySendDateByDocNumberDesc(
            @Param("docNumber") String docNumber,
            @Param("titleDescription") String titleDescription,
            @Param("documentType") String documentType,
            @Param("documentStatus") List<String> documentStatus,
            @Param("userNameApprover") List<String> userNameApprover,
            Pageable pageable
    );

    @Query("SELECT  count(distinct u) " +
            "from Document u " +
            "left join u.request r " +
            "left join r.requestApprovers ra " +
            "where u.docNumber like CONCAT('%',:docNumber,'%') " +
            "and (u.titleDescription like CONCAT('%',:titleDescription,'%') or u.titleDescription is null) " +
            "and u.documentType = :documentType "+
            "and u.documentStatus in :documentStatus " +
            "and ra.userNameApprover in :userNameApprover " +
            "and ra.requestStatusCode is not null ")
//    @Query(value = "SELECT DISTINCT * FROM DOCUMENT D INNER JOIN REQUEST R ON D.REQUEST_ID = R.ID WHERE D.DOC_NUMBER LIKE %?1% AND (D.TITLE_DESCRIPTION IS NULL OR D.TITLE_DESCRIPTION LIKE %?2%) AND D.DOCUMENT_TYPE = ?3 AND DOCUMENT_STATUS IN (?4) AND R.ID IN (?5) ORDER BY D.SEND_DATE DESC",nativeQuery = true)
    Long countByDocNumberAndTitleDescriptionAndDocumentTypeAndDocumentStatusInAndRequestIn(
            @Param("docNumber") String docNumber,
            @Param("titleDescription") String titleDescription,
            @Param("documentType") String documentType,
            @Param("documentStatus") List<String> documentStatus,
            @Param("userNameApprover") List<String> userNameApprover
    );

}
