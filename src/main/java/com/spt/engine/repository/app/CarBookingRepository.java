package com.spt.engine.repository.app;

import com.spt.engine.entity.app.CarBooking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CarBookingRepository extends JpaSpecificationExecutor<CarBooking>, JpaRepository<CarBooking, Long>, PagingAndSortingRepository<CarBooking, Long> {

    @Query("select DISTINCT u from CarBooking u left join u.documentApproveItem r where r.id in :documentApproveItem order by u.id ")
    List<CarBooking> findByDocumentApproveItemId(@Param("documentApproveItem") Long documentApproveItem);
}
