package com.spt.engine.repository.app;

import com.spt.engine.entity.app.DocumentAccountRemark;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DocumentAccountRemarkRepository extends JpaSpecificationExecutor<DocumentAccountRemark>, JpaRepository<DocumentAccountRemark, Long>, PagingAndSortingRepository<DocumentAccountRemark, Long> {

    @Query("select DISTINCT u from DocumentAccountRemark u where u.documentId = :document order by u.id ")
    List<DocumentAccountRemark> findByDocumentId(@Param("document") Long document);
}
