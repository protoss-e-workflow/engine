package com.spt.engine.repository.app;

import com.spt.engine.entity.app.Reimburse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ReimburseRepository extends JpaSpecificationExecutor<Reimburse>, JpaRepository<Reimburse, Long>, PagingAndSortingRepository<Reimburse, Long> {



	Reimburse findByRoleCode(@Param("roleCode")String roleCode);



	@Query("select DISTINCT u from Reimburse u  where  u.expenseType.id = :expenseType")
	Page<Reimburse> findReimburseByExpenseTypeId(
            @Param("expenseType") Long expenseType,
            Pageable pageable);
}
