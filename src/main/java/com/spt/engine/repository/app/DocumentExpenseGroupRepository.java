package com.spt.engine.repository.app;

import com.spt.engine.entity.app.DocumentExpenseGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface DocumentExpenseGroupRepository extends JpaSpecificationExecutor<DocumentExpenseGroup>, JpaRepository<DocumentExpenseGroup, Long>, PagingAndSortingRepository<DocumentExpenseGroup, Long> {

    @Query("select de from DocumentExpenseGroup de " +
            "where de.documentExp.id = :documentExp " +
            "order by de.id")
    List<DocumentExpenseGroup> findByDocumentExp(@Param("documentExp") Long documentExp);
}
