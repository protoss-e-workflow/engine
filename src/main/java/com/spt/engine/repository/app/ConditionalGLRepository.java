package com.spt.engine.repository.app;

import com.spt.engine.entity.app.ConditionalGL;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;


public interface ConditionalGLRepository extends JpaSpecificationExecutor<ConditionalGL>, JpaRepository<ConditionalGL, Long>, PagingAndSortingRepository<ConditionalGL, Long> {

    ConditionalGL findByGlCreateIgnoreCaseAndIoIgnoreCaseAndGlSapIgnoreCase(
            @Param("glCreate") String glCreate,
            @Param("io") String io,
            @Param("glSap") String glSap
    );

}
