package com.spt.engine.repository.app.custom;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 	Create by SiriradC. 2017.07.17
 *  Process Calculate PerDiem Interface
 */

public interface PerDiemProcessCalculateRepositoryCustom  {
	public Map<String,Object> processCalculatePerDiemDomestic(Long empLevel,List<Map<String,Object>> data);
	public Map<String,Object> processCalculatePerDiemForeign(Long empLevel,BigDecimal sellingRate,List<Map<String,Object>> data);
}
