package com.spt.engine.repository.app;

import java.util.List;

import com.spt.engine.repository.app.custom.TravelDetailRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.app.TravelDetail;

public interface TravelDetailRepository  extends JpaSpecificationExecutor<TravelDetail>, JpaRepository<TravelDetail, Long>, PagingAndSortingRepository<TravelDetail, Long>,TravelDetailRepositoryCustom {

	@Query("select DISTINCT u from TravelDetail u left join u.documentApproveItem r where r.id in :documentApproveItem order by u.id ")
	List<TravelDetail> findByDocumentApproveItemId( @Param("documentApproveItem") List<Long> documentApproveItem);

}
