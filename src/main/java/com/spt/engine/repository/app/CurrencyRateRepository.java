package com.spt.engine.repository.app;

import com.spt.engine.entity.app.CurrencyRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public interface CurrencyRateRepository extends JpaSpecificationExecutor<CurrencyRate>, JpaRepository<CurrencyRate, Long>, PagingAndSortingRepository<CurrencyRate, Long> {

    @Query("select c from CurrencyRate c where c.dateAction >= :actionDateFrom and c.dateAction <= :actionDateTo")
    CurrencyRate findByActionDateFromActionDateTo(@Param("actionDateFrom") Date actionDateFrom, @Param("actionDateTo") Date actionDateTo);

    @Query("select c from CurrencyRate c where c.dateAction < :actionDate order by c.dateAction desc")
    List<CurrencyRate> findByActionDate(@Param("actionDate") Date actionDate);
}
