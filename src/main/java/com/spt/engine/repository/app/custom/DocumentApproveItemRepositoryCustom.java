package com.spt.engine.repository.app.custom;

/**
 * Created by dev-spt on 2/2/2561.
 */
public interface DocumentApproveItemRepositoryCustom {
    void deleteDocumentApproveItem(Long id);
}
