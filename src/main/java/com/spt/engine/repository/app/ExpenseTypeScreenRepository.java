package com.spt.engine.repository.app;

import com.spt.engine.entity.app.ExpenseTypeScreen;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ExpenseTypeScreenRepository extends JpaSpecificationExecutor<ExpenseTypeScreen>, JpaRepository<ExpenseTypeScreen, Long>, PagingAndSortingRepository<ExpenseTypeScreen, Long> {




	@Query("select DISTINCT u from ExpenseTypeScreen u  where  u.expenseType.id = :expenseType")
	Page<ExpenseTypeScreen> findExpenseTypeScreenByExpenseTypeId(
            @Param("expenseType") Long expenseType,
            Pageable pageable);









}
