package com.spt.engine.repository.app;

import com.spt.engine.entity.app.DocumentApproveItem;
import com.spt.engine.repository.app.custom.DocumentApproveItemRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by dev-spt on 2/2/2561.
 */
public interface DocumentApproveItemRepository extends JpaSpecificationExecutor<DocumentApproveItem>, JpaRepository<DocumentApproveItem, Long>, PagingAndSortingRepository<DocumentApproveItem, Long> ,DocumentApproveItemRepositoryCustom{
}
