package com.spt.engine.repository.app.custom;

import com.spt.engine.entity.app.Delegate;
import com.spt.engine.entity.app.EmployeeReplacement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class EmployeeReplacementRepositoryImpl implements EmployeeReplacementRepositoryCustom{

    @PersistenceContext
    private EntityManager entityManager;

    static final Logger LOGGER = LoggerFactory.getLogger(EmployeeReplacementRepositoryImpl.class);


    @Override
    public Page<EmployeeReplacement> findByEmpCodeReplaceIgnoreCaseContaining(String empCodeReplace, Pageable pageable) {
        // TODO Auto-generated method stub
        Query query = entityManager.createQuery("SELECT DISTINCT u.empCodeReplace As empCodeReplace FROM EmployeeReplacement u WHERE u.empCodeReplace like CONCAT('%',:empCodeReplace,'%') ");
        query.setParameter("empCodeReplace", empCodeReplace);
        query.setMaxResults(pageable.getPageSize());
        query.setFirstResult(pageable.getPageSize() * pageable.getPageNumber());

        List<String> resultSet = query.getResultList();
        List<EmployeeReplacement> resultObj  = new ArrayList();
        if(resultSet != null) for(String obj :resultSet){
            EmployeeReplacement model = new EmployeeReplacement();
            model.setEmpCodeReplace(obj);
            resultObj.add(model);
        }


        Query queryCount = entityManager.createQuery("SELECT count(DISTINCT u.empCodeReplace) FROM EmployeeReplacement u WHERE u.empCodeReplace like CONCAT('%',:empCodeReplace,'%') ");
        queryCount.setParameter("empCodeReplace", empCodeReplace);
        Long totalCount = (Long) queryCount.getSingleResult();

        return new PageImpl(resultObj, pageable, totalCount);
    }



}