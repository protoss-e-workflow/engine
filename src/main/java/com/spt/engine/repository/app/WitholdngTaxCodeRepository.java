package com.spt.engine.repository.app;

import com.spt.engine.entity.general.WithholdingTaxCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by korrakote on 10/19/17.
 */
public interface WitholdngTaxCodeRepository extends JpaSpecificationExecutor<WithholdingTaxCode>, JpaRepository<WithholdingTaxCode, Long>, PagingAndSortingRepository<WithholdingTaxCode, Long> {
}
