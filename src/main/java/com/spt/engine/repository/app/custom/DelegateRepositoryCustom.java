package com.spt.engine.repository.app.custom;

import com.spt.engine.entity.app.Company;
import com.spt.engine.entity.app.Delegate;
import com.spt.engine.entity.app.ExpenseTypeScreen;
import com.spt.engine.entity.general.RunningType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;

public interface DelegateRepositoryCustom  {

    Page<Delegate> findByEmpCodeAssigneeIgnoreCaseContaining(@Param("empCodeAssignee") String empCodeAssignee,Pageable pageable);

}