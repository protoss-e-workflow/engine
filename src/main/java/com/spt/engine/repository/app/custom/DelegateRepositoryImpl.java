package com.spt.engine.repository.app.custom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.spt.engine.controller.CarBookingController;
import com.spt.engine.entity.app.Delegate;

public class DelegateRepositoryImpl implements DelegateRepositoryCustom{

    @PersistenceContext
    private EntityManager entityManager;

    static final Logger LOGGER = LoggerFactory.getLogger(DelegateRepositoryImpl.class);


    @Override
    public Page<Delegate> findByEmpCodeAssigneeIgnoreCaseContaining(String empCodeAssignee,Pageable pageable) {
        // TODO Auto-generated method stub
        Query query = entityManager.createQuery("SELECT DISTINCT u.empCodeAssignee As empCodeAssignee FROM Delegate u WHERE u.empCodeAssignee like CONCAT('%',:empCodeAssignee,'%') ");
        query.setParameter("empCodeAssignee", empCodeAssignee);
        query.setMaxResults(pageable.getPageSize());
        query.setFirstResult(pageable.getPageSize() * pageable.getPageNumber());

        List<String> resultSet = query.getResultList();
        List<Delegate> resultObj  = new ArrayList();
        if(resultSet != null) for(String obj :resultSet){
            Delegate model = new Delegate();
            model.setEmpCodeAssignee(obj);
            resultObj.add(model);
        }


        Query queryCount = entityManager.createQuery("SELECT count(DISTINCT u.empCodeAssignee) FROM Delegate u WHERE u.empCodeAssignee like CONCAT('%',:empCodeAssignee,'%') ");
        queryCount.setParameter("empCodeAssignee", empCodeAssignee);
        Long totalCount = (Long) queryCount.getSingleResult();

        return new PageImpl(resultObj, pageable, totalCount);
    }



}