package com.spt.engine.repository.app;

import com.spt.engine.entity.app.MonthlyPhoneBillPerYear;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface MonthlyPhoneBillPerYearRepository extends JpaSpecificationExecutor<MonthlyPhoneBillPerYear>, JpaRepository<MonthlyPhoneBillPerYear, Long>, PagingAndSortingRepository<MonthlyPhoneBillPerYear, Long> {

    MonthlyPhoneBillPerYear findByUserNameAndYear(@Param("userName")String username,@Param("year")String year);
}
