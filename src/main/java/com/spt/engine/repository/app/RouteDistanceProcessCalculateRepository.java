package com.spt.engine.repository.app;

/**
 * 	Create by SiriradC. 2017.07.17
 *  Process Calculate PerDiem Interface
 */

import com.spt.engine.entity.app.Location;
import com.spt.engine.entity.app.RouteDistance;
import com.spt.engine.entity.general.RunningNumber;
import com.spt.engine.repository.app.custom.PerDiemProcessCalculateRepositoryCustom;
import com.spt.engine.repository.app.custom.RouteDistanceProcessCalculateRepositoryCustom;
import com.spt.engine.repository.app.custom.RouteDistanceProcessCalculateRepositoryImpl;
import com.spt.engine.repository.app.projection.InlineRouteDistance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(excerptProjection = InlineRouteDistance.class)

public interface RouteDistanceProcessCalculateRepository extends CrudRepository<RouteDistance, Long>, RouteDistanceProcessCalculateRepositoryCustom {


    @Query("select DISTINCT u from RouteDistance u  where  u.locationFrom.id = :locationFrom and  u.locationTo.id = :locationTo ")
        RouteDistance findByLocationFromAndLocationTo(@Param("locationFrom") Long locationFrom,
                                                      @Param("locationTo") Long locationTo);


    @Query("select DISTINCT r from RouteDistance r , Location l1, Location l2 where  lower(l1.description) like CONCAT('%',lower(:locationFromDescription),'%') and "
            + " lower(l2.description) like CONCAT('%',lower(:locationToDescription),'%')  and "
            + " l1.id = r.locationFrom and l2.id = r.locationTo ")
    Page<RouteDistance> findByLocationFromDescriptionAndLocationToDescription(
            @Param("locationFromDescription") String locationFromDescription,
            @Param("locationToDescription") String locationToDescription,
            Pageable pageable);

}
