package com.spt.engine.repository.app;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.app.Company;
import com.spt.engine.entity.app.CostCenter;
import com.spt.engine.entity.app.DocumentAttachment;
import com.spt.engine.entity.app.TravelDetail;
import com.spt.engine.entity.general.RunningType;

public interface DocumentAttachmentRepository  extends JpaSpecificationExecutor<DocumentAttachment>, JpaRepository<DocumentAttachment, Long>, PagingAndSortingRepository<DocumentAttachment, Long> {

	@Query("select DISTINCT u from DocumentAttachment u left join u.document r where r.id in :document order by u.id ")
	List<DocumentAttachment> findByDocumentId( @Param("document") Long document);
}
