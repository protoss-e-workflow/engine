package com.spt.engine.repository.app;

import com.spt.engine.entity.app.PetrolAllowancePerMonth;
import com.spt.engine.entity.general.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;


public interface PetrolAllowancePerMonthRepository extends JpaSpecificationExecutor<PetrolAllowancePerMonth>, JpaRepository<PetrolAllowancePerMonth, Long>, PagingAndSortingRepository<PetrolAllowancePerMonth, Long>  {


	@Query("select DISTINCT p from PetrolAllowancePerMonth p where  lower(p.empCode) like CONCAT('%',lower(:empCode),'%')" )
	Page<PetrolAllowancePerMonth> findByEmpCodeIgnoreCaseContaining(
            @Param("empCode") String empCode,
            Pageable pageable);

	PetrolAllowancePerMonth findByEmpCode( @Param("empCode") String empCode);

	PetrolAllowancePerMonth findByEmpUserName( @Param("empUserName") String empUserName);

}
