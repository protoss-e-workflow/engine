package com.spt.engine.repository.app;

import com.spt.engine.entity.general.WithholdingTax;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WithholdingTaxRepository extends JpaSpecificationExecutor<WithholdingTax>, JpaRepository<WithholdingTax, Long>, PagingAndSortingRepository<WithholdingTax, Long> {
}
