package com.spt.engine.repository.app;

/**
 * 	Create by SiriradC. 2017.07.17
 *  Process Calculate PerDiem Interface
 */
import org.springframework.data.repository.CrudRepository;

import com.spt.engine.entity.general.RunningNumber;
import com.spt.engine.repository.app.custom.PerDiemProcessCalculateRepositoryCustom;

public interface PerDiemProcessCalculateRepository extends CrudRepository<RunningNumber, Long>, PerDiemProcessCalculateRepositoryCustom {
	
}
