package com.spt.engine.repository.app;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.app.TravelDetail;
import com.spt.engine.entity.app.TravelMember;

public interface TravelMemberRepository extends JpaSpecificationExecutor<TravelMember>, JpaRepository<TravelMember, Long>, PagingAndSortingRepository<TravelMember, Long> {

	@Query("select DISTINCT u from TravelMember u left join u.documentApproveItem r where r.id in :documentApproveItem order by u.id ")
	List<TravelMember> findByDocumentApproveItemId( @Param("documentApproveItem") List<Long> documentApproveItem);

	@Query("select  u from TravelMember u left join u.documentApproveItem r where u.memberUser = :memberUser and r.id in :documentApproveItem order by u.id ")
	List<TravelMember> findByMemberUserAndDocumentApproveItem(@Param("memberUser") String memberUser,@Param("documentApproveItem") Long documentApproveItem);
}
