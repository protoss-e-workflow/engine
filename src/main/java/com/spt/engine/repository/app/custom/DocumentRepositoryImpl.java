package com.spt.engine.repository.app.custom;

import com.google.gson.*;
import com.spt.engine.ConstantVariable;
import com.spt.engine.controller.DocumentController;
import com.spt.engine.entity.app.*;
import com.spt.engine.entity.general.MasterDataDetail;
import com.spt.engine.entity.general.RunningType;
import com.spt.engine.entity.general.User;

import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

import com.spt.engine.repository.app.DocumentExpenseGroupRepository;
import com.spt.engine.repository.app.DocumentExpenseItemRepository;
import com.spt.engine.repository.app.DocumentRepository;
import com.spt.engine.repository.app.ExpenseTypeByCompanyRepository;
import com.spt.engine.repository.general.*;
import com.spt.engine.service.EmplyeeProfileService;
import com.spt.engine.util.DateUtil;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionBuilder;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.print.Doc;
import javax.validation.constraints.Null;

public class DocumentRepositoryImpl implements DocumentRepositoryCustom{

    static final Logger LOGGER = LoggerFactory.getLogger(DocumentRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    DocumentRepository documentRepository;

    @Autowired
    ExpenseTypeByCompanyRepository expenseTypeByCompanyRepository;

    @Autowired
    DocumentExpenseItemRepository documentExpenseItemRepository;

    @Autowired
    DocumentExpenseGroupRepository documentExpenseGroupRepository;

    @Autowired
    RunningTypeRepository runningTypeRepository;

    @Autowired
    RequestRepository requestRepository;

    @Autowired
    EmplyeeProfileService employeeProfileService;

    @Autowired
    RunningNumberRepository runningNumberRepository;

    @Autowired
    RequestApproverRepository requestApproverRepository;

    @Autowired
    InterfaceActiveLogRepository interfaceActiveLogRepository;

    @Autowired
    MasterDataDetailRepository masterDataDetailRepository;

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws com.google.gson.JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };


    protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();

    @Override
    public List<Document> findByDocumentTypeAndDocumentStatusAndApproveTypeAndRequester(String documentType, String documentStatus, List<String> approveType,String requester) {
        Query query = entityManager.createQuery("SELECT DISTINCT u.docNumber AS docNumber from Document u where u.documentType = :documentType and u.documentStatus = :documentStatus and u.approveType in :approveType and u.requester = :requester");
        query.setParameter("documentType", documentType);
        query.setParameter("documentStatus", documentStatus);
        query.setParameter("approveType", approveType);
        query.setParameter("requester", requester);


        List<String> resultSet = query.getResultList();
        List<Document> resultObj  = new ArrayList();
        if(resultSet != null) for(String obj :resultSet){
            Document model = new Document();
            model.setDocNumber(obj);
            resultObj.add(model);
        }

        return resultObj;
    }

    @Override
    public List<Document> findByDocumentTypeAndDocumentStatusAndApproveTypeAndRequesterAndKeySearch(String documentType, String documentStatus, List<String> approveType,String requester,String keySearch) {
        Query query = entityManager.createQuery("SELECT DISTINCT u.docNumber AS docNumber from Document u where u.documentType = :documentType and u.documentStatus = :documentStatus and u.approveType in :approveType and u.requester = :requester and u.docNumber like CONCAT('%',:docNumber,'%')");
        query.setParameter("documentType", documentType);
        query.setParameter("documentStatus", documentStatus);
        query.setParameter("approveType", approveType);
        query.setParameter("requester", requester);
        query.setParameter("docNumber", keySearch);


        List<String> resultSet = query.getResultList();
        List<Document> resultObj  = new ArrayList();
        if(resultSet != null) for(String obj :resultSet){
            Document model = new Document();
            model.setDocNumber(obj);
            resultObj.add(model);
        }

        return resultObj;
    }


	@Override
	public List<Document> findSAPDocumentNumberDocumentAdvanceByRequestNumber(String requestNumber) {
		
				Query query = entityManager.createQuery("SELECT new com.spt.engine.repository.app.custom.DucumentDTO(da.externalDocNumber,u.totalAmount) from Document u LEFT JOIN u.documentAdvance da where u.docNumber like CONCAT('%',:requestNumber,'%') ",DucumentDTO.class);
		        query.setParameter("requestNumber", requestNumber);
		        


		        List<DucumentDTO> resultSet = query.getResultList();
		        List<Document> resultObj  = new ArrayList();
		        if(resultSet != null) for(DucumentDTO obj :resultSet){
		            Document model = new Document();
		            
		            model.setDocNumber(obj.getDocNumber());
		            model.setTotalAmount(obj.getTotalAmount());
		            resultObj.add(model);
		        }

		        return resultObj;
	}

	@Override
	public DocumentAdvance findDocumentAdvanceByExternalDocNumber(String externalDocNumber,String companyCode) {
		Query query = entityManager.createQuery("SELECT DISTINCT da from Document u LEFT JOIN u.documentAdvance da  where da.externalDocNumber like CONCAT('%',:externalDocNumber,'%') AND u.companyCode  like CONCAT('%',:companyCode,'%')  ");
        query.setParameter("externalDocNumber", externalDocNumber);
        query.setParameter("companyCode", companyCode);

        if(query.getResultList().size()>0){
            return (DocumentAdvance)query.getResultList().get(0);
        }else{
            return null;
        }

	}

	@Override
	public List<DocumentExpenseItem> findDocumentExpenseItemByExternalDocNumber(String externalDocNumber,String companyCode) {
		Query query = entityManager.createQuery("SELECT DISTINCT dei from Document u LEFT JOIN u.documentExpense de LEFT JOIN de.documentExpenseItems dei  where dei.externalDocNumber like CONCAT('%',:externalDocNumber,'%')  AND u.companyCode  like CONCAT('%',:companyCode,'%')   ");
        query.setParameter("externalDocNumber", externalDocNumber);
        query.setParameter("companyCode", companyCode);
		return query.getResultList();
	}

	@Override
	public List<DocumentExpenseItem> findDocumentExpenseItemByExternalClearingDocNumber(String externalDocNumber,String companyCode) {
		//Query query = entityManager.createQuery("SELECT DISTINCT u from DocumentExpenseItem u where u.externalClearingDocNumber like CONCAT('%',:externalDocNumber,'%') ");
		Query query = entityManager.createQuery("SELECT DISTINCT dei from Document u LEFT JOIN u.documentExpense de LEFT JOIN de.documentExpenseItems dei  where dei.externalClearingDocNumber like CONCAT('%',:externalDocNumber,'%')  AND u.companyCode  like CONCAT('%',:companyCode,'%')   ");
        query.setParameter("externalDocNumber", externalDocNumber);
        query.setParameter("companyCode", companyCode);
		return query.getResultList();
	}
	

    @Override
    @SuppressWarnings("Duplicates")
    public List<Document> findByCreatedDateStartAndCreatedDateEndAndCreatorAndDocNumberAndDocumentType(
            String createdDateStart,
            String createdDateEnd,
            String creator,
            String documentNumber,
            List<String> documentType,
            List<String> documentStatus,
            Long first,
            Long max) {

        int pageNumber = (int) (long) first;
        int pageSize = (int) (long) max;

         Query  query = entityManager.createQuery("" +
                "SELECT d from Document d where CREATED_DATE " +
                "BETWEEN CONVERT(datetime,CONVERT(nvarchar(20),CAST (:createdDateStart as date),110)+' 00:00:00',110) " +
                "AND CONVERT(datetime,CONVERT(nvarchar(20),CAST(:createdDateEnd as date),110)+' 23:59:59',110) " +
                "AND created_by like CONCAT('%',:creator,'%') AND doc_number like CONCAT('%',:documentNumber,'%') " +
                "AND document_type IN (:documentType) and document_status IN :documentStatus " +
                 "ORDER BY CREATED_DATE DESC")
                .setParameter("createdDateStart",createdDateStart)
                .setParameter("createdDateEnd",createdDateEnd)
                .setParameter("creator",creator)
                .setParameter("documentNumber",documentNumber)
                .setParameter("documentType",documentType)
                 .setParameter("documentStatus",documentStatus)
                 .setFirstResult(pageNumber)
                 .setMaxResults(pageSize);

        List<Document> resultSet = query.getResultList();

        return resultSet;

//        Criteria criteria = ((Session) entityManager.getDelegate()).createCriteria(Document.class,"Document").setFirstResult(pageNumber).setMaxResults(pageSize);
//
//
//        try{
//            if(!"".equals(creator)){
//                criteria.add(Restrictions.like("Document.createdBy","%"+ creator +"%").ignoreCase());
//            }
//            if(!"".equals(documentNumber)){
//                criteria.add(Restrictions.like("Document.docNumber","%"+ documentNumber +"%").ignoreCase());
//            }
//            if(!"".equals(createdDateStart)){
//                criteria.add(Restrictions.ge("Document.createdDate", DateUtil.getTimeStamp(createdDateStart)));
//            }
//            if(!"".equals(createdDateEnd)){
//                criteria.add(Restrictions.le("Document.createdDate", DateUtil.getTimeStampGetMaxTime(createdDateEnd)));
//            }
//            if(!"".equals(documentType)){
//                criteria.add(Restrictions.in("Document.documentType",documentType));
//            }
//            criteria.add(Restrictions.in("Document.documentStatus",new ArrayList(Arrays.asList("CMP","ONP","CCL","DRF","REJ"))));
//
//            criteria.addOrder(Order.desc("Document.createdDate"));
//        }catch(Exception e){
//            e.printStackTrace();
//
//        }
//        return criteria.list();
    }


    public Long pagingFindByCreatedDateStartAndCreatedDateEndAndCreatorAndDocNumberAndDocumentType(
            String createdDateStart,
            String createdDateEnd,
            String creator,
            String documentNumber,
            List<String> documentType,
            List<String> documentStatus){

        Query  query = entityManager.createQuery("" +
                "SELECT count (d) from Document d where CREATED_DATE " +
                "BETWEEN CONVERT(datetime,CONVERT(nvarchar(20),CAST (:createdDateStart as date),110)+' 00:00:00',110) " +
                "AND CONVERT(datetime,CONVERT(nvarchar(20),CAST(:createdDateEnd as date),110)+' 23:59:59',110) " +
                "AND created_by like CONCAT('%',:creator,'%') AND doc_number like CONCAT('%',:documentNumber,'%') " +
                "AND document_type IN (:documentType) and document_status IN :documentStatus ")
                .setParameter("createdDateStart",createdDateStart)
                .setParameter("createdDateEnd",createdDateEnd)
                .setParameter("creator",creator)
                .setParameter("documentNumber",documentNumber)
                .setParameter("documentType",documentType)
                .setParameter("documentStatus",documentStatus);

        return Long.valueOf(String.valueOf(query.getResultList().get(0)));

//        Criteria criteria = ((Session) entityManager.getDelegate()).createCriteria(Document.class,"Document");
////        Long count = null;
//        try{
//            if(!"".equals(creator)){
//                criteria.add(Restrictions.like("Document.createdBy","%"+ creator +"%").ignoreCase());
//            }
//            if(!"".equals(documentNumber)){
//                criteria.add(Restrictions.like("Document.docNumber","%"+ documentNumber +"%").ignoreCase());
//            }
//            if(!"".equals(createdDateStart)){
//                criteria.add(Restrictions.ge("Document.createdDate", DateUtil.getTimeStamp(createdDateStart)));
//            }
//            if(!"".equals(createdDateEnd)){
//                criteria.add(Restrictions.le("Document.createdDate", DateUtil.getTimeStampGetMaxTime(createdDateEnd)));
//            }
//            if(!"".equals(documentType)){
//                criteria.add(Restrictions.in("Document.documentType",documentType));
//            }
//            criteria.add(Restrictions.in("Document.documentStatus",new ArrayList(Arrays.asList("CMP","ONP","CCL","DRF","REJ"))));
//
//        }catch(Exception e){
//            e.printStackTrace();
//        }
////        count = Long.valueOf(criteria.list().size());
//
//        Integer totalResult = ((Number)criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
//
//        return Long.valueOf(totalResult);



    }

    @Override
    @Transactional
    public String cancelDocumentManual(String documentNumber) {
        String status = "FAIL";
        Document document = documentRepository.findByDocNumber(documentNumber);
        if(document != null){
            document.setDocumentStatus(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_REJECT);
            entityManager.merge(document);
            entityManager.flush();

            Request request = document.getRequests();
            if(request != null){
                request.setNextApprover(null);
                request.setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_REJECT);
                request.setUpdateBy("ssReject");
                entityManager.merge(request);
                entityManager.flush();

                List<RequestApprover> requestApproverLs = new ArrayList<>(request.getRequestApprovers());
                if(requestApproverLs.size() > 0){
                    for(RequestApprover approver : requestApproverLs){
                        if(approver.getActionTime() == null || StringUtils.isEmpty(approver.getActionTime())){
                            approver.setActionTime(new Timestamp(new Date().getTime()));
                            entityManager.merge(approver);
                            entityManager.flush();
                            status = "SUCCESS";
                        }
                    }
                }
            }
        }

        return status;
    }

    @Override
    @Transactional
    public String clearDocumentManual(String documentNumber) {
        String status = "FAIL";
        Document document = documentRepository.findByDocNumber(documentNumber);
        if(document != null){
            DocumentAdvance advance = document.getDocumentAdvance();
            if(advance != null){
                advance.setExternalClearingDocNumber("SAP_MANUAL");
                entityManager.merge(advance);
                entityManager.flush();

                status = "SUCCESS";
            }
        }

        return status;
    }

    @Override
	public DocumentExpenseItem findDocumentExpenseItemByDocumentIdAndSequence(Long documentId, String seqKey) {
		// TODO Auto-generated method stub
		Query query = entityManager.createQuery("SELECT u from DocumentExpenseItem u where u.documentId =:documentId  AND u.seqKey  like CONCAT('%',:seqKey,'%')   ");
        query.setParameter("documentId", documentId);
        query.setParameter("seqKey", seqKey);
		return (DocumentExpenseItem) query.getResultList().get(0);
	}

    @Override
    @Transactional
    public Map<String, String> createDocumentFromEmed(String document) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Map<String,String> map = new HashMap<>();
        List<ExpenseTypeByCompany> expenseTypeByCompanyList = null;
        JSONObject jsonFromEmed = new JSONObject(document);

        try {

            /** find Requester **/
            ResponseEntity<String> employeeData = employeeProfileService.findEmployeeProfileByKeySearch(jsonFromEmed.getString("empId"));
            Map employeeMap = gson.fromJson(employeeData.getBody(), Map.class);
            List employeeList = (List) employeeMap.get("restBodyList");
            employeeMap = (Map) employeeList.get(0);

            String paPsa = jsonFromEmed.getString("compCode") + "-" + jsonFromEmed.getString("location");
            LOGGER.info("PA-PSA From EMED    :   {}",paPsa);
            paPsa = employeeMap.get("Personal_PA_ID").toString() + "-" + employeeMap.get("Personal_PSA_ID").toString();
            LOGGER.info("PA-PSA From EWORKFLOW    :   {}",paPsa);


            /** find Account **/
            ResponseEntity<String> accountByPaPsa = employeeProfileService.findAccountByPaPsaAndKeySearch(paPsa, employeeMap.get("User_name").toString());
            Map accountByPaPsaMap = gson.fromJson(accountByPaPsa.getBody(), Map.class);
            String userNameAccount = accountByPaPsaMap.get("profile").toString().split(":")[1].split("-")[0];
            ResponseEntity<String> accountData = employeeProfileService.findEmployeeProfileByKeySearch(userNameAccount);
            Map accountMap = gson.fromJson(accountData.getBody(), Map.class);
            List accountList = (List) accountMap.get("restBodyList");
            accountMap = (Map) accountList.get(0);
            String accountName = accountMap.get("FOA").toString() + " " + accountMap.get("FNameTH").toString() + " " + accountMap.get("LNameTH").toString();

            /** find Finance **/
            ResponseEntity<String> financeByPaPsa = employeeProfileService.findFinanceByPaPsaAndKeySearch(paPsa, employeeMap.get("User_name").toString());
            Map financeByPaPsaMap = gson.fromJson(financeByPaPsa.getBody(), Map.class);
            String userNameFinance = financeByPaPsaMap.get("profile").toString().split(":")[1].split("-")[0];
            ResponseEntity<String> financeData = employeeProfileService.findEmployeeProfileByKeySearch(userNameFinance);
            Map financeMap = gson.fromJson(financeData.getBody(), Map.class);
            List financeList = (List) financeMap.get("restBodyList");
            financeMap = (Map) financeList.get(0);
            String financeName = financeMap.get("FOA").toString() + " " + financeMap.get("FNameTH").toString() + " " + financeMap.get("LNameTH").toString();

            /** find Vertify **/
            ResponseEntity<String> vertifyData = employeeProfileService.findEmployeeProfileByKeySearch(jsonFromEmed.getString("approver1"));
            Map vertifyMap = gson.fromJson(vertifyData.getBody(), Map.class);
            List vertifyList = (List) vertifyMap.get("restBodyList");
            vertifyMap = (Map) vertifyList.get(0);
            String vertifyName = vertifyMap.get("FOA").toString() + " " + vertifyMap.get("FNameTH").toString() + " " + vertifyMap.get("LNameTH").toString();

            /** find Approver **/
            ResponseEntity<String> approverData = employeeProfileService.findEmployeeProfileByKeySearch(jsonFromEmed.getString("approver2"));
            Map approverMap = gson.fromJson(approverData.getBody(), Map.class);
            List approverList = (List) approverMap.get("restBodyList");
            approverMap = (Map) approverList.get(0);
            String approverName = approverMap.get("FOA").toString() + " " + approverMap.get("FNameTH").toString() + " " + approverMap.get("LNameTH").toString();

            expenseTypeByCompanyList = expenseTypeByCompanyRepository.findExpenseTypeByCompanyByPaAndPsaAndExpenseTypeCode(jsonFromEmed.getString("compCode"), jsonFromEmed.getString("location"), jsonFromEmed.getString("withdrawFlag"));

            /** Gen Document Number **/
            String prefix = "";
            String documentNumber = "";
            RunningType runningType = runningTypeRepository.findByCode(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_EXPENSE);
            if (runningType != null) {
                prefix = runningNumberRepository.generateRealRunningNumber(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_EXPENSE, jsonFromEmed.getString("compCode"), ConstantVariable.DOCUMENT_STATUS_REAL);
                documentNumber = runningNumberRepository.generateRunningNumber(runningType, prefix);
            }

            Document newDocument = new Document();
            newDocument.setDocNumber(documentNumber);
            newDocument.setDocumentType(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_TYPE_EXPENSE);
            newDocument.setDocumentStatus(ConstantVariable.MASTER_DATA_DETAIL_DOCUMENT_STATUS_ON_PROCESS);
            newDocument.setRequester(employeeMap.get("User_name").toString().toLowerCase());
            newDocument.setCreatedBy(employeeMap.get("User_name").toString().toLowerCase());
            newDocument.setTmpDocNumber(documentNumber);
            newDocument.setTotalAmount(jsonFromEmed.getDouble("amount"));
            newDocument.setDepartmentCode(employeeMap.get("Act_Org_ID").toString());
            newDocument.setCompanyCode(employeeMap.get("Personal_PA_ID").toString());
            newDocument.setPsa(employeeMap.get("Personal_PSA_ID").toString());
            newDocument.setCostCenterCode(jsonFromEmed.getString("costCenter"));
            newDocument.setPersonalId(jsonFromEmed.getString("empId"));
            newDocument.setSendDate(new Timestamp(new Date().getTime()));
            newDocument.setCreatedDate(new Timestamp(new Date().getTime()));
            newDocument.setExternalDocReference(jsonFromEmed.getString("docNumber"));
            newDocument.setExternalDocChanel(jsonFromEmed.getString("source"));
            newDocument.setRequesterName(employeeMap.get("FOA").toString() + " " + employeeMap.get("FNameTH").toString() + " " + employeeMap.get("LNameTH").toString());
            documentRepository.saveAndFlush(newDocument);

            DocumentExpense newDocumentExpense = new DocumentExpense();
            newDocumentExpense.setDocument(newDocument);
            entityManager.persist(newDocumentExpense);
            entityManager.flush();

            newDocument.setDocumentExpense(newDocumentExpense);

            DocumentExpenseItemDetail newDocumentExpenseItemDetail = new DocumentExpenseItemDetail();
            entityManager.persist(newDocumentExpenseItemDetail);
            entityManager.flush();

            DocumentExpenseItem newDocumentExpenseItem = new DocumentExpenseItem();
            newDocumentExpenseItem.setRemark(jsonFromEmed.getString("remark"));
            newDocumentExpenseItem.setInternalOrder(jsonFromEmed.getString("internalOrder"));
            newDocumentExpenseItem.setSeller("-");
            newDocumentExpenseItem.setTaxIDNumber("0000000000000");
            newDocumentExpenseItem.setBranch("00000");
            newDocumentExpenseItem.setAmount(jsonFromEmed.getDouble("amount"));
            newDocumentExpenseItem.setVatCode("VX");
            newDocumentExpenseItem.setNetAmount(jsonFromEmed.getDouble("amount"));
            newDocumentExpenseItem.setDocNumber("-");
            newDocumentExpenseItem.setDocumentDate(new Timestamp(new Date().getTime()));

            List<String> branchParameter = new ArrayList<String>();
            branchParameter.add("M019");

            MasterDataDetail masterDataDetail;
            try {
                masterDataDetail = masterDataDetailRepository.findByMasterdataInAndMasterDataDetailCodeOrderByCode(branchParameter, jsonFromEmed.getString("compCode")+"-"+jsonFromEmed.getString("location"));
                newDocumentExpenseItem.setBusinessPlace(masterDataDetail.getVariable3());
            } catch (Exception e) {
                newDocumentExpenseItem.setBusinessPlace("0000");
                e.printStackTrace();
            }

            newDocumentExpenseItem.setDocumentExp(newDocumentExpense);
            newDocumentExpenseItem.setVatCodeValue("0");
            newDocumentExpenseItem.setDocumentId(newDocument.getId());
            newDocumentExpenseItem.setDocExpItemDetail(newDocumentExpenseItemDetail);

            DocumentExpenseGroup newDocumentExpenseGroup = new DocumentExpenseGroup();
            newDocumentExpenseGroup.setDocumentExp(newDocumentExpense);

            if (null != expenseTypeByCompanyList && expenseTypeByCompanyList.size() > 0) {
                newDocumentExpenseItem.setExpTypeByCompany(expenseTypeByCompanyList.get(0));
                char charIndex4 = jsonFromEmed.getString("costCenter").charAt(3);
                String costCenter = jsonFromEmed.getString("costCenter");
                if ("9".equals("" + charIndex4) || "1017920010".equals(costCenter)) {
                    newDocumentExpenseItem.setGlCode(expenseTypeByCompanyList.get(0).getExpenseTypes().getHeadOfficeGL());
                } else {
                    newDocumentExpenseItem.setGlCode(expenseTypeByCompanyList.get(0).getExpenseTypes().getFactoryGL());
                }

                newDocument.setTitleDescription(expenseTypeByCompanyList.get(0).getExpenseTypes().getDescription());

                newDocumentExpenseGroup.setExpTypeByCompanys(expenseTypeByCompanyList.get(0));
            }

            documentExpenseItemRepository.saveAndFlush(newDocumentExpenseItem);
            documentExpenseGroupRepository.saveAndFlush(newDocumentExpenseGroup);

            Request newRequest = new Request();
            newRequest.setRequestStatusCode(ConstantVariable.MASTER_DATA_DETAIL_REQUEST_STATUS_CODE_CREATE);
            newRequest.setNextApprover(userNameAccount.toLowerCase());
            newRequest.setLastActionTime(new Timestamp(new Date().getTime()));
            newRequest.setRequestNumber("EMED");
            newRequest.setDocument(newDocument);

            requestRepository.saveAndFlush(newRequest);
            newDocument.setRequest(newRequest);

            RequestApprover requestApproverVertify = new RequestApprover();
            requestApproverVertify.setActionState(ConstantVariable.ACTION_STATE_VERIFY);
            requestApproverVertify.setRequestStatusCode(ConstantVariable.ACTION_STATE_VERIFY);
            requestApproverVertify.setActionStateName(ConstantVariable.JBPM_STATE_VERIFY);
            requestApproverVertify.setApprover(vertifyName);
            requestApproverVertify.setUserNameApprover(jsonFromEmed.getString("approver1"));
            requestApproverVertify.setRequest(newRequest);
            requestApproverVertify.setCreatedDate(new Timestamp(new Date().getTime()));
            requestApproverVertify.setActionTime(new Timestamp(dateFormat.parse(jsonFromEmed.getString("approverTime1")).getTime()));
            requestApproverVertify.setSequence(1);

            RequestApprover requestApproverApprover = new RequestApprover();
            requestApproverApprover.setActionState(ConstantVariable.ACTION_STATE_APPROVE);
            requestApproverApprover.setRequestStatusCode(ConstantVariable.ACTION_STATE_APPROVE);
            requestApproverApprover.setActionStateName(ConstantVariable.JBPM_STATE_APPROVE);
            requestApproverApprover.setApprover(approverName);
            requestApproverApprover.setUserNameApprover(jsonFromEmed.getString("approver2"));
            requestApproverApprover.setRequest(newRequest);
            requestApproverApprover.setCreatedDate(new Timestamp(new Date().getTime()));
            requestApproverApprover.setActionTime(new Timestamp(dateFormat.parse(jsonFromEmed.getString("approverTime2")).getTime()));
            requestApproverApprover.setSequence(2);

            RequestApprover requestApproverAccount = new RequestApprover();
            requestApproverAccount.setActionState(ConstantVariable.ACTION_STATE_ACCOUNT);
            requestApproverAccount.setActionStateName(ConstantVariable.JBPM_STATE_ACCOUNT);
            requestApproverAccount.setApprover(accountName);
            requestApproverAccount.setUserNameApprover(userNameAccount.toLowerCase());
            requestApproverAccount.setRequest(newRequest);
            requestApproverAccount.setCreatedDate(new Timestamp(new Date().getTime()));
            requestApproverAccount.setSequence(3);

            RequestApprover requestApproverFinance = new RequestApprover();
            requestApproverFinance.setActionState(ConstantVariable.ACTION_STATE_FINANCE);
            requestApproverFinance.setActionStateName(ConstantVariable.JBPM_STATE_FINANCE);
            requestApproverFinance.setApprover(financeName);
            requestApproverFinance.setUserNameApprover(userNameFinance.toLowerCase());
            requestApproverFinance.setRequest(newRequest);
            requestApproverFinance.setCreatedDate(new Timestamp(new Date().getTime()));
            requestApproverFinance.setSequence(4);

            RequestApprover requestApproverPaid = new RequestApprover();
            requestApproverPaid.setActionState(ConstantVariable.ACTION_STATE_PAID);
            requestApproverPaid.setActionStateName(ConstantVariable.ACTION_STATE_NAME_PAID);
            requestApproverPaid.setRequest(newRequest);
            requestApproverPaid.setCreatedDate(new Timestamp(new Date().getTime()));
            requestApproverPaid.setSequence(5);

            requestApproverRepository.saveAndFlush(requestApproverVertify);
            requestApproverRepository.saveAndFlush(requestApproverApprover);
            requestApproverRepository.saveAndFlush(requestApproverAccount);
            requestApproverRepository.saveAndFlush(requestApproverFinance);
            requestApproverRepository.saveAndFlush(requestApproverPaid);
            documentRepository.saveAndFlush(newDocument);

            /** set document attachment file **/
            JSONArray jsonArrayFile = jsonFromEmed.getJSONArray("fileList");

            if(jsonArrayFile.length() > 0){
                for (int i = 0; i < jsonArrayFile.length(); i++) {
                    JSONObject jsonObjectFile = jsonArrayFile.getJSONObject(i);
                    DocumentExpItemAttachment documentExpItemAttachment = new DocumentExpItemAttachment();
                    documentExpItemAttachment.setAttachmentCode("M151");
                    documentExpItemAttachment.setFileName(jsonObjectFile.getString("fileName"));
                    documentExpItemAttachment.setSequence(jsonObjectFile.getInt("seq"));
                    documentExpItemAttachment.setFlagEmed("Y");
                    documentExpItemAttachment.setDocumentExpenseItem(newDocumentExpenseItem);
                    documentExpItemAttachment.setDocEmed(jsonFromEmed.getString("docNumber"));
                    entityManager.persist(documentExpItemAttachment);
                }
            }else{
                LOGGER.error("File is null");
            }


            map.put("gwfDocNo", documentNumber);
            map.put("id", newDocument.getId().toString());
            map.put("account", accountName);
            map.put("finance", financeName);

            List<InterfaceActiveLog> interfaceActiveLogList = interfaceActiveLogRepository.findByDocNumberEquals(jsonFromEmed.getString("docNumber"));
            if(interfaceActiveLogList.size()>0){
                interfaceActiveLogList.get(0).setStatus("Complete");
                interfaceActiveLogRepository.saveAndFlush(interfaceActiveLogList.get(0));
            }

            return map;
        }catch (Exception e){

            List<InterfaceActiveLog> interfaceActiveLogList = interfaceActiveLogRepository.findByDocNumberEquals(jsonFromEmed.getString("docNumber"));
            if(interfaceActiveLogList.size()>0){
                interfaceActiveLogList.get(0).setStatus("Error");
                interfaceActiveLogRepository.saveAndFlush(interfaceActiveLogList.get(0));
            }
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }
}

class DucumentDTO {
	 
    private Double totalAmount;
 
    private String docNumber;
 
    public DucumentDTO(String docNumber,Double totalAmount) {
        this.totalAmount = totalAmount;
        this.docNumber = docNumber;
    }
 
    public Double getTotalAmount() {
        return totalAmount;
    }
 
    public String getDocNumber() {
        return docNumber;
    }
}
