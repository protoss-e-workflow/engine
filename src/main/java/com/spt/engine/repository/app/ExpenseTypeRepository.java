package com.spt.engine.repository.app;

import com.spt.engine.entity.app.ExpenseType;
import com.spt.engine.entity.app.ExpenseTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ExpenseTypeRepository extends JpaSpecificationExecutor<ExpenseType>, JpaRepository<ExpenseType, Long>, PagingAndSortingRepository<ExpenseType, Long> {

	ExpenseType findByCode(@Param("code") String code);




	Page<ExpenseType> findByDescriptionIgnoreCaseContaining(
			@Param("description") String description,
			Pageable pageable);






	@Query("select DISTINCT u from ExpenseType u left join u.expenseTypeReferences r   where u.id = :expenseType and r.completeReason = :completeReason  ")
	ExpenseType findExpenseTypeByIdAndExpenseTypeReferencesCompleteReason(@Param("completeReason") String completeReason,
											  @Param("expenseType") Long expenseType);


	@Query("select DISTINCT u from ExpenseType u left join u.expenseTypeFiles r   where u.id = :expenseType and r.attachmentTypeCode = :attachmentTypeCode  ")
	ExpenseType findExpenseTypeByIdAndExpenseTypeFileAttachmentCode(@Param("attachmentTypeCode") String attachmentTypeCode,
																		  @Param("expenseType") Long expenseType);



	@Query("select DISTINCT u from ExpenseType u left join u.expenseTypeScreens r   where u.id = :expenseType and r.structureField = :structureField  ")
	ExpenseType findExpenseTypeByExpenseTypeScreenStructureField(@Param("structureField") String structureField,
																 @Param("expenseType") Long expenseType);


	@Query("select DISTINCT u from ExpenseType u left join u.expTypeByCompanies r   where u.id = :expenseType and r.pa = :pa and r.psa = :psa and r.glCode = :glCode  ")
	ExpenseType findExpenseTypeByIdAndExpenseTypeCompanyPaAndExpenseTypeCompanyPsaAndExpenseTypeCompanyGlCode(@Param("pa") String pa,
																	                                          @Param("psa") String psa,
																	                                          @Param("glCode") String glCode,
																	                                          @Param("expenseType") Long expenseType
																											  );


	@Query("select DISTINCT u from ExpenseType u left join u.reimburses r   where u.id = :expenseType and r.roleCode = :roleCode  ")
	ExpenseType findExpenseTypeByIdAndReimburseRoleCode(@Param("roleCode") String roleCode,
																	@Param("expenseType") Long expenseType);



	@Query("select DISTINCT u from ExpenseType u order by u.code")
	List<ExpenseType> findAllExpenseType();



	ExpenseType findByFlowType(@Param("flowType")String flowType);




}
