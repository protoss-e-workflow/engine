package com.spt.engine.repository.app;
import com.spt.engine.entity.app.InternalOrder;
import com.spt.engine.repository.app.custom.InternalOrderRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InternalOrderRepository extends JpaSpecificationExecutor<InternalOrder>, JpaRepository<InternalOrder, Long>, PagingAndSortingRepository<InternalOrder, Long>, InternalOrderRepositoryCustom {

    InternalOrder findByIoCode(@Param("ioCode") String ioCode);


    List<InternalOrder> findByIoCodeAndCompanyCodeOrderByDateTimeDesc(@Param("ioCode") String ioCode, @Param("companyCode") String companyCode);

}
