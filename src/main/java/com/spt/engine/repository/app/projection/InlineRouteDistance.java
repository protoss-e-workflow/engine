package com.spt.engine.repository.app.projection;

import com.spt.engine.entity.app.Location;
import com.spt.engine.entity.app.RouteDistance;
import org.springframework.data.rest.core.config.Projection;

import java.sql.Timestamp;
import java.util.Set;

@Projection(name = "inlineRouteDistance",types = RouteDistance.class)
public interface InlineRouteDistance {

    Long getId();
    Long getVersion();
    String getCreatedBy();
    String getUpdateBy();
    Timestamp getCreatedDate();
    Timestamp getUpdateDate();
    Double getDistance();
    Boolean getFlagActive();

    Location getLocationFrom();

    Location getLocationTo();


}
