package com.spt.engine.repository.app;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.app.CostCenter;
import com.spt.engine.entity.app.Department;
import com.spt.engine.entity.general.RunningType;

public interface DepartmentRepository  extends JpaSpecificationExecutor<Department>, JpaRepository<Department, Long>, PagingAndSortingRepository<Department, Long> {

	Department findByCode(@Param("code") String code);
}
