package com.spt.engine.repository.app.custom;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.Location;
import com.spt.engine.entity.general.MasterData;
import com.spt.engine.entity.general.MasterDataDetail;
import com.spt.engine.entity.general.Parameter;
import com.spt.engine.entity.general.ParameterDetail;
import com.spt.engine.repository.app.LocationRepository;
import com.spt.engine.repository.general.MasterDataRepository;
import com.spt.engine.repository.general.ParameterRepository;

/**
 * 	Create by SiriradC. 2017.07.17
 *  Process Calculate PerDiem Implements
 */

public class PerDiemProcessCalculateRepositoryImpl implements PerDiemProcessCalculateRepositoryCustom{
	
	static final Logger LOGGER = LoggerFactory.getLogger(PerDiemProcessCalculateRepositoryImpl.class);
		
	@Autowired
	MasterDataRepository masterDataRepository;
	
	@Autowired
	ParameterRepository parameterRepository;
	
	@Autowired
	LocationRepository locationRepository;

	@Override
	public Map<String, Object> processCalculatePerDiemDomestic(Long empLevel, List<Map<String,Object>> data) {
		
		LOGGER.info("-=## METHOD PROCESS CALCULATE PERDIEM DOMESTIC ##=-");
		LOGGER.info("><>< empLevel : {} ",empLevel);
				
		Long levelFrom					= null;
		Long levelTo					= null;
		BigDecimal amountAllowance		= null;
		
		Map<String,Object> perDiemMap 	= new HashMap<>();
		MasterData perDiemMaster 		= new MasterData();
		MasterData allowanceMaster 		= new MasterData();
		
		List<Map<String,Object>> perDiemRateLs 			= new ArrayList<>();
		List<MasterDataDetail> perDiemRateDomesticSet 	= null;
		List<MasterDataDetail> allowanceMasterDetail 	= null;
		
		try {
						
			/* find perDiem by empLevel for calculate */
			perDiemMaster = masterDataRepository.findByCode(ConstantVariable.MASTER_DATA_PER_DIEM_DATA_CODE);
			perDiemRateDomesticSet = new ArrayList<>(perDiemMaster.getDetails());
			
			allowanceMaster = masterDataRepository.findByCode(ConstantVariable.MASTER_DATA_PER_DIEM_RISK_CODE);
			allowanceMasterDetail = new ArrayList<>(allowanceMaster.getDetails());
			if(empLevel != null && empLevel != 0l){
				if(perDiemRateDomesticSet.size() > 0 && perDiemRateDomesticSet != null){
					perDiemRateLs = getPerDemRateListByEmpLevel(empLevel,perDiemRateDomesticSet);
				}
				/* get amount allowance rate */
				for(MasterDataDetail obj : allowanceMasterDetail){
					levelFrom 	= Long.valueOf(obj.getVariable1());
					levelTo 	= Long.valueOf(obj.getVariable2());
					if(empLevel >= levelFrom && empLevel <= levelTo){
						amountAllowance = new BigDecimal(obj.getVariable3());
					}
				}
			}
			
			/* validate value */
			if(data.size() > 0 && data != null){
				/* case one day trip */
				if(data.size() == 1){					
					
					/* call method CALCULATE PER DIEM PROCESS */
					perDiemMap = calculatePerDiemMethod(calculateDifferenceDay(data), perDiemRateLs,amountAllowance);
				}else{
					perDiemMap = calculatePerDiemMethod(validateProcess(data), perDiemRateLs,amountAllowance);
				}				
			}			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return perDiemMap;
	}

	public List<Map<String,Object>> calculateDifferenceDay(List<Map<String,Object>> data){
		/* initial variable */
		Long diffInDays 	= 0l;
		Long diffHour 		= 0l;
		Long rateType		= 0l;
		String startDate	= null;
		String startTime	= null;
		String endDate		= null;
		String endTime		= null;
		String location	    = null;
		Map<String,Object> diff = null;
		Map<String,Object> perDiemRateMap = null;
		List<Map<String,Object>> perDiemRateLs = new ArrayList<>();
		try{
			for(Map<String,Object> map : data){
				startDate 	= String.valueOf(map.get(ConstantVariable.START_DATE_KEY));
				startTime 	= String.valueOf(map.get(ConstantVariable.START_TIME_KEY));
				endDate 	= String.valueOf(map.get(ConstantVariable.END_DATE_KEY));
				endTime 	= String.valueOf(map.get(ConstantVariable.END_TIME_KEY));
				rateType	= Long.valueOf(map.get(ConstantVariable.RATE_TYPE_KEY).toString());
				location 	= String.valueOf(map.get(ConstantVariable.LOCATION_KEY).toString());
				
				if(!startDate.equals("null") && startDate != null && !startDate.equals("") 
						&& !endDate.equals("null") && endDate != null && !endDate.equals("")){
					startDate = startDate.concat(" "+startTime);
					endDate	  = endDate.concat(" "+endTime);
					diff = convertDateToFormatAndValidateDate(startDate, endDate,null);
					diffInDays = Long.valueOf(diff.get("days").toString());
					diffHour   = Long.valueOf(diff.get("hours").toString());
					
				}
				perDiemRateMap = new HashMap<>();
				perDiemRateMap.put("days", diffInDays);
				perDiemRateMap.put("hours", diffHour);
				perDiemRateMap.put("rateType", rateType);
				perDiemRateMap.put("location", location);
				perDiemRateLs.add(perDiemRateMap);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return perDiemRateLs;
	}
	
	public Map<String, Object> calculatePerDiemMethod(List<Map<String,Object>> perDiemDataLs, List<Map<String,Object>> perDiemRates,BigDecimal amountAllowance){
		LOGGER.info("><>< CALCULATE PER DIEM METHOD ><><");
		LOGGER.info("><>< perDiemDataLs : {} ",perDiemDataLs);
		LOGGER.info("><>< perDiemRates : {} ",perDiemRates.size());
		
		Long days 		= null;
		Long rateType 	= null;
		Long hrFrom		= null;
		Long hrTo		= null;
		Float hours 	= null;
		String location = null;
		
		BigDecimal summary			= new BigDecimal(0);
		BigDecimal amountRisk 		= new BigDecimal(0);
		BigDecimal rateCal	 		= null;
		BigDecimal hoursCal	 		= null;
		BigDecimal perDiem	 		= new BigDecimal(0);
		BigDecimal calculate 		= new BigDecimal(0);
		Set<String> calculateLs		= new LinkedHashSet<>();
		Set<String> calculateLs1	= new LinkedHashSet<>();
		Map<String,Object> resultPerDiemMap 		= new HashMap<>();
		Map<Long,Set<String>> resultCalculateMap 	= new HashMap<>();
		
		/* case a day trip */
		if(perDiemDataLs.size() == 1){
			for(Map<String,Object> map : perDiemDataLs){
				days = Long.valueOf(map.get("days").toString());
				hours = Float.valueOf(map.get("hours").toString());
				rateType = Long.valueOf(map.get("rateType").toString());
				location = String.valueOf(map.get("location"));
				
				/* validate location for get amount location RISK */
				amountRisk = getAmountRiskByLocation(location);
								
				if(days == 0 || (days == 1 && hours == 0)){
					
					/* get perDiem amount by hours */
					perDiem = getAmountPerDiemByHours(perDiemRates,days , hours,perDiemDataLs.size());
					/* calculate one day solution = perDiem * rateType * (hoursUse / hoursPerDay) */ 
					calculate = perDiem.multiply(new BigDecimal(rateType).divide(new BigDecimal(ConstantVariable.RATE_100_PERCENT))).
							multiply(new BigDecimal(ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN).divide(new BigDecimal(ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN)))
							.add(amountRisk);
					calculateLs.add(calculate.toString());
					resultCalculateMap.put(days, calculateLs);
				}else{
					LOGGER.info("><><>< IN CASE SERVERALS DAYS ><><><");
					resultCalculateMap = calculatePerDiemForCaseSeveralsDays(days,hours,rateType,location,perDiemRates);
				}
			}
		}
		else{
			for(Map<String,Object> map : perDiemDataLs){
				days = Long.valueOf(map.get("days").toString());
				hours = Float.valueOf(map.get("hours").toString());
				rateType = Long.valueOf(map.get("rateType").toString());
				location = String.valueOf(map.get("location"));
				
				/* validate location for get amount location RISK */
				amountRisk = getAmountRiskByLocation(location);
				
				if(days == 0 || (days == 1 && hours == 0)){
					/* get perDiem amount by hours */
					perDiem = getAmountPerDiemByHours(perDiemRates,days , hours, perDiemDataLs.size());
					
					/* calculate one day solution = perDiem * rateType * (hoursUse / hoursPerDay) */ 
					hoursCal = new BigDecimal(hours / ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN);
					rateCal  = new BigDecimal(rateType).divide(new BigDecimal(ConstantVariable.RATE_100_PERCENT));
					calculate = perDiem.multiply(rateCal).multiply(hoursCal).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING, RoundingMode.CEILING)
							.add(amountRisk);
					calculateLs.add(calculate.toString());
					resultCalculateMap.put(days, calculateLs);
										
				}else{
					/* get perDiem amount by hours */
					perDiem = getAmountPerDiemByHours(perDiemRates,days, hours, perDiemDataLs.size());
					
					/* calculate one day solution = perDiem * rateType * (hoursUse / hoursPerDay) */ 
					hoursCal = new BigDecimal(ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN / ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN);
					rateCal  = new BigDecimal(rateType).divide(new BigDecimal(ConstantVariable.RATE_100_PERCENT));
					calculate = perDiem.multiply(rateCal).multiply(hoursCal).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING, RoundingMode.CEILING)
							.add(amountRisk);;
					calculateLs1.add(calculate.toString());
					resultCalculateMap.put(days, calculateLs1);
				}
			}		
		}
		resultPerDiemMap = calculateNonTax(resultCalculateMap);
		
		/* validate include allowance */
		if(days >= 30 && amountRisk.compareTo(new BigDecimal(0)) == 1){
			summary = new BigDecimal(resultPerDiemMap.get(ConstantVariable.SUMMARY_KEY).toString());
			summary = summary.add(amountAllowance);
			resultPerDiemMap.put(ConstantVariable.SUMMARY_KEY,summary);
		}
		return resultPerDiemMap;
	}

	public List<Map<String,Object>> getPerDemRateListByEmpLevel(Long empLevel,List<MasterDataDetail> perDiemRateDomesticMaster){
		Map<String,Object> perDiemRateMap = null;
		List<Map<String,Object>> perDiemRateLs = new ArrayList<>();
		for(MasterDataDetail ms : perDiemRateDomesticMaster){
			if(empLevel >= Long.valueOf(ms.getVariable1()) && empLevel <= Long.valueOf(ms.getVariable2()) ){
				perDiemRateMap = new HashMap<>();
				perDiemRateMap.put("hoursFrom", ms.getVariable3());
				perDiemRateMap.put("hoursTo", ms.getVariable4());
				perDiemRateMap.put("amount", ms.getVariable5());
				perDiemRateLs.add(perDiemRateMap);
			}
		}
		
		return perDiemRateLs;
	}
	
	public List<Map<String,Object>> validateProcess(List<Map<String,Object>> data){
		
		/* initial data */
		String startDate	= null;
		String location		= null;
		String startTime	= null;
		String startTime1	= null;
		String endDate		= null;
		String endTime		= null;
		String endTime1		= null;
		String rateType		= null;
		Map<String,Object> mapData 					= null;
		Map<String,Object>	diff					= new HashMap<>();
		Set<Map<String,Object>> setData				= new LinkedHashSet<>();
		List<Map<String,Object>> perDiemDataLs 		= null;
		
		for(Map<String,Object> map : data){
			startDate 	= String.valueOf(map.get(ConstantVariable.START_DATE_KEY));
			endDate 	= String.valueOf(map.get(ConstantVariable.END_DATE_KEY));
			if(startDate.equals(endDate)){
				location 	= String.valueOf(map.get(ConstantVariable.LOCATION_KEY));
				startTime 	= String.valueOf(map.get(ConstantVariable.START_TIME_KEY));
				endTime 	= String.valueOf(map.get(ConstantVariable.END_TIME_KEY));
				rateType 	= String.valueOf(map.get(ConstantVariable.RATE_TYPE_KEY));
				diff = convertDateToFormatAndValidateDate(startDate.concat(" "+startTime),startDate.concat(" "+endTime),null);
				mapData = new HashMap<>();
				mapData.put(ConstantVariable.LOCATION_KEY, location);
				mapData.put(ConstantVariable.DAYS_KEY,String.valueOf(diff.get(ConstantVariable.DAYS_KEY)));
				mapData.put(ConstantVariable.HOURS_KEY,String.valueOf(diff.get(ConstantVariable.HOURS_KEY)));
				mapData.put(ConstantVariable.RATE_TYPE_KEY,rateType);
				setData.add(mapData);
			}else if(!startDate.equals(endDate)){
				location 	= String.valueOf(map.get(ConstantVariable.LOCATION_KEY));
				startTime1 	= String.valueOf(map.get(ConstantVariable.START_TIME_KEY));
				endTime1 	= String.valueOf(map.get(ConstantVariable.END_TIME_KEY));
				rateType 	= String.valueOf(map.get(ConstantVariable.RATE_TYPE_KEY));
				diff = convertDateToFormatAndValidateDate(startDate.concat(" "+endTime),startDate.concat(" "+ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_FROM_HOUR),startDate.concat(" "+startTime));
				mapData = new HashMap<>();
				mapData.put(ConstantVariable.LOCATION_KEY, location);
				mapData.put(ConstantVariable.DAYS_KEY,String.valueOf(diff.get(ConstantVariable.DAYS_KEY)));
				mapData.put(ConstantVariable.HOURS_KEY,String.valueOf(diff.get(ConstantVariable.HOURS_KEY)));
				mapData.put(ConstantVariable.RATE_TYPE_KEY,rateType);
				setData.add(mapData);
			}
		}
		
		for(Map<String,Object> map : data){
			endDate 	= String.valueOf(map.get(ConstantVariable.END_DATE_KEY));
			if(!endDate.equals(startDate)){
				location 	= String.valueOf(map.get(ConstantVariable.LOCATION_KEY));
				startTime1	= String.valueOf(map.get(ConstantVariable.START_TIME_KEY));
				endTime1 	= String.valueOf(map.get(ConstantVariable.END_TIME_KEY));
				rateType 	= String.valueOf(map.get(ConstantVariable.RATE_TYPE_KEY));
				diff = convertDateToFormatAndValidateDate(startDate.concat(" "+startTime),endDate.concat(" "+endTime1),null);
				mapData = new HashMap<>();
				mapData.put(ConstantVariable.LOCATION_KEY, location);
				mapData.put(ConstantVariable.DAYS_KEY,String.valueOf(diff.get(ConstantVariable.DAYS_KEY)));
				mapData.put(ConstantVariable.HOURS_KEY,String.valueOf(diff.get(ConstantVariable.HOURS_KEY)));
				mapData.put(ConstantVariable.RATE_TYPE_KEY,rateType);
				setData.add(mapData);
			}
		}
		
		LOGGER.info("><>< FIRST DATE  : {} ==> DATA : {} ><><>< SIZE : {} ",startDate,setData,setData.size());

		perDiemDataLs = new ArrayList<>(setData);
		
		return perDiemDataLs;
	}
	 
	public Map<String,Object>  convertDateToFormatAndValidateDate(String startDate,String endDate,String shiftDate){
		LOGGER.info("=============== CONVERT DATE TO FORMAT AND VALIDATE DATE ==============");
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date newerDate 		= null;
		Date olderDate 		= null;
		Date shiftDateTime	= null;
		Long diff 			= 0l;
		Long diffInDays 	= 0l;
		Long diffHour 		= 0l;
		Map<String,Object> result 		= new HashMap<>();
		try{
			newerDate = formatter.parse(startDate);
			olderDate = formatter.parse(endDate);
					
			diff 			= (olderDate.getTime() - newerDate.getTime());
			diffInDays 		= diff/ ConstantVariable.CALCULATE_DIFFERENCE_DAY;
			diffHour		= diff/ ConstantVariable.CALCULATE_DIFFERENCE_HOURS % ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN;
			
			/* validate minute over 30 or less 30 */
			if(newerDate.getMinutes() >= 30){
				diffHour = diffHour + 1;
			}
			if(olderDate.getMinutes() >= 30){
				diffHour = diffHour + 1;
			}
			
			if(shiftDate != null && !shiftDate.equals("null")){
				shiftDateTime = formatter.parse(shiftDate);
				diffHour = diffHour + shiftDateTime.getHours();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		result.put(ConstantVariable.DAYS_KEY, diffInDays);
		result.put(ConstantVariable.HOURS_KEY, diffHour);
		
		return result;
	}

	public Map<String,Object> calculateNonTax(Map<Long,Set<String>> calculateLs){
		LOGGER.info("=============== CALCULATE NONTAX ==============");
		BigDecimal incomeAmount = new BigDecimal(ConstantVariable.NON_TAX_AMOUNT);
		BigDecimal nonTax		= new BigDecimal(0);
		BigDecimal data			= new BigDecimal(0);
		BigDecimal tax			= new BigDecimal(0);
		BigDecimal calNonTax	= new BigDecimal(0);
		BigDecimal calTax		= new BigDecimal(0);
		BigDecimal summary		= new BigDecimal(0);
		
		List<String> dataLs 	= new ArrayList<>();
		Map<String,Object> perDiemResult = new HashMap<>();
		
		/* find range income amount from parameter */
		List<ParameterDetail> parameterDetailLs =  new ArrayList<>(parameterRepository.findByCode(ConstantVariable.PARAMETER_RATE_INCOME).getDetails());
		if(parameterDetailLs.size() > 0 && parameterDetailLs != null){
			incomeAmount = new BigDecimal(parameterDetailLs.get(0).getVariable1());
		}
		
		/* validate calculate comepareTo income amount */
		for(Map.Entry<Long,Set<String>> map : calculateLs.entrySet()){
			dataLs = new ArrayList<>(map.getValue());
			if(dataLs.size() > 1){
				for(int i=0;i<dataLs.size();i++){
					data = data.add(new BigDecimal(dataLs.get(i)));
				}
			}else{
				data = new BigDecimal(dataLs.get(0));
			}
			
			if(data.compareTo(incomeAmount) == 1){
				tax = data.subtract(incomeAmount); 
				nonTax = incomeAmount;
			}else{
				nonTax = data;
			}
			calNonTax = calNonTax.add(nonTax);
			calTax	  = calTax.add(tax);
			tax			= new BigDecimal(0);
		}

		summary = calNonTax.add(calTax);
		perDiemResult.put(ConstantVariable.NON_TAX_KEY, calNonTax.setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING, RoundingMode.CEILING));
		perDiemResult.put(ConstantVariable.TAX_KEY, calTax.setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING, RoundingMode.CEILING));
		perDiemResult.put(ConstantVariable.SUMMARY_KEY, summary.setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING, RoundingMode.CEILING));
		
		return perDiemResult;
	}

	public BigDecimal getAmountPerDiemByHours(List<Map<String,Object>> perDiemRates,Long days,Float hours,Integer dataSize){
		Long hrFrom		= null;
		Long hrTo		= null;
		BigDecimal perDiem	 = new BigDecimal(0);
		
		if(dataSize == 1){
			for(Map<String,Object> rateMap : perDiemRates){
				hrFrom = Long.valueOf(rateMap.get("hoursFrom").toString());
				hrTo = Long.valueOf(rateMap.get("hoursTo").toString());
				if((hours == 0 ? ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN : hours) >= hrFrom && (hours == 0 ? ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN : hours) <= hrTo ){
					 perDiem = new BigDecimal(rateMap.get("amount").toString());
				}
			}
		}else if(days == 0 || (days == 1 && hours == 0)){
			for(Map<String,Object> rateMap : perDiemRates){
				hrFrom = Long.valueOf(rateMap.get("hoursFrom").toString());
				hrTo = Long.valueOf(rateMap.get("hoursTo").toString());
				if(ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN <= hrTo ){
					 perDiem = new BigDecimal(rateMap.get("amount").toString());
				}
			}
		}else{
			for(Map<String,Object> rateMap : perDiemRates){
				hrFrom = Long.valueOf(rateMap.get("hoursFrom").toString());
				hrTo = Long.valueOf(rateMap.get("hoursTo").toString());
				if((hours == 0 ? ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN : hours) >= hrFrom && (hours == 0 ? ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN : hours) <= hrTo ){
					 perDiem = new BigDecimal(rateMap.get("amount").toString());
				}
			}
		}
		return perDiem;
	}

	public Map<Long,Set<String>> calculatePerDiemForCaseSeveralsDays(Long days,Float hours,Long rateType, String location, List<Map<String,Object>> perDiemRates){
		LOGGER.info("=========== CALCULATE PERDIEM FOR CASE SEVERALS DAY ============");
				
		Map<Long,Set<String>> resultCalculateMap = new HashMap<>();
		Integer index 		 = null;
		BigDecimal amountRisk		= new BigDecimal(0);
		BigDecimal calculate 		= new BigDecimal(0);
		Set<String> calculateLs		= new LinkedHashSet<>();
		
		
		/* loop calculate for 1-4 days */
		for(int i=0; i<=days;i++){
			
			index = i;
			
			/* validate location are case RISK LOCATION */
			amountRisk = getAmountRiskByLocation(location);
			calculate  = calculateAmountForCaseSeveralsDays(index, new BigDecimal(days), hours, rateType, perDiemRates).add(amountRisk); 
			calculateLs = new LinkedHashSet<>();
			calculateLs.add(calculate.toString());
			resultCalculateMap.put(Long.valueOf(index), calculateLs);			

		}
				
		return resultCalculateMap;
	}

	public BigDecimal getAmountRiskByLocation(String location){
		LOGGER.info("========== GET AMOUNT RISK BY LOCATION ========== ");
		LOGGER.info("><><>< location : {} ",location);
		
		BigDecimal amount	 = new BigDecimal(0);
		
		/* get location by name find special rate */
		Location locationData = locationRepository.findByCode(location);
		
		/* refactor by iMilkii 2017.09.08
		MasterData locationMaster = masterDataRepository.findByCode(ConstantVariableUtil.LOCATION_RISK_CODE);
		
		List<MasterDataDetail> locationData = new ArrayList<>(locationMaster.getDetails());
		
		for(MasterDataDetail locationObj : locationData){
			if(locationObj.getVariable1().equals(location)){
				amount = new BigDecimal(locationObj.getVariable2().toString());
			}
		}*/
		
		if(locationData != null && locationData.getSpeacialRate() != null){
			amount = new BigDecimal(locationData.getSpeacialRate());
		}
		
		return amount;
	}
	
	public BigDecimal calculateAmountForCaseSeveralsDays(Integer index,BigDecimal days,Float hours,Long rateType, List<Map<String,Object>> perDiemRates){
		LOGGER.info("-= METHOD CALCULATE AMOUNT FOR CASE SEVERALS DAYS =-");
		BigDecimal amount 			= new BigDecimal(0);
		BigDecimal amountLongTerm	= new BigDecimal(0);
		BigDecimal perDiem	 		= new BigDecimal(0);
		BigDecimal rateCal	 		= null;
		BigDecimal hoursCal	 		= null;
		
		Parameter paramAmount		= parameterRepository.findByCode(ConstantVariable.PARAMETER_LONG_TERM_AMOUNT);
		List<ParameterDetail> paramAmountDetail = new ArrayList<>(paramAmount.getDetails());
		
		if(paramAmountDetail.size() > 0){
			amountLongTerm = new BigDecimal(paramAmountDetail.get(0).getVariable1());
		}
		
		if(new BigDecimal(index).compareTo(days) != 0  && index >= 4 ){
			perDiem = getAmountPerDiemByHours(perDiemRates,0l , hours, 2);
			
			/* calculate one day solution = perDiem * rateType * (hoursUse / hoursPerDay) */ 
			hoursCal = new BigDecimal(ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN / ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN);
			rateCal  = new BigDecimal(rateType).divide(new BigDecimal(ConstantVariable.RATE_100_PERCENT));
			amount 	 = perDiem.multiply(rateCal).multiply(hoursCal).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING, RoundingMode.CEILING)
					.add(amountLongTerm);			
		}else if(new BigDecimal(index).compareTo(days) != 0 && index < 4){
			perDiem = getAmountPerDiemByHours(perDiemRates,0l , hours, 2);
			
			/* calculate one day solution = perDiem * rateType * (hoursUse / hoursPerDay) */ 
			hoursCal = new BigDecimal(ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN / ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN);
			rateCal  = new BigDecimal(rateType).divide(new BigDecimal(ConstantVariable.RATE_100_PERCENT));
			amount 	 = perDiem.multiply(rateCal).multiply(hoursCal).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING, RoundingMode.CEILING);		
		}else if(new BigDecimal(index).compareTo(days) == 0 && index >= 4){
			perDiem = getAmountPerDiemByHours(perDiemRates,days.longValue() , hours, 1);
			/* calculate one day solution = perDiem * rateType * (hoursUse / hoursPerDay) */ 
			hoursCal = new BigDecimal(ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN / ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN);
			rateCal  = new BigDecimal(rateType).divide(new BigDecimal(ConstantVariable.RATE_100_PERCENT));
			amount 	 = perDiem.multiply(rateCal).multiply(hoursCal).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING, RoundingMode.CEILING);	
			
			if(hours > 0 && hours >= 8){
				amount = amount.add(amountLongTerm);
			}
		}else{
			perDiem = getAmountPerDiemByHours(perDiemRates,days.longValue() , hours, 1);
			/* calculate one day solution = perDiem * rateType * (hoursUse / hoursPerDay) */ 
			hoursCal = new BigDecimal(ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN / ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN);
			rateCal  = new BigDecimal(rateType).divide(new BigDecimal(ConstantVariable.RATE_100_PERCENT));
			amount 	 = perDiem.multiply(rateCal).multiply(hoursCal).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING, RoundingMode.CEILING);	
		}
		
		return amount;
	}

	@Override
	public Map<String, Object> processCalculatePerDiemForeign(Long empLevel, BigDecimal sellingRate, List<Map<String, Object>> data) {
		LOGGER.info("-=## METHOD PROCESS CALCULATE PERDIEM FOREIGN ##=-");
		LOGGER.info("><><>< empLevel : {} ",empLevel);
		LOGGER.info("><><>< data : {} ",data);
		
		String zoneStatus 	= null;
		String startDate 	= null;
		String endDate		= null;
		String startTime	= null;
		String endTime		= null;
		String rateType		= null;
		String location		= null;
		BigDecimal perDiemAmount = new BigDecimal(0);
		Map<String,Object> perDiemMap = new HashMap<>();
		Map<String,Object> validateDateMap	= new HashMap<>();
		Map<String,Object> diffDateMap 		= new HashMap<>();
		Set<Map<String,Object>> setData		= new LinkedHashSet<>();
		 
		try{
			
			/* verify hours for 24 hours */
			for(Map<String,Object> map : data){
				startDate = String.valueOf(map.get(ConstantVariable.START_DATE_KEY));
				endDate   = String.valueOf(map.get(ConstantVariable.END_DATE_KEY));
				startTime = String.valueOf(map.get(ConstantVariable.START_TIME_KEY));
				endTime   = String.valueOf(map.get(ConstantVariable.END_TIME_KEY));
				rateType  = String.valueOf(map.get(ConstantVariable.RATE_TYPE_KEY));
				location  = String.valueOf(map.get(ConstantVariable.LOCATION_KEY));

				perDiemAmount = getAmountRateForCaseForeign(location, empLevel);
				
				validateDateMap = convertDateToFormatAndValidateDateForeign(startDate.concat(" "+startTime),endDate.concat(" "+endTime),null);
				diffDateMap = new HashMap<>();
				diffDateMap.put(ConstantVariable.START_DATE_KEY, startDate);
				diffDateMap.put(ConstantVariable.DAYS_KEY, validateDateMap.get(ConstantVariable.DAYS_KEY));
				diffDateMap.put(ConstantVariable.HOURS_KEY, validateDateMap.get(ConstantVariable.HOURS_KEY));
				diffDateMap.put(ConstantVariable.MINUTE_KEY, validateDateMap.get(ConstantVariable.MINUTE_KEY));
				diffDateMap.put(ConstantVariable.RATE_TYPE_KEY, rateType);
				diffDateMap.put(ConstantVariable.AMOUNT_KEY, perDiemAmount);
				setData.add(diffDateMap);
			}
			
			perDiemMap = calculatePerDiemForeignRate(setData,sellingRate);
			
			LOGGER.info("><>< perDiemAmount : {} ",perDiemAmount);
			LOGGER.info("><>< perDiemMap : {} ",perDiemMap);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return perDiemMap;
	}
	
	public Map<String,Object> convertDateToFormatAndValidateDateForeign(String startDate,String endDate,String shiftDate){
		LOGGER.info("=============== CONVERT DATE TO FORMAT AND VALIDATE DATE ==============");
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date newerDate 		= null;
		Date olderDate 		= null;
		Date shiftDateTime	= null;
		Long diff 			= 0l;
		Long diffInDays 	= 0l;
		Long diffHour 		= 0l;
		Long diffMinute		= 0l;
		Map<String,Object> result 		= new HashMap<>();
		try{
			newerDate = formatter.parse(startDate);
			olderDate = formatter.parse(endDate);
					
			diff 			= (olderDate.getTime() - newerDate.getTime());
			diffInDays 		= diff/ ConstantVariable.CALCULATE_DIFFERENCE_DAY;
			diffHour		= diff/ ConstantVariable.CALCULATE_DIFFERENCE_HOURS % ConstantVariable.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN;
			diffMinute		= diff/ ConstantVariable.CALCULATE_DIFFERENCE_MINUTE  % ConstantVariable.TIME_SECOND_PER_HOURS;
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		result.put(ConstantVariable.DAYS_KEY, diffInDays);
		result.put(ConstantVariable.HOURS_KEY, diffHour);
		result.put(ConstantVariable.MINUTE_KEY, diffMinute);
		
		return result;
	}

	public Map<String,Object> calculatePerDiemForeignRate(Set<Map<String,Object>> setData,BigDecimal sellingRate){
		LOGGER.info("-= CALCULATE PERDIEM FOREIGN RATE =-");
		LOGGER.info("><><>< setData : {} ",setData);
		
		BigDecimal diffDays 		= null;
		BigDecimal diffHours 		= null;
		BigDecimal diffMinute 		= null;
		BigDecimal hours 			= null;
		BigDecimal rateType			= null;
		BigDecimal amountRate		= null;
		BigDecimal ratePercent		= new BigDecimal(ConstantVariable.RATE_100_PERCENT);
		BigDecimal minuteConvert	= new BigDecimal(ConstantVariable.TIME_MINUTE_PER_HOURS);
		BigDecimal hoursToMinute	= new BigDecimal(ConstantVariable.CALCULATE_CONVERT_HOURS_TO_MINUTE);
		BigDecimal hoursCal			= new BigDecimal(0);
		BigDecimal calculate		= new BigDecimal(0);
		BigDecimal calDays			= new BigDecimal(0);
		BigDecimal calUSD			= new BigDecimal(0);
		BigDecimal calBaht			= new BigDecimal(0);
		
		Map<String,Object> perDiemAmount	= new HashMap<>();
		
		List<Map<String,Object>> dataLs = new ArrayList<>(setData);
				
		for(Map<String,Object> map : dataLs){
			calDays		= new BigDecimal(0);
			diffDays 	= new BigDecimal((map.get(ConstantVariable.DAYS_KEY).toString()));
			diffHours 	= new BigDecimal((map.get(ConstantVariable.HOURS_KEY).toString()));
			diffMinute 	= new BigDecimal((map.get(ConstantVariable.MINUTE_KEY).toString()));
			rateType 	= new BigDecimal((map.get(ConstantVariable.RATE_TYPE_KEY).toString()));
			amountRate	= new BigDecimal((map.get(ConstantVariable.AMOUNT_KEY).toString()));
			
			/* convert hours to minute */
			if(diffDays.compareTo(new BigDecimal(1)) == 0 && diffHours.compareTo(new BigDecimal(0)) == 0){
				LOGGER.info("><><>< CASE DIFF DAY == 0 && DIFF HOURS == 0 ><><>< ");
				hours = hoursToMinute.divide(hoursToMinute).setScale(ConstantVariable.SCALE_4_DIGIT_ROUDING, RoundingMode.CEILING);
				calculate = amountRate.multiply((rateType.divide(ratePercent))).multiply(hours)
						.setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING, RoundingMode.CEILING);
				calUSD = calUSD.add(calculate);
			}else if(diffDays.compareTo(new BigDecimal(1)) == 1){
				LOGGER.info("><><>< CASE DIFF DATE MORE THAN 1 ><><><");
				hours = hoursToMinute.divide(hoursToMinute).setScale(ConstantVariable.SCALE_4_DIGIT_ROUDING, RoundingMode.CEILING);
				calculate = amountRate.multiply((rateType.divide(ratePercent))).multiply(hours)
						.setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING, RoundingMode.CEILING);
				calDays = calDays.add(calculate).multiply(diffDays);
				calUSD = calUSD.add(calDays);
				
				if(diffHours.compareTo(new BigDecimal(0)) == 1){
					LOGGER.info("><><>< CASE HOURS NOT EQUALS 24 ><><><");
					hours = diffHours.multiply(minuteConvert);
					hours = hours.add(diffMinute);
					hoursCal = hours.divide(hoursToMinute,ConstantVariable.SCALE_4_DIGIT_ROUDING, RoundingMode.HALF_EVEN);
					calculate = amountRate.multiply((rateType.divide(ratePercent))).multiply(hoursCal);
					calUSD = calUSD.add(calculate).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING, BigDecimal.ROUND_HALF_EVEN);
				}
			}else{
				LOGGER.info("><><>< CASE HOURS NOT EQUALS 24 ><><><");
				hours = diffHours.multiply(minuteConvert);
				hours = hours.add(diffMinute);
				hoursCal = hours.divide(hoursToMinute,ConstantVariable.SCALE_4_DIGIT_ROUDING, RoundingMode.HALF_EVEN);
				calculate = amountRate.multiply((rateType.divide(ratePercent))).multiply(hoursCal);
				calUSD = calUSD.add(calculate).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING, BigDecimal.ROUND_HALF_EVEN);
			}
		}
		
		/* calculate USD amount convert to BAHT amount*/
		calBaht = calUSD.multiply(sellingRate).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING, RoundingMode.CEILING);
		
		perDiemAmount.put(ConstantVariable.USD_KEY, calUSD.setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING, BigDecimal.ROUND_HALF_EVEN));
		perDiemAmount.put(ConstantVariable.BAHT_KEY, calBaht.setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING, BigDecimal.ROUND_HALF_EVEN));
		
		return perDiemAmount;
	}
	
	public BigDecimal getAmountRateForCaseForeign(String location,Long empLevel){
		LOGGER.info("-= METHOD GET AMOUNT RATE FOR CASE FOREIGN =-");
		LOGGER.info("><><>< location : {} ",location);
		LOGGER.info("><><>< empLevel : {} ",empLevel);
		String zoneStatus 	= null;
		BigDecimal perDiemAmount = new BigDecimal(0);
		
		/*
		MasterData locationForeign = masterDataRepository.findByCode(ConstantVariableUtil.LOCATION_FOREIGN_CODE);
		List<MasterDataDetail> locationForeignDataLs = new ArrayList<>(locationForeign.getDetails());
		
		for(MasterDataDetail locationObj : locationForeignDataLs){
			if(locationObj.getVariable1().equals(location)){
				zoneStatus = locationObj.getVariable2();
			}
		}
		*/
		
		/* refactor code location get zone code */
		Location locationData = locationRepository.findByCode(location);
		if(locationData != null && locationData.getZoneCode() != null){
			zoneStatus = locationData.getZoneCode();
		}
		
		/* get perDiem rate location foreign by empLevel and location */
		MasterData perDiemRateForeign = masterDataRepository.findByCode(ConstantVariable.MASTER_DATA_PER_DIEM_FOREIGN_CODE);
		List<MasterDataDetail> perDiemRateForeignDetailLs = new ArrayList<>(perDiemRateForeign.getDetails());
		for(MasterDataDetail ms : perDiemRateForeignDetailLs){
			if(empLevel >= Long.valueOf(ms.getVariable1()) && empLevel <= Long.valueOf(ms.getVariable2()) 
					&& zoneStatus.equals(ms.getVariable4())){
				perDiemAmount = new BigDecimal(ms.getVariable3());
			}
		}
		
		return perDiemAmount;
	}
}
