package com.spt.engine.repository.app;

import com.spt.engine.entity.app.ExpenseTypeByCompany;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ExpenseTypeByCompanyRepository extends JpaSpecificationExecutor<ExpenseTypeByCompany>, JpaRepository<ExpenseTypeByCompany, Long>, PagingAndSortingRepository<ExpenseTypeByCompany, Long> {

    List<ExpenseTypeByCompany> findByCompanyCode(@Param("psaCode") String code);

    ExpenseTypeByCompany findByPaAndPsaAndGlCode(@Param("pa") String pa,
                                                 @Param("psa") String psa,
                                                 @Param("glCode") String glCode);

    @Query("select DISTINCT u from ExpenseTypeByCompany u  where  u.expenseType.id = :expenseType")
    Page<ExpenseTypeByCompany> findExpenseTypeByCompanyByExpenseTypeId(
            @Param("expenseType") Long expenseType,
            Pageable pageable);

    @Query("select DISTINCT u from ExpenseTypeByCompany u " +
            "left join u.expenseType e " +
            "left join e.reimburses r " +
            "where  u.pa = :pa and u.psa = :psa  and (lower(e.description) like CONCAT('%',lower(:keySearch),'%') or (lower(e.engDescription) like CONCAT('%',lower(:keySearch),'%')) or lower(e.headOfficeGL) like CONCAT('%',lower(:headOfficeGL),'%') or lower(e.factoryGL) like CONCAT('%',lower(:factoryGL),'%') ) " +
            "and r.roleCode in :listRole order by u.id")
    List<ExpenseTypeByCompany> findExpenseTypeByCompanyByPaAndPsaAndKeySearchAndHeadOfficeGLAndFactoryGLAndRole(@Param("pa") String pa, @Param("psa") String psa, @Param("keySearch") String keySearch, @Param("headOfficeGL") String headOfficeGL, @Param("factoryGL") String factoryGL,
                                                                                                                @Param("listRole") List<String> listRole, Pageable pageable);

    @Query("select DISTINCT u from ExpenseTypeByCompany u " +
            "left join u.expenseType e " +
            "left join e.reimburses r " +
            "where  u.pa = :pa and u.psa = :psa and e.favorite = true " +
            "and r.roleCode in :listRole order by u.id")
    List<ExpenseTypeByCompany> findExpenseTypeByCompanyByPaAndPsaAndFavoriteAndRole(@Param("pa") String pa,@Param("psa") String psa,@Param("listRole") List<String> listRole,Pageable pageable);

    @Query("select DISTINCT u from ExpenseTypeByCompany u " +
            "left join u.expenseType e " +
            "where  u.pa = :pa and u.psa = :psa and e.code = :expenseTypeCode")
    List<ExpenseTypeByCompany> findExpenseTypeByCompanyByPaAndPsaAndExpenseTypeCode(@Param("pa") String pa,@Param("psa") String psa,@Param("expenseTypeCode") String expenseTypeCode);
}
