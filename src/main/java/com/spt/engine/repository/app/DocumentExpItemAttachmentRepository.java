package com.spt.engine.repository.app;

import com.spt.engine.entity.app.DocumentExpItemAttachment;
import com.spt.engine.entity.app.DocumentExpenseItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DocumentExpItemAttachmentRepository extends JpaSpecificationExecutor<DocumentExpItemAttachment>, JpaRepository<DocumentExpItemAttachment, Long>, PagingAndSortingRepository<DocumentExpItemAttachment, Long> {


}
