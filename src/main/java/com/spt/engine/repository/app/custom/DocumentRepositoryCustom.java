package com.spt.engine.repository.app.custom;

import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentAdvance;
import com.spt.engine.entity.app.DocumentExpenseItem;

import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;

import javax.print.Doc;
import java.util.List;
import java.util.Map;

public interface DocumentRepositoryCustom {

    List<Document> findByDocumentTypeAndDocumentStatusAndApproveTypeAndRequester(@Param("documentType") String documentType, @Param("documentStatus") String documentStatus,@Param("approveType") List<String> approveType,@Param("requester") String requester);
    List<Document> findByDocumentTypeAndDocumentStatusAndApproveTypeAndRequesterAndKeySearch(@Param("documentType") String documentType, @Param("documentStatus") String documentStatus,@Param("approveType") List<String> approveType,@Param("requester") String requester,String keySearch);
    List<Document> findSAPDocumentNumberDocumentAdvanceByRequestNumber(@Param("requestNumber") String requestNumber);
    DocumentAdvance findDocumentAdvanceByExternalDocNumber(@Param("externalDocNumber") String externalDocNumber,@Param("companyCode") String companyCode);
    List<DocumentExpenseItem> findDocumentExpenseItemByExternalDocNumber(@Param("externalDocNumber") String externalDocNumber,@Param("companyCode") String companyCode);
    List<DocumentExpenseItem> findDocumentExpenseItemByExternalClearingDocNumber(@Param("externalDocNumber") String externalDocNumber,@Param("companyCode") String companyCode);

    DocumentExpenseItem findDocumentExpenseItemByDocumentIdAndSequence(@Param("documentId") Long documentId,@Param("seqKey") String seqKey);
    List<Document> findByCreatedDateStartAndCreatedDateEndAndCreatorAndDocNumberAndDocumentType(
            @Param("createdDateStart") String createdDateStart,
            @Param("createdDateEnd") String createdDateEnd,
            @Param("creator") String creator,
            @Param("documentNumber") String documentNumber,
            @Param("documentType") List<String> documentType,
            @Param("documentStatus") List<String> documentStatus,
            @Param("firstResult") Long first,
            @Param("maxResult") Long max
    );

    Long pagingFindByCreatedDateStartAndCreatedDateEndAndCreatorAndDocNumberAndDocumentType(
            @Param("createdDateStart") String createdDateStart,
            @Param("createdDateEnd") String createdDateEnd,
            @Param("creator") String creator,
            @Param("documentNumber") String documentNumber,
            @Param("documentType") List<String> documentType,
            @Param("documentStatus") List<String> documentStatus
    );

    String cancelDocumentManual(String documentNumber);
    String clearDocumentManual(String documentNumber);
    Map<String,String> createDocumentFromEmed(String document);
}
