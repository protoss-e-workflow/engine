package com.spt.engine.repository.general;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.spt.engine.entity.general.ReportProjection;


public interface ReportProjectionRepository extends JpaSpecificationExecutor<ReportProjection>, JpaRepository<ReportProjection, Long>, PagingAndSortingRepository<ReportProjection, Long>  {

	
}
