package com.spt.engine.repository.general;

import com.spt.engine.entity.app.Request;
import com.spt.engine.entity.app.RequestApprover;
import com.spt.engine.repository.app.custom.RequestApproverRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RequestApproverRepository extends JpaSpecificationExecutor<RequestApprover>, JpaRepository<RequestApprover, Long>, PagingAndSortingRepository<RequestApprover, Long> ,RequestApproverRepositoryCustom{


    @Query("select DISTINCT u from RequestApprover u left join u.request r where r.id = :request order by u.id  ")
    List<RequestApprover> findByRequest(@Param("request") Long request);

    @Query("select DISTINCT u.request from RequestApprover u where u.userNameApprover = :userNameApprover and u.requestStatusCode is not null")
    List<Request> findByUserNameApprover(@Param("userNameApprover") String userNameApprover);

}
