package com.spt.engine.repository.general.custom;

import com.spt.engine.entity.general.RunningType;


public interface RunningNumberRepositoryCustom   {

	public String generateRunningNumber( RunningType runningType,String prefix) ;
	public String generateRunningNumber( String docStatus,String docType,String compCode,String status) ;
	public String generateRealRunningNumber( String docType,String compCode,String status) ;
}
