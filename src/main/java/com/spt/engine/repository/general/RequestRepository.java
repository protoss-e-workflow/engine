package com.spt.engine.repository.general;

import com.spt.engine.entity.app.Request;
import com.spt.engine.repository.general.custom.RequestRepositoryCustom;
import com.spt.engine.repository.general.projection.HaveRequestApprover;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(excerptProjection = HaveRequestApprover.class)
public interface RequestRepository extends JpaSpecificationExecutor<Request>, JpaRepository<Request, Long>, PagingAndSortingRepository<Request, Long>,RequestRepositoryCustom {

    @Query("select DISTINCT u from Request u left join u.document r where r.documentType like CONCAT('%',:documentType,'%')  ")
    List<Request> findByDocumentType(@Param("documentType") String documentType);

    @Query("select  u from Request u left join u.document r where r.id = :document")
    Request findByDocument(@Param("document") Long document);

    List<Request> findByNextApproverAndAndDocument_DocumentTypeIn(
            @Param("nextApprover") String nextApprover,
            @Param("documentType") List<String> documentType
    );

    @Query(value = "SELECT DISTINCT * FROM REQUEST R INNER JOIN DOCUMENT D ON R.ID = D.REQUEST_ID WHERE R.NEXT_APPROVER IN ?1 AND D.REQUESTER LIKE %?2% AND (D.TITLE_DESCRIPTION IS NULL OR D.TITLE_DESCRIPTION LIKE %?3%) AND D.DOCUMENT_STATUS = ?4 ORDER BY D.SEND_DATE DESC",nativeQuery = true)
    List<Request> findByNextApproverInAndDocument_RequesterIgnoreCaseContainingAndDocument_TitleDescriptionIgnoreCaseContainingAndDocument_DocumentStatusOrderByDocument_SendDateDesc(
            @Param("nextApprover")List<String> nextApprover,
            @Param("requester")String requester,
            @Param("titleDescription") String titleDescription,
            @Param("doc_status") String documentStatus
    );

    @Query(value = "SELECT DISTINCT * FROM REQUEST R INNER JOIN DOCUMENT D ON R.ID = D.REQUEST_ID WHERE R.NEXT_APPROVER = ?1 AND D.REQUESTER LIKE %?2% AND (D.TITLE_DESCRIPTION IS NULL OR D.TITLE_DESCRIPTION LIKE %?3%) AND D.DOCUMENT_STATUS = ?4 ORDER BY D.SEND_DATE DESC",nativeQuery = true)
    List<Request> findByNextApproverAndDocument_RequesterIgnoreCaseContainingAndDocument_TitleDescriptionIgnoreCaseContainingAndDocument_DocumentStatusOrderByDocument_SendDateDesc(
            @Param("nextApprover")String nextApprover,
            @Param("requester")String requester,
            @Param("titleDescription") String titleDescription,
            @Param("doc_status") String documentStatus
    );

    @Query(value = "SELECT DISTINCT * FROM REQUEST R INNER JOIN DOCUMENT D ON R.ID = D.REQUEST_ID WHERE R.NEXT_APPROVER = ?1 AND D.DOC_NUMBER LIKE %?2% AND (D.TITLE_DESCRIPTION IS NULL OR D.TITLE_DESCRIPTION LIKE %?3%) AND D.DOCUMENT_TYPE = ?4 AND D.DOCUMENT_STATUS IN (?5) ORDER BY D.SEND_DATE DESC ",nativeQuery = true)
    List<Request> findByNextApproverAndDocument_DocNumberIgnoreCaseContainingAndDocument_TitleDescriptionIgnoreCaseContainingAndDocument_DocumentTypeAndDocument_DocumentStatusInOrderByDocument_SendDateDesc(
            @Param("nextApprover") String nextApprover,
            @Param("docNumber") String docNumber,
            @Param("titleDescription") String titleDescription,
            @Param("documentType") String documentType,
            @Param("documentStatus") List<String> documentStatus
    );

    Request findById(@Param("id") Long id);

    Request findByRequestNumber(@Param("requestNumber")String requestNumber);

    List<Request> findByNextApproverInAndDocument_RequesterIgnoreCaseContainingAndDocument_DocNumberIgnoreCaseContainingAndDocument_DocumentStatusOrderByDocument_SendDateDesc(
            @Param("nextApprover")List<String> nextApprover,
            @Param("requester")String requester,
            @Param("docNumber") String docNumber,
            @Param("doc_status") String documentStatus
    );

    Page<Request> findByNextApproverInAndDocument_RequesterIgnoreCaseContainingAndDocument_DocNumberIgnoreCaseContainingAndDocument_DocumentStatusAndDocument_DocumentTypeInOrderByDocument_SendDateDesc(
            @Param("nextApprover")List<String> nextApprover,
            @Param("requester")String requester,
            @Param("docNumber") String docNumber,
            @Param("documentStatus") String documentStatus,
            @Param("documentType") List<String> documentType,
            Pageable pageable
    );

    List<Request> findByNextApproverAndDocument_RequesterIgnoreCaseContainingAndDocument_DocNumberIgnoreCaseContainingAndDocument_DocumentStatusOrderByDocument_SendDateDesc(
            @Param("nextApprover")String nextApprover,
            @Param("requester")String requester,
            @Param("docNumber") String docNumber,
            @Param("doc_status") String documentStatus
    );

    Page<Request> findByNextApproverInAndDocument_RequesterIgnoreCaseContainingAndDocument_DocNumberIgnoreCaseContainingAndDocument_DocumentStatusAndDocument_DocumentTypeInAndDocument_CompanyCodeInOrderByDocument_SendDateDesc(
            @Param("nextApprover")List<String> nextApprover,
            @Param("requester")String requester,
            @Param("docNumber") String docNumber,
            @Param("documentStatus") String documentStatus,
            @Param("documentType") List<String> documentType,
            @Param("companyCode") List<String> companyCode,
            Pageable pageable
    );

}
