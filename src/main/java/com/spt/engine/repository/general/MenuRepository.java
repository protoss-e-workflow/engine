package com.spt.engine.repository.general;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.general.Menu;


public interface MenuRepository extends JpaSpecificationExecutor<Menu>, JpaRepository<Menu, Long>, PagingAndSortingRepository<Menu, Long>  {

	
	@Query("select DISTINCT u from Menu u left join u.roles r where  lower(u.code) like CONCAT('%',lower(:code),'%') and "
			+ " lower(u.name) like CONCAT('%',lower(:name),'%') and "
            + " r.id in :roles ")
	Page<Menu> findByCodeIgnoreCaseContainingAndNameIgnoreCaseContainingAndRolesIn(
			@Param("code") String code,
			@Param("name") String name,
			@Param("roles") List<Long> roles,
			Pageable pageable);
	
	
	Menu findByCode(@Param("code") String code);
}
