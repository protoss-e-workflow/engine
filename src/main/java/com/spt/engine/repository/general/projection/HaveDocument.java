package com.spt.engine.repository.general.projection;

import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.Request;
import com.spt.engine.entity.app.RequestApprover;
import org.springframework.data.rest.core.config.Projection;

import java.sql.Timestamp;
import java.util.Set;

@Projection(types = {Request.class})
public interface HaveDocument {
    Long getId();
    String getCreatedBy();
    Timestamp getCreatedDate();
    String getUpdateBy();
    Timestamp getUpdateDate();
    Timestamp getRequestDate();
    String getRequestNumber();
    String getRequestTypeCode();
    String getRequestStatusCode();
    String getNextApprover();
    String getDocFlow();
    Timestamp getLastActionTime();
    Document getDocument();
    Set<RequestApprover> getRequestApprovers();
}
