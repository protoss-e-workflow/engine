package com.spt.engine.repository.general;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.general.LocaleMessage;

import java.util.List;


public interface LocaleMessageRepository extends JpaSpecificationExecutor<LocaleMessage>, JpaRepository<LocaleMessage, Long>, PagingAndSortingRepository<LocaleMessage, Long>  {

	Page<LocaleMessage> findByGroupCode(
			@Param("groupCode") String groupCode,
			Pageable pageable);
	LocaleMessage findByCode(@Param("code") String code);
	

	@Query("select DISTINCT u from LocaleMessage u  where  lower(u.groupCode) like CONCAT('%',lower(:groupCode),'%') and "
			+ " lower(u.code) like CONCAT('%',lower(:code),'%')  and "
			+ " lower(u.fullText) like CONCAT('%',lower(:fullText),'%') ")
	Page<LocaleMessage> findByGroupCodeAndCodeIgnoreCaseContainingAndFullTextIgnoreCaseContaining(
			@Param("groupCode") String groupCode,
			@Param("code") String code,
			@Param("fullText") String fullText,
			Pageable pageable);

	@Query("select DISTINCT msg from LocaleMessage msg where msg.code = :code")
	List<LocaleMessage> findLocaleMessageByCode(@Param("code") String code);
}
