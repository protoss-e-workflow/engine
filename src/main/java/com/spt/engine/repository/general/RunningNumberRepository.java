package com.spt.engine.repository.general;

import org.springframework.data.repository.CrudRepository;

import com.spt.engine.entity.general.RunningNumber;
import com.spt.engine.repository.general.custom.RunningNumberRepositoryCustom;

public interface RunningNumberRepository  extends CrudRepository<RunningNumber, Long>, RunningNumberRepositoryCustom {

}
