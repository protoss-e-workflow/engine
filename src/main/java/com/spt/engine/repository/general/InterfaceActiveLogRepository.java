package com.spt.engine.repository.general;

import com.spt.engine.entity.app.InterfaceActiveLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InterfaceActiveLogRepository extends JpaSpecificationExecutor<InterfaceActiveLog>, JpaRepository<InterfaceActiveLog, Long>, PagingAndSortingRepository<InterfaceActiveLog, Long> {

    List<InterfaceActiveLog> findByDocNumberEquals(@Param("docNumber")String docNumber);
}
