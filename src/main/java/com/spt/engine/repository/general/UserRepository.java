package com.spt.engine.repository.general;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.spt.engine.entity.general.User;


public interface UserRepository extends JpaSpecificationExecutor<User>, JpaRepository<User, Long>, PagingAndSortingRepository<User, Long>  {

	User findByUsername(@Param("username") String username);
	
	@Query("select DISTINCT u from User u left join u.roles r where  lower(u.username) like CONCAT('%',lower(:username),'%') and "
			+ " ( lower(u.firstName) like CONCAT('%',lower(:firstName),'%') or  lower(u.lastName) like CONCAT('%',lower(:lastName),'%') ) and "
            + " r.id in :roles ")
	Page<User> findByUsernameIgnoreCaseContainingAndFirstNameIgnoreCaseContainingOrLastNameIgnoreCaseContainingAndRolesIn(
			@Param("username") String username,
			@Param("firstName") String firstName,
			@Param("lastName") String lastName,
			@Param("roles") List<Long> roles,
			Pageable pageable);
	

	@Query("select DISTINCT u from User u left join u.roles r where  lower(u.username) like CONCAT('%',lower(:username),'%') and "
			+ " lower(u.fullName) like CONCAT('%',lower(:fullName),'%')  and "
			+ " r.id in :roles ")
	Page<User> findByUsernameIgnoreCaseContainingAndFullNameIgnoreCaseContainingAndRolesIn(
			@Param("username") String username,
			@Param("fullName") String fullName,
			@Param("roles") List<Long> roles,
			Pageable pageable);
}
