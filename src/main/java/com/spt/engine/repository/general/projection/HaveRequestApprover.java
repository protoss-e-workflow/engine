package com.spt.engine.repository.general.projection;

import com.spt.engine.entity.app.Request;
import com.spt.engine.entity.app.RequestApprover;
import org.springframework.data.rest.core.config.Projection;

import java.sql.Timestamp;
import java.util.Set;

@Projection(types = {Request.class})
public interface HaveRequestApprover {
    Timestamp getLastActionTime();
    String getNextApprover();
    String getRequestStatusCode();
    Set<RequestApprover> getRequestApprovers();
}
