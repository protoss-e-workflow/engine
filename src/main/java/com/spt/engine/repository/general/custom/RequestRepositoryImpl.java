package com.spt.engine.repository.general.custom;

import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.InternalOrder;
import com.spt.engine.entity.app.Request;
import com.spt.engine.repository.app.InternalOrderRepository;
import com.spt.engine.repository.app.custom.InternalOrderRepositoryCustom;
import com.spt.engine.repository.general.RequestRepository;
import com.spt.engine.service.InternalOrderService;
import com.spt.engine.service.StampStatusRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

public class RequestRepositoryImpl implements RequestRepositoryCustom {

    static final Logger LOGGER = LoggerFactory.getLogger(RequestRepositoryImpl.class);


   @Autowired
    StampStatusRequestService stampStatusRequestService;

   @Autowired
    RequestRepository requestRepository;

    @Transactional
    @Override
    public void stampPaidStatusToRequest() {


        Request request = null;

        try{



            List<Map<String,Object>> listResult = stampStatusRequestService.readDataRequest();

            if(!listResult.isEmpty()  ){

                for(Map mapData : listResult){

                    if( (String.valueOf(mapData.get("requestNumber"))) != null    ){

                        request   =  requestRepository.findByRequestNumber(String.valueOf(mapData.get("requestNumber")));
                        request.setPaidStatus(ConstantVariable.PAID_STATUS);
                        requestRepository.saveAndFlush(request);
                    }



                }
            }




        }catch(Exception e){

            e.printStackTrace();

        }

    }
}
