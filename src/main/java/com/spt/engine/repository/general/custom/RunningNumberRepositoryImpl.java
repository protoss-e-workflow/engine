package com.spt.engine.repository.general.custom;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.general.RunningNumber;
import com.spt.engine.entity.general.RunningType;

public class RunningNumberRepositoryImpl implements RunningNumberRepositoryCustom  {

	static final Logger LOGGER = LoggerFactory.getLogger(RunningNumberRepositoryImpl.class);
	
	@PersistenceContext
    private EntityManager entityManager;
	
	@Transactional
	@Override
	public synchronized String generateRunningNumber( RunningType runningType,String prefix) {
		String runningNumberStr = null;
		// TODO Auto-generated method stub
		if(runningType!=null){
			Query query = entityManager.createQuery("SELECT u FROM RunningNumber u WHERE u.runningType = :runningType and u.prefix=:prefix ");
			query.setParameter("runningType", runningType);
			query.setParameter("prefix", prefix);
			RunningNumber runningNumberObject;
			
			try {
				runningNumberObject = (RunningNumber) query.getSingleResult();
				runningNumberObject.setRunning(runningNumberObject.getRunning()+1);
			} catch (javax.persistence.NoResultException e) {
				// TODO Auto-generated catch block
				runningNumberObject = new RunningNumber();
				runningNumberObject.setRunningType(runningType);
				runningNumberObject.setPrefix(prefix);
				runningNumberObject.setRunning(1);
				entityManager.persist(runningNumberObject);
			}
			
			String runningString = ""+runningNumberObject.getRunning();
			if(runningType.getRunningFormat() != null){
				int countZero = StringUtils.countOccurrencesOf(runningType.getRunningFormat(), "0");
				String zeroStream = "";
				for(int i=0;i<countZero;i++){
					zeroStream += "0";
				}
				
				if(zeroStream.length() > 0){
					NumberFormat nf = new DecimalFormat(zeroStream);
					runningString = nf.format(runningNumberObject.getRunning());
				}
				
				
			}
			runningNumberStr = prefix+""+runningString;
		}
		
		
		return runningNumberStr;
	}

	@Transactional
	@Override
	public synchronized  String generateRunningNumber(String docStatus, String docType, String compCode, String status) {
		// TODO Auto-generated method stub
		String prefix = "";
		Date date = new Date();
		SimpleDateFormat format1 = new SimpleDateFormat("YYMMdd");
		String dateCurrent = format1.format(date); 
		
//		prefix = docStatus+ConstantVariable.UNDER_SCORE+docType+ConstantVariable.UNDER_SCORE+
//				compCode+ConstantVariable.UNDER_SCORE+dateCurrent+status;
		
		prefix = docStatus+docType+compCode+dateCurrent+status;
		return prefix;
	}

	@Transactional
	@Override
	public synchronized  String generateRealRunningNumber(String docType, String compCode, String status) {
		// TODO Auto-generated method stub
		String prefix = "";
		Date date = new Date();
		SimpleDateFormat format1 = new SimpleDateFormat("YYMMdd");
		String dateCurrent = format1.format(date);

//		prefix = docType+ConstantVariable.UNDER_SCORE+
//				compCode+ConstantVariable.UNDER_SCORE+dateCurrent+status;
		prefix = docType+compCode+dateCurrent+status;
		return prefix;
	}

	

}
