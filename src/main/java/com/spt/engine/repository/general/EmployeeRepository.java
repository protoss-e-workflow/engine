package com.spt.engine.repository.general;

import com.spt.engine.entity.app.Company;
import com.spt.engine.entity.general.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EmployeeRepository extends JpaSpecificationExecutor<Employee>, JpaRepository<Employee, Long>, PagingAndSortingRepository<Employee, Long> {



    @Query("select DISTINCT u from Employee u order by u.empCode")
    List<Employee> findAllEmployee();


    Employee findByEmpCode(@Param("empCode") String empCode);

    Employee findByEmpUsername(@Param("empUsername")String empUsername);


}
