package com.spt.engine.repository.general;

import java.util.List;
import java.util.Set;

import com.spt.engine.ConstantVariable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.spt.engine.entity.general.MasterDataDetail;
import org.springframework.data.repository.query.Param;


public interface MasterDataDetailRepository extends JpaSpecificationExecutor<MasterDataDetail>, JpaRepository<MasterDataDetail, Long>, PagingAndSortingRepository<MasterDataDetail, Long>  {

	MasterDataDetail findByCode(@Param("code") String code);
	List<MasterDataDetail> findByCodeAndFlagActive(@Param("code") String code,@Param("flagActive") Boolean flagActive);


	Set<MasterDataDetail> findByOrgCodeOrderByVariable1Asc(@Param("orgCode") String orgCode);




	@Query("select DISTINCT u from MasterDataDetail u left join u.masterdata r where  lower(u.code) like CONCAT('%',lower(:code),'%') and "
			+ " lower(u.name) like CONCAT('%',lower(:name),'%')  and "
			+ " lower(u.orgCode) like CONCAT('%',lower(:orgCode),'%')  and "
			+ " r.id in :masterdata ")
	Page<MasterDataDetail> findByCodeIgnoreCaseContainingAndNameIgnoreCaseContainingAndOrgCodeIgnoreCaseContainingAndMasterdataIn(
			@Param("code") String code,
			@Param("name") String name,
			@Param("orgCode") String orgCode,
			@Param("masterdata") List<Long> masterdata,
			Pageable pageable);
	

	@Query("select DISTINCT u from MasterDataDetail u left join u.masterdata r where  lower(u.code) like CONCAT('%',lower(:code),'%') and "
			+ " lower(u.name) like CONCAT('%',lower(:name),'%')  and "
			+ " r.id in :masterdata ")
	Page<MasterDataDetail> findByCodeIgnoreCaseContainingAndNameIgnoreCaseContainingAndMasterdataIn(
			@Param("code") String code,
			@Param("name") String name,
			@Param("masterdata") List<Long> masterdata,
			Pageable pageable);

	@Query("select DISTINCT u from MasterDataDetail u left join u.masterdata r where r.code in :masterdata order by u.id ")
	List<MasterDataDetail> findByMasterdataIn( @Param("masterdata") List<String> masterdata);


	@Query("select DISTINCT u from MasterDataDetail u left join u.masterdata r where r.code in :masterdata order by u.code ")
	List<MasterDataDetail> findByMasterdataInOrderByCode( @Param("masterdata") List<String> masterdata);

	@Query("select DISTINCT u from MasterDataDetail u left join u.masterdata r where r.code in :masterdata and u.code = :code  order by u.code ")
	MasterDataDetail findByMasterdataInAndMasterDataDetailCodeOrderByCode(@Param("masterdata") List<String> masterdata,
																		  @Param("code")String code);

	@Query("select DISTINCT u from MasterDataDetail u left join u.masterdata r where r.code in :masterdata and u.code = :code  order by u.code ")
	List<MasterDataDetail> findByMasterdataInAndMasterDataDetailCodeOrderByCodeList(@Param("masterdata") List<String> masterdata,
																		  @Param("code")String code);



	@Query("select DISTINCT u from MasterDataDetail u left join u.masterdata r where r.code in :masterdata and u.code like CONCAT(:code,'%')  order by u.code ")
	MasterDataDetail findByMasterdataInAndMasterDataDetailCodeLikeOrderByCode(@Param("masterdata") List<String> masterdata,
																		      @Param("code")String code);

	List<MasterDataDetail> findByVariable1(@Param("userHead") String variable1);







}
