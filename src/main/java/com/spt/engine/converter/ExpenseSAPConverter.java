package com.spt.engine.converter;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentExpenseItem;
import com.spt.engine.entity.app.DocumentReference;
import com.spt.engine.util.NumberUtil;
import com.spt.engine.util.TextUtil;



public class ExpenseSAPConverter {
	static final Logger LOGGER = LoggerFactory.getLogger(ExpenseSAPConverter.class);



	private static SimpleDateFormat  sdfYYYYMMDD = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
	private static SimpleDateFormat  sdfYYYY = new SimpleDateFormat("yyyy", Locale.ENGLISH);
	private static SimpleDateFormat  sdfMM = new SimpleDateFormat("MM", Locale.ENGLISH);
	private static SimpleDateFormat  sdfYYYYMMDDHHmmss = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);

	static JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
		public Date deserialize(JsonElement json, Type typeOfT,
								JsonDeserializationContext context) throws JsonParseException {
			return json == null ? null : new Date(json.getAsLong());
		}
	};

	protected static Gson gson = new GsonBuilder().disableHtmlEscaping().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();


	@SuppressWarnings("Duplicates")
	public static Map toSAPStructure(Document document,Map param){
		
		Map returnMap = new HashMap();
		
		Date currentDate = new Date();
		List<Map<String,String>> dataDoc = new ArrayList<Map<String,String>>();
		Set<DocumentExpenseItem> documentExpenseItems = document.getDocumentExpense().getDocumentExpenseItems();
		String docAdvanceRef    = (String) param.get("advanceSapDocNumber");
		String advanceDocNumber = (String) param.get("advanceDocNumber");
		String clearingCase     = (String) param.get("clearingCase");
		String glBankReceive    = (String) param.get("glBankReceive");



		BigDecimal whtAmountForWht = new BigDecimal(0);


		Map<String,Map> fiSet = new HashMap();
		Map<String,Map> fiGlSet = new HashMap();
		Map<String,Map> fiWhtSet = new HashMap();
		for(DocumentExpenseItem item:documentExpenseItems){
			String invNo   = String.valueOf(item.getDocNumber());
			if(invNo.trim().length()==0){
				invNo="null";
			}
			String vatCode = String.valueOf(item.getVatCode());
			
			if(vatCode.trim().length()==0){
				vatCode="VX";
			}
			
			if(vatCode.equals("VX")){
				invNo="null";
			}
			String vatCodeValue = String.valueOf(item.getVatCodeValue());
			String glCode = String.valueOf(item.getGlCode());
			
			String whtType = String.valueOf(item.getWhtType());
			
			if(whtType.trim().length()==0){
				whtType="NWHT";
			}
			
			String key    = String.valueOf(item.getSeller())+"_"+invNo+"_"+vatCode+whtType;
			String keyGl  = String.valueOf(item.getSeller())+"_"+invNo+"_"+vatCode+whtType+"|"+glCode+"|"+item.getId();
			String keyWht = String.valueOf(item.getSeller())+"_"+String.valueOf(item.getWhtType())+"_"+String.valueOf(item.getWhtCode());

			Map dataItem = (Map) fiSet.get(key);
			if(dataItem==null){
				dataItem = new HashMap();
			}

			Map dataItemGl = (Map) fiGlSet.get(keyGl);
			if(dataItemGl==null){
				dataItemGl = new HashMap();
			}
			
			Map dataItemWht = (Map) fiWhtSet.get(keyWht);
			if(dataItemWht==null && NumberUtil.getBigDecimal(item.getWhtAmount()).intValue() > 0){
				dataItemWht = new HashMap();
			}

			BigDecimal netAmount      = NumberUtil.getBigDecimal(dataItem.get("netAmount")).add(NumberUtil.getBigDecimal(item.getNetAmount()));
			BigDecimal vatAmount      = NumberUtil.getBigDecimal(dataItem.get("taxAmount")).add(NumberUtil.getBigDecimal(item.getVat()));
			BigDecimal amount         = NumberUtil.getBigDecimal(dataItem.get("amount")).add(NumberUtil.getBigDecimal(item.getAmount()));
			BigDecimal netAmountGl    = NumberUtil.getBigDecimal(dataItemGl.get("netAmount")).add(NumberUtil.getBigDecimal(item.getNetAmount()));
			BigDecimal whtAmount      = NumberUtil.getBigDecimal(dataItem.get("whtAmount")).add(NumberUtil.getBigDecimal(item.getWhtAmount()));
			

			whtAmountForWht      = whtAmountForWht.add(NumberUtil.getBigDecimal(item.getWhtAmount()));




			String streamId = "";
			if(dataItem.get("itemId")==null){
				streamId = ","+String.valueOf(item.getSeqKey());
			}else{
				streamId = String.valueOf(dataItem.get("itemId"))+","+String.valueOf(item.getSeqKey());
			}

			dataItem.put("amount",amount);
			dataItem.put("netAmount",netAmount);
			dataItem.put("invNo",invNo);
			dataItem.put("vatCode",vatCode);
			dataItem.put("vatCodeValue",vatCodeValue);
			dataItem.put("seller",item.getSeller());
			dataItem.put("address",String.valueOf(item.getAddress()));
			dataItem.put("itemId",streamId);
			dataItem.put("internalOrder",item.getInternalOrder());
			dataItem.put("exchangeRate","1.00000");//item.getExchangeRate() // Fix
			dataItem.put("taxAmount",vatAmount);
			dataItem.put("taxNumber",item.getTaxIDNumber());
			dataItem.put("branch",String.valueOf(item.getBranch()));
			dataItem.put("businessPlace",String.valueOf(item.getBusinessPlace()));
			dataItem.put("remark",String.valueOf(item.getRemark()));
			dataItem.put("street",String.valueOf(item.getStreet()));
			dataItem.put("city",String.valueOf(item.getCity()));
			dataItem.put("postcode",String.valueOf(item.getPostcode()));
			dataItem.put("whtType",String.valueOf(item.getWhtType()));
			dataItem.put("whtCode",String.valueOf(item.getWhtCode()));
			dataItem.put("assignment",String.valueOf(item.getAssignment()));
			dataItem.put("profitCenter",String.valueOf(item.getProfitCenter()));
			dataItem.put("whtAmount",whtAmount);
			if(!"null".equals(String.valueOf(item.getWhtGroup())) && !"".equals(String.valueOf(item.getWhtGroup()).trim())){
				dataItem.put("whtGroup",String.valueOf(item.getWhtGroup()));
			}else if(!"null".equals(String.valueOf(item.getWhtType())) && !"".equals(String.valueOf(item.getWhtType()).trim())){
				dataItem.put("whtGroup",String.valueOf(item.getWhtType()).substring(0,1).toUpperCase());
			}
			
			
			dataItem.put("documentDate",item.getDocumentDate());

			fiSet.put(key, dataItem);



			dataItemGl.put("amount",NumberUtil.getBigDecimal(item.getAmount()));
			dataItemGl.put("netAmount",netAmountGl);
			dataItemGl.put("internalOrder",item.getInternalOrder());
			dataItemGl.put("profitCenter",item.getProfitCenter());
			dataItemGl.put("exchangeRate","1.00000");//item.getExchangeRate() // Fix
			dataItemGl.put("seller",item.getSeller());
			dataItemGl.put("branch",String.valueOf(item.getBranch()));
			dataItemGl.put("businessPlace",String.valueOf(item.getBusinessPlace()));
			dataItemGl.put("taxNumber",item.getTaxIDNumber());
			dataItemGl.put("address",String.valueOf(item.getAddress()));
			dataItemGl.put("remark",String.valueOf(item.getRemark()));
            dataItemGl.put("assignment",String.valueOf(item.getAssignment()));
            dataItemGl.put("wbs",String.valueOf(item.getWbs()));
            dataItemGl.put("quantity",String.valueOf(item.getQuantity()));
            dataItemGl.put("unit",String.valueOf(item.getUnit()));
			fiGlSet.put(keyGl, dataItemGl);

			

			
			//dataItemWht.put("seller",item.getSeller());
			//dataItemWht.put("whtType",String.valueOf(item.getWhtType()));
			//dataItemWht.put("whtCode",String.valueOf(item.getWhtCode()));
			//dataItemWht.put("whtAmount", whtAmountForWht);
			//fiWhtSet.put(keyWht, dataItemWht);
			

		}
		LOGGER.info("fiSet={}",gson.toJson(fiSet,Map.class));
		LOGGER.info("fiGlSet={}",gson.toJson(fiGlSet,Map.class));
		
		//
		Double expenseOverAmount = NumberUtil.getBigDecimal((String) param.get("expenseOverAmount")).doubleValue();
		Double whtAmountValue = NumberUtil.getBigDecimal(whtAmountForWht).doubleValue();
		Double expenseOverAmountAndWhtAmountValue = 0d;
		int compareExpenseOverAmountAndWhtAmountValue = Double.compare(whtAmountValue,expenseOverAmount);
		if(compareExpenseOverAmountAndWhtAmountValue ==1){
			expenseOverAmountAndWhtAmountValue = whtAmountValue - expenseOverAmount;
		}
		boolean isPrintedBankReceive = false;


		/* Split Doc FI */
		Map mapHeader =  null;
		for(String key:fiSet.keySet()){
			/* Set Header */
			mapHeader = new HashMap();
			mapHeader.put("COMP_CODE",  document.getCompanyCode());
			mapHeader.put("DOC_DATE",   sdfYYYYMMDD.format(fiSet.get(key).get("documentDate")));
			if(document.getPostingDate()!=null){
				mapHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(document.getPostingDate()));
				//mapHeader.put("FISC_YEAR",  sdfYYYY.format(document.getPostingDate()));
			}else{
				mapHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(currentDate));
				//mapHeader.put("FISC_YEAR",  sdfYYYY.format(currentDate));
			}
			
			
			mapHeader.put("FIS_PERIOD", null);
			mapHeader.put("DOC_TYPE",   "KC");
			
			String refferenceDoc = "-";
			if(!"null".equals(fiSet.get(key).get("invNo")) && !"".equals(fiSet.get(key).get("invNo"))){
				refferenceDoc = ""+fiSet.get(key).get("invNo");
			}
			mapHeader.put("REF_DOC_NO", refferenceDoc);//invoice number
			mapHeader.put("HEADER_TXT", docAdvanceRef);//ADVANCE_NUMBER//WHEN EXISTS

			try{
				Thread.sleep(1000);
			}catch (Exception e){
				throw new RuntimeException(e);
			}
//
			LOGGER.info("Time   :   {}",System.currentTimeMillis());
			String keyMapValue = sdfYYYYMMDDHHmmss.format(new Date());
			mapHeader.put("EXPENSE_NUMBER", keyMapValue);//Expense|item id,
			returnMap.put(keyMapValue, document.getId()+"#"+String.valueOf(fiSet.get(key).get("itemId")).substring(1));
			
			//mapHeader.put("EXPENSE_NUMBER", document.getId()+"#"+String.valueOf(fiSet.get(key).get("itemId")).substring(1));//Expense|item id,

			String url = param.get("appServer")+"/expense/expenseDetail?doc="+document.getId();
			mapHeader.put("URL",        url);


			/*Setup Item1 */
			String vatCode = String.valueOf(fiSet.get(key).get("vatCode"));
			String vatCodeValue = String.valueOf(fiSet.get(key).get("vatCodeValue"));
			List<Map<String,String>> dataBankItem = new ArrayList<Map<String,String>>();
			List<Map<String,String>> dataItem = new ArrayList<Map<String,String>>();
			int indexItem = 0;
			for(String keyGl:fiGlSet.keySet()){
				Map mapDataItem1 =null;

				if(keyGl.startsWith(key)){
					indexItem++;
					mapDataItem1 = new HashMap();
					mapDataItem1.put("ITEMNO_ACC", String.format("%03d", indexItem));
					mapDataItem1.put("WBS_ELEMENT", "null".equals(""+fiGlSet.get(keyGl).get("wbs"))?"":fiGlSet.get(keyGl).get("wbs"));
					mapDataItem1.put("ASSIGNMENT", "null".equals(""+fiGlSet.get(keyGl).get("assignment"))?"":fiGlSet.get(keyGl).get("assignment"));
					mapDataItem1.put("QUANTITY", "null".equals(""+fiGlSet.get(keyGl).get("quantity"))?"":fiGlSet.get(keyGl).get("quantity"));
					mapDataItem1.put("BASE_UOM", "null".equals(""+fiGlSet.get(keyGl).get("unit"))?"":fiGlSet.get(keyGl).get("unit"));
					mapDataItem1.put("GL_ACCOUNT", keyGl.split("\\|")[1]);
					mapDataItem1.put("VENDOR_NO", null);//"M"+document.getPersonalId().substring(1)
					mapDataItem1.put("BLINE_DATE", null);
					mapDataItem1.put("BUSINESSPLACE", fiGlSet.get(keyGl).get("businessPlace"));
					String branch = "00000";
					if(!"null".equals(fiGlSet.get(keyGl).get("branch")) && !"".equals(fiGlSet.get(keyGl).get("branch"))){
						branch = ""+fiGlSet.get(keyGl).get("branch");
					}
					if("null".equals(vatCode)){
						branch = "NVAT";
						vatCode = null;//"VX";
						vatCodeValue = null;
					}
					mapDataItem1.put("J_1TPBUPL", branch);
					mapDataItem1.put("ALLOC_NMBR", advanceDocNumber);
					mapDataItem1.put("ITEM_TEXT", fiGlSet.get(keyGl).get("remark"));
					mapDataItem1.put("ACCT_TYPE", "S");
					mapDataItem1.put("TAX_CODE", vatCode);
					mapDataItem1.put("TAX_RATE", vatCodeValue);
					mapDataItem1.put("COSTCENTER", document.getCostCenterCode());
					mapDataItem1.put("PROFIT_CTR", fiGlSet.get(keyGl).get("profitCenter"));
					//mapDataItem1.put("WBS_ELEMENT", fiGlSet.get(keyGl).get("wbs"));
					mapDataItem1.put("ORDERID", fiGlSet.get(keyGl).get("internalOrder"));
					mapDataItem1.put("CURRENCY_ISO", "THB");
					mapDataItem1.put("EXCH_RATE", fiGlSet.get(keyGl).get("exchangeRate"));
					mapDataItem1.put("AMT_DOCCUR", fiGlSet.get(keyGl).get("amount"));
//					mapDataItem1.put("ASSIGNMENT", fiGlSet.get(keyGl).get("assignment"));
//					mapDataItem1.put("QUANTITY", fiGlSet.get(keyGl).get("quantity"));
//					mapDataItem1.put("BASE_UOM", fiGlSet.get(keyGl).get("unit"));
					mapDataItem1.put("AMT_BASE", "0.0000");
					mapDataItem1.put("TAX_AMT", "0.0000");

					

					mapDataItem1.put("WITTHOLDINGTAX", new HashMap());
					dataItem.add(mapDataItem1);

				}

			}

			/* Set Item 2 :Employee */
			indexItem++;
			Map mapDataItem2 = new HashMap();
			mapDataItem2.put("ITEMNO_ACC", String.format("%03d", indexItem));
			mapDataItem2.put("GL_ACCOUNT", null);
			mapDataItem2.put("VENDOR_NO", "M"+document.getPersonalId().substring(1));
			mapDataItem2.put("SP_GL_IND", null);
			mapDataItem2.put("BLINE_DATE", sdfYYYYMMDD.format(currentDate));
			mapDataItem2.put("BUSINESSPLACE", fiSet.get(key).get("businessPlace"));
			
			String branch = "00000";
			if(!"null".equals(fiSet.get(key).get("branch")) && !"".equals(fiSet.get(key).get("branch"))){
				branch = ""+fiSet.get(key).get("branch");
			}
			if("null".equals(vatCode)){
				branch = "NVAT";
			}
			mapDataItem2.put("J_1TPBUPL", branch);
			
			
			if(advanceDocNumber!=null){
				mapDataItem2.put("ALLOC_NMBR", advanceDocNumber);
			}else{
				mapDataItem2.put("ALLOC_NMBR", document.getDocNumber());
			}
			mapDataItem2.put("ITEM_TEXT", fiSet.get(key).get("remark"));
			mapDataItem2.put("ACCT_TYPE", "K");
			mapDataItem2.put("TAX_CODE", vatCode);
			mapDataItem2.put("TAX_RATE", vatCodeValue);
			mapDataItem2.put("COSTCENTER", null);
			mapDataItem2.put("PROFIT_CTR", null);
			mapDataItem2.put("WBS_ELEMENT", null);
			mapDataItem2.put("ORDERID", null);
			mapDataItem2.put("CURRENCY_ISO", "THB");
			mapDataItem2.put("EXCH_RATE", fiSet.get(key).get("exchangeRate"));
			if("XXX".equals(String.valueOf(fiSet.get(key).get("whtGroup")))){
				mapDataItem2.put("AMT_DOCCUR", "-"+NumberUtil.getBigDecimal(fiSet.get(key).get("netAmount")));
			}else{
				mapDataItem2.put("AMT_DOCCUR", "-"+NumberUtil.getBigDecimal(fiSet.get(key).get("netAmount")).add(NumberUtil.getBigDecimal(fiSet.get(key).get("whtAmount"))));
			}
			mapDataItem2.put("AMT_BASE", "0.0000");
			mapDataItem2.put("TAX_AMT", "0.0000");
			
			
			/* Set Withholding Tax */
			Map holdingMap = new HashMap<String,String>();
			if(!"null".equals(String.valueOf(fiSet.get(key).get("whtCode"))) &&
					!"".equals(String.valueOf(fiSet.get(key).get("whtCode")).trim()) &&
					!"null".equals(String.valueOf(fiSet.get(key).get("whtType"))) &&
					!"".equals(String.valueOf(fiSet.get(key).get("whtType")).trim())){
				LOGGER.info("Data whtAmount = |{}|",String.valueOf(fiSet.get(key).get("whtAmount")));
				holdingMap.put("ITEMNO_ACC", String.format("%03d", indexItem));//
				holdingMap.put("WT_TYPE", String.valueOf(fiSet.get(key).get("whtType")));
				holdingMap.put("WT_CODE",String.valueOf(fiSet.get(key).get("whtCode")));
				holdingMap.put("BAS_AMT_TC", String.valueOf(fiSet.get(key).get("amount")));
				holdingMap.put("MAN_AMT_TC",String.valueOf(fiSet.get(key).get("whtAmount")));
				holdingMap.put("ALLOC_NMBR",String.valueOf(fiSet.get(key).get("assignment")));
				holdingMap.put("ITEM_TEXT",String.valueOf(fiSet.get(key).get("assignment")));
				holdingMap.put("PROFIT_CTR",String.valueOf(fiSet.get(key).get("profitCenter")));

				Map<String,String> addressMap = TextUtil.getAddressMap(String.valueOf(fiSet.get(key).get("address")));
				List<String> seller = TextUtil.spiltSeller(String.valueOf(fiSet.get(key).get("seller")));
				if(seller.size()>1){
					mapDataItem2.put("NAME1", seller.get(0));
					mapDataItem2.put("NAME2", seller.get(1));
				}else{
					mapDataItem2.put("NAME1", seller.get(0));
					mapDataItem2.put("NAME2", null);
				}
				mapDataItem2.put("NAME3", null);
				mapDataItem2.put("NAME4", null);
				mapDataItem2.put("PSTLZ", fiSet.get(key).get("postcode"));
				mapDataItem2.put("ORT01", fiSet.get(key).get("city"));
				mapDataItem2.put("LAND1", "TH");
				mapDataItem2.put("STRAS", fiSet.get(key).get("street"));
				mapDataItem2.put("STCD1", fiSet.get(key).get("taxNumber"));
				
			}

			if(!"null".equals(vatCode) &&
					!"V0".equals(vatCode) &&
					!"VX".equals(vatCode) ){
				Map<String,String> addressMap = TextUtil.getAddressMap(String.valueOf(fiSet.get(key).get("address")));
				List<String> seller = TextUtil.spiltSeller(String.valueOf(fiSet.get(key).get("seller")));
				if(seller.size()>1){
					mapDataItem2.put("NAME1", seller.get(0));
					mapDataItem2.put("NAME2", seller.get(1));
				}else{
					mapDataItem2.put("NAME1", seller.get(0));
					mapDataItem2.put("NAME2", null);
				}

				mapDataItem2.put("NAME3", null);
				mapDataItem2.put("NAME4", null);
				mapDataItem2.put("PSTLZ", fiSet.get(key).get("postcode"));
				mapDataItem2.put("ORT01", fiSet.get(key).get("city"));
				mapDataItem2.put("LAND1", "TH");
				mapDataItem2.put("STRAS", fiSet.get(key).get("street"));
				mapDataItem2.put("STCD1", fiSet.get(key).get("taxNumber"));
			}
			
			
			mapDataItem2.put("WITTHOLDINGTAX", holdingMap);
			dataItem.add(mapDataItem2);





			/* Set Item 3 :Vat */
			if(vatCode != null &&
					!"null".equals(vatCode) &&
					!"V0".equals(vatCode) &&
					!"VX".equals(vatCode) ){
				LOGGER.info("============== Have Vat |{}|",vatCode);
				indexItem++;
				Map mapDataItem3 = new HashMap();
				mapDataItem3.put("ITEMNO_ACC", String.format("%03d", indexItem));
				mapDataItem3.put("GL_ACCOUNT", "124100");//Fix
				mapDataItem3.put("VENDOR_NO", null);//"M"+document.getPersonalId()
				mapDataItem3.put("SP_GL_IND", null);
				mapDataItem3.put("BLINE_DATE", null);
				mapDataItem3.put("BUSINESSPLACE",fiSet.get(key).get("businessPlace"));
				mapDataItem3.put("J_1TPBUPL", branch);
				if(advanceDocNumber!=null){
					mapDataItem3.put("ALLOC_NMBR", advanceDocNumber);
				}else{
					mapDataItem3.put("ALLOC_NMBR", document.getDocNumber());
				}
				mapDataItem3.put("ITEM_TEXT", fiSet.get(key).get("remark"));
				mapDataItem3.put("ACCT_TYPE", "S");
				mapDataItem3.put("TAX_CODE", vatCode);
				mapDataItem3.put("TAX_RATE", vatCodeValue);
				mapDataItem3.put("COSTCENTER", null);
				mapDataItem3.put("PROFIT_CTR", null);
				mapDataItem3.put("WBS_ELEMENT", null);
				mapDataItem3.put("ORDERID", null);
				mapDataItem3.put("CURRENCY_ISO", "THB");
				mapDataItem3.put("EXCH_RATE", fiSet.get(key).get("exchangeRate"));
				mapDataItem3.put("AMT_DOCCUR", fiSet.get(key).get("taxAmount"));
				mapDataItem3.put("AMT_BASE", fiSet.get(key).get("amount"));
				mapDataItem3.put("TAX_AMT", fiSet.get(key).get("taxAmount"));
				mapDataItem3.put("WITTHOLDINGTAX", new HashMap());
				dataItem.add(mapDataItem3);
			}

			mapHeader.put("ACCOUNTGL_ITEM", dataItem);
			dataDoc.add(mapHeader);

			

			if(!isPrintedBankReceive){
				if("A>E".equals(clearingCase)){
					
					String clearingDiffAmount = ""+whtAmountForWht.add(NumberUtil.getBigDecimal((String) param.get("clearingDiffAmount"))).setScale(2, BigDecimal.ROUND_HALF_EVEN);
					
					/* Set Header */
					Map mapBankHeader = new HashMap();
					mapBankHeader.put("COMP_CODE",  document.getCompanyCode());
					if(document.getPayinDate() !=null){
						mapBankHeader.put("DOC_DATE",   sdfYYYYMMDD.format(document.getPayinDate()));
					}else{
						mapBankHeader.put("DOC_DATE",   sdfYYYYMMDD.format(currentDate));
					}
					
					
					if(document.getPostingDate()!=null){
						mapBankHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(document.getPostingDate()));
						//mapBankHeader.put("FISC_YEAR",  sdfYYYY.format(document.getPostingDate()));
					}else{
						mapBankHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(currentDate));
						//mapBankHeader.put("FISC_YEAR",  sdfYYYY.format(currentDate));
					}
					
					mapBankHeader.put("FIS_PERIOD", null);//12
					mapBankHeader.put("DOC_TYPE",   "KA");
					mapBankHeader.put("REF_DOC_NO", refferenceDoc);//invoice number
					mapBankHeader.put("HEADER_TXT", docAdvanceRef);//ADVANCE_NUMBER//WHEN EXISTS
					
					String keyMapValueBank = sdfYYYYMMDDHHmmss.format(new Date());
					mapBankHeader.put("EXPENSE_NUMBER", keyMapValueBank);//Expense|item id,
					returnMap.put(keyMapValueBank, document.getId()+"#"+String.valueOf(fiSet.get(key).get("itemId")).substring(1));
					
					mapBankHeader.put("URL",        url);

					/* Set Item 1 :Employee */
					mapDataItem2 = new HashMap();
					mapDataItem2.put("ITEMNO_ACC", "001");
					mapDataItem2.put("GL_ACCOUNT", null);
					mapDataItem2.put("VENDOR_NO", "M"+document.getPersonalId().substring(1));
					mapDataItem2.put("SP_GL_IND", null);
					mapDataItem2.put("BLINE_DATE", sdfYYYYMMDD.format(currentDate));
					mapDataItem2.put("BUSINESSPLACE", fiSet.get(key).get("businessPlace"));
					mapDataItem2.put("J_1TPBUPL", branch);
					mapDataItem2.put("ALLOC_NMBR", null);
					mapDataItem2.put("ITEM_TEXT", fiSet.get(key).get("remark"));
					mapDataItem2.put("ACCT_TYPE", "K");
					mapDataItem2.put("TAX_CODE", null);
					mapDataItem2.put("TAX_RATE", null);
					mapDataItem2.put("COSTCENTER", null);
					mapDataItem2.put("PROFIT_CTR", null);
					mapDataItem2.put("WBS_ELEMENT", null);
					mapDataItem2.put("ORDERID", null);
					mapDataItem2.put("CURRENCY_ISO", "THB");
					mapDataItem2.put("EXCH_RATE", fiSet.get(key).get("exchangeRate"));
					mapDataItem2.put("AMT_DOCCUR", "-"+clearingDiffAmount);
					mapDataItem2.put("AMT_BASE", "0.0000");
					mapDataItem2.put("TAX_AMT", "0.0000");
					/* Set Withholding Tax */
					mapDataItem2.put("WITTHOLDINGTAX", new HashMap<String,String>());
					dataBankItem.add(mapDataItem2);

					/* Set Item 2 :Bank */
					Map mapDataItem3 = new HashMap();
					mapDataItem3.put("ITEMNO_ACC", "002");
					mapDataItem3.put("GL_ACCOUNT", glBankReceive);//Fix
					mapDataItem3.put("VENDOR_NO", null);//"M"+document.getPersonalId()
					mapDataItem3.put("SP_GL_IND", null);
					mapDataItem3.put("BLINE_DATE", sdfYYYYMMDD.format(currentDate));
					mapDataItem3.put("BUSINESSPLACE",fiSet.get(key).get("businessPlace"));
					mapDataItem3.put("J_1TPBUPL", null);
					mapDataItem3.put("ALLOC_NMBR", null);
					mapDataItem3.put("ITEM_TEXT", fiSet.get(key).get("remark"));
					mapDataItem3.put("ACCT_TYPE", "S");
					mapDataItem3.put("TAX_CODE", null);
					mapDataItem3.put("TAX_RATE", null);
					mapDataItem3.put("COSTCENTER", null);
					mapDataItem3.put("PROFIT_CTR", null);
					mapDataItem3.put("WBS_ELEMENT", null);
					mapDataItem3.put("ORDERID", null);
					mapDataItem3.put("CURRENCY_ISO", "THB");
					mapDataItem3.put("EXCH_RATE", fiSet.get(key).get("exchangeRate"));
					mapDataItem3.put("AMT_DOCCUR", clearingDiffAmount);
					mapDataItem3.put("AMT_BASE", "0.0000");
					mapDataItem3.put("TAX_AMT", "0.0000");
					mapDataItem3.put("WITTHOLDINGTAX", new HashMap());
					dataBankItem.add(mapDataItem3);

					mapBankHeader.put("ACCOUNTGL_ITEM", dataBankItem);
					dataDoc.add(mapBankHeader);


				}else if("A=E".equals(clearingCase) && whtAmountForWht.compareTo(BigDecimal.ZERO)!=0){
					
					String clearingDiffAmount = ""+whtAmountForWht.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					
					/* Set Header */
					Map mapBankHeader = new HashMap();
					mapBankHeader.put("COMP_CODE",  document.getCompanyCode());
					if(document.getPayinDate() !=null){
						mapBankHeader.put("DOC_DATE",   sdfYYYYMMDD.format(document.getPayinDate()));
					}else{
						mapBankHeader.put("DOC_DATE",   sdfYYYYMMDD.format(currentDate));
					}
					
					
					if(document.getPostingDate()!=null){
						mapBankHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(document.getPostingDate()));
						//mapBankHeader.put("FISC_YEAR",  sdfYYYY.format(document.getPostingDate()));
					}else{
						mapBankHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(currentDate));
						//mapBankHeader.put("FISC_YEAR",  sdfYYYY.format(currentDate));
					}
					
					mapBankHeader.put("FIS_PERIOD", null);//12
					mapBankHeader.put("DOC_TYPE",   "KA");
					mapBankHeader.put("REF_DOC_NO", refferenceDoc);//invoice number
					mapBankHeader.put("HEADER_TXT", docAdvanceRef);//ADVANCE_NUMBER//WHEN EXISTS
					String keyMapValueBank = sdfYYYYMMDDHHmmss.format(new Date());
					mapBankHeader.put("EXPENSE_NUMBER", keyMapValueBank);//Expense|item id,
					returnMap.put(keyMapValueBank, document.getId()+"#"+String.valueOf(fiSet.get(key).get("itemId")).substring(1));
					
					
					mapBankHeader.put("URL",        url);

					/* Set Item 1 :Employee */
					mapDataItem2 = new HashMap();
					mapDataItem2.put("ITEMNO_ACC", "001");
					mapDataItem2.put("GL_ACCOUNT", null);
					mapDataItem2.put("VENDOR_NO", "M"+document.getPersonalId().substring(1));
					mapDataItem2.put("SP_GL_IND", null);
					mapDataItem2.put("BLINE_DATE", sdfYYYYMMDD.format(currentDate));
					mapDataItem2.put("BUSINESSPLACE", fiSet.get(key).get("businessPlace"));
					mapDataItem2.put("J_1TPBUPL", branch);
					mapDataItem2.put("ALLOC_NMBR", null);
					mapDataItem2.put("ITEM_TEXT", fiSet.get(key).get("remark"));
					mapDataItem2.put("ACCT_TYPE", "K");
					mapDataItem2.put("TAX_CODE", null);
					mapDataItem2.put("TAX_RATE", null);
					mapDataItem2.put("COSTCENTER", null);
					mapDataItem2.put("PROFIT_CTR", null);
					mapDataItem2.put("WBS_ELEMENT", null);
					mapDataItem2.put("ORDERID", null);
					mapDataItem2.put("CURRENCY_ISO", "THB");
					mapDataItem2.put("EXCH_RATE", fiSet.get(key).get("exchangeRate"));
					mapDataItem2.put("AMT_DOCCUR", "-"+clearingDiffAmount);
					mapDataItem2.put("AMT_BASE", "0.0000");
					mapDataItem2.put("TAX_AMT", "0.0000");
					/* Set Withholding Tax */
					mapDataItem2.put("WITTHOLDINGTAX", new HashMap<String,String>());
					dataBankItem.add(mapDataItem2);

					/* Set Item 2 :Bank */
					Map mapDataItem3 = new HashMap();
					mapDataItem3.put("ITEMNO_ACC", "002");
					mapDataItem3.put("GL_ACCOUNT", glBankReceive);//Fix
					mapDataItem3.put("VENDOR_NO", null);//"M"+document.getPersonalId()
					mapDataItem3.put("SP_GL_IND", null);
					mapDataItem3.put("BLINE_DATE", sdfYYYYMMDD.format(currentDate));
					mapDataItem3.put("BUSINESSPLACE",fiSet.get(key).get("businessPlace"));
					mapDataItem3.put("J_1TPBUPL", null);
					mapDataItem3.put("ALLOC_NMBR", null);
					mapDataItem3.put("ITEM_TEXT", fiSet.get(key).get("remark"));
					mapDataItem3.put("ACCT_TYPE", "S");
					mapDataItem3.put("TAX_CODE", null);
					mapDataItem3.put("TAX_RATE", null);
					mapDataItem3.put("COSTCENTER", null);
					mapDataItem3.put("PROFIT_CTR", null);
					mapDataItem3.put("WBS_ELEMENT", null);
					mapDataItem3.put("ORDERID", null);
					mapDataItem3.put("CURRENCY_ISO", "THB");
					mapDataItem3.put("EXCH_RATE", fiSet.get(key).get("exchangeRate"));
					mapDataItem3.put("AMT_DOCCUR", clearingDiffAmount);
					mapDataItem3.put("AMT_BASE", "0.0000");
					mapDataItem3.put("TAX_AMT", "0.0000");
					mapDataItem3.put("WITTHOLDINGTAX", new HashMap());
					dataBankItem.add(mapDataItem3);

					mapBankHeader.put("ACCOUNTGL_ITEM", dataBankItem);
					dataDoc.add(mapBankHeader);


				}else if("A<E".equals(clearingCase) && (compareExpenseOverAmountAndWhtAmountValue ==1)){
					
					String clearingDiffAmount = ""+NumberUtil.getBigDecimal(expenseOverAmountAndWhtAmountValue).setScale(2, BigDecimal.ROUND_HALF_EVEN);
					
					/* Set Header */
					Map mapBankHeader = new HashMap();
					mapBankHeader.put("COMP_CODE",  document.getCompanyCode());
					if(document.getPayinDate() !=null){
						mapBankHeader.put("DOC_DATE",   sdfYYYYMMDD.format(document.getPayinDate()));
					}else{
						mapBankHeader.put("DOC_DATE",   sdfYYYYMMDD.format(currentDate));
					}
					
					
					if(document.getPostingDate()!=null){
						mapBankHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(document.getPostingDate()));
						//mapBankHeader.put("FISC_YEAR",  sdfYYYY.format(document.getPostingDate()));
					}else{
						mapBankHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(currentDate));
						//mapBankHeader.put("FISC_YEAR",  sdfYYYY.format(currentDate));
					}
					
					mapBankHeader.put("FIS_PERIOD", null);//12
					mapBankHeader.put("DOC_TYPE",   "KA");
					mapBankHeader.put("REF_DOC_NO", refferenceDoc);//invoice number
					mapBankHeader.put("HEADER_TXT", docAdvanceRef);//ADVANCE_NUMBER//WHEN EXISTS
					String keyMapValueBank = sdfYYYYMMDDHHmmss.format(new Date());
					mapBankHeader.put("EXPENSE_NUMBER", keyMapValueBank);//Expense|item id,
					returnMap.put(keyMapValueBank, document.getId()+"#"+String.valueOf(fiSet.get(key).get("itemId")).substring(1));
					mapBankHeader.put("URL",        url);

					/* Set Item 1 :Employee */
					mapDataItem2 = new HashMap();
					mapDataItem2.put("ITEMNO_ACC", "001");
					mapDataItem2.put("GL_ACCOUNT", null);
					mapDataItem2.put("VENDOR_NO", "M"+document.getPersonalId().substring(1));
					mapDataItem2.put("SP_GL_IND", null);
					mapDataItem2.put("BLINE_DATE", sdfYYYYMMDD.format(currentDate));
					mapDataItem2.put("BUSINESSPLACE", fiSet.get(key).get("businessPlace"));
					mapDataItem2.put("J_1TPBUPL", branch);
					mapDataItem2.put("ALLOC_NMBR", null);
					mapDataItem2.put("ITEM_TEXT", fiSet.get(key).get("remark"));
					mapDataItem2.put("ACCT_TYPE", "K");
					mapDataItem2.put("TAX_CODE", null);
					mapDataItem2.put("TAX_RATE", null);
					mapDataItem2.put("COSTCENTER", null);
					mapDataItem2.put("PROFIT_CTR", null);
					mapDataItem2.put("WBS_ELEMENT", null);
					mapDataItem2.put("ORDERID", null);
					mapDataItem2.put("CURRENCY_ISO", "THB");
					mapDataItem2.put("EXCH_RATE", fiSet.get(key).get("exchangeRate"));
					mapDataItem2.put("AMT_DOCCUR", "-"+clearingDiffAmount);
					mapDataItem2.put("AMT_BASE", "0.0000");
					mapDataItem2.put("TAX_AMT", "0.0000");
					/* Set Withholding Tax */
					mapDataItem2.put("WITTHOLDINGTAX", new HashMap<String,String>());
					dataBankItem.add(mapDataItem2);

					/* Set Item 2 :Bank */
					Map mapDataItem3 = new HashMap();
					mapDataItem3.put("ITEMNO_ACC", "002");
					mapDataItem3.put("GL_ACCOUNT", glBankReceive);//Fix
					mapDataItem3.put("VENDOR_NO", null);//"M"+document.getPersonalId()
					mapDataItem3.put("SP_GL_IND", null);
					mapDataItem3.put("BLINE_DATE", sdfYYYYMMDD.format(currentDate));
					mapDataItem3.put("BUSINESSPLACE",fiSet.get(key).get("businessPlace"));
					mapDataItem3.put("J_1TPBUPL", null);
					mapDataItem3.put("ALLOC_NMBR", null);
					mapDataItem3.put("ITEM_TEXT", fiSet.get(key).get("remark"));
					mapDataItem3.put("ACCT_TYPE", "S");
					mapDataItem3.put("TAX_CODE", null);
					mapDataItem3.put("TAX_RATE", null);
					mapDataItem3.put("COSTCENTER", null);
					mapDataItem3.put("PROFIT_CTR", null);
					mapDataItem3.put("WBS_ELEMENT", null);
					mapDataItem3.put("ORDERID", null);
					mapDataItem3.put("CURRENCY_ISO", "THB");
					mapDataItem3.put("EXCH_RATE", fiSet.get(key).get("exchangeRate"));
					mapDataItem3.put("AMT_DOCCUR", clearingDiffAmount);
					mapDataItem3.put("AMT_BASE", "0.0000");
					mapDataItem3.put("TAX_AMT", "0.0000");
					mapDataItem3.put("WITTHOLDINGTAX", new HashMap());
					dataBankItem.add(mapDataItem3);

					mapBankHeader.put("ACCOUNTGL_ITEM", dataBankItem);
					dataDoc.add(mapBankHeader);


				}
				
				
				isPrintedBankReceive = true;
			}
			
			
			
			

		}
		
		


		/* Set Doc */
		Map headerMap = new HashMap();
		headerMap.put("DOCUMENTHEADER",  dataDoc);
		String jsonData=gson.toJson(headerMap,Map.class);
		LOGGER.info("expense data={}",jsonData);
		
		returnMap.put("jsonData", jsonData);

		return returnMap;
	}
	
	public static Map toSAPStructureBak20180103(Document document,Map param){
		
		Map returnMap = new HashMap();
		
		Date currentDate = new Date();
		List<Map<String,String>> dataDoc = new ArrayList<Map<String,String>>();
		Set<DocumentExpenseItem> documentExpenseItems = document.getDocumentExpense().getDocumentExpenseItems();
		String docAdvanceRef    = (String) param.get("advanceSapDocNumber");
		String advanceDocNumber = (String) param.get("advanceDocNumber");
		String clearingCase     = (String) param.get("clearingCase");
		String glBankReceive    = (String) param.get("glBankReceive");





		Map<String,Map> fiSet = new HashMap();
		Map<String,Map> fiGlSet = new HashMap();
		Map<String,Map> fiWhtSet = new HashMap();
		for(DocumentExpenseItem item:documentExpenseItems){
			String invNo   = String.valueOf(item.getDocNumber());
			if(invNo.trim().length()==0){
				invNo="null";
			}
			String vatCode = String.valueOf(item.getVatCode());
			
			if(vatCode.trim().length()==0){
				vatCode="VX";
			}
			
			if(vatCode.equals("VX")){
				invNo="null";
			}
			String vatCodeValue = String.valueOf(item.getVatCodeValue());
			String glCode = String.valueOf(item.getGlCode());
			
			String key    = invNo+"_"+vatCode;
			String keyGl  = invNo+"_"+vatCode+"|"+glCode+"|"+item.getId();
			String keyWht = String.valueOf(item.getSeller())+"_"+String.valueOf(item.getWhtType())+"_"+String.valueOf(item.getWhtCode());

			Map dataItem = (Map) fiSet.get(key);
			if(dataItem==null){
				dataItem = new HashMap();
			}

			Map dataItemGl = (Map) fiGlSet.get(keyGl);
			if(dataItemGl==null){
				dataItemGl = new HashMap();
			}
			
			Map dataItemWht = (Map) fiWhtSet.get(keyWht);
			if(dataItemWht==null && NumberUtil.getBigDecimal(item.getWhtAmount()).intValue() > 0){
				dataItemWht = new HashMap();
			}

			BigDecimal netAmount      = NumberUtil.getBigDecimal(dataItem.get("netAmount")).add(NumberUtil.getBigDecimal(item.getNetAmount()));
			BigDecimal vatAmount      = NumberUtil.getBigDecimal(dataItem.get("taxAmount")).add(NumberUtil.getBigDecimal(item.getVat()));
			BigDecimal amount         = NumberUtil.getBigDecimal(dataItem.get("amount")).add(NumberUtil.getBigDecimal(item.getAmount()));
			BigDecimal netAmountGl    = NumberUtil.getBigDecimal(dataItemGl.get("netAmount")).add(NumberUtil.getBigDecimal(item.getNetAmount()));
			BigDecimal whtAmount      = NumberUtil.getBigDecimal(dataItem.get("whtAmount")).add(NumberUtil.getBigDecimal(item.getWhtAmount()));
			

			BigDecimal whtAmountForWht      = NumberUtil.getBigDecimal(dataItemWht.get("whtAmount")).add(NumberUtil.getBigDecimal(item.getWhtAmount()));




			String streamId = "";
			if(dataItem.get("itemId")==null){
				streamId = ","+String.valueOf(item.getSeqKey());
			}else{
				streamId = String.valueOf(dataItem.get("itemId"))+","+String.valueOf(item.getSeqKey());
			}

			dataItem.put("amount",amount);
			dataItem.put("netAmount",netAmount);
			dataItem.put("invNo",invNo);
			dataItem.put("vatCode",vatCode);
			dataItem.put("vatCodeValue",vatCodeValue);
			dataItem.put("seller",item.getSeller());
			dataItem.put("address",String.valueOf(item.getAddress()));
			dataItem.put("itemId",streamId);
			dataItem.put("internalOrder",item.getInternalOrder());
			dataItem.put("exchangeRate","1.00000");//item.getExchangeRate() // Fix
			dataItem.put("taxAmount",vatAmount);
			dataItem.put("taxNumber",item.getTaxIDNumber());
			dataItem.put("branch",String.valueOf(item.getBranch()));
			dataItem.put("businessPlace",String.valueOf(item.getBusinessPlace()));
			dataItem.put("remark",String.valueOf(item.getRemark()));
			dataItem.put("whtType",String.valueOf(item.getWhtType()));
			dataItem.put("whtCode",String.valueOf(item.getWhtCode()));
			dataItem.put("whtAmount",whtAmount);
			if(!"null".equals(String.valueOf(item.getWhtGroup())) && !"".equals(String.valueOf(item.getWhtGroup()).trim())){
				dataItem.put("whtGroup",String.valueOf(item.getWhtGroup()));
			}else if(!"null".equals(String.valueOf(item.getWhtType())) && !"".equals(String.valueOf(item.getWhtType()).trim())){
				dataItem.put("whtGroup",String.valueOf(item.getWhtType()).substring(0,1).toUpperCase());
			}
			
			
			dataItem.put("documentDate",item.getDocumentDate());

			fiSet.put(key, dataItem);



			dataItemGl.put("amount",NumberUtil.getBigDecimal(item.getAmount()));
			dataItemGl.put("netAmount",netAmountGl);
			dataItemGl.put("internalOrder",item.getInternalOrder());
			dataItemGl.put("exchangeRate","1.00000");//item.getExchangeRate() // Fix
			dataItemGl.put("seller",item.getSeller());
			dataItemGl.put("branch",String.valueOf(item.getBranch()));
			dataItemGl.put("businessPlace",String.valueOf(item.getBusinessPlace()));
			dataItemGl.put("taxNumber",item.getTaxIDNumber());
			dataItemGl.put("address",String.valueOf(item.getAddress()));
			dataItemGl.put("remark",String.valueOf(item.getRemark()));
			fiGlSet.put(keyGl, dataItemGl);

			

			
			dataItemWht.put("seller",item.getSeller());
			dataItemWht.put("whtType",String.valueOf(item.getWhtType()));
			dataItemWht.put("whtCode",String.valueOf(item.getWhtCode()));
			dataItemWht.put("whtAmount", whtAmountForWht);
			fiWhtSet.put(keyWht, dataItemWht);
			

		}
		LOGGER.info("fiSet={}",gson.toJson(fiSet,Map.class));
		//LOGGER.info("fiGlSet={}",gson.toJson(fiGlSet,Map.class));
		
		
		/* Split Doc FI */
		Map mapHeader =  null;
		for(String key:fiSet.keySet()){
			/* Set Header */
			mapHeader = new HashMap();
			mapHeader.put("COMP_CODE",  document.getCompanyCode());
			mapHeader.put("DOC_DATE",   sdfYYYYMMDD.format(fiSet.get(key).get("documentDate")));
			if(document.getPostingDate()!=null){
				mapHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(document.getPostingDate()));
				//mapHeader.put("FISC_YEAR",  sdfYYYY.format(document.getPostingDate()));
			}else{
				mapHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(currentDate));
				//mapHeader.put("FISC_YEAR",  sdfYYYY.format(currentDate));
			}
			
			
			mapHeader.put("FIS_PERIOD", null);
			mapHeader.put("DOC_TYPE",   "KC");
			
			String refferenceDoc = "-";
			if(!"null".equals(fiSet.get(key).get("invNo")) && !"".equals(fiSet.get(key).get("invNo"))){
				refferenceDoc = ""+fiSet.get(key).get("invNo");
			}
			mapHeader.put("REF_DOC_NO", refferenceDoc);//invoice number
			mapHeader.put("HEADER_TXT", docAdvanceRef);//ADVANCE_NUMBER//WHEN EXISTS
			
			String keyMapValue = sdfYYYYMMDDHHmmss.format(new Date());
			mapHeader.put("EXPENSE_NUMBER", keyMapValue);//Expense|item id,
			returnMap.put(keyMapValue, document.getId()+"#"+String.valueOf(fiSet.get(key).get("itemId")).substring(1));
			
			//mapHeader.put("EXPENSE_NUMBER", document.getId()+"#"+String.valueOf(fiSet.get(key).get("itemId")).substring(1));//Expense|item id,

			String url = param.get("appServer")+"/expense/expenseDetail?doc="+document.getId();
			mapHeader.put("URL",        url);


			/*Setup Item1 */
			String vatCode = String.valueOf(fiSet.get(key).get("vatCode"));
			String vatCodeValue = String.valueOf(fiSet.get(key).get("vatCodeValue"));
			List<Map<String,String>> dataBankItem = new ArrayList<Map<String,String>>();
			List<Map<String,String>> dataItem = new ArrayList<Map<String,String>>();
			int indexItem = 0;
			for(String keyGl:fiGlSet.keySet()){
				Map mapDataItem1 =null;

				if(keyGl.startsWith(key)){
					indexItem++;
					mapDataItem1 = new HashMap();
					mapDataItem1.put("ITEMNO_ACC", String.format("%03d", indexItem));
					mapDataItem1.put("GL_ACCOUNT", keyGl.split("\\|")[1]);
					mapDataItem1.put("VENDOR_NO", null);//"M"+document.getPersonalId().substring(1)
					mapDataItem1.put("BLINE_DATE", null);
					mapDataItem1.put("BUSINESSPLACE", fiGlSet.get(keyGl).get("businessPlace"));
					String branch = "00000";
					if(!"null".equals(fiGlSet.get(keyGl).get("branch")) && !"".equals(fiGlSet.get(keyGl).get("branch"))){
						branch = ""+fiGlSet.get(keyGl).get("branch");
					}
					if("null".equals(vatCode)){
						branch = "NVAT";
						vatCode = null;//"VX";
						vatCodeValue = null;
					}
					mapDataItem1.put("J_1TPBUPL", branch);
					mapDataItem1.put("ALLOC_NMBR", advanceDocNumber);
					mapDataItem1.put("ITEM_TEXT", fiGlSet.get(keyGl).get("remark"));
					mapDataItem1.put("ACCT_TYPE", "S");
					mapDataItem1.put("TAX_CODE", vatCode);
					mapDataItem1.put("TAX_RATE", vatCodeValue);
					mapDataItem1.put("COSTCENTER", document.getCostCenterCode());
					mapDataItem1.put("PROFIT_CTR", null);
					mapDataItem1.put("WBS_ELEMENT", null);
					mapDataItem1.put("ORDERID", fiGlSet.get(keyGl).get("internalOrder"));
					mapDataItem1.put("CURRENCY_ISO", "THB");
					mapDataItem1.put("EXCH_RATE", fiGlSet.get(keyGl).get("exchangeRate"));
					mapDataItem1.put("AMT_DOCCUR", fiGlSet.get(keyGl).get("amount"));
					mapDataItem1.put("AMT_BASE", "0.0000");
					mapDataItem1.put("TAX_AMT", "0.0000");

					

					mapDataItem1.put("WITTHOLDINGTAX", new HashMap());
					dataItem.add(mapDataItem1);

				}

			}

			/* Set Item 2 :Employee */
			indexItem++;
			Map mapDataItem2 = new HashMap();
			mapDataItem2.put("ITEMNO_ACC", String.format("%03d", indexItem));
			mapDataItem2.put("GL_ACCOUNT", null);
			mapDataItem2.put("VENDOR_NO", "M"+document.getPersonalId().substring(1));
			mapDataItem2.put("SP_GL_IND", null);
			mapDataItem2.put("BLINE_DATE", sdfYYYYMMDD.format(currentDate));
			mapDataItem2.put("BUSINESSPLACE", fiSet.get(key).get("businessPlace"));
			
			String branch = "00000";
			if(!"null".equals(fiSet.get(key).get("branch")) && !"".equals(fiSet.get(key).get("branch"))){
				branch = ""+fiSet.get(key).get("branch");
			}
			if("null".equals(vatCode)){
				branch = "NVAT";
			}
			mapDataItem2.put("J_1TPBUPL", branch);
			mapDataItem2.put("ALLOC_NMBR", advanceDocNumber);
			mapDataItem2.put("ITEM_TEXT", fiSet.get(key).get("remark"));
			mapDataItem2.put("ACCT_TYPE", "K");
			mapDataItem2.put("TAX_CODE", vatCode);
			mapDataItem2.put("TAX_RATE", vatCodeValue);
			mapDataItem2.put("COSTCENTER", null);
			mapDataItem2.put("PROFIT_CTR", null);
			mapDataItem2.put("WBS_ELEMENT", null);
			mapDataItem2.put("ORDERID", null);
			mapDataItem2.put("CURRENCY_ISO", "THB");
			mapDataItem2.put("EXCH_RATE", fiSet.get(key).get("exchangeRate"));
			if("XXX".equals(String.valueOf(fiSet.get(key).get("whtGroup")))){
				mapDataItem2.put("AMT_DOCCUR", "-"+NumberUtil.getBigDecimal(fiSet.get(key).get("netAmount")));
			}else{
				mapDataItem2.put("AMT_DOCCUR", "-"+NumberUtil.getBigDecimal(fiSet.get(key).get("netAmount")).add(NumberUtil.getBigDecimal(fiSet.get(key).get("whtAmount"))));
			}
			mapDataItem2.put("AMT_BASE", "0.0000");
			mapDataItem2.put("TAX_AMT", "0.0000");
			
			String whtAmountData = null;
			
			/* Set Withholding Tax */
			Map holdingMap = new HashMap<String,String>();
			if(!"null".equals(String.valueOf(fiSet.get(key).get("whtCode"))) &&
					!"".equals(String.valueOf(fiSet.get(key).get("whtCode")).trim()) &&
					!"null".equals(String.valueOf(fiSet.get(key).get("whtType"))) &&
					!"".equals(String.valueOf(fiSet.get(key).get("whtType")).trim())){
				LOGGER.info("Data whtAmount = |{}|",String.valueOf(fiSet.get(key).get("whtAmount")));
				whtAmountData = String.valueOf(fiSet.get(key).get("whtAmount"));
				holdingMap.put("ITEMNO_ACC", String.format("%03d", indexItem));//
				holdingMap.put("WT_TYPE", String.valueOf(fiSet.get(key).get("whtType")));
				holdingMap.put("WT_CODE",String.valueOf(fiSet.get(key).get("whtCode")));
				holdingMap.put("BAS_AMT_TC", String.valueOf(fiSet.get(key).get("amount")));
				holdingMap.put("MAN_AMT_TC",String.valueOf(fiSet.get(key).get("whtAmount")));
				
				
				Map<String,String> addressMap = TextUtil.getAddressMap(String.valueOf(fiSet.get(key).get("address")));
				mapDataItem2.put("NAME1", fiSet.get(key).get("seller"));
				mapDataItem2.put("NAME2", null);
				mapDataItem2.put("NAME3", null);
				mapDataItem2.put("NAME4", null);
				mapDataItem2.put("PSTLZ", addressMap.get("postCode"));
				mapDataItem2.put("ORT01", addressMap.get("country"));
				mapDataItem2.put("LAND1", "TH");
				mapDataItem2.put("STRAS", addressMap.get("address"));
				mapDataItem2.put("STCD1", fiSet.get(key).get("taxNumber"));
				
			}

			if(!"null".equals(vatCode) &&
					!"V0".equals(vatCode) &&
					!"VX".equals(vatCode) ){
				Map<String,String> addressMap = TextUtil.getAddressMap(String.valueOf(fiSet.get(key).get("address")));
				mapDataItem2.put("NAME1", fiSet.get(key).get("seller"));
				mapDataItem2.put("NAME2", null);
				mapDataItem2.put("NAME3", null);
				mapDataItem2.put("NAME4", null);
				mapDataItem2.put("PSTLZ", addressMap.get("postCode"));
				mapDataItem2.put("ORT01", addressMap.get("country"));
				mapDataItem2.put("LAND1", "TH");
				mapDataItem2.put("STRAS", addressMap.get("address"));
				mapDataItem2.put("STCD1", fiSet.get(key).get("taxNumber"));
			}
			
			
			mapDataItem2.put("WITTHOLDINGTAX", holdingMap);
			dataItem.add(mapDataItem2);





			/* Set Item 3 :Vat */
			if(vatCode != null &&
					!"null".equals(vatCode) &&
					!"V0".equals(vatCode) &&
					!"VX".equals(vatCode) ){
				LOGGER.info("============== Have Vat |{}|",vatCode);
				indexItem++;
				Map mapDataItem3 = new HashMap();
				mapDataItem3.put("ITEMNO_ACC", String.format("%03d", indexItem));
				mapDataItem3.put("GL_ACCOUNT", "124100");//Fix
				mapDataItem3.put("VENDOR_NO", null);//"M"+document.getPersonalId()
				mapDataItem3.put("SP_GL_IND", null);
				mapDataItem3.put("BLINE_DATE", null);
				mapDataItem3.put("BUSINESSPLACE",fiSet.get(key).get("businessPlace"));
				mapDataItem3.put("J_1TPBUPL", branch);
				mapDataItem3.put("ALLOC_NMBR", advanceDocNumber);
				mapDataItem3.put("ITEM_TEXT", fiSet.get(key).get("remark"));
				mapDataItem3.put("ACCT_TYPE", "S");
				mapDataItem3.put("TAX_CODE", vatCode);
				mapDataItem3.put("TAX_RATE", vatCodeValue);
				mapDataItem3.put("COSTCENTER", null);
				mapDataItem3.put("PROFIT_CTR", null);
				mapDataItem3.put("WBS_ELEMENT", null);
				mapDataItem3.put("ORDERID", null);
				mapDataItem3.put("CURRENCY_ISO", "THB");
				mapDataItem3.put("EXCH_RATE", fiSet.get(key).get("exchangeRate"));
				mapDataItem3.put("AMT_DOCCUR", fiSet.get(key).get("taxAmount"));
				mapDataItem3.put("AMT_BASE", fiSet.get(key).get("amount"));
				mapDataItem3.put("TAX_AMT", fiSet.get(key).get("taxAmount"));
				mapDataItem3.put("WITTHOLDINGTAX", new HashMap());
				dataItem.add(mapDataItem3);
			}

			mapHeader.put("ACCOUNTGL_ITEM", dataItem);
			dataDoc.add(mapHeader);


			Double expenseOverAmount = NumberUtil.getBigDecimal((String) param.get("expenseOverAmount")).doubleValue();
			Double whtAmountValue = NumberUtil.getBigDecimal(whtAmountData).doubleValue();
			Double expenseOverAmountAndWhtAmountValue = 0d;
			int compareExpenseOverAmountAndWhtAmountValue = Double.compare(whtAmountValue,expenseOverAmount);
			if(compareExpenseOverAmountAndWhtAmountValue ==1){
				expenseOverAmountAndWhtAmountValue = whtAmountValue - expenseOverAmount;
			}
			if("A>E".equals(clearingCase)){
				
				String clearingDiffAmount = ""+NumberUtil.getBigDecimal(whtAmountData).add(NumberUtil.getBigDecimal((String) param.get("clearingDiffAmount"))).setScale(2, BigDecimal.ROUND_HALF_EVEN);
				
				/* Set Header */
				Map mapBankHeader = new HashMap();
				mapBankHeader.put("COMP_CODE",  document.getCompanyCode());
				if(document.getPayinDate() !=null){
					mapBankHeader.put("DOC_DATE",   sdfYYYYMMDD.format(document.getPayinDate()));
				}else{
					mapBankHeader.put("DOC_DATE",   sdfYYYYMMDD.format(currentDate));
				}
				
				
				if(document.getPostingDate()!=null){
					mapBankHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(document.getPostingDate()));
					//mapBankHeader.put("FISC_YEAR",  sdfYYYY.format(document.getPostingDate()));
				}else{
					mapBankHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(currentDate));
					//mapBankHeader.put("FISC_YEAR",  sdfYYYY.format(currentDate));
				}
				
				mapBankHeader.put("FIS_PERIOD", null);//12
				mapBankHeader.put("DOC_TYPE",   "KA");
				mapBankHeader.put("REF_DOC_NO", refferenceDoc);//invoice number
				mapBankHeader.put("HEADER_TXT", docAdvanceRef);//ADVANCE_NUMBER//WHEN EXISTS
				
				String keyMapValueBank = sdfYYYYMMDDHHmmss.format(new Date());
				mapBankHeader.put("EXPENSE_NUMBER", keyMapValueBank);//Expense|item id,
				returnMap.put(keyMapValueBank, document.getId()+"#"+String.valueOf(fiSet.get(key).get("itemId")).substring(1));
				
				mapBankHeader.put("URL",        url);

				/* Set Item 1 :Employee */
				mapDataItem2 = new HashMap();
				mapDataItem2.put("ITEMNO_ACC", "001");
				mapDataItem2.put("GL_ACCOUNT", null);
				mapDataItem2.put("VENDOR_NO", "M"+document.getPersonalId().substring(1));
				mapDataItem2.put("SP_GL_IND", null);
				mapDataItem2.put("BLINE_DATE", sdfYYYYMMDD.format(currentDate));
				mapDataItem2.put("BUSINESSPLACE", fiSet.get(key).get("businessPlace"));
				mapDataItem2.put("J_1TPBUPL", branch);
				mapDataItem2.put("ALLOC_NMBR", null);
				mapDataItem2.put("ITEM_TEXT", fiSet.get(key).get("remark"));
				mapDataItem2.put("ACCT_TYPE", "K");
				mapDataItem2.put("TAX_CODE", null);
				mapDataItem2.put("TAX_RATE", null);
				mapDataItem2.put("COSTCENTER", null);
				mapDataItem2.put("PROFIT_CTR", null);
				mapDataItem2.put("WBS_ELEMENT", null);
				mapDataItem2.put("ORDERID", null);
				mapDataItem2.put("CURRENCY_ISO", "THB");
				mapDataItem2.put("EXCH_RATE", fiSet.get(key).get("exchangeRate"));
				mapDataItem2.put("AMT_DOCCUR", "-"+clearingDiffAmount);
				mapDataItem2.put("AMT_BASE", "0.0000");
				mapDataItem2.put("TAX_AMT", "0.0000");
				/* Set Withholding Tax */
				mapDataItem2.put("WITTHOLDINGTAX", new HashMap<String,String>());
				dataBankItem.add(mapDataItem2);

				/* Set Item 2 :Bank */
				Map mapDataItem3 = new HashMap();
				mapDataItem3.put("ITEMNO_ACC", "002");
				mapDataItem3.put("GL_ACCOUNT", glBankReceive);//Fix
				mapDataItem3.put("VENDOR_NO", null);//"M"+document.getPersonalId()
				mapDataItem3.put("SP_GL_IND", null);
				mapDataItem3.put("BLINE_DATE", sdfYYYYMMDD.format(currentDate));
				mapDataItem3.put("BUSINESSPLACE",fiSet.get(key).get("businessPlace"));
				mapDataItem3.put("J_1TPBUPL", null);
				mapDataItem3.put("ALLOC_NMBR", null);
				mapDataItem3.put("ITEM_TEXT", fiSet.get(key).get("remark"));
				mapDataItem3.put("ACCT_TYPE", "S");
				mapDataItem3.put("TAX_CODE", null);
				mapDataItem3.put("TAX_RATE", null);
				mapDataItem3.put("COSTCENTER", null);
				mapDataItem3.put("PROFIT_CTR", null);
				mapDataItem3.put("WBS_ELEMENT", null);
				mapDataItem3.put("ORDERID", null);
				mapDataItem3.put("CURRENCY_ISO", "THB");
				mapDataItem3.put("EXCH_RATE", fiSet.get(key).get("exchangeRate"));
				mapDataItem3.put("AMT_DOCCUR", clearingDiffAmount);
				mapDataItem3.put("AMT_BASE", "0.0000");
				mapDataItem3.put("TAX_AMT", "0.0000");
				mapDataItem3.put("WITTHOLDINGTAX", new HashMap());
				dataBankItem.add(mapDataItem3);

				mapBankHeader.put("ACCOUNTGL_ITEM", dataBankItem);
				dataDoc.add(mapBankHeader);


			}else if("A=E".equals(clearingCase) && whtAmountData!=null){
				
				String clearingDiffAmount = ""+NumberUtil.getBigDecimal(whtAmountData).setScale(2, BigDecimal.ROUND_HALF_EVEN);
				
				/* Set Header */
				Map mapBankHeader = new HashMap();
				mapBankHeader.put("COMP_CODE",  document.getCompanyCode());
				if(document.getPayinDate() !=null){
					mapBankHeader.put("DOC_DATE",   sdfYYYYMMDD.format(document.getPayinDate()));
				}else{
					mapBankHeader.put("DOC_DATE",   sdfYYYYMMDD.format(currentDate));
				}
				
				
				if(document.getPostingDate()!=null){
					mapBankHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(document.getPostingDate()));
					//mapBankHeader.put("FISC_YEAR",  sdfYYYY.format(document.getPostingDate()));
				}else{
					mapBankHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(currentDate));
					//mapBankHeader.put("FISC_YEAR",  sdfYYYY.format(currentDate));
				}
				
				mapBankHeader.put("FIS_PERIOD", null);//12
				mapBankHeader.put("DOC_TYPE",   "KA");
				mapBankHeader.put("REF_DOC_NO", refferenceDoc);//invoice number
				mapBankHeader.put("HEADER_TXT", docAdvanceRef);//ADVANCE_NUMBER//WHEN EXISTS
				String keyMapValueBank = sdfYYYYMMDDHHmmss.format(new Date());
				mapBankHeader.put("EXPENSE_NUMBER", keyMapValueBank);//Expense|item id,
				returnMap.put(keyMapValueBank, document.getId()+"#"+String.valueOf(fiSet.get(key).get("itemId")).substring(1));
				
				
				mapBankHeader.put("URL",        url);

				/* Set Item 1 :Employee */
				mapDataItem2 = new HashMap();
				mapDataItem2.put("ITEMNO_ACC", "001");
				mapDataItem2.put("GL_ACCOUNT", null);
				mapDataItem2.put("VENDOR_NO", "M"+document.getPersonalId().substring(1));
				mapDataItem2.put("SP_GL_IND", null);
				mapDataItem2.put("BLINE_DATE", sdfYYYYMMDD.format(currentDate));
				mapDataItem2.put("BUSINESSPLACE", fiSet.get(key).get("businessPlace"));
				mapDataItem2.put("J_1TPBUPL", branch);
				mapDataItem2.put("ALLOC_NMBR", null);
				mapDataItem2.put("ITEM_TEXT", fiSet.get(key).get("remark"));
				mapDataItem2.put("ACCT_TYPE", "K");
				mapDataItem2.put("TAX_CODE", null);
				mapDataItem2.put("TAX_RATE", null);
				mapDataItem2.put("COSTCENTER", null);
				mapDataItem2.put("PROFIT_CTR", null);
				mapDataItem2.put("WBS_ELEMENT", null);
				mapDataItem2.put("ORDERID", null);
				mapDataItem2.put("CURRENCY_ISO", "THB");
				mapDataItem2.put("EXCH_RATE", fiSet.get(key).get("exchangeRate"));
				mapDataItem2.put("AMT_DOCCUR", "-"+clearingDiffAmount);
				mapDataItem2.put("AMT_BASE", "0.0000");
				mapDataItem2.put("TAX_AMT", "0.0000");
				/* Set Withholding Tax */
				mapDataItem2.put("WITTHOLDINGTAX", new HashMap<String,String>());
				dataBankItem.add(mapDataItem2);

				/* Set Item 2 :Bank */
				Map mapDataItem3 = new HashMap();
				mapDataItem3.put("ITEMNO_ACC", "002");
				mapDataItem3.put("GL_ACCOUNT", glBankReceive);//Fix
				mapDataItem3.put("VENDOR_NO", null);//"M"+document.getPersonalId()
				mapDataItem3.put("SP_GL_IND", null);
				mapDataItem3.put("BLINE_DATE", sdfYYYYMMDD.format(currentDate));
				mapDataItem3.put("BUSINESSPLACE",fiSet.get(key).get("businessPlace"));
				mapDataItem3.put("J_1TPBUPL", null);
				mapDataItem3.put("ALLOC_NMBR", null);
				mapDataItem3.put("ITEM_TEXT", fiSet.get(key).get("remark"));
				mapDataItem3.put("ACCT_TYPE", "S");
				mapDataItem3.put("TAX_CODE", null);
				mapDataItem3.put("TAX_RATE", null);
				mapDataItem3.put("COSTCENTER", null);
				mapDataItem3.put("PROFIT_CTR", null);
				mapDataItem3.put("WBS_ELEMENT", null);
				mapDataItem3.put("ORDERID", null);
				mapDataItem3.put("CURRENCY_ISO", "THB");
				mapDataItem3.put("EXCH_RATE", fiSet.get(key).get("exchangeRate"));
				mapDataItem3.put("AMT_DOCCUR", clearingDiffAmount);
				mapDataItem3.put("AMT_BASE", "0.0000");
				mapDataItem3.put("TAX_AMT", "0.0000");
				mapDataItem3.put("WITTHOLDINGTAX", new HashMap());
				dataBankItem.add(mapDataItem3);

				mapBankHeader.put("ACCOUNTGL_ITEM", dataBankItem);
				dataDoc.add(mapBankHeader);


			}else if("A<E".equals(clearingCase) && (compareExpenseOverAmountAndWhtAmountValue ==1)){
				
				String clearingDiffAmount = ""+NumberUtil.getBigDecimal(expenseOverAmountAndWhtAmountValue).setScale(2, BigDecimal.ROUND_HALF_EVEN);
				
				/* Set Header */
				Map mapBankHeader = new HashMap();
				mapBankHeader.put("COMP_CODE",  document.getCompanyCode());
				if(document.getPayinDate() !=null){
					mapBankHeader.put("DOC_DATE",   sdfYYYYMMDD.format(document.getPayinDate()));
				}else{
					mapBankHeader.put("DOC_DATE",   sdfYYYYMMDD.format(currentDate));
				}
				
				
				if(document.getPostingDate()!=null){
					mapBankHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(document.getPostingDate()));
					//mapBankHeader.put("FISC_YEAR",  sdfYYYY.format(document.getPostingDate()));
				}else{
					mapBankHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(currentDate));
					//mapBankHeader.put("FISC_YEAR",  sdfYYYY.format(currentDate));
				}
				
				mapBankHeader.put("FIS_PERIOD", null);//12
				mapBankHeader.put("DOC_TYPE",   "KA");
				mapBankHeader.put("REF_DOC_NO", refferenceDoc);//invoice number
				mapBankHeader.put("HEADER_TXT", docAdvanceRef);//ADVANCE_NUMBER//WHEN EXISTS
				String keyMapValueBank = sdfYYYYMMDDHHmmss.format(new Date());
				mapBankHeader.put("EXPENSE_NUMBER", keyMapValueBank);//Expense|item id,
				returnMap.put(keyMapValueBank, document.getId()+"#"+String.valueOf(fiSet.get(key).get("itemId")).substring(1));
				mapBankHeader.put("URL",        url);

				/* Set Item 1 :Employee */
				mapDataItem2 = new HashMap();
				mapDataItem2.put("ITEMNO_ACC", "001");
				mapDataItem2.put("GL_ACCOUNT", null);
				mapDataItem2.put("VENDOR_NO", "M"+document.getPersonalId().substring(1));
				mapDataItem2.put("SP_GL_IND", null);
				mapDataItem2.put("BLINE_DATE", sdfYYYYMMDD.format(currentDate));
				mapDataItem2.put("BUSINESSPLACE", fiSet.get(key).get("businessPlace"));
				mapDataItem2.put("J_1TPBUPL", branch);
				mapDataItem2.put("ALLOC_NMBR", null);
				mapDataItem2.put("ITEM_TEXT", fiSet.get(key).get("remark"));
				mapDataItem2.put("ACCT_TYPE", "K");
				mapDataItem2.put("TAX_CODE", null);
				mapDataItem2.put("TAX_RATE", null);
				mapDataItem2.put("COSTCENTER", null);
				mapDataItem2.put("PROFIT_CTR", null);
				mapDataItem2.put("WBS_ELEMENT", null);
				mapDataItem2.put("ORDERID", null);
				mapDataItem2.put("CURRENCY_ISO", "THB");
				mapDataItem2.put("EXCH_RATE", fiSet.get(key).get("exchangeRate"));
				mapDataItem2.put("AMT_DOCCUR", "-"+clearingDiffAmount);
				mapDataItem2.put("AMT_BASE", "0.0000");
				mapDataItem2.put("TAX_AMT", "0.0000");
				/* Set Withholding Tax */
				mapDataItem2.put("WITTHOLDINGTAX", new HashMap<String,String>());
				dataBankItem.add(mapDataItem2);

				/* Set Item 2 :Bank */
				Map mapDataItem3 = new HashMap();
				mapDataItem3.put("ITEMNO_ACC", "002");
				mapDataItem3.put("GL_ACCOUNT", glBankReceive);//Fix
				mapDataItem3.put("VENDOR_NO", null);//"M"+document.getPersonalId()
				mapDataItem3.put("SP_GL_IND", null);
				mapDataItem3.put("BLINE_DATE", sdfYYYYMMDD.format(currentDate));
				mapDataItem3.put("BUSINESSPLACE",fiSet.get(key).get("businessPlace"));
				mapDataItem3.put("J_1TPBUPL", null);
				mapDataItem3.put("ALLOC_NMBR", null);
				mapDataItem3.put("ITEM_TEXT", fiSet.get(key).get("remark"));
				mapDataItem3.put("ACCT_TYPE", "S");
				mapDataItem3.put("TAX_CODE", null);
				mapDataItem3.put("TAX_RATE", null);
				mapDataItem3.put("COSTCENTER", null);
				mapDataItem3.put("PROFIT_CTR", null);
				mapDataItem3.put("WBS_ELEMENT", null);
				mapDataItem3.put("ORDERID", null);
				mapDataItem3.put("CURRENCY_ISO", "THB");
				mapDataItem3.put("EXCH_RATE", fiSet.get(key).get("exchangeRate"));
				mapDataItem3.put("AMT_DOCCUR", clearingDiffAmount);
				mapDataItem3.put("AMT_BASE", "0.0000");
				mapDataItem3.put("TAX_AMT", "0.0000");
				mapDataItem3.put("WITTHOLDINGTAX", new HashMap());
				dataBankItem.add(mapDataItem3);

				mapBankHeader.put("ACCOUNTGL_ITEM", dataBankItem);
				dataDoc.add(mapBankHeader);


			}
			
			

		}
		
		


		/* Set Doc */
		Map headerMap = new HashMap();
		headerMap.put("DOCUMENTHEADER",  dataDoc);
		String jsonData=gson.toJson(headerMap,Map.class);
		LOGGER.info("expense data={}",jsonData);
		
		returnMap.put("jsonData", jsonData);

		return returnMap;
	}

	public static String sapReturnJson(Map<String,Map<String,String>> param,Map resultMap){
		String jsonData= null;
		if(param.get("EXP_MESSAGE") instanceof Map){
			Map returnMap = new HashMap();
			String returnType   = String.valueOf(param.get("EXP_MESSAGE").get("TYPE"));
			String returnClass  = String.valueOf(param.get("EXP_MESSAGE").get("ID"));
			String returnNumber = String.valueOf(param.get("EXP_MESSAGE").get("NUMBER"));
			String returnMessage = String.valueOf(param.get("EXP_MESSAGE").get("MESSAGE"));
			returnMap.put("returnType", returnType);
			returnMap.put("returnClass", returnClass);
			returnMap.put("returnNumber", returnNumber);
			returnMap.put("returnMessage", returnMessage);
			String expenseSAPDocNumber = "";
			String expenseDocId = "";
			String expenseItemId = "";
			if("S".equals(returnType)  && returnMessage.indexOf("Document posted successfully") >= 0){
				try {
					expenseSAPDocNumber = String.valueOf(param.get("EXP_MESSAGE").get("MESSAGE").split(":")[1]).trim().split(" ")[1];
					
					String docItem = (String) resultMap.get(String.valueOf(param.get("EXP_MESSAGE").get("MESSAGE").split("\\|")[1]).trim());
					
					expenseDocId  = docItem.trim().split("#")[0];
					expenseItemId = docItem.trim().split("#")[1];
					expenseSAPDocNumber = expenseSAPDocNumber.substring(0, 10);
					if(expenseSAPDocNumber.startsWith("68")){
						returnMap.put("returnType", "B");
					}
				} catch (Exception e) { /* Ignore */}
			}else if("S".equals(returnType)  && returnMessage.indexOf("Clearing document is") >= 0){
				try {
					expenseSAPDocNumber = String.valueOf(param.get("EXP_MESSAGE").get("MESSAGE").split("Clearing document is")[1]).trim();
					returnMap.put("returnType", "C");
					expenseSAPDocNumber = expenseSAPDocNumber.substring(0, 10);
				} catch (Exception e) { /* Ignore */}
			}else if("A".equals(returnType)) {
				returnMap.put("returnType", "E");
			}

			String message = "";
			if(returnMessage.split("\\|").length > 2){
				message += returnMessage.split("\\|")[2];
			}

			returnMap.put("returnMessage", message);

			returnMap.put("sapDocNumber", expenseSAPDocNumber);
			returnMap.put("expenseDocId", expenseDocId);
			returnMap.put("expenseItemId", expenseItemId);

			List<Map<String,String>> returnList = new ArrayList();
			returnList.add(returnMap);
			jsonData=gson.toJson(returnList,List.class);
		}else if(param.get("EXP_MESSAGE") instanceof List){
			List<Map<String,String>> returnList = new ArrayList();
			Map returnMap = null;
			List<Map<String,String>> paramListMap = (List<Map<String, String>>) param.get("EXP_MESSAGE");

			Map uniqeMessage =new HashMap();

			for(Map<String,String> paramObj:paramListMap){
				returnMap = new HashMap();
				String returnType   = String.valueOf(paramObj.get("TYPE"));
				String returnClass  = String.valueOf(paramObj.get("ID"));
				String returnNumber = String.valueOf(paramObj.get("NUMBER"));
				String returnMessage = String.valueOf(paramObj.get("MESSAGE"));
				returnMap.put("returnType", returnType);
				returnMap.put("returnClass", returnClass);
				returnMap.put("returnNumber", returnNumber);
				returnMap.put("returnMessage", returnMessage);
				String expenseSAPDocNumber = "";
				String advanceSAPDocNumber = "";
				String expenseDocId = "";
				String expenseItemId = "";
				//Clearing document is 6800000374
				if("S".equals(returnType) && returnMessage.indexOf("Document posted successfully") >= 0){
					try {
						expenseSAPDocNumber = String.valueOf(paramObj.get("MESSAGE").split(":")[1]).trim().split(" ")[1];
						advanceSAPDocNumber = String.valueOf(paramObj.get("MESSAGE").split("\\|")[0]).trim();
						
						String docItem = (String) resultMap.get(String.valueOf(paramObj.get("MESSAGE").split("\\|")[1]).trim());
						
						expenseDocId = docItem.trim().split("#")[0];
						expenseItemId = docItem.trim().split("#")[1];
						expenseSAPDocNumber = expenseSAPDocNumber.substring(0, 10);
						if(expenseSAPDocNumber.startsWith("68")){
							returnMap.put("returnType", "B");
						}
					} catch (Exception e) { /* Ignore */}
				}else if(("I".equals(returnType) || "S".equals(returnType))  && returnMessage.indexOf("Clearing document is") >= 0){
					try {
						expenseSAPDocNumber = String.valueOf(paramObj.get("MESSAGE").split("Clearing document is")[1]).trim();
						expenseSAPDocNumber = expenseSAPDocNumber.substring(0, 10);
						returnMap.put("returnType", "C");
					} catch (Exception e) { /* Ignore */}
				}else if("A".equals(returnType)) {
					returnMap.put("returnType", "E");
				}


				String message = "";
				if(returnMessage.split("\\|").length > 2){
					message += returnMessage.split("\\|")[2];
				}
				
				if(message.isEmpty()){
					message = returnMessage;
				}

				returnMap.put("returnMessage", message);

				returnMap.put("sapDocNumber", expenseSAPDocNumber);
				returnMap.put("advanceSapDocNumber", advanceSAPDocNumber);
				returnMap.put("expenseDocId", expenseDocId);
				returnMap.put("expenseItemId", expenseItemId);
				if(!"609.0".equals(returnNumber) && !"777.0".equals(returnNumber)){
					if(uniqeMessage.get(returnMessage)==null){
						returnList.add(returnMap);
						uniqeMessage.put(returnMessage, returnMessage);
					}

				}

			}
			jsonData=gson.toJson(returnList,List.class);
		}

		return jsonData;

	}
}
