package com.spt.engine.converter;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.entity.app.Document;



public class AdvanceSAPConverter {
	static final Logger LOGGER = LoggerFactory.getLogger(AdvanceSAPConverter.class);



	private static SimpleDateFormat  sdfYYYYMMDD = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
	private static SimpleDateFormat  sdfYYYY = new SimpleDateFormat("yyyy", Locale.ENGLISH);
	private static SimpleDateFormat  sdfMM = new SimpleDateFormat("MM", Locale.ENGLISH);

	static JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
		public Date deserialize(JsonElement json, Type typeOfT,
								JsonDeserializationContext context) throws JsonParseException {
			return json == null ? null : new Date(json.getAsLong());
		}
	};

	protected static Gson gson = new GsonBuilder().disableHtmlEscaping().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();


	public static String toSAPStructure(Document document,Map param){
		Date currentDate = new Date();
		
		/* Set Header */
		Map mapHeader = new HashMap();
		mapHeader.put("COMP_CODE",  document.getCompanyCode());
		mapHeader.put("DOC_DATE",   sdfYYYYMMDD.format(document.getSendDate()));
		mapHeader.put("PSTNG_DATE", sdfYYYYMMDD.format(currentDate));
		//mapHeader.put("FISC_YEAR",  sdfYYYY.format(currentDate));
		//mapHeader.put("FIS_PERIOD", "12");
		mapHeader.put("DOC_TYPE",   "KR");
		mapHeader.put("REF_DOC_NO", document.getDocNumber());
		mapHeader.put("HEADER_TXT", document.getDocNumber());

		String url = new String();
		if("APP".equals(document.getDocumentType())){
			url = param.get("appServer")+"/approve/viewCreateDocSetDetail?doc="+document.getId();
		}else if("ADV".equals(document.getDocumentType())){
			url = param.get("appServer")+"/advance/advanceDetail?doc="+document.getId();
		}
		mapHeader.put("URL",        url);

	      
		/* Set Detail */
		List<Map<String,String>> dataItem = new ArrayList<Map<String,String>>();
		Map mapData = new HashMap();
		mapData.put("ITEMNO_ACC", "001");
		mapData.put("VENDOR_NO", "M"+document.getPersonalId().substring(1));
		mapData.put("BLINE_DATE", sdfYYYYMMDD.format(document.getSendDate()));
		mapData.put("SP_GL_IND", "E");
		mapData.put("BUSINESSPLACE", String.valueOf(param.get("businessPlace")));
		mapData.put("J_1TPBUPL", "NVAT");
		mapData.put("ALLOC_NMBR", document.getDocNumber());
		mapData.put("ITEM_TEXT", "เงินยืมทดรอง "+document.getDocumentAdvance().getAdvancePurpose());
		mapData.put("ACCT_TYPE", "K");
		mapData.put("CURRENCY_ISO", "THB");
		mapData.put("AMT_DOCCUR", ""+document.getDocumentAdvance().getAmount());
		mapData.put("WITTHOLDINGTAX", new HashMap());
		dataItem.add(mapData);

		Map mapData2 = new HashMap();
		mapData2.put("ITEMNO_ACC", "002");
		mapData2.put("VENDOR_NO", "M"+document.getPersonalId().substring(1));
		mapData2.put("BLINE_DATE",sdfYYYYMMDD.format(document.getSendDate()));
		mapData2.put("BUSINESSPLACE", String.valueOf(param.get("businessPlace")));
		mapData2.put("J_1TPBUPL", "NVAT");
		mapData2.put("ALLOC_NMBR", document.getDocNumber());
		mapData2.put("ITEM_TEXT", "เงินยืมทดรอง "+document.getDocumentAdvance().getAdvancePurpose());
		mapData2.put("ACCT_TYPE", "K");
		mapData2.put("CURRENCY_ISO", "THB");
		mapData2.put("AMT_DOCCUR", "-"+document.getDocumentAdvance().getAmount());
		mapData2.put("WITTHOLDINGTAX", new HashMap());
		dataItem.add(mapData2);

		mapHeader.put("ACCOUNTGL_ITEM", dataItem);

		/* Set Doc */
		Map headerMap = new HashMap();
		headerMap.put("DOCUMENTHEADER",  mapHeader);

		String jsonData=gson.toJson(headerMap,Map.class);
		return jsonData;
	}

	public static String sapReturnJson(Map<String,Map<String,String>> param){
		String jsonData= null;
		if(param.get("EXP_MESSAGE") instanceof Map){
			Map returnMap = new HashMap();
			String returnType   = String.valueOf(param.get("EXP_MESSAGE").get("TYPE"));
			String returnClass  = String.valueOf(param.get("EXP_MESSAGE").get("ID"));
			String returnNumber = String.valueOf(param.get("EXP_MESSAGE").get("NUMBER"));
			String returnMessage = String.valueOf(param.get("EXP_MESSAGE").get("MESSAGE"));
			returnMap.put("returnType", returnType);
			returnMap.put("returnClass", returnClass);
			returnMap.put("returnNumber", returnNumber);
			returnMap.put("returnMessage", returnMessage);
			String advanceSAPDocNumber = "";
			if("S".equals(returnType)){
				try {
					advanceSAPDocNumber = String.valueOf(param.get("EXP_MESSAGE").get("MESSAGE").split(":")[1]).trim().split(" ")[1];
					advanceSAPDocNumber = advanceSAPDocNumber.substring(0, 10);
				} catch (Exception e) { /* Ignore */}
			}else{
				returnMap.put("returnType", "E");
			}

			returnMap.put("sapDocNumber", advanceSAPDocNumber);

			List<Map<String,String>> returnList = new ArrayList();
			returnList.add(returnMap);
			jsonData=gson.toJson(returnList,List.class);
		}else if(param.get("EXP_MESSAGE") instanceof List){
			List<Map<String,String>> returnList = new ArrayList();
			Map returnMap = null;
			List<Map<String,String>> paramListMap = (List<Map<String, String>>) param.get("EXP_MESSAGE");

			Map uniqeMessage =new HashMap();

			for(Map<String,String> paramObj:paramListMap){
				returnMap = new HashMap();
				String returnType   = String.valueOf(paramObj.get("TYPE"));
				String returnClass  = String.valueOf(paramObj.get("ID"));
				String returnNumber = String.valueOf(paramObj.get("NUMBER"));
				String returnMessage = String.valueOf(paramObj.get("MESSAGE"));
				returnMap.put("returnType", returnType);
				returnMap.put("returnClass", returnClass);
				returnMap.put("returnNumber", returnNumber);
				returnMap.put("returnMessage", returnMessage);
				String advanceSAPDocNumber = "";
				if("S".equals(returnType)){
					try {
						advanceSAPDocNumber = String.valueOf(paramObj.get("MESSAGE").split(":")[1]).trim().split(" ")[1];
						advanceSAPDocNumber = advanceSAPDocNumber.substring(0, 10);
					} catch (Exception e) { /* Ignore */}
				}else if("A".equals(returnType)) {
					returnMap.put("returnType", "E");
				}

				returnMap.put("sapDocNumber", advanceSAPDocNumber);
				if(!"609.0".equals(returnNumber)){
					if(uniqeMessage.get(returnMessage)==null){
						returnList.add(returnMap);
						uniqeMessage.put(returnMessage, returnMessage);
					}

				}

			}
			jsonData=gson.toJson(returnList,List.class);
		}

		return jsonData;
	}
}
