package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.spt.engine.entity.general.MasterData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class DocumentAdvance {

    private @Id
    @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version
    @JsonIgnore
    Long version;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
    private String createdBy;
    private String updateBy;
    private String advancePurpose;
    private String requiredReference;
    private Double amount;
    private String bankNumber;
    private String remark;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp startDate;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp endDate;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp dueDate;
    private String externalDocNumber;
    private String externalPaymentDocNumber;
    private String externalClearingDocNumber;
    private String externalCancelDocNumber;
    private String paymentStatus;
    private Double paymentAmount;
    private String flagUnlock;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp valueDate;

    @OneToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Document document;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "documentAdvance")
    private Set<DocumentAdvanceItem> documentAdvanceItems = new HashSet<DocumentAdvanceItem>();
}
