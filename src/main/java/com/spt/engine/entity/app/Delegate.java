package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Delegate {

    private @Id
    @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version
    @JsonIgnore
    Long version;
    private String createdBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private String updateBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;

    private String empUserNameAssignee;
    private String empUserNameAssigner;
    private String empCodeAssignee; /* ุ้มอบอำนาจ */
    private String empCodeAssigner; /*  ผู้รับมอบอำนาจ */
    private String flowType;
    private Double amount;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp startDate;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp endDate;
}
