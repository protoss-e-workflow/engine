package com.spt.engine.entity.app;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class ExpenseTypeFile {

	private @Id @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version @JsonIgnore Long version;
    private String createdBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private String updateBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
    
    private String attachmentTypeCode;
    private Boolean require;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "expenseType")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private ExpenseType expenseType;




    public ExpenseTypeFile(String attachmentTypeCode,Boolean require, ExpenseType expenseType){
        super();
        this.attachmentTypeCode = attachmentTypeCode;
        this.require = require;
        this.expenseType = expenseType;

    }


}
