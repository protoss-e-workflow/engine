package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.spt.engine.entity.general.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class TravelMember {

    private @Id @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version @JsonIgnore Long version;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
    private String updateBy;
    private String createdBy;
    private String memberCode;
    private String memberPersonalId;
    private String memberUser;
    private Boolean flagCar;
    private Boolean flagFlight;
    private Boolean flagBed;
    private Boolean flagBedTwin;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "documentApproveItem")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private DocumentApproveItem documentApproveItem;
}
