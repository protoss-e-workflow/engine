package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.spt.engine.entity.general.MasterData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class HotelBooking {

    private @Id @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version @JsonIgnore Long version;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp startDate;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp endDate;
    private String createdBy;
    private String updateBy;
    private Integer singleRoom;
    private Integer twinRoom;
    private String informationHotel;
    private String otherHotel;

    @OneToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private DocumentApproveItem documentApproveItem;
}
