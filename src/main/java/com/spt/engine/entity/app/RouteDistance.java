package com.spt.engine.entity.app;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class RouteDistance {

	private @Id @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version @JsonIgnore Long version;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
    private String createdBy;
    private String updateBy;
    
    private Double distance;
    
    @OneToOne
    private Location locationFrom;

    @OneToOne
    private Location locationTo;
    
    private Boolean flagActive;

}
