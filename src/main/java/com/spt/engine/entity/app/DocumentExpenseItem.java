package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class DocumentExpenseItem {

    private @Id
    @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version
    @JsonIgnore
    Long version;
    private String createdBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private String updateBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;

    private String internalOrder;
    private String wbs;
    private String assignment;
    private String unit;
    private String remark;
    private String docNumber;
    private String seller;
    private String taxIDNumber;
    private String branch;
    private String address;
    private Double amount;
    private Double baseAmount;
    private Double vat;
    private String vatCode;
    private String whtCode;
    private Double netAmount;
    private Double whtAmount;
    private Double quantity;
    private String postcode;
    private String city;
    private String street;
    private String whtType;
    private String whtGroup;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp documentDate;
    private String glCode;
    private String vatCodeValue;
    private String exchangeRate;
    private String externalDocNumber;
    private String externalPaymentDocNumber;
    private String externalClearingDocNumber;
    private String externalBankReceiveDocNumber;
    private String externalCancelDocNumber;
    private String businessPlace;
    private String paymentStatus;
    private Double paymentAmount;
    private Boolean accommodaionStatus;
    private Long documentId;
    private String seqKey;
    private String profitCenter;
    private Long subExpenseTypeId;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp valueDate;

    @OneToOne
    private ExpenseTypeByCompany expTypeByCompany;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "documentExpenseItem")
    private Set<DocumentExpItemAttachment> docExpItemAttachments = new HashSet<DocumentExpItemAttachment>();

    @OneToOne
    private DocumentExpenseItemDetail docExpItemDetail;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "documentExp")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY) 
    private DocumentExpense documentExp;

    public ExpenseTypeByCompany getExpenseTypeByCompanys(){return this.expTypeByCompany;}

    public Set<DocumentExpItemAttachment> getdocExpItemAttachment(){
        return this.docExpItemAttachments;
    }

}
