package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class InterfaceActiveLog {

    private @Id
    @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version
    @JsonIgnore
    Long version;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private String interfaceChanel;
    private Double amount;
    private String remark;
    private String location;
    private String empId;
    private String costCenter;
    private String withdrawFlag;
    private String docNumber;
    private String compCode;
    private String status;
    private String internalOrder;
    private String vertify;
    private String vertifyDate;
    private String approver;
    private String approverDate;
}
