package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.spt.engine.entity.general.ExpenseFavorite;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class ExpenseTypeByCompany {

    private @Id
    @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version
    @JsonIgnore
    Long version;
    private String createdBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private String updateBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;

    @NonNull
    private String glCode;

    private Boolean favorite;

    private String pa; /* personal area */

    private String psa;

    @OneToOne
    private Company company;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "expenseType")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private ExpenseType expenseType;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "expenseTypeByCompany")
    private Set<ExpenseFavorite> expenseFavorites = new HashSet<ExpenseFavorite>();

    public ExpenseType getExpenseTypes(){return this.expenseType;}

    public Company getCompanys(){return this.company;}




    public ExpenseTypeByCompany(String pa,String psa,String glCode,ExpenseType expenseType){
        this.pa = pa;
        this.psa = psa;
        this.glCode = glCode;
        this.expenseType = expenseType;
    }

}
