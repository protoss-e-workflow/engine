package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;


import java.sql.Timestamp;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class TravelDetail {

    private @Id @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version @JsonIgnore Long version;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp startDate;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp endDate;
    private String createdBy;
    private String updateBy;
    private String startTime;
    private String endTime;
    private String remark;
    private Integer travelDays;
    private String otherOrigin;
    private String otherDestination;

    @OneToOne
    private Location origin;

    @OneToOne
    private Location destination;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "documentApproveItem")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private DocumentApproveItem documentApproveItem;

    public Location getOrigins() {return this.origin;}

    public Location getDestinations() {return this.destination;}
    

}
