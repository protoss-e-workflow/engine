package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.spt.engine.entity.general.MasterData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class DocumentApproveItem {

    private @Id @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version @JsonIgnore Long version;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
    private String createdBy;
    private String updateBy;
    private String approveType;
    private String travelReason;
    private String flagTravelPurposeCal;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "documentApprove")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY) 
    private DocumentApprove documentApprove;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "documentApproveItem")
    private Set<TravelDetail> travelDetails = new HashSet<TravelDetail>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "documentApproveItem")
    private Set<TravelMember> travelMembers = new HashSet<TravelMember>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "documentApproveItem")
    private Set<ExternalMember> externalMembers = new HashSet<ExternalMember>();

    @OneToOne(cascade = CascadeType.ALL)
    private CarBooking carBooking;

    @OneToOne(cascade = CascadeType.ALL)
    private HotelBooking hotelBooking;

    @OneToOne(cascade = CascadeType.ALL)
    private FlightTicket flightTicket;
    
    public String toString() {
    	 return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    public CarBooking getCarBookings() {return this.carBooking;}

    public Set<TravelDetail> getTravelDetail(){
        return this.travelDetails;
    }

    public Set<TravelMember> getTravelMember(){
        return this.travelMembers;
    }

    public Set<ExternalMember> getExternalMember(){
        return this.externalMembers;
    }

//    public HotelBooking getHotelBookings() {return this.hotelBooking;}
//
//    public FlightTicket getFlightTickets() {return this.flightTicket;}
}
