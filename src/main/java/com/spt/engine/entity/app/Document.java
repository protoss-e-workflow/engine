package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.spt.engine.entity.general.MasterData;
import com.spt.engine.entity.general.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by milkii on 8/15/17.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Document {

    private @Id @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version @JsonIgnore Long version;
    private String createdBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private String updateBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
    
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp sendDate;
    @Column(unique = true)
    private String docNumber;
    private String documentType;
    private String documentStatus;
    private String requester;
    private String tmpDocNumber;
    private String titleDescription;
    private Double totalAmount;
    private String approveType;
    private String departmentCode;
    private String companyCode;
    private String psa;
    private String processId;
    private String docFlow;
    private String costCenterCode;
    private String userNameMember;
    private String personalId;
    private Double payinAmount;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp payinDate;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp postingDate;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp interfaceOutDate;
    private Double advanceAmount;
    private String verifyStatus;
    private String verifyRemark;
    private String verifyDescription;
    private String verifyName;
    private String ccMailAddress;
    private String requesterName;
    private String requesterNameEn;
    private String externalDocReference;
    private String externalDocChanel;


    @OneToOne
    private CostCenter costCenter;

    @OneToOne
    private Request request;

    @OneToOne(cascade = CascadeType.ALL)
    private DocumentApprove documentApprove;

    @OneToOne(cascade = CascadeType.ALL)
    private DocumentAdvance documentAdvance;

    @OneToOne(cascade = CascadeType.ALL)
    private DocumentExpense documentExpense;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "document")
    private Set<DocumentReference> documentReferences = new HashSet<DocumentReference>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "document")
    private Set<DocumentAttachment> documentAttachment = new HashSet<DocumentAttachment>();

    public CostCenter getCostCenters() {
        return this.costCenter;
    }

    public Request getRequests() {
        return this.request;
    }

    public Set<DocumentReference> getDocumentReference(){
        return this.documentReferences;
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
