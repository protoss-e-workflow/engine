package com.spt.engine.entity.app;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;


@Data
@Entity
@NoArgsConstructor 
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class EmailTransaction {
	
	private @Id @GeneratedValue(strategy=GenerationType.TABLE)Long id;
	private @Version @JsonIgnore Long version;
	private String createdBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
	private String updateBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
	
	private String fromAddress;
	private String toAddress;
	private String ccAddress;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp sendDate;
	private String documentNumber;
	private String actionEvent;
	private String subActionEvent;
	
	public EmailTransaction(String fromAddress, String toAddress, String ccAddress, Timestamp sendDate,
			String documentNumber, String actionEvent, String subActionEvent) {
		super();
		this.fromAddress = fromAddress;
		this.toAddress = toAddress;
		this.ccAddress = ccAddress;
		this.sendDate = sendDate;
		this.documentNumber = documentNumber;
		this.actionEvent = actionEvent;
		this.subActionEvent = subActionEvent;
	}
	
	
	
}
