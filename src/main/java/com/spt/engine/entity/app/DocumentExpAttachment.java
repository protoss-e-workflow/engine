package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.spt.engine.entity.general.MasterData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class DocumentExpAttachment {

    private @Id
    @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version
    @JsonIgnore
    Long version;
    private String createdBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private String updateBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
    private String attachmentTypeCode;
    private String fileName;
    private String realFileName;
    private String uploadTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "documentExp")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private DocumentExpense documentExp;

    @OneToOne
    private ExpenseTypeByCompany expTypeByCompanys;
}
