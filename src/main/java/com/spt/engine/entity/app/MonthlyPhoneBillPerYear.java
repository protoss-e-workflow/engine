package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class MonthlyPhoneBillPerYear {

    private @Id
    @GeneratedValue(strategy= GenerationType.TABLE)Long id;

    private @Version
    @JsonIgnore
    Long version;
    private String createdBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private String updateBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;

    private String userName;
    private String year;
    private Double amountJan;
    private Double amountFeb;
    private Double amountMar;
    private Double amountApr;
    private Double amountMay;
    private Double amountJun;
    private Double amountJul;
    private Double amountAug;
    private Double amountSep;
    private Double amountOct;
    private Double amountNov;
    private Double amountDec;

}
