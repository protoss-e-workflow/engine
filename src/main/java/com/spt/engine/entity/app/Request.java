package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.spt.engine.entity.general.MasterData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Request {

    private @Id
    @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version
    @JsonIgnore
    Long version;
    private String createdBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private String updateBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp requestDate;
    @Column(unique = true)
    private String requestNumber;  /* processId */
    private String requestTypeCode;
    private String requestStatusCode;
    private String nextApprover;
    private String docFlow;
    private String paidStatus;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp lastActionTime;

    @OneToOne
    private Document document;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "request",orphanRemoval=true)
    private Set<RequestApprover> requestApprovers = new HashSet<RequestApprover>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "request")
    private Set<RequestMessage> requestMessages = new HashSet<RequestMessage>();

}
