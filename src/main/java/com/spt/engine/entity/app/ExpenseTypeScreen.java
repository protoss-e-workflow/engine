package com.spt.engine.entity.app;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class ExpenseTypeScreen {

	private @Id @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version @JsonIgnore Long version;
    private String createdBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private String updateBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
    
    private String nameTH;
    private String nameENG;
    private String type;
    private String structureField;
    private Boolean require;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "expenseType")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private ExpenseType expenseType;

	public ExpenseTypeScreen(String nameTH, String nameENG, String type, String structureField, Boolean require,
			ExpenseType expenseType) {
		super();
		this.nameTH = nameTH;
		this.nameENG = nameENG;
		this.type = type;
		this.structureField = structureField;
		this.require = require;
		this.expenseType = expenseType;
	}
    
    
    
}
