package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class FlowConfigure {

    private @Id
    @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version
    @JsonIgnore
    Long version;
    private String createdBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private String updateBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
    private String typeCode;
    private String code;
    private String description;
    
    private String stateVerify;
    private String stateAprove;
    private String stateAdmin;
    private String stateHr;
    private String stateAccount;
    private String stateFinance;
    
    private Integer sequenceVerify;
    private Integer sequenceAprove;
    private Integer sequenceAdmin;
    private Integer sequenceHr;
    private Integer sequenceAccount;
    private Integer sequenceFinance;
    
    
	public FlowConfigure(String typeCode,String code, String description, String stateVerify, String stateAprove, String stateAdmin,
			String stateHr, String stateAccount, String stateFinance, Integer sequenceVerify, Integer sequenceAprove,
			Integer sequenceAdmin, Integer sequenceHr, Integer sequenceAccount, Integer sequenceFinance) {
		super();
		this.typeCode = typeCode;
		this.code = code;
		this.description = description;
		this.stateVerify = stateVerify;
		this.stateAprove = stateAprove;
		this.stateAdmin = stateAdmin;
		this.stateHr = stateHr;
		this.stateAccount = stateAccount;
		this.stateFinance = stateFinance;
		this.sequenceVerify = sequenceVerify;
		this.sequenceAprove = sequenceAprove;
		this.sequenceAdmin = sequenceAdmin;
		this.sequenceHr = sequenceHr;
		this.sequenceAccount = sequenceAccount;
		this.sequenceFinance = sequenceFinance;
	}
    
    
    

    
    
    
    
    
}
