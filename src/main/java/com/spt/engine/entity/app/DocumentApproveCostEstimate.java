package com.spt.engine.entity.app;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class DocumentApproveCostEstimate {

    private @Id @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version @JsonIgnore Long version;
    private String createdBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss") Timestamp createdDate;
    private String updateBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;

    private String remark;
    private String gl;
    private String expenseId;
    private String descriptionTh;
    private String descriptionEn;
    private BigDecimal amount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "documentApprove")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private DocumentApprove documentApprove;


}
