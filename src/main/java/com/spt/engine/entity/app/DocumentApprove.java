package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by milkii on 8/15/17.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class DocumentApprove {

    private @Id @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version @JsonIgnore Long version;
    private String createdBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private String updateBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
    private String remark;
    
    @OneToOne
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Document document;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "documentApprove")
    private Set<DocumentApproveItem> documentApproveItems = new HashSet<DocumentApproveItem>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "documentApprove")
    private Set<DocumentApproveCostEstimate> documentApproveEstimate = new HashSet<DocumentApproveCostEstimate>();

    public Set<DocumentApproveItem> getDocumentApproveItem(){
        return this.documentApproveItems;
    }
    public Set<DocumentApproveCostEstimate> getDocumentApproveCostEstimate(){ return this.documentApproveEstimate; }
}
