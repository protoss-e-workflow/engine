package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class ExpenseType {

    private @Id @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version @JsonIgnore Long version;
    private String createdBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private String updateBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
    private String code;
    private String description;
    private String engDescription;
    private Boolean favorite;
    private String screenType;
    private String fixCode;
    private String flowType;

    private String headOfficeGL;
    private String factoryGL;
    private String wbs;

    
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "expenseType")
    private Set<ExpenseTypeByCompany> expTypeByCompanies = new HashSet<ExpenseTypeByCompany>();
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "expenseType")
    private Set<Reimburse> reimburses = new HashSet<Reimburse>();
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "expenseType")
    private Set<ExpenseTypeScreen> expenseTypeScreens = new HashSet<ExpenseTypeScreen>();
   
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "expenseType")
    private Set<ExpenseTypeReference> expenseTypeReferences = new HashSet<ExpenseTypeReference>();
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "expenseType")
    private Set<ExpenseTypeFile> expenseTypeFiles = new HashSet<ExpenseTypeFile>();

    public Set<ExpenseTypeScreen> getExpenseTypeScreen(){return this.expenseTypeScreens;}

    public Set<ExpenseTypeFile> getExpenseTypeFile(){return this.expenseTypeFiles;}

    public Set<ExpenseTypeReference> getExpenseTypeReference(){return this.expenseTypeReferences;}

    
	public ExpenseType(String code, String description, Boolean favorite, String screenType, String fixCode,
			String flowType) {
		super();
		this.code = code;
		this.description = description;
		this.favorite = favorite;
		this.screenType = screenType;
		this.fixCode = fixCode;
		this.flowType = flowType;
	}
	
	public ExpenseType(String headOfficeGL,String factoryGL,String code, String description, Boolean favorite, String screenType, String fixCode,
			String flowType) {
		super();
		this.headOfficeGL = headOfficeGL;
		this.factoryGL = factoryGL;
		this.code = code;
		this.description = description;
		this.favorite = favorite;
		this.screenType = screenType;
		this.fixCode = fixCode;
		this.flowType = flowType;
	}

}
