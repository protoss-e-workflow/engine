package com.spt.engine.entity.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class DocumentExpenseItemDetail {

    private @Id
    @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version
    @JsonIgnore
    Long version;
    private String createdBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private String updateBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;

    private String employee;
    private String receiver;
    private String receiverCompany;
    private String description;
    private String hotelCode; /* code from master hotel */
    private String place1;
    private String placeOther1;
    private String place2;
    private String placeOther2;
    private String subjectName;
    private String country;
    private String time1;
    private String time2;
    private String carLicence;
    private String carLicenceOther;
    private String liter;
    private String month;
    private String phoneNumber;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp date1;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp date2;

    private String variable1;
    private String variable2;
    private String variable3;
    private String variable4;
    private String variable5;
    private String variable6;
    private String variable7;
    private String variable8;
    private String variable9;
    private String variable10;
    private String variable11;
    private String variable12;
    private String variable13;
    private String variable14;
    private String variable15;
    private String variable16;
    private String variable17;
    private String variable18;
    private String variable19;
    private String variable20;


    @OneToOne
    private DocumentExpenseItem docExpenseItem;
}
