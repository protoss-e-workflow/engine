package com.spt.engine.entity.general;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Formula;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@Entity
@NoArgsConstructor 
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
@Table(name="app_role")
public class Role {
	
	
	private @Id @GeneratedValue(strategy=GenerationType.TABLE)Long id;
	private @Version @JsonIgnore Long version;
	private String createdBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
	private String updateBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
	
	
	private @Formula("CONCAT('ROLE_',role_name)") String role;
	private @Column(unique=true) String roleName;
	private Boolean flagActive;
	
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
    private Set<User> user = new HashSet<User>();
	

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
    private Set<Menu> menu = new HashSet<Menu>();
	
}
