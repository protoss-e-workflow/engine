package com.spt.engine.entity.general;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;


public abstract class BaseModel{

	private String createdBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
	private String updateBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
	
	
}
