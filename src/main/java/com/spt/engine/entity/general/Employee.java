package com.spt.engine.entity.general;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.spt.engine.entity.app.Company;
import com.spt.engine.entity.app.Location;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Employee {

    private @Id
    @GeneratedValue(strategy= GenerationType.TABLE)Long id;
    private @Version
    @JsonIgnore
    Long version;
    private String createdBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
    private String updateBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
    private String empCode;
    private String empName;
    private String empLastName;
    private String empLevel;
    private String bankNumber;
    private String empUsername;
    
    @OneToOne
    private Company company;

    public Company getCompanys(){return this.company;}
    
    @OneToOne
    private Location location;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "employee")
    private Set<ExpenseFavorite> expenseFavorites = new HashSet<ExpenseFavorite>();
}
