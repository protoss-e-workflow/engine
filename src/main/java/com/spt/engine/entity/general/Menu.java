package com.spt.engine.entity.general;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Formula;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@Entity
@NoArgsConstructor 
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
@Table(name="app_menu")
public class Menu {
	

	
	private @Id @GeneratedValue(strategy=GenerationType.TABLE) Long id;
	private @Version @JsonIgnore Long version;
	private String createdBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
	private String updateBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
	
	private Boolean flagActive;
	private Boolean flagHasChild;
	private String  programPath;// For check authorization
	private String  urlLink;
	private String  icon;
	private String  code;
	private Long    parent;
	private Integer sequence;
	
	private @Formula("CONCAT(name_lang1,' ',name_lang2,' ',name_lang3,' ',name_lang4,' ',name_lang5,' ',name_lang6,' ',name_lang7,' ',name_lang8)") String name;
	
	private String nameLang1;
	private String nameLang2;
	private String nameLang3;
	private String nameLang4;
	private String nameLang5;
	private String nameLang6;
	private String nameLang7;
	private String nameLang8;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "menuRole", joinColumns = {
			@JoinColumn(name = "menu_id", nullable = false, updatable = false) },
			inverseJoinColumns = { @JoinColumn(name = "role_id",nullable = false, updatable = false) })
    private Set<Role> roles = new HashSet<Role>();
	
	public Set<Role> getPrivilege(){
		return this.roles;
	}

	public Menu(String code,String icon, String urlLink, String programPath, Integer sequence,Boolean flagHasChild, 
			Long parent,  String nameLang1, String nameLang2) {
		this.flagHasChild = flagHasChild;
		this.programPath = programPath;
		this.urlLink = urlLink;
		this.icon = icon;
		this.code = code;
		this.parent = parent;
		this.sequence = sequence;
		this.nameLang1 = nameLang1;
		this.nameLang2 = nameLang2;
	}
	
	
	
}
