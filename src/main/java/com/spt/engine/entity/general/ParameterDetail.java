package com.spt.engine.entity.general;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@Entity
@NoArgsConstructor 
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class ParameterDetail {

	public ParameterDetail(String code, String variable1, String variable2, String variable3,String description) {
		this.code = code;
		this.variable1 = variable1;
		this.variable2 = variable2;
		this.variable3 = variable3;
	}

	private @Id @GeneratedValue(strategy=GenerationType.TABLE) Long id;
	private @Version @JsonIgnore Long version;
	private String createdBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createdDate;
	private String updateBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
	
	private String code;
	private String name;
	private String description;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parameter")
    private Parameter parameter;
	
	
	private String variable1;
	private String variable2;
	private String variable3;
	private String variable4;
	private String variable5;
	private String variable6;
	private String variable7;
	private String variable8;
	private String variable9;
	private String variable10;
	
	
}
