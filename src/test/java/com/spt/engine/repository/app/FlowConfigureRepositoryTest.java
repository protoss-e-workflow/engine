package com.spt.engine.repository.app;
/**
 * 	Create by SiriradC. 2017.07.17
 * 	Test Process Calculate PerDiem 
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.FlowConfigure;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class FlowConfigureRepositoryTest {

	static final Logger LOGGER = LoggerFactory.getLogger(FlowConfigureRepositoryTest.class);
	
	@Autowired
	FlowConfigureRepository flowConfigureRepository;
	
	@Test
	public void findByStateVerifyAndStateAproveAndStateAdminAndStateHrAndStateAccountAndStateFinance_FullApproveAdvanceNoHRAdmin(){
		String typeCode     = "EXP";
		String stateVerify  = "Y";
		String stateAprove  = "Y";
		String stateAdmin   = "N";
		String stateHr      = "N";
		String stateAccount = "Y";
		String stateFinance = "Y";
		FlowConfigure flow  = flowConfigureRepository.findByTypeCodeAndStateVerifyAndStateAproveAndStateAdminAndStateHrAndStateAccountAndStateFinance(typeCode,stateVerify, stateAprove, stateAdmin, stateHr, stateAccount, stateFinance);
		assertEquals(flow.getCode(),"MPG:DocFlowExpense:0.99-DocFlowExpense.FullApproveAdvanceNoHRAdmin");
	}
	
	public void findByStateVerifyAndStateAproveAndStateAdminAndStateHrAndStateAccountAndStateFinance_FullApproveAdvance(){
		String typeCode     = "EXP";
		String stateVerify  = "Y";
		String stateAprove  = "Y";
		String stateAdmin   = "Y";
		String stateHr      = "Y";
		String stateAccount = "Y";
		String stateFinance = "Y";
		FlowConfigure flow = flowConfigureRepository.findByTypeCodeAndStateVerifyAndStateAproveAndStateAdminAndStateHrAndStateAccountAndStateFinance(typeCode,stateVerify, stateAprove, stateAdmin, stateHr, stateAccount, stateFinance);
		assertEquals(flow.getCode(),"MPG:DocFlowExpense:0.99-DocFlowExpense.FullApproveAdvance");
	}
	
}
