package com.spt.engine.repository.app;
/**
 * 	Create by SiriradC. 2017.07.17
 * 	Test Process Calculate PerDiem 
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.spt.engine.ConstantVariable;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class PerDiemRepositoryTest {

	static final Logger LOGGER = LoggerFactory.getLogger(PerDiemRepositoryTest.class);
	
	public static final String  MITPHOL_SUGAR_LOCATION		= "0001"; //"โรงงานน้ำตาลมิตรผล(ด่านช้าง)";
	public static final String  SINGBURI_SUGAR_LOCATION		= "0002" ;//"โรงงานน้ำตาลสิงห์บุรี";
	public static final String  RISK_LOCATION				= "0006"; //"จังหวัดนราธิวาส";
	public static final String  EU_LOCATION					= "0012"; //"สหรัฐอเมริกา";
	public static final String  SA_LOCATION					= "0017"; //"นิวซีแลนด์";
	public static final String  OT_LOCATION					= "0019";//"กลุ่มประเทศอื่นๆ";
	public static final String  MM_LOCATION					= "0021"; //"สหภาพพม่า";
	
	@Autowired
	PerDiemProcessCalculateRepository perDiemProcessCalculateRepository;
	
	@Test
	public void processCalculatePerDiem_CaseOneDayLessthan24HourAndStartDateMorethan30MinuteOperationMitphol(){
		LOGGER.info("----------- START CASE 1 ------------");
		/* initial Data */
		Long empLevel 			= 8l;
		String startDate 		= "01/07/2017";
		String startTime		= "07:50:00";
		String endDate			= "01/07/2017";
		String endTime			= "19:20:00";
		String location			= MITPHOL_SUGAR_LOCATION;
		Integer rateType		= ConstantVariable.PER_DIEM_MITPHOL_RATE;
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();
		resultPerDiemRate.put(ConstantVariable.START_DATE_KEY, startDate);
		resultPerDiemRate.put(ConstantVariable.START_TIME_KEY, startTime);
		resultPerDiemRate.put(ConstantVariable.END_DATE_KEY, endDate);
		resultPerDiemRate.put(ConstantVariable.END_TIME_KEY, endTime);
		resultPerDiemRate.put(ConstantVariable.RATE_TYPE_KEY, rateType);
		resultPerDiemRate.put(ConstantVariable.LOCATION_KEY, location);
		
		data.add(resultPerDiemRate);
		
		/* call method calculate PerDiem Process */
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemDomestic(empLevel, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.NON_TAX_KEY), new BigDecimal(157.50).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.TAX_KEY), new BigDecimal(0.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.SUMMARY_KEY), new BigDecimal(157.50).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		
		LOGGER.info("----------- END CASE 1 ------------");
	}
	
	@Test
	public void processCalculatePerDiem_CaseOneDayLessthan24HourAndEndDateMorethan30MinuteOperationMitphol(){
		LOGGER.info("----------- START CASE 2 ------------");
		/* initial Data */
		Long empLevel 			= 8l;
		String startDate 		= "01/07/2017";
		String startTime		= "07:00:00";
		String endDate			= "01/07/2017";
		String endTime			= "19:50:00";
		String location			= MITPHOL_SUGAR_LOCATION;
		Integer rateType		= ConstantVariable.PER_DIEM_MITPHOL_RATE;
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();
		resultPerDiemRate.put(ConstantVariable.START_DATE_KEY, startDate);
		resultPerDiemRate.put(ConstantVariable.START_TIME_KEY, startTime);
		resultPerDiemRate.put(ConstantVariable.END_DATE_KEY, endDate);
		resultPerDiemRate.put(ConstantVariable.END_TIME_KEY, endTime);
		resultPerDiemRate.put(ConstantVariable.RATE_TYPE_KEY, rateType);
		resultPerDiemRate.put(ConstantVariable.LOCATION_KEY, location);
		
		data.add(resultPerDiemRate);
		
		/* call method calculate PerDiem Process */
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemDomestic(empLevel, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.NON_TAX_KEY), new BigDecimal(210.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.TAX_KEY), new BigDecimal(0.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.SUMMARY_KEY), new BigDecimal(210.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		
		LOGGER.info("----------- END CASE 2 ------------");
	}
	
	@Test
	public void processCalculatePerDiem_CaseOneDayEquals24HourOperationMitphol(){
		LOGGER.info("----------- START CASE 3 ------------");
		/* initial Data */
		Long empLevel 			= 8l;
		String startDate 		= "01/07/2017";
		String startTime		= "07:00:00";
		String endDate			= "02/07/2017";
		String endTime			= "07:00:00";
		String location			= MITPHOL_SUGAR_LOCATION;
		Integer rateType		= ConstantVariable.PER_DIEM_MITPHOL_RATE;
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();
		
		resultPerDiemRate.put(ConstantVariable.START_DATE_KEY, startDate);
		resultPerDiemRate.put(ConstantVariable.START_TIME_KEY, startTime);
		resultPerDiemRate.put(ConstantVariable.END_DATE_KEY, endDate);
		resultPerDiemRate.put(ConstantVariable.END_TIME_KEY, endTime);
		resultPerDiemRate.put(ConstantVariable.RATE_TYPE_KEY, rateType);
		resultPerDiemRate.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemRate);
		
		/* call method calculate PerDiem Process */
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemDomestic(empLevel, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.NON_TAX_KEY), new BigDecimal(210.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.TAX_KEY), new BigDecimal(0.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.SUMMARY_KEY), new BigDecimal(210.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		
		LOGGER.info("----------- END CASE 3 ------------");
	}
	
	@Test
	public void processCalculatePerDiem_CaseOneDayLessthan24HoursOperationNotMitphol(){
		LOGGER.info("----------- START CASE 4 ------------");
		/* initial Data */
		Long empLevel 			= 8l;
		String startDate 		= "01/07/2017";
		String startTime		= "07:00:00";
		String endDate			= "01/07/2017";
		String endTime			= "17:00:00";
		String location			= SINGBURI_SUGAR_LOCATION;
		Integer rateType		= ConstantVariable.PER_DIEM_FULL_RATE;
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();

		resultPerDiemRate.put(ConstantVariable.START_DATE_KEY, startDate);
		resultPerDiemRate.put(ConstantVariable.START_TIME_KEY, startTime);
		resultPerDiemRate.put(ConstantVariable.END_DATE_KEY, endDate);
		resultPerDiemRate.put(ConstantVariable.END_TIME_KEY, endTime);
		resultPerDiemRate.put(ConstantVariable.RATE_TYPE_KEY, rateType);
		resultPerDiemRate.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemRate);
		
		
		/* call method calculate PerDiem Process */
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemDomestic(empLevel, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.NON_TAX_KEY), new BigDecimal(225.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.TAX_KEY), new BigDecimal(0.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.SUMMARY_KEY), new BigDecimal(225.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		LOGGER.info("----------- END CASE 4 ------------");
	}
	
	@Test
	public void processCalculatePerDiem_CaseOneDayEquals24HoursOperationNotMitphol(){
		LOGGER.info("----------- START CASE 5 ------------");
		/* initial Data */
		Long empLevel 			= 8l;
		String startDate 		= "01/07/2017";
		String startTime		= "07:00:00";
		String endDate			= "02/07/2017";
		String endTime			= "07:00:00";
		String location			= SINGBURI_SUGAR_LOCATION;
		Integer rateType		= ConstantVariable.PER_DIEM_FULL_RATE;
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();
		
		resultPerDiemRate.put(ConstantVariable.START_DATE_KEY, startDate);
		resultPerDiemRate.put(ConstantVariable.START_TIME_KEY, startTime);
		resultPerDiemRate.put(ConstantVariable.END_DATE_KEY, endDate);
		resultPerDiemRate.put(ConstantVariable.END_TIME_KEY, endTime);
		resultPerDiemRate.put(ConstantVariable.RATE_TYPE_KEY, rateType);
		resultPerDiemRate.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemRate);
		
		/* call method calculate PerDiem Process */
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemDomestic(empLevel, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.NON_TAX_KEY), new BigDecimal(240.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.TAX_KEY), new BigDecimal(60.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.SUMMARY_KEY), new BigDecimal(300.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		
		LOGGER.info("----------- END CASE 5 ------------");
	}
	
	@Test
	public void processCalculatePerDiem_CaseOneDayEquals24HoursOperation2Location(){
		LOGGER.info("----------- START CASE 6 ------------");
		/* initial Data */
		Long empLevel 			= 8l;
		String startDate1 		= "01/07/2017";
		String startTime1		= "07:00:00";
		String endDate1			= "01/07/2017";
		String endTime1			= "17:00:00";
		String location1		= SINGBURI_SUGAR_LOCATION;
		Integer rateType1		= ConstantVariable.PER_DIEM_FULL_RATE;
		
		String startDate2 		= "01/07/2017";
		String startTime2		= "17:00:00";
		String endDate2			= "02/07/2017";
		String endTime2			= "19:00:00";
		String location2		= MITPHOL_SUGAR_LOCATION;
		Integer rateType2		= ConstantVariable.PER_DIEM_MITPHOL_RATE;
		
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();	
		
		Map<String,Object> resultPerDiemData = new HashMap<>();
		resultPerDiemData.put(ConstantVariable.START_DATE_KEY, startDate1);
		resultPerDiemData.put(ConstantVariable.START_TIME_KEY, startTime1);
		resultPerDiemData.put(ConstantVariable.END_DATE_KEY, endDate1);
		resultPerDiemData.put(ConstantVariable.END_TIME_KEY, endTime1);
		resultPerDiemData.put(ConstantVariable.RATE_TYPE_KEY, rateType1);
		resultPerDiemData.put(ConstantVariable.LOCATION_KEY, location1);
		data.add(resultPerDiemData);
		
		Map<String,Object> resultPerDiemData1 = new HashMap<>();
		resultPerDiemData1.put(ConstantVariable.START_DATE_KEY, startDate2);
		resultPerDiemData1.put(ConstantVariable.START_TIME_KEY, startTime2);
		resultPerDiemData1.put(ConstantVariable.END_DATE_KEY, endDate2);
		resultPerDiemData1.put(ConstantVariable.END_TIME_KEY, endTime2);
		resultPerDiemData1.put(ConstantVariable.RATE_TYPE_KEY, rateType2);
		resultPerDiemData1.put(ConstantVariable.LOCATION_KEY, location2);
		data.add(resultPerDiemData1);
		
		/* call method calculate PerDiem Process */		
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemDomestic(empLevel, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.NON_TAX_KEY), new BigDecimal(397.50).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.TAX_KEY), new BigDecimal(7.50).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.SUMMARY_KEY), new BigDecimal(405.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		
		LOGGER.info("----------- END CASE 6 ------------");
	}
	
	@Test
	public void processCalculatePerDiem_CaseOneDayEquals24TimeDiscontHoursOperation2Location(){
		LOGGER.info("----------- START CASE 7 ------------");
		/* initial Data */
		Long empLevel 			= 8l;
		String startDate1 		= "01/07/2017";
		String startTime1		= "07:00:00";
		String endDate1			= "01/07/2017";
		String endTime1			= "17:00:00";
		String location1		= SINGBURI_SUGAR_LOCATION;
		Integer rateType1		= ConstantVariable.PER_DIEM_FULL_RATE;
		
		String startDate2 		= "01/07/2017";
		String startTime2		= "20:00:00";
		String endDate2			= "02/07/2017";
		String endTime2			= "19:00:00";
		String location2		= MITPHOL_SUGAR_LOCATION;
		Integer rateType2		= ConstantVariable.PER_DIEM_MITPHOL_RATE;
		
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();	
		
		Map<String,Object> resultPerDiemData = new HashMap<>();
		resultPerDiemData.put(ConstantVariable.START_DATE_KEY, startDate1);
		resultPerDiemData.put(ConstantVariable.START_TIME_KEY, startTime1);
		resultPerDiemData.put(ConstantVariable.END_DATE_KEY, endDate1);
		resultPerDiemData.put(ConstantVariable.END_TIME_KEY, endTime1);
		resultPerDiemData.put(ConstantVariable.RATE_TYPE_KEY, rateType1);
		resultPerDiemData.put(ConstantVariable.LOCATION_KEY, location1);
		data.add(resultPerDiemData);
		
		Map<String,Object> resultPerDiemData1 = new HashMap<>();
		resultPerDiemData1.put(ConstantVariable.START_DATE_KEY, startDate2);
		resultPerDiemData1.put(ConstantVariable.START_TIME_KEY, startTime2);
		resultPerDiemData1.put(ConstantVariable.END_DATE_KEY, endDate2);
		resultPerDiemData1.put(ConstantVariable.END_TIME_KEY, endTime2);
		resultPerDiemData1.put(ConstantVariable.RATE_TYPE_KEY, rateType2);
		resultPerDiemData1.put(ConstantVariable.LOCATION_KEY, location2);
		data.add(resultPerDiemData1);
		
		/* call method calculate PerDiem Process */		
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemDomestic(empLevel, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.NON_TAX_KEY), new BigDecimal(397.50).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.TAX_KEY), new BigDecimal(7.50).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.SUMMARY_KEY), new BigDecimal(405.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		
		LOGGER.info("----------- END CASE 7 ------------");
	}
	
	@Test
	public void processCalculatePerDiem_CaseWorkingLongRunLastDayMorethan8HoursOnMitpholLocation(){
		LOGGER.info("----------- START CASE 8 ------------");
		/* initial Data */
		Long empLevel 			= 8l;
		String startDate 		= "01/07/2017";
		String startTime		= "07:00:00";
		String endDate			= "05/07/2017";
		String endTime			= "16:00:00";
		String location			= MITPHOL_SUGAR_LOCATION;
		Integer rateType		= ConstantVariable.PER_DIEM_MITPHOL_RATE;
		
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();	
		
		Map<String,Object> resultPerDiemData = new HashMap<>();
		resultPerDiemData.put(ConstantVariable.START_DATE_KEY, startDate);
		resultPerDiemData.put(ConstantVariable.START_TIME_KEY, startTime);
		resultPerDiemData.put(ConstantVariable.END_DATE_KEY, endDate);
		resultPerDiemData.put(ConstantVariable.END_TIME_KEY, endTime);
		resultPerDiemData.put(ConstantVariable.RATE_TYPE_KEY, rateType);
		resultPerDiemData.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemData);
		
		/* call method calculate PerDiem Process */		
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemDomestic(empLevel, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.NON_TAX_KEY), new BigDecimal(1080).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.TAX_KEY), new BigDecimal(67.50).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.SUMMARY_KEY), new BigDecimal(1147.50).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		
		LOGGER.info("----------- END CASE 8 ------------");
	}
	
	@Test
	public void processCalculatePerDiem_CaseWorkingLongRunLastDayLessthan8HoursOnMitpholLocation(){
		LOGGER.info("----------- START CASE 9 ------------");
		/* initial Data */
		Long empLevel 			= 8l;
		String startDate 		= "01/07/2017";
		String startTime		= "07:00:00";
		String endDate			= "05/07/2017";
		String endTime			= "12:00:00";
		String location			= MITPHOL_SUGAR_LOCATION;
		Integer rateType		= ConstantVariable.PER_DIEM_MITPHOL_RATE;
		
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();	
		
		Map<String,Object> resultPerDiemData = new HashMap<>();
		resultPerDiemData.put(ConstantVariable.START_DATE_KEY, startDate);
		resultPerDiemData.put(ConstantVariable.START_TIME_KEY, startTime);
		resultPerDiemData.put(ConstantVariable.END_DATE_KEY, endDate);
		resultPerDiemData.put(ConstantVariable.END_TIME_KEY, endTime);
		resultPerDiemData.put(ConstantVariable.RATE_TYPE_KEY, rateType);
		resultPerDiemData.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemData);
		
		/* call method calculate PerDiem Process */		
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemDomestic(empLevel, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.NON_TAX_KEY), new BigDecimal(945.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.TAX_KEY), new BigDecimal(0.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.SUMMARY_KEY), new BigDecimal(945.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		
		LOGGER.info("----------- END CASE 9 ------------");
	}
	
	@Test
	public void processCalculatePerDiem_CaseWorkingLongRunLastDayMorethan8HoursMorethan5Days(){
		LOGGER.info("----------- START CASE 10 ------------");
		/* initial Data */
		Long empLevel 			= 8l;
		String startDate 		= "01/07/2017";
		String startTime		= "07:00:00";
		String endDate			= "10/07/2017";
		String endTime			= "16:00:00";
		String location			= MITPHOL_SUGAR_LOCATION;
		Integer rateType		= ConstantVariable.PER_DIEM_MITPHOL_RATE;
		
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();	
		
		Map<String,Object> resultPerDiemData = new HashMap<>();
		resultPerDiemData.put(ConstantVariable.START_DATE_KEY, startDate);
		resultPerDiemData.put(ConstantVariable.START_TIME_KEY, startTime);
		resultPerDiemData.put(ConstantVariable.END_DATE_KEY, endDate);
		resultPerDiemData.put(ConstantVariable.END_TIME_KEY, endTime);
		resultPerDiemData.put(ConstantVariable.RATE_TYPE_KEY, rateType);
		resultPerDiemData.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemData);
		
		/* call method calculate PerDiem Process */		
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemDomestic(empLevel, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.NON_TAX_KEY), new BigDecimal(2280.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.TAX_KEY), new BigDecimal(667.50).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.SUMMARY_KEY), new BigDecimal(2947.50).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		
		LOGGER.info("----------- END CASE 10 ------------");
	}
	
	@Test
	public void processCalculatePerDiem_CaseWorkingRiskLocationOneDays(){
		LOGGER.info("----------- START CASE 11 ------------");
		
		/* initial Data */
		Long empLevel 			= 8l;
		String startDate 		= "01/07/2017";
		String startTime		= "07:00:00";
		String endDate			= "01/07/2017";
		String endTime			= "20:00:00";
		String location			= RISK_LOCATION;
		Integer rateType		= ConstantVariable.PER_DIEM_MITPHOL_RATE;
		
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();	
		
		Map<String,Object> resultPerDiemData = new HashMap<>();
		resultPerDiemData.put(ConstantVariable.START_DATE_KEY, startDate);
		resultPerDiemData.put(ConstantVariable.START_TIME_KEY, startTime);
		resultPerDiemData.put(ConstantVariable.END_DATE_KEY, endDate);
		resultPerDiemData.put(ConstantVariable.END_TIME_KEY, endTime);
		resultPerDiemData.put(ConstantVariable.RATE_TYPE_KEY, rateType);
		resultPerDiemData.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemData);
		
		/* call method calculate PerDiem Process */		
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemDomestic(empLevel, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.NON_TAX_KEY), new BigDecimal(240.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.TAX_KEY), new BigDecimal(170.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.SUMMARY_KEY), new BigDecimal(410.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		
		LOGGER.info("----------- END CASE 11 ------------");
	}
	
	@Test
	public void processCalculatePerDiem_CaseWorkingRiskLocationMorethanOneDaysLessthan5Days(){
		LOGGER.info("----------- START CASE 12 ------------");
		
		/* initial Data */
		Long empLevel 			= 8l;
		String startDate 		= "01/07/2017";
		String startTime		= "07:00:00";
		String endDate			= "03/07/2017";
		String endTime			= "13:00:00";
		String location			= RISK_LOCATION;
		Integer rateType		= ConstantVariable.PER_DIEM_MITPHOL_RATE;
		
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();	
		
		Map<String,Object> resultPerDiemData = new HashMap<>();
		resultPerDiemData.put(ConstantVariable.START_DATE_KEY, startDate);
		resultPerDiemData.put(ConstantVariable.START_TIME_KEY, startTime);
		resultPerDiemData.put(ConstantVariable.END_DATE_KEY, endDate);
		resultPerDiemData.put(ConstantVariable.END_TIME_KEY, endTime);
		resultPerDiemData.put(ConstantVariable.RATE_TYPE_KEY, rateType);
		resultPerDiemData.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemData);
		
		/* call method calculate PerDiem Process */		
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemDomestic(empLevel, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.NON_TAX_KEY), new BigDecimal(720.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.TAX_KEY), new BigDecimal(405.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.SUMMARY_KEY), new BigDecimal(1125.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		
		LOGGER.info("----------- END CASE 12 ------------");
	}
	
	@Test
	public void processCalculatePerDiem_CaseWorkingRiskLocationMorethanOneDays(){
		LOGGER.info("----------- START CASE 13 ------------");
		
		/* initial Data */
		Long empLevel 			= 8l;
		String startDate 		= "01/07/2017";
		String startTime		= "07:00:00";
		String endDate			= "06/07/2017";
		String endTime			= "20:00:00";
		String location			= RISK_LOCATION;
		Integer rateType		= ConstantVariable.PER_DIEM_MITPHOL_RATE;
		
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();	
		
		Map<String,Object> resultPerDiemData = new HashMap<>();
		resultPerDiemData.put(ConstantVariable.START_DATE_KEY, startDate);
		resultPerDiemData.put(ConstantVariable.START_TIME_KEY, startTime);
		resultPerDiemData.put(ConstantVariable.END_DATE_KEY, endDate);
		resultPerDiemData.put(ConstantVariable.END_TIME_KEY, endTime);
		resultPerDiemData.put(ConstantVariable.RATE_TYPE_KEY, rateType);
		resultPerDiemData.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemData);
		
		/* call method calculate PerDiem Process */		
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemDomestic(empLevel, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.NON_TAX_KEY), new BigDecimal(1440.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.TAX_KEY), new BigDecimal(1320.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.SUMMARY_KEY), new BigDecimal(2760.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		
		LOGGER.info("----------- END CASE 13 ------------");
	}
	
	@Test
	public void processCalculatePerDiem_CaseWorkingRiskLocationOneMonth(){
		LOGGER.info("----------- START CASE 14 ------------");
		
		/* initial Data */
		Long empLevel 			= 8l;
		String startDate 		= "01/07/2017";
		String startTime		= "07:00:00";
		String endDate			= "31/07/2017";
		String endTime			= "12:00:00";
		String location			= RISK_LOCATION;
		Integer rateType		= ConstantVariable.PER_DIEM_MITPHOL_RATE;
		
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();	
		
		Map<String,Object> resultPerDiemData = new HashMap<>();
		resultPerDiemData.put(ConstantVariable.START_DATE_KEY, startDate);
		resultPerDiemData.put(ConstantVariable.START_TIME_KEY, startTime);
		resultPerDiemData.put(ConstantVariable.END_DATE_KEY, endDate);
		resultPerDiemData.put(ConstantVariable.END_TIME_KEY, endTime);
		resultPerDiemData.put(ConstantVariable.RATE_TYPE_KEY, rateType);
		resultPerDiemData.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemData);
		
		/* call method calculate PerDiem Process */		
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemDomestic(empLevel, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.NON_TAX_KEY), new BigDecimal(7440.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.TAX_KEY), new BigDecimal(9065.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		assertEquals(resultPerDiemRate.get(ConstantVariable.SUMMARY_KEY), new BigDecimal(23005.00).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING));
		
		LOGGER.info("----------- END CASE 14 ------------");
	}
	
	
	@Test
	public void processCalculatePerDiem_CaseLocationZoneUSA(){
		LOGGER.info("----------- START CASE 15 ------------");
		
		/* initial Data */
		Long empLevel 			= 7l;
		BigDecimal sellingRate	= new BigDecimal(35.8022);
		String location			= EU_LOCATION;
		
		String startDate 		= "01/07/2017";
		String startTime		= "23:50:00";
		String endDate			= "02/07/2017";
		String endTime			= "23:50:00";
		Integer rateType		= ConstantVariable.PER_DIEM_THIRTY_RATE;
		
		String startDate1 		= "02/07/2017";
		String startTime1		= "23:50:00";
		String endDate1			= "03/07/2017";
		String endTime1			= "23:50:00";
		Integer rateType1		= ConstantVariable.PER_DIEM_SEVENTY_RATE;
		
		String startDate2 		= "03/07/2017";
		String startTime2		= "23:50:00";
		String endDate2			= "04/07/2017";
		String endTime2			= "23:50:00";
		Integer rateType2		= ConstantVariable.PER_DIEM_SEVENTY_RATE;
		
		String startDate3 		= "04/07/2017";
		String startTime3		= "23:50:00";
		String endDate3			= "05/07/2017";
		String endTime3			= "23:50:00";
		Integer rateType3		= ConstantVariable.PER_DIEM_THIRTY_RATE;
		
		String startDate4 		= "05/07/2017";
		String startTime4		= "23:50:00";
		String endDate4			= "06/07/2017";
		String endTime4			= "13:20:00";
		Integer rateType4		= ConstantVariable.PER_DIEM_FULL_RATE;
		
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();	
		
		Map<String,Object> resultPerDiemData = new HashMap<>();
		resultPerDiemData.put(ConstantVariable.START_DATE_KEY, startDate);
		resultPerDiemData.put(ConstantVariable.START_TIME_KEY, startTime);
		resultPerDiemData.put(ConstantVariable.END_DATE_KEY, endDate);
		resultPerDiemData.put(ConstantVariable.END_TIME_KEY, endTime);
		resultPerDiemData.put(ConstantVariable.RATE_TYPE_KEY, rateType);
		resultPerDiemData.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemData);
		
		Map<String,Object> resultPerDiemData1 = new HashMap<>();
		resultPerDiemData1.put(ConstantVariable.START_DATE_KEY, startDate1);
		resultPerDiemData1.put(ConstantVariable.START_TIME_KEY, startTime1);
		resultPerDiemData1.put(ConstantVariable.END_DATE_KEY, endDate1);
		resultPerDiemData1.put(ConstantVariable.END_TIME_KEY, endTime1);
		resultPerDiemData1.put(ConstantVariable.RATE_TYPE_KEY, rateType1);
		resultPerDiemData1.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemData1);
		
		Map<String,Object> resultPerDiemData2 = new HashMap<>();
		resultPerDiemData2.put(ConstantVariable.START_DATE_KEY, startDate2);
		resultPerDiemData2.put(ConstantVariable.START_TIME_KEY, startTime2);
		resultPerDiemData2.put(ConstantVariable.END_DATE_KEY, endDate2);
		resultPerDiemData2.put(ConstantVariable.END_TIME_KEY, endTime2);
		resultPerDiemData2.put(ConstantVariable.RATE_TYPE_KEY, rateType2);
		resultPerDiemData2.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemData2);
		
		Map<String,Object> resultPerDiemData3 = new HashMap<>();
		resultPerDiemData3.put(ConstantVariable.START_DATE_KEY, startDate3);
		resultPerDiemData3.put(ConstantVariable.START_TIME_KEY, startTime3);
		resultPerDiemData3.put(ConstantVariable.END_DATE_KEY, endDate3);
		resultPerDiemData3.put(ConstantVariable.END_TIME_KEY, endTime3);
		resultPerDiemData3.put(ConstantVariable.RATE_TYPE_KEY, rateType3);
		resultPerDiemData3.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemData3);
		
		Map<String,Object> resultPerDiemData4 = new HashMap<>();
		resultPerDiemData4.put(ConstantVariable.START_DATE_KEY, startDate4);
		resultPerDiemData4.put(ConstantVariable.START_TIME_KEY, startTime4);
		resultPerDiemData4.put(ConstantVariable.END_DATE_KEY, endDate4);
		resultPerDiemData4.put(ConstantVariable.END_TIME_KEY, endTime4);
		resultPerDiemData4.put(ConstantVariable.RATE_TYPE_KEY, rateType4);
		resultPerDiemData4.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemData4);
		
		/* call method calculate PerDiem Process */		
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemForeign(empLevel,sellingRate, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.USD_KEY), new BigDecimal(179.38).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING,BigDecimal.ROUND_HALF_EVEN));
		assertEquals(resultPerDiemRate.get(ConstantVariable.BAHT_KEY), new BigDecimal(6422.20).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING,BigDecimal.ROUND_HALF_EVEN));
		
		LOGGER.info("----------- END CASE 15 ------------");
	}
	
	@Test
	public void processCalculatePerDiem_CaseLocationZoneSA(){
		LOGGER.info("----------- START CASE 16 ------------");
		
		/* initial Data */
		Long empLevel 			= 7l;
		BigDecimal sellingRate	= new BigDecimal(35.8022);
		String location			= EU_LOCATION;
		
		String startDate 		= "01/07/2017";
		String startTime		= "23:50:00";
		String endDate			= "04/07/2017";
		String endTime			= "13:50:00";
		Integer rateType		= ConstantVariable.PER_DIEM_THIRTY_RATE;
		
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();	
		
		Map<String,Object> resultPerDiemData = new HashMap<>();
		resultPerDiemData.put(ConstantVariable.START_DATE_KEY, startDate);
		resultPerDiemData.put(ConstantVariable.START_TIME_KEY, startTime);
		resultPerDiemData.put(ConstantVariable.END_DATE_KEY, endDate);
		resultPerDiemData.put(ConstantVariable.END_TIME_KEY, endTime);
		resultPerDiemData.put(ConstantVariable.RATE_TYPE_KEY, rateType);
		resultPerDiemData.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemData);
		
		
		/* call method calculate PerDiem Process */		
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemForeign(empLevel,sellingRate, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.USD_KEY), new BigDecimal(54.25).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING,BigDecimal.ROUND_HALF_EVEN));
		assertEquals(resultPerDiemRate.get(ConstantVariable.BAHT_KEY), new BigDecimal(1942.27).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING,BigDecimal.ROUND_HALF_EVEN));
		
		LOGGER.info("----------- END CASE 16 ------------");
	}
	
	@Test
	public void processCalculatePerDiem_CaseLocationZoneSAToZoneMM(){
		LOGGER.info("----------- START CASE 17 ------------");
		
		/* initial Data */
		Long empLevel 			= 7l;
		BigDecimal sellingRate	= new BigDecimal(35.8022);
		
		String startDate 		= "01/07/2017";
		String startTime		= "23:50:00";
		String endDate			= "04/07/2017";
		String endTime			= "13:50:00";
		Integer rateType		= ConstantVariable.PER_DIEM_THIRTY_RATE;
		String location			= EU_LOCATION;
		
		String startDate1 		= "04/07/2017";
		String startTime1		= "13:50:00";
		String endDate1			= "10/07/2017";
		String endTime1			= "23:20:00";
		Integer rateType1		= ConstantVariable.PER_DIEM_SEVENTY_RATE;
		String location1		= MM_LOCATION;
		
		Map<String,Object> resultPerDiemRate = new HashMap<>();
		List<Map<String,Object>> data = new ArrayList<>();	
		
		Map<String,Object> resultPerDiemData = new HashMap<>();
		resultPerDiemData.put(ConstantVariable.START_DATE_KEY, startDate);
		resultPerDiemData.put(ConstantVariable.START_TIME_KEY, startTime);
		resultPerDiemData.put(ConstantVariable.END_DATE_KEY, endDate);
		resultPerDiemData.put(ConstantVariable.END_TIME_KEY, endTime);
		resultPerDiemData.put(ConstantVariable.RATE_TYPE_KEY, rateType);
		resultPerDiemData.put(ConstantVariable.LOCATION_KEY, location);
		data.add(resultPerDiemData);
		
		Map<String,Object> resultPerDiemData1 = new HashMap<>();
		resultPerDiemData1.put(ConstantVariable.START_DATE_KEY, startDate1);
		resultPerDiemData1.put(ConstantVariable.START_TIME_KEY, startTime1);
		resultPerDiemData1.put(ConstantVariable.END_DATE_KEY, endDate1);
		resultPerDiemData1.put(ConstantVariable.END_TIME_KEY, endTime1);
		resultPerDiemData1.put(ConstantVariable.RATE_TYPE_KEY, rateType1);
		resultPerDiemData1.put(ConstantVariable.LOCATION_KEY, location1);
		data.add(resultPerDiemData1);
		
		
		/* call method calculate PerDiem Process */		
		resultPerDiemRate = perDiemProcessCalculateRepository.processCalculatePerDiemForeign(empLevel,sellingRate, data);
		
		/* validate result */
		assertNotNull(resultPerDiemRate);
		assertEquals(resultPerDiemRate.get(ConstantVariable.USD_KEY), new BigDecimal(188.56).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING,BigDecimal.ROUND_HALF_EVEN));
		assertEquals(resultPerDiemRate.get(ConstantVariable.BAHT_KEY), new BigDecimal(6750.87).setScale(ConstantVariable.SCALE_2_DIGIT_ROUDING,BigDecimal.ROUND_HALF_EVEN));
		
		LOGGER.info("----------- END CASE 17 ------------");
	}
	
}
