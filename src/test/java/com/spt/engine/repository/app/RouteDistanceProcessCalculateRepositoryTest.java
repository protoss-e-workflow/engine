package com.spt.engine.repository.app;
/**
 * 	Create by SiriradC. 2017.07.17
 * 	Test Process Calculate PerDiem 
 */

import com.spt.engine.ConstantVariable;
import com.spt.engine.entity.app.Location;
import com.spt.engine.entity.app.RouteDistance;
import com.spt.engine.entity.general.MasterData;
import com.spt.engine.entity.general.MasterDataDetail;
import com.spt.engine.repository.general.MasterDataDetailRepository;
import com.spt.engine.repository.general.MasterDataRepository;
import org.apache.commons.lang.math.NumberUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class RouteDistanceProcessCalculateRepositoryTest {

	static final Logger LOGGER = LoggerFactory.getLogger(RouteDistanceProcessCalculateRepositoryTest.class);
	
 		@Autowired
		RouteDistanceProcessCalculateRepository routeDistanceProcessCalculateRepository;



		@Autowired
		LocationRepository locationRepository;

		@Autowired
		MasterDataDetailRepository masterDataDetailRepository;

		@Autowired
		MasterDataRepository masterDataRepository;
	
	@Test
	public void processCalculateDistance_checkProcessMethod_MapData3Range_ShouldBe_CorrectMappingKeyValue_Case1(){
		LOGGER.info("----------- START CASE 1 ------------");
		/* initial Data */


		MasterData  masterDataItem =  masterDataRepository.findByCode(ConstantVariable.MASTER_DATA_MILEAGE_CODE);
		            assertNotNull(masterDataItem);


		Set<MasterDataDetail>	   item =  masterDataDetailRepository.findByOrgCodeOrderByVariable1Asc("1010");
		                           assertNotNull(item);

		for (MasterDataDetail o : item) {
			assertEquals(o.getOrgCode(),masterDataDetailRepository.findOne(o.getId()).getOrgCode());
			assertEquals(o.getName(),masterDataDetailRepository.findOne(o.getId()).getName());

		}



		List<Map<String,Object>>   mapData = routeDistanceProcessCalculateRepository.convertMasterDataDistanceToMapData(item);
							       assertNotNull(mapData);


							       assertEquals(mapData.get(0).get("rate"),"50");
							       assertEquals(mapData.get(0).get("value"),"8");
							       assertEquals(mapData.get(1).get("rate"),"80");
							       assertEquals(mapData.get(1).get("value"),"3");
							       assertEquals(mapData.get(2).get("rate"),"OtherRate");
							       assertEquals(mapData.get(2).get("value"),"1");



		LOGGER.info("----------- END CASE 1 ------------");
	}
	@Test
	public void processCalculateDistance_checkProcessMethod_MapData1Range_ShouldBe_CorrectMappingKeyValue_Case2(){
		LOGGER.info("----------- START CASE 2 ------------");
		/* initial Data */


		MasterData  masterDataItem = new MasterData();
		masterDataItem = masterDataRepository.findByCode(ConstantVariable.MASTER_DATA_MILEAGE_CODE);
		assertNotNull(masterDataItem);

		Set<MasterDataDetail>	   item =  masterDataDetailRepository.findByOrgCodeOrderByVariable1Asc("10000");
		assertNotNull(item);

		for (MasterDataDetail o : item) {
			assertEquals(o.getOrgCode(),masterDataDetailRepository.findOne(o.getId()).getOrgCode());
			assertEquals(o.getName(),masterDataDetailRepository.findOne(o.getId()).getName());

		}



		List<Map<String,Object>>   mapData = routeDistanceProcessCalculateRepository.convertMasterDataDistanceToMapData(item);
		assertNotNull(mapData);


		assertEquals(mapData.get(0).get("rate"),"OtherRate");
		assertEquals(mapData.get(0).get("value"),"9");




		LOGGER.info("----------- END CASE 2 ------------");
	}
	@Test
	public void processCalculateDistance_Case_DistanceZero_And_ScopeDistance_Have_ThreeScope(){
		LOGGER.info("----------- START CASE3 ------------");


		//  ========================= Review Data ======================

				//This Case Distance =0 Km and Rate Distance//
//		                 0-50  : 8
//		                 51-80 : 3
//		                 81+   : 1

      //  ========================= Review Data ======================


				/* initial Data */
		String masterDataCode = "M013";
		String masterDataDetailOrgCode = "1010";
		String locationCode1 = "0005";
		String locationCode2 = "0006";
		String codeCompany = "1010";


		Location location1 = locationRepository.findByCode(locationCode1);
		assertNotNull(location1);
		assertEquals(location1.getCode(),"0005");
//		assertEquals(location1.getDescription(),"จังหวัดยะลา");
		assertEquals(location1.getLocationType(),"D");

		Location location2 = locationRepository.findByCode(locationCode2);
		assertNotNull(location2);
		assertEquals(location2.getCode(),"0006");
//		assertEquals(location2.getDescription(),"จังหวัดนราธิวาส");
		assertEquals(location2.getLocationType(),"D");

		MasterData masterData =masterDataRepository.findByCode(masterDataCode);
		assertNotNull(masterData);
		assertEquals(masterData.getCode(),masterDataRepository.findOne(masterData.getId()).getCode());
		assertEquals(masterData.getName(),masterDataRepository.findOne(masterData.getId()).getName());

		Set<MasterDataDetail> setItem = masterDataDetailRepository.findByOrgCodeOrderByVariable1Asc(masterDataDetailOrgCode);
		assertNotNull(setItem);

		for(MasterDataDetail o : setItem){

			assertEquals(o.getOrgCode(),masterDataDetailRepository.findOne(o.getId()).getOrgCode());
			assertEquals(o.getName(),masterDataDetailRepository.findOne(o.getId()).getName());
		}


		Double result = routeDistanceProcessCalculateRepository.processCalculateTotalAmountDistanceRoute(location1.getId(),location2.getId(),codeCompany);
		assertNotNull(result);
		assertEquals(result.toString(),"0.0");


		LOGGER.info("----------- END CASE 3 ------------");
	}
	@Test
	public void processCalculateDistance_CaseDistance_One_AndScopeDistance_Have_ThreeScope(){
		LOGGER.info("----------- START CASE4 ------------");

		//  ========================= Review Data ======================

		//This Case Distance =1 Km and Rate Distance//
//		                 0-50  : 8
//		                 51-80 : 3
//		                 81+   : 1

		//  ========================= Review Data ======================


				/* initial Data */
		String masterDataCode = "M013";
		String masterDataDetailOrgCode = "1010";
		String locationCode1 = "0007";
		String locationCode2 = "0008";
		String codeCompany = "1010";


		Location location1 = locationRepository.findByCode(locationCode1);
		assertNotNull(location1);
		assertEquals(location1.getCode(),"0007");
//		assertEquals(location1.getDescription(),"อำเภอสะบ้าย้อย จังหวัดสงขลา");
		assertEquals(location1.getLocationType(),"D");

		Location location2 = locationRepository.findByCode(locationCode2);
		assertNotNull(location2);
		assertEquals(location2.getCode(),"0008");
//		assertEquals(location2.getDescription(),"อำเภอเทพา จังหวัดสงขลา");
		assertEquals(location2.getLocationType(),"D");

		MasterData masterData =masterDataRepository.findByCode(masterDataCode);
		assertNotNull(masterData);
		assertEquals(masterData.getCode(),masterDataRepository.findOne(masterData.getId()).getCode());
		assertEquals(masterData.getName(),masterDataRepository.findOne(masterData.getId()).getName());

		Set<MasterDataDetail> setItem = masterDataDetailRepository.findByOrgCodeOrderByVariable1Asc(masterDataDetailOrgCode);
		assertNotNull(setItem);

		for(MasterDataDetail o : setItem){

			assertEquals(o.getOrgCode(),masterDataDetailRepository.findOne(o.getId()).getOrgCode());
			assertEquals(o.getName(),masterDataDetailRepository.findOne(o.getId()).getName());
		}


		Double result = routeDistanceProcessCalculateRepository.processCalculateTotalAmountDistanceRoute(location1.getId(),location2.getId(),codeCompany);
		assertNotNull(result);
		assertEquals(result.toString(),"8.0");


		LOGGER.info("----------- END CASE 4 ------------");
	}
	@Test
	public void processCalculateDistance_CaseDistance_BetweenInScope_And_RangeDistance_Have_ThreeScope(){
		LOGGER.info("----------- START CASE 5 ------------");

		//  ========================= Review Data ======================

		//This Case Distance =66 Km and Rate Distance//
//		                 0-50  : 8
//		                 51-80 : 3
//		                 81+   : 1

		//  ========================= Review Data ======================


				/* initial Data */
		String masterDataCode = "M013";
		String masterDataDetailOrgCode = "1010";
		String locationCode1 = "0003";
		String locationCode2 = "0004";
		String codeCompany = "1010";


		Location location1 = locationRepository.findByCode(locationCode1);
		assertNotNull(location1);
		assertEquals(location1.getCode(),"0003");
//		assertEquals(location1.getDescription(),"สำนักงานใหญ่");
		assertEquals(location1.getLocationType(),"D");

		Location location2 = locationRepository.findByCode(locationCode2);
		assertNotNull(location2);
		assertEquals(location2.getCode(),"0004");
//		assertEquals(location2.getDescription(),"จังหวัดปัตตานี");
		assertEquals(location2.getLocationType(),"D");

		MasterData masterData =masterDataRepository.findByCode(masterDataCode);
		assertNotNull(masterData);
		assertEquals(masterData.getCode(),masterDataRepository.findOne(masterData.getId()).getCode());
		assertEquals(masterData.getName(),masterDataRepository.findOne(masterData.getId()).getName());

		Set<MasterDataDetail> setItem = masterDataDetailRepository.findByOrgCodeOrderByVariable1Asc(masterDataDetailOrgCode);
		assertNotNull(setItem);

		for(MasterDataDetail o : setItem){

			assertEquals(o.getOrgCode(),masterDataDetailRepository.findOne(o.getId()).getOrgCode());
			assertEquals(o.getName(),masterDataDetailRepository.findOne(o.getId()).getName());
		}


		Double result = routeDistanceProcessCalculateRepository.processCalculateTotalAmountDistanceRoute(location1.getId(),location2.getId(),codeCompany);
		assertNotNull(result);
		assertEquals(result.toString(),"448.0");


		LOGGER.info("----------- END CASE 5 ------------");
	}
	@Test
	public void processCalculateDistance_CaseDistance_OutOfScope_And_ScopeDistance_Have_ThreeScope(){
		LOGGER.info("----------- START CASE 6 ------------");

		//  ========================= Review Data ======================

		//This Case Distance = 200 Km and Rate Distance//
//		                 0-50  : 8
//		                 51-80 : 3
//		                 81+   : 1

		//  ========================= Review Data ======================



				/* initial Data */
		        String masterDataCode = "M013";
		        String masterDataDetailOrgCode = "1010";
		        String locationCode1 = "0001";
		        String locationCode2 = "0002";
		        String codeCompany = "1010";


		        Location location1 = locationRepository.findByCode(locationCode1);
		        assertNotNull(location1);
		        assertEquals(location1.getCode(),"0001");
//		        assertEquals(location1.getDescription(),"โรงงานน้ำตาลมิตรผล(ด่านช้าง)");
		        assertEquals(location1.getLocationType(),"D");

				Location location2 = locationRepository.findByCode(locationCode2);
				assertNotNull(location2);
				assertEquals(location2.getCode(),"0002");
//				assertEquals(location2.getDescription(),"น้ำตาลมิตรผล สิงห์บุรี");
				assertEquals(location2.getLocationType(),"D");

				MasterData masterData =masterDataRepository.findByCode(masterDataCode);
				assertNotNull(masterData);
				assertEquals(masterData.getCode(),masterDataRepository.findOne(masterData.getId()).getCode());
				assertEquals(masterData.getName(),masterDataRepository.findOne(masterData.getId()).getName());

				Set<MasterDataDetail> setItem = masterDataDetailRepository.findByOrgCodeOrderByVariable1Asc(masterDataDetailOrgCode);
		        assertNotNull(setItem);

		        for(MasterDataDetail o : setItem){

					assertEquals(o.getOrgCode(),masterDataDetailRepository.findOne(o.getId()).getOrgCode());
					assertEquals(o.getName(),masterDataDetailRepository.findOne(o.getId()).getName());
		        }


		       Double result = routeDistanceProcessCalculateRepository.processCalculateTotalAmountDistanceRoute(location1.getId(),location2.getId(),codeCompany);
		       assertNotNull(result);
		       assertEquals(result.toString(),"610.0");


		LOGGER.info("----------- END CASE 6 ------------");
	}
	@Test
	public void processCalculateDistance_Case_DistanceZero_And_ScopeDistance_Have_TwoScope(){
		LOGGER.info("----------- START CASE 7 ------------");


		//  ========================= Review Data ======================

		//This Case Distance =0 Km and Rate Distance//
//		                 0-100  : 15
//		                 101+ : 10
//

		//  ========================= Review Data ======================


				/* initial Data */
		String masterDataCode = "M013";
		String masterDataDetailOrgCode = "10020";
		String locationCode1 = "0005";
		String locationCode2 = "0006";
		String codeCompany = "10020";


		Location location1 = locationRepository.findByCode(locationCode1);
		assertNotNull(location1);
		assertEquals(location1.getCode(),"0005");
//		assertEquals(location1.getDescription(),"จังหวัดยะลา");
		assertEquals(location1.getLocationType(),"D");

		Location location2 = locationRepository.findByCode(locationCode2);
		assertNotNull(location2);
		assertEquals(location2.getCode(),"0006");
//		assertEquals(location2.getDescription(),"จังหวัดนราธิวาส");
		assertEquals(location2.getLocationType(),"D");

		MasterData masterData =masterDataRepository.findByCode(masterDataCode);
		assertNotNull(masterData);
		assertEquals(masterData.getCode(),masterDataRepository.findOne(masterData.getId()).getCode());
		assertEquals(masterData.getName(),masterDataRepository.findOne(masterData.getId()).getName());

		Set<MasterDataDetail> setItem = masterDataDetailRepository.findByOrgCodeOrderByVariable1Asc(masterDataDetailOrgCode);
		assertNotNull(setItem);

		for(MasterDataDetail o : setItem){

			assertEquals(o.getOrgCode(),masterDataDetailRepository.findOne(o.getId()).getOrgCode());
			assertEquals(o.getName(),masterDataDetailRepository.findOne(o.getId()).getName());
		}


		Double result = routeDistanceProcessCalculateRepository.processCalculateTotalAmountDistanceRoute(location1.getId(),location2.getId(),codeCompany);
		assertNotNull(result);
		assertEquals(result.toString(),"0.0");


		LOGGER.info("----------- END CASE 7 ------------");
	}
	@Test
	public void processCalculateDistance_CaseDistance_One_AndScopeDistance_Have_TwoScope(){
		LOGGER.info("----------- START CASE 8 ------------");

		//  ========================= Review Data ======================

		//This Case Distance =1 Km and Rate Distance//
//		                 0-100  : 15
//		                 101+ : 10
//

		//  ========================= Review Data ======================



				/* initial Data */
		String masterDataCode = "M013";
		String masterDataDetailOrgCode = "10020";
		String locationCode1 = "0007";
		String locationCode2 = "0008";
		String codeCompany = "10020";


		Location location1 = locationRepository.findByCode(locationCode1);
		assertNotNull(location1);
		assertEquals(location1.getCode(),"0007");
//		assertEquals(location1.getDescription(),"อำเภอสะบ้าย้อย จังหวัดสงขลา");
		assertEquals(location1.getLocationType(),"D");

		Location location2 = locationRepository.findByCode(locationCode2);
		assertNotNull(location2);
		assertEquals(location2.getCode(),"0008");
//		assertEquals(location2.getDescription(),"อำเภอเทพา จังหวัดสงขลา");
		assertEquals(location2.getLocationType(),"D");

		MasterData masterData =masterDataRepository.findByCode(masterDataCode);
		assertNotNull(masterData);
		assertEquals(masterData.getCode(),masterDataRepository.findOne(masterData.getId()).getCode());
		assertEquals(masterData.getName(),masterDataRepository.findOne(masterData.getId()).getName());

		Set<MasterDataDetail> setItem = masterDataDetailRepository.findByOrgCodeOrderByVariable1Asc(masterDataDetailOrgCode);
		assertNotNull(setItem);

		for(MasterDataDetail o : setItem){

			assertEquals(o.getOrgCode(),masterDataDetailRepository.findOne(o.getId()).getOrgCode());
			assertEquals(o.getName(),masterDataDetailRepository.findOne(o.getId()).getName());
		}


		Double result = routeDistanceProcessCalculateRepository.processCalculateTotalAmountDistanceRoute(location1.getId(),location2.getId(),codeCompany);
		assertNotNull(result);
		assertEquals(result.toString(),"15.0");


		LOGGER.info("----------- END CASE 8 ------------");
	}
	@Test
	public void processCalculateDistance_CaseDistance_BetweenInScope_And_RangeDistance_Have_TwoScope(){
		LOGGER.info("----------- START CASE 9 ------------");

		//  ========================= Review Data ======================

		//This Case Distance =66 Km and Rate Distance//
//		                 0-100  : 15
//		                 101++ : 10


		//  ========================= Review Data ======================


				/* initial Data */
		String masterDataCode = "M013";
		String masterDataDetailOrgCode = "10020";
		String locationCode1 = "0003";
		String locationCode2 = "0004";
		String codeCompany = "10020";


		Location location1 = locationRepository.findByCode(locationCode1);
		assertNotNull(location1);
		assertEquals(location1.getCode(),"0003");
//		assertEquals(location1.getDescription(),"สำนักงานใหญ่");
		assertEquals(location1.getLocationType(),"D");

		Location location2 = locationRepository.findByCode(locationCode2);
		assertNotNull(location2);
		assertEquals(location2.getCode(),"0004");
//		assertEquals(location2.getDescription(),"จังหวัดปัตตานี");
		assertEquals(location2.getLocationType(),"D");

		MasterData masterData =masterDataRepository.findByCode(masterDataCode);
		assertNotNull(masterData);
		assertEquals(masterData.getCode(),masterDataRepository.findOne(masterData.getId()).getCode());
		assertEquals(masterData.getName(),masterDataRepository.findOne(masterData.getId()).getName());

		Set<MasterDataDetail> setItem = masterDataDetailRepository.findByOrgCodeOrderByVariable1Asc(masterDataDetailOrgCode);
		assertNotNull(setItem);

		for(MasterDataDetail o : setItem){

			assertEquals(o.getOrgCode(),masterDataDetailRepository.findOne(o.getId()).getOrgCode());
			assertEquals(o.getName(),masterDataDetailRepository.findOne(o.getId()).getName());
		}


		Double result = routeDistanceProcessCalculateRepository.processCalculateTotalAmountDistanceRoute(location1.getId(),location2.getId(),codeCompany);
		assertNotNull(result);
		assertEquals(result.toString(),"990.0");


		LOGGER.info("----------- END CASE 9 ------------");
	}
	@Test
	public void processCalculateDistance_CaseDistance_OutOfScope_And_ScopeDistance_Have_TwoScope(){
		LOGGER.info("----------- START CASE 10 ------------");

		//  ========================= Review Data ======================

		//This Case Distance = 200 Km and Rate Distance//
//		                 0-100  : 15
//		                 101+ : 10


		//  ========================= Review Data ======================



				/* initial Data */
		String masterDataCode = "M013";
		String masterDataDetailOrgCode = "10020";
		String locationCode1 = "0001";
		String locationCode2 = "0002";
		String codeCompany = "10020";


		Location location1 = locationRepository.findByCode(locationCode1);
		assertNotNull(location1);
		assertEquals(location1.getCode(),"0001");
//		assertEquals(location1.getDescription(),"โรงงานน้ำตาลมิตรผล(ด่านช้าง)");
		assertEquals(location1.getLocationType(),"D");

		Location location2 = locationRepository.findByCode(locationCode2);
		assertNotNull(location2);
		assertEquals(location2.getCode(),"0002");
//		assertEquals(location2.getDescription(),"น้ำตาลมิตรผล สิงห์บุรี");
		assertEquals(location2.getLocationType(),"D");

		MasterData masterData =masterDataRepository.findByCode(masterDataCode);
		assertNotNull(masterData);
		assertEquals(masterData.getCode(),masterDataRepository.findOne(masterData.getId()).getCode());
		assertEquals(masterData.getName(),masterDataRepository.findOne(masterData.getId()).getName());

		Set<MasterDataDetail> setItem = masterDataDetailRepository.findByOrgCodeOrderByVariable1Asc(masterDataDetailOrgCode);
		assertNotNull(setItem);

		for(MasterDataDetail o : setItem){

			assertEquals(o.getOrgCode(),masterDataDetailRepository.findOne(o.getId()).getOrgCode());
			assertEquals(o.getName(),masterDataDetailRepository.findOne(o.getId()).getName());
		}


		Double result = routeDistanceProcessCalculateRepository.processCalculateTotalAmountDistanceRoute(location1.getId(),location2.getId(),codeCompany);
		assertNotNull(result);
		assertEquals(result.toString(),"2500.0");


		LOGGER.info("----------- END CASE 10 ------------");
	}




	@Test
	public void processCalculateDistance_CaseNotFound_RouteDistace(){
		LOGGER.info("----------- START CASE 10 ------------");




		Double result = routeDistanceProcessCalculateRepository.processCalculateTotalAmountDistanceRoute(Long.valueOf(10),Long.valueOf(12),"1010");
//		assertEquals(result,null);
		assertEquals(result.toString(),"0.0");


		LOGGER.info("----------- END CASE 10 ------------");
	}





}
