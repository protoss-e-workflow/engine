package com.spt.engine.controller;

import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class DocumentControllerTest {

	static final Logger LOGGER = LoggerFactory.getLogger(DocumentControllerTest.class);

	@Autowired
	protected WebApplicationContext wac;
	protected MockMvc mockMvc;

	protected MediaType APPLICATION_JSON_UTF8 = new MediaType(
			MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Before
	public void setup()
	{
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	
	@Test
	public void saveDocumentTest(){
		try {
            mockMvc.perform(post("/documentCustom/")
					.contentType(MediaType.APPLICATION_JSON)
            		.content("{\"updateDate\":\"2017-08-21 10:24:31\",\"documentType\":\"APP\",\"updateBy\":\"ss\",\"documentStatus\":\"DRF\"}")
                    .param("costCenter","1")
                    .param("department", "1")
                    .param("company", "1"))
                    .andExpect(status().isOk());
        }catch (Exception e){
            e.printStackTrace();
        }
	}
}
