package com.spt.engine.service;
/**
 * 	Create by SiriradC. 2017.07.17
 * 	Test Process Calculate PerDiem 
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.converter.AdvanceSAPConverter;
import com.spt.engine.converter.ExpenseSAPConverter;
import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentAdvance;
import com.spt.engine.entity.app.DocumentExpense;
import com.spt.engine.entity.app.DocumentExpenseItem;
import com.spt.engine.entity.app.DocumentReference;
import com.spt.engine.repository.app.DocumentRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BasicClearingServiceTest {

	static final Logger LOGGER = LoggerFactory.getLogger(BasicClearingServiceTest.class);
	
	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };
    private SimpleDateFormat  sdfYYYYMMDDHHmmss = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    @Value("${AppServer}")
	public  String AppServer ;
	
	@Autowired
	ExpenseTranferService expenseTranferService;
	
	@Autowired
	DocumentRepository documentRepository;
	
	@PersistenceContext
    private EntityManager entityManager;
	
	@Test
	@Transactional
	public void expense_expenseVat7WHT3_EquaAdvanceTest(){
		    Map<String,String> advanceMap = requestAdvance(599.20d,"00006138");
		    String advanceDocNumber = advanceMap.get("advanceDocNumber");
		    String advanceSapDocNumber = advanceMap.get("sapDocNumber");
		    String expenseDocNumber = "EXP_1010_"+sdfYYYYMMDDHHmmss.format(new Date());
			Document document = new Document();
			document.setDocumentType("EXP");
			document.setCompanyCode("1010");
			document.setTotalAmount(599.20d);
			document.setSendDate(new Timestamp(new Date().getTime()));
			document.setDocNumber(expenseDocNumber);
			document.setPersonalId("00006138");
			document.setCostCenterCode("1019213000");
			document.setPsa("0001");
			documentRepository.save(document);
			
			DocumentExpense documentExpense = new DocumentExpense();
			documentExpense.setDocument(document);
			entityManager.persist(documentExpense);
			
			DocumentExpenseItem item1 = new DocumentExpenseItem();
			item1.setDocNumber("INV0001");
			item1.setGlCode("810400");
			item1.setDocumentDate(new Timestamp(new Date().getTime()));
			//item1.setInternalOrder("30534010040");
			item1.setBranch("00000");
			item1.setBusinessPlace("0000");
			item1.setSeller("SoftSquare");
			item1.setTaxIDNumber("1309800003482");
			
			/* Amount */
			item1.setAmount(560.0d);
			/* Vat */
			item1.setVat(39.2d);
			item1.setVatCode("V7");
			item1.setVatCodeValue("7");
			/* WHT */
			//item1.setWhtAmount(16.8);
			//item1.setWhtType("P4");
			//item1.setWhtCode("02");
			/* Net Amount */
			item1.setNetAmount(599.2d);
			
			item1.setExchangeRate("33.12");
			item1.setAddress("79ม.3ถ.ปู่เจ้าสมิงพรายต.บางหญ้าแพรก อ.พระประแดง จ.สมุทรปราการ 10130");
			item1.setRemark("ค่าใช้จ่ายทดสอบ");
			entityManager.persist(item1);
			
			
			Set<DocumentExpenseItem> documentExpenseItems = new HashSet();
			documentExpenseItems.add(item1);
			
			
			documentExpense.setDocumentExpenseItems(documentExpenseItems);
			entityManager.merge(documentExpense);
			
			
			
			
			DocumentReference documentReference = new DocumentReference();
			documentReference.setDocumentTypeCode("APP");
			documentReference.setDocReferenceNumber("APP_1010_20171013R0000001");
			entityManager.persist(documentReference);
			
			DocumentReference documentReference2 = new DocumentReference();
			documentReference2.setDocumentTypeCode("ADV");
			documentReference2.setDocReferenceNumber(advanceDocNumber);
			entityManager.persist(documentReference2);

			
			Set<DocumentReference> documentReferenceSet = new HashSet();
			documentReferenceSet.add(documentReference);
			documentReferenceSet.add(documentReference2);

			
			document.setDocumentReferences(documentReferenceSet);
			document.setDocumentExpense(documentExpense);
			entityManager.merge(document);
			
			
			Map paramCheck = new HashMap();
			paramCheck.put("id", document.getId());
			paramCheck.put("appServer", AppServer);
			paramCheck.put("advanceSapDocNumber", advanceSapDocNumber);
			paramCheck.put("advanceDocNumber", advanceDocNumber);
			
			
			/* Validate JSON*/
			Map returnMap = ExpenseSAPConverter.toSAPStructure(document,paramCheck);
			String jsonString = (String) returnMap.get("jsonData");
			Map jsonMapVerify = gson.fromJson(jsonString, Map.class);
			List jsonRootVerify = (List) jsonMapVerify.get("DOCUMENTHEADER");
			assertEquals(1,jsonRootVerify.size());
			Map jsonDocumentVerify = (Map) jsonRootVerify.get(0);
			assertEquals(advanceSapDocNumber,jsonDocumentVerify.get("HEADER_TXT"));
			List jsonItemVerify = (List) jsonDocumentVerify.get("ACCOUNTGL_ITEM");
			assertEquals(3,jsonItemVerify.size());
			
			/* Item 1 */
			Map jsonItem1Verify = (Map) jsonItemVerify.get(0);
			assertEquals("001",jsonItem1Verify.get("ITEMNO_ACC"));
			assertEquals(null,jsonItem1Verify.get("VENDOR_NO"));
			assertEquals("S",jsonItem1Verify.get("ACCT_TYPE"));
			assertEquals("V7",jsonItem1Verify.get("TAX_CODE"));
			assertEquals(advanceDocNumber,jsonItem1Verify.get("ALLOC_NMBR"));
			assertEquals(560.0d,jsonItem1Verify.get("AMT_DOCCUR"));
			assertEquals("0.0000",jsonItem1Verify.get("AMT_BASE"));
			assertEquals("0.0000",jsonItem1Verify.get("TAX_AMT"));
			assertEquals(new HashMap(),jsonItem1Verify.get("WITTHOLDINGTAX"));
			
			/* Item 2 */
			Map jsonItem2Verify = (Map) jsonItemVerify.get(1);
			assertEquals("002",jsonItem2Verify.get("ITEMNO_ACC"));
			assertEquals("M0006138",jsonItem2Verify.get("VENDOR_NO"));
			assertEquals("K",jsonItem2Verify.get("ACCT_TYPE"));
			assertEquals("V7",jsonItem2Verify.get("TAX_CODE"));
			assertEquals(advanceDocNumber,jsonItem2Verify.get("ALLOC_NMBR"));
			assertEquals("-599.2",jsonItem2Verify.get("AMT_DOCCUR"));
			assertEquals("0.0000",jsonItem2Verify.get("AMT_BASE"));
			assertEquals("0.0000",jsonItem2Verify.get("TAX_AMT"));
			Map holdingMap = new HashMap();
//			holdingMap.put("ITEMNO_ACC", "001");
//			holdingMap.put("WT_TYPE", "P4");
//			holdingMap.put("WT_CODE","02");  
//			holdingMap.put("BAS_AMT_TC", "560.0");  
//			holdingMap.put("MAN_AMT_TC","16.8");
			assertEquals(holdingMap,jsonItem2Verify.get("WITTHOLDINGTAX"));
			

			/* Item 3 */
			Map jsonItem3Verify = (Map) jsonItemVerify.get(2);
			assertEquals("003",jsonItem3Verify.get("ITEMNO_ACC"));
			assertEquals("V7",jsonItem3Verify.get("TAX_CODE"));
			assertEquals("7",jsonItem3Verify.get("TAX_RATE"));
			assertEquals(39.2d,jsonItem3Verify.get("AMT_DOCCUR"));
			assertEquals(560.0d,jsonItem3Verify.get("AMT_BASE"));
			assertEquals(39.2d,jsonItem3Verify.get("TAX_AMT"));
			assertEquals(new HashMap(),jsonItem3Verify.get("WITTHOLDINGTAX"));
			
			/* Execute */
			Map param = new HashMap();
			param.put("id", document.getId());
			Map retMap = expenseTranferService.expenseTranfer(param);
			String jsonReturn = (String) retMap.get("returnStr");
			LOGGER.info("expense return ={}",jsonReturn);
			String sapDocNumber = null;
			String sapClearingDocNumber = null;
			
			List<Map<String,String>> dataReturn = gson.fromJson(jsonReturn, List.class);
			for(Map<String,String> dataMap:dataReturn){
				if("S".equals(dataMap.get("returnType"))){
					for(String itemId:String.valueOf(dataMap.get("expenseItemId")).split(",")){
						DocumentExpenseItem item = entityManager.find(DocumentExpenseItem.class, Long.valueOf(itemId));
						item.setExternalDocNumber(dataMap.get("sapDocNumber"));
						sapDocNumber = dataMap.get("sapDocNumber");
						entityManager.merge(document);
					}
				}
				
				if("C".equals(dataMap.get("returnType"))){
					LOGGER.info("Clearing Number ={}",dataMap.get("sapDocNumber"));
					sapClearingDocNumber = dataMap.get("sapDocNumber");
				}
				
				
			}
			assertNotNull(sapDocNumber);
			assertNotNull(sapClearingDocNumber);
		}
	
	
	@Transactional
	private Map<String,String> requestAdvance(Double amount,String personalId){
		String advanceDocNumber = "ADV_1010_"+sdfYYYYMMDDHHmmss.format(new Date());
		Map<String,String> retMap = new HashMap<String,String>();
		Document document = new Document();
		document.setDocumentType("ADV");
		document.setCompanyCode("1010");
		document.setSendDate(new Timestamp(new Date().getTime()));
		document.setDocNumber(advanceDocNumber);
		document.setTotalAmount(amount);
		document.setPersonalId(personalId);
		document.setCompanyCode("1010");
		document.setPsa("0001");
		
		DocumentAdvance documentAdvance = new DocumentAdvance();
		documentAdvance.setAdvancePurpose("For Test");
		documentAdvance.setAmount(amount);
		document.setDocumentAdvance(documentAdvance);
		documentRepository.save(document);
		      
		Map param = new HashMap();
		param.put("id", document.getId());
		param.put("appServer", AppServer);
		/* Validate JSON*/
		String jsonString = AdvanceSAPConverter.toSAPStructure(document,param);
		Map jsonMapVerify = gson.fromJson(jsonString, Map.class);
		Map jsonDocumentVerify = (Map) jsonMapVerify.get("DOCUMENTHEADER");
		List jsonItemVerify = (List) jsonDocumentVerify.get("ACCOUNTGL_ITEM");
		assertEquals(2,jsonItemVerify.size());
		
		/* Item 1 */
		Map jsonItem1Verify = (Map) jsonItemVerify.get(0);
		assertEquals("001",jsonItem1Verify.get("ITEMNO_ACC"));
		assertEquals("E",jsonItem1Verify.get("SP_GL_IND"));
		assertEquals("NVAT",jsonItem1Verify.get("J_1TPBUPL"));
		assertEquals("K",jsonItem1Verify.get("ACCT_TYPE"));
		assertEquals(advanceDocNumber,jsonItem1Verify.get("ALLOC_NMBR"));
		assertEquals(""+amount,jsonItem1Verify.get("AMT_DOCCUR"));
		assertEquals(new HashMap(),jsonItem1Verify.get("WITTHOLDINGTAX"));
		
		/* Item 2 */
		Map jsonItem2Verify = (Map) jsonItemVerify.get(1);
		assertEquals("002",jsonItem2Verify.get("ITEMNO_ACC"));
		assertEquals(null,jsonItem2Verify.get("SP_GL_IND"));
		assertEquals("NVAT",jsonItem2Verify.get("J_1TPBUPL"));
		assertEquals("K",jsonItem2Verify.get("ACCT_TYPE"));
		assertEquals(advanceDocNumber,jsonItem2Verify.get("ALLOC_NMBR"));
		assertEquals("-"+amount,jsonItem2Verify.get("AMT_DOCCUR"));
		assertEquals(new HashMap(),jsonItem2Verify.get("WITTHOLDINGTAX"));
		
		
		/* Execute */
		String jsonReturn = expenseTranferService.advanceTranfer(param);
		LOGGER.info("advance return ={}",jsonReturn);
		
		String sapDocNumber = null;
		List<Map<String,String>> returnList = gson.fromJson(jsonReturn, List.class);
		//Example:
		//[{"sapDocNumber":"","returnClass":"ZCCA1","returnMessage":"FI - ระบุ Branch Code เป็นค่า NVAT หรื่อตัวเลข 5 หลักเท่านั้น","returnNumber":"036","returnType":"E"}]
		for(Map<String,String> returnData :returnList){
			if("S".equals(returnData.get("returnType"))){
				documentAdvance.setExternalDocNumber(String.valueOf(returnData.get("sapDocNumber")));
				sapDocNumber=returnData.get("sapDocNumber");
			}
			
		}
		retMap.put("sapDocNumber", sapDocNumber);
		retMap.put("advanceDocNumber", advanceDocNumber);
		return retMap;
	}
}
