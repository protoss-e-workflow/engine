package com.spt.engine.service;
/**
 * 	Create by SiriradC. 2017.07.17
 * 	Test Process Calculate PerDiem
 */

import com.spt.engine.entity.app.*;
import com.spt.engine.repository.app.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class CheckAccommodationAuthorizeServiceTest {

    static final Logger LOGGER = LoggerFactory.getLogger(CheckAccommodationAuthorizeServiceTest.class);



    @Autowired
    InternalMemberAccomodationDetailRepository restDetailRepository;


    @Autowired
    CheckAccommodationAuthorizeService checkAccommodationAuthorizeService;




    @Autowired
    ExternalMemberAccomodationDetailRepository externalMemberAccomodationDetailRepository;




    @Before
    public void initialData(){


//			======================= CASE PASS [2 Person] =====================================

        InternalMemberAccomodationDetail restDetail_test1 = new InternalMemberAccomodationDetail();
        restDetail_test1.setDocNumber("DOC_TEST1234");
        restDetail_test1.setItemId((long) 1);
        restDetail_test1.setCost((double) 600);
        restDetail_test1.setLvEmp("C2");
        restDetail_test1.setEmpUser("suriya_e");
        restDetailRepository.save(restDetail_test1);



        InternalMemberAccomodationDetail restDetail_test2 = new InternalMemberAccomodationDetail();
        restDetail_test2.setDocNumber("DOC_TEST1234");
        restDetail_test2.setItemId((long) 1);
        restDetail_test2.setCost((double) 1000);
        restDetail_test2.setLvEmp("C9");
        restDetail_test2.setEmpUser("panupong_c");
        restDetailRepository.save(restDetail_test2);





//			======================= CASE PASS [3 Person Have and OverCost 300 ] =====================================

        InternalMemberAccomodationDetail restDetail_test3 = new InternalMemberAccomodationDetail();
        restDetail_test3.setDocNumber("DOC_TEST789");
        restDetail_test3.setItemId((long) 2);
        restDetail_test3.setCost((double) 1000+300);
        restDetail_test3.setLvEmp("C9");
        restDetail_test3.setEmpUser("suriya_e");
        restDetailRepository.save(restDetail_test3);

        InternalMemberAccomodationDetail restDetail_test4 = new InternalMemberAccomodationDetail();
        restDetail_test4.setDocNumber("DOC_TEST789");
        restDetail_test4.setItemId((long) 2);
        restDetail_test4.setCost((double) 800+300);
        restDetail_test4.setLvEmp("C7");
        restDetail_test4.setEmpUser("ratthanan_w");
        restDetailRepository.save(restDetail_test4);


        InternalMemberAccomodationDetail restDetail_test5 = new InternalMemberAccomodationDetail();
        restDetail_test5.setDocNumber("DOC_TEST789");
        restDetail_test5.setItemId((long) 2);
        restDetail_test5.setCost((double) 600+300);
        restDetail_test5.setLvEmp("C5");
        restDetail_test5.setEmpUser("panupong_c");
        restDetailRepository.save(restDetail_test5);







        //			======================= CASE PASS [3 Person Have EmployeeLv more than 10 ] =====================================

        InternalMemberAccomodationDetail restDetail_test6 = new InternalMemberAccomodationDetail();
        restDetail_test6.setDocNumber("DOC_TEST000");
        restDetail_test6.setItemId((long) 3);
        restDetail_test6.setCost((double) 1000);
        restDetail_test6.setLvEmp("C12");
        restDetail_test6.setEmpUser("suriya_e");
        restDetailRepository.save(restDetail_test6);

        InternalMemberAccomodationDetail restDetail_test7 = new InternalMemberAccomodationDetail();
        restDetail_test7.setDocNumber("DOC_TEST000");
        restDetail_test7.setItemId((long) 3);
        restDetail_test7.setCost((double) 800);
        restDetail_test7.setLvEmp("C7");
        restDetail_test7.setEmpUser("ratthanan_w");
        restDetailRepository.save(restDetail_test7);


        InternalMemberAccomodationDetail restDetail_test8 = new InternalMemberAccomodationDetail();
        restDetail_test8.setDocNumber("DOC_TEST000");
        restDetail_test8.setItemId((long) 3);
        restDetail_test8.setCost((double) 600);
        restDetail_test8.setLvEmp("C5");
        restDetail_test8.setEmpUser("panupong_c");
        restDetailRepository.save(restDetail_test8);







        //			======================= CASE PASS [2 Person Cost Over than empLv > 200 ] =====================================

        InternalMemberAccomodationDetail restDetail_test9 = new InternalMemberAccomodationDetail();
        restDetail_test9.setDocNumber("DOC_TEST1234");
        restDetail_test9.setItemId((long) 4);
        restDetail_test9.setCost((double) 800);
        restDetail_test9.setLvEmp("C2");
        restDetail_test9.setEmpUser("suriya_e");
        restDetailRepository.save(restDetail_test9);



        InternalMemberAccomodationDetail restDetail_test10 = new InternalMemberAccomodationDetail();
        restDetail_test10.setDocNumber("DOC_TEST1234");
        restDetail_test10.setItemId((long) 4);
        restDetail_test10.setCost((double) 1200);
        restDetail_test10.setLvEmp("C9");
        restDetail_test10.setEmpUser("panupong_c");
        restDetailRepository.save(restDetail_test10);





        //			======================= CASE PASS [3 InternalMember 2 ExternalMember ] =====================================

        InternalMemberAccomodationDetail restDetail_test11 = new InternalMemberAccomodationDetail();
        restDetail_test11.setDocNumber("DOC_TEST1234");
        restDetail_test11.setItemId((long) 5);
        restDetail_test11.setCost((double) 300);
        restDetail_test11.setLvEmp("C2");
        restDetail_test11.setEmpUser("suriya_e");
        restDetailRepository.save(restDetail_test11);



        InternalMemberAccomodationDetail restDetail_test12 = new InternalMemberAccomodationDetail();
        restDetail_test12.setDocNumber("DOC_TEST1234");
        restDetail_test12.setItemId((long) 5);
        restDetail_test12.setCost((double) 300);
        restDetail_test12.setLvEmp("C6");
        restDetail_test12.setEmpUser("panupong_c");
        restDetailRepository.save(restDetail_test12);

        InternalMemberAccomodationDetail restDetail_test13 = new InternalMemberAccomodationDetail();
        restDetail_test13.setDocNumber("DOC_TEST1234");
        restDetail_test13.setItemId((long) 5);
        restDetail_test13.setCost((double) 300);
        restDetail_test13.setLvEmp("C9");
        restDetail_test13.setEmpUser("ratthanan_w");
        restDetailRepository.save(restDetail_test13);


        ExternalMemberAccomodationDetail externalMemberAccomodationDetail_1 = new ExternalMemberAccomodationDetail();
        externalMemberAccomodationDetail_1.setCost((double) 300);
        externalMemberAccomodationDetail_1.setDocNumber("DOC_TEST1234");
        externalMemberAccomodationDetail_1.setItemId((long) 5);
        externalMemberAccomodationDetailRepository.save(externalMemberAccomodationDetail_1);

        ExternalMemberAccomodationDetail externalMemberAccomodationDetail_2 = new ExternalMemberAccomodationDetail();
        externalMemberAccomodationDetail_2.setCost((double) 300);
        externalMemberAccomodationDetail_2.setDocNumber("DOC_TEST1234");
        externalMemberAccomodationDetail_2.setItemId((long) 5);
        externalMemberAccomodationDetailRepository.save(externalMemberAccomodationDetail_2);







    }




    @Transactional
    @Test
    public void caseTwoPersonInternalMember_ZoneTypeA_ShouldBe_PassResult(){

        final String   docNumber   = "DOC_TEST1234";
        final Long     itemId      = Long.valueOf(1);
        final String   zoneType    = "A";
        final boolean  flagIbis_And_standard = false;



        boolean result = checkAccommodationAuthorizeService.checkAccommodationAuthorize(docNumber,itemId,flagIbis_And_standard,zoneType);
        assertTrue(result);



    }



    @Transactional
    @Test
    public void caseThreePersonInternalMember_ZoneTypeA_ShouldBe_PassResult(){

        final String   docNumber   = "DOC_TEST789";
        final Long     itemId      = Long.valueOf(2);
        final String   zoneType    = "A";
        final boolean  flagIbis_And_standard = false;



        boolean result = checkAccommodationAuthorizeService.checkAccommodationAuthorize(docNumber,itemId,flagIbis_And_standard,zoneType);
        assertTrue(result);



    }


    @Transactional
    @Test
    public void caseThreePersonInternalMember_NoZoneType_And_HaveEmployeeLvMoreThan10_ShouldBe_PassResult(){

        final String   docNumber   = "DOC_TEST000";
        final Long     itemId      = Long.valueOf(3);
        final String   zoneType    = null;
        final boolean  flagIbis_And_standard = false;



        boolean result = checkAccommodationAuthorizeService.checkAccommodationAuthorize(docNumber,itemId,flagIbis_And_standard,zoneType);
        assertTrue(result);



    }


    @Transactional
    @Test
    public void caseTwoPersonInternalMember_HotelisIbis_ShouldBe_PassResult(){

        final String   docNumber   = "DOC_TEST1234";
        final Long     itemId      = Long.valueOf(1);
        final String   zoneType    = null;
        final boolean  flagIbis_And_standard = true;



        boolean result = checkAccommodationAuthorizeService.checkAccommodationAuthorize(docNumber,itemId,flagIbis_And_standard,zoneType);
        assertTrue(result);



    }




    @Transactional
    @Test
    public void caseThreePersonInternalMember_ZoneTypeB_ShouldBe_PassResult(){

        final String   docNumber   = "DOC_TEST1234";
        final Long     itemId      = Long.valueOf(4);
        final String   zoneType    = "B";
        final boolean  flagIbis_And_standard = false;



        boolean result = checkAccommodationAuthorizeService.checkAccommodationAuthorize(docNumber,itemId,flagIbis_And_standard,zoneType);
        assertTrue(result);



    }




    @Transactional
    @Test
    public void caseThreePersonInternalMember_NoZoneType_ShouldBe_FailResult_OverCost(){

        final String   docNumber   = "DOC_TEST1234";
        final Long     itemId      = Long.valueOf(4);
        final String   zoneType    = null;
        final boolean  flagIbis_And_standard = false;



        boolean result = checkAccommodationAuthorizeService.checkAccommodationAuthorize(docNumber,itemId,flagIbis_And_standard,zoneType);
        assertFalse(result);



    }



    @Transactional
    @Test
    public void case_ThreeInternalMember_TwoExternalMember_Shouldbe_PassResult(){

        final String   docNumber   = "DOC_TEST1234";
        final Long     itemId      = Long.valueOf(5);
        final String   zoneType    = null;
        final boolean  flagIbis_And_standard = false;



        boolean result = checkAccommodationAuthorizeService.checkAccommodationAuthorize(docNumber,itemId,flagIbis_And_standard,zoneType);
        assertTrue(result);



    }


    @Transactional
    @Test
    public void case_NotFound_Data_Shouldbe_ResultFail(){

        final String   docNumber   = "DOC_9955CC";
        final Long     itemId      = Long.valueOf(5);
        final String   zoneType    = null;
        final boolean  flagIbis_And_standard = false;



        boolean result = checkAccommodationAuthorizeService.checkAccommodationAuthorize(docNumber,itemId,flagIbis_And_standard,zoneType);
        assertFalse(result);



    }


    @Transactional
    @Test
    public void case_NotFound2_Data_Shouldbe_ResultFail(){

        final String   docNumber   = "DOC_TEST1234";
        final Long     itemId      = Long.valueOf(999);
        final String   zoneType    = null;
        final boolean  flagIbis_And_standard = false;



        boolean result = checkAccommodationAuthorizeService.checkAccommodationAuthorize(docNumber,itemId,flagIbis_And_standard,zoneType);
        assertFalse(result);



    }




}