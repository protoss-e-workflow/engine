package com.spt.engine.service;

import com.spt.engine.entity.app.*;
import com.spt.engine.repository.app.DocumentApproveItemRepository;
import com.spt.engine.repository.app.DocumentRepository;
import com.spt.engine.repository.app.LocationRepository;
import com.spt.engine.repository.app.TravelDetailRepository;
import com.spt.engine.util.AbstractReportJasperPDF;
import com.spt.engine.util.DateUtil;
import net.sf.jasperreports.engine.JasperPrint;
import org.apache.poi.util.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@Transactional
public class ReportServiceTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportServiceTest.class);


    @Autowired
    ReportService reportService;

    @Autowired
    DocumentRepository documentRepository;



    @Autowired
    DocumentApproveItemRepository documentApproveItemRepository;

    @Autowired
    LocationRepository locationRepository;


    @Autowired
    TravelDetailRepository travelDetailRepository;


    @Test
    public void printFormApproveTest() {


        try {
            Map<String, String> mapMessage = new HashMap<>();


            ByteArrayInputStream inputStream = null;
            JasperPrint jasperPrint = null;
            OutputStream out = null;
            try {
                Map<String,String> mapParam = new HashMap<>();
                mapParam.put("REPORT_APPROVE","REPORT_APPROVE");
                mapParam.put("LABEL_DOC_NUMBER","LABEL_DOC_NUMBER");
                mapParam.put("LABEL_EMPLOYEE_NAME","LABEL_EMPLOYEE_NAME");
                mapParam.put("LABEL_POSITION","LABEL_POSITION");
                mapParam.put("LABEL_DOCUMENT_CREATOR_NAME","LABEL_DOCUMENT_CREATOR_NAME");
                mapParam.put("LABEL_DOCUMENT_DATE","LABEL_DOCUMENT_DATE");
                mapParam.put("LABEL_COMPANY_AGENCY","LABEL_COMPANY_AGENCY");
                mapParam.put("LABEL_PSA_DETAIL","LABEL_PSA_DETAIL");
                mapParam.put("LABEL_COST_CENTER","LABEL_COST_CENTER");
                mapParam.put("LABEL_TRAVEL_TYPE","LABEL_TRAVEL_TYPE");
                mapParam.put("LABEL_COMPANY_AGENCY_NAME","LABEL_COMPANY_AGENCY_NAME");
                mapParam.put("LABEL_COMPANY_AGENCY_CODE","LABEL_COMPANY_AGENCY_CODE");
                mapParam.put("LABEL_LOCATION_EMPLOYEE","LABEL_LOCATION_EMPLOYEE");

                mapParam.put("LABEL_TRAVEL_INFORMATION","LABEL_TRAVEL_INFORMATION");
                mapParam.put("LABEL_ORIGIN","LABEL_ORIGIN");
                mapParam.put("LABEL_DESTINATION","LABEL_DESTINATION");
                mapParam.put("LABEL_START_DATE","LABEL_START_DATE");
                mapParam.put("LABEL_END_DATE","LABEL_ENgit sD_DATE");
                mapParam.put("LABEL_REMARK","LABEL_REMARK");

                mapParam.put("LABEL_COST_DETAIL_AND_ADVANCE","LABEL_COST_DETAIL_AND_ADVANCE");

                mapParam.put("LABEL_TRAVEL_MEMBER_NAME","LABEL_TRAVEL_MEMBER_NAME");
                mapParam.put("LABEL_EMPLOYEE_CODE","LABEL_EMPLOYEE_CODE");


                mapParam.put("LABEL_ADVANCE","LABEL_ADVANCE");
                mapParam.put("LABEL_MONEY_AMOUNT_BATH","LABEL_MONEY_AMOUNT_BATH");
                mapParam.put("LABEL_BORROW_DATE","LABEL_BORROW_DATE");
                mapParam.put("LABEL_DUE_DATE","LABEL_DUE_DATE");
                mapParam.put("LABEL_BANK_NUMBER","LABEL_BANK_NUMBER");
                mapParam.put("LABEL_CONTRACT_ACCOUNT","LABEL_CONTRACT_ACCOUNT");
                mapParam.put("LABEL_PAYMENT_TYPE","LABEL_PAYMENT_TYPE");


                mapParam.put("LABEL_ADMIN_INFORMATION","LABEL_ADMIN_INFORMATION");
                mapParam.put("LABEL_RESERVE_CAR","LABEL_RESERVE_CAR");
                mapParam.put("LABEL_BOOKING_HOTEL","LABEL_BOOKING_HOTEL");
                mapParam.put("LABEL_TICKET_BOOKING","LABEL_TICKET_BOOKING");
                mapParam.put("LABEL_CONTRACT_ADMIN","LABEL_CONTRACT_ADMIN");

                mapParam.put("LABEL_LINE_APPROVER","LABEL_LINE_APPROVER");
                mapParam.put("LABEL_POSITION","LABEL_POSITION");
                mapParam.put("LABEL_ROLES","LABEL_ROLES");
                mapParam.put("LABEL_STATUS","LABEL_STATUS");
                mapParam.put("LABEL_LIST_COST","LABEL_LIST_COST");

                List<JasperPrint> jasperPrintList = new ArrayList<>();
                jasperPrint = reportService.formReportApprove("APP1010180215R0008",mapParam,"th");
                jasperPrintList.add(jasperPrint);
                LOGGER.info("jasperPrintList Resull : {}", jasperPrintList);
                byte[] b = AbstractReportJasperPDF.generateReport(jasperPrintList);
                inputStream = new ByteArrayInputStream(b);


//                LOGGER.info("Byte Resull : {}", b);

                if (inputStream != null) {
                    String fileName = "formApprove.pdf";
                    out = new FileOutputStream("jasperreports/" + fileName);
                    IOUtils.copy(inputStream, out);
                }

            } catch (Exception e) {
                e.getMessage();
            } finally {
                IOUtils.closeQuietly(inputStream);
                IOUtils.closeQuietly(out);
            }

        } catch (Exception e) {
            LOGGER.error("error msg : {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }


    @Test
    public void printFormAdvanceTest() {


        try {
            Map<String, String> mapMessage = new HashMap<>();


            ByteArrayInputStream inputStream = null;
            JasperPrint jasperPrint = null;
            OutputStream out = null;
            try {
                Map<String,String> mapParam = new HashMap<>();
                mapParam.put("REPORT_ADVANCE","REPORT_ADVANCE");
                mapParam.put("LABEL_DOC_NUMBER","LABEL_DOC_NUMBER");
                mapParam.put("LABEL_EMPLOYEE_NAME","LABEL_EMPLOYEE_NAME");
                mapParam.put("LABEL_POSITION","LABEL_POSITION");
                mapParam.put("LABEL_DOCUMENT_CREATOR_NAME","LABEL_DOCUMENT_CREATOR_NAME");
                mapParam.put("LABEL_DOCUMENT_DATE","LABEL_DOCUMENT_DATE");
                mapParam.put("LABEL_COMPANY_AGENCY","LABEL_COMPANY_AGENCY");
                mapParam.put("LABEL_PSA_DETAIL","LABEL_PSA_DETAIL");
                mapParam.put("LABEL_COST_CENTER","LABEL_COST_CENTER");
                mapParam.put("LABEL_TRAVEL_TYPE","LABEL_TRAVEL_TYPE");
                mapParam.put("LABEL_COMPANY_AGENCY_NAME","LABEL_COMPANY_AGENCY_NAME");
                mapParam.put("LABEL_COMPANY_AGENCY_CODE","LABEL_COMPANY_AGENCY_CODE");
                mapParam.put("LABEL_LOCATION_EMPLOYEE","LABEL_LOCATION_EMPLOYEE");

                mapParam.put("LABEL_TRAVEL_INFORMATION","LABEL_TRAVEL_INFORMATION");
                mapParam.put("LABEL_ORIGIN","LABEL_ORIGIN");
                mapParam.put("LABEL_DESTINATION","LABEL_DESTINATION");
                mapParam.put("LABEL_START_DATE","LABEL_START_DATE");
                mapParam.put("LABEL_END_DATE","LABEL_END_DATE");
                mapParam.put("LABEL_REMARK","LABEL_REMARK");

                mapParam.put("LABEL_COST_DETAIL_AND_ADVANCE","LABEL_COST_DETAIL_AND_ADVANCE");

                mapParam.put("LABEL_TRAVEL_MEMBER_NAME","LABEL_TRAVEL_MEMBER_NAME");
                mapParam.put("LABEL_EMPLOYEE_CODE","LABEL_EMPLOYEE_CODE");


                mapParam.put("LABEL_ADVANCE","LABEL_ADVANCE");
                mapParam.put("LABEL_MONEY_AMOUNT_BATH","LABEL_MONEY_AMOUNT_BATH");
                mapParam.put("LABEL_BORROW_DATE","LABEL_BORROW_DATE");
                mapParam.put("LABEL_DUE_DATE","LABEL_DUE_DATE");
                mapParam.put("LABEL_BANK_NUMBER","LABEL_BANK_NUMBER");
                mapParam.put("LABEL_CONTRACT_ACCOUNT","LABEL_CONTRACT_ACCOUNT");
                mapParam.put("LABEL_PAYMENT_TYPE","LABEL_PAYMENT_TYPE");


                mapParam.put("LABEL_ADMIN_INFORMATION","LABEL_ADMIN_INFORMATION");
                mapParam.put("LABEL_RESERVE_CAR","LABEL_RESERVE_CAR");
                mapParam.put("LABEL_BOOKING_HOTEL","LABEL_BOOKING_HOTEL");
                mapParam.put("LABEL_TICKET_BOOKING","LABEL_TICKET_BOOKING");
                mapParam.put("LABEL_CONTRACT_ADMIN","LABEL_CONTRACT_ADMIN");

                mapParam.put("LABEL_LINE_APPROVER","LABEL_LINE_APPROVER");
                mapParam.put("LABEL_POSITION","LABEL_POSITION");
                mapParam.put("LABEL_ROLES","LABEL_ROLES");
                mapParam.put("LABEL_STATUS","LABEL_STATUS");
                mapParam.put("LABEL_LIST_COST","LABEL_LIST_COST");
                mapParam.put("LABEL_DOC_REF","LABEL_DOC_REF");
                mapParam.put("LABEL_ADVANCE_REMARK","LABEL_ADVANCE_REMARK");
                mapParam.put("LABEL_ADVANCE_PURPOSE","LABEL_ADVANCE_PURPOSE");

                List<JasperPrint> jasperPrintList = new ArrayList<>();
                jasperPrint = reportService.formReportAdvance("ADV1010180409R0001",mapParam,"en");
                jasperPrintList.add(jasperPrint);
                LOGGER.info("jasperPrintList Resull : {}", jasperPrintList);
                byte[] b = AbstractReportJasperPDF.generateReport(jasperPrintList);
                inputStream = new ByteArrayInputStream(b);


//                LOGGER.info("Byte Resull : {}", b);

                if (inputStream != null) {
                    String fileName = "formAdvance.pdf";
                    out = new FileOutputStream("jasperreports/" + fileName);
                    IOUtils.copy(inputStream, out);
                }

            } catch (Exception e) {
                e.getMessage();
            } finally {
                IOUtils.closeQuietly(inputStream);
                IOUtils.closeQuietly(out);
            }

        } catch (Exception e) {
            LOGGER.error("error msg : {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }


    @Test
    public void printFormExpenseTest() {


        try {
            Map<String, String> mapMessage = new HashMap<>();


            ByteArrayInputStream inputStream = null;
            JasperPrint jasperPrint = null;
            OutputStream out = null;
            try {
                Map<String,String> mapParam = new HashMap<>();
                mapParam.put("REPORT_EXPENSE","REPORT_EXPENSE");
                mapParam.put("LABEL_DOC_NUMBER","LABEL_DOC_NUMBER");
                mapParam.put("LABEL_EMPLOYEE_NAME","LABEL_EMPLOYEE_NAME");
                mapParam.put("LABEL_POSITION","LABEL_POSITION");
                mapParam.put("LABEL_DOCUMENT_CREATOR_NAME","LABEL_DOCUMENT_CREATOR_NAME");
                mapParam.put("LABEL_DOCUMENT_DATE","LABEL_DOCUMENT_DATE");
                mapParam.put("LABEL_COMPANY_AGENCY","LABEL_COMPANY_AGENCY");
                mapParam.put("LABEL_PSA_DETAIL","LABEL_PSA_DETAIL");
                mapParam.put("LABEL_COST_CENTER","LABEL_COST_CENTER");
                mapParam.put("LABEL_TRAVEL_TYPE","LABEL_TRAVEL_TYPE");
                mapParam.put("LABEL_COMPANY_AGENCY_NAME","LABEL_COMPANY_AGENCY_NAME");
                mapParam.put("LABEL_COMPANY_AGENCY_CODE","LABEL_COMPANY_AGENCY_CODE");
                mapParam.put("LABEL_LOCATION_EMPLOYEE","LABEL_LOCATION_EMPLOYEE");

                mapParam.put("LABEL_TRAVEL_INFORMATION","LABEL_TRAVEL_INFORMATION");
                mapParam.put("LABEL_ORIGIN","LABEL_ORIGIN");
                mapParam.put("LABEL_DESTINATION","LABEL_DESTINATION");
                mapParam.put("LABEL_START_DATE","LABEL_START_DATE");
                mapParam.put("LABEL_END_DATE","LABEL_END_DATE");
                mapParam.put("LABEL_REMARK","LABEL_REMARK");

                mapParam.put("LABEL_COST_DETAIL_AND_ADVANCE","LABEL_COST_DETAIL_AND_ADVANCE");

                mapParam.put("LABEL_TRAVEL_MEMBER_NAME","LABEL_TRAVEL_MEMBER_NAME");
                mapParam.put("LABEL_EMPLOYEE_CODE","LABEL_EMPLOYEE_CODE");


                mapParam.put("LABEL_ADVANCE","LABEL_ADVANCE");
                mapParam.put("LABEL_MONEY_AMOUNT_BATH","LABEL_MONEY_AMOUNT_BATH");
                mapParam.put("LABEL_BORROW_DATE","LABEL_BORROW_DATE");
                mapParam.put("LABEL_DUE_DATE","LABEL_DUE_DATE");
                mapParam.put("LABEL_BANK_NUMBER","LABEL_BANK_NUMBER");
                mapParam.put("LABEL_CONTRACT_ACCOUNT","LABEL_CONTRACT_ACCOUNT");
                mapParam.put("LABEL_PAYMENT_TYPE","LABEL_PAYMENT_TYPE");


                mapParam.put("LABEL_ADMIN_INFORMATION","LABEL_ADMIN_INFORMATION");
                mapParam.put("LABEL_RESERVE_CAR","LABEL_RESERVE_CAR");
                mapParam.put("LABEL_BOOKING_HOTEL","LABEL_BOOKING_HOTEL");
                mapParam.put("LABEL_TICKET_BOOKING","LABEL_TICKET_BOOKING");
                mapParam.put("LABEL_CONTRACT_ADMIN","LABEL_CONTRACT_ADMIN");

                mapParam.put("LABEL_LINE_APPROVER","LABEL_LINE_APPROVER");
                mapParam.put("LABEL_POSITION","LABEL_POSITION");
                mapParam.put("LABEL_ROLES","LABEL_ROLES");
                mapParam.put("LABEL_STATUS","LABEL_STATUS");
                mapParam.put("LABEL_LIST_COST","LABEL_LIST_COST");
                mapParam.put("LABEL_DOC_REF","LABEL_DOC_REF");
                mapParam.put("LABEL_ADVANCE_REMARK","LABEL_ADVANCE_REMARK");
                mapParam.put("LABEL_ADVANCE_PURPOSE","LABEL_ADVANCE_PURPOSE");


                mapParam.put("LABEL_DOCUMENT_REFERENCE","LABEL_DOCUMENT_REFERENCE");
                mapParam.put("LABEL_APPROVE_DOCUMENT_REFERENCE","LABEL_APPROVE_DOCUMENT_REFERENCE");
                mapParam.put("LABEL_DOCUMENT_ADVANCE","LABEL_DOCUMENT_ADVANCE");
                mapParam.put("LABEL_EXPENSE_DETAIL","LABEL_EXPENSE_DETAIL");
                mapParam.put("LABEL_LIST_EXPENSE","LABEL_LIST_EXPENSE");
                mapParam.put("LABEL_MONEY_AMOUNT_BATH","LABEL_MONEY_AMOUNT_BATH");

                List<JasperPrint> jasperPrintList = new ArrayList<>();
                jasperPrint = reportService.formReportExpense("EXP1010180410R0005",mapParam,"en");
                jasperPrintList.add(jasperPrint);
                LOGGER.info("jasperPrintList Resull : {}", jasperPrintList);
                byte[] b = AbstractReportJasperPDF.generateReport(jasperPrintList);
                inputStream = new ByteArrayInputStream(b);


//                LOGGER.info("Byte Resull : {}", b);

                if (inputStream != null) {
                    String fileName = "formExpense.pdf";
                    out = new FileOutputStream("jasperreports/" + fileName);
                    IOUtils.copy(inputStream, out);
                }

            } catch (Exception e) {
                e.getMessage();
            } finally {
                IOUtils.closeQuietly(inputStream);
                IOUtils.closeQuietly(out);
            }

        } catch (Exception e) {
            LOGGER.error("error msg : {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

}
