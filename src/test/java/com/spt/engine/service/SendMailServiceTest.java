package com.spt.engine.service;
/**
 * 	Create by SiriradC. 2017.07.17
 * 	Test Process Calculate PerDiem 
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.engine.converter.AdvanceSAPConverter;
import com.spt.engine.converter.ExpenseSAPConverter;
import com.spt.engine.entity.app.Document;
import com.spt.engine.entity.app.DocumentAdvance;
import com.spt.engine.entity.app.DocumentExpense;
import com.spt.engine.entity.app.DocumentExpenseItem;
import com.spt.engine.entity.app.DocumentReference;
import com.spt.engine.repository.app.DocumentRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SendMailServiceTest {

	static final Logger LOGGER = LoggerFactory.getLogger(SendMailServiceTest.class);
	
	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };
    private SimpleDateFormat  sdfYYYYMMDDHHmmss = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

	@Autowired
	SendMailService sendMailService;
	
	@Test
	public void sendMail_Test(){
		String from="XXX";
		String fromAddress="suriya_e@softsquaregroup.com";
		String addressTo="suriya_e@softsquaregroup.com";
		String subject="Tesssss";
		String contentText="ffffffffffff";
		String ccAddress="suriya_e@softsquaregroup.com";
		String[] pathfileLs= {"//Users//se//Desktop//kkk"};
		String[] fileNameLs= {"suriya.png"};
		sendMailService.sendMail(from, fromAddress, addressTo, subject, contentText, ccAddress,pathfileLs,fileNameLs);
	}
	
}
