$Host.UI.RawUI.WindowTitle="Build GWFEngine"
Write-Host "Build Project."
Try
{
    mvn clean compile "-Dmaven.test.skip=true" package
    Write-Host "Extract File"
    mkdir C:\Work\GWF\MPGGWFEngine\target\temp
    [System.Reflection.Assembly]::LoadWithPartialName('System.IO.Compression.FileSystem')
    [System.IO.Compression.ZipFile]::ExtractToDirectory("C:\Work\GWF\MPGGWFEngine\target\GWFEngine-0.99.jar", "C:\Work\GWF\MPGGWFEngine\target\temp")
    # Expand-Archive target\GWFEngine-0.99.jar -DestinationPath target\temp
    Write-Host "Delete Library"
    Remove-Item C:\Work\GWF\MPGGWFEngine\target\temp\BOOT-INF\lib\*.*
    Write-Host "Convert JAR to WAR"
    Set-Location C:\Work\GWF\MPGGWFEngine\target\temp
    jar cfm ..\GWFEngine.war META-INF\MANIFEST.MF BOOT-INF META-INF org
    Set-Location ..\..\
    Write-Host "--== Complete ==--"
}
Catch
{
    Write-Error $_.Exception.Message
}